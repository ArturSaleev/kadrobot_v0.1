<nav class="navbar navbar-expand navbar-light navbar-white">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- Left Side Of Navbar -->
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button">
                    <i class="icon ion-md-menu"></i>
                </a>
            </li>
            <li class="nav-item">
                <label class="nav-link">
                    @yield('title')
                </label>
            </li>
        </ul>

        <!-- Right Side Of Navbar -->
        <ul class="navbar-nav ml-auto">
            @php
                $locales = \App\Traits\LocaleTrait::getAllLanguage();
                $activeLocale = $locales[0]['name'];
                foreach($locales as $locale){
                    if(Illuminate\Support\Facades\App::currentLocale() == $locale['code']){
                        $activeLocale = $locale['name'];
                    }
                }
            @endphp
            @if(count(config('panel.available_languages', [])) > 1)
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ $activeLocale }} ({{ Illuminate\Support\Facades\App::currentLocale() }})
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        @foreach($locales as $lang)
                            <a class="dropdown-item" href="{{ url()->current() }}?change_language={{ $lang['code'] }}">{{ $lang['name'] }} ({{ $lang['code'] }})</a>
                        @endforeach
                    </div>
                </li>
            @endif
            <!-- Authentication Links -->
            @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('auth.login_text') }}</a>
                </li>
{{--                @if (Route::has('register'))--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>--}}
{{--                    </li>--}}
{{--                @endif--}}
            @else
                <li class="nav-item">
                    <span class="nav-link" style="margin-right: 30px"></span>
                </li>
            @endguest
        </ul>
    </div>
</nav>
