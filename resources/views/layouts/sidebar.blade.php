<?php
$menus = [
    [
        "icon" => '<i class="nav-icon icon ion-ios-business"></i>',
        "title" => __("crud.structure"),
        "child" => [
            [
                "class" => App\Models\Company::class,
                "route" => ['companies.show', '1'],
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.companies.name"),
            ],
            [
                "class" => App\Models\Branch::class,
                "route" => 'branches.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.branches.name"),
            ],
            [
                "class" => App\Models\Department::class,
                "route" => 'departments.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.departments.name"),
            ],
            [
                "class" => App\Models\Position::class,
                "route" => 'positions.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.positions.name"),
            ],
        ]
    ],
    [
        "icon" => '<i class="nav-icon icon ion-md-people"></i>',
        "title" => __("crud.employees.name"),
        "child" => [
            [
                "class" => App\Models\Employee::class,
                "route" => 'employees.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.employees.index_title"),
            ],
            [
                "class" => App\Models\Curator::class,
                "route" => 'curators.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.curators.name"),
            ],
        ]
    ],
    [
        "icon" => '<i class="nav-icon icon ion-md-book"></i>',
        "title" => __("crud.references"),
        "child" => [
            [
                "class" => App\Models\RefAccountType::class,
                "route" => 'ref-account-types.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.ref_account_types.name"),
            ],
//            [
//                "class" => App\Models\RefAction::class,
//                "route" => 'ref-actions.index',
//                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
//                "title" => __("crud.ref_actions.name"),
//            ],
            [
                "class" => App\Models\RefAddressType::class,
                "route" => 'ref-address-types.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.ref_address_types.name"),
            ],
            [
                "class" => App\Models\RefAllowanceType::class,
                "route" => 'ref-allowance-types.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.ref_allowance_types.name"),
            ],
            [
                "class" => App\Models\RefBank::class,
                "route" => 'ref-banks.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.ref_banks.name"),
            ],
            [
                "class" => App\Models\RefCountry::class,
                "route" => 'ref-countries.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.ref_countries.name"),
            ],
            [
                "class" => App\Models\RefDocPlace::class,
                "route" => 'ref-doc-places.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.ref_doc_places.name"),
            ],
            [
                "class" => App\Models\RefDocType::class,
                "route" => 'ref-doc-types.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.ref_doc_types.name"),
            ],
            [
                "class" => App\Models\RefFamilyState::class,
                "route" => 'ref-family-states.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.ref_family_states.name"),
            ],
            [
                "class" => App\Models\RefKindHoliday::class,
                "route" => 'ref-kind-holidays.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.ref_kind_holidays.name"),
            ],
            [
                "class" => App\Models\RefNationality::class,
                "route" => 'ref-nationalities.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.ref_nationalities.name"),
            ],
            [
                "class" => App\Models\RefOked::class,
                "route" => 'ref-okeds.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.ref_okeds.name"),
            ],
            [
                "class" => App\Models\RefPersonState::class,
                "route" => 'ref-person-states.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.ref_person_states.name"),
            ],
            [
                "class" => App\Models\RefSex::class,
                "route" => 'ref-sexes.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.ref_sexes.name"),
            ],
            [
                "class" => App\Models\RefStatus::class,
                "route" => 'ref-statuses.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.ref_statuses.name"),
            ],
            [
                "class" => App\Models\RefTechnicType::class,
                "route" => 'ref-technic-types.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.ref_technic_types.name"),
            ],
            [
                "class" => App\Models\RefTransportTrip::class,
                "route" => 'ref-transport-trips.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.ref_transport_trips.name"),
            ],
            [
                "class" => App\Models\RefTypePhone::class,
                "route" => 'ref-type-phones.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.ref_type_phones.name"),
            ],
            [
                "class" => App\Models\RefTypeRodstv::class,
                "route" => 'ref-type-rodstvs.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.ref_type_rodstvs.name"),
            ],
            [
                "class" => App\Models\RefUserStatus::class,
                "route" => 'ref-user-statuses.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.ref_user_statuses.name"),
            ],
            [
                "class" => App\Models\RefVidHoliday::class,
                "route" => 'ref-vid-holidays.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.ref_vid_holidays.name"),
            ],
            [
                "class" => App\Models\RefVidHolidayType::class,
                "route" => 'ref-vid-holiday-types.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.ref_vid_holiday_types.name"),
            ],
            [
                "class" => App\Models\Holiday::class,
                "route" => 'holidays.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.holidays.name"),
            ],
        ]
    ],

    [
        "icon" => '<i class="nav-icon icon ion-md-people"></i>',
        "title" => __("crud.reports"),
        "child" => [
            [
                "class" => App\Models\RefMeta::class,
                "route" => 'ref-metas.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.ref_metas.name"),
            ],
            [
                "class" => App\Models\T2Card::class,
                "route" => 't2-cards.index',
                "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
                "title" => __("crud.t2_cards.name"),
            ]
        ]
    ],
    [
        "class" => App\Models\TemplateDoc::class,
        "route" => 'template_docs.index',
        "icon" => '<i class="nav-icon icon ion-md-document"></i>',
        "title" => __("crud.templates_docs"),
    ],

//    [
//        "class" => App\Models\BranchAddreses::class,
//        "route" => 'all-branch-addreses.index',
//        "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
//        "title" => __("crud.branch_addreses.name"),
//    ],
//    [
//        "class" => App\Models\BranchPhone::class,
//        "route" => 'branch-phones.index',
//        "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
//        "title" => __("crud.branch_phones.name"),
//    ],
//    [
//        "class" => App\Models\DepartmentPhone::class,
//        "route" => 'department-phones.index',
//        "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
//        "title" => __("crud.department_phones.name"),
//    ],
//    [
//        "class" => App\Models\EmployeeAddress::class,
//        "route" => 'employee-addresses.index',
//        "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
//        "title" => __("crud.employee_addresses.name"),
//    ],
//    [
//        "class" => App\Models\EmployeeDeclension::class,
//        "route" => 'employee-declensions.index',
//        "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
//        "title" => __("crud.employee_declensions.name"),
//    ],
//    [
//        "class" => App\Models\EmployeeDocument::class,
//        "route" => 'employee-documents.index',
//        "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
//        "title" => __("crud.employee_documents.name"),
//    ],
//    [
//        "class" => App\Models\EmployeeEducation::class,
//        "route" => 'employee-educations.index',
//        "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
//        "title" => __("crud.employee_educations.name"),
//    ],
//    [
//        "class" => App\Models\EmployeeFamily::class,
//        "route" => 'employee-families.index',
//        "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
//        "title" => __("crud.employee_families.name"),
//    ],
//    [
//        "class" => App\Models\EmployeeHoliday::class,
//        "route" => 'employee-holidays.index',
//        "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
//        "title" => __("crud.employee_holidays.name"),
//    ],
//    [
//        "class" => App\Models\EmployeeHolidaysPeriod::class,
//        "route" => 'employee-holidays-periods.index',
//        "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
//        "title" => __("crud.employee_holidays_periods.name"),
//    ],
//    [
//        "class" => App\Models\EmployeeHospital::class,
//        "route" => 'employee-hospitals.index',
//        "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
//        "title" => __("crud.employee_hospitals.name"),
//    ],
//    [
//        "class" => App\Models\EmployeeInvalide::class,
//        "route" => 'employee-invalides.index',
//        "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
//        "title" => __("crud.employee_invalides.name"),
//    ],
//    [
//        "class" => App\Models\EmployeeMilitary::class,
//        "route" => 'employee-militaries.index',
//        "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
//        "title" => __("crud.employee_militaries.name"),
//    ],
//    [
//        "class" => App\Models\EmployeePayAllowance::class,
//        "route" => 'employee-pay-allowances.index',
//        "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
//        "title" => __("crud.employee_pay_allowances.name"),
//    ],
//    [
//        "class" => App\Models\EmployeePhone::class,
//        "route" => 'employee-phones.index',
//        "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
//        "title" => __("crud.employee_phones.name"),
//    ],
//    [
//        "class" => App\Models\EmployeeStazh::class,
//        "route" => 'employee-stazhs.index',
//        "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
//        "title" => __("crud.employee_stazhs.name"),
//    ],
//    [
//        "class" => App\Models\EmployeeTechic::class,
//        "route" => 'employee-techics.index',
//        "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
//        "title" => __("crud.employee_techics.name"),
//    ],
//    [
//        "class" => App\Models\EmployeeTrip::class,
//        "route" => 'employee-trips.index',
//        "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
//        "title" => __("crud.employee_trips.name"),
//    ],
//    [
//        "class" => App\Models\EmployeeTripFromTo::class,
//        "route" => 'employee-trip-from-tos.index',
//        "icon" => '<i class="nav-icon icon ion-md-radio-button-off"></i>',
//        "title" => __("crud.employee_trip_from_tos.name"),
//    ],

];
?>

<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ url('/') }}" class="brand-link">
        <img src="{{ asset('images/kadrobot.svg') }}" alt="KADROBOT" class="brand-image bg-white img-circle">
        <span class="brand-text font-weight-light ml-2">KADROBOT</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent" data-widget="treeview" role="menu">

                @auth
                    <li class="nav-item">
                        <a href="{{ route('home') }}" class="nav-link {{ (Request::route()?->getName() == 'home') ? 'active' : '' }}">
                            <i class="nav-icon icon ion-md-apps"></i>
                            <p>
                                {{ __("crud.main") }}
                            </p>
                        </a>
                    </li>

                    @foreach($menus as $child)
                        @if(isset($child['child']))
                            @php
                                $activeRouteMain = '';
                                foreach($child['child'] as $child_new){
                                    if($activeRouteMain == ''){
                                        $routeName = (is_array($child_new['route'])) ? $child_new['route'][0] : $child_new['route'];
                                        $activeRouteMain = (Request::route()?->getName() == $routeName) ? 'menu-open menu-is-opening' : '';
                                    }
                                }
                            @endphp

                            <li class="nav-item {{ $activeRouteMain }}">
                                <a href="#" class="nav-link">
                                    {!! $child['icon'] !!}
                                    <p>
                                        {{ $child['title'] }}
                                        <i class="nav-icon right icon ion-md-arrow-round-back"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview" {!! ($activeRouteMain !== "") ? 'style="display: block;"' : ''  !!}>
                                    @foreach($child['child'] as $child_new)
                                        @can('view-any', $child_new['class'])
                                            <?php
                                                $route = (is_array($child_new['route'])) ? route($child_new['route'][0], $child_new['route'][1]) : route($child_new['route']);
                                                $routeName = (is_array($child_new['route'])) ? $child_new['route'][0] : $child_new['route'];
                                                $activeRoute = (Request::route()?->getName() == $routeName) ? 'active' : '';
                                            ?>
                                            <li class="nav-item">
                                                <a href="{{ $route }}" class="nav-link {{ $activeRoute }}">
                                                            <span class="sidebar-icon">
                                                                {!! $child_new['icon'] !!}
                                                            </span>
                                                    <span class="sidebar-text">{{$child_new['title']}}</span>
                                                </a>
                                            </li>
                                        @endcan
                                    @endforeach

                                </ul>
                            </li>



                        @else
                            @can('view-any', $child['class'])
                                <li class="nav-item">
                                    <a href="{{ route($child['route']) }}" class="nav-link {{ (Request::route()?->getName() == $child['route']) ? 'active' : '' }}">
                                        <span class="sidebar-icon">
                                            {!! $child['icon'] !!}
                                        </span>
                                        <span class="sidebar-text">{{$child['title']}}</span>
                                    </a>
                                </li>
                            @endcan
                        @endif
                    @endforeach

                    @if (Auth::user()->can('view-any', Spatie\Permission\Models\Role::class) ||
                        Auth::user()->can('view-any', Spatie\Permission\Models\Permission::class))
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon icon ion-md-key"></i>
                                <p>
                                    Access Management
                                    <i class="nav-icon right icon ion-md-arrow-round-back"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @can('view-any', Spatie\Permission\Models\Role::class)
                                    <li class="nav-item">
                                        <a href="{{ route('roles.index') }}" class="nav-link">
                                            <i class="nav-icon icon ion-md-radio-button-off"></i>
                                            <p>Roles</p>
                                        </a>
                                    </li>
                                @endcan

                                @can('view-any', Spatie\Permission\Models\Permission::class)
                                    <li class="nav-item">
                                        <a href="{{ route('permissions.index') }}" class="nav-link">
                                            <i class="nav-icon icon ion-md-radio-button-off"></i>
                                            <p>Permissions</p>
                                        </a>
                                    </li>
                                @endcan

                                    @can('view-any', \App\Models\User::class)
                                        <li class="nav-item">
                                            <a href="{{ route('users.index') }}" class="nav-link">
                                                <i class="nav-icon icon ion-md-radio-button-off"></i>
                                                <p>{{ __("crud.users.name") }}</p>
                                            </a>
                                        </li>
                                    @endcan
                            </ul>
                        </li>
                    @endif

                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="nav-icon icon ion-md-exit"></i>
                            <p>@lang('Logout')</p>
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                @endauth
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
