@extends('layouts.app')

@section('title')
    @lang('crud.ref_family_states.show_title')
@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <div class="mt-0">
                @include('components.lang_name_array_show', ['model' => $refFamilyState])
            </div>

            <div class="mt-4">
                <a
                    href="{{ route('ref-family-states.index') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\RefFamilyState::class)
                <a
                    href="{{ route('ref-family-states.create') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
