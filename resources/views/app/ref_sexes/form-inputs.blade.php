@php $editing = isset($refSex) @endphp

<div class="row">
    <div class="col-lg-12 col-12 col-sm-12">
        @include('components.lang_name_array_edit', ['model' => ($editing) ? $refSex : null, 'editing' => $editing, 'noCol' => true])
    </div>
</div>
