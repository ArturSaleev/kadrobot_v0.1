@extends('layouts.app')

@section('title')
    @lang('crud.ref_banks.show_title')
@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <div class="mt-4">
                <div class="mt-0">
                    @include('components.lang_name_array_show', ['model' => $refAddressType])
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.ref_banks.inputs.ref_status_id')</h5>
                    <span>{{ optional($refBank->refStatus)->id ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.ref_banks.inputs.mfo')</h5>
                    <span>{{ $refBank->mfo ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.ref_banks.inputs.mfo_head')</h5>
                    <span>{{ $refBank->mfo_head ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.ref_banks.inputs.mfo_rkc')</h5>
                    <span>{{ $refBank->mfo_rkc ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.ref_banks.inputs.kor_account')</h5>
                    <span>{{ $refBank->kor_account ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.ref_banks.inputs.commis')</h5>
                    <span>{{ $refBank->commis ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.ref_banks.inputs.bin')</h5>
                    <span>{{ $refBank->bin ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.ref_banks.inputs.bik_old')</h5>
                    <span>{{ $refBank->bik_old ?? '-' }}</span>
                </div>
            </div>

            <div class="mt-4">
                <a href="{{ route('ref-banks.index') }}" class="btn btn-light">
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\RefBank::class)
                <a href="{{ route('ref-banks.create') }}" class="btn btn-light">
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
