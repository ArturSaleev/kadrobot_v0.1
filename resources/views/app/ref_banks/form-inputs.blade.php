@php $editing = isset($refBank) @endphp

<div class="row">
    @include('components.lang_name_array_edit', ['model' => ($editing) ? $refAddressType : null, 'editing' => $editing])
    <div class="col-lg-6 col-12 col-sm-12">
        <x-inputs.text
            name="bin"
            label="{{ __('crud.ref_banks.inputs.bin') }}"
            value="{{ old('bin', ($editing ? $refBank->bin : '')) }}"
            maxlength="255"
            placeholder="{{ __('crud.ref_banks.inputs.bin') }}"
        ></x-inputs.text>
        <x-inputs.text
            name="bik_old"
            label="{{ __('crud.ref_banks.inputs.bik_old') }}"
            value="{{ old('bik_old', ($editing ? $refBank->bik_old : '')) }}"
            maxlength="255"
            placeholder="{{ __('crud.ref_banks.inputs.bik_old') }}"
        ></x-inputs.text>

        <x-inputs.select name="ref_status_id"
                         label="{{ __('crud.ref_banks.inputs.ref_status_id') }}"
                         required>
            @php $selected = old('ref_status_id', ($editing ? $refBank->ref_status_id : '1')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
            @foreach($refStatuses as $value)
                <option value="{{ $value->id }}" {{ $selected == $value->id ? 'selected' : '' }} >{{ $value->name }}</option>
            @endforeach
        </x-inputs.select>

        <x-inputs.text
            name="mfo"
            label="{{ __('crud.ref_banks.inputs.mfo') }}"
            value="{{ old('mfo', ($editing ? $refBank->mfo : '')) }}"
            maxlength="255"
            placeholder="{{ __('crud.ref_banks.inputs.mfo') }}"
        ></x-inputs.text>

        <x-inputs.text
            name="mfo_head"
            label="{{ __('crud.ref_banks.inputs.mfo_head') }}"
            value="{{ old('mfo_head', ($editing ? $refBank->mfo_head : '')) }}"
            maxlength="255"
            placeholder="{{ __('crud.ref_banks.inputs.mfo_head') }}"
        ></x-inputs.text>
        <x-inputs.text
            name="mfo_rkc"
            label="{{ __('crud.ref_banks.inputs.mfo_rkc') }}"
            value="{{ old('mfo_rkc', ($editing ? $refBank->mfo_rkc : '')) }}"
            maxlength="255"
            placeholder="{{ __('crud.ref_banks.inputs.mfo_rkc') }}"
        ></x-inputs.text>
        <x-inputs.text
            name="kor_account"
            label="{{ __('crud.ref_banks.inputs.kor_account') }}"
            value="{{ old('kor_account', ($editing ? $refBank->kor_account : '')) }}"
            maxlength="255"
            placeholder="{{ __('crud.ref_banks.inputs.kor_account') }}"
        ></x-inputs.text>
        <x-inputs.number
            name="commis"
            label="{{ __('crud.ref_banks.inputs.commis') }}"
            value="{{ old('commis', ($editing ? $refBank->commis : '0')) }}"
            max="255"
            step="0.01"
            placeholder="{{ __('crud.ref_banks.inputs.commis') }}"
            required
        ></x-inputs.number>

    </div>
</div>
