@php $editing = isset($employeeTripFromTo) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="employee_trip_id" label="Employee Trip" required>
            @php $selected = old('employee_trip_id', ($editing ? $employeeTripFromTo->employee_trip_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
            @foreach($employeeTrips as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.textarea
            name="from_place"
            label="From Place"
            maxlength="255"
            required
            >{{ old('from_place', ($editing ? $employeeTripFromTo->from_place :
            '')) }}</x-inputs.textarea
        >
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.textarea
            name="to_place"
            label="To Place"
            maxlength="255"
            required
            >{{ old('to_place', ($editing ? $employeeTripFromTo->to_place : ''))
            }}</x-inputs.textarea
        >
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.select
            name="ref_transport_trip_id"
            label="Ref Transport Trip"
            required
        >
            @php $selected = old('ref_transport_trip_id', ($editing ? $employeeTripFromTo->ref_transport_trip_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
            @foreach($refTransportTrips as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>
</div>
