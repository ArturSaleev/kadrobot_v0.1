@extends('layouts.app')

@section('title')

@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a
                    href="{{ route('employee-trip-from-tos.index') }}"
                    class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.employee_trip_from_tos.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_trip_from_tos.inputs.employee_trip_id')
                    </h5>
                    <span
                        >{{
                        optional($employeeTripFromTo->employeeTrip)->date_begin
                        ?? '-' }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_trip_from_tos.inputs.from_place')
                    </h5>
                    <span>{{ $employeeTripFromTo->from_place ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_trip_from_tos.inputs.to_place')
                    </h5>
                    <span>{{ $employeeTripFromTo->to_place ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_trip_from_tos.inputs.ref_transport_trip_id')
                    </h5>
                    <span
                        >{{ optional($employeeTripFromTo->refTransportTrip)->id
                        ?? '-' }}</span
                    >
                </div>
            </div>

            <div class="mt-4">
                <a
                    href="{{ route('employee-trip-from-tos.index') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\EmployeeTripFromTo::class)
                <a
                    href="{{ route('employee-trip-from-tos.create') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
