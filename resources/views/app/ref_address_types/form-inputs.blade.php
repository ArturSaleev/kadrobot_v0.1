@php $editing = isset($refAddressType) @endphp

<div class="row">
    <div class="col-lg-12 col-12 col-sm-12">
        @include('components.lang_name_array_edit', ['model' => ($editing) ? $refAddressType : null, 'editing' => $editing, 'noCol' => true])
    </div>
</div>
