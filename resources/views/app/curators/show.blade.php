@extends('layouts.app')

@section('title')
    @lang('crud.curators.show_title')
@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.curators.inputs.employee_id')</h5>
                    <span>{{ optional($curator->employee)->iin ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.curators.inputs.department_id')</h5>
                    <span
                        >{{ optional($curator->department)->email ?? '-'
                        }}</span
                    >
                </div>
            </div>

            <div class="mt-4">
                <a href="{{ route('curators.index') }}" class="btn btn-light">
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\Curator::class)
                <a href="{{ route('curators.create') }}" class="btn btn-light">
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
