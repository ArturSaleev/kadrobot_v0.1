@php
    $editing = isset($curator);
@endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="employee_id"
                         class="select2"
                         label="{{ __('crud.curators.inputs.employee_id') }}"
                         required>
            @php $selected = old('employee_id', ($editing ? $curator->employee_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
            @foreach($employees as $employee)
            <option value="{{ $employee->id }}" {{ $selected == $employee->id ? 'selected' : '' }} >{{ $employee->employeeFio() }} ({{ $employee->iin }})</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="department_id"
                         class="select2"
                         label="{{ __('crud.curators.inputs.department_id') }}"
                         required>
            @php $selected = old('department_id', ($editing ? $curator->department_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
            @foreach($departments as $department)
            <option value="{{ $department->id }}" {{ $selected == $department->id ? 'selected' : '' }} >{{ $department->name }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>
</div>
