@php $editing = isset($employeeTechic) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="employee_id" label="Employee" required>
            @php $selected = old('employee_id', ($editing ? $employeeTechic->employee_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
            @foreach($employees as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.select
            name="ref_technic_type_id"
            label="Ref Technic Type"
            required
        >
            @php $selected = old('ref_technic_type_id', ($editing ? $employeeTechic->ref_technic_type_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
            @foreach($refTechnicTypes as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="invent_num"
            label="Invent Num"
            value="{{ old('invent_num', ($editing ? $employeeTechic->invent_num : '')) }}"
            maxlength="255"
            placeholder="Invent Num"
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.number
            name="price"
            label="Price"
            value="{{ old('price', ($editing ? $employeeTechic->price : '')) }}"
            max="255"
            step="0.01"
            placeholder="Price"
        ></x-inputs.number>
    </x-inputs.group>
</div>
