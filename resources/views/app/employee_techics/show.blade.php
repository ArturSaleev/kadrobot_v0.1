@extends('layouts.app')

@section('title')

@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('employee-techics.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.employee_techics.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.employee_techics.inputs.employee_id')</h5>
                    <span
                        >{{ optional($employeeTechic->employee)->iin ?? '-'
                        }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_techics.inputs.ref_technic_type_id')
                    </h5>
                    <span
                        >{{ optional($employeeTechic->refTechnicType)->name ??
                        '-' }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_techics.inputs.invent_num')</h5>
                    <span>{{ $employeeTechic->invent_num ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_techics.inputs.price')</h5>
                    <span>{{ $employeeTechic->price ?? '-' }}</span>
                </div>
            </div>

            <div class="mt-4">
                <a
                    href="{{ route('employee-techics.index') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\EmployeeTechic::class)
                <a
                    href="{{ route('employee-techics.create') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
