@extends('layouts.app')

@section('title')

@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('employee-families.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.employee_families.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.employee_families.inputs.employee_id')</h5>
                    <span
                        >{{ optional($employeeFamily->employee)->iin ?? '-'
                        }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_families.inputs.lastname')</h5>
                    <span>{{ $employeeFamily->lastname ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_families.inputs.firstname')</h5>
                    <span>{{ $employeeFamily->firstname ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_families.inputs.middlename')</h5>
                    <span>{{ $employeeFamily->middlename ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_families.inputs.birthday')</h5>
                    <span>{{ $employeeFamily->birthday ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_families.inputs.ref_family_state_id')
                    </h5>
                    <span
                        >{{ optional($employeeFamily->refFamilyState)->id ?? '-'
                        }}</span
                    >
                </div>
            </div>

            <div class="mt-4">
                <a
                    href="{{ route('employee-families.index') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\EmployeeFamily::class)
                <a
                    href="{{ route('employee-families.create') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
