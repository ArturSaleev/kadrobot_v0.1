@php $editing = isset($refAction) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.number
            name="act_type"
            label="Act Type"
            value="{{ old('act_type', ($editing ? $refAction->act_type : '0')) }}"
            max="255"
            placeholder="Act Type"
        ></x-inputs.number>
    </x-inputs.group>
</div>
