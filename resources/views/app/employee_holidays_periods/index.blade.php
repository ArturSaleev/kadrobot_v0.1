@extends('layouts.app')

@section('title')

@endsection

@section('content')
<div class="container">
    <div class="searchbar mt-0 mb-4">
        <div class="row">
            <div class="col-md-6">
                <form>
                    <div class="input-group">
                        <input
                            id="indexSearch"
                            type="text"
                            name="search"
                            placeholder="{{ __('crud.common.search') }}"
                            value="{{ $search ?? '' }}"
                            class="form-control"
                            autocomplete="off"
                        />
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-primary">
                                <i class="icon ion-md-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6 text-right">
                @can('create', App\Models\EmployeeHolidaysPeriod::class)
                <a
                    href="{{ route('employee-holidays-periods.create') }}"
                    class="btn btn-primary"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div style="display: flex; justify-content: space-between;">
                <h4 class="card-title">
                    @lang('crud.employee_holidays_periods.index_title')
                </h4>
            </div>

            <div class="table-responsive">
                <table class="table table-borderless table-hover">
                    <thead>
                        <tr>
                            <th class="text-left">
                                @lang('crud.employee_holidays_periods.inputs.employee_id')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_holidays_periods.inputs.period_start')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_holidays_periods.inputs.period_end')
                            </th>
                            <th class="text-right">
                                @lang('crud.employee_holidays_periods.inputs.day_count_used_for_today')
                            </th>
                            <th class="text-right">
                                @lang('crud.employee_holidays_periods.inputs.didnt_add')
                            </th>
                            <th class="text-right">
                                @lang('crud.employee_holidays_periods.inputs.paying_for_health')
                            </th>
                            <th class="text-center">
                                @lang('crud.common.actions')
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($employeeHolidaysPeriods as
                        $employeeHolidaysPeriod)
                        <tr>
                            <td>
                                {{
                                optional($employeeHolidaysPeriod->employee)->iin
                                ?? '-' }}
                            </td>
                            <td>
                                {{ $employeeHolidaysPeriod->period_start ?? '-'
                                }}
                            </td>
                            <td>
                                {{ $employeeHolidaysPeriod->period_end ?? '-' }}
                            </td>
                            <td>
                                {{
                                $employeeHolidaysPeriod->day_count_used_for_today
                                ?? '-' }}
                            </td>
                            <td>
                                {{ $employeeHolidaysPeriod->didnt_add ?? '-' }}
                            </td>
                            <td>
                                {{ $employeeHolidaysPeriod->paying_for_health ??
                                '-' }}
                            </td>
                            <td class="text-center" style="width: 134px;">
                                <div
                                    role="group"
                                    aria-label="Row Actions"
                                    class="btn-group"
                                >
                                    @can('update', $employeeHolidaysPeriod)
                                    <a
                                        href="{{ route('employee-holidays-periods.edit', $employeeHolidaysPeriod) }}"
                                    >
                                        <button
                                            type="button"
                                            class="btn btn-light"
                                        >
                                            <i class="icon ion-md-create"></i>
                                        </button>
                                    </a>
                                    @endcan @can('view',
                                    $employeeHolidaysPeriod)
                                    <a
                                        href="{{ route('employee-holidays-periods.show', $employeeHolidaysPeriod) }}"
                                    >
                                        <button
                                            type="button"
                                            class="btn btn-light"
                                        >
                                            <i class="icon ion-md-eye"></i>
                                        </button>
                                    </a>
                                    @endcan @can('delete',
                                    $employeeHolidaysPeriod)
                                    <form
                                        action="{{ route('employee-holidays-periods.destroy', $employeeHolidaysPeriod) }}"
                                        method="POST"
                                        onsubmit="return confirm('{{ __('crud.common.are_you_sure') }}')"
                                    >
                                        @csrf @method('DELETE')
                                        <button
                                            type="submit"
                                            class="btn btn-light text-danger"
                                        >
                                            <i class="icon ion-md-trash"></i>
                                        </button>
                                    </form>
                                    @endcan
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="7">
                                @lang('crud.common.no_items_found')
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="7">
                                {!! $employeeHolidaysPeriods->render() !!}
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
