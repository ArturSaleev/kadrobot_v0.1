@extends('layouts.app')

@section('title')

@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a
                    href="{{ route('employee-holidays-periods.index') }}"
                    class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.employee_holidays_periods.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_holidays_periods.inputs.employee_id')
                    </h5>
                    <span
                        >{{ optional($employeeHolidaysPeriod->employee)->iin ??
                        '-' }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_holidays_periods.inputs.period_start')
                    </h5>
                    <span
                        >{{ $employeeHolidaysPeriod->period_start ?? '-'
                        }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_holidays_periods.inputs.period_end')
                    </h5>
                    <span
                        >{{ $employeeHolidaysPeriod->period_end ?? '-' }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_holidays_periods.inputs.day_count_used_for_today')
                    </h5>
                    <span
                        >{{ $employeeHolidaysPeriod->day_count_used_for_today ??
                        '-' }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_holidays_periods.inputs.didnt_add')
                    </h5>
                    <span>{{ $employeeHolidaysPeriod->didnt_add ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_holidays_periods.inputs.paying_for_health')
                    </h5>
                    <span
                        >{{ $employeeHolidaysPeriod->paying_for_health ?? '-'
                        }}</span
                    >
                </div>
            </div>

            <div class="mt-4">
                <a
                    href="{{ route('employee-holidays-periods.index') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\EmployeeHolidaysPeriod::class)
                <a
                    href="{{ route('employee-holidays-periods.create') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
