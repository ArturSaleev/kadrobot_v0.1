@php $editing = isset($employeeHolidaysPeriod) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="employee_id" label="Employee" required>
            @php $selected = old('employee_id', ($editing ? $employeeHolidaysPeriod->employee_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
            @foreach($employees as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.date
            name="period_start"
            label="Period Start"
            value="{{ old('period_start', ($editing ? optional($employeeHolidaysPeriod->period_start)->format('Y-m-d') : '')) }}"
            max="255"
            required
        ></x-inputs.date>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.date
            name="period_end"
            label="Period End"
            value="{{ old('period_end', ($editing ? optional($employeeHolidaysPeriod->period_end)->format('Y-m-d') : '')) }}"
            max="255"
            required
        ></x-inputs.date>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.number
            name="day_count_used_for_today"
            label="Day Count Used For Today"
            value="{{ old('day_count_used_for_today', ($editing ? $employeeHolidaysPeriod->day_count_used_for_today : '')) }}"
            max="255"
            placeholder="Day Count Used For Today"
            required
        ></x-inputs.number>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.number
            name="didnt_add"
            label="Didnt Add"
            value="{{ old('didnt_add', ($editing ? $employeeHolidaysPeriod->didnt_add : '')) }}"
            max="255"
            placeholder="Didnt Add"
            required
        ></x-inputs.number>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.number
            name="paying_for_health"
            label="Paying For Health"
            value="{{ old('paying_for_health', ($editing ? $employeeHolidaysPeriod->paying_for_health : '')) }}"
            max="255"
            placeholder="Paying For Health"
            required
        ></x-inputs.number>
    </x-inputs.group>
</div>
