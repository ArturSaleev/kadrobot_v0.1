@extends('layouts.app')

@section('title')

@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('employee-educations.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.employee_educations.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_educations.inputs.employee_id')
                    </h5>
                    <span
                        >{{ optional($employeeEducation->employee)->iin ?? '-'
                        }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_educations.inputs.institution')
                    </h5>
                    <span>{{ $employeeEducation->institution ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_educations.inputs.year_begin')</h5>
                    <span>{{ $employeeEducation->year_begin ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_educations.inputs.year_end')</h5>
                    <span>{{ $employeeEducation->year_end ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_educations.inputs.date_begin')</h5>
                    <span>{{ $employeeEducation->date_begin ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_educations.inputs.date_end')</h5>
                    <span>{{ $employeeEducation->date_end ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_educations.inputs.speciality')</h5>
                    <span>{{ $employeeEducation->speciality ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_educations.inputs.qualification')
                    </h5>
                    <span>{{ $employeeEducation->qualification ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_educations.inputs.diplom_num')</h5>
                    <span>{{ $employeeEducation->diplom_num ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_educations.inputs.diplom_date')
                    </h5>
                    <span>{{ $employeeEducation->diplom_date ?? '-' }}</span>
                </div>
            </div>

            <div class="mt-4">
                <a
                    href="{{ route('employee-educations.index') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\EmployeeEducation::class)
                <a
                    href="{{ route('employee-educations.create') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
