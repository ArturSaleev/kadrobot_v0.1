@php $editing = isset($employeeEducation) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="employee_id" label="Employee" required>
            @php $selected = old('employee_id', ($editing ? $employeeEducation->employee_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
            @foreach($employees as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="institution"
            label="Institution"
            value="{{ old('institution', ($editing ? $employeeEducation->institution : '')) }}"
            maxlength="255"
            placeholder="Institution"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.number
            name="year_begin"
            label="Year Begin"
            value="{{ old('year_begin', ($editing ? $employeeEducation->year_begin : '')) }}"
            max="255"
            placeholder="Year Begin"
        ></x-inputs.number>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.number
            name="year_end"
            label="Year End"
            value="{{ old('year_end', ($editing ? $employeeEducation->year_end : '')) }}"
            max="255"
            placeholder="Year End"
        ></x-inputs.number>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.date
            name="date_begin"
            label="Date Begin"
            value="{{ old('date_begin', ($editing ? optional($employeeEducation->date_begin)->format('Y-m-d') : '')) }}"
            max="255"
        ></x-inputs.date>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.date
            name="date_end"
            label="Date End"
            value="{{ old('date_end', ($editing ? optional($employeeEducation->date_end)->format('Y-m-d') : '')) }}"
            max="255"
        ></x-inputs.date>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="speciality"
            label="Speciality"
            value="{{ old('speciality', ($editing ? $employeeEducation->speciality : '')) }}"
            maxlength="255"
            placeholder="Speciality"
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="qualification"
            label="Qualification"
            value="{{ old('qualification', ($editing ? $employeeEducation->qualification : '')) }}"
            maxlength="255"
            placeholder="Qualification"
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="diplom_num"
            label="Diplom Num"
            value="{{ old('diplom_num', ($editing ? $employeeEducation->diplom_num : '')) }}"
            maxlength="255"
            placeholder="Diplom Num"
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.date
            name="diplom_date"
            label="Diplom Date"
            value="{{ old('diplom_date', ($editing ? optional($employeeEducation->diplom_date)->format('Y-m-d') : '')) }}"
            max="255"
        ></x-inputs.date>
    </x-inputs.group>
</div>
