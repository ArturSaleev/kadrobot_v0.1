@extends('layouts.app')

@section('title')

@endsection

@section('content')
<div class="container">
    <div class="searchbar mt-0 mb-4">
        <div class="row">
            <div class="col-md-6">
                <form>
                    <div class="input-group">
                        <input
                            id="indexSearch"
                            type="text"
                            name="search"
                            placeholder="{{ __('crud.common.search') }}"
                            value="{{ $search ?? '' }}"
                            class="form-control"
                            autocomplete="off"
                        />
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-primary">
                                <i class="icon ion-md-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6 text-right">
                @can('create', App\Models\EmployeeEducation::class)
                <a
                    href="{{ route('employee-educations.create') }}"
                    class="btn btn-primary"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div style="display: flex; justify-content: space-between;">
                <h4 class="card-title">
                    @lang('crud.employee_educations.index_title')
                </h4>
            </div>

            <div class="table-responsive">
                <table class="table table-borderless table-hover">
                    <thead>
                        <tr>
                            <th class="text-left">
                                @lang('crud.employee_educations.inputs.employee_id')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_educations.inputs.institution')
                            </th>
                            <th class="text-right">
                                @lang('crud.employee_educations.inputs.year_begin')
                            </th>
                            <th class="text-right">
                                @lang('crud.employee_educations.inputs.year_end')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_educations.inputs.date_begin')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_educations.inputs.date_end')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_educations.inputs.speciality')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_educations.inputs.qualification')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_educations.inputs.diplom_num')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_educations.inputs.diplom_date')
                            </th>
                            <th class="text-center">
                                @lang('crud.common.actions')
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($employeeEducations as $employeeEducation)
                        <tr>
                            <td>
                                {{ optional($employeeEducation->employee)->iin
                                ?? '-' }}
                            </td>
                            <td>
                                {{ $employeeEducation->institution ?? '-' }}
                            </td>
                            <td>{{ $employeeEducation->year_begin ?? '-' }}</td>
                            <td>{{ $employeeEducation->year_end ?? '-' }}</td>
                            <td>{{ $employeeEducation->date_begin ?? '-' }}</td>
                            <td>{{ $employeeEducation->date_end ?? '-' }}</td>
                            <td>{{ $employeeEducation->speciality ?? '-' }}</td>
                            <td>
                                {{ $employeeEducation->qualification ?? '-' }}
                            </td>
                            <td>{{ $employeeEducation->diplom_num ?? '-' }}</td>
                            <td>
                                {{ $employeeEducation->diplom_date ?? '-' }}
                            </td>
                            <td class="text-center" style="width: 134px;">
                                <div
                                    role="group"
                                    aria-label="Row Actions"
                                    class="btn-group"
                                >
                                    @can('update', $employeeEducation)
                                    <a
                                        href="{{ route('employee-educations.edit', $employeeEducation) }}"
                                    >
                                        <button
                                            type="button"
                                            class="btn btn-light"
                                        >
                                            <i class="icon ion-md-create"></i>
                                        </button>
                                    </a>
                                    @endcan @can('view', $employeeEducation)
                                    <a
                                        href="{{ route('employee-educations.show', $employeeEducation) }}"
                                    >
                                        <button
                                            type="button"
                                            class="btn btn-light"
                                        >
                                            <i class="icon ion-md-eye"></i>
                                        </button>
                                    </a>
                                    @endcan @can('delete', $employeeEducation)
                                    <form
                                        action="{{ route('employee-educations.destroy', $employeeEducation) }}"
                                        method="POST"
                                        onsubmit="return confirm('{{ __('crud.common.are_you_sure') }}')"
                                    >
                                        @csrf @method('DELETE')
                                        <button
                                            type="submit"
                                            class="btn btn-light text-danger"
                                        >
                                            <i class="icon ion-md-trash"></i>
                                        </button>
                                    </form>
                                    @endcan
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="11">
                                @lang('crud.common.no_items_found')
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="11">
                                {!! $employeeEducations->render() !!}
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
