@extends('layouts.app')

@section('title')
    @lang('crud.companies.edit_title')
@endsection

@section('content')
    <div class="container">

        <x-form
            method="PUT"
            action="{{ route('companies.update', $company) }}"
        >
            @include('app.companies.form-inputs')

            <div class="mt-4">
                <button type="submit" class="btn btn-primary float-right">
                    <i class="icon ion-md-save"></i>
                    @lang('crud.common.save')
                </button>
            </div>
        </x-form>

    </div>
@endsection
