@php
    $editing = isset($company);
@endphp

<div class="row">
    <div class="col-lg-6 col-12">
        <div class="card">
            <div class="card-body">
                <div>
                    <label class="card-title">@lang('crud.names')</label>
                    <br/>
                </div>
                <div>
                    @foreach(config('panel.available_languages', []) as $lang=>$title)
                        <div class="mt-2">
                            <span class="text-dark">{{ $title }}</span>
                            <x-inputs.text
                                name="name[{{ $lang }}]"
                                value="{{ $editing ? $company->translate($lang)->name : '' }}"
                                maxlength="255"
                                placeholder="{{ $title }}"
                                required
                            ></x-inputs.text>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6 col-12 col-sm-12">
        <div class="card">
            <div class="card-body">
                <x-inputs.text
                    name="bin"
                    label="{{ __('crud.companies.inputs.bin') }}"
                    value="{{ old('bin', ($editing ? $company->bin : '')) }}"
                    maxlength="255"
                    placeholder="{{ __('crud.companies.inputs.bin') }}"
                    required
                ></x-inputs.text>

                <x-inputs.text
                    name="rnn"
                    label="{{ __('crud.companies.inputs.rnn') }}"
                    value="{{ old('rnn', ($editing ? $company->rnn : '')) }}"
                    maxlength="255"
                    placeholder="{{ __('crud.companies.inputs.rnn') }}"
                    required
                ></x-inputs.text>

                <x-inputs.select
                    name="oked"
                    label="{{ __('crud.companies.inputs.oked') }}"
                    class="select2"
                >
                    @php $selected = old('oked', ($editing ? $company->oked : '')) @endphp
                    <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
                    @foreach($refOkeds as $refOked)
                        <option
                            value="{{ $refOked->id }}" {{ $selected == $refOked->id ? 'selected' : '' }} >{{ $refOked->oked }} {{ $refOked->name_oked }}</option>
                    @endforeach
                </x-inputs.select>

            </div>
        </div>
    </div>
</div>
