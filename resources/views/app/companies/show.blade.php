@extends('layouts.app')

@section('title')
    @lang('crud.companies.show_title')
@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <div class="row mb-4">
                    <div class="col-lg-6 col-12 col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <label class="card-title">@lang('crud.names')</label>
                                <br />
                                @foreach(config('panel.available_languages', []) as $lang=>$title)
                                    <div class="mt-2">
                                        <span class="text-dark">{{ $title }}</span>
                                        <span class="form-control">{{ $company->translate($lang)->name ?? '-' }}</span>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                    </div>


                    <div class="col-lg-6 col-12 col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <label class="card-title">@lang('crud.names')</label>
                                <br />

                                <div class="mt-2">
                                    <span class="text-dark">@lang('crud.companies.inputs.bin')</span>
                                    <span class="form-control">{{ $company->bin ?? '-' }}</span>
                                </div>
                                <div class="mt-2">
                                    <span class="text-dark">@lang('crud.companies.inputs.rnn')</span>
                                    <span class="form-control">{{ $company->rnn ?? '-' }}</span>
                                </div>
                                <div class="mt-2">
                                    <span class="text-dark">@lang('crud.companies.inputs.oked')</span>
                                    <br />
                                    <span>
                            {!! ($oked) ? $oked->oked." ".$oked->name_oked."<br />".$oked->name : '-' !!}
                        </span>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

            <div class="mt-4">
                @can('create', App\Models\Company::class)
                <a href="{{ route('companies.edit', 1) }}" class="btn btn-light">
                    <i class="icon ion-md-create"></i> @lang('crud.common.edit')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
