@extends('layouts.app')

@section('title')

@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('employee-hospitals.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.employee_hospitals.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.employee_hospitals.inputs.date_begin')</h5>
                    <span>{{ $employeeHospital->date_begin ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_hospitals.inputs.date_end')</h5>
                    <span>{{ $employeeHospital->date_end ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_hospitals.inputs.cnt_days')</h5>
                    <span>{{ $employeeHospital->cnt_days ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_hospitals.inputs.employee_id')</h5>
                    <span
                        >{{ optional($employeeHospital->employee)->iin ?? '-'
                        }}</span
                    >
                </div>
            </div>

            <div class="mt-4">
                <a
                    href="{{ route('employee-hospitals.index') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\EmployeeHospital::class)
                <a
                    href="{{ route('employee-hospitals.create') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
