
<script src="{{ asset('ckeditor/translations/ru.js') }}"></script>
<script src="{{ asset('ckeditor/js/ckeditor.js') }}"></script>
{{--<script src="{{ asset('ckeditor/js/ckbox.js') }}"></script>--}}
<script>
    let editors = [];
    let editPanel = [];
    @foreach(config('panel.available_languages', []) as $lang=>$title)
    DecoupledEditor
        .create(document.querySelector('#editor_{{ $lang }}'), {
            // toolbar: [ 'heading', '|', 'bold', 'italic', 'link' ]
        })
        .then(editor => {
            const toolbarContainer = document.querySelector('.toolbar-container-{{ $lang }}');

            toolbarContainer.prepend(editor.ui.view.toolbar.element);

            editPanel.push(editor);
            // window.editor = editor;
        })
        .catch(err => {
            console.error(err.stack);
        });
    editors.push("{{ $lang }}");
    @endforeach

    $('#save_form_template').on('submit', function(event){
        event.preventDefault();

        $.each(editors, function(i, lng){
            let html = $("#editor_"+lng).html();
            console.log(lng, html);
        })
    })

    $('#search_marker').bind("change paste keyup", function(){
        let text = $(this).val();
        $('.marker').each(function(i, item){
            let html = item.innerHTML.toUpperCase();
            if(html.indexOf(text.toUpperCase()) < 0){
                item.style.display = 'none';
            }else{
                item.style.display ='block';
            }
        })
    })

    $('.marker').click(function(){
        let data = $(this).attr('data-call');
        let text = $(this).html();
        let lang = $('.nav-link.active').attr('id').replaceAll('custom-tabs-', '').replaceAll('-tab', '');
        let index = editors.indexOf(lang);

        let content = '<a href="#'+data+'">'+text+'</a>';

        // content = '<span style="color:hsl(210, 75%, 60%);">Данные сотрудника - Дата принятия</span>';

        const viewFragment = editPanel[index].data.processor.toView( content );
        const modelFragment = editPanel[index].data.toModel( viewFragment );
        editPanel[index].model.insertContent( modelFragment );
    })
</script>
