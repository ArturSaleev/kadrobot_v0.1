@php
    $editing = isset($template_doc);

    $emp = new \App\Models\Employee();
    $markers = $emp->getAllAttributes();
@endphp


<ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
    @foreach(config('panel.available_languages', []) as $lang=>$title)
        <li class="nav-item">
            <a class="nav-link {{ ($lang == app()->getLocale()) ? 'active' : '' }}"
               id="custom-tabs-{{ $lang }}-tab"
               data-toggle="pill"
               href="#custom-tabs-{{ $lang }}"
               role="tab"
               aria-controls="custom-tabs-{{ $lang }}"
               aria-selected="true"
            >{{ $title }}</a>
        </li>
    @endforeach
</ul>

<div class="row mt-2">
    <div class="col-lg-9">

        <div class="tab-content" id="custom-tabs-one-tabContent">
            @foreach(config('panel.available_languages', []) as $lang=>$title)
                <div class="tab-pane fade {{ ($lang == app()->getLocale()) ? 'active show' : '' }}"
                     id="custom-tabs-{{ $lang }}" role="tabpanel"
                     aria-labelledby="custom-tabs-{{ $lang }}-tab"
                >
                    <div class="row">
                        <div class="col-lg-12 col-12 col-sm-12">
                            <div class="form-group">
                                <label>{{ __('crud.report_name') }}</label>
                                <input type="text"
                                       class="form-control"
                                       name="name[{{ $lang }}]"
                                       placeholder="{{ __('crud.report_name') }}"
                                       value="{{ old('name', ($editing ? $template_doc->translation($lang)->name : '')) }}"
                                       required
                                >
                            </div>
                        </div>
                        <main class="col-lg-12 col-12 col-sm-12">
                            <div class="toolbar-container-{{ $lang }}"></div>
                            <div class="content-container">
                                <div class="form-control editor editor_{{ $lang }}" id="editor_{{ $lang }}"></div>
                                <textarea name="body[{{ $lang }}]" id="body_{{ $lang }}"
                                          style="display: none"></textarea>
                            </div>
                        </main>
                    </div>


                </div>
            @endforeach
        </div>

    </div>
    <div class="col-lg-3">
        <label>{{ __('crud.report_markers') }}</label>
        <input type="text" class="form-control" id="search_marker" value=""
               placeholder="{{ __('crud.common.search') }}">

        <div style="height: 80vh; overflow-y: auto; margin-top: 15px">
            @foreach($markers as $marker)
                <span class="btn btn-default btn-sm btn-block marker"
                      data-call="{{ $marker['call'] }}"
                >{{ $marker['title'] }}</span>
            @endforeach
        </div>
    </div>
</div>





