@php $editing = isset($refVidHoliday) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.select
            name="ref_vid_holiday_type_id"
            label="Ref Vid Holiday Type"
            required
        >
            @php $selected = old('ref_vid_holiday_type_id', ($editing ? $refVidHoliday->ref_vid_holiday_type_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
            @foreach($refVidHolidayTypes as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>
</div>
