@extends('layouts.app')

@section('title')

@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('employee-holidays.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.employee_holidays.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.employee_holidays.inputs.employee_id')</h5>
                    <span
                        >{{ optional($employeeHoliday->employee)->iin ?? '-'
                        }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_holidays.inputs.date_begin')</h5>
                    <span>{{ $employeeHoliday->date_begin ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_holidays.inputs.date_end')</h5>
                    <span>{{ $employeeHoliday->date_end ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_holidays.inputs.cnt_days')</h5>
                    <span>{{ $employeeHoliday->cnt_days ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_holidays.inputs.period_begin')</h5>
                    <span>{{ $employeeHoliday->period_begin ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_holidays.inputs.period_end')</h5>
                    <span>{{ $employeeHoliday->period_end ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_holidays.inputs.order_num')</h5>
                    <span>{{ $employeeHoliday->order_num ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_holidays.inputs.order_date')</h5>
                    <span>{{ $employeeHoliday->order_date ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_holidays.inputs.ref_vid_holiday_id')
                    </h5>
                    <span
                        >{{ optional($employeeHoliday->refVidHoliday)->id ?? '-'
                        }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_holidays.inputs.deligate')</h5>
                    <span>{{ $employeeHoliday->deligate ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_holidays.inputs.deligate_emp_id')
                    </h5>
                    <span>{{ $employeeHoliday->deligate_emp_id ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_holidays.inputs.doc_content')</h5>
                    <span>{{ $employeeHoliday->doc_content ?? '-' }}</span>
                </div>
            </div>

            <div class="mt-4">
                <a
                    href="{{ route('employee-holidays.index') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\EmployeeHoliday::class)
                <a
                    href="{{ route('employee-holidays.create') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
