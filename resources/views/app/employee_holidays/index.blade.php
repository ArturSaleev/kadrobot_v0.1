@extends('layouts.app')

@section('title')

@endsection

@section('content')
<div class="container">
    <div class="searchbar mt-0 mb-4">
        <div class="row">
            <div class="col-md-6">
                <form>
                    <div class="input-group">
                        <input
                            id="indexSearch"
                            type="text"
                            name="search"
                            placeholder="{{ __('crud.common.search') }}"
                            value="{{ $search ?? '' }}"
                            class="form-control"
                            autocomplete="off"
                        />
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-primary">
                                <i class="icon ion-md-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6 text-right">
                @can('create', App\Models\EmployeeHoliday::class)
                <a
                    href="{{ route('employee-holidays.create') }}"
                    class="btn btn-primary"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div style="display: flex; justify-content: space-between;">
                <h4 class="card-title">
                    @lang('crud.employee_holidays.index_title')
                </h4>
            </div>

            <div class="table-responsive">
                <table class="table table-borderless table-hover">
                    <thead>
                        <tr>
                            <th class="text-left">
                                @lang('crud.employee_holidays.inputs.employee_id')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_holidays.inputs.date_begin')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_holidays.inputs.date_end')
                            </th>
                            <th class="text-right">
                                @lang('crud.employee_holidays.inputs.cnt_days')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_holidays.inputs.period_begin')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_holidays.inputs.period_end')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_holidays.inputs.order_num')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_holidays.inputs.order_date')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_holidays.inputs.ref_vid_holiday_id')
                            </th>
                            <th class="text-right">
                                @lang('crud.employee_holidays.inputs.deligate')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_holidays.inputs.deligate_emp_id')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_holidays.inputs.doc_content')
                            </th>
                            <th class="text-center">
                                @lang('crud.common.actions')
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($employeeHolidays as $employeeHoliday)
                        <tr>
                            <td>
                                {{ optional($employeeHoliday->employee)->iin ??
                                '-' }}
                            </td>
                            <td>{{ $employeeHoliday->date_begin ?? '-' }}</td>
                            <td>{{ $employeeHoliday->date_end ?? '-' }}</td>
                            <td>{{ $employeeHoliday->cnt_days ?? '-' }}</td>
                            <td>{{ $employeeHoliday->period_begin ?? '-' }}</td>
                            <td>{{ $employeeHoliday->period_end ?? '-' }}</td>
                            <td>{{ $employeeHoliday->order_num ?? '-' }}</td>
                            <td>{{ $employeeHoliday->order_date ?? '-' }}</td>
                            <td>
                                {{ optional($employeeHoliday->refVidHoliday)->id
                                ?? '-' }}
                            </td>
                            <td>{{ $employeeHoliday->deligate ?? '-' }}</td>
                            <td>
                                {{ $employeeHoliday->deligate_emp_id ?? '-' }}
                            </td>
                            <td>{{ $employeeHoliday->doc_content ?? '-' }}</td>
                            <td class="text-center" style="width: 134px;">
                                <div
                                    role="group"
                                    aria-label="Row Actions"
                                    class="btn-group"
                                >
                                    @can('update', $employeeHoliday)
                                    <a
                                        href="{{ route('employee-holidays.edit', $employeeHoliday) }}"
                                    >
                                        <button
                                            type="button"
                                            class="btn btn-light"
                                        >
                                            <i class="icon ion-md-create"></i>
                                        </button>
                                    </a>
                                    @endcan @can('view', $employeeHoliday)
                                    <a
                                        href="{{ route('employee-holidays.show', $employeeHoliday) }}"
                                    >
                                        <button
                                            type="button"
                                            class="btn btn-light"
                                        >
                                            <i class="icon ion-md-eye"></i>
                                        </button>
                                    </a>
                                    @endcan @can('delete', $employeeHoliday)
                                    <form
                                        action="{{ route('employee-holidays.destroy', $employeeHoliday) }}"
                                        method="POST"
                                        onsubmit="return confirm('{{ __('crud.common.are_you_sure') }}')"
                                    >
                                        @csrf @method('DELETE')
                                        <button
                                            type="submit"
                                            class="btn btn-light text-danger"
                                        >
                                            <i class="icon ion-md-trash"></i>
                                        </button>
                                    </form>
                                    @endcan
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="13">
                                @lang('crud.common.no_items_found')
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="13">
                                {!! $employeeHolidays->render() !!}
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
