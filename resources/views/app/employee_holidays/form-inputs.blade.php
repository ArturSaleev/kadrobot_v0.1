@php $editing = isset($employeeHoliday) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="employee_id" label="Employee" required>
            @php $selected = old('employee_id', ($editing ? $employeeHoliday->employee_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
            @foreach($employees as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.date
            name="date_begin"
            label="Date Begin"
            value="{{ old('date_begin', ($editing ? optional($employeeHoliday->date_begin)->format('Y-m-d') : '')) }}"
            max="255"
        ></x-inputs.date>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.date
            name="date_end"
            label="Date End"
            value="{{ old('date_end', ($editing ? optional($employeeHoliday->date_end)->format('Y-m-d') : '')) }}"
            max="255"
        ></x-inputs.date>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.number
            name="cnt_days"
            label="Cnt Days"
            value="{{ old('cnt_days', ($editing ? $employeeHoliday->cnt_days : '')) }}"
            max="255"
            placeholder="Cnt Days"
        ></x-inputs.number>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.date
            name="period_begin"
            label="Period Begin"
            value="{{ old('period_begin', ($editing ? optional($employeeHoliday->period_begin)->format('Y-m-d') : '')) }}"
            max="255"
        ></x-inputs.date>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.date
            name="period_end"
            label="Period End"
            value="{{ old('period_end', ($editing ? optional($employeeHoliday->period_end)->format('Y-m-d') : '')) }}"
            max="255"
        ></x-inputs.date>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="order_num"
            label="Order Num"
            value="{{ old('order_num', ($editing ? $employeeHoliday->order_num : '')) }}"
            maxlength="255"
            placeholder="Order Num"
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.date
            name="order_date"
            label="Order Date"
            value="{{ old('order_date', ($editing ? optional($employeeHoliday->order_date)->format('Y-m-d') : '')) }}"
            max="255"
        ></x-inputs.date>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.select
            name="ref_vid_holiday_id"
            label="Ref Vid Holiday"
            required
        >
            @php $selected = old('ref_vid_holiday_id', ($editing ? $employeeHoliday->ref_vid_holiday_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
            @foreach($refVidHolidays as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.number
            name="deligate"
            label="Deligate"
            value="{{ old('deligate', ($editing ? $employeeHoliday->deligate : '')) }}"
            max="255"
            placeholder="Deligate"
        ></x-inputs.number>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="deligate_emp_id"
            label="Deligate Emp Id"
            value="{{ old('deligate_emp_id', ($editing ? $employeeHoliday->deligate_emp_id : '')) }}"
            maxlength="255"
            placeholder="Deligate Emp Id"
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.textarea
            name="doc_content"
            label="Doc Content"
            maxlength="255"
            required
            >{{ old('doc_content', ($editing ? $employeeHoliday->doc_content :
            '')) }}</x-inputs.textarea
        >
    </x-inputs.group>
</div>
