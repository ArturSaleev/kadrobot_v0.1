@php $editing = isset($branchAddreses) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="branch_id" label="Branch" required>
            @php $selected = old('branch_id', ($editing ? $branchAddreses->branch_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
            @foreach($branches as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.select
            name="ref_address_type_id"
            label="Ref Address Type"
            required
        >
            @php $selected = old('ref_address_type_id', ($editing ? $branchAddreses->ref_address_type_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
            @foreach($refAddressTypes as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>
</div>
