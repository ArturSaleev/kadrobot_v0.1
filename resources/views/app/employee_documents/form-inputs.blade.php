@php $editing = isset($employeeDocument) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="employee_id" label="Employee" required>
            @php $selected = old('employee_id', ($editing ? $employeeDocument->employee_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
            @foreach($employees as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="ref_doc_type_id" label="Ref Doc Type" required>
            @php $selected = old('ref_doc_type_id', ($editing ? $employeeDocument->ref_doc_type_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
            @foreach($refDocTypes as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="ref_doc_place_id" label="Ref Doc Place" required>
            @php $selected = old('ref_doc_place_id', ($editing ? $employeeDocument->ref_doc_place_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
            @foreach($refDocPlaces as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="doc_seria"
            label="Doc Seria"
            value="{{ old('doc_seria', ($editing ? $employeeDocument->doc_seria : '')) }}"
            maxlength="255"
            placeholder="Doc Seria"
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="doc_num"
            label="Doc Num"
            value="{{ old('doc_num', ($editing ? $employeeDocument->doc_num : '')) }}"
            maxlength="255"
            placeholder="Doc Num"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.date
            name="doc_date"
            label="Doc Date"
            value="{{ old('doc_date', ($editing ? optional($employeeDocument->doc_date)->format('Y-m-d') : '')) }}"
            max="255"
            required
        ></x-inputs.date>
    </x-inputs.group>
</div>
