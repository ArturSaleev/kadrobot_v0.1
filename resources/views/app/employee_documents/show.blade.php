@extends('layouts.app')

@section('title')

@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('employee-documents.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.employee_documents.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.employee_documents.inputs.employee_id')</h5>
                    <span
                        >{{ optional($employeeDocument->employee)->iin ?? '-'
                        }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_documents.inputs.ref_doc_type_id')
                    </h5>
                    <span
                        >{{ optional($employeeDocument->refDocType)->id ?? '-'
                        }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_documents.inputs.ref_doc_place_id')
                    </h5>
                    <span
                        >{{ optional($employeeDocument->refDocPlace)->id ?? '-'
                        }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_documents.inputs.doc_seria')</h5>
                    <span>{{ $employeeDocument->doc_seria ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_documents.inputs.doc_num')</h5>
                    <span>{{ $employeeDocument->doc_num ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_documents.inputs.doc_date')</h5>
                    <span>{{ $employeeDocument->doc_date ?? '-' }}</span>
                </div>
            </div>

            <div class="mt-4">
                <a
                    href="{{ route('employee-documents.index') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\EmployeeDocument::class)
                <a
                    href="{{ route('employee-documents.create') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
