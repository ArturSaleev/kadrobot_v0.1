@extends('layouts.app')

@section('title')

@endsection

@section('content')
<div class="container">
    <div class="searchbar mt-0 mb-4">
        <div class="row">
            <div class="col-md-6">
                <form>
                    <div class="input-group">
                        <input
                            id="indexSearch"
                            type="text"
                            name="search"
                            placeholder="{{ __('crud.common.search') }}"
                            value="{{ $search ?? '' }}"
                            class="form-control"
                            autocomplete="off"
                        />
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-primary">
                                <i class="icon ion-md-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6 text-right">
                @can('create', App\Models\EmployeeDocument::class)
                <a
                    href="{{ route('employee-documents.create') }}"
                    class="btn btn-primary"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div style="display: flex; justify-content: space-between;">
                <h4 class="card-title">
                    @lang('crud.employee_documents.index_title')
                </h4>
            </div>

            <div class="table-responsive">
                <table class="table table-borderless table-hover">
                    <thead>
                        <tr>
                            <th class="text-left">
                                @lang('crud.employee_documents.inputs.employee_id')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_documents.inputs.ref_doc_type_id')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_documents.inputs.ref_doc_place_id')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_documents.inputs.doc_seria')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_documents.inputs.doc_num')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_documents.inputs.doc_date')
                            </th>
                            <th class="text-center">
                                @lang('crud.common.actions')
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($employeeDocuments as $employeeDocument)
                        <tr>
                            <td>
                                {{ optional($employeeDocument->employee)->iin ??
                                '-' }}
                            </td>
                            <td>
                                {{ optional($employeeDocument->refDocType)->id
                                ?? '-' }}
                            </td>
                            <td>
                                {{ optional($employeeDocument->refDocPlace)->id
                                ?? '-' }}
                            </td>
                            <td>{{ $employeeDocument->doc_seria ?? '-' }}</td>
                            <td>{{ $employeeDocument->doc_num ?? '-' }}</td>
                            <td>{{ $employeeDocument->doc_date ?? '-' }}</td>
                            <td class="text-center" style="width: 134px;">
                                <div
                                    role="group"
                                    aria-label="Row Actions"
                                    class="btn-group"
                                >
                                    @can('update', $employeeDocument)
                                    <a
                                        href="{{ route('employee-documents.edit', $employeeDocument) }}"
                                    >
                                        <button
                                            type="button"
                                            class="btn btn-light"
                                        >
                                            <i class="icon ion-md-create"></i>
                                        </button>
                                    </a>
                                    @endcan @can('view', $employeeDocument)
                                    <a
                                        href="{{ route('employee-documents.show', $employeeDocument) }}"
                                    >
                                        <button
                                            type="button"
                                            class="btn btn-light"
                                        >
                                            <i class="icon ion-md-eye"></i>
                                        </button>
                                    </a>
                                    @endcan @can('delete', $employeeDocument)
                                    <form
                                        action="{{ route('employee-documents.destroy', $employeeDocument) }}"
                                        method="POST"
                                        onsubmit="return confirm('{{ __('crud.common.are_you_sure') }}')"
                                    >
                                        @csrf @method('DELETE')
                                        <button
                                            type="submit"
                                            class="btn btn-light text-danger"
                                        >
                                            <i class="icon ion-md-trash"></i>
                                        </button>
                                    </form>
                                    @endcan
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="7">
                                @lang('crud.common.no_items_found')
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="7">
                                {!! $employeeDocuments->render() !!}
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
