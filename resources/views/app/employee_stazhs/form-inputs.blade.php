@php $editing = isset($employeeStazh) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="employee_id" label="Employee" required>
            @php $selected = old('employee_id', ($editing ? $employeeStazh->employee_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
            @foreach($employees as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.date
            name="date_begin"
            label="Date Begin"
            value="{{ old('date_begin', ($editing ? optional($employeeStazh->date_begin)->format('Y-m-d') : '')) }}"
            max="255"
            required
        ></x-inputs.date>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.date
            name="date_end"
            label="Date End"
            value="{{ old('date_end', ($editing ? optional($employeeStazh->date_end)->format('Y-m-d') : '')) }}"
            max="255"
            required
        ></x-inputs.date>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.number
            name="cnt_mes"
            label="Cnt Mes"
            value="{{ old('cnt_mes', ($editing ? $employeeStazh->cnt_mes : '')) }}"
            max="255"
            placeholder="Cnt Mes"
            required
        ></x-inputs.number>
    </x-inputs.group>
</div>
