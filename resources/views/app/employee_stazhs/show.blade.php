@extends('layouts.app')

@section('title')

@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('employee-stazhs.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.employee_stazhs.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.employee_stazhs.inputs.employee_id')</h5>
                    <span
                        >{{ optional($employeeStazh->employee)->iin ?? '-'
                        }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_stazhs.inputs.date_begin')</h5>
                    <span>{{ $employeeStazh->date_begin ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_stazhs.inputs.date_end')</h5>
                    <span>{{ $employeeStazh->date_end ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_stazhs.inputs.cnt_mes')</h5>
                    <span>{{ $employeeStazh->cnt_mes ?? '-' }}</span>
                </div>
            </div>

            <div class="mt-4">
                <a
                    href="{{ route('employee-stazhs.index') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\EmployeeStazh::class)
                <a
                    href="{{ route('employee-stazhs.create') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
