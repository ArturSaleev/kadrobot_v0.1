@php $editing = isset($employeePayAllowance) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="employee_id" label="Employee" required>
            @php $selected = old('employee_id', ($editing ? $employeePayAllowance->employee_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
            @foreach($employees as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.select
            name="ref_allowance_type_id"
            label="Ref Allowance Type"
            required
        >
            @php $selected = old('ref_allowance_type_id', ($editing ? $employeePayAllowance->ref_allowance_type_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
            @foreach($refAllowanceTypes as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.date
            name="date_add"
            label="Date Add"
            value="{{ old('date_add', ($editing ? optional($employeePayAllowance->date_add)->format('Y-m-d') : '')) }}"
            max="255"
            required
        ></x-inputs.date>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.date
            name="period_begin"
            label="Period Begin"
            value="{{ old('period_begin', ($editing ? optional($employeePayAllowance->period_begin)->format('Y-m-d') : '')) }}"
            max="255"
            required
        ></x-inputs.date>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.date
            name="period_end"
            label="Period End"
            value="{{ old('period_end', ($editing ? optional($employeePayAllowance->period_end)->format('Y-m-d') : '')) }}"
            max="255"
            required
        ></x-inputs.date>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.number
            name="paysum"
            label="Paysum"
            value="{{ old('paysum', ($editing ? $employeePayAllowance->paysum : '')) }}"
            max="255"
            step="0.01"
            placeholder="Paysum"
            required
        ></x-inputs.number>
    </x-inputs.group>
</div>
