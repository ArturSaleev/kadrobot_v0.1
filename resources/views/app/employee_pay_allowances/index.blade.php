@extends('layouts.app')

@section('title')

@endsection

@section('content')
<div class="container">
    <div class="searchbar mt-0 mb-4">
        <div class="row">
            <div class="col-md-6">
                <form>
                    <div class="input-group">
                        <input
                            id="indexSearch"
                            type="text"
                            name="search"
                            placeholder="{{ __('crud.common.search') }}"
                            value="{{ $search ?? '' }}"
                            class="form-control"
                            autocomplete="off"
                        />
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-primary">
                                <i class="icon ion-md-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6 text-right">
                @can('create', App\Models\EmployeePayAllowance::class)
                <a
                    href="{{ route('employee-pay-allowances.create') }}"
                    class="btn btn-primary"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div style="display: flex; justify-content: space-between;">
                <h4 class="card-title">
                    @lang('crud.employee_pay_allowances.index_title')
                </h4>
            </div>

            <div class="table-responsive">
                <table class="table table-borderless table-hover">
                    <thead>
                        <tr>
                            <th class="text-left">
                                @lang('crud.employee_pay_allowances.inputs.employee_id')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_pay_allowances.inputs.ref_allowance_type_id')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_pay_allowances.inputs.date_add')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_pay_allowances.inputs.period_begin')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_pay_allowances.inputs.period_end')
                            </th>
                            <th class="text-right">
                                @lang('crud.employee_pay_allowances.inputs.paysum')
                            </th>
                            <th class="text-center">
                                @lang('crud.common.actions')
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($employeePayAllowances as
                        $employeePayAllowance)
                        <tr>
                            <td>
                                {{
                                optional($employeePayAllowance->employee)->iin
                                ?? '-' }}
                            </td>
                            <td>
                                {{
                                optional($employeePayAllowance->refAllowanceType)->id
                                ?? '-' }}
                            </td>
                            <td>
                                {{ $employeePayAllowance->date_add ?? '-' }}
                            </td>
                            <td>
                                {{ $employeePayAllowance->period_begin ?? '-' }}
                            </td>
                            <td>
                                {{ $employeePayAllowance->period_end ?? '-' }}
                            </td>
                            <td>{{ $employeePayAllowance->paysum ?? '-' }}</td>
                            <td class="text-center" style="width: 134px;">
                                <div
                                    role="group"
                                    aria-label="Row Actions"
                                    class="btn-group"
                                >
                                    @can('update', $employeePayAllowance)
                                    <a
                                        href="{{ route('employee-pay-allowances.edit', $employeePayAllowance) }}"
                                    >
                                        <button
                                            type="button"
                                            class="btn btn-light"
                                        >
                                            <i class="icon ion-md-create"></i>
                                        </button>
                                    </a>
                                    @endcan @can('view', $employeePayAllowance)
                                    <a
                                        href="{{ route('employee-pay-allowances.show', $employeePayAllowance) }}"
                                    >
                                        <button
                                            type="button"
                                            class="btn btn-light"
                                        >
                                            <i class="icon ion-md-eye"></i>
                                        </button>
                                    </a>
                                    @endcan @can('delete',
                                    $employeePayAllowance)
                                    <form
                                        action="{{ route('employee-pay-allowances.destroy', $employeePayAllowance) }}"
                                        method="POST"
                                        onsubmit="return confirm('{{ __('crud.common.are_you_sure') }}')"
                                    >
                                        @csrf @method('DELETE')
                                        <button
                                            type="submit"
                                            class="btn btn-light text-danger"
                                        >
                                            <i class="icon ion-md-trash"></i>
                                        </button>
                                    </form>
                                    @endcan
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="7">
                                @lang('crud.common.no_items_found')
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="7">
                                {!! $employeePayAllowances->render() !!}
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
