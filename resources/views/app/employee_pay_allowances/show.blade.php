@extends('layouts.app')

@section('title')

@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a
                    href="{{ route('employee-pay-allowances.index') }}"
                    class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.employee_pay_allowances.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_pay_allowances.inputs.employee_id')
                    </h5>
                    <span
                        >{{ optional($employeePayAllowance->employee)->iin ??
                        '-' }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_pay_allowances.inputs.ref_allowance_type_id')
                    </h5>
                    <span
                        >{{
                        optional($employeePayAllowance->refAllowanceType)->id ??
                        '-' }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_pay_allowances.inputs.date_add')
                    </h5>
                    <span>{{ $employeePayAllowance->date_add ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_pay_allowances.inputs.period_begin')
                    </h5>
                    <span
                        >{{ $employeePayAllowance->period_begin ?? '-' }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_pay_allowances.inputs.period_end')
                    </h5>
                    <span>{{ $employeePayAllowance->period_end ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_pay_allowances.inputs.paysum')</h5>
                    <span>{{ $employeePayAllowance->paysum ?? '-' }}</span>
                </div>
            </div>

            <div class="mt-4">
                <a
                    href="{{ route('employee-pay-allowances.index') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\EmployeePayAllowance::class)
                <a
                    href="{{ route('employee-pay-allowances.create') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
