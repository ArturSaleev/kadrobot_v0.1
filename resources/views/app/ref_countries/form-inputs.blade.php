@php $editing = isset($refCountry) @endphp

<div class="row">
    @include('components.lang_name_array_edit', ['model' => ($editing) ? $refCountry : null, 'editing' => $editing])

    <x-inputs.group class="col-lg-6 col-12 col-sm-12">
        <x-inputs.text
            name="code"
            label="Code"
            value="{{ old('code', ($editing ? $refCountry->code : '')) }}"
            maxlength="255"
            placeholder="Code"
            required
        ></x-inputs.text>
    </x-inputs.group>
</div>
