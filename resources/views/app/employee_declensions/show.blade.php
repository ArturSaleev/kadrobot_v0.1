@extends('layouts.app')

@section('title')

@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('employee-declensions.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.employee_declensions.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_declensions.inputs.employee_id')
                    </h5>
                    <span
                        >{{ optional($employeeDeclension->employee)->iin ?? '-'
                        }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_declensions.inputs.locale')</h5>
                    <span>{{ $employeeDeclension->locale ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_declensions.inputs.lastname')</h5>
                    <span>{{ $employeeDeclension->lastname ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_declensions.inputs.firstname')</h5>
                    <span>{{ $employeeDeclension->firstname ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_declensions.inputs.middlename')
                    </h5>
                    <span>{{ $employeeDeclension->middlename ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_declensions.inputs.case_type')</h5>
                    <span>{{ $employeeDeclension->case_type ?? '-' }}</span>
                </div>
            </div>

            <div class="mt-4">
                <a
                    href="{{ route('employee-declensions.index') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\EmployeeDeclension::class)
                <a
                    href="{{ route('employee-declensions.create') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
