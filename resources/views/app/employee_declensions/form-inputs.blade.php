@php $editing = isset($employeeDeclension) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="employee_id" label="Employee" required>
            @php $selected = old('employee_id', ($editing ? $employeeDeclension->employee_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
            @foreach($employees as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="locale"
            label="Locale"
            value="{{ old('locale', ($editing ? $employeeDeclension->locale : '')) }}"
            maxlength="8"
            placeholder="Locale"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="lastname"
            label="Lastname"
            value="{{ old('lastname', ($editing ? $employeeDeclension->lastname : '')) }}"
            maxlength="255"
            placeholder="Lastname"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="firstname"
            label="Firstname"
            value="{{ old('firstname', ($editing ? $employeeDeclension->firstname : '')) }}"
            maxlength="255"
            placeholder="Firstname"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="middlename"
            label="Middlename"
            value="{{ old('middlename', ($editing ? $employeeDeclension->middlename : '')) }}"
            maxlength="255"
            placeholder="Middlename"
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="case_type"
            label="Case Type"
            value="{{ old('case_type', ($editing ? $employeeDeclension->case_type : '')) }}"
            maxlength="255"
            placeholder="Case Type"
        ></x-inputs.text>
    </x-inputs.group>
</div>
