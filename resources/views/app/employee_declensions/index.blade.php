@extends('layouts.app')

@section('title')

@endsection

@section('content')
<div class="container">
    <div class="searchbar mt-0 mb-4">
        <div class="row">
            <div class="col-md-6">
                <form>
                    <div class="input-group">
                        <input
                            id="indexSearch"
                            type="text"
                            name="search"
                            placeholder="{{ __('crud.common.search') }}"
                            value="{{ $search ?? '' }}"
                            class="form-control"
                            autocomplete="off"
                        />
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-primary">
                                <i class="icon ion-md-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6 text-right">
                @can('create', App\Models\EmployeeDeclension::class)
                <a
                    href="{{ route('employee-declensions.create') }}"
                    class="btn btn-primary"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div style="display: flex; justify-content: space-between;">
                <h4 class="card-title">
                    @lang('crud.employee_declensions.index_title')
                </h4>
            </div>

            <div class="table-responsive">
                <table class="table table-borderless table-hover">
                    <thead>
                        <tr>
                            <th class="text-left">
                                @lang('crud.employee_declensions.inputs.employee_id')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_declensions.inputs.locale')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_declensions.inputs.lastname')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_declensions.inputs.firstname')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_declensions.inputs.middlename')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_declensions.inputs.case_type')
                            </th>
                            <th class="text-center">
                                @lang('crud.common.actions')
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($employeeDeclensions as $employeeDeclension)
                        <tr>
                            <td>
                                {{ optional($employeeDeclension->employee)->iin
                                ?? '-' }}
                            </td>
                            <td>{{ $employeeDeclension->locale ?? '-' }}</td>
                            <td>{{ $employeeDeclension->lastname ?? '-' }}</td>
                            <td>{{ $employeeDeclension->firstname ?? '-' }}</td>
                            <td>
                                {{ $employeeDeclension->middlename ?? '-' }}
                            </td>
                            <td>{{ $employeeDeclension->case_type ?? '-' }}</td>
                            <td class="text-center" style="width: 134px;">
                                <div
                                    role="group"
                                    aria-label="Row Actions"
                                    class="btn-group"
                                >
                                    @can('update', $employeeDeclension)
                                    <a
                                        href="{{ route('employee-declensions.edit', $employeeDeclension) }}"
                                    >
                                        <button
                                            type="button"
                                            class="btn btn-light"
                                        >
                                            <i class="icon ion-md-create"></i>
                                        </button>
                                    </a>
                                    @endcan @can('view', $employeeDeclension)
                                    <a
                                        href="{{ route('employee-declensions.show', $employeeDeclension) }}"
                                    >
                                        <button
                                            type="button"
                                            class="btn btn-light"
                                        >
                                            <i class="icon ion-md-eye"></i>
                                        </button>
                                    </a>
                                    @endcan @can('delete', $employeeDeclension)
                                    <form
                                        action="{{ route('employee-declensions.destroy', $employeeDeclension) }}"
                                        method="POST"
                                        onsubmit="return confirm('{{ __('crud.common.are_you_sure') }}')"
                                    >
                                        @csrf @method('DELETE')
                                        <button
                                            type="submit"
                                            class="btn btn-light text-danger"
                                        >
                                            <i class="icon ion-md-trash"></i>
                                        </button>
                                    </form>
                                    @endcan
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="7">
                                @lang('crud.common.no_items_found')
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="7">
                                {!! $employeeDeclensions->render() !!}
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
