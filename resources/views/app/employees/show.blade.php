@extends('layouts.app')

@section('title')
    @lang('crud.employees.show_title')
@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.employees.inputs.date_post')</h5>
                    <span>{{ $employee->date_post ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employees.inputs.date_loyoff')</h5>
                    <span>{{ $employee->date_loyoff ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employees.inputs.tab_num')</h5>
                    <span>{{ $employee->tab_num ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employees.inputs.birthday')</h5>
                    <span>{{ $employee->birthday ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employees.inputs.birth_place')</h5>
                    <span>{{ $employee->birth_place ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employees.inputs.ref_sex_id')</h5>
                    <span>{{ optional($employee->refSex)->id ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employees.inputs.iin')</h5>
                    <span>{{ $employee->iin ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employees.inputs.ref_nationality_id')</h5>
                    <span
                        >{{ optional($employee->refNationality)->id ?? '-'
                        }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employees.inputs.ref_family_state_id')</h5>
                    <span
                        >{{ optional($employee->refFamilyState)->id ?? '-'
                        }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employees.inputs.contract_num')</h5>
                    <span>{{ $employee->contract_num ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employees.inputs.contract_date')</h5>
                    <span>{{ $employee->contract_date ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employees.inputs.ref_account_type_id')</h5>
                    <span
                        >{{ optional($employee->refAccountType)->id ?? '-'
                        }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employees.inputs.email')</h5>
                    <span>{{ $employee->email ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employees.inputs.account')</h5>
                    <span>{{ $employee->account ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employees.inputs.date_zav')</h5>
                    <span>{{ $employee->date_zav ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employees.inputs.oklad')</h5>
                    <span>{{ $employee->oklad ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employees.inputs.gos_nagr')</h5>
                    <span>{{ $employee->gos_nagr ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employees.inputs.pens')</h5>
                    <span>{{ $employee->pens ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employees.inputs.pens_date')</h5>
                    <span>{{ $employee->pens_date ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employees.inputs.lgot')</h5>
                    <span>{{ $employee->lgot ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employees.inputs.person_email')</h5>
                    <span>{{ $employee->person_email ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employees.inputs.ref_user_status_id')</h5>
                    <span
                        >{{ optional($employee->refUserStatus)->id ?? '-'
                        }}</span
                    >
                </div>
            </div>

            <div class="mt-4">
                <a href="{{ route('employees.index') }}" class="btn btn-light">
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\Employee::class)
                <a href="{{ route('employees.create') }}" class="btn btn-light">
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
