@php
    $editing = isset($employee);
@endphp


<div class="card card-primary card-tabs">
    <div class="card-header p-0 pt-1">
        <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active"
                   id="custom-tabs-one-home-tab"
                   data-toggle="pill"
                   href="#custom-tabs-one-home"
                   role="tab"
                   aria-controls="custom-tabs-one-home"
                   aria-selected="true"
                >Личные данные</a>
            </li>
            <li class="nav-item">
                <a class="nav-link"
                   id="custom-tabs-one-profile-tab"
                   data-toggle="pill"
                   href="#custom-tabs-one-profile"
                   role="tab"
                   aria-controls="custom-tabs-one-profile"
                   aria-selected="false"
                >Договор</a>
            </li>
            {{--            <li class="nav-item">--}}
            {{--                <a class="nav-link"--}}
            {{--                   id="custom-tabs-one-messages-tab"--}}
            {{--                   data-toggle="pill"--}}
            {{--                   href="#custom-tabs-one-messages"--}}
            {{--                   role="tab"--}}
            {{--                   aria-controls="custom-tabs-one-messages"--}}
            {{--                   aria-selected="false"--}}
            {{--                >Семейное положение</a>--}}
            {{--            </li>--}}
            {{--            <li class="nav-item">--}}
            {{--                <a class="nav-link"--}}
            {{--                   id="custom-tabs-one-settings-tab"--}}
            {{--                   data-toggle="pill"--}}
            {{--                   href="#custom-tabs-one-settings"--}}
            {{--                   role="tab"--}}
            {{--                   aria-controls="custom-tabs-one-settings"--}}
            {{--                   aria-selected="false"--}}
            {{--                >Стаж</a>--}}
            {{--            </li>--}}
        </ul>
    </div>
    <div class="card-body">
        <div class="tab-content" id="custom-tabs-one-tabContent">
            <div class="tab-pane fade active show" id="custom-tabs-one-home" role="tabpanel"
                 aria-labelledby="custom-tabs-one-home-tab">
                <div class="row">
                    <div class="col-lg-4 col-12 col-sm-12">
                        <label>{{ __('crud.employees.inputs.lastname') }}</label>
                        <input type="text"
                               class="form-control"
                               name="lastname"
                               placeholder="{{ __('crud.employees.inputs.lastname') }}"
                               value="{{ old('lastname', ($editing ? $employee->employeeFio(true)->lastname : '')) }}"
                               required
                        />
                    </div>
                    <div class="col-lg-4 col-12 col-sm-12">
                        <label>{{ __('crud.employees.inputs.firstname') }}</label>
                        <input type="text"
                               class="form-control"
                               name="firstname"
                               placeholder="{{ __('crud.employees.inputs.firstname') }}"
                               value="{{ old('firstname', ($editing ? $employee->employeeFio(true)->firstname : '')) }}"
                               required
                        />
                    </div>
                    <div class="col-lg-4 col-12 col-sm-12">
                        <label>{{ __('crud.employees.inputs.middlename') }}</label>
                        <input type="text"
                               class="form-control"
                               name="middlename"
                               placeholder="{{ __('crud.employees.inputs.middlename') }}"
                               value="{{ old('middlename', ($editing ? $employee->employeeFio(true)->middlename : '')) }}"
                        />
                    </div>
                </div>
                <div class="row">
                    <x-inputs.group class="col-lg-4 col-12 col-sm-12">
                        <x-inputs.text
                            name="iin"
                            label="{{ __('crud.employees.inputs.iin') }}"
                            value="{{ old('iin', ($editing ? $employee->iin : '')) }}"
                            maxlength="255"
                            placeholder="{{ __('crud.employees.inputs.iin') }}"
                            required
                        ></x-inputs.text>
                    </x-inputs.group>
                    <x-inputs.group class="col-lg-4 col-12 col-sm-12">
                        <x-inputs.select name="ref_sex_id"
                                         label="{{ __('crud.employees.inputs.ref_sex_id') }}"
                        >
                            @php $selected = old('ref_sex_id', ($editing ? $employee->ref_sex_id : '')) @endphp
                            <option
                                disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
                            @foreach($refSexes as $value)
                                <option
                                    value="{{ $value->id }}" {{ $selected == $value->id ? 'selected' : '' }} >{{ $value->name }}</option>
                            @endforeach
                        </x-inputs.select>
                    </x-inputs.group>
                    <x-inputs.group class="col-lg-4 col-12 col-sm-12">
                        <x-inputs.date
                            name="birthday"
                            label="{{ __('crud.employees.inputs.birthday') }}"
                            value="{{ old('birthday', ($editing ? optional($employee->birthday)->format('Y-m-d') : '')) }}"
                            max="255"
                            required
                        ></x-inputs.date>
                    </x-inputs.group>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-sm-12 col-12">
                        <x-inputs.select name="ref_nationality_id"
                                         label="{{ __('crud.employees.inputs.ref_nationality_id') }}"
                                         class="select2"
                        >
                            @php $selected = old('ref_nationality_id', ($editing ? $employee->ref_nationality_id : '')) @endphp
                            <option
                                disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
                            @foreach($refNationalities as $value)
                                <option
                                    value="{{ $value->id }}" {{ $selected == $value->id ? 'selected' : '' }} >{{ $value->name }}</option>
                            @endforeach
                        </x-inputs.select>

                        <x-inputs.select name="ref_family_state_id"
                                         label="{{ __('crud.employees.inputs.ref_family_state_id') }}"
                        >
                            @php $selected = old('ref_family_state_id', ($editing ? $employee->ref_family_state_id : '')) @endphp
                            <option
                                disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
                            @foreach($refFamilyStates as $value)
                                <option
                                    value="{{ $value->id }}" {{ $selected == $value->id ? 'selected' : '' }} >{{ $value->name }}</option>
                            @endforeach
                        </x-inputs.select>
                    </div>

                    <div class="col-lg-8 col-sm-12 col-12">
                        <label class="label " for="birth_place">{{ __('crud.employees.inputs.birth_place') }}</label>
                        <textarea id="birth_place"
                                  name="birth_place"
                                  rows="4"
                                  class="form-control"
                                  maxlength="255"
                                  autocomplete="off"
                        >{{ old('birth_place', ($editing ? $employee->birth_place : ''))}}</textarea>
                    </div>
                </div>

                <div class="row">
                    <x-inputs.group class="col-lg-4 col-sm-12 col-12">
                        <x-inputs.text
                            name="person_email"
                            label="{{ __('crud.employees.inputs.person_email') }}"
                            value="{{ old('person_email', ($editing ? $employee->person_email : '')) }}"
                            maxlength="255"
                            placeholder="{{ __('crud.employees.inputs.person_email') }}"
                        ></x-inputs.text>
                    </x-inputs.group>
                </div>

            </div>
            <div class="tab-pane fade" id="custom-tabs-one-profile" role="tabpanel"
                 aria-labelledby="custom-tabs-one-profile-tab"
            >
                <div class="row">
                    <x-inputs.group class="col-lg-4 col-12 col-sm-12">
                        <x-inputs.text
                            name="contract_num"
                            label="{{ __('crud.employees.inputs.contract_num') }}"
                            value="{{ old('contract_num', ($editing ? $employee->contract_num : '')) }}"
                            maxlength="255"
                            placeholder="{{ __('crud.employees.inputs.contract_num') }}"
                            required
                        ></x-inputs.text>
                    </x-inputs.group>
                    <x-inputs.group class="col-lg-4 col-12 col-sm-12">
                        <x-inputs.date
                            name="contract_date"
                            label="{{ __('crud.employees.inputs.contract_date') }}"
                            value="{{ old('contract_date', ($editing ? optional($employee->contract_date)->format('Y-m-d') : '')) }}"
                            max="255"
                            required
                        ></x-inputs.date>
                    </x-inputs.group>
                    <x-inputs.group class="col-lg-4 col-12 col-sm-12">
                        <x-inputs.date
                            name="date_zav"
                            label="{{ __('crud.employees.inputs.date_zav') }}"
                            value="{{ old('date_zav', ($editing ? optional($employee->date_zav)->format('Y-m-d') : '')) }}"
                            max="255"
                            required
                        ></x-inputs.date>
                    </x-inputs.group>
                </div>
                <div class="row">
                    <x-inputs.group class="col-lg-4 col-12 col-sm-12">
                        <x-inputs.number
                            name="tab_num"
                            label="{{ __('crud.employees.inputs.tab_num') }}"
                            value="{{ old('tab_num', ($editing ? $employee->tab_num : '')) }}"
                            placeholder="{{ __('crud.employees.inputs.tab_num') }}"
                        ></x-inputs.number>
                    </x-inputs.group>
                    <x-inputs.group class="col-lg-4 col-12 col-sm-12">
                        <x-inputs.date
                            name="date_post"
                            label="{{ __('crud.employees.inputs.date_post') }}"
                            value="{{ old('date_post', ($editing ? optional($employee->date_post)->format('Y-m-d') : '')) }}"
                            max="255"
                        ></x-inputs.date>
                    </x-inputs.group>
                    <div class="form-group col-lg-4 col-12 col-sm-12">
                        <label>{{ __('crud.employees.inputs.position_id') }}</label>
                        <div>
                            <select class="form-control select2" name="position_id">
                                @foreach($positions as $branches)
                                    @foreach($branches->refDepartments as $departments)
                                        <optgroup label="{{ $branches->name }} - {{ $departments->name }}">
                                            @foreach($departments->positions as $position)
                                                <option value="{{ $position->id }}">{{ $position->name }}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <x-inputs.group class="col-lg-4 col-12 col-sm-12">
                        <x-inputs.select
                            name="ref_account_type_id"
                            label="{{ __('crud.employees.inputs.ref_account_type_id') }}"
                            required
                        >
                            @php $selected = old('ref_account_type_id', ($editing ? $employee->ref_account_type_id : '')) @endphp
                            <option
                                disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
                            @foreach($refAccountTypes as $value)
                                <option
                                    value="{{ $value->id }}" {{ $selected == $value->id ? 'selected' : '' }} >{{ $value->name }}</option>
                            @endforeach
                        </x-inputs.select>
                    </x-inputs.group>
                    <x-inputs.group class="col-lg-4 col-12 col-sm-12">
                        <x-inputs.text
                            name="account"
                            label="{{ __('crud.employees.inputs.account') }}"
                            value="{{ old('account', ($editing ? $employee->account : '')) }}"
                            maxlength="255"
                            placeholder="{{ __('crud.employees.inputs.account') }}"
                        ></x-inputs.text>
                    </x-inputs.group>
                    <x-inputs.group class="col-lg-4 col-12 col-sm-12">
                        <x-inputs.number
                            name="oklad"
                            label="{{ __('crud.employees.inputs.oklad') }}"
                            value="{{ old('oklad', ($editing ? $employee->oklad : '')) }}"
                            step="1"
                            placeholder="{{ __('crud.employees.inputs.oklad') }}"
                        ></x-inputs.number>
                    </x-inputs.group>
                    <x-inputs.group class="col-lg-4 col-12 col-sm-12">
                        <x-inputs.email
                            name="email"
                            label="Email"
                            value="{{ old('email', ($editing ? $employee->email : '')) }}"
                            maxlength="255"
                            placeholder="Email"
                            required
                        ></x-inputs.email>
                    </x-inputs.group>
                    <x-inputs.group class="col-lg-4 col-12 col-sm-12">
                        <x-inputs.select
                            name="ref_user_status_id"
                            label="{{ __('crud.employees.inputs.ref_user_status_id') }}"
                            required
                        >
                            @php $selected = old('ref_user_status_id', ($editing ? $employee->ref_user_status_id : '')) @endphp
                            <option
                                disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
                            @foreach($refUserStatuses as $value)
                                <option
                                    value="{{ $value->id }}" {{ $selected == $value->id ? 'selected' : '' }} >{{ $value->name }}</option>
                            @endforeach
                        </x-inputs.select>
                    </x-inputs.group>








{{--                    <x-inputs.group class="col-sm-12">--}}
{{--                        <x-inputs.text--}}
{{--                            name="gos_nagr"--}}
{{--                            label="Gos Nagr"--}}
{{--                            value="{{ old('gos_nagr', ($editing ? $employee->gos_nagr : '')) }}"--}}
{{--                            maxlength="255"--}}
{{--                            placeholder="Gos Nagr"--}}
{{--                            required--}}
{{--                        ></x-inputs.text>--}}
{{--                    </x-inputs.group>--}}

{{--                    <x-inputs.group class="col-sm-12">--}}
{{--                        <x-inputs.checkbox--}}
{{--                            name="pens"--}}
{{--                            label="Pens"--}}
{{--                            :checked="old('pens', ($editing ? $employee->pens : 0))"--}}
{{--                        ></x-inputs.checkbox>--}}
{{--                    </x-inputs.group>--}}

{{--                    <x-inputs.group class="col-sm-12">--}}
{{--                        <x-inputs.date--}}
{{--                            name="pens_date"--}}
{{--                            label="Pens Date"--}}
{{--                            value="{{ old('pens_date', ($editing ? optional($employee->pens_date)->format('Y-m-d') : '')) }}"--}}
{{--                            max="255"--}}
{{--                        ></x-inputs.date>--}}
{{--                    </x-inputs.group>--}}

{{--                    <x-inputs.group class="col-sm-12">--}}
{{--                        <x-inputs.text--}}
{{--                            name="lgot"--}}
{{--                            label="Lgot"--}}
{{--                            value="{{ old('lgot', ($editing ? $employee->lgot : '')) }}"--}}
{{--                            maxlength="255"--}}
{{--                            placeholder="Lgot"--}}
{{--                        ></x-inputs.text>--}}
{{--                    </x-inputs.group>--}}
                </div>
            </div>
            {{--            <div class="tab-pane fade" id="custom-tabs-one-messages" role="tabpanel" aria-labelledby="custom-tabs-one-messages-tab">--}}
            {{--                Morbi turpis dolor, vulputate vitae felis non, tincidunt congue mauris. Phasellus volutpat augue id mi placerat mollis. Vivamus faucibus eu massa eget condimentum. Fusce nec hendrerit sem, ac tristique nulla. Integer vestibulum orci odio. Cras nec augue ipsum. Suspendisse ut velit condimentum, mattis urna a, malesuada nunc. Curabitur eleifend facilisis velit finibus tristique. Nam vulputate, eros non luctus efficitur, ipsum odio volutpat massa, sit amet sollicitudin est libero sed ipsum. Nulla lacinia, ex vitae gravida fermentum, lectus ipsum gravida arcu, id fermentum metus arcu vel metus. Curabitur eget sem eu risus tincidunt eleifend ac ornare magna.--}}
            {{--            </div>--}}
            {{--            <div class="tab-pane fade" id="custom-tabs-one-settings" role="tabpanel" aria-labelledby="custom-tabs-one-settings-tab">--}}
            {{--                Pellentesque vestibulum commodo nibh nec blandit. Maecenas neque magna, iaculis tempus turpis ac, ornare sodales tellus. Mauris eget blandit dolor. Quisque tincidunt venenatis vulputate. Morbi euismod molestie tristique. Vestibulum consectetur dolor a vestibulum pharetra. Donec interdum placerat urna nec pharetra. Etiam eget dapibus orci, eget aliquet urna. Nunc at consequat diam. Nunc et felis ut nisl commodo dignissim. In hac habitasse platea dictumst. Praesent imperdiet accumsan ex sit amet facilisis.--}}
            {{--            </div>--}}
        </div>
    </div>

</div>
