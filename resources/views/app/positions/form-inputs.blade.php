@php
    $editing = isset($position);
    $desclensions = [];
    if($editing){
        if(count($position->positionDeclensions) > 0){
            foreach($position->positionDeclensions as $decl){
                $desclensions[$decl->locale]['name'] = 'name';
                $desclensions[$decl->locale]['data'][] = [
                    "type" => $decl->case_type,
                    "name" => $decl->type_description,
                    "value" => $decl->value
                ];
            }
        }
    }
@endphp

<div class="row">
    <div class="col-lg-6 col-12 col-sm-12">
        @include('components.lang_name_array_edit', ['editing' => $editing, 'model' => ($editing) ? $position : null, 'noCol' => true])
        <span class="btn btn-sm btn-info btn-block" onclick="sendDeclensions()">
            {{ __('declension.labels.show') }}
        </span>
    </div>

    <div class="col-lg-6 col-12 col-sm-12">
        <x-inputs.select name="department_id"
                         label="{{ __('crud.positions.inputs.department_id') }}"
                         class="select2"
                         required
        >
            @php $selected = old('department_id', ($editing ? $position->department_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
            @foreach($departments as $department)
                <option value="{{ $department->id }}" {{ $selected == $department->id ? 'selected' : '' }} >{{ $department->branch->name }} > {{ $department->name }}</option>
            @endforeach
        </x-inputs.select>

        <x-inputs.select name="pos_level"
                         label="{{ __('crud.positions.inputs.pos_level') }}"
                         class="select2"
                         required
        >
            @php $selected = old('pos_level', ($editing ? $position->pos_level : '')) @endphp
            <option value="0" {{ empty($selected) ? 'selected' : '' }}>нет</option>
            @foreach($positions as $pos)
                <option value="{{ $pos->id }}" {{ $selected == $pos->id ? 'selected' : '' }} >{{ $pos->name }}</option>
            @endforeach
        </x-inputs.select>

        <x-inputs.number
            name="cnt"
            label="{{ __('crud.positions.inputs.cnt') }}"
            value="{{ old('cnt', ($editing ? $position->cnt : '0')) }}"
            max="255"
            placeholder="{{ __('crud.positions.inputs.cnt') }}"
            required
        ></x-inputs.number>

        <x-inputs.number
            name="min_salary"
            label="{{ __('crud.positions.inputs.min_salary') }}"
            value="{{ old('min_salary', ($editing ? $position->min_salary : '')) }}"
            step="1.00"
            placeholder="{{ __('crud.positions.inputs.min_salary') }}"
        ></x-inputs.number>
        <x-inputs.number
            name="max_salary"
            label="{{ __('crud.positions.inputs.max_salary') }}"
            value="{{ old('max_salary', ($editing ? $position->max_salary : '')) }}"
            step="1.00"
            placeholder="{{ __('crud.positions.inputs.max_salary') }}"
        ></x-inputs.number>
    </div>
    <div class="col-lg-12 col-12 col-sm-12 mt-4" id="declensions">
        @include('components.declensions_text', ['result' => $desclensions])
    </div>
</div>

<script>
    function sendDeclensions()
    {
        let allTextInputs = $('input[type=text]');
        let data = [];
        allTextInputs.each((i, item) => {
            let spl = item.name.split('[');
            if(item.value.trim() == ''){
                $(document).Toasts('create', {
                    class: 'bg-danger',
                    title: 'Ошибка!',
                    subtitle: '',
                    body: '{{ __('declension.errors.trim_name') }}'
                })
                return false;
            }
            if(spl[0] === 'name' && spl[1]){
                data.push({
                    name: spl[0],
                    lang: spl[1].replaceAll(']', ''),
                    value: item.value
                })
            }
        })

        $.post("{{ asset('position_declensions') }}", {declensions: data}).then(res => {
            $('#declensions').html(res)
        })
    }
</script>
