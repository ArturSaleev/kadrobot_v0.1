@extends('layouts.app')

@section('title')
    @lang('crud.positions.show_title')
@endsection

@section('content')

    @php
        $desclensions = [];
        if(count($position->positionDeclensions) > 0){
            foreach($position->positionDeclensions as $decl){
                $desclensions[$decl->locale]['name'] = 'name';
                $desclensions[$decl->locale]['data'][] = [
                    "type" => $decl->case_type,
                    "name" => $decl->type_description,
                    "value" => $decl->value
                ];
            }
        }
    @endphp

    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6 col-12 col-sm-12">
                        @include('components.lang_name_array_show', ['model' => $position])
                    </div>
                    <div class="col-lg-6 col-12 col-sm-12">
                        <label>{{ __('crud.other_data') }}</label>
                        <table class="table table-border">
                            <tr>
                                <td class="text-info">@lang('crud.positions.inputs.department_id')</td>
                                <td><span>{{ $position->department->name ?? '-' }}</span></td>
                            </tr>
                            <tr>
                                <td class="text-info">@lang('crud.positions.inputs.cnt')</td>
                                <td><span>{{ $position->cnt ?? '-' }}</span></td>
                            </tr>
                            <tr>
                                <td class="text-info">@lang('crud.positions.inputs.pos_level')</td>
                                <td><span>{{ $main_position ?? '-' }}</span></td>
                            </tr>
                            <tr>
                                <td class="text-info">@lang('crud.positions.inputs.min_salary')</td>
                                <td><span>{{ $position->min_salary ?? '-' }}</span></td>
                            </tr>
                            <tr>
                                <td class="text-info">@lang('crud.positions.inputs.max_salary')</td>
                                <td><span>{{ $position->max_salary ?? '-' }}</span></td>
                            </tr>
                        </table>

                    </div>
                </div>

                <div class="col-lg-12 col-12 col-sm-12">
                    @include('components.declensions_text_view', ['result' => $desclensions])
                </div>

                <div class="mt-4">
                    <a href="{{ route('positions.index') }}" class="btn btn-light">
                        <i class="icon ion-md-return-left"></i>
                        @lang('crud.common.back')
                    </a>

                    @can('create', App\Models\Position::class)
                        <a href="{{ route('positions.create') }}" class="btn btn-light">
                            <i class="icon ion-md-add"></i> @lang('crud.common.create')
                        </a>
                    @endcan
                </div>
            </div>
        </div>
    </div>
@endsection
