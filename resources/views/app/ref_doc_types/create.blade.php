@extends('layouts.app')

@section('title')
    @lang('crud.ref_doc_types.create_title')
@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <x-form
                method="POST"
                action="{{ route('ref-doc-types.store') }}"
            >
                @include('app.ref_doc_types.form-inputs')

                <div class="mt-4">
                    <a
                        href="{{ route('ref-doc-types.index') }}"
                        class="btn btn-light"
                    >
                        <i class="icon ion-md-return-left text-primary"></i>
                        @lang('crud.common.back')
                    </a>

                    <button type="submit" class="btn btn-primary float-right">
                        <i class="icon ion-md-save"></i>
                        @lang('crud.common.create')
                    </button>
                </div>
            </x-form>
        </div>
    </div>
</div>
@endsection
