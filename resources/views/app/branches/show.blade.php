@extends('layouts.app')

@section('title')
    @lang('crud.branches.show_title')
@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-5">
                    @include('components.lang_name_array_show', ['model' => $branch])

                    <div class="mb-4">
                        <label>@lang('crud.branches.inputs.branch_main')</label>
                        <span>{{ ($branch->branch_main) ? 'Да' : 'Нет' }}</span>
                    </div>
                </div>

                <div class="col-lg-7">
                    <div class="mb-4">
                        <label>@lang('crud.branches.inputs.phone')</label>
                        @foreach($branch->branchPhones as $phones)
                        <div class="row">
                            <div class="col-lg-4">
                                <span>{{ $phones->refTypePhone->name ?? '-' }}</span>
                            </div>
                            <div class="col-lg-8">
                                <span class="form-control">{{ $phones->name ?? '-' }}</span>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <div class="mb-4">
                        <label>@lang('crud.branches.inputs.address')</label>
                        @foreach($branch->branchAddreses as $address)
                            <div class="row">
                                <div class="col-lg-4">
                                    <span>{{ $address->refAddressType->name ?? '-' }}</span>
                                </div>
                                <div class="col-lg-8 mb-4">
                                    @include('components.lang_name_group_array_show', ['model' => $address])
                                </div>
                            </div>
                        @endforeach
                    </div>


                </div>

            </div>

            <div class="mt-4">
                <a href="{{ route('branches.index') }}" class="btn btn-light">
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\Branch::class)
                <a href="{{ route('branches.create') }}" class="btn btn-light">
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
