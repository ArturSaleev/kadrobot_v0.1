<div class="row phones" id="{{ (isset($id)) ? $id : ':id' }}">
    <x-inputs.group class="col-sm-6 col-lg-6">
        <x-inputs.select name="phones[{{ (isset($id)) ? $id : ':id' }}][ref_type_phone_id]"
                         label="{{ __('crud.ref_type_phones.name') }}"
                         required>
            @php
                $selected = (isset($phone)) ? $phone->ref_type_phone_id : ''
            @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
            @foreach($refTypePhone as $typePhone)
                <option value="{{ $typePhone->id }}" {{ $selected == $typePhone->id ? 'selected' : '' }} >
                    {{ $typePhone->name }}
                </option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>
    <x-inputs.group class="col-sm-6 col-lg-6">
        <label>{{ __('crud.ref_type_phones.inputs.name') }}</label>
        <div class="input-group">
            <input type="text"
                   class="form-control"
                   name="phones[{{ (isset($id)) ? $id : ':id' }}][name]"
                   placeholder="{{ __('crud.ref_type_phones.placeholder') }}"
                   value="{{ (isset($phone)) ? $phone->name : '' }}"
                   required
            />
            <div class="input-group-append">
                    <span class="input-group-text btn btn-danger" onclick="removePhone({{ (isset($id)) ? $id : ':id' }})">
                        <i class="icon ion-md-trash"></i>
                    </span>
            </div>
        </div>
    </x-inputs.group>
</div>
