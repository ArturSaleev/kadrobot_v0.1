<div class="row address" id="{{ (isset($id)) ? $id : ':id' }}">
    <x-inputs.group class="col-sm-12 col-lg-12">
        <label>{{ __('crud.ref_address_types.name') }}</label>
        <div class="input-group">
            <select name="branchAddress[{{ (isset($id)) ? $id : ':id' }}][ref_type_address_id]"
                    class="form-control select2"
                    required>
                @php
                    $selected = (isset($address)) ? $address->ref_address_type_id : '';
                @endphp
                <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
                @foreach($refTypeAddress as $typeAddress)
                    <option value="{{ $typeAddress->id }}" {{ $selected == $typeAddress->id ? 'selected' : '' }}>
                        {{ $typeAddress->name }}
                    </option>
                @endforeach
            </select>

            <div class="input-group-append">
                    <span class="input-group-text btn btn-danger" onclick="removeAddress({{ (isset($id)) ? $id : ':id' }})">
                        <i class="icon ion-md-trash"></i>
                    </span>
            </div>
        </div>
    </x-inputs.group>

    @foreach(config('panel.available_languages', []) as $lang=>$title)
        <x-inputs.group class="col-sm-12 col-lg-12">
            <div class="input-group">
                <input type="text"
                       class="form-control"
                       name="branchAddress[{{ (isset($id)) ? $id : ':id' }}][name][{{ $lang }}]"
                       placeholder="{{ $title }}"
                       value="{{ (isset($address)) ? $address->translate($lang)->name : '' }}"
                />
            </div>
        </x-inputs.group>
    @endforeach



</div>
