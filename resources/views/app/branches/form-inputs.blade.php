@php $editing = isset($branch) @endphp

<x-inputs.hidden name="company_id" value="1"></x-inputs.hidden>
<x-inputs.group class="col-sm-12 col-lg-12 text-right">
    <x-inputs.checkbox
        name="branch_main"
        label="{{ __('crud.branches.inputs.branch_main') }}"
        :checked="old('branch_main', ($editing ? $branch->branch_main : 0))"
    ></x-inputs.checkbox>
</x-inputs.group>


<div class="row">
    @include('components.lang_name_array_edit', ['editing' => $editing, 'model' => ($editing) ? $branch : null])

    <div class="col-lg-6 col-sm-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-9 col-9 col-sm-9">
                        {{ __('crud.branch_phones.name') }}
                    </div>
                    <div class="col-lg-3 col-3 col-sm-3 text-right">
                        <span class="btn btn-sm btn-outline-primary" id="add_phone">
                            <i class="icon ion-md-add"></i> {{ __('crud.common.add') }}
                        </span>
                    </div>
                </div>
            </div>
            <div class="card-body" id="list_phones">
                @if($editing)
                    @foreach($branch->branchPhones as $phone)
                        @include('app.branches.branchPhoneInputs', ['id' => $phone->id, 'phone' => $phone])
                    @endforeach
                @endif
            </div>
        </div>

    </div>

    <div class="col-lg-12 col-12 col-sm-12">
        <div class="card mt-2">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-9 col-9 col-sm-9">
                        {{ __('crud.all_branch_addreses.name') }}
                    </div>
                    <div class="col-lg-3 col-3 col-sm-3 text-right">
                        <span class="btn btn-sm btn-outline-primary" id="add_address">
                            <i class="icon ion-md-add"></i> {{ __('crud.common.add') }}
                        </span>
                    </div>
                </div>
            </div>
            <div class="card-body" id="list_address">
                @if($editing)
                    @foreach($branch->branchAddreses as $address)
                        @include('app.branches.branchAddressInputs', ['refTypeAddress' => $refTypeAddress, 'id' => $address->id, 'address' => $address])
                    @endforeach
                @endif
            </div>
        </div>
    </div>

</div>

<script>
    $('#add_phone').click(function () {
        let html = $('#phones').html();
        let id = Math.round(Math.random() * 100000);
        html = html.replaceAll(':id', id.toString());
        $('#list_phones').append(html);
    })

    $('#add_address').click(function () {
        let html = $('#addresses').html();
        let id = Math.round(Math.random() * 100000);
        html = html.replaceAll(':id', id.toString());
        $('#list_address').append(html);
    })

    function removePhone(id)
    {
        $('.phones#'+id).remove();
    }

    function removeAddress(id)
    {
        $('.address#'+id).remove();
    }


</script>


