@extends('layouts.app')

@section('title')

@endsection

@section('content')
<div class="container">
    <div class="searchbar mt-0 mb-4">
        <div class="row">
            <div class="col-md-6">
                <form>
                    <div class="input-group">
                        <input
                            id="indexSearch"
                            type="text"
                            name="search"
                            placeholder="{{ __('crud.common.search') }}"
                            value="{{ $search ?? '' }}"
                            class="form-control"
                            autocomplete="off"
                        />
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-primary">
                                <i class="icon ion-md-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6 text-right">
                @can('create', App\Models\T2Card::class)
                <a
                    href="{{ route('t2-cards.create') }}"
                    class="btn btn-primary"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div style="display: flex; justify-content: space-between;">
                <h4 class="card-title">@lang('crud.t2_cards.index_title')</h4>
            </div>

            <div class="table-responsive">
                <table class="table table-borderless table-hover">
                    <thead>
                        <tr>
                            <th class="text-left">
                                @lang('crud.t2_cards.inputs.company_id')
                            </th>
                            <th class="text-left">
                                @lang('crud.t2_cards.inputs.employee_id')
                            </th>
                            <th class="text-left">
                                @lang('crud.t2_cards.inputs.ref_branch_id')
                            </th>
                            <th class="text-left">
                                @lang('crud.t2_cards.inputs.ref_action_id')
                            </th>
                            <th class="text-left">
                                @lang('crud.t2_cards.inputs.ref_position_id')
                            </th>
                            <th class="text-center">
                                @lang('crud.common.actions')
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($t2Cards as $t2Card)
                        <tr>
                            <td>
                                {{ optional($t2Card->company)->bin ?? '-' }}
                            </td>
                            <td>
                                {{ optional($t2Card->employee)->iin ?? '-' }}
                            </td>
                            <td>
                                {{ optional($t2Card->refBranch)->id ?? '-' }}
                            </td>
                            <td>
                                {{ optional($t2Card->refAction)->id ?? '-' }}
                            </td>
                            <td>
                                {{ optional($t2Card->refPosition)->id ?? '-' }}
                            </td>
                            <td class="text-center" style="width: 134px;">
                                <div
                                    role="group"
                                    aria-label="Row Actions"
                                    class="btn-group"
                                >
                                    @can('update', $t2Card)
                                    <a
                                        href="{{ route('t2-cards.edit', $t2Card) }}"
                                    >
                                        <button
                                            type="button"
                                            class="btn btn-light"
                                        >
                                            <i class="icon ion-md-create"></i>
                                        </button>
                                    </a>
                                    @endcan @can('view', $t2Card)
                                    <a
                                        href="{{ route('t2-cards.show', $t2Card) }}"
                                    >
                                        <button
                                            type="button"
                                            class="btn btn-light"
                                        >
                                            <i class="icon ion-md-eye"></i>
                                        </button>
                                    </a>
                                    @endcan @can('delete', $t2Card)
                                    <form
                                        action="{{ route('t2-cards.destroy', $t2Card) }}"
                                        method="POST"
                                        onsubmit="return confirm('{{ __('crud.common.are_you_sure') }}')"
                                    >
                                        @csrf @method('DELETE')
                                        <button
                                            type="submit"
                                            class="btn btn-light text-danger"
                                        >
                                            <i class="icon ion-md-trash"></i>
                                        </button>
                                    </form>
                                    @endcan
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="6">
                                @lang('crud.common.no_items_found')
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="6">{!! $t2Cards->render() !!}</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
