@extends('layouts.app')

@section('title')

@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('t2-cards.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.t2_cards.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.t2_cards.inputs.company_id')</h5>
                    <span>{{ optional($t2Card->company)->bin ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.t2_cards.inputs.employee_id')</h5>
                    <span>{{ optional($t2Card->employee)->iin ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.t2_cards.inputs.ref_branch_id')</h5>
                    <span>{{ optional($t2Card->refBranch)->id ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.t2_cards.inputs.ref_action_id')</h5>
                    <span>{{ optional($t2Card->refAction)->id ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.t2_cards.inputs.ref_position_id')</h5>
                    <span>{{ optional($t2Card->refPosition)->id ?? '-' }}</span>
                </div>
            </div>

            <div class="mt-4">
                <a href="{{ route('t2-cards.index') }}" class="btn btn-light">
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\T2Card::class)
                <a href="{{ route('t2-cards.create') }}" class="btn btn-light">
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
