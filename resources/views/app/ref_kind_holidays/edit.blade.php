@extends('layouts.app')

@section('title')
    @lang('crud.ref_kind_holidays.edit_title')
@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <x-form
                method="PUT"
                action="{{ route('ref-kind-holidays.update', $refKindHoliday) }}"
            >
                @include('app.ref_kind_holidays.form-inputs')

                <div class="mt-4">
                    <a
                        href="{{ route('ref-kind-holidays.index') }}"
                        class="btn btn-light"
                    >
                        <i class="icon ion-md-return-left text-primary"></i>
                        @lang('crud.common.back')
                    </a>

                    <a
                        href="{{ route('ref-kind-holidays.create') }}"
                        class="btn btn-light"
                    >
                        <i class="icon ion-md-add text-primary"></i>
                        @lang('crud.common.create')
                    </a>

                    <button type="submit" class="btn btn-primary float-right">
                        <i class="icon ion-md-save"></i>
                        @lang('crud.common.update')
                    </button>
                </div>
            </x-form>
        </div>
    </div>
</div>
@endsection
