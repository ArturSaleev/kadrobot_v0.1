@php $editing = isset($refOked) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="oked"
            label="{{ __('crud.ref_okeds.inputs.oked') }}"
            value="{{ old('oked', ($editing ? $refOked->oked : '')) }}"
            maxlength="255"
            placeholder="{{ __('crud.ref_okeds.inputs.oked') }}"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <div class="col-sm-12">
        @include('components.lang_name_array_edit', ['model' => ($editing) ? $refOked : null, 'editing' => $editing, 'noCol' => true, 'title' => 'crud.ref_okeds.inputs.name'])
    </div>
    <div class="col-sm-12">
        @include('components.lang_name_array_edit', ['model' => ($editing) ? $refOked : null, 'editing' => $editing, 'noCol' => true, 'title' => 'crud.ref_okeds.inputs.name_oked', 'name' => 'name_oked'])
    </div>
</div>
