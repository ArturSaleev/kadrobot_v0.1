@extends('layouts.app')

@section('title')
    @lang('crud.ref_okeds.show_title')
@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.ref_okeds.inputs.oked')</h5>
                    <span>{{ $refOked->oked ?? '-' }}</span>
                </div>
            </div>
            <div class="mt-4">
                @include('components.lang_name_array_show', ['model' => $refOked, 'title' => 'crud.ref_okeds.inputs.name'])
            </div>
            <div class="mt-4">
                @include('components.lang_name_array_show', ['model' => $refOked, 'column_name' => 'name_oked', 'title' => 'crud.ref_okeds.inputs.name_oked'])
            </div>
            <div class="mt-4">
                <a href="{{ route('ref-okeds.index') }}" class="btn btn-light">
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\RefOked::class)
                <a href="{{ route('ref-okeds.create') }}" class="btn btn-light">
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
