@extends('layouts.app')

@section('title')

@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('ref-metas.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.ref_metas.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.ref_metas.inputs.title')</h5>
                    <span>{{ $refMeta->title ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.ref_metas.inputs.name')</h5>
                    <span>{{ $refMeta->name ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.ref_metas.inputs.tablename')</h5>
                    <span>{{ $refMeta->tablename ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.ref_metas.inputs.column_name')</h5>
                    <span>{{ $refMeta->column_name ?? '-' }}</span>
                </div>
            </div>

            <div class="mt-4">
                <a href="{{ route('ref-metas.index') }}" class="btn btn-light">
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\RefMeta::class)
                <a href="{{ route('ref-metas.create') }}" class="btn btn-light">
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
