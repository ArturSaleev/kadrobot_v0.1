@php $editing = isset($refMeta) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="title"
            label="Title"
            value="{{ old('title', ($editing ? $refMeta->title : '')) }}"
            maxlength="255"
            placeholder="Title"
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="name"
            label="Name"
            value="{{ old('name', ($editing ? $refMeta->name : '')) }}"
            maxlength="255"
            placeholder="Name"
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="tablename"
            label="Tablename"
            value="{{ old('tablename', ($editing ? $refMeta->tablename : '')) }}"
            maxlength="255"
            placeholder="Tablename"
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="column_name"
            label="Column Name"
            value="{{ old('column_name', ($editing ? $refMeta->column_name : '')) }}"
            maxlength="255"
            placeholder="Column Name"
        ></x-inputs.text>
    </x-inputs.group>
</div>
