@extends('layouts.app')

@section('title')

@endsection

@section('content')
<div class="container">
    <div class="searchbar mt-0 mb-4">
        <div class="row">
            <div class="col-md-6">
                <form>
                    <div class="input-group">
                        <input
                            id="indexSearch"
                            type="text"
                            name="search"
                            placeholder="{{ __('crud.common.search') }}"
                            value="{{ $search ?? '' }}"
                            class="form-control"
                            autocomplete="off"
                        />
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-primary">
                                <i class="icon ion-md-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6 text-right">
                @can('create', App\Models\EmployeeMilitary::class)
                <a
                    href="{{ route('employee-militaries.create') }}"
                    class="btn btn-primary"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div style="display: flex; justify-content: space-between;">
                <h4 class="card-title">
                    @lang('crud.employee_militaries.index_title')
                </h4>
            </div>

            <div class="table-responsive">
                <table class="table table-borderless table-hover">
                    <thead>
                        <tr>
                            <th class="text-left">
                                @lang('crud.employee_militaries.inputs.employee_id')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_militaries.inputs.group')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_militaries.inputs.category')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_militaries.inputs.rank')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_militaries.inputs.speciality')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_militaries.inputs.voenkom')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_militaries.inputs.spec_uch')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_militaries.inputs.spec_uch_num')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_militaries.inputs.fit')
                            </th>
                            <th class="text-center">
                                @lang('crud.common.actions')
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($employeeMilitaries as $employeeMilitary)
                        <tr>
                            <td>
                                {{ optional($employeeMilitary->employee)->iin ??
                                '-' }}
                            </td>
                            <td>{{ $employeeMilitary->group ?? '-' }}</td>
                            <td>{{ $employeeMilitary->category ?? '-' }}</td>
                            <td>{{ $employeeMilitary->rank ?? '-' }}</td>
                            <td>{{ $employeeMilitary->speciality ?? '-' }}</td>
                            <td>{{ $employeeMilitary->voenkom ?? '-' }}</td>
                            <td>{{ $employeeMilitary->spec_uch ?? '-' }}</td>
                            <td>
                                {{ $employeeMilitary->spec_uch_num ?? '-' }}
                            </td>
                            <td>{{ $employeeMilitary->fit ?? '-' }}</td>
                            <td class="text-center" style="width: 134px;">
                                <div
                                    role="group"
                                    aria-label="Row Actions"
                                    class="btn-group"
                                >
                                    @can('update', $employeeMilitary)
                                    <a
                                        href="{{ route('employee-militaries.edit', $employeeMilitary) }}"
                                    >
                                        <button
                                            type="button"
                                            class="btn btn-light"
                                        >
                                            <i class="icon ion-md-create"></i>
                                        </button>
                                    </a>
                                    @endcan @can('view', $employeeMilitary)
                                    <a
                                        href="{{ route('employee-militaries.show', $employeeMilitary) }}"
                                    >
                                        <button
                                            type="button"
                                            class="btn btn-light"
                                        >
                                            <i class="icon ion-md-eye"></i>
                                        </button>
                                    </a>
                                    @endcan @can('delete', $employeeMilitary)
                                    <form
                                        action="{{ route('employee-militaries.destroy', $employeeMilitary) }}"
                                        method="POST"
                                        onsubmit="return confirm('{{ __('crud.common.are_you_sure') }}')"
                                    >
                                        @csrf @method('DELETE')
                                        <button
                                            type="submit"
                                            class="btn btn-light text-danger"
                                        >
                                            <i class="icon ion-md-trash"></i>
                                        </button>
                                    </form>
                                    @endcan
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="10">
                                @lang('crud.common.no_items_found')
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="10">
                                {!! $employeeMilitaries->render() !!}
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
