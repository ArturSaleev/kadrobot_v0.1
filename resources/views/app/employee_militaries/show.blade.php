@extends('layouts.app')

@section('title')

@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('employee-militaries.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.employee_militaries.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_militaries.inputs.employee_id')
                    </h5>
                    <span
                        >{{ optional($employeeMilitary->employee)->iin ?? '-'
                        }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_militaries.inputs.group')</h5>
                    <span>{{ $employeeMilitary->group ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_militaries.inputs.category')</h5>
                    <span>{{ $employeeMilitary->category ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_militaries.inputs.rank')</h5>
                    <span>{{ $employeeMilitary->rank ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_militaries.inputs.speciality')</h5>
                    <span>{{ $employeeMilitary->speciality ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_militaries.inputs.voenkom')</h5>
                    <span>{{ $employeeMilitary->voenkom ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_militaries.inputs.spec_uch')</h5>
                    <span>{{ $employeeMilitary->spec_uch ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_militaries.inputs.spec_uch_num')
                    </h5>
                    <span>{{ $employeeMilitary->spec_uch_num ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_militaries.inputs.fit')</h5>
                    <span>{{ $employeeMilitary->fit ?? '-' }}</span>
                </div>
            </div>

            <div class="mt-4">
                <a
                    href="{{ route('employee-militaries.index') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\EmployeeMilitary::class)
                <a
                    href="{{ route('employee-militaries.create') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
