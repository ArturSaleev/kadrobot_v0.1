@php $editing = isset($employeeMilitary) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="employee_id" label="Employee" required>
            @php $selected = old('employee_id', ($editing ? $employeeMilitary->employee_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
            @foreach($employees as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="group"
            label="Group"
            value="{{ old('group', ($editing ? $employeeMilitary->group : '')) }}"
            maxlength="255"
            placeholder="Group"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="category"
            label="Category"
            value="{{ old('category', ($editing ? $employeeMilitary->category : '')) }}"
            maxlength="255"
            placeholder="Category"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="rank"
            label="Rank"
            value="{{ old('rank', ($editing ? $employeeMilitary->rank : '')) }}"
            maxlength="255"
            placeholder="Rank"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="speciality"
            label="Speciality"
            value="{{ old('speciality', ($editing ? $employeeMilitary->speciality : '')) }}"
            maxlength="255"
            placeholder="Speciality"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="voenkom"
            label="Voenkom"
            value="{{ old('voenkom', ($editing ? $employeeMilitary->voenkom : '')) }}"
            maxlength="255"
            placeholder="Voenkom"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="spec_uch"
            label="Spec Uch"
            value="{{ old('spec_uch', ($editing ? $employeeMilitary->spec_uch : '')) }}"
            maxlength="255"
            placeholder="Spec Uch"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="spec_uch_num"
            label="Spec Uch Num"
            value="{{ old('spec_uch_num', ($editing ? $employeeMilitary->spec_uch_num : '')) }}"
            maxlength="255"
            placeholder="Spec Uch Num"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="fit"
            label="Fit"
            value="{{ old('fit', ($editing ? $employeeMilitary->fit : '')) }}"
            maxlength="255"
            placeholder="Fit"
            required
        ></x-inputs.text>
    </x-inputs.group>
</div>
