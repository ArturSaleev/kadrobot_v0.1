@extends('layouts.app')

@section('title')
    @lang('crud.ref_allowance_types.edit_title')
@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <x-form
                method="PUT"
                action="{{ route('ref-allowance-types.update', $refAllowanceType) }}"
            >
                @include('app.ref_allowance_types.form-inputs')

                <div class="mt-4">
                    <a
                        href="{{ route('ref-allowance-types.index') }}"
                        class="btn btn-light"
                    >
                        <i class="icon ion-md-return-left text-primary"></i>
                        @lang('crud.common.back')
                    </a>

                    <a
                        href="{{ route('ref-allowance-types.create') }}"
                        class="btn btn-light"
                    >
                        <i class="icon ion-md-add text-primary"></i>
                        @lang('crud.common.create')
                    </a>

                    <button type="submit" class="btn btn-primary float-right">
                        <i class="icon ion-md-save"></i>
                        @lang('crud.common.update')
                    </button>
                </div>
            </x-form>
        </div>
    </div>
</div>
@endsection
