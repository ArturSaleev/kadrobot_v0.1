@extends('layouts.app')

@section('title')
    @lang('crud.ref_allowance_types.show_title')
@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <div class="mt-4"></div>

            <div class="mt-4">
                <a
                    href="{{ route('ref-allowance-types.index') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\RefAllowanceType::class)
                <a
                    href="{{ route('ref-allowance-types.create') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
