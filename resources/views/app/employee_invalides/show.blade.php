@extends('layouts.app')

@section('title')

@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('employee-invalides.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.employee_invalides.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.employee_invalides.inputs.employee_id')</h5>
                    <span
                        >{{ optional($employeeInvalide->employee)->iin ?? '-'
                        }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_invalides.inputs.num')</h5>
                    <span>{{ $employeeInvalide->num ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_invalides.inputs.date_add')</h5>
                    <span>{{ $employeeInvalide->date_add ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_invalides.inputs.period_begin')
                    </h5>
                    <span>{{ $employeeInvalide->period_begin ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_invalides.inputs.period_end')</h5>
                    <span>{{ $employeeInvalide->period_end ?? '-' }}</span>
                </div>
            </div>

            <div class="mt-4">
                <a
                    href="{{ route('employee-invalides.index') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\EmployeeInvalide::class)
                <a
                    href="{{ route('employee-invalides.create') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
