@extends('layouts.app')

@section('title')
    @lang('crud.departments.show_title')
@endsection

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6 col-12 col-sm-12">
                        @include('components.lang_name_array_show', ['model' => $department])
                    </div>

                    <div class="col-lg-6 col-12 col-sm-12">
                        <div class="mb-4">
                            <label>@lang('crud.departments.inputs.branch_id')</label>
                            <span class="form-control">{{ optional($department->branch)->name ?? '-' }}</span>
                        </div>
                        <div class="mb-4">
                            <label>@lang('crud.departments.inputs.email')</label>
                            <span class="form-control">{{ $department->email ?? '-' }}</span>
                        </div>

                        <div class="mb-4">
                            <label>@lang('crud.departments.inputs.phone')</label>
                            @foreach($department->departmentPhones as $phones)
                                <div class="row">
                                    <div class="col-lg-4">
                                        <span>{{ $phones->refTypePhone->name ?? '-' }}</span>
                                    </div>
                                    <div class="col-lg-8">
                                        <span class="form-control">{{ $phones->name ?? '-' }}</span>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="mt-4">
                    <a
                        href="{{ route('departments.index') }}"
                        class="btn btn-light"
                    >
                        <i class="icon ion-md-return-left"></i>
                        @lang('crud.common.back')
                    </a>

                    @can('create', App\Models\Department::class)
                        <a
                            href="{{ route('departments.create') }}"
                            class="btn btn-light"
                        >
                            <i class="icon ion-md-add"></i> @lang('crud.common.create')
                        </a>
                    @endcan
                </div>
            </div>
        </div>
    </div>
@endsection
