@extends('layouts.app')

@section('title')
    @lang('crud.departments.edit_title')
@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <x-form
                method="PUT"
                action="{{ route('departments.update', $department) }}"
            >
                @include('app.departments.form-inputs')

                <div class="mt-4">
                    <a
                        href="{{ route('departments.index') }}"
                        class="btn btn-light"
                    >
                        <i class="icon ion-md-return-left text-primary"></i>
                        @lang('crud.common.back')
                    </a>

                    <a
                        href="{{ route('departments.create') }}"
                        class="btn btn-light"
                    >
                        <i class="icon ion-md-add text-primary"></i>
                        @lang('crud.common.create')
                    </a>

                    <button type="submit" class="btn btn-primary float-right">
                        <i class="icon ion-md-save"></i>
                        @lang('crud.common.save')
                    </button>
                </div>
            </x-form>

            <noscript id="phones">
                @include('app.branches.branchPhoneInputs', ['refTypePhone' => $refTypePhone])
            </noscript>
        </div>
    </div>
</div>
@endsection
