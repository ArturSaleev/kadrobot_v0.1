@php $editing = isset($department) @endphp

<div class="row">
    @include('components.lang_name_array_edit', ['editing' => $editing, 'model' => ($editing) ? $department : null])

    <div class="col-lg-6 col-12 col-sm-12">
        <x-inputs.select name="branch_id"
                         class="select2"
                         label="{{ __('crud.departments.inputs.branch_id') }}"
                         required
        >
            @php $selected = old('branch_id', ($editing ? $department->branch_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
            @foreach($branches as $branche)
                <option
                    value="{{ $branche->id }}" {{ $selected == $branche->id ? 'selected' : '' }} >{{ $branche->name }}</option>
            @endforeach
        </x-inputs.select>

        <x-inputs.email
            name="email"
            label="{{ __('crud.departments.inputs.email') }}"
            value="{{ old('email', ($editing ? $department->email : '')) }}"
            maxlength="255"
            placeholder="{{ __('crud.departments.inputs.email') }}"
            required
        ></x-inputs.email>


        <div class="card mt-2">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-9 col-9 col-sm-9">
                        {{ __('crud.branch_phones.name') }}
                    </div>
                    <div class="col-lg-3 col-3 col-sm-3 text-right">
                        <span class="btn btn-sm btn-outline-primary" id="add_phone">
                            <i class="icon ion-md-add"></i> {{ __('crud.common.add') }}
                        </span>
                    </div>
                </div>
            </div>
            <div class="card-body" id="list_phones">
                @if($editing)
                    @foreach($department->departmentPhones as $phone)
                        @include('app.branches.branchPhoneInputs', ['id' => $phone->id, 'phone' => $phone])
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>

<script>
    $('#add_phone').click(function () {
        let html = $('#phones').html();
        let id = Math.round(Math.random() * 100000);
        html = html.replaceAll(':id', id.toString());
        $('#list_phones').append(html);
    })

    function removePhone(id) {
        $('.phones#' + id).remove();
    }
</script>
