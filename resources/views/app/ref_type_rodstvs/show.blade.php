@extends('layouts.app')

@section('title')
    @lang('crud.ref_type_rodstvs.show_title')
@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <div class="mt-0">
                @include('components.lang_name_array_show', ['model' => $refTypeRodstv])
            </div>

            <div class="mt-4">
                <a
                    href="{{ route('ref-type-rodstvs.index') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\RefTypeRodstv::class)
                <a
                    href="{{ route('ref-type-rodstvs.create') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
