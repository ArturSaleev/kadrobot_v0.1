@extends('layouts.app')

@section('title')

@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('ref-transport-trips.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.ref_transport_trips.edit_title')
            </h4>

            <x-form
                method="PUT"
                action="{{ route('ref-transport-trips.update', $refTransportTrip) }}"
                class="mt-4"
            >
                @include('app.ref_transport_trips.form-inputs')

                <div class="mt-4">
                    <a
                        href="{{ route('ref-transport-trips.index') }}"
                        class="btn btn-light"
                    >
                        <i class="icon ion-md-return-left text-primary"></i>
                        @lang('crud.common.back')
                    </a>

                    <a
                        href="{{ route('ref-transport-trips.create') }}"
                        class="btn btn-light"
                    >
                        <i class="icon ion-md-add text-primary"></i>
                        @lang('crud.common.create')
                    </a>

                    <button type="submit" class="btn btn-primary float-right">
                        <i class="icon ion-md-save"></i>
                        @lang('crud.common.update')
                    </button>
                </div>
            </x-form>
        </div>
    </div>
</div>
@endsection
