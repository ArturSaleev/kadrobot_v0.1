@php $editing = isset($employeeAddress) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="employee_id" label="Employee" required>
            @php $selected = old('employee_id', ($editing ? $employeeAddress->employee_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
            @foreach($employees as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="locale"
            label="Locale"
            value="{{ old('locale', ($editing ? $employeeAddress->locale : '')) }}"
            maxlength="8"
            placeholder="Locale"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.select
            name="ref_address_type_id"
            label="Ref Address Type"
            required
        >
            @php $selected = old('ref_address_type_id', ($editing ? $employeeAddress->ref_address_type_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>@lang('crud.please_select')</option>
            @foreach($refAddressTypes as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>
</div>
