@extends('layouts.app')

@section('title')

@endsection

@section('content')
<div class="container">
    <div class="searchbar mt-0 mb-4">
        <div class="row">
            <div class="col-md-6">
                <form>
                    <div class="input-group">
                        <input
                            id="indexSearch"
                            type="text"
                            name="search"
                            placeholder="{{ __('crud.common.search') }}"
                            value="{{ $search ?? '' }}"
                            class="form-control"
                            autocomplete="off"
                        />
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-primary">
                                <i class="icon ion-md-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6 text-right">
                @can('create', App\Models\EmployeeTrip::class)
                <a
                    href="{{ route('employee-trips.create') }}"
                    class="btn btn-primary"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div style="display: flex; justify-content: space-between;">
                <h4 class="card-title">
                    @lang('crud.employee_trips.index_title')
                </h4>
            </div>

            <div class="table-responsive">
                <table class="table table-borderless table-hover">
                    <thead>
                        <tr>
                            <th class="text-left">
                                @lang('crud.employee_trips.inputs.employee_id')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_trips.inputs.ref_transport_trip_id')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_trips.inputs.date_begin')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_trips.inputs.date_end')
                            </th>
                            <th class="text-right">
                                @lang('crud.employee_trips.inputs.cnt_days')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_trips.inputs.order_num')
                            </th>
                            <th class="text-left">
                                @lang('crud.employee_trips.inputs.order_date')
                            </th>
                            <th class="text-center">
                                @lang('crud.common.actions')
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($employeeTrips as $employeeTrip)
                        <tr>
                            <td>
                                {{ optional($employeeTrip->employee)->iin ?? '-'
                                }}
                            </td>
                            <td>
                                {{ optional($employeeTrip->refTransportTrip)->id
                                ?? '-' }}
                            </td>
                            <td>{{ $employeeTrip->date_begin ?? '-' }}</td>
                            <td>{{ $employeeTrip->date_end ?? '-' }}</td>
                            <td>{{ $employeeTrip->cnt_days ?? '-' }}</td>
                            <td>{{ $employeeTrip->order_num ?? '-' }}</td>
                            <td>{{ $employeeTrip->order_date ?? '-' }}</td>
                            <td class="text-center" style="width: 134px;">
                                <div
                                    role="group"
                                    aria-label="Row Actions"
                                    class="btn-group"
                                >
                                    @can('update', $employeeTrip)
                                    <a
                                        href="{{ route('employee-trips.edit', $employeeTrip) }}"
                                    >
                                        <button
                                            type="button"
                                            class="btn btn-light"
                                        >
                                            <i class="icon ion-md-create"></i>
                                        </button>
                                    </a>
                                    @endcan @can('view', $employeeTrip)
                                    <a
                                        href="{{ route('employee-trips.show', $employeeTrip) }}"
                                    >
                                        <button
                                            type="button"
                                            class="btn btn-light"
                                        >
                                            <i class="icon ion-md-eye"></i>
                                        </button>
                                    </a>
                                    @endcan @can('delete', $employeeTrip)
                                    <form
                                        action="{{ route('employee-trips.destroy', $employeeTrip) }}"
                                        method="POST"
                                        onsubmit="return confirm('{{ __('crud.common.are_you_sure') }}')"
                                    >
                                        @csrf @method('DELETE')
                                        <button
                                            type="submit"
                                            class="btn btn-light text-danger"
                                        >
                                            <i class="icon ion-md-trash"></i>
                                        </button>
                                    </form>
                                    @endcan
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="8">
                                @lang('crud.common.no_items_found')
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="8">
                                {!! $employeeTrips->render() !!}
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
