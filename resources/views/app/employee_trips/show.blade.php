@extends('layouts.app')

@section('title')

@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('employee-trips.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.employee_trips.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.employee_trips.inputs.employee_id')</h5>
                    <span
                        >{{ optional($employeeTrip->employee)->iin ?? '-'
                        }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>
                        @lang('crud.employee_trips.inputs.ref_transport_trip_id')
                    </h5>
                    <span
                        >{{ optional($employeeTrip->refTransportTrip)->id ?? '-'
                        }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_trips.inputs.date_begin')</h5>
                    <span>{{ $employeeTrip->date_begin ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_trips.inputs.date_end')</h5>
                    <span>{{ $employeeTrip->date_end ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_trips.inputs.cnt_days')</h5>
                    <span>{{ $employeeTrip->cnt_days ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_trips.inputs.order_num')</h5>
                    <span>{{ $employeeTrip->order_num ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.employee_trips.inputs.order_date')</h5>
                    <span>{{ $employeeTrip->order_date ?? '-' }}</span>
                </div>
            </div>

            <div class="mt-4">
                <a
                    href="{{ route('employee-trips.index') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\EmployeeTrip::class)
                <a
                    href="{{ route('employee-trips.create') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
