@php
    $langs = config('panel.available_languages', []);
    $i = 0;
@endphp
<div class="row">
    <div class="col-5 col-sm-3">
        <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
            @foreach($result as $lang=>$data)
                <a class="nav-link {{ ($i == 0) ? 'active' : '' }}"
                   id="vert-tabs-home-tab"
                   data-toggle="pill"
                   href="#{{ $lang }}"
                   role="tab"
                   aria-controls="vert-tabs-{{ $lang }}"
                   aria-selected="true"
                >{{ $langs[$lang] ?? $lang }}</a>
                @php $i++; @endphp
            @endforeach
        </div>
    </div>
    @php $i = 0; @endphp
    <div class="col-7 col-sm-9">
        <div class="tab-content" id="vert-tabs-tabContent">
            @foreach($result as $lang=>$data)
                <div class="tab-pane text-left fade {{ ($i == 0) ? 'active show' : '' }}"
                     id="{{ $lang }}"
                     role="tabpanel"
                     aria-labelledby="vert-tabs-{{ $lang }}-tab"
                >
                    <table class="table table-bordered">
                        @foreach($data['data'] as $i=>$item)
                            <tr>
                                <td>{{ $item['name'] }}</td>
                                <td>
                                    <input type="text" name="declensions[{{ $data['name'] }}][{{ $lang }}][{{ $i }}][{{ $item['type'] }}]" class="form-control" value="{{ $item['value'] }}">
                                    <input type="hidden" name="declensions_descript[{{ $data['name'] }}][{{ $lang }}][{{ $i }}][{{ $item['type'] }}]" class="form-control" value="{{ $item['name'] }}">
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
                @php $i++; @endphp
            @endforeach
        </div>
    </div>
</div>
