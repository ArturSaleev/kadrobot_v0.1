@if(isset($title))
    <label>@lang($title)</label>
@else
    <label>@lang('crud.names')</label>
@endif
@foreach(config('panel.available_languages', []) as $lang=>$title)
    <div class="mb-2">
        <span class="text-dark">{{ $title }}</span>
        @if(isset($column_name))
            <span class="form-control">{{ $model->translate($lang)->$column_name ?? '-' }}</span>
        @else
            <span class="form-control">{{ $model->translate($lang)->name ?? '-' }}</span>
        @endif
    </div>
@endforeach
