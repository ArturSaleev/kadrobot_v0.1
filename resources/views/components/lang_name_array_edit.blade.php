@if(empty($noCol))
<div class="col-lg-6 col-12 col-sm-12">
@endif
    <div class="card">
        <div class="card-body">
            <div>
                @if(isset($title))
                    <label class="card-title">@lang($title)</label>
                @else
                    <label class="card-title">@lang('crud.names')</label>
                @endif
                <br/>
            </div>
            <div>
                @foreach(config('panel.available_languages', []) as $lang=>$title)
                    <div class="form-group mt-2">
                        <span class="text-dark">{{ $title }}</span>
                        <input type="text"
                               class="form-control"
                               name="{{ $name ?? 'name' }}[{{ $lang }}]"
                               value="{{ ($editing) ? $model->translate($lang)->name ?? '' : '' }}"
                               placeholder="{{ $title }}"
                               maxlength="255"
                               required
                        />
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@if(empty($noCol))
</div>
@endif
