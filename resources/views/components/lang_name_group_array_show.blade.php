@foreach(config('panel.available_languages', []) as $lang=>$title)
    <div class="input-group">
        <div class="input-group-append">
            <span class="input-group-text" style="min-width: 7em">
                {{ $title }}
            </span>
        </div>
        <span class="form-control">{{ $model->translate($lang)->name ?? '-' }}</span>
    </div>
@endforeach
