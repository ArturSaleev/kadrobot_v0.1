<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Hash;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // Adding an admin user

        $user = \App\Models\User::factory()
            ->count(1)
            ->create([
                'email' => 'admin@admin.com',
                'password' => Hash::make('admin'),
            ]);
        $this->call(PermissionsSeeder::class);

        $this->call(RefStatusSeeder::class);
        $this->call(RefTypePhoneSeeder::class);
        $this->call(RefSexSeeder::class);
        $this->call(RefDocPlaceSeeder::class);
        $this->call(RefDocTypeSeeder::class);
        $this->call(RefNationalitySeeder::class);
        $this->call(RefAddressTypeSeeder::class);
        $this->call(RefFamilyStateSeeder::class);
        $this->call(RefAccountTypeSeeder::class);
        $this->call(RefUserStatusSeeder::class);
        $this->call(RefOkedSeeder::class);

        $this->call(CompanySeeder::class);
        $this->call(BranchSeeder::class);
        $this->call(PositionSeeder::class);

    }
}
