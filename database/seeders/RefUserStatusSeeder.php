<?php

namespace Database\Seeders;

use App\Models\RefUserStatus;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RefUserStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            "ru" => [
                "Активный",
                "Не активный"
            ],
            "kz" => [
                "Белсенді",
                "Белсенді емес"
            ],
        ];

        foreach($data["ru"] as $key=>$name){
            $refUserStatus = new RefUserStatus();
            $refUserStatus->translateOrNew("ru")->name = $name;
            if(isset($data["kz"][$key])){
                $refUserStatus->translateOrNew("kz")->name = $data["kz"][$key];
            }
            $refUserStatus->save();
        }
    }
}
