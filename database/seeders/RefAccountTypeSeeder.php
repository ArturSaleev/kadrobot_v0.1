<?php

namespace Database\Seeders;

use App\Models\RefAccountType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RefAccountTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            "ru" => [
                "Не выбран",
                "Банковская карта",
                "Лицевой счет",
                "Наличный расчет",
            ],
            "kz" => [
                "Таңдалмаған",
                "Банк картасы",
                "Дербес шот",
                "Қолма-қол ақша",
            ]
        ];

        foreach($data["ru"] as $key=>$name){
            $refAccountType = new RefAccountType();
            $refAccountType->translateOrNew("ru")->name = $name;
            if(isset($data["kz"][$key])){
                $refAccountType->translateOrNew("kz")->name = $data["kz"][$key];
            }
            $refAccountType->save();
        }
    }
}
