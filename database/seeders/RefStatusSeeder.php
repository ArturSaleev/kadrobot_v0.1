<?php

namespace Database\Seeders;

use App\Models\RefStatus;
use App\Traits\LocaleTrait;
use Illuminate\Database\Seeder;

class RefStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $refStatus = new RefStatus();
        foreach(LocaleTrait::getAllLanguage() as $lang){
            $name = LocaleTrait::getVariableCode($lang['code'], 'active');
            $refStatus->translateOrNew($lang['code'])->name = $name;
        }
        $refStatus->save();

        $refStatus = new RefStatus();
        foreach(LocaleTrait::getAllLanguage() as $lang){
            $name = LocaleTrait::getVariableCode($lang['code'], 'no_active');
            $refStatus->translateOrNew($lang['code'])->name = $name;
        }
        $refStatus->save();
    }
}
