<?php

namespace Database\Seeders;

use App\Models\RefDocType;
use App\Traits\LocaleTrait;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RefDocTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $items = [
            "Паспорт РК",
            "Удостоверение РК",
            "Вид на жительство РК",
            "Свидетельство о рождении",
            "Лицо без гражданства РК",
            "Иной документ"
        ];

        foreach($items as $item){
            $refDocType = new RefDocType();
            foreach (LocaleTrait::getAllLanguage() as $lang) {
                $refDocType->translateOrNew($lang['code'])->name = $item;
            }
            $refDocType->save();
        }
    }
}
