<?php

namespace Database\Seeders;

use App\Models\EmployeeFamily;
use Illuminate\Database\Seeder;

class EmployeeFamilySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EmployeeFamily::factory()
            ->count(5)
            ->create();
    }
}
