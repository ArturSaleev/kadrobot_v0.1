<?php

namespace Database\Seeders;

use App\Models\RefSex;
use Illuminate\Database\Seeder;

class RefSexSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefSex::factory()
            ->count(5)
            ->create();
    }
}
