<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RefSexTranslation;

class RefSexTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefSexTranslation::factory()
            ->count(5)
            ->create();
    }
}
