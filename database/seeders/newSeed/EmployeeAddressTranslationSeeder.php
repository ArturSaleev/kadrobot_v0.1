<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\EmployeeAddressTranslation;

class EmployeeAddressTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EmployeeAddressTranslation::factory()
            ->count(5)
            ->create();
    }
}
