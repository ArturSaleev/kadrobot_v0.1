<?php

namespace Database\Seeders;

use App\Models\RefTypePhone;
use Illuminate\Database\Seeder;

class RefTypePhoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefTypePhone::factory()
            ->count(5)
            ->create();
    }
}
