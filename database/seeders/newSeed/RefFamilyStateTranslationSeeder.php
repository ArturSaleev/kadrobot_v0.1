<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RefFamilyStateTranslation;

class RefFamilyStateTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefFamilyStateTranslation::factory()
            ->count(5)
            ->create();
    }
}
