<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RefPersonStateTranslation;

class RefPersonStateTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefPersonStateTranslation::factory()
            ->count(5)
            ->create();
    }
}
