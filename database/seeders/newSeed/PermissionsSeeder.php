<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\PermissionRegistrar;

class PermissionsSeeder extends Seeder
{
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // Create default permissions
        Permission::create(['name' => 'list branches']);
        Permission::create(['name' => 'view branches']);
        Permission::create(['name' => 'create branches']);
        Permission::create(['name' => 'update branches']);
        Permission::create(['name' => 'delete branches']);

        Permission::create(['name' => 'list allbranchaddreses']);
        Permission::create(['name' => 'view allbranchaddreses']);
        Permission::create(['name' => 'create allbranchaddreses']);
        Permission::create(['name' => 'update allbranchaddreses']);
        Permission::create(['name' => 'delete allbranchaddreses']);

        Permission::create(['name' => 'list branchaddresestranslations']);
        Permission::create(['name' => 'view branchaddresestranslations']);
        Permission::create(['name' => 'create branchaddresestranslations']);
        Permission::create(['name' => 'update branchaddresestranslations']);
        Permission::create(['name' => 'delete branchaddresestranslations']);

        Permission::create(['name' => 'list branchphones']);
        Permission::create(['name' => 'view branchphones']);
        Permission::create(['name' => 'create branchphones']);
        Permission::create(['name' => 'update branchphones']);
        Permission::create(['name' => 'delete branchphones']);

        Permission::create(['name' => 'list branchtranslations']);
        Permission::create(['name' => 'view branchtranslations']);
        Permission::create(['name' => 'create branchtranslations']);
        Permission::create(['name' => 'update branchtranslations']);
        Permission::create(['name' => 'delete branchtranslations']);

        Permission::create(['name' => 'list companies']);
        Permission::create(['name' => 'view companies']);
        Permission::create(['name' => 'create companies']);
        Permission::create(['name' => 'update companies']);
        Permission::create(['name' => 'delete companies']);

        Permission::create(['name' => 'list companytranslations']);
        Permission::create(['name' => 'view companytranslations']);
        Permission::create(['name' => 'create companytranslations']);
        Permission::create(['name' => 'update companytranslations']);
        Permission::create(['name' => 'delete companytranslations']);

        Permission::create(['name' => 'list curators']);
        Permission::create(['name' => 'view curators']);
        Permission::create(['name' => 'create curators']);
        Permission::create(['name' => 'update curators']);
        Permission::create(['name' => 'delete curators']);

        Permission::create(['name' => 'list departments']);
        Permission::create(['name' => 'view departments']);
        Permission::create(['name' => 'create departments']);
        Permission::create(['name' => 'update departments']);
        Permission::create(['name' => 'delete departments']);

        Permission::create(['name' => 'list departmentphones']);
        Permission::create(['name' => 'view departmentphones']);
        Permission::create(['name' => 'create departmentphones']);
        Permission::create(['name' => 'update departmentphones']);
        Permission::create(['name' => 'delete departmentphones']);

        Permission::create(['name' => 'list departmenttranslations']);
        Permission::create(['name' => 'view departmenttranslations']);
        Permission::create(['name' => 'create departmenttranslations']);
        Permission::create(['name' => 'update departmenttranslations']);
        Permission::create(['name' => 'delete departmenttranslations']);

        Permission::create(['name' => 'list employees']);
        Permission::create(['name' => 'view employees']);
        Permission::create(['name' => 'create employees']);
        Permission::create(['name' => 'update employees']);
        Permission::create(['name' => 'delete employees']);

        Permission::create(['name' => 'list employeeaddresses']);
        Permission::create(['name' => 'view employeeaddresses']);
        Permission::create(['name' => 'create employeeaddresses']);
        Permission::create(['name' => 'update employeeaddresses']);
        Permission::create(['name' => 'delete employeeaddresses']);

        Permission::create(['name' => 'list employeeaddresstranslations']);
        Permission::create(['name' => 'view employeeaddresstranslations']);
        Permission::create(['name' => 'create employeeaddresstranslations']);
        Permission::create(['name' => 'update employeeaddresstranslations']);
        Permission::create(['name' => 'delete employeeaddresstranslations']);

        Permission::create(['name' => 'list employeedeclensions']);
        Permission::create(['name' => 'view employeedeclensions']);
        Permission::create(['name' => 'create employeedeclensions']);
        Permission::create(['name' => 'update employeedeclensions']);
        Permission::create(['name' => 'delete employeedeclensions']);

        Permission::create(['name' => 'list employeedocuments']);
        Permission::create(['name' => 'view employeedocuments']);
        Permission::create(['name' => 'create employeedocuments']);
        Permission::create(['name' => 'update employeedocuments']);
        Permission::create(['name' => 'delete employeedocuments']);

        Permission::create(['name' => 'list employeeeducations']);
        Permission::create(['name' => 'view employeeeducations']);
        Permission::create(['name' => 'create employeeeducations']);
        Permission::create(['name' => 'update employeeeducations']);
        Permission::create(['name' => 'delete employeeeducations']);

        Permission::create(['name' => 'list employeefamilies']);
        Permission::create(['name' => 'view employeefamilies']);
        Permission::create(['name' => 'create employeefamilies']);
        Permission::create(['name' => 'update employeefamilies']);
        Permission::create(['name' => 'delete employeefamilies']);

        Permission::create(['name' => 'list employeeholidays']);
        Permission::create(['name' => 'view employeeholidays']);
        Permission::create(['name' => 'create employeeholidays']);
        Permission::create(['name' => 'update employeeholidays']);
        Permission::create(['name' => 'delete employeeholidays']);

        Permission::create(['name' => 'list employeeholidaysperiods']);
        Permission::create(['name' => 'view employeeholidaysperiods']);
        Permission::create(['name' => 'create employeeholidaysperiods']);
        Permission::create(['name' => 'update employeeholidaysperiods']);
        Permission::create(['name' => 'delete employeeholidaysperiods']);

        Permission::create(['name' => 'list employeehospitals']);
        Permission::create(['name' => 'view employeehospitals']);
        Permission::create(['name' => 'create employeehospitals']);
        Permission::create(['name' => 'update employeehospitals']);
        Permission::create(['name' => 'delete employeehospitals']);

        Permission::create(['name' => 'list employeeinvalides']);
        Permission::create(['name' => 'view employeeinvalides']);
        Permission::create(['name' => 'create employeeinvalides']);
        Permission::create(['name' => 'update employeeinvalides']);
        Permission::create(['name' => 'delete employeeinvalides']);

        Permission::create(['name' => 'list employeemilitaries']);
        Permission::create(['name' => 'view employeemilitaries']);
        Permission::create(['name' => 'create employeemilitaries']);
        Permission::create(['name' => 'update employeemilitaries']);
        Permission::create(['name' => 'delete employeemilitaries']);

        Permission::create(['name' => 'list employeepayallowances']);
        Permission::create(['name' => 'view employeepayallowances']);
        Permission::create(['name' => 'create employeepayallowances']);
        Permission::create(['name' => 'update employeepayallowances']);
        Permission::create(['name' => 'delete employeepayallowances']);

        Permission::create(['name' => 'list employeephones']);
        Permission::create(['name' => 'view employeephones']);
        Permission::create(['name' => 'create employeephones']);
        Permission::create(['name' => 'update employeephones']);
        Permission::create(['name' => 'delete employeephones']);

        Permission::create(['name' => 'list employeestazhs']);
        Permission::create(['name' => 'view employeestazhs']);
        Permission::create(['name' => 'create employeestazhs']);
        Permission::create(['name' => 'update employeestazhs']);
        Permission::create(['name' => 'delete employeestazhs']);

        Permission::create(['name' => 'list employeestazhtranslations']);
        Permission::create(['name' => 'view employeestazhtranslations']);
        Permission::create(['name' => 'create employeestazhtranslations']);
        Permission::create(['name' => 'update employeestazhtranslations']);
        Permission::create(['name' => 'delete employeestazhtranslations']);

        Permission::create(['name' => 'list employeetechics']);
        Permission::create(['name' => 'view employeetechics']);
        Permission::create(['name' => 'create employeetechics']);
        Permission::create(['name' => 'update employeetechics']);
        Permission::create(['name' => 'delete employeetechics']);

        Permission::create(['name' => 'list employeetrips']);
        Permission::create(['name' => 'view employeetrips']);
        Permission::create(['name' => 'create employeetrips']);
        Permission::create(['name' => 'update employeetrips']);
        Permission::create(['name' => 'delete employeetrips']);

        Permission::create(['name' => 'list employeetripfromtos']);
        Permission::create(['name' => 'view employeetripfromtos']);
        Permission::create(['name' => 'create employeetripfromtos']);
        Permission::create(['name' => 'update employeetripfromtos']);
        Permission::create(['name' => 'delete employeetripfromtos']);

        Permission::create(['name' => 'list employeetriptranslations']);
        Permission::create(['name' => 'view employeetriptranslations']);
        Permission::create(['name' => 'create employeetriptranslations']);
        Permission::create(['name' => 'update employeetriptranslations']);
        Permission::create(['name' => 'delete employeetriptranslations']);

        Permission::create(['name' => 'list holidays']);
        Permission::create(['name' => 'view holidays']);
        Permission::create(['name' => 'create holidays']);
        Permission::create(['name' => 'update holidays']);
        Permission::create(['name' => 'delete holidays']);

        Permission::create(['name' => 'list positions']);
        Permission::create(['name' => 'view positions']);
        Permission::create(['name' => 'create positions']);
        Permission::create(['name' => 'update positions']);
        Permission::create(['name' => 'delete positions']);

        Permission::create(['name' => 'list positiondeclensions']);
        Permission::create(['name' => 'view positiondeclensions']);
        Permission::create(['name' => 'create positiondeclensions']);
        Permission::create(['name' => 'update positiondeclensions']);
        Permission::create(['name' => 'delete positiondeclensions']);

        Permission::create(['name' => 'list positiontranslations']);
        Permission::create(['name' => 'view positiontranslations']);
        Permission::create(['name' => 'create positiontranslations']);
        Permission::create(['name' => 'update positiontranslations']);
        Permission::create(['name' => 'delete positiontranslations']);

        Permission::create(['name' => 'list refaccounttypes']);
        Permission::create(['name' => 'view refaccounttypes']);
        Permission::create(['name' => 'create refaccounttypes']);
        Permission::create(['name' => 'update refaccounttypes']);
        Permission::create(['name' => 'delete refaccounttypes']);

        Permission::create(['name' => 'list refaccounttypetranslations']);
        Permission::create(['name' => 'view refaccounttypetranslations']);
        Permission::create(['name' => 'create refaccounttypetranslations']);
        Permission::create(['name' => 'update refaccounttypetranslations']);
        Permission::create(['name' => 'delete refaccounttypetranslations']);

        Permission::create(['name' => 'list refactions']);
        Permission::create(['name' => 'view refactions']);
        Permission::create(['name' => 'create refactions']);
        Permission::create(['name' => 'update refactions']);
        Permission::create(['name' => 'delete refactions']);

        Permission::create(['name' => 'list refactiontranslations']);
        Permission::create(['name' => 'view refactiontranslations']);
        Permission::create(['name' => 'create refactiontranslations']);
        Permission::create(['name' => 'update refactiontranslations']);
        Permission::create(['name' => 'delete refactiontranslations']);

        Permission::create(['name' => 'list refaddresstypes']);
        Permission::create(['name' => 'view refaddresstypes']);
        Permission::create(['name' => 'create refaddresstypes']);
        Permission::create(['name' => 'update refaddresstypes']);
        Permission::create(['name' => 'delete refaddresstypes']);

        Permission::create(['name' => 'list refaddresstypetranslations']);
        Permission::create(['name' => 'view refaddresstypetranslations']);
        Permission::create(['name' => 'create refaddresstypetranslations']);
        Permission::create(['name' => 'update refaddresstypetranslations']);
        Permission::create(['name' => 'delete refaddresstypetranslations']);

        Permission::create(['name' => 'list refallowancetypes']);
        Permission::create(['name' => 'view refallowancetypes']);
        Permission::create(['name' => 'create refallowancetypes']);
        Permission::create(['name' => 'update refallowancetypes']);
        Permission::create(['name' => 'delete refallowancetypes']);

        Permission::create(['name' => 'list refallowancetypetranslations']);
        Permission::create(['name' => 'view refallowancetypetranslations']);
        Permission::create(['name' => 'create refallowancetypetranslations']);
        Permission::create(['name' => 'update refallowancetypetranslations']);
        Permission::create(['name' => 'delete refallowancetypetranslations']);

        Permission::create(['name' => 'list refbanks']);
        Permission::create(['name' => 'view refbanks']);
        Permission::create(['name' => 'create refbanks']);
        Permission::create(['name' => 'update refbanks']);
        Permission::create(['name' => 'delete refbanks']);

        Permission::create(['name' => 'list refbanktranslations']);
        Permission::create(['name' => 'view refbanktranslations']);
        Permission::create(['name' => 'create refbanktranslations']);
        Permission::create(['name' => 'update refbanktranslations']);
        Permission::create(['name' => 'delete refbanktranslations']);

        Permission::create(['name' => 'list refcountries']);
        Permission::create(['name' => 'view refcountries']);
        Permission::create(['name' => 'create refcountries']);
        Permission::create(['name' => 'update refcountries']);
        Permission::create(['name' => 'delete refcountries']);

        Permission::create(['name' => 'list refcountrytranslations']);
        Permission::create(['name' => 'view refcountrytranslations']);
        Permission::create(['name' => 'create refcountrytranslations']);
        Permission::create(['name' => 'update refcountrytranslations']);
        Permission::create(['name' => 'delete refcountrytranslations']);

        Permission::create(['name' => 'list refdocplaces']);
        Permission::create(['name' => 'view refdocplaces']);
        Permission::create(['name' => 'create refdocplaces']);
        Permission::create(['name' => 'update refdocplaces']);
        Permission::create(['name' => 'delete refdocplaces']);

        Permission::create(['name' => 'list refdocplacetranslations']);
        Permission::create(['name' => 'view refdocplacetranslations']);
        Permission::create(['name' => 'create refdocplacetranslations']);
        Permission::create(['name' => 'update refdocplacetranslations']);
        Permission::create(['name' => 'delete refdocplacetranslations']);

        Permission::create(['name' => 'list refdoctypes']);
        Permission::create(['name' => 'view refdoctypes']);
        Permission::create(['name' => 'create refdoctypes']);
        Permission::create(['name' => 'update refdoctypes']);
        Permission::create(['name' => 'delete refdoctypes']);

        Permission::create(['name' => 'list refdoctypetranslations']);
        Permission::create(['name' => 'view refdoctypetranslations']);
        Permission::create(['name' => 'create refdoctypetranslations']);
        Permission::create(['name' => 'update refdoctypetranslations']);
        Permission::create(['name' => 'delete refdoctypetranslations']);

        Permission::create(['name' => 'list reffamilystates']);
        Permission::create(['name' => 'view reffamilystates']);
        Permission::create(['name' => 'create reffamilystates']);
        Permission::create(['name' => 'update reffamilystates']);
        Permission::create(['name' => 'delete reffamilystates']);

        Permission::create(['name' => 'list reffamilystatetranslations']);
        Permission::create(['name' => 'view reffamilystatetranslations']);
        Permission::create(['name' => 'create reffamilystatetranslations']);
        Permission::create(['name' => 'update reffamilystatetranslations']);
        Permission::create(['name' => 'delete reffamilystatetranslations']);

        Permission::create(['name' => 'list refkindholidays']);
        Permission::create(['name' => 'view refkindholidays']);
        Permission::create(['name' => 'create refkindholidays']);
        Permission::create(['name' => 'update refkindholidays']);
        Permission::create(['name' => 'delete refkindholidays']);

        Permission::create(['name' => 'list refkindholidaytranslations']);
        Permission::create(['name' => 'view refkindholidaytranslations']);
        Permission::create(['name' => 'create refkindholidaytranslations']);
        Permission::create(['name' => 'update refkindholidaytranslations']);
        Permission::create(['name' => 'delete refkindholidaytranslations']);

        Permission::create(['name' => 'list refmetas']);
        Permission::create(['name' => 'view refmetas']);
        Permission::create(['name' => 'create refmetas']);
        Permission::create(['name' => 'update refmetas']);
        Permission::create(['name' => 'delete refmetas']);

        Permission::create(['name' => 'list refnationalities']);
        Permission::create(['name' => 'view refnationalities']);
        Permission::create(['name' => 'create refnationalities']);
        Permission::create(['name' => 'update refnationalities']);
        Permission::create(['name' => 'delete refnationalities']);

        Permission::create(['name' => 'list refnationalitytranslations']);
        Permission::create(['name' => 'view refnationalitytranslations']);
        Permission::create(['name' => 'create refnationalitytranslations']);
        Permission::create(['name' => 'update refnationalitytranslations']);
        Permission::create(['name' => 'delete refnationalitytranslations']);

        Permission::create(['name' => 'list refokeds']);
        Permission::create(['name' => 'view refokeds']);
        Permission::create(['name' => 'create refokeds']);
        Permission::create(['name' => 'update refokeds']);
        Permission::create(['name' => 'delete refokeds']);

        Permission::create(['name' => 'list refokedtranslations']);
        Permission::create(['name' => 'view refokedtranslations']);
        Permission::create(['name' => 'create refokedtranslations']);
        Permission::create(['name' => 'update refokedtranslations']);
        Permission::create(['name' => 'delete refokedtranslations']);

        Permission::create(['name' => 'list refpersonstates']);
        Permission::create(['name' => 'view refpersonstates']);
        Permission::create(['name' => 'create refpersonstates']);
        Permission::create(['name' => 'update refpersonstates']);
        Permission::create(['name' => 'delete refpersonstates']);

        Permission::create(['name' => 'list refpersonstatetranslations']);
        Permission::create(['name' => 'view refpersonstatetranslations']);
        Permission::create(['name' => 'create refpersonstatetranslations']);
        Permission::create(['name' => 'update refpersonstatetranslations']);
        Permission::create(['name' => 'delete refpersonstatetranslations']);

        Permission::create(['name' => 'list refsexes']);
        Permission::create(['name' => 'view refsexes']);
        Permission::create(['name' => 'create refsexes']);
        Permission::create(['name' => 'update refsexes']);
        Permission::create(['name' => 'delete refsexes']);

        Permission::create(['name' => 'list refsextranslations']);
        Permission::create(['name' => 'view refsextranslations']);
        Permission::create(['name' => 'create refsextranslations']);
        Permission::create(['name' => 'update refsextranslations']);
        Permission::create(['name' => 'delete refsextranslations']);

        Permission::create(['name' => 'list refstatuses']);
        Permission::create(['name' => 'view refstatuses']);
        Permission::create(['name' => 'create refstatuses']);
        Permission::create(['name' => 'update refstatuses']);
        Permission::create(['name' => 'delete refstatuses']);

        Permission::create(['name' => 'list refstatustranslations']);
        Permission::create(['name' => 'view refstatustranslations']);
        Permission::create(['name' => 'create refstatustranslations']);
        Permission::create(['name' => 'update refstatustranslations']);
        Permission::create(['name' => 'delete refstatustranslations']);

        Permission::create(['name' => 'list reftechnictypes']);
        Permission::create(['name' => 'view reftechnictypes']);
        Permission::create(['name' => 'create reftechnictypes']);
        Permission::create(['name' => 'update reftechnictypes']);
        Permission::create(['name' => 'delete reftechnictypes']);

        Permission::create(['name' => 'list reftechnictypetranslations']);
        Permission::create(['name' => 'view reftechnictypetranslations']);
        Permission::create(['name' => 'create reftechnictypetranslations']);
        Permission::create(['name' => 'update reftechnictypetranslations']);
        Permission::create(['name' => 'delete reftechnictypetranslations']);

        Permission::create(['name' => 'list reftransporttrips']);
        Permission::create(['name' => 'view reftransporttrips']);
        Permission::create(['name' => 'create reftransporttrips']);
        Permission::create(['name' => 'update reftransporttrips']);
        Permission::create(['name' => 'delete reftransporttrips']);

        Permission::create(['name' => 'list reftransporttriptranstions']);
        Permission::create(['name' => 'view reftransporttriptranstions']);
        Permission::create(['name' => 'create reftransporttriptranstions']);
        Permission::create(['name' => 'update reftransporttriptranstions']);
        Permission::create(['name' => 'delete reftransporttriptranstions']);

        Permission::create(['name' => 'list reftypephones']);
        Permission::create(['name' => 'view reftypephones']);
        Permission::create(['name' => 'create reftypephones']);
        Permission::create(['name' => 'update reftypephones']);
        Permission::create(['name' => 'delete reftypephones']);

        Permission::create(['name' => 'list reftypephonetranslations']);
        Permission::create(['name' => 'view reftypephonetranslations']);
        Permission::create(['name' => 'create reftypephonetranslations']);
        Permission::create(['name' => 'update reftypephonetranslations']);
        Permission::create(['name' => 'delete reftypephonetranslations']);

        Permission::create(['name' => 'list reftyperodstvs']);
        Permission::create(['name' => 'view reftyperodstvs']);
        Permission::create(['name' => 'create reftyperodstvs']);
        Permission::create(['name' => 'update reftyperodstvs']);
        Permission::create(['name' => 'delete reftyperodstvs']);

        Permission::create(['name' => 'list reftyperodstvtranslations']);
        Permission::create(['name' => 'view reftyperodstvtranslations']);
        Permission::create(['name' => 'create reftyperodstvtranslations']);
        Permission::create(['name' => 'update reftyperodstvtranslations']);
        Permission::create(['name' => 'delete reftyperodstvtranslations']);

        Permission::create(['name' => 'list refuserstatuses']);
        Permission::create(['name' => 'view refuserstatuses']);
        Permission::create(['name' => 'create refuserstatuses']);
        Permission::create(['name' => 'update refuserstatuses']);
        Permission::create(['name' => 'delete refuserstatuses']);

        Permission::create(['name' => 'list refuserstatustranslations']);
        Permission::create(['name' => 'view refuserstatustranslations']);
        Permission::create(['name' => 'create refuserstatustranslations']);
        Permission::create(['name' => 'update refuserstatustranslations']);
        Permission::create(['name' => 'delete refuserstatustranslations']);

        Permission::create(['name' => 'list refvidholidays']);
        Permission::create(['name' => 'view refvidholidays']);
        Permission::create(['name' => 'create refvidholidays']);
        Permission::create(['name' => 'update refvidholidays']);
        Permission::create(['name' => 'delete refvidholidays']);

        Permission::create(['name' => 'list refvidholidaytranslations']);
        Permission::create(['name' => 'view refvidholidaytranslations']);
        Permission::create(['name' => 'create refvidholidaytranslations']);
        Permission::create(['name' => 'update refvidholidaytranslations']);
        Permission::create(['name' => 'delete refvidholidaytranslations']);

        Permission::create(['name' => 'list refvidholidaytypes']);
        Permission::create(['name' => 'view refvidholidaytypes']);
        Permission::create(['name' => 'create refvidholidaytypes']);
        Permission::create(['name' => 'update refvidholidaytypes']);
        Permission::create(['name' => 'delete refvidholidaytypes']);

        Permission::create(['name' => 'list refvidholidaytypetranslations']);
        Permission::create(['name' => 'view refvidholidaytypetranslations']);
        Permission::create(['name' => 'create refvidholidaytypetranslations']);
        Permission::create(['name' => 'update refvidholidaytypetranslations']);
        Permission::create(['name' => 'delete refvidholidaytypetranslations']);

        Permission::create(['name' => 'list reporthtmls']);
        Permission::create(['name' => 'view reporthtmls']);
        Permission::create(['name' => 'create reporthtmls']);
        Permission::create(['name' => 'update reporthtmls']);
        Permission::create(['name' => 'delete reporthtmls']);

        Permission::create(['name' => 'list reporthtmlothers']);
        Permission::create(['name' => 'view reporthtmlothers']);
        Permission::create(['name' => 'create reporthtmlothers']);
        Permission::create(['name' => 'update reporthtmlothers']);
        Permission::create(['name' => 'delete reporthtmlothers']);

        Permission::create(['name' => 'list t2cards']);
        Permission::create(['name' => 'view t2cards']);
        Permission::create(['name' => 'create t2cards']);
        Permission::create(['name' => 'update t2cards']);
        Permission::create(['name' => 'delete t2cards']);

        // Create user role and assign existing permissions
        $currentPermissions = Permission::all();
        $userRole = Role::create(['name' => 'user']);
        $userRole->givePermissionTo($currentPermissions);

        // Create admin exclusive permissions
        Permission::create(['name' => 'list roles']);
        Permission::create(['name' => 'view roles']);
        Permission::create(['name' => 'create roles']);
        Permission::create(['name' => 'update roles']);
        Permission::create(['name' => 'delete roles']);

        Permission::create(['name' => 'list permissions']);
        Permission::create(['name' => 'view permissions']);
        Permission::create(['name' => 'create permissions']);
        Permission::create(['name' => 'update permissions']);
        Permission::create(['name' => 'delete permissions']);

        Permission::create(['name' => 'list users']);
        Permission::create(['name' => 'view users']);
        Permission::create(['name' => 'create users']);
        Permission::create(['name' => 'update users']);
        Permission::create(['name' => 'delete users']);

        // Create admin role and assign all permissions
        $allPermissions = Permission::all();
        $adminRole = Role::create(['name' => 'super-admin']);
        $adminRole->givePermissionTo($allPermissions);

        $user = \App\Models\User::whereEmail('admin@admin.com')->first();

        if ($user) {
            $user->assignRole($adminRole);
        }
    }
}
