<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RefTypeRodstvTranslation;

class RefTypeRodstvTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefTypeRodstvTranslation::factory()
            ->count(5)
            ->create();
    }
}
