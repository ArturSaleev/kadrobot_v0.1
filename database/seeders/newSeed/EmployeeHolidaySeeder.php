<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\EmployeeHoliday;

class EmployeeHolidaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EmployeeHoliday::factory()
            ->count(5)
            ->create();
    }
}
