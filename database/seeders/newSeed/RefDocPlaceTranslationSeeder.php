<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RefDocPlaceTranslation;

class RefDocPlaceTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefDocPlaceTranslation::factory()
            ->count(5)
            ->create();
    }
}
