<?php

namespace Database\Seeders;

use App\Models\RefMeta;
use Illuminate\Database\Seeder;

class RefMetaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefMeta::factory()
            ->count(5)
            ->create();
    }
}
