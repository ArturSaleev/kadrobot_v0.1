<?php

namespace Database\Seeders;

use App\Models\RefPersonState;
use Illuminate\Database\Seeder;

class RefPersonStateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefPersonState::factory()
            ->count(5)
            ->create();
    }
}
