<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RefCountryTranslation;

class RefCountryTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefCountryTranslation::factory()
            ->count(5)
            ->create();
    }
}
