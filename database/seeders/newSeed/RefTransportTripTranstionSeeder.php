<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RefTransportTripTranstion;

class RefTransportTripTranstionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefTransportTripTranstion::factory()
            ->count(5)
            ->create();
    }
}
