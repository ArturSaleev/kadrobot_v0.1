<?php

namespace Database\Seeders;

use App\Models\RefAddressType;
use Illuminate\Database\Seeder;

class RefAddressTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefAddressType::factory()
            ->count(5)
            ->create();
    }
}
