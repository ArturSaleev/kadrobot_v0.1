<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\EmployeeInvalide;

class EmployeeInvalideSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EmployeeInvalide::factory()
            ->count(5)
            ->create();
    }
}
