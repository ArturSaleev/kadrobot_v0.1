<?php

namespace Database\Seeders;

use App\Models\EmployeeTrip;
use Illuminate\Database\Seeder;

class EmployeeTripSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EmployeeTrip::factory()
            ->count(5)
            ->create();
    }
}
