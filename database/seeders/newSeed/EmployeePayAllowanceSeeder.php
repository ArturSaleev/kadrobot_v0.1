<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\EmployeePayAllowance;

class EmployeePayAllowanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EmployeePayAllowance::factory()
            ->count(5)
            ->create();
    }
}
