<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CompanyTranslation;

class CompanyTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CompanyTranslation::factory()
            ->count(5)
            ->create();
    }
}
