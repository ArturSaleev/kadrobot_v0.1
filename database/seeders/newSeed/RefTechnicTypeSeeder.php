<?php

namespace Database\Seeders;

use App\Models\RefTechnicType;
use Illuminate\Database\Seeder;

class RefTechnicTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefTechnicType::factory()
            ->count(5)
            ->create();
    }
}
