<?php

namespace Database\Seeders;

use App\Models\EmployeePhone;
use Illuminate\Database\Seeder;

class EmployeePhoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EmployeePhone::factory()
            ->count(5)
            ->create();
    }
}
