<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RefAccountTypeTranslation;

class RefAccountTypeTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefAccountTypeTranslation::factory()
            ->count(5)
            ->create();
    }
}
