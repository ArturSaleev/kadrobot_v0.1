<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RefOkedTranslation;

class RefOkedTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefOkedTranslation::factory()
            ->count(5)
            ->create();
    }
}
