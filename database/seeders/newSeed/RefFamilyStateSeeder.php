<?php

namespace Database\Seeders;

use App\Models\RefFamilyState;
use Illuminate\Database\Seeder;

class RefFamilyStateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefFamilyState::factory()
            ->count(5)
            ->create();
    }
}
