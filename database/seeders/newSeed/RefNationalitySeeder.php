<?php

namespace Database\Seeders;

use App\Models\RefNationality;
use Illuminate\Database\Seeder;

class RefNationalitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefNationality::factory()
            ->count(5)
            ->create();
    }
}
