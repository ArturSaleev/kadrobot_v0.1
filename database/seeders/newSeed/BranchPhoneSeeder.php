<?php

namespace Database\Seeders;

use App\Models\BranchPhone;
use Illuminate\Database\Seeder;

class BranchPhoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BranchPhone::factory()
            ->count(5)
            ->create();
    }
}
