<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RefNationalityTranslation;

class RefNationalityTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefNationalityTranslation::factory()
            ->count(5)
            ->create();
    }
}
