<?php

namespace Database\Seeders;

use App\Models\RefVidHoliday;
use Illuminate\Database\Seeder;

class RefVidHolidaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefVidHoliday::factory()
            ->count(5)
            ->create();
    }
}
