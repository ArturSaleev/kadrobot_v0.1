<?php

namespace Database\Seeders;

use App\Models\RefCountry;
use Illuminate\Database\Seeder;

class RefCountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefCountry::factory()
            ->count(5)
            ->create();
    }
}
