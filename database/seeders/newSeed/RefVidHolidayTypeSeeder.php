<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RefVidHolidayType;

class RefVidHolidayTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefVidHolidayType::factory()
            ->count(5)
            ->create();
    }
}
