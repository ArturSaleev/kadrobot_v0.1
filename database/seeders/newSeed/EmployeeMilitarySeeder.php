<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\EmployeeMilitary;

class EmployeeMilitarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EmployeeMilitary::factory()
            ->count(5)
            ->create();
    }
}
