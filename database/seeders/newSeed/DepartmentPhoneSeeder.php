<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\DepartmentPhone;

class DepartmentPhoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DepartmentPhone::factory()
            ->count(5)
            ->create();
    }
}
