<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RefUserStatusTranslation;

class RefUserStatusTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefUserStatusTranslation::factory()
            ->count(5)
            ->create();
    }
}
