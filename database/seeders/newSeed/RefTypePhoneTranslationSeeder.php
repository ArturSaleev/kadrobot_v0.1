<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RefTypePhoneTranslation;

class RefTypePhoneTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefTypePhoneTranslation::factory()
            ->count(5)
            ->create();
    }
}
