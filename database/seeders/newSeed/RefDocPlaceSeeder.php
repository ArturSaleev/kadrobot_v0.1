<?php

namespace Database\Seeders;

use App\Models\RefDocPlace;
use Illuminate\Database\Seeder;

class RefDocPlaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefDocPlace::factory()
            ->count(5)
            ->create();
    }
}
