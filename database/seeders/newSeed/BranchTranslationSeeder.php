<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\BranchTranslation;

class BranchTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BranchTranslation::factory()
            ->count(5)
            ->create();
    }
}
