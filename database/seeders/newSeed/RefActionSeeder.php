<?php

namespace Database\Seeders;

use App\Models\RefAction;
use Illuminate\Database\Seeder;

class RefActionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefAction::factory()
            ->count(5)
            ->create();
    }
}
