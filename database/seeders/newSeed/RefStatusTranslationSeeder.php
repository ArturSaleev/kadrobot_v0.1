<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RefStatusTranslation;

class RefStatusTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefStatusTranslation::factory()
            ->count(5)
            ->create();
    }
}
