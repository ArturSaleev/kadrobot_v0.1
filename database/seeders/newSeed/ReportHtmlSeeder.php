<?php

namespace Database\Seeders;

use App\Models\ReportHtml;
use Illuminate\Database\Seeder;

class ReportHtmlSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ReportHtml::factory()
            ->count(5)
            ->create();
    }
}
