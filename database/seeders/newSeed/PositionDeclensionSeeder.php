<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PositionDeclension;

class PositionDeclensionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PositionDeclension::factory()
            ->count(5)
            ->create();
    }
}
