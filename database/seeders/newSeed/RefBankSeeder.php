<?php

namespace Database\Seeders;

use App\Models\RefBank;
use Illuminate\Database\Seeder;

class RefBankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefBank::factory()
            ->count(5)
            ->create();
    }
}
