<?php

namespace Database\Seeders;

use App\Models\Curator;
use Illuminate\Database\Seeder;

class CuratorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Curator::factory()
            ->count(5)
            ->create();
    }
}
