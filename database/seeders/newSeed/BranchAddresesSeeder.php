<?php

namespace Database\Seeders;

use App\Models\BranchAddreses;
use Illuminate\Database\Seeder;

class BranchAddresesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BranchAddreses::factory()
            ->count(5)
            ->create();
    }
}
