<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Adding an admin user
        $user = \App\Models\User::factory()
            ->count(1)
            ->create([
                'email' => 'admin@admin.com',
                'password' => \Hash::make('admin'),
            ]);
        $this->call(PermissionsSeeder::class);

        // $this->call(BranchSeeder::class);
        // $this->call(BranchAddresesSeeder::class);
        // $this->call(BranchAddresesTranslationSeeder::class);
        // $this->call(BranchPhoneSeeder::class);
        // $this->call(BranchTranslationSeeder::class);
        // $this->call(CompanySeeder::class);
        // $this->call(CompanyTranslationSeeder::class);
        // $this->call(CuratorSeeder::class);
        // $this->call(DepartmentSeeder::class);
        // $this->call(DepartmentPhoneSeeder::class);
        // $this->call(DepartmentTranslationSeeder::class);
        // $this->call(EmployeeSeeder::class);
        // $this->call(EmployeeAddressSeeder::class);
        // $this->call(EmployeeAddressTranslationSeeder::class);
        // $this->call(EmployeeDeclensionSeeder::class);
        // $this->call(EmployeeDocumentSeeder::class);
        // $this->call(EmployeeEducationSeeder::class);
        // $this->call(EmployeeFamilySeeder::class);
        // $this->call(EmployeeHolidaySeeder::class);
        // $this->call(EmployeeHolidaysPeriodSeeder::class);
        // $this->call(EmployeeHospitalSeeder::class);
        // $this->call(EmployeeInvalideSeeder::class);
        // $this->call(EmployeeMilitarySeeder::class);
        // $this->call(EmployeePayAllowanceSeeder::class);
        // $this->call(EmployeePhoneSeeder::class);
        // $this->call(EmployeeStazhSeeder::class);
        // $this->call(EmployeeStazhTranslationSeeder::class);
        // $this->call(EmployeeTechicSeeder::class);
        // $this->call(EmployeeTripSeeder::class);
        // $this->call(EmployeeTripFromToSeeder::class);
        // $this->call(EmployeeTripTranslationSeeder::class);
        // $this->call(HolidaySeeder::class);
        // $this->call(PositionSeeder::class);
        // $this->call(PositionDeclensionSeeder::class);
        // $this->call(PositionTranslationSeeder::class);
        // $this->call(RefAccountTypeSeeder::class);
        // $this->call(RefAccountTypeTranslationSeeder::class);
        // $this->call(RefActionSeeder::class);
        // $this->call(RefActionTranslationSeeder::class);
        // $this->call(RefAddressTypeSeeder::class);
        // $this->call(RefAddressTypeTranslationSeeder::class);
        // $this->call(RefAllowanceTypeSeeder::class);
        // $this->call(RefAllowanceTypeTranslationSeeder::class);
        // $this->call(RefBankSeeder::class);
        // $this->call(RefBankTranslationSeeder::class);
        // $this->call(RefCountrySeeder::class);
        // $this->call(RefCountryTranslationSeeder::class);
        // $this->call(RefDocPlaceSeeder::class);
        // $this->call(RefDocPlaceTranslationSeeder::class);
        // $this->call(RefDocTypeSeeder::class);
        // $this->call(RefDocTypeTranslationSeeder::class);
        // $this->call(RefFamilyStateSeeder::class);
        // $this->call(RefFamilyStateTranslationSeeder::class);
        // $this->call(RefKindHolidaySeeder::class);
        // $this->call(RefKindHolidayTranslationSeeder::class);
        // $this->call(RefMetaSeeder::class);
        // $this->call(RefNationalitySeeder::class);
        // $this->call(RefNationalityTranslationSeeder::class);
        // $this->call(RefOkedSeeder::class);
        // $this->call(RefOkedTranslationSeeder::class);
        // $this->call(RefPersonStateSeeder::class);
        // $this->call(RefPersonStateTranslationSeeder::class);
        // $this->call(RefSexSeeder::class);
        // $this->call(RefSexTranslationSeeder::class);
        // $this->call(RefStatusSeeder::class);
        // $this->call(RefStatusTranslationSeeder::class);
        // $this->call(RefTechnicTypeSeeder::class);
        // $this->call(RefTechnicTypeTranslationSeeder::class);
        // $this->call(RefTransportTripSeeder::class);
        // $this->call(RefTransportTripTranstionSeeder::class);
        // $this->call(RefTypePhoneSeeder::class);
        // $this->call(RefTypePhoneTranslationSeeder::class);
        // $this->call(RefTypeRodstvSeeder::class);
        // $this->call(RefTypeRodstvTranslationSeeder::class);
        // $this->call(RefUserStatusSeeder::class);
        // $this->call(RefUserStatusTranslationSeeder::class);
        // $this->call(RefVidHolidaySeeder::class);
        // $this->call(RefVidHolidayTranslationSeeder::class);
        // $this->call(RefVidHolidayTypeSeeder::class);
        // $this->call(RefVidHolidayTypeTranslationSeeder::class);
        // $this->call(ReportHtmlSeeder::class);
        // $this->call(ReportHtmlOtherSeeder::class);
        // $this->call(T2CardSeeder::class);
        // $this->call(UserSeeder::class);
    }
}
