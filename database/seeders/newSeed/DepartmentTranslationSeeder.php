<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\DepartmentTranslation;

class DepartmentTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DepartmentTranslation::factory()
            ->count(5)
            ->create();
    }
}
