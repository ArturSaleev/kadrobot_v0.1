<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RefTransportTrip;

class RefTransportTripSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefTransportTrip::factory()
            ->count(5)
            ->create();
    }
}
