<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RefDocTypeTranslation;

class RefDocTypeTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefDocTypeTranslation::factory()
            ->count(5)
            ->create();
    }
}
