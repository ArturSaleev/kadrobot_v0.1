<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RefVidHolidayTranslation;

class RefVidHolidayTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefVidHolidayTranslation::factory()
            ->count(5)
            ->create();
    }
}
