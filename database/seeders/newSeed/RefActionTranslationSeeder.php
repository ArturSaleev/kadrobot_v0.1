<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RefActionTranslation;

class RefActionTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefActionTranslation::factory()
            ->count(5)
            ->create();
    }
}
