<?php

namespace Database\Seeders;

use App\Models\RefUserStatus;
use Illuminate\Database\Seeder;

class RefUserStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefUserStatus::factory()
            ->count(5)
            ->create();
    }
}
