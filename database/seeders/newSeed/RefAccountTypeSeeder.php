<?php

namespace Database\Seeders;

use App\Models\RefAccountType;
use Illuminate\Database\Seeder;

class RefAccountTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefAccountType::factory()
            ->count(5)
            ->create();
    }
}
