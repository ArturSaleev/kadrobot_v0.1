<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\BranchAddresesTranslation;

class BranchAddresesTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BranchAddresesTranslation::factory()
            ->count(5)
            ->create();
    }
}
