<?php

namespace Database\Seeders;

use App\Models\RefOked;
use Illuminate\Database\Seeder;

class RefOkedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefOked::factory()
            ->count(5)
            ->create();
    }
}
