<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RefTechnicTypeTranslation;

class RefTechnicTypeTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefTechnicTypeTranslation::factory()
            ->count(5)
            ->create();
    }
}
