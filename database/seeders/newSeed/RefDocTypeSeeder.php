<?php

namespace Database\Seeders;

use App\Models\RefDocType;
use Illuminate\Database\Seeder;

class RefDocTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefDocType::factory()
            ->count(5)
            ->create();
    }
}
