<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RefAllowanceType;

class RefAllowanceTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefAllowanceType::factory()
            ->count(5)
            ->create();
    }
}
