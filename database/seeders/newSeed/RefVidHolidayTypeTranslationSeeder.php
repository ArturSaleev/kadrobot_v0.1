<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RefVidHolidayTypeTranslation;

class RefVidHolidayTypeTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefVidHolidayTypeTranslation::factory()
            ->count(5)
            ->create();
    }
}
