<?php

namespace Database\Seeders;

use App\Models\RefKindHoliday;
use Illuminate\Database\Seeder;

class RefKindHolidaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefKindHoliday::factory()
            ->count(5)
            ->create();
    }
}
