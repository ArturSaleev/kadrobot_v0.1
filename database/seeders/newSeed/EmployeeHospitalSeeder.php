<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\EmployeeHospital;

class EmployeeHospitalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EmployeeHospital::factory()
            ->count(5)
            ->create();
    }
}
