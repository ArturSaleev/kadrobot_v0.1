<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\EmployeeTripFromTo;

class EmployeeTripFromToSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EmployeeTripFromTo::factory()
            ->count(5)
            ->create();
    }
}
