<?php

namespace Database\Seeders;

use App\Models\EmployeeStazh;
use Illuminate\Database\Seeder;

class EmployeeStazhSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EmployeeStazh::factory()
            ->count(5)
            ->create();
    }
}
