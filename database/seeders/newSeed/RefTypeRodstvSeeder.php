<?php

namespace Database\Seeders;

use App\Models\RefTypeRodstv;
use Illuminate\Database\Seeder;

class RefTypeRodstvSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefTypeRodstv::factory()
            ->count(5)
            ->create();
    }
}
