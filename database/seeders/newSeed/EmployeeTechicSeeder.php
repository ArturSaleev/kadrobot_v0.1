<?php

namespace Database\Seeders;

use App\Models\EmployeeTechic;
use Illuminate\Database\Seeder;

class EmployeeTechicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EmployeeTechic::factory()
            ->count(5)
            ->create();
    }
}
