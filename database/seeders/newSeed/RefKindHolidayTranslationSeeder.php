<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RefKindHolidayTranslation;

class RefKindHolidayTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefKindHolidayTranslation::factory()
            ->count(5)
            ->create();
    }
}
