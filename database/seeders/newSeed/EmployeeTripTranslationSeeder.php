<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\EmployeeTripTranslation;

class EmployeeTripTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EmployeeTripTranslation::factory()
            ->count(5)
            ->create();
    }
}
