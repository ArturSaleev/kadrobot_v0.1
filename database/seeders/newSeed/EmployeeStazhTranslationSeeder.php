<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\EmployeeStazhTranslation;

class EmployeeStazhTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EmployeeStazhTranslation::factory()
            ->count(5)
            ->create();
    }
}
