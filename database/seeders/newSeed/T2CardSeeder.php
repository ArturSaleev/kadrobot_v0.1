<?php

namespace Database\Seeders;

use App\Models\T2Card;
use Illuminate\Database\Seeder;

class T2CardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        T2Card::factory()
            ->count(5)
            ->create();
    }
}
