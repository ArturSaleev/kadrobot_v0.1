<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ReportHtmlOther;

class ReportHtmlOtherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ReportHtmlOther::factory()
            ->count(5)
            ->create();
    }
}
