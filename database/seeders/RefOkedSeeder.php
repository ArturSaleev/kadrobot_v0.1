<?php

namespace Database\Seeders;

use App\Models\RefOked;
use App\Traits\WebEasyTrait;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RefOkedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = (new WebEasyTrait())->getHttpData('ref-okeds');
        $i = 0;
        foreach($items as $item){
            try {
                $oked = new RefOked();
                $oked->oked = $item['oked'];
                $oked->translateOrNew('ru')->name = $item['name'];
                $oked->translateOrNew('ru')->name_oked = $item['name_oked'];
                $oked->save();
                $i++;
            }catch(\Exception $e){
                $this->command->info("error insert ".$item['oked']." - ".$item['name']." - ".$item['name_oked']."\t\n".$e->getMessage());
                continue;
            }
        }
        $this->command->info("new Items count $i inserting example");
    }
}
