<?php

namespace Database\Seeders;

use App\Models\RefTypePhone;
use App\Traits\LocaleTrait;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RefTypePhoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $refTypePhone = new RefTypePhone();
        foreach(LocaleTrait::getAllLanguage() as $lang){
            $refTypePhone->translateOrNew($lang['code'])->name = 'Городской';
        }
        $refTypePhone->save();
    }
}
