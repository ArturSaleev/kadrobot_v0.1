<?php

namespace Database\Seeders;

use App\Models\RefSex;
use App\Traits\LocaleTrait;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RefSexSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $refStatus = new RefSex();
        foreach(LocaleTrait::getAllLanguage() as $lang){
            $name = LocaleTrait::getVariableCode($lang['code'], 'man_sex');
            $refStatus->translateOrNew($lang['code'])->name = $name;
        }
        $refStatus->save();

        $refStatus = new RefSex();
        foreach(LocaleTrait::getAllLanguage() as $lang){
            $name = LocaleTrait::getVariableCode($lang['code'], 'women_sex');
            $refStatus->translateOrNew($lang['code'])->name = $name;
        }
        $refStatus->save();
    }
}
