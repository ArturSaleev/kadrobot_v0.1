<?php

namespace Database\Seeders;

use App\Models\RefAddressType;
use App\Traits\LocaleTrait;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RefAddressTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = ["Головной офис", "Основной", "Дом", "Квартира"];
        foreach($data as $name) {
            $refAddressType = new RefAddressType();
            foreach (LocaleTrait::getAllLanguage() as $lang) {
                $refAddressType->translateOrNew($lang['code'])->name = $name;
            }
            $refAddressType->save();
        }
    }
}
