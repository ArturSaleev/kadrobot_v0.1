<?php

namespace Database\Seeders;

use App\Models\RefFamilyState;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RefFamilyStateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            "ru" => [
                'Никогда не состоял (не состояла) в браке',
                'Состоит в зарегистрированном браке',
                'Состоит в незарегистрированном браке',
                'Разведен (разведена)',
                'Вдовец (вдова)'
            ],
            "kz" => [
                'Ешқашан үйленбеген (жоқ).',
                'Тіркелген некеде',
                'Тіркелмеген некеде',
                'ажырасқан',
                'Жесір'
            ]
        ];

        foreach($data["ru"] as $key=>$name){
            $refFamilyState = new RefFamilyState();
            $refFamilyState->translateOrNew("ru")->name = $name;
            if(isset($data["kz"][$key])){
                $refFamilyState->translateOrNew("kz")->name = $data["kz"][$key];
            }
            $refFamilyState->save();
        }
    }
}
