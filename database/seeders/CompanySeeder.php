<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Traits\LocaleTrait;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $company = new Company();
        $company->bin = "000000000000";
        $company->rnn = "000000000000";
        $company->ref_status_id = 1;
        foreach(LocaleTrait::getAllLanguage() as $lang){
            $company->translateOrNew($lang['code'])->name = "Название компании";
        }
        $company->save();
    }
}
