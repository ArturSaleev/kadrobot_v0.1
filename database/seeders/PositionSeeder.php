<?php

namespace Database\Seeders;

use App\Models\Branch;
use App\Models\Department;
use App\Models\DepartmentPhone;
use App\Models\Position;
use App\Models\RefTypePhone;
use App\Traits\LocaleTrait;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $branch = Branch::query()->first();
        if(!$branch){
            $this->command->warn("Не найден ни 1 филиал компании");
            return;
        }

        $department = new Department();
        $department->branch_id = $branch->id;
        $department->email = "info.dep@org.com";
        foreach(LocaleTrait::getAllLanguage() as $lang){
            $department->translateOrNew($lang['code'])->name = "Департамент / Отдел";
        }
        $department->save();

        $departmentPhone = new DepartmentPhone();
        $departmentPhone->department_id = $department->id;
        $departmentPhone->ref_type_phone_id = RefTypePhone::first()->id;
        $departmentPhone->name = '+7(7172)77-77-71';
        $departmentPhone->save();

        $position = new Position();
        $position->department_id = $department->id;
        $position->cnt = 2;
        $position->pos_level = 1;
        $position->min_salary = 0;
        $position->max_salary = 0;
        foreach(LocaleTrait::getAllLanguage() as $lang){
            $position->translateOrNew($lang['code'])->name = "Должность ".$lang['code'];
        }
        $position->save();
    }
}
