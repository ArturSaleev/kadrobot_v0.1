<?php

namespace Database\Seeders;

use App\Models\RefDocPlace;
use App\Traits\LocaleTrait;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RefDocPlaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            [
                "ru" => "Министерством внутренних дел",
                "kz" => "Ішкі істер министрлігімен",
                "en" => "Министерством внутренних дел",
            ],
            [
                "ru" => "Министерством юстиции",
                "kz" => "Әділет министрлігімен",
                "en" => "Министерством юстиции",
            ]
        ];
        foreach($data as $item) {
            $refDocPlace = new RefDocPlace();
            foreach (LocaleTrait::getAllLanguage() as $lang) {
                $name = (isset($item[$lang['code']])) ? $item[$lang['code']] : $item["ru"];
                $refDocPlace->translateOrNew($lang['code'])->name = $name;
            }
            $refDocPlace->save();
        }
    }
}
