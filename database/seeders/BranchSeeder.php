<?php

namespace Database\Seeders;

use App\Models\Branch;
use App\Models\BranchPhone;
use App\Models\Company;
use App\Models\RefTypePhone;
use App\Traits\LocaleTrait;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BranchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $branch = new Branch();
        $branch->company_id = Company::first()->id;
        $branch->branch_main = true;
        foreach(LocaleTrait::getAllLanguage() as $lang){
            $branch->translateOrNew($lang['code'])->name = "Головной офис компании";
            $branch->translateOrNew($lang['code'])->short_name = "Головной офис";
        }
        $branch->save();

        $branchPhone = new BranchPhone();
        $branchPhone->branch_id = $branch->id;
        $branchPhone->ref_type_phone_id = RefTypePhone::first()->id;
        $branchPhone->name = '+7(7172)77-77-77';
        $branchPhone->save();
    }
}
