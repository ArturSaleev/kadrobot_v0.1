<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\RefAccountTypeTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefAccountTypeTranslationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefAccountTypeTranslation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'name' => $this->faker->name,
            'ref_account_type_id' => \App\Models\RefAccountType::factory(),
        ];
    }
}
