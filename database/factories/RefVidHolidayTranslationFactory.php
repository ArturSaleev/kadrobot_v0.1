<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\RefVidHolidayTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefVidHolidayTranslationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefVidHolidayTranslation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'name' => $this->faker->name,
            'ref_vid_holiday_id' => \App\Models\RefVidHoliday::factory(),
        ];
    }
}
