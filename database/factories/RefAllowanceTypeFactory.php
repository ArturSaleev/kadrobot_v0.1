<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\RefAllowanceType;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefAllowanceTypeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefAllowanceType::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [];
    }
}
