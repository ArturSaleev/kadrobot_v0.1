<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\EmployeeFamily;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeFamilyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EmployeeFamily::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'lastname' => $this->faker->lastName,
            'firstname' => $this->faker->text(255),
            'middlename' => $this->faker->text(255),
            'birthday' => $this->faker->date,
            'employee_id' => \App\Models\Employee::factory(),
            'ref_family_state_id' => \App\Models\RefFamilyState::factory(),
        ];
    }
}
