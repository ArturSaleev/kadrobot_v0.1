<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\EmployeeMilitary;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeMilitaryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EmployeeMilitary::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'group' => $this->faker->text(255),
            'category' => $this->faker->text(255),
            'rank' => $this->faker->text(255),
            'speciality' => $this->faker->text(255),
            'voenkom' => $this->faker->text(255),
            'spec_uch' => $this->faker->text(255),
            'spec_uch_num' => $this->faker->text(255),
            'fit' => $this->faker->text(255),
            'employee_id' => \App\Models\Employee::factory(),
        ];
    }
}
