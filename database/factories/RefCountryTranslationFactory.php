<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\RefCountryTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefCountryTranslationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefCountryTranslation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'name' => $this->faker->name,
            'ref_country_id' => \App\Models\RefCountry::factory(),
        ];
    }
}
