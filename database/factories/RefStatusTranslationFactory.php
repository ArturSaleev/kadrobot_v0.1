<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\RefStatusTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefStatusTranslationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefStatusTranslation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'name' => $this->faker->name,
            'ref_status_id' => \App\Models\RefStatus::factory(),
        ];
    }
}
