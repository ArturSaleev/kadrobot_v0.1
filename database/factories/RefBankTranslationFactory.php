<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\RefBankTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefBankTranslationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefBankTranslation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'name' => $this->faker->name,
            'ref_bank_id' => \App\Models\RefBank::factory(),
        ];
    }
}
