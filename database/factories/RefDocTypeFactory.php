<?php

namespace Database\Factories;

use App\Models\RefDocType;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefDocTypeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefDocType::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [];
    }
}
