<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\RefKindHolidayTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefKindHolidayTranslationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefKindHolidayTranslation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'name' => $this->faker->name,
            'ref_kind_holiday_id' => \App\Models\RefKindHoliday::factory(),
        ];
    }
}
