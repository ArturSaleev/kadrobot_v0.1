<?php

namespace Database\Factories;

use App\Models\Employee;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Employee::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'iin' => $this->faker->text(255),
            'date_post' => $this->faker->date,
            'date_loyoff' => $this->faker->date,
            'reason_loyoff' => $this->faker->text,
            'contract_num' => $this->faker->text(255),
            'contract_date' => $this->faker->date,
            'tab_num' => $this->faker->randomNumber(0),
            'birthday' => $this->faker->date,
            'birth_place' => $this->faker->text,
            'email' => $this->faker->email,
            'account' => $this->faker->text(255),
            'date_zav' => $this->faker->date,
            'oklad' => $this->faker->randomNumber(2),
            'gos_nagr' => $this->faker->text(255),
            'pens' => $this->faker->boolean,
            'pens_date' => $this->faker->date,
            'lgot' => $this->faker->text(255),
            'person_email' => $this->faker->text(255),
            'ref_sex_id' => \App\Models\RefSex::factory(),
            'ref_nationality_id' => \App\Models\RefNationality::factory(),
            'ref_family_state_id' => \App\Models\RefFamilyState::factory(),
            'ref_account_type_id' => \App\Models\RefAccountType::factory(),
            'ref_user_status_id' => \App\Models\RefUserStatus::factory(),
            'position_id' => \App\Models\Position::factory(),
        ];
    }
}
