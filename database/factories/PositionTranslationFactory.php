<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\PositionTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class PositionTranslationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PositionTranslation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'name' => $this->faker->name,
            'position_id' => \App\Models\Position::factory(),
        ];
    }
}
