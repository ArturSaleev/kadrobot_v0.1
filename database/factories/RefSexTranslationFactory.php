<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\RefSexTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefSexTranslationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefSexTranslation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'name' => $this->faker->name,
            'ref_sex_id' => \App\Models\RefSex::factory(),
        ];
    }
}
