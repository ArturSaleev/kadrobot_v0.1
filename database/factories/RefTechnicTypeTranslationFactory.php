<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\RefTechnicTypeTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefTechnicTypeTranslationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefTechnicTypeTranslation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'locale' => $this->faker->locale,
            'ref_technic_type_id' => \App\Models\RefTechnicType::factory(),
        ];
    }
}
