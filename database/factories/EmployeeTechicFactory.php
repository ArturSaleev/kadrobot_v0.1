<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\EmployeeTechic;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeTechicFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EmployeeTechic::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'invent_num' => $this->faker->text(255),
            'price' => $this->faker->randomFloat(2, 0, 9999),
            'employee_id' => \App\Models\Employee::factory(),
            'ref_technic_type_id' => \App\Models\RefTechnicType::factory(),
        ];
    }
}
