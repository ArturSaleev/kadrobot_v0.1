<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\RefTypePhoneTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefTypePhoneTranslationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefTypePhoneTranslation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'name' => $this->faker->name,
            'ref_type_phone_id' => \App\Models\RefTypePhone::factory(),
        ];
    }
}
