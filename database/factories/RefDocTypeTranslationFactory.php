<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\RefDocTypeTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefDocTypeTranslationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefDocTypeTranslation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'name' => $this->faker->name,
            'ref_doc_type_id' => \App\Models\RefDocType::factory(),
        ];
    }
}
