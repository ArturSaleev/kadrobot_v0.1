<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\RefTransportTripTranstion;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefTransportTripTranstionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefTransportTripTranstion::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'name' => $this->faker->name,
            'ref_transport_trip_id' => \App\Models\RefTransportTrip::factory(),
        ];
    }
}
