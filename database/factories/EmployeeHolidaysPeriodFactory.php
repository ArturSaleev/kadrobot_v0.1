<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\EmployeeHolidaysPeriod;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeHolidaysPeriodFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EmployeeHolidaysPeriod::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'period_start' => $this->faker->date,
            'period_end' => $this->faker->date,
            'day_count_used_for_today' => $this->faker->randomNumber(0),
            'didnt_add' => $this->faker->randomNumber(0),
            'paying_for_health' => $this->faker->randomNumber(0),
            'employee_id' => \App\Models\Employee::factory(),
        ];
    }
}
