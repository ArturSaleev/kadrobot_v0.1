<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\EmployeeEducation;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeEducationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EmployeeEducation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'institution' => $this->faker->text(255),
            'year_begin' => $this->faker->randomNumber(0),
            'year_end' => $this->faker->randomNumber(0),
            'date_begin' => $this->faker->date,
            'date_end' => $this->faker->date,
            'speciality' => $this->faker->text(255),
            'qualification' => $this->faker->text(255),
            'diplom_num' => $this->faker->text(255),
            'diplom_date' => $this->faker->date,
            'employee_id' => \App\Models\Employee::factory(),
        ];
    }
}
