<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\BranchTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class BranchTranslationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BranchTranslation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'name' => $this->faker->name,
            'short_name' => $this->faker->text(255),
            'branch_id' => \App\Models\Branch::factory(),
        ];
    }
}
