<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\RefTypePhone;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefTypePhoneFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefTypePhone::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [];
    }
}
