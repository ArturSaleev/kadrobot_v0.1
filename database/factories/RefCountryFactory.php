<?php

namespace Database\Factories;

use App\Models\RefCountry;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefCountryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefCountry::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'code' => $this->faker->unique->text(255),
        ];
    }
}
