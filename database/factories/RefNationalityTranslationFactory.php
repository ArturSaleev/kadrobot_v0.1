<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\RefNationalityTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefNationalityTranslationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefNationalityTranslation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'name' => $this->faker->name,
            'ref_nationality_id' => \App\Models\RefNationality::factory(),
        ];
    }
}
