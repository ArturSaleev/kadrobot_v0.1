<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\RefTypeRodstvTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefTypeRodstvTranslationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefTypeRodstvTranslation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'name' => $this->faker->name,
            'ref_type_rodstv_id' => \App\Models\RefTypeRodstv::factory(),
        ];
    }
}
