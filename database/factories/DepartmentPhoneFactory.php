<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\DepartmentPhone;
use Illuminate\Database\Eloquent\Factories\Factory;

class DepartmentPhoneFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DepartmentPhone::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'department_id' => \App\Models\Department::factory(),
            'ref_type_phone_id' => \App\Models\RefTypePhone::factory(),
        ];
    }
}
