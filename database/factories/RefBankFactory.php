<?php

namespace Database\Factories;

use App\Models\RefBank;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefBankFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefBank::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'mfo' => $this->faker->text(255),
            'mfo_head' => $this->faker->text(255),
            'mfo_rkc' => $this->faker->text(255),
            'kor_account' => $this->faker->text(255),
            'commis' => $this->faker->randomNumber(2),
            'bin' => $this->faker->text(255),
            'bik_old' => $this->faker->text(255),
            'ref_status_id' => \App\Models\RefStatus::factory(),
        ];
    }
}
