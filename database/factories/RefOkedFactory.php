<?php

namespace Database\Factories;

use App\Models\RefOked;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefOkedFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefOked::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'oked' => $this->faker->unique->text(255),
        ];
    }
}
