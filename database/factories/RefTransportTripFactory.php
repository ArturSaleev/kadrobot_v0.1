<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\RefTransportTrip;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefTransportTripFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefTransportTrip::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [];
    }
}
