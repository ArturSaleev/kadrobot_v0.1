<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\RefVidHolidayTypeTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefVidHolidayTypeTranslationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefVidHolidayTypeTranslation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'name' => $this->faker->name,
            'ref_vid_holiday_type_id' => \App\Models\RefVidHolidayType::factory(),
        ];
    }
}
