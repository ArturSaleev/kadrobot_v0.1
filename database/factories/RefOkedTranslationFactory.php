<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\RefOkedTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefOkedTranslationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefOkedTranslation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'name_oked' => $this->faker->text(255),
            'name' => $this->faker->name,
            'ref_oked_id' => \App\Models\RefOked::factory(),
        ];
    }
}
