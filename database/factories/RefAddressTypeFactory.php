<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\RefAddressType;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefAddressTypeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefAddressType::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [];
    }
}
