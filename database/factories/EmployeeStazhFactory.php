<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\EmployeeStazh;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeStazhFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EmployeeStazh::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'date_begin' => $this->faker->date,
            'date_end' => $this->faker->date,
            'cnt_mes' => $this->faker->randomNumber(0),
            'employee_id' => \App\Models\Employee::factory(),
        ];
    }
}
