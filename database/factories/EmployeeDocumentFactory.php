<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\EmployeeDocument;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeDocumentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EmployeeDocument::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'doc_seria' => $this->faker->text(255),
            'doc_num' => $this->faker->text(255),
            'doc_date' => $this->faker->date,
            'employee_id' => \App\Models\Employee::factory(),
            'ref_doc_type_id' => \App\Models\RefDocType::factory(),
            'ref_doc_place_id' => \App\Models\RefDocPlace::factory(),
        ];
    }
}
