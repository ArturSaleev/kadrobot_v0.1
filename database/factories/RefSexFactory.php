<?php

namespace Database\Factories;

use App\Models\RefSex;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefSexFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefSex::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [];
    }
}
