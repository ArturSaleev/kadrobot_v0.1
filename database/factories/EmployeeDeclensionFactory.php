<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\EmployeeDeclension;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeDeclensionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EmployeeDeclension::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'lastname' => $this->faker->lastName,
            'firstname' => $this->faker->text(255),
            'middlename' => $this->faker->text(255),
            'case_type' => $this->faker->text(255),
            'type_description' => $this->faker->text(255),
            'employee_id' => \App\Models\Employee::factory(),
        ];
    }
}
