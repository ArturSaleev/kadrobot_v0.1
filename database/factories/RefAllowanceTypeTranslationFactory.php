<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\RefAllowanceTypeTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefAllowanceTypeTranslationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefAllowanceTypeTranslation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'name' => $this->faker->name,
            'ref_allowance_type_id' => \App\Models\RefAllowanceType::factory(),
        ];
    }
}
