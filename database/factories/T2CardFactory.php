<?php

namespace Database\Factories;

use App\Models\T2Card;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class T2CardFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = T2Card::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'company_id' => \App\Models\Company::factory(),
            'employee_id' => \App\Models\Employee::factory(),
            'ref_branch_id' => \App\Models\Branch::factory(),
            'ref_action_id' => \App\Models\RefAction::factory(),
            'ref_position_id' => \App\Models\Position::factory(),
        ];
    }
}
