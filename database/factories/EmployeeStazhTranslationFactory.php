<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\EmployeeStazhTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeStazhTranslationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EmployeeStazhTranslation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'organization' => $this->faker->text,
            'dolgnost' => $this->faker->text(255),
            'address' => $this->faker->address,
            'employee_stazh_id' => \App\Models\EmployeeStazh::factory(),
        ];
    }
}
