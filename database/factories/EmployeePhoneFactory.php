<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\EmployeePhone;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeePhoneFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EmployeePhone::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'phone' => $this->faker->phoneNumber,
            'ref_type_phone_id' => \App\Models\RefTypePhone::factory(),
            'employee_id' => \App\Models\Employee::factory(),
        ];
    }
}
