<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\DepartmentTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class DepartmentTranslationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DepartmentTranslation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'name' => $this->faker->name,
            'department_id' => \App\Models\Department::factory(),
        ];
    }
}
