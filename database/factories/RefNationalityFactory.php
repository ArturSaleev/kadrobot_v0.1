<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\RefNationality;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefNationalityFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefNationality::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [];
    }
}
