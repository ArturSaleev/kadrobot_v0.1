<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\CompanyTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyTranslationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CompanyTranslation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'name' => $this->faker->text,
            'company_id' => \App\Models\Company::factory(),
        ];
    }
}
