<?php

namespace Database\Factories;

use App\Models\BranchPhone;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class BranchPhoneFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BranchPhone::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'branch_id' => \App\Models\Branch::factory(),
            'ref_type_phone_id' => \App\Models\RefTypePhone::factory(),
        ];
    }
}
