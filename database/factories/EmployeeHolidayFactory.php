<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\EmployeeHoliday;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeHolidayFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EmployeeHoliday::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'date_begin' => $this->faker->date,
            'date_end' => $this->faker->date,
            'cnt_days' => $this->faker->randomNumber(0),
            'period_begin' => $this->faker->date,
            'period_end' => $this->faker->date,
            'order_num' => $this->faker->text(255),
            'order_date' => $this->faker->date,
            'deligate' => $this->faker->randomNumber(0),
            'deligate_emp_id' => $this->faker->randomNumber,
            'doc_content' => $this->faker->text,
            'employee_id' => \App\Models\Employee::factory(),
            'ref_vid_holiday_id' => \App\Models\RefVidHoliday::factory(),
        ];
    }
}
