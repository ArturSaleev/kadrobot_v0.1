<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\BranchAddresesTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class BranchAddresesTranslationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BranchAddresesTranslation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'name' => $this->faker->text,
            'branch_addreses_id' => \App\Models\BranchAddreses::factory(),
        ];
    }
}
