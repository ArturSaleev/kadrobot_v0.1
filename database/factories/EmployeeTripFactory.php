<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\EmployeeTrip;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeTripFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EmployeeTrip::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'date_begin' => $this->faker->date,
            'date_end' => $this->faker->date,
            'cnt_days' => $this->faker->randomNumber(0),
            'order_num' => $this->faker->text(255),
            'order_date' => $this->faker->date,
            'employee_id' => \App\Models\Employee::factory(),
            'ref_transport_trip_id' => \App\Models\RefTransportTrip::factory(),
        ];
    }
}
