<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\BranchAddreses;
use Illuminate\Database\Eloquent\Factories\Factory;

class BranchAddresesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BranchAddreses::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'ref_address_type_id' => \App\Models\RefAddressType::factory(),
            'branch_id' => \App\Models\Branch::factory(),
        ];
    }
}
