<?php

namespace Database\Factories;

use App\Models\Company;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Company::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'bin' => $this->faker->text(255),
            'rnn' => $this->faker->text(255),
            'oked' => $this->faker->text(255),
            'ref_status_id' => \App\Models\RefStatus::factory(),
        ];
    }
}
