<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\RefActionTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefActionTranslationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefActionTranslation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'name' => $this->faker->name,
            'ref_action_id' => \App\Models\RefAction::factory(),
        ];
    }
}
