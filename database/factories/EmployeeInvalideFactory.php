<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\EmployeeInvalide;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeInvalideFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EmployeeInvalide::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'num' => $this->faker->text(255),
            'date_add' => $this->faker->date,
            'period_begin' => $this->faker->date,
            'period_end' => $this->faker->date,
            'employee_id' => \App\Models\Employee::factory(),
        ];
    }
}
