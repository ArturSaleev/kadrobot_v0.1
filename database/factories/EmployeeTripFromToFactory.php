<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\EmployeeTripFromTo;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeTripFromToFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EmployeeTripFromTo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'from_place' => $this->faker->text,
            'to_place' => $this->faker->text,
            'employee_trip_id' => \App\Models\EmployeeTrip::factory(),
            'ref_transport_trip_id' => \App\Models\RefTransportTrip::factory(),
        ];
    }
}
