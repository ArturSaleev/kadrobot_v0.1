<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\EmployeeAddress;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeAddressFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EmployeeAddress::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'employee_id' => \App\Models\Employee::factory(),
            'ref_address_type_id' => \App\Models\RefAddressType::factory(),
        ];
    }
}
