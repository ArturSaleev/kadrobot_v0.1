<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\RefAddressTypeTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefAddressTypeTranslationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefAddressTypeTranslation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'name' => $this->faker->name,
            'ref_address_type_id' => \App\Models\RefAddressType::factory(),
        ];
    }
}
