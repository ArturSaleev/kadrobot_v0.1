<?php

namespace Database\Factories;

use App\Models\RefMeta;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefMetaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefMeta::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->text(255),
            'name' => $this->faker->name,
            'tablename' => $this->faker->text(255),
            'column_name' => $this->faker->text(255),
        ];
    }
}
