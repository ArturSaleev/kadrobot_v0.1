<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\RefPersonStateTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefPersonStateTranslationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefPersonStateTranslation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'name' => $this->faker->name,
            'ref_person_state_id' => \App\Models\RefPersonState::factory(),
        ];
    }
}
