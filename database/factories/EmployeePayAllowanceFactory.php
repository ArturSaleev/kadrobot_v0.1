<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\EmployeePayAllowance;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeePayAllowanceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EmployeePayAllowance::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'date_add' => $this->faker->date,
            'period_begin' => $this->faker->date,
            'period_end' => $this->faker->date,
            'paysum' => $this->faker->randomNumber(2),
            'employee_id' => \App\Models\Employee::factory(),
            'ref_allowance_type_id' => \App\Models\RefAllowanceType::factory(),
        ];
    }
}
