<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\PositionDeclension;
use Illuminate\Database\Eloquent\Factories\Factory;

class PositionDeclensionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PositionDeclension::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'value' => $this->faker->text(255),
            'case_type' => $this->faker->text(255),
            'type_description' => $this->faker->text(255),
            'position_id' => \App\Models\Position::factory(),
        ];
    }
}
