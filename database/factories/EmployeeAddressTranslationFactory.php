<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\EmployeeAddressTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeAddressTranslationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EmployeeAddressTranslation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'employee_address_id' => \App\Models\EmployeeAddress::factory(),
        ];
    }
}
