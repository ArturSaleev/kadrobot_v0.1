<?php

namespace Database\Factories;

use App\Models\RefAction;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefActionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefAction::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'act_type' => $this->faker->randomNumber(0),
        ];
    }
}
