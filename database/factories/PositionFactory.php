<?php

namespace Database\Factories;

use App\Models\Position;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class PositionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Position::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'cnt' => $this->faker->randomNumber(0),
            'pos_level' => $this->faker->randomNumber(0),
            'min_salary' => $this->faker->randomNumber(2),
            'max_salary' => $this->faker->randomNumber(2),
            'department_id' => \App\Models\Department::factory(),
        ];
    }
}
