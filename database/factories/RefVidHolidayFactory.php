<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\RefVidHoliday;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefVidHolidayFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefVidHoliday::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'ref_vid_holiday_type_id' => \App\Models\RefVidHolidayType::factory(),
        ];
    }
}
