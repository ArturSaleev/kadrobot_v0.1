<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\EmployeeHospital;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeHospitalFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EmployeeHospital::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'date_begin' => $this->faker->date,
            'date_end' => $this->faker->date,
            'cnt_days' => $this->faker->randomNumber(0),
            'employee_id' => \App\Models\Employee::factory(),
        ];
    }
}
