<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\RefDocPlaceTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefDocPlaceTranslationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefDocPlaceTranslation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'name' => $this->faker->name,
            'ref_doc_place_id' => \App\Models\RefDocPlace::factory(),
        ];
    }
}
