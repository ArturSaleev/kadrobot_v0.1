<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\RefUserStatusTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefUserStatusTranslationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefUserStatusTranslation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'name' => $this->faker->name,
            'ref_user_status_id' => \App\Models\RefUserStatus::factory(),
        ];
    }
}
