<?php

namespace Database\Factories;

use App\Models\Curator;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class CuratorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Curator::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'employee_id' => \App\Models\Employee::factory(),
            'department_id' => \App\Models\Department::factory(),
        ];
    }
}
