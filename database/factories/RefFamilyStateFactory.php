<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\RefFamilyState;
use Illuminate\Database\Eloquent\Factories\Factory;

class RefFamilyStateFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RefFamilyState::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [];
    }
}
