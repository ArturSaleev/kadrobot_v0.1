<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\EmployeeTripTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeTripTranslationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EmployeeTripTranslation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'locale' => $this->faker->locale,
            'aim' => $this->faker->text,
            'final_distantion' => $this->faker->text,
            'employee_trip_id' => \App\Models\EmployeeTrip::factory(),
        ];
    }
}
