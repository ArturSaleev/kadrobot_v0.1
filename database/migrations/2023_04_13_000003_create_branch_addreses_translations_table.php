<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_addreses_translations', function (
            Blueprint $table
        ) {
            $table->bigIncrements('id');
            $table->string('locale', 8);
            $table->text('name');
            $table->unsignedBigInteger('branch_addreses_id');

            $table->index('locale');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_addreses_translations');
    }
};
