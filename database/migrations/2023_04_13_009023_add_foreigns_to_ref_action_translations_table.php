<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_action_translations', function (Blueprint $table) {
            $table
                ->foreign('ref_action_id')
                ->references('id')
                ->on('ref_actions')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_action_translations', function (Blueprint $table) {
            $table->dropForeign(['ref_action_id']);
        });
    }
};
