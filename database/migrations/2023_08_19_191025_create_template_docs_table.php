<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('template_docs', function (Blueprint $table) {
            $table->id();
            $table->integer('id_type')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('template_doc_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('template_doc_id');
            $table->string('locale', 8);
            $table->string('name');
            $table->text('body')->nullable();

            $table->index('locale');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('template_doc_translations', function (Blueprint $table) {
            $table
                ->foreign('template_doc_id')
                ->references('id')
                ->on('template_docs')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('template_doc_translations', function (Blueprint $table) {
            $table->dropForeign(['template_doc_id']);
        });
        Schema::dropIfExists('template_doc_translations');
        Schema::dropIfExists('template_docs');
    }
};
