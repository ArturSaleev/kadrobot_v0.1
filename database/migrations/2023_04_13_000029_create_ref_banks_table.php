<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_banks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('ref_status_id')->default(1);
            $table->string('mfo')->nullable();
            $table->string('mfo_head')->nullable();
            $table->string('mfo_rkc')->nullable();
            $table->string('kor_account')->nullable();
            $table->float('commis')->default(0);
            $table->string('bin')->nullable();
            $table->string('bik_old')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_banks');
    }
};
