<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_stazh_translations', function (
            Blueprint $table
        ) {
            $table
                ->foreign('employee_stazh_id')
                ->references('id')
                ->on('employee_stazhs')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_stazh_translations', function (
            Blueprint $table
        ) {
            $table->dropForeign(['employee_stazh_id']);
        });
    }
};
