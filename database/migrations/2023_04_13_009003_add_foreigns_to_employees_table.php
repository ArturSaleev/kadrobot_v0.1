<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table
                ->foreign('ref_sex_id')
                ->references('id')
                ->on('ref_sexes')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');

            $table
                ->foreign('ref_nationality_id')
                ->references('id')
                ->on('ref_nationalities')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');

            $table
                ->foreign('ref_family_state_id')
                ->references('id')
                ->on('ref_family_states')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');

            $table
                ->foreign('ref_account_type_id')
                ->references('id')
                ->on('ref_account_types')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');

            $table
                ->foreign('ref_user_status_id')
                ->references('id')
                ->on('ref_user_statuses')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');

            $table
                ->foreign('position_id')
                ->references('id')
                ->on('positions')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->dropForeign(['ref_sex_id']);
            $table->dropForeign(['ref_nationality_id']);
            $table->dropForeign(['ref_family_state_id']);
            $table->dropForeign(['ref_account_type_id']);
            $table->dropForeign(['ref_user_status_id']);
            $table->dropForeign(['position_id']);
        });
    }
};
