<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_doc_type_translations', function (Blueprint $table) {
            $table
                ->foreign('ref_doc_type_id')
                ->references('id')
                ->on('ref_doc_types')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_doc_type_translations', function (Blueprint $table) {
            $table->dropForeign(['ref_doc_type_id']);
        });
    }
};
