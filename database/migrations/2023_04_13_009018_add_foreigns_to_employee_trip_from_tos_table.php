<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_trip_from_tos', function (Blueprint $table) {
            $table
                ->foreign('employee_trip_id')
                ->references('id')
                ->on('employee_trips')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');

            $table
                ->foreign('ref_transport_trip_id')
                ->references('id')
                ->on('ref_transport_trips')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_trip_from_tos', function (Blueprint $table) {
            $table->dropForeign(['employee_trip_id']);
            $table->dropForeign(['ref_transport_trip_id']);
        });
    }
};
