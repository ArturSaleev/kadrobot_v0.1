<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_educations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_id');
            $table->string('institution');
            $table->integer('year_begin')->nullable();
            $table->integer('year_end')->nullable();
            $table->date('date_begin')->nullable();
            $table->date('date_end')->nullable();
            $table->string('speciality')->nullable();
            $table->string('qualification')->nullable();
            $table->string('diplom_num')->nullable();
            $table->date('diplom_date')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_educations');
    }
};
