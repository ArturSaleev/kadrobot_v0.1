<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('iin');
            $table->date('date_post')->nullable();
            $table->date('date_loyoff')->nullable();
            $table->text('reason_loyoff')->nullable();
            $table->string('contract_num')->nullable();
            $table->date('contract_date')->nullable();
            $table->integer('tab_num')->nullable();
            $table->date('birthday')->nullable();
            $table->text('birth_place')->nullable();
            $table->unsignedBigInteger('ref_sex_id')->nullable();
            $table->unsignedBigInteger('ref_nationality_id')->nullable();
            $table->unsignedBigInteger('ref_family_state_id')->nullable();
            $table->unsignedBigInteger('ref_account_type_id');
            $table->unsignedBigInteger('ref_user_status_id');
            $table->unsignedBigInteger('position_id');
            $table->string('email');
            $table->string('account')->nullable();
            $table->date('date_zav')->nullable();
            $table->float('oklad')->nullable();
            $table->string('gos_nagr')->nullable();
            $table->boolean('pens')->default(false);
            $table->date('pens_date')->nullable();
            $table->string('lgot')->nullable();
            $table->string('person_email')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
};
