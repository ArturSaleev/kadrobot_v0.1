<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_holidays', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_id');
            $table->date('date_begin')->nullable();
            $table->date('date_end')->nullable();
            $table->integer('cnt_days')->nullable();
            $table->date('period_begin')->nullable();
            $table->date('period_end')->nullable();
            $table->string('order_num')->nullable();
            $table->date('order_date')->nullable();
            $table->unsignedBigInteger('ref_vid_holiday_id');
            $table->integer('deligate')->nullable();
            $table->bigInteger('deligate_emp_id')->nullable();
            $table->text('doc_content');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_holidays');
    }
};
