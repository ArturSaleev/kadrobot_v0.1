<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_family_state_translations', function (
            Blueprint $table
        ) {
            $table
                ->foreign('ref_family_state_id')
                ->references('id')
                ->on('ref_family_states')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_family_state_translations', function (
            Blueprint $table
        ) {
            $table->dropForeign(['ref_family_state_id']);
        });
    }
};
