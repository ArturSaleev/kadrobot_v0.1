<?php


namespace App\Traits;


use Illuminate\Support\Facades\App;

class LocaleTrait
{
    /**
     * @return array
     */
    public static function getAllLanguage($onData = false, $type = 'crud'): array
    {
        if(count(config('panel.available_languages', [])) > 1){
            $langs = [];
            foreach(config('panel.available_languages') as $key=>$name){
                array_push($langs, [
                    "code" => $key,
                    "name" => $name,
                    "translation" => (!$onData) ? [] : trans($type, [], $key)
                ]);
            }
            return $langs;
        }else{
            return [];
        }
    }

    public static function getVariableCode($lang, $type)
    {
        return trans($type, [], $lang);
    }
}
