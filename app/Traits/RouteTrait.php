<?php


namespace App\Traits;


use Illuminate\Support\Facades\Route;

class RouteTrait
{
    public static function getDescription($name)
    {
        $langApi = __('api');
        $nameArray = explode('.', $name);
        if(count($nameArray) <= 0){
            return "";
        }
        if(count($nameArray) === 1){
            return __($nameArray[0]);
        }
        if(count($nameArray) === 2){
            return $langApi[$nameArray[1]] ?? "";
        }
        if(count($nameArray) === 3){
            return $langApi[$nameArray[1]][$nameArray[2]] ?? "";
        }
        return "";
    }
    /**
     * @return array
     */
    public static function getAllApi(): array
    {
        $apiRoutes = collect();
        $apiRoutesNames = [];
        foreach (Route::getRoutes() as $route) {
            if (empty($route->action['prefix'])) continue;
            if ($route->action['prefix'] !== 'api') continue;

            if(in_array($route->uri, ["api/login", "api/user", "api/languages"])) continue;

            $apiRoutes->push($route);
            $trans_name = str_replace('/', '.', $route->action['as'] ?? $route->uri);

            $apiRoutesNames[] = [
                "name" => $route->action['as'] ?? $route->uri,
                "uri" => $route->uri,
                "method" => $route->methods[0],
                "description" => self::getDescription($trans_name)
            ];
        }

        $apiRoutesNames = array_filter($apiRoutesNames);
        return $apiRoutesNames;
    }
}
