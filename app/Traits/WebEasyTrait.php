<?php


namespace App\Traits;


use Illuminate\Support\Facades\Http;

class WebEasyTrait
{
    private string $mainUrl = "https://webeasy.kz/api";
    protected string $token = "";
    protected string $username = 'testapi@webeasy.kz';
    protected string $password = 'Aa123456@';

    private function getToken()
    {
        if($this->token === ""){
            $response = Http::post("$this->mainUrl/login", [
                "email" => $this->username,
                "password" => $this->password
            ]);
            $token = $response->json("token");
            $this->token = $token;
        }
        return $this->token;
    }

    public function getHttpData($url)
    {
        $token = $this->getToken();

        $responseData = Http::withHeaders([
            "Authorization" => "Bearer $token",
            "Accept" => "application/json"
        ])->get("$this->mainUrl/$url");

        return $responseData->json("data");
    }

}
