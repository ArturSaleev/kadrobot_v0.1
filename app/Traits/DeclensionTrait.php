<?php


namespace App\Traits;


use Illuminate\Support\Facades\Http;

class DeclensionTrait
{
    private static function sendApi($url)
    {
        $response = Http::get("$url");
        $body = $response->body();

        $xml = simplexml_load_string($body);
        $json = json_encode($xml);
        return json_decode($json, TRUE);
    }

    public static function declensionsOther($lang, $name)
    {
        $langs = config('panel.available_languages', []);
        if(empty($langs[$lang])){
            $decl = ["im", "rod", "dat", "vin", "tvor", "predl", "gde",];
            $data = [];
            foreach($decl as $dec){
                $data[] = [
                    "type" => $dec,
                    "description" => LocaleTrait::getVariableCode("en", "declension.$dec"),
                    "name" => $name
                ];
            }
            return $data;
        }

        if($lang === 'kz'){
            $response = self::sendApi("https://ws3.morpher.ru/qazaq/declension?s=$name");
            if(empty($response['message'])){
                return self::qazakhFioToArray($response, false);
            }
            //Если ответ пришел с ошибкой тогда переделываем на русский и выставляем как по русски в падежах
            $lang = 'ru';
        }

        if($lang === 'ru'){
            $response = self::sendApi("https://ws3.morpher.ru/russian/declension?s=$name");
            return self::russianFioToArray($response, false);
        }

        return self::englishFioToArray(false, $name);

    }

    public static function getDeclensions($onExplode, $lastname, $firstname = "", $middlename = "")
    {
        $langs = LocaleTrait::getAllLanguage();
        $result = [];

        $linksUrl = [
            "ru" => [
                "url" => "russian",
                "funFio" => "russianFioToArray"
            ],
            "kz" => [
                "url" => "qazaq",
                "funFio" => "qazakhFioToArray"
            ]
        ];

        $s_url = $lastname;
        if($firstname !== "")$s_url .= " ".$firstname;
        if($middlename !== "")$s_url .= " ".$middlename;

        foreach ($langs as $lang) {
            $langCode = $lang["code"];
            if (isset($linksUrl[$langCode])) {
                $linkCode = $linksUrl[$langCode]['url'];
                $fun = $linksUrl[$langCode]['funFio'];
                $url = "https://ws3.morpher.ru/$linkCode/declension?s=$s_url";
                $response = self::sendApi($url);
                $result[$langCode] = self::$fun($response, $onExplode);
            } else {
                $result[$langCode] = self::englishFioToArray($onExplode, $lastname, $firstname, $middlename);
            }
        }
        return $result;
    }

    private static function englishFioToArray($onFio, $lastname, $firstname = '', $middlename = '')
    {
        $result = [];
        $decl = ["im", "rod", "dat", "vin", "tvor", "predl", "gde",];
        if($onFio) {
            $newLastname = self::translit($lastname);
            $newFirstname = self::translit($firstname);
            $middlename = self::translit($middlename);
            foreach ($decl as $dec) {
                $data = [
                    "type" => $dec,
                    "name" => LocaleTrait::getVariableCode("en", "declension.$dec"),
                    "lastname" => $newLastname,
                    "firstname" => $newFirstname,
                    "middlename" => $middlename
                ];
                array_push($result, $data);
            }
        }else{
            foreach ($decl as $dec) {
                $data = [
                    "type" => $dec,
                    "name" => LocaleTrait::getVariableCode("en", "declension.$dec"),
                    "value" => self::translit($lastname)
                ];
                array_push($result, $data);
            }
        }

        return $result;
    }

    private static function qazakhFioToArray($array, $declFio = true, $key_param = "", $old_data = [])
    {
        /**
         * A   или    im    именительный падеж
         * І   или    rod    родительный падеж
         * Б   или    dat    дательный падеж
         * Т   или    vin винительный падеж
         * Ш   или    predl исходный падеж
         * Ж   или gde местный падеж
         * К   или    tvor творительный падеж
         */

        $result = ($key_param !== "") ? $old_data : [];
        $decl = [
            "A" => "im",
            "І" => "rod",
            "Б" => "dat",
            "Т" => "vin",
            "К" => "tvor",
            "Ш" => "predl",
            "Ж" => "gde",
        ];

        $decl_params = [
            "менің" => "my",
            "сенің" => "your",
            "сіздің" => "yours",
            "оның" => "his",
            "біздің" => "our",
            "сендердің" => "you_are",
            "сіздердің" => "you_ares",
            "олардың" => "their",
            "көпше" => "plural",
            "множественное" => "plural",
        ];

        foreach ($array as $key => $value) {
            if (!is_array($value)) {
                $key_var = $decl[$key];
                if ($key_param !== "") {
                    $key_var = "";
                    $key_explode = explode(' ', $key_param);
                    foreach ($key_explode as $i => $item) {
                        $key_var .= (($i > 0) ? "_" : "") . $decl_params[$item];
                    }
                    $key_var .= "_" . $decl[$key];
                }
                if ($declFio) {
                    $fio = explode(" ", $value);
                    $name = ($key_param !== "") ? $key_param . " " . LocaleTrait::getVariableCode("kz", "declension." . $decl[$key]) :
                        LocaleTrait::getVariableCode("kz", "declension." . $decl[$key]);
                    $data = [
                        "type" => $key_var,
                        "name" => $name,
                        "lastname" => $fio[0],
                        "firstname" => $fio[1] ?? "",
                        "middlename" => $fio[2] ?? ""
                    ];
                } else {
                    $name = ($key_param !== "") ? $key_param . " " . LocaleTrait::getVariableCode("kz", "declension." . $decl[$key]) :
                        LocaleTrait::getVariableCode("kz", "declension." . $decl[$key]);
                    $data = [
                        "type" => $key_var,
                        "name" => $name,
                        "value" => $value
                    ];
                }
                array_push($result, $data);
            } else {
                $result = self::qazakhFioToArray($value, $declFio, ($key_param !== "") ? $key_param . " " . $key : $key, $result);
            }
        }

        return $result;
    }

    private static function russianFioToArray($array, $declFio = true, $key_param = "", $old_data = [])
    {
        /**
         * И    или    im    именительный падеж
         * Р    или    rod    родительный падеж
         * Д    или    dat    дательный падеж
         * В    или    vin    винительный падеж
         * Т    или    tvor    творительный падеж
         * П    или    predl    предложный падеж без предлога
         * П_о    или    predl-o    предложный падеж с предлогом о/об/обо
         * М    или    gde    местный падеж (отвечает на вопрос где?)
         */
        $result = [];
        $decl = [
            "И" => "im",
            "ФИО" => "im",
            "Р" => "rod",
            "Д" => "dat",
            "В" => "vin",
            "Т" => "tvor",
            "П" => "predl",
            "П_о" => "predl-o",
            "М" => "gde",
        ];

        $decl_params = [
            "множественное" => "plural",
        ];

        foreach ($array as $key => $value) {
            if ($declFio) {
                $fio = [];
                if (is_array($value)) {
                    if ($key == "ФИО") {
                        $fio[0] = $value["Ф"];
                        $fio[1] = $value["И"] ?? "";
                        $fio[2] = $value["О"] ?? "";
                    }
                } else {
                    $fio = explode(" ", $value);
                }
                $data = [
                    "type" => $decl[$key],
                    "name" => LocaleTrait::getVariableCode("ru", "declension." . $decl[$key]),
                    "lastname" => $fio[0],
                    "firstname" => $fio[1] ?? "",
                    "middlename" => $fio[2] ?? ""
                ];
                array_push($result, $data);
            } else {
                if(is_array($value)){
                    $data = self::russianFioToArray($value, $declFio, $key, $result);
                    foreach($data as $item){
                        array_push($result, $item);
                    }
                }else {
                    $name = ($key_param !== "") ? $key_param . " " . LocaleTrait::getVariableCode("ru", "declension." . $decl[$key]) :
                        LocaleTrait::getVariableCode("ru", "declension." . $decl[$key]);

                    $key_var = $decl[$key];
                    if ($key_param !== "") {
                        $key_var = "";
                        $key_explode = explode(' ', $key_param);
                        foreach ($key_explode as $i => $item) {
                            $key_var .= (($i > 0) ? "_" : "") . $decl_params[$item];
                        }
                        $key_var .= "_" . $decl[$key];
                    }

                    $data = [
                        "type" => $key_var,
                        "name" => $name,
                        "value" => $value
                    ];
                    array_push($result, $data);
                }
            }
        }
        return $result;
    }

    public static function translit($str)
    {
        $tr = array(
            "А" => "A", "Б" => "B", "В" => "V", "Г" => "G",
            "Д" => "D", "Е" => "E", "Ж" => "J", "З" => "Z", "И" => "I",
            "Й" => "Y", "К" => "K", "Л" => "L", "М" => "M", "Н" => "N",
            "О" => "O", "П" => "P", "Р" => "R", "С" => "S", "Т" => "T",
            "У" => "U", "Ф" => "F", "Х" => "H", "Ц" => "TS", "Ч" => "CH",
            "Ш" => "SH", "Щ" => "SCH", "Ъ" => "", "Ы" => "YI", "Ь" => "",
            "Э" => "E", "Ю" => "YU", "Я" => "YA", "а" => "a", "б" => "b",
            "в" => "v", "г" => "g", "д" => "d", "е" => "e", "ж" => "j",
            "з" => "z", "и" => "i", "й" => "y", "к" => "k", "л" => "l",
            "м" => "m", "н" => "n", "о" => "o", "п" => "p", "р" => "r",
            "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "h",
            "ц" => "ts", "ч" => "ch", "ш" => "sh", "щ" => "sch", "ъ" => "y",
            "ы" => "yi", "ь" => "'", "э" => "e", "ю" => "yu", "я" => "ya",
            "." => "_", //" " => "_",
            "?" => "_", "/" => "_", "\\" => "_",
            "*" => "_", ":" => "_", "*" => "_", "\"" => "_", "<" => "_",
            ">" => "_", "|" => "_"
        );
        return strtr($str, $tr);
    }
}
