<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefTechnicType extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    use Translatable;

    public $translatedAttributes = ['name'];

    // protected $fillable = ['name'];
    protected $fillable = [];

    protected $searchableFields = ['*'];

    protected $table = 'ref_technic_types';

    public function employeeTechics()
    {
        return $this->hasMany(EmployeeTechic::class);
    }

    public function refTechnicTypeTranslations()
    {
        return $this->hasMany(RefTechnicTypeTranslation::class);
    }
}
