<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefAddressType extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    use Translatable;

    public $translatedAttributes = ['name'];

    protected $fillable = [];

    protected $searchableFields = ['*'];

    protected $table = 'ref_address_types';

    public function employeeAddresses()
    {
        return $this->hasMany(EmployeeAddress::class);
    }

    public function allCompanyAddreses()
    {
        return $this->hasMany(BranchAddreses::class);
    }

    public function refAddressTypeTranslations()
    {
        return $this->hasMany(RefAddressTypeTranslation::class);
    }
}
