<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EmployeeInvalide extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = [
        'employee_id',
        'num',
        'date_add',
        'period_begin',
        'period_end',
    ];

    protected $searchableFields = ['*'];

    protected $table = 'employee_invalides';

    protected $casts = [
        'date_add' => 'date',
        'period_begin' => 'date',
        'period_end' => 'date',
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
