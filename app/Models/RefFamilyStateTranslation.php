<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefFamilyStateTranslation extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = ['ref_family_state_id', 'locale', 'name'];

    protected $searchableFields = ['*'];

    protected $table = 'ref_family_state_translations';

    public function refFamilyState()
    {
        return $this->belongsTo(RefFamilyState::class);
    }
}
