<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EmployeeHospital extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = ['date_begin', 'date_end', 'cnt_days', 'employee_id'];

    protected $searchableFields = ['*'];

    protected $table = 'employee_hospitals';

    protected $casts = [
        'date_begin' => 'date',
        'date_end' => 'date',
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
