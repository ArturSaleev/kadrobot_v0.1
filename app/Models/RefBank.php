<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefBank extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    use Translatable;

    public $translatedAttributes = ['name'];

    protected $fillable = [
        'ref_status_id',
        'mfo',
        'mfo_head',
        'mfo_rkc',
        'kor_account',
        'commis',
        'bin',
        'bik_old',
    ];

    protected $searchableFields = ['*'];

    protected $table = 'ref_banks';

    public function refStatus()
    {
        return $this->belongsTo(RefStatus::class);
    }

    public function refBankTranslations()
    {
        return $this->hasMany(RefBankTranslation::class);
    }
}
