<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefTypeRodstvTranslation extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = ['ref_type_rodstv_id', 'locale', 'name'];

    protected $searchableFields = ['*'];

    protected $table = 'ref_type_rodstv_translations';

    public function refTypeRodstv()
    {
        return $this->belongsTo(RefTypeRodstv::class);
    }
}
