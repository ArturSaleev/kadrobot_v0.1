<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CompanyTranslation extends MainModel
{
    use HasFactory;
    use Searchable;

    protected $fillable = ['company_id', 'locale', 'name'];

    protected $searchableFields = ['*'];

    protected $table = 'company_translations';

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
