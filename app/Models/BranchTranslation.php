<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BranchTranslation extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = ['branch_id', 'locale', 'name', 'short_name'];

    protected $searchableFields = ['*'];

    protected $table = 'branch_translations';

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }
}
