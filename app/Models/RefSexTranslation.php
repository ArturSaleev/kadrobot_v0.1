<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefSexTranslation extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = ['ref_sex_id', 'locale', 'name'];

    protected $searchableFields = ['*'];

    protected $table = 'ref_sex_translations';

    public function refSex()
    {
        return $this->belongsTo(RefSex::class);
    }
}
