<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefCountryTranslation extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = ['ref_country_id', 'locale', 'name'];

    protected $searchableFields = ['*'];

    protected $table = 'ref_country_translations';

    public function refCountry()
    {
        return $this->belongsTo(RefCountry::class);
    }
}
