<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefAction extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    use Translatable;

    public $translatedAttributes = ['name'];

    protected $fillable = ['act_type'];

    protected $searchableFields = ['*'];

    protected $table = 'ref_actions';

    public function t2Cards()
    {
        return $this->hasMany(T2Card::class);
    }

    public function refActionTranslations()
    {
        return $this->hasMany(RefActionTranslation::class);
    }
}
