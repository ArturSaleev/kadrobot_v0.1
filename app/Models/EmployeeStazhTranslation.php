<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EmployeeStazhTranslation extends MainModel
{
    use HasFactory;
    use Searchable;

    protected $fillable = [
        'employee_stazh_id',
        'locale',
        'organization',
        'dolgnost',
        'address',
    ];

    protected $searchableFields = ['*'];

    protected $table = 'employee_stazh_translations';

    public function employeeStazh()
    {
        return $this->belongsTo(EmployeeStazh::class);
    }
}
