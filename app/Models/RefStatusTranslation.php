<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefStatusTranslation extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = ['ref_status_id', 'locale', 'name'];

    protected $searchableFields = ['*'];

    protected $table = 'ref_status_translations';

    public function refStatus()
    {
        return $this->belongsTo(RefStatus::class);
    }
}
