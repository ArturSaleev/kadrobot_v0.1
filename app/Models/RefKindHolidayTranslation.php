<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefKindHolidayTranslation extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = ['ref_kind_holiday_id', 'locale', 'name'];

    protected $searchableFields = ['*'];

    protected $table = 'ref_kind_holiday_translations';

    public function refKindHoliday()
    {
        return $this->belongsTo(RefKindHoliday::class);
    }
}
