<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefDocPlaceTranslation extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = ['ref_doc_place_id', 'locale', 'name'];

    protected $searchableFields = ['*'];

    protected $table = 'ref_doc_place_translations';

    public function refDocPlace()
    {
        return $this->belongsTo(RefDocPlace::class);
    }
}
