<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefTechnicTypeTranslation extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = ['name', 'locale', 'ref_technic_type_id'];

    protected $searchableFields = ['*'];

    protected $table = 'ref_technic_type_translations';

    public function refTechnicType()
    {
        return $this->belongsTo(RefTechnicType::class);
    }
}
