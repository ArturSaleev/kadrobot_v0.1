<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Branch extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    use Translatable;

    public $translatedAttributes = ['name', 'short_name'];

    protected $fillable = ['company_id', 'branch_main'];

    protected $searchableFields = ['*'];

    protected $casts = [
        'branch_main' => 'boolean',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function t2Cards()
    {
        return $this->hasMany(T2Card::class, 'ref_branch_id');
    }

    public function branchPhones()
    {
        return $this->hasMany(BranchPhone::class);
    }

    public function refDepartments()
    {
        return $this->hasMany(Department::class);
    }

    public function branchTranslations()
    {
        return $this->hasMany(BranchTranslation::class);
    }

    public function branchAddreses()
    {
        return $this->hasMany(BranchAddreses::class);
    }
}
