<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Position extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    use Translatable;

    public $translatedAttributes = ['name'];

    protected $fillable = [
        'cnt',
        'pos_level',
        'min_salary',
        'max_salary',
        'department_id',
    ];

    protected $searchableFields = ['*'];

    public function t2Cards()
    {
        return $this->hasMany(T2Card::class, 'ref_position_id');
    }

    public function positionTranslations()
    {
        return $this->hasMany(PositionTranslation::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }

    public function positionDeclensions()
    {
        return $this->hasMany(PositionDeclension::class);
    }
}
