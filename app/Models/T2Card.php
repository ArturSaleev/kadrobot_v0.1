<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class T2Card extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = [
        'company_id',
        'employee_id',
        'ref_branch_id',
        'ref_action_id',
        'ref_position_id',
    ];

    protected $searchableFields = ['*'];

    protected $table = 't2_cards';

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function refBranch()
    {
        return $this->belongsTo(Branch::class, 'ref_branch_id');
    }

    public function refAction()
    {
        return $this->belongsTo(RefAction::class);
    }

    public function refPosition()
    {
        return $this->belongsTo(Position::class, 'ref_position_id');
    }
}
