<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EmployeeMilitary extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = [
        'employee_id',
        'group',
        'category',
        'rank',
        'speciality',
        'voenkom',
        'spec_uch',
        'spec_uch_num',
        'fit',
    ];

    protected $searchableFields = ['*'];

    protected $table = 'employee_militaries';

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
