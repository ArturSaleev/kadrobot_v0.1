<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Employee
 * @package App\Models
 * @property $iin - ИИН
 * @property $date_post - Дата принятия на работу
 * @property $date_loyoff - Дата увольнения
 * @property $reason_loyoff - Причина увольнения
 * @property $contract_num - Номер договора
 * @property $contract_date - Дата договора
 * @property $tab_num - Табельный номер
 * @property $birthday - Дата рождения
 * @property $birth_place - Место рождения
 * @property $ref_sex_id - Пол
 * @property $ref_nationality_id - Национальность
 * @property $ref_family_state_id - Семейное положение
 * @property $ref_account_type_id - Тип счета
 * @property $ref_user_status_id - Статус сотрудника
 * @property $position_id - Позиция/Должность
 * @property $email - email сотрудника
 * @property $account - Номер счета
 * @property $date_zav - Дата заявления
 * @property $oklad - Оклад
 * @property $gos_nagr - Государственные награды
 * @property $pens - Пенсионер
 * @property $pens_date - Дата выхода на пенчию
 * @property $lgot - Льготы
 * @property $person_email - Персональный email
 *
 */

class Employee extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = [
        'iin',
        'date_post',
        'date_loyoff',
        'reason_loyoff',
        'contract_num',
        'contract_date',
        'tab_num',
        'birthday',
        'birth_place',
        'ref_sex_id',
        'ref_nationality_id',
        'ref_family_state_id',
        'ref_account_type_id',
        'ref_user_status_id',
        'position_id',
        'email',
        'account',
        'date_zav',
        'oklad',
        'gos_nagr',
        'pens',
        'pens_date',
        'lgot',
        'person_email',
    ];

    protected $searchableFields = ['*'];

    protected $casts = [
        'date_post' => 'date',
        'date_loyoff' => 'date',
        'contract_date' => 'date',
        'birthday' => 'date',
        'date_zav' => 'date',
        'pens' => 'boolean',
        'pens_date' => 'date',
    ];

    /**
     * @return BelongsTo
     * @description Гендерный-справочник
     */
    public function refSex(): BelongsTo
    {
        return $this->belongsTo(RefSex::class);
    }

    public function refNationality()
    {
        return $this->belongsTo(RefNationality::class);
    }

    public function refFamilyState()
    {
        return $this->belongsTo(RefFamilyState::class);
    }

    public function curators()
    {
        return $this->hasMany(Curator::class);
    }

    public function employeePhones()
    {
        return $this->hasMany(EmployeePhone::class);
    }

    public function employeeAddresses()
    {
        return $this->hasMany(EmployeeAddress::class);
    }

    public function employeeMilitaries()
    {
        return $this->hasMany(EmployeeMilitary::class);
    }

    public function refAccountType()
    {
        return $this->belongsTo(RefAccountType::class);
    }

    public function employeeDocuments()
    {
        return $this->hasMany(EmployeeDocument::class);
    }

    public function employeeInvalides()
    {
        return $this->hasMany(EmployeeInvalide::class);
    }

    public function employeePayAllowances()
    {
        return $this->hasMany(EmployeePayAllowance::class);
    }

    public function refUserStatus()
    {
        return $this->belongsTo(RefUserStatus::class);
    }

    public function employeeFamilies()
    {
        return $this->hasMany(EmployeeFamily::class);
    }

    public function employeeEducations()
    {
        return $this->hasMany(EmployeeEducation::class);
    }

    public function employeeStazhs()
    {
        return $this->hasMany(EmployeeStazh::class);
    }

    public function employeeTrips()
    {
        return $this->hasMany(EmployeeTrip::class);
    }

    public function employeeTechics()
    {
        return $this->hasMany(EmployeeTechic::class);
    }

    public function employeeHolidays()
    {
        return $this->hasMany(EmployeeHoliday::class);
    }

    public function employeeHolidaysPeriods()
    {
        return $this->hasMany(EmployeeHolidaysPeriod::class);
    }

    public function t2Cards()
    {
        return $this->hasMany(T2Card::class);
    }

    public function employeeDeclensions()
    {
        return $this->hasMany(EmployeeDeclension::class);
    }

    public function employeeFio($onArray = false)
    {
        $data = EmployeeDeclension::query()->where([
            'employee_id' => $this->id,
            'case_type' => 'im',
            'locale' => app()->getLocale()
        ])->first(['lastname', 'firstname', 'middlename']);
        if(!$data){
            $resFioNull = (object) ['lastname' => '', 'firstname' => '', 'middlename' => ''];
            return ($onArray) ? $resFioNull : '';
        }
        if($onArray){
            return $data;
        }
        return $data->lastname." ".$data->firstname.' '.$data->middlename;
    }

    public function employeeHospitals()
    {
        return $this->hasMany(EmployeeHospital::class);
    }

    public function position()
    {
        return $this->belongsTo(Position::class);
    }


}
