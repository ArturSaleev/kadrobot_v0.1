<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EmployeeDocument extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = [
        'employee_id',
        'ref_doc_type_id',
        'ref_doc_place_id',
        'doc_seria',
        'doc_num',
        'doc_date',
    ];

    protected $searchableFields = ['*'];

    protected $table = 'employee_documents';

    protected $casts = [
        'doc_date' => 'date',
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function refDocType()
    {
        return $this->belongsTo(RefDocType::class);
    }

    public function refDocPlace()
    {
        return $this->belongsTo(RefDocPlace::class);
    }
}
