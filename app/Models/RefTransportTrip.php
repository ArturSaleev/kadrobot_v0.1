<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefTransportTrip extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    use Translatable;

    public $translatedAttributes = ['name'];

    protected $fillable = [];

    protected $searchableFields = ['*'];

    protected $table = 'ref_transport_trips';

    public function employeeTrips()
    {
        return $this->hasMany(EmployeeTrip::class);
    }

    public function employeeTripFromTos()
    {
        return $this->hasMany(EmployeeTripFromTo::class);
    }

    public function refTransportTripTranstions()
    {
        return $this->hasMany(RefTransportTripTranstion::class);
    }
}
