<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BranchAddresesTranslation extends MainModel
{
    use HasFactory;
    use Searchable;

    protected $fillable = ['locale', 'name', 'branch_addreses_id'];

    protected $searchableFields = ['*'];

    protected $table = 'branch_addreses_translations';

    public function branchAddreses()
    {
        return $this->belongsTo(BranchAddreses::class);
    }
}
