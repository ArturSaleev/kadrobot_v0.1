<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefAddressTypeTranslation extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = ['ref_address_type_id', 'locale', 'name'];

    protected $searchableFields = ['*'];

    protected $table = 'ref_address_type_translations';

    public function refAddressType()
    {
        return $this->belongsTo(RefAddressType::class);
    }
}
