<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefVidHolidayType extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    use Translatable;

    public $translatedAttributes = ['name'];

    protected $fillable = [];

    protected $searchableFields = ['*'];

    protected $table = 'ref_vid_holiday_types';

    public function refVidHolidays()
    {
        return $this->hasMany(RefVidHoliday::class);
    }

    public function refVidHolidayTypeTranslations()
    {
        return $this->hasMany(RefVidHolidayTypeTranslation::class);
    }
}
