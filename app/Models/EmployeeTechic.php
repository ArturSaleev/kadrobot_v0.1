<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EmployeeTechic extends MainModel
{
    use HasFactory;
    use Searchable;

    protected $fillable = [
        'employee_id',
        'ref_technic_type_id',
        'invent_num',
        'price',
    ];

    protected $searchableFields = ['*'];

    protected $table = 'employee_techics';

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function refTechnicType()
    {
        return $this->belongsTo(RefTechnicType::class);
    }
}
