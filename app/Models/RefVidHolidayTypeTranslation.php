<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefVidHolidayTypeTranslation extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = ['ref_vid_holiday_type_id', 'locale', 'name'];

    protected $searchableFields = ['*'];

    protected $table = 'ref_vid_holiday_type_translations';

    public function refVidHolidayType()
    {
        return $this->belongsTo(RefVidHolidayType::class);
    }
}
