<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BranchAddreses extends MainModel
{
    use HasFactory;
    use Searchable;

    use Translatable;

    public $translatedAttributes = ['name'];

    protected $fillable = ['branch_id', 'ref_address_type_id'];

    protected $searchableFields = ['*'];

    protected $table = 'branch_addreses';

    public function refAddressType()
    {
        return $this->belongsTo(RefAddressType::class);
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function branchAddresesTranslations()
    {
        return $this->hasMany(BranchAddresesTranslation::class);
    }
}
