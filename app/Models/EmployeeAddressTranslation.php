<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EmployeeAddressTranslation extends MainModel
{
    use HasFactory;
    use Searchable;

    protected $fillable = ['employee_address_id'];

    protected $searchableFields = ['*'];

    protected $table = 'employee_address_translations';

    public function employeeAddress()
    {
        return $this->belongsTo(EmployeeAddress::class);
    }
}
