<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EmployeeEducation extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = [
        'employee_id',
        'institution',
        'year_begin',
        'year_end',
        'date_begin',
        'date_end',
        'speciality',
        'qualification',
        'diplom_num',
        'diplom_date',
    ];

    protected $searchableFields = ['*'];

    protected $table = 'employee_educations';

    protected $casts = [
        'date_begin' => 'date',
        'date_end' => 'date',
        'diplom_date' => 'date',
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
