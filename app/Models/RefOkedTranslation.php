<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefOkedTranslation extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = ['ref_oked_id', 'locale', 'name_oked', 'name'];

    protected $searchableFields = ['*'];

    protected $table = 'ref_oked_translations';

    public function refOked()
    {
        return $this->belongsTo(RefOked::class);
    }
}
