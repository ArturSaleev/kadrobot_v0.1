<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EmployeeDeclension extends MainModel
{
    use HasFactory;
    use Searchable;

    protected $fillable = [
        'employee_id',
        'locale',
        'lastname',
        'firstname',
        'middlename',
        'case_type',
        'type_description',
    ];

    protected $searchableFields = ['*'];

    protected $table = 'employee_declensions';

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
