<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EmployeeStazh extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    use Translatable;

    public $translatedAttributes = ['organization', 'dolgnost', 'address'];

    protected $fillable = ['employee_id', 'date_begin', 'date_end', 'cnt_mes'];

    protected $searchableFields = ['*'];

    protected $table = 'employee_stazhs';

    protected $casts = [
        'date_begin' => 'date',
        'date_end' => 'date',
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function employeeStazhTranslations()
    {
        return $this->hasMany(EmployeeStazhTranslation::class);
    }
}
