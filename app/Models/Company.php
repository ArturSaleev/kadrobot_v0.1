<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Company extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    use Translatable;

    public $translatedAttributes = ['name'];

    protected $fillable = ['ref_status_id', 'bin', 'rnn', 'oked'];

    protected $searchableFields = ['*'];

    public function refStatus()
    {
        return $this->belongsTo(RefStatus::class);
    }

    public function refBranches()
    {
        return $this->hasMany(Branch::class);
    }

    public function t2Cards()
    {
        return $this->hasMany(T2Card::class);
    }

    public function companyTranslations()
    {
        return $this->hasMany(CompanyTranslation::class);
    }
}
