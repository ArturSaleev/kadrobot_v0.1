<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefOked extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    use Translatable;

    public $translatedAttributes = ['name', 'name_oked'];

    protected $fillable = ['oked'];

    protected $searchableFields = ['*'];

    protected $table = 'ref_okeds';

    public function refOkedTransations()
    {
        return $this->hasMany(RefOkedTranslation::class);
    }
}
