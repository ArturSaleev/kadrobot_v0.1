<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class DepartmentTranslation extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = ['department_id', 'locale', 'name'];

    protected $searchableFields = ['*'];

    protected $table = 'department_translations';

    public function department()
    {
        return $this->belongsTo(Department::class);
    }
}
