<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefKindHoliday extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    use Translatable;

    public $translatedAttributes = ['name'];

    protected $fillable = [];

    protected $searchableFields = ['*'];

    protected $table = 'ref_kind_holidays';

    public function refKindHolidayTranslations()
    {
        return $this->hasMany(RefKindHolidayTranslation::class);
    }
}
