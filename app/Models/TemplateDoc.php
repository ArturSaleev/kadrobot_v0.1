<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class TemplateDoc extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    use Translatable;

    public $translatedAttributes = ['name', 'body'];

    protected $table = 'template_docs';

    protected $fillable = ['id_type'];

    protected $searchableFields = ['*'];

    public function templateDocTranslations(): HasMany
    {
        return $this->hasMany(TemplateDocTranslation::class);
    }

    public static function listTypes(): array
    {
        return [
            [
                'id' => 0,
                'name' => __('crud.id_types.pattern')
            ],
            [
                'id' => 1,
                'name' => __('crud.id_types.document')
            ]
        ];
    }
}
