<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Str;
use ReflectionClass;
use ReflectionMethod;

class MainModel extends Model
{
    public function getAllAttributes()
    {
        $columns = $this->getFillable();
        $modelName = $this->getMorphClass();
        $model = str_replace('App\Models\\', '', $modelName);
        $var = strtolower($model);

        $attributes = [];

        $reflection = new ReflectionClass($this);
//        $docComments = $reflection->getDocComment();

        foreach ($columns as $column) {
//            if (!array_key_exists($column, $attributes))
//            {
//                if(!strpos($column, "_id")) {
//                    $attributes[$column] = [
//                        "call" => "$var->$column",
//                        "array" => false,
//                        "title" => ""
//                    ];
//                }
//            }
            if (!strpos($column, "_id")) {
                $attributes[] = [
                    "call" => "$var->$column",
                    "array" => false,
                    "type" => "column",
                    "title" => $this->getComment($column, $this->getTable()),
                ];
            }
        }

        $methods = $reflection->getMethods(ReflectionMethod::IS_PUBLIC);
        $method_exceptions = ["getAllAttributes", "forceDelete", "factory",
            "scopeSearchLatestPaginated", "scopeSearch", "bootSoftDeletes",
            "initializeSoftDeletes", "forceDeleteQuietly", "restore",
            "restoreQuietly", "trashed", "softDeleted", "restoring",
            "restored", "forceDeleting", "forceDeleted", "isForceDeleting",
            "getDeletedAtColumn", "getQualifiedDeletedAtColumn"
        ];

        foreach ($methods as $method) {
            $class = $method->class;
            $name = $method->name;

            if (strpos($class, 'Models') > 0) {
                if (!in_array($name, $method_exceptions)) {
                    $c_name = $reflection->getMethod($name)->name;
                    try {
                        $u = new ReflectionClass($this->$c_name());
                    }catch (\Exception $e){
                        $attributes[] = [
                            "call" => "$var->$name->$c_name()",
                            "array" => false,
                            "type" => "method",
                            "title" => "",
                        ];
                        continue;
                    }
                    $type = str_replace('Illuminate\Database\Eloquent\Relations\\', '', $u->getName());
                    $className = "App\\Models\\" . Str::studly(Str::singular($c_name));

                    if (class_exists($className)) {
                        $newClass = new $className();
                        $columns = [];
                        foreach ($newClass->fillable as $value) {
                            $columns[] = $value;
                        }
                        $translates = $newClass->translatedAttributes;
                        if ($translates !== null) {
                            foreach ($translates as $translate) {
                                foreach (config('panel.available_languages', []) as $lang => $name) {
                                    $columns[] = "translation('$lang')->$translate";
                                }
                            }
                        }

                        foreach ($columns as $column) {
                            $callName = "$var->$c_name";
                            if($type !== 'BelongsTo'){
                                $callName .= "()";
                            }else{
                                $callName .= "->".$column;
                            }

                            $attributes[] = [
                                "call" => $callName,
                                "loop_column" => ($type !== 'BelongsTo') ? $column : "",
                                "array" => ($type !== 'BelongsTo'),
                                "type" => $type,
                                "title" => $this->getComment($column, $newClass->getTable()),
                            ];
                        }
                    }
                }
            }
        }

        return $attributes;
    }

    private function getComment($name, $tableName)
    {
        $crud = __("crud");
        $title = $crud[$tableName]['show_title'] ?? "";
        $result = $crud[$tableName]['inputs'][$name] ?? "";

        if(strpos($name, 'translation') !== false){
            $new_name = str_replace("'", '', $name);
            $new_name = str_replace('"', '', $new_name);
            $new_name = str_replace('translation', '', $new_name);
            $new_name = str_replace('(', '', $new_name);
            $new_name = str_replace(')', '', $new_name);
            $split_translate = explode('->', $new_name);
            $lang = $split_translate[0];
            $column = $split_translate[1];
            $result = $crud[$tableName]['inputs'][$column] ?? "";
            if($result === ""){
                dd($name, $tableName);
            }
            $result.= " (".$crud['lang_input'][$lang].")";
        }
        return $title." - ".$result;
    }


    private function getModelRelationshipMethods(string $modelClass)
    {
        //can define this at class level
        $relationshipMethods = [
            'hasMany',
            'hasOne',
            'belongsTo',
            'belongsToMany',
        ];

        $reflector = new ReflectionClass($modelClass);
        $path = $reflector->getFileName();
        //lines of the file
        $lines = file($path);
        $methods = $reflector->getMethods();
        $relations = [];
        foreach ($methods as $method) {
            //if its a concrete class method
            if ($method->class == $modelClass) {
                $start = $method->getStartLine();
                $end = $method->getEndLine();
                //loop through lines of the method
                for ($i = $start - 1; $i <= $end - 1; $i++) {
                    // look for text between -> and ( assuming that its on one line
                    preg_match('~\->(.*?)\(~', $lines[$i], $matches);
                    // if there is a match
                    if (count($matches)) {
                        //loop to check if the found text is in relationshipMethods list
                        foreach ($matches as $match) {
                            // if so add it to the output array
                            if (in_array($match, $relationshipMethods)) {
                                $relations[] = [
                                    //function name of the relation definition
                                    'method_name' => $method->name,
                                    //type of relation
                                    'relation' => $match,
                                    //related Class name
                                    'related' => (preg_match('/' . $match . '\((.*?),/', $lines[$i], $related) == 1) ? $related[1] : null,
                                ];
                            }
                        }
                    }
                }
            }
        }

        return $relations;
    }
}
