<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EmployeeFamily extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = [
        'employee_id',
        'lastname',
        'firstname',
        'middlename',
        'birthday',
        'ref_family_state_id',
    ];

    protected $searchableFields = ['*'];

    protected $table = 'employee_families';

    protected $casts = [
        'birthday' => 'date',
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function refFamilyState()
    {
        return $this->belongsTo(RefFamilyState::class);
    }
}
