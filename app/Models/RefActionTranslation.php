<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefActionTranslation extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = ['ref_action_id', 'locale', 'name'];

    protected $searchableFields = ['*'];

    protected $table = 'ref_action_translations';

    public function refAction()
    {
        return $this->belongsTo(RefAction::class);
    }
}
