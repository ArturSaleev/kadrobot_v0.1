<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PositionDeclension extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = [
        'position_id',
        'locale',
        'value',
        'case_type',
        'type_description',
    ];

    protected $searchableFields = ['*'];

    protected $table = 'position_declensions';

    public function position()
    {
        return $this->belongsTo(Position::class);
    }
}
