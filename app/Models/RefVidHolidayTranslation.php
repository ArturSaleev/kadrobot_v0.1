<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefVidHolidayTranslation extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = ['ref_vid_holiday_id', 'locale', 'name'];

    protected $searchableFields = ['*'];

    protected $table = 'ref_vid_holiday_translations';

    public function refVidHoliday()
    {
        return $this->belongsTo(RefVidHoliday::class);
    }
}
