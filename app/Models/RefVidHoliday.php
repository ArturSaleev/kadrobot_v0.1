<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefVidHoliday extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    use Translatable;

    public $translatedAttributes = ['name'];

    protected $fillable = ['ref_vid_holiday_type_id'];

    protected $searchableFields = ['*'];

    protected $table = 'ref_vid_holidays';

    public function refVidHolidayType()
    {
        return $this->belongsTo(RefVidHolidayType::class);
    }

    public function employeeHolidays()
    {
        return $this->hasMany(EmployeeHoliday::class);
    }

    public function refVidHolidayTranslates()
    {
        return $this->hasMany(RefVidHolidayTranslation::class);
    }
}
