<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefAccountTypeTranslation extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = ['ref_account_type_id', 'locale', 'name'];

    protected $searchableFields = ['*'];

    protected $table = 'ref_account_type_translations';

    public function refAccountType()
    {
        return $this->belongsTo(RefAccountType::class);
    }
}
