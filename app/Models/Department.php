<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Department extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    use Translatable;

    public $translatedAttributes = ['name'];

    protected $fillable = ['parent_id', 'branch_id', 'email'];

    protected $searchableFields = ['*'];

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function departmentPhones()
    {
        return $this->hasMany(DepartmentPhone::class);
    }

    public function curators()
    {
        return $this->hasMany(Curator::class);
    }

    public function departmentTranstions()
    {
        return $this->hasMany(DepartmentTranslation::class);
    }

    public function positions()
    {
        return $this->hasMany(Position::class);
    }
}
