<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EmployeeAddress extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    use Translatable;

    public $translatedAttributes = ['name'];

    protected $fillable = ['employee_id', 'locale', 'ref_address_type_id'];

    protected $searchableFields = ['*'];

    protected $table = 'employee_addresses';

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function refAddressType()
    {
        return $this->belongsTo(RefAddressType::class);
    }

    public function employeeAddressTranslations()
    {
        return $this->hasMany(EmployeeAddressTranslation::class);
    }
}
