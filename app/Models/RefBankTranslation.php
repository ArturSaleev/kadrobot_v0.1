<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefBankTranslation extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = ['ref_bank_id', 'locale', 'name'];

    protected $searchableFields = ['*'];

    protected $table = 'ref_bank_translations';

    public function refBank()
    {
        return $this->belongsTo(RefBank::class);
    }
}
