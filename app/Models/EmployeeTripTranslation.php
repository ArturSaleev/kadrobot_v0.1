<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EmployeeTripTranslation extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = [
        'employee_trip_id',
        'locale',
        'aim',
        'final_distantion',
    ];

    protected $searchableFields = ['*'];

    protected $table = 'employee_trip_translations';

    public function employeeTrip()
    {
        return $this->belongsTo(EmployeeTrip::class);
    }
}
