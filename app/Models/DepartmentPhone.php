<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class DepartmentPhone extends MainModel
{
    use HasFactory;
    use Searchable;

    protected $fillable = ['department_id', 'ref_type_phone_id', 'name'];

    protected $searchableFields = ['*'];

    protected $table = 'department_phones';

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function refTypePhone()
    {
        return $this->belongsTo(RefTypePhone::class);
    }
}
