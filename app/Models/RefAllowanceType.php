<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefAllowanceType extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    use Translatable;

    public $translatedAttributes = ['name'];

    protected $fillable = [];

    protected $searchableFields = ['*'];

    protected $table = 'ref_allowance_types';

    public function employeePayAllowances()
    {
        return $this->hasMany(EmployeePayAllowance::class);
    }

    public function refAllowanceTypeTranslations()
    {
        return $this->hasMany(RefAllowanceTypeTranslation::class);
    }
}
