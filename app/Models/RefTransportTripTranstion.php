<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefTransportTripTranstion extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = ['ref_transport_trip_id', 'locale', 'name'];

    protected $searchableFields = ['*'];

    protected $table = 'ref_transport_trip_transtions';

    public function refTransportTrip()
    {
        return $this->belongsTo(RefTransportTrip::class);
    }
}
