<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefNationalityTranslation extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = ['ref_nationality_id', 'locale', 'name'];

    protected $searchableFields = ['*'];

    protected $table = 'ref_nationality_translations';

    public function refNationality()
    {
        return $this->belongsTo(RefNationality::class);
    }
}
