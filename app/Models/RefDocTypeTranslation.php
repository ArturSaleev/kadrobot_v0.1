<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefDocTypeTranslation extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = ['ref_doc_type_id', 'locale', 'name'];

    protected $searchableFields = ['*'];

    protected $table = 'ref_doc_type_translations';

    public function refDocType()
    {
        return $this->belongsTo(RefDocType::class);
    }
}
