<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefDocPlace extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    use Translatable;

    public $translatedAttributes = ['name'];

    protected $fillable = [];

    protected $searchableFields = ['*'];

    protected $table = 'ref_doc_places';

    public function employeeDocuments()
    {
        return $this->hasMany(EmployeeDocument::class);
    }

    public function refDocPlaceTranlations()
    {
        return $this->hasMany(RefDocPlaceTranslation::class);
    }
}
