<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EmployeePhone extends MainModel
{
    use HasFactory;
    use Searchable;

    protected $fillable = ['ref_type_phone_id', 'employee_id', 'phone'];

    protected $searchableFields = ['*'];

    protected $table = 'employee_phones';

    public function refTypePhone()
    {
        return $this->belongsTo(RefTypePhone::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
