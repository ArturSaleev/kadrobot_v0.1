<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BranchPhone extends MainModel
{
    use HasFactory;
    use Searchable;

    protected $fillable = ['branch_id', 'ref_type_phone_id', 'name'];

    protected $searchableFields = ['*'];

    protected $table = 'branch_phones';

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function refTypePhone()
    {
        return $this->belongsTo(RefTypePhone::class);
    }
}
