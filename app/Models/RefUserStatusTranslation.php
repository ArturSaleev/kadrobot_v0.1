<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefUserStatusTranslation extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = ['ref_user_status_id', 'locale', 'name'];

    protected $searchableFields = ['*'];

    protected $table = 'ref_user_status_translations';

    public function refUserStatus()
    {
        return $this->belongsTo(RefUserStatus::class);
    }
}
