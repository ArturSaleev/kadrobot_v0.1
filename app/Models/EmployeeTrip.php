<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EmployeeTrip extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    use Translatable;

    public $translatedAttributes = ['name'];

    protected $fillable = [
        'employee_id',
        'ref_transport_trip_id',
        'date_begin',
        'date_end',
        'cnt_days',
        'order_num',
        'order_date',
    ];

    protected $searchableFields = ['*'];

    protected $table = 'employee_trips';

    protected $casts = [
        'date_begin' => 'date',
        'date_end' => 'date',
        'order_date' => 'date',
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function refTransportTrip()
    {
        return $this->belongsTo(RefTransportTrip::class);
    }

    public function employeeTripFromTos()
    {
        return $this->hasMany(EmployeeTripFromTo::class);
    }

    public function employeeTripTranslations()
    {
        return $this->hasMany(EmployeeTripTranslation::class);
    }
}
