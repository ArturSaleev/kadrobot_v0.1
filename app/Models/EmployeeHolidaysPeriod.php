<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EmployeeHolidaysPeriod extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = [
        'employee_id',
        'period_start',
        'period_end',
        'day_count_used_for_today',
        'didnt_add',
        'paying_for_health',
    ];

    protected $searchableFields = ['*'];

    protected $table = 'employee_holidays_periods';

    protected $casts = [
        'period_start' => 'date',
        'period_end' => 'date',
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
