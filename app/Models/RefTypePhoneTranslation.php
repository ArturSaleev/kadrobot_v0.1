<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefTypePhoneTranslation extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = ['ref_type_phone_id', 'locale', 'name'];

    protected $searchableFields = ['*'];

    protected $table = 'ref_type_phone_translations';

    public function refTypePhone()
    {
        return $this->belongsTo(RefTypePhone::class);
    }
}
