<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TemplateDocTranslation extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = ['template_doc_id', 'locale', 'name', 'body'];

    protected $searchableFields = ['*'];

    protected $table = 'template_doc_translations';

    public function templateDoc()
    {
        return $this->belongsTo(TemplateDoc::class);
    }

}
