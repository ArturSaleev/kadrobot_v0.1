<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefTypePhone extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    use Translatable;

    public $translatedAttributes = ['name'];

    protected $fillable = [];

    protected $searchableFields = ['*'];

    protected $table = 'ref_type_phones';

    public function employeePhones()
    {
        return $this->hasMany(EmployeePhone::class);
    }

    public function departmentPhones()
    {
        return $this->hasMany(DepartmentPhone::class);
    }

    public function refTypePhoneTranslations()
    {
        return $this->hasMany(RefTypePhoneTranslation::class);
    }

    public function branchPhones()
    {
        return $this->hasMany(BranchPhone::class);
    }
}
