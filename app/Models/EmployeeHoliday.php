<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EmployeeHoliday extends MainModel
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected $fillable = [
        'employee_id',
        'date_begin',
        'date_end',
        'cnt_days',
        'period_begin',
        'period_end',
        'order_num',
        'order_date',
        'ref_vid_holiday_id',
        'deligate',
        'deligate_emp_id',
        'doc_content',
    ];

    protected $searchableFields = ['*'];

    protected $table = 'employee_holidays';

    protected $casts = [
        'date_begin' => 'date',
        'date_end' => 'date',
        'period_begin' => 'date',
        'period_end' => 'date',
        'order_date' => 'date',
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function refVidHoliday()
    {
        return $this->belongsTo(RefVidHoliday::class);
    }
}
