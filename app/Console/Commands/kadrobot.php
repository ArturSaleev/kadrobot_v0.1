<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class kadrobot extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kadrobot:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Запуск развертывания приложения';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Artisan::call("config:clear");
        $this->info("Config clear");

        Artisan::call("cache:clear");
        $this->info("Cache clear");

        $res = exec("composer dump-autoload");
        $this->info("Dump autoload ".$res);

        Artisan::call("db:wipe --force");
        $this->info("Remove all tables on database");

        Artisan::call("migrate --seed");
        Artisan::call("storage:link");
        return Command::SUCCESS;
    }
}
