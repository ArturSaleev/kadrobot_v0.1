<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PositionStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cnt' => ['required', 'numeric'],
            'pos_level' => ['sometimes', 'numeric'],
            'min_salary' => ['sometimes', 'numeric'],
            'max_salary' => ['sometimes', 'numeric'],
            'department_id' => ['required', 'exists:branches,id'],
            'name' => ['required', 'array'],
            'declensions' => ['sometimes', 'array'],
            'declensions_descript' => ['sometimes', 'array'],
        ];
    }
}
