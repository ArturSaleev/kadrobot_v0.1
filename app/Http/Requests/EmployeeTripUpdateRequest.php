<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeTripUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_id' => ['required', 'exists:employees,id'],
            'ref_transport_trip_id' => [
                'required',
                'exists:ref_transport_trips,id',
            ],
            'date_begin' => ['required', 'date'],
            'date_end' => ['required', 'date'],
            'cnt_days' => ['required', 'numeric'],
            'order_num' => ['required', 'max:255', 'string'],
            'order_date' => ['required', 'date'],
        ];
    }
}
