<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeEducationStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_id' => ['required', 'exists:employees,id'],
            'institution' => ['required', 'max:255', 'string'],
            'year_begin' => ['nullable', 'numeric'],
            'year_end' => ['nullable', 'numeric'],
            'date_begin' => ['nullable', 'date'],
            'date_end' => ['nullable', 'date'],
            'speciality' => ['nullable', 'max:255', 'string'],
            'qualification' => ['nullable', 'max:255', 'string'],
            'diplom_num' => ['nullable', 'max:255', 'string'],
            'diplom_date' => ['nullable', 'date'],
        ];
    }
}
