<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeePayAllowanceUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_id' => ['required', 'exists:employees,id'],
            'ref_allowance_type_id' => [
                'required',
                'exists:ref_allowance_types,id',
            ],
            'date_add' => ['required', 'date'],
            'period_begin' => ['required', 'date'],
            'period_end' => ['required', 'date'],
            'paysum' => ['required', 'numeric'],
        ];
    }
}
