<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class T2CardStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => ['required', 'exists:companies,id'],
            'employee_id' => ['required', 'exists:employees,id'],
            'ref_branch_id' => ['required', 'exists:branches,id'],
            'ref_action_id' => ['required', 'exists:ref_actions,id'],
            'ref_position_id' => ['required', 'exists:positions,id'],
        ];
    }
}
