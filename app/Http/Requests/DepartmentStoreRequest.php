<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DepartmentStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'parent_id' => ['sometimes', 'max:255'],
            'branch_id' => ['required', 'exists:branches,id'],
            'email' => ['required', 'email'],
            'name' => ['required', 'array'],
            'phones' => ['sometimes', 'array']
        ];
    }
}
