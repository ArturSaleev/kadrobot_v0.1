<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeDocumentStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_id' => ['required', 'exists:employees,id'],
            'ref_doc_type_id' => ['required', 'exists:ref_doc_types,id'],
            'ref_doc_place_id' => ['required', 'exists:ref_doc_places,id'],
            'doc_seria' => ['nullable', 'max:255', 'string'],
            'doc_num' => ['required', 'max:255', 'string'],
            'doc_date' => ['required', 'date'],
        ];
    }
}
