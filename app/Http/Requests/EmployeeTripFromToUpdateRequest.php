<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeTripFromToUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_trip_id' => ['required', 'exists:employee_trips,id'],
            'from_place' => ['required', 'max:255', 'string'],
            'to_place' => ['required', 'max:255', 'string'],
            'ref_transport_trip_id' => [
                'required',
                'exists:ref_transport_trips,id',
            ],
        ];
    }
}
