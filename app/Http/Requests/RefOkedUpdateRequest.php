<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class RefOkedUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'oked' => [
                'required',
//                Rule::unique('ref_okeds', 'oked')->ignore($this->refOked),
                'max:255',
                'string',
            ],
            'name' => ['required', 'array'],
            'name_oked' => ['required', 'array']
        ];
    }
}
