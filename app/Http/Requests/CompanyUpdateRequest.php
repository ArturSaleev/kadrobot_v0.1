<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bin' => ['required', 'max:255', 'string'],
//            'ref_status_id' => ['required', 'exists:ref_statuses,id'],
            'rnn' => ['required', 'max:255', 'string'],
            'oked' => ['sometimes'],
            'name' => ['required', 'array'],
        ];
    }
}
