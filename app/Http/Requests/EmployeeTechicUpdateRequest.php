<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeTechicUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_id' => ['required', 'exists:employees,id'],
            'ref_technic_type_id' => [
                'required',
                'exists:ref_technic_types,id',
            ],
            'invent_num' => ['nullable', 'max:255', 'string'],
            'price' => ['nullable', 'numeric'],
        ];
    }
}
