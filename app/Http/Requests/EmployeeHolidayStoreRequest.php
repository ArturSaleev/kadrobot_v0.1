<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeHolidayStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_id' => ['required', 'exists:employees,id'],
            'date_begin' => ['nullable', 'date'],
            'date_end' => ['nullable', 'date'],
            'cnt_days' => ['nullable', 'numeric'],
            'period_begin' => ['nullable', 'date'],
            'period_end' => ['nullable', 'date'],
            'order_num' => ['nullable', 'max:255', 'string'],
            'order_date' => ['nullable', 'date'],
            'ref_vid_holiday_id' => ['required', 'exists:ref_vid_holidays,id'],
            'deligate' => ['nullable', 'numeric'],
            'deligate_emp_id' => ['nullable', 'max:255'],
            'doc_content' => ['required', 'max:255', 'string'],
        ];
    }
}
