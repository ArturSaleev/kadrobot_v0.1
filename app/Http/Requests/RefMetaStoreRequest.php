<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RefMetaStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['nullable', 'max:255', 'string'],
            'name' => ['nullable', 'max:255', 'string'],
            'tablename' => ['nullable', 'max:255', 'string'],
            'column_name' => ['nullable', 'max:255', 'string'],
        ];
    }
}
