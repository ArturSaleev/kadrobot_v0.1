<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RefBankUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ref_status_id' => ['required', 'exists:ref_statuses,id'],
            'mfo' => ['nullable', 'max:255', 'string'],
            'mfo_head' => ['nullable', 'max:255', 'string'],
            'mfo_rkc' => ['nullable', 'max:255', 'string'],
            'kor_account' => ['nullable', 'max:255', 'string'],
            'commis' => ['required', 'numeric'],
            'bin' => ['nullable', 'max:255', 'string'],
            'bik_old' => ['nullable', 'max:255', 'string'],
            'name' => ['required', 'array']
        ];
    }
}
