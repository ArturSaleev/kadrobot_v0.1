<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BranchPhoneStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'branch_id' => ['required', 'exists:branches,id'],
            'ref_type_phone_id' => ['required', 'exists:ref_type_phones,id'],
            'name' => ['required', 'max:255', 'string'],
        ];
    }
}
