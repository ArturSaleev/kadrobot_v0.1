<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RefVidHolidayStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ref_vid_holiday_type_id' => [
                'required',
                'exists:ref_vid_holiday_types,id',
            ],
        ];
    }
}
