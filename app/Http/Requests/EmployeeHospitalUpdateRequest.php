<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeHospitalUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date_begin' => ['required', 'date'],
            'date_end' => ['required', 'date'],
            'cnt_days' => ['required', 'numeric'],
            'employee_id' => ['required', 'exists:employees,id'],
        ];
    }
}
