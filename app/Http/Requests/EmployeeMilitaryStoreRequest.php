<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeMilitaryStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_id' => ['required', 'exists:employees,id'],
            'group' => ['required', 'max:255', 'string'],
            'category' => ['required', 'max:255', 'string'],
            'rank' => ['required', 'max:255', 'string'],
            'speciality' => ['required', 'max:255', 'string'],
            'voenkom' => ['required', 'max:255', 'string'],
            'spec_uch' => ['required', 'max:255', 'string'],
            'spec_uch_num' => ['required', 'max:255', 'string'],
            'fit' => ['required', 'max:255', 'string'],
        ];
    }
}
