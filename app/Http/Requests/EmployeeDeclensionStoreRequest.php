<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeDeclensionStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_id' => ['required', 'exists:employees,id'],
            'locale' => ['required', 'max:8', 'string'],
            'lastname' => ['required', 'max:255', 'string'],
            'firstname' => ['required', 'max:255', 'string'],
            'middlename' => ['nullable', 'max:255', 'string'],
            'case_type' => ['nullable', 'max:255', 'string'],
        ];
    }
}
