<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeInvalideUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_id' => ['required', 'exists:employees,id'],
            'num' => ['nullable', 'max:255', 'string'],
            'date_add' => ['nullable', 'date'],
            'period_begin' => ['nullable', 'date'],
            'period_end' => ['nullable', 'date'],
        ];
    }
}
