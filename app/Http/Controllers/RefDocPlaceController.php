<?php

namespace App\Http\Controllers;

use App\Models\RefDocPlace;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Http\Requests\RefDocPlaceStoreRequest;
use App\Http\Requests\RefDocPlaceUpdateRequest;
use Illuminate\Http\Response;

class RefDocPlaceController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefDocPlace::class);

        $search = $request->get('search', '');

        $refDocPlaces = RefDocPlace::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.ref_doc_places.index',
            compact('refDocPlaces', 'search')
        );
    }

    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function create(Request $request)
    {
        $this->authorize('create', RefDocPlace::class);

        return view('app.ref_doc_places.create');
    }

    /**
     * @param RefDocPlaceStoreRequest $request
     * @return Response
     * @throws AuthorizationException
     */
    public function store(RefDocPlaceStoreRequest $request)
    {
        $this->authorize('create', RefDocPlace::class);

        $validated = $request->validated();

        $refDocPlace = new RefDocPlace();
        $refDocPlace = $this->save($validated, $refDocPlace);

        return redirect()
            ->route('ref-doc-places.edit', $refDocPlace)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param Request $request
     * @param RefDocPlace $refDocPlace
     * @return Response
     * @throws AuthorizationException
     */
    public function show(Request $request, RefDocPlace $refDocPlace)
    {
        $this->authorize('view', $refDocPlace);

        return view('app.ref_doc_places.show', compact('refDocPlace'));
    }

    /**
     * @param Request $request
     * @param RefDocPlace $refDocPlace
     * @return Response
     * @throws AuthorizationException
     */
    public function edit(Request $request, RefDocPlace $refDocPlace)
    {
        $this->authorize('update', $refDocPlace);

        return view('app.ref_doc_places.edit', compact('refDocPlace'));
    }

    /**
     * @param RefDocPlaceUpdateRequest $request
     * @param RefDocPlace $refDocPlace
     * @return Response
     * @throws AuthorizationException
     */
    public function update(
        RefDocPlaceUpdateRequest $request,
        RefDocPlace $refDocPlace
    ) {
        $this->authorize('update', $refDocPlace);

        $validated = $request->validated();

        $this->save($validated, $refDocPlace);

        return redirect()
            ->route('ref-doc-places.edit', $refDocPlace)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param Request $request
     * @param RefDocPlace $refDocPlace
     * @return Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request, RefDocPlace $refDocPlace)
    {
        $this->authorize('delete', $refDocPlace);

        $refDocPlace->delete();

        return redirect()
            ->route('ref-doc-places.index')
            ->withSuccess(__('crud.common.removed'));
    }

    private function save($validated, $model)
    {
        foreach($validated['name'] as $lang => $value){
            $model->translateOrNew($lang)->name = $value;
        }
        $model->save();
        return $model;
    }
}
