<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\EmployeeTechic;
use App\Models\RefTechnicType;
use App\Http\Requests\EmployeeTechicStoreRequest;
use App\Http\Requests\EmployeeTechicUpdateRequest;

class EmployeeTechicController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeeTechic::class);

        $search = $request->get('search', '');

        $employeeTechics = EmployeeTechic::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.employee_techics.index',
            compact('employeeTechics', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', EmployeeTechic::class);

        $employees = Employee::pluck('iin', 'id');
        $refTechnicTypes = RefTechnicType::pluck('name', 'id');

        return view(
            'app.employee_techics.create',
            compact('employees', 'refTechnicTypes')
        );
    }

    /**
     * @param \App\Http\Requests\EmployeeTechicStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeTechicStoreRequest $request)
    {
        $this->authorize('create', EmployeeTechic::class);

        $validated = $request->validated();

        $employeeTechic = EmployeeTechic::create($validated);

        return redirect()
            ->route('employee-techics.edit', $employeeTechic)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeTechic $employeeTechic
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EmployeeTechic $employeeTechic)
    {
        $this->authorize('view', $employeeTechic);

        return view('app.employee_techics.show', compact('employeeTechic'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeTechic $employeeTechic
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, EmployeeTechic $employeeTechic)
    {
        $this->authorize('update', $employeeTechic);

        $employees = Employee::pluck('iin', 'id');
        $refTechnicTypes = RefTechnicType::pluck('name', 'id');

        return view(
            'app.employee_techics.edit',
            compact('employeeTechic', 'employees', 'refTechnicTypes')
        );
    }

    /**
     * @param \App\Http\Requests\EmployeeTechicUpdateRequest $request
     * @param \App\Models\EmployeeTechic $employeeTechic
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeeTechicUpdateRequest $request,
        EmployeeTechic $employeeTechic
    ) {
        $this->authorize('update', $employeeTechic);

        $validated = $request->validated();

        $employeeTechic->update($validated);

        return redirect()
            ->route('employee-techics.edit', $employeeTechic)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeTechic $employeeTechic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, EmployeeTechic $employeeTechic)
    {
        $this->authorize('delete', $employeeTechic);

        $employeeTechic->delete();

        return redirect()
            ->route('employee-techics.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
