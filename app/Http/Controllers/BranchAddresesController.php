<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use Illuminate\Http\Request;
use App\Models\BranchAddreses;
use App\Models\RefAddressType;
use App\Http\Requests\BranchAddresesStoreRequest;
use App\Http\Requests\BranchAddresesUpdateRequest;

class BranchAddresesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', BranchAddreses::class);

        $search = $request->get('search', '');

        $allBranchAddreses = BranchAddreses::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.all_branch_addreses.index',
            compact('allBranchAddreses', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', BranchAddreses::class);

        $branches = Branch::pluck('id', 'id');
        $refAddressTypes = RefAddressType::pluck('id', 'id');

        return view(
            'app.all_branch_addreses.create',
            compact('branches', 'refAddressTypes')
        );
    }

    /**
     * @param \App\Http\Requests\BranchAddresesStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(BranchAddresesStoreRequest $request)
    {
        $this->authorize('create', BranchAddreses::class);

        $validated = $request->validated();

        $branchAddreses = BranchAddreses::create($validated);

        return redirect()
            ->route('all-branch-addreses.edit', $branchAddreses)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\BranchAddreses $branchAddreses
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, BranchAddreses $branchAddreses)
    {
        $this->authorize('view', $branchAddreses);

        return view('app.all_branch_addreses.show', compact('branchAddreses'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\BranchAddreses $branchAddreses
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, BranchAddreses $branchAddreses)
    {
        $this->authorize('update', $branchAddreses);

        $branches = Branch::pluck('id', 'id');
        $refAddressTypes = RefAddressType::pluck('id', 'id');

        return view(
            'app.all_branch_addreses.edit',
            compact('branchAddreses', 'branches', 'refAddressTypes')
        );
    }

    /**
     * @param \App\Http\Requests\BranchAddresesUpdateRequest $request
     * @param \App\Models\BranchAddreses $branchAddreses
     * @return \Illuminate\Http\Response
     */
    public function update(
        BranchAddresesUpdateRequest $request,
        BranchAddreses $branchAddreses
    ) {
        $this->authorize('update', $branchAddreses);

        $validated = $request->validated();

        $branchAddreses->update($validated);

        return redirect()
            ->route('all-branch-addreses.edit', $branchAddreses)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\BranchAddreses $branchAddreses
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, BranchAddreses $branchAddreses)
    {
        $this->authorize('delete', $branchAddreses);

        $branchAddreses->delete();

        return redirect()
            ->route('all-branch-addreses.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
