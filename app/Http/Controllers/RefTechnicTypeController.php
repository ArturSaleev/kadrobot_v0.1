<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RefTechnicType;
use App\Http\Requests\RefTechnicTypeStoreRequest;
use App\Http\Requests\RefTechnicTypeUpdateRequest;

class RefTechnicTypeController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefTechnicType::class);

        $search = $request->get('search', '');

        $refTechnicTypes = RefTechnicType::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.ref_technic_types.index',
            compact('refTechnicTypes', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', RefTechnicType::class);

        return view('app.ref_technic_types.create');
    }

    /**
     * @param \App\Http\Requests\RefTechnicTypeStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefTechnicTypeStoreRequest $request)
    {
        $this->authorize('create', RefTechnicType::class);

        $validated = $request->validated();

        $refTechnicType = RefTechnicType::create($validated);

        return redirect()
            ->route('ref-technic-types.edit', $refTechnicType)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTechnicType $refTechnicType
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefTechnicType $refTechnicType)
    {
        $this->authorize('view', $refTechnicType);

        return view('app.ref_technic_types.show', compact('refTechnicType'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTechnicType $refTechnicType
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, RefTechnicType $refTechnicType)
    {
        $this->authorize('update', $refTechnicType);

        return view('app.ref_technic_types.edit', compact('refTechnicType'));
    }

    /**
     * @param \App\Http\Requests\RefTechnicTypeUpdateRequest $request
     * @param \App\Models\RefTechnicType $refTechnicType
     * @return \Illuminate\Http\Response
     */
    public function update(
        RefTechnicTypeUpdateRequest $request,
        RefTechnicType $refTechnicType
    ) {
        $this->authorize('update', $refTechnicType);

        $validated = $request->validated();

        $refTechnicType->update($validated);

        return redirect()
            ->route('ref-technic-types.edit', $refTechnicType)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTechnicType $refTechnicType
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, RefTechnicType $refTechnicType)
    {
        $this->authorize('delete', $refTechnicType);

        $refTechnicType->delete();

        return redirect()
            ->route('ref-technic-types.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
