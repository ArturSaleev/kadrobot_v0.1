<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\RefAllowanceType;
use App\Models\EmployeePayAllowance;
use App\Http\Requests\EmployeePayAllowanceStoreRequest;
use App\Http\Requests\EmployeePayAllowanceUpdateRequest;

class EmployeePayAllowanceController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeePayAllowance::class);

        $search = $request->get('search', '');

        $employeePayAllowances = EmployeePayAllowance::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.employee_pay_allowances.index',
            compact('employeePayAllowances', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', EmployeePayAllowance::class);

        $employees = Employee::pluck('iin', 'id');
        $refAllowanceTypes = RefAllowanceType::pluck('id', 'id');

        return view(
            'app.employee_pay_allowances.create',
            compact('employees', 'refAllowanceTypes')
        );
    }

    /**
     * @param \App\Http\Requests\EmployeePayAllowanceStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeePayAllowanceStoreRequest $request)
    {
        $this->authorize('create', EmployeePayAllowance::class);

        $validated = $request->validated();

        $employeePayAllowance = EmployeePayAllowance::create($validated);

        return redirect()
            ->route('employee-pay-allowances.edit', $employeePayAllowance)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeePayAllowance $employeePayAllowance
     * @return \Illuminate\Http\Response
     */
    public function show(
        Request $request,
        EmployeePayAllowance $employeePayAllowance
    ) {
        $this->authorize('view', $employeePayAllowance);

        return view(
            'app.employee_pay_allowances.show',
            compact('employeePayAllowance')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeePayAllowance $employeePayAllowance
     * @return \Illuminate\Http\Response
     */
    public function edit(
        Request $request,
        EmployeePayAllowance $employeePayAllowance
    ) {
        $this->authorize('update', $employeePayAllowance);

        $employees = Employee::pluck('iin', 'id');
        $refAllowanceTypes = RefAllowanceType::pluck('id', 'id');

        return view(
            'app.employee_pay_allowances.edit',
            compact('employeePayAllowance', 'employees', 'refAllowanceTypes')
        );
    }

    /**
     * @param \App\Http\Requests\EmployeePayAllowanceUpdateRequest $request
     * @param \App\Models\EmployeePayAllowance $employeePayAllowance
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeePayAllowanceUpdateRequest $request,
        EmployeePayAllowance $employeePayAllowance
    ) {
        $this->authorize('update', $employeePayAllowance);

        $validated = $request->validated();

        $employeePayAllowance->update($validated);

        return redirect()
            ->route('employee-pay-allowances.edit', $employeePayAllowance)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeePayAllowance $employeePayAllowance
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        Request $request,
        EmployeePayAllowance $employeePayAllowance
    ) {
        $this->authorize('delete', $employeePayAllowance);

        $employeePayAllowance->delete();

        return redirect()
            ->route('employee-pay-allowances.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
