<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Models\RefAddressType;
use App\Http\Requests\RefAddressTypeStoreRequest;
use App\Http\Requests\RefAddressTypeUpdateRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class RefAddressTypeController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefAddressType::class);

        $search = $request->get('search', '');

        $refAddressTypes = RefAddressType::search($search)
            ->paginate()
            ->withQueryString();

        return view(
            'app.ref_address_types.index',
            compact('refAddressTypes', 'search')
        );
    }

    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function create(Request $request)
    {
        $this->authorize('create', RefAddressType::class);

        return view('app.ref_address_types.create');
    }

    /**
     * @param RefAddressTypeStoreRequest $request
     * @return Response
     * @throws AuthorizationException
     */
    public function store(RefAddressTypeStoreRequest $request)
    {
        $this->authorize('create', RefAddressType::class);

        $validated = $request->validated();

        $refAddressType = new RefAddressType();
        $refAddressType = $this->save($validated, $refAddressType);

        return redirect()
            ->route('ref-address-types.edit', $refAddressType)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param Request $request
     * @param RefAddressType $refAddressType
     * @return Response
     * @throws AuthorizationException
     */
    public function show(Request $request, RefAddressType $refAddressType)
    {
        $this->authorize('view', $refAddressType);

        return view('app.ref_address_types.show', compact('refAddressType'));
    }

    /**
     * @param Request $request
     * @param RefAddressType $refAddressType
     * @return Response
     * @throws AuthorizationException
     */
    public function edit(Request $request, RefAddressType $refAddressType)
    {
        $this->authorize('update', $refAddressType);

        return view('app.ref_address_types.edit', compact('refAddressType'));
    }

    /**
     * @param RefAddressTypeUpdateRequest $request
     * @param RefAddressType $refAddressType
     * @return Response
     * @throws AuthorizationException
     */
    public function update(
        RefAddressTypeUpdateRequest $request,
        RefAddressType $refAddressType
    ) {
        $this->authorize('update', $refAddressType);

        $validated = $request->validated();

        $this->save($validated, $refAddressType);

        return redirect()
            ->route('ref-address-types.edit', $refAddressType)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param Request $request
     * @param RefAddressType $refAddressType
     * @return Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request, RefAddressType $refAddressType)
    {
        $this->authorize('delete', $refAddressType);

        $refAddressType->delete();

        return redirect()
            ->route('ref-address-types.index')
            ->withSuccess(__('crud.common.removed'));
    }

    private function save($validated, $model)
    {
        foreach($validated['name'] as $lang => $value){
            $model->translateOrNew($lang)->name = $value;
        }
        $model->save();
        return $model;
    }
}
