<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\EmployeeHolidaysPeriod;
use App\Http\Requests\EmployeeHolidaysPeriodStoreRequest;
use App\Http\Requests\EmployeeHolidaysPeriodUpdateRequest;

class EmployeeHolidaysPeriodController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeeHolidaysPeriod::class);

        $search = $request->get('search', '');

        $employeeHolidaysPeriods = EmployeeHolidaysPeriod::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.employee_holidays_periods.index',
            compact('employeeHolidaysPeriods', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', EmployeeHolidaysPeriod::class);

        $employees = Employee::pluck('iin', 'id');

        return view(
            'app.employee_holidays_periods.create',
            compact('employees')
        );
    }

    /**
     * @param \App\Http\Requests\EmployeeHolidaysPeriodStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeHolidaysPeriodStoreRequest $request)
    {
        $this->authorize('create', EmployeeHolidaysPeriod::class);

        $validated = $request->validated();

        $employeeHolidaysPeriod = EmployeeHolidaysPeriod::create($validated);

        return redirect()
            ->route('employee-holidays-periods.edit', $employeeHolidaysPeriod)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeHolidaysPeriod $employeeHolidaysPeriod
     * @return \Illuminate\Http\Response
     */
    public function show(
        Request $request,
        EmployeeHolidaysPeriod $employeeHolidaysPeriod
    ) {
        $this->authorize('view', $employeeHolidaysPeriod);

        return view(
            'app.employee_holidays_periods.show',
            compact('employeeHolidaysPeriod')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeHolidaysPeriod $employeeHolidaysPeriod
     * @return \Illuminate\Http\Response
     */
    public function edit(
        Request $request,
        EmployeeHolidaysPeriod $employeeHolidaysPeriod
    ) {
        $this->authorize('update', $employeeHolidaysPeriod);

        $employees = Employee::pluck('iin', 'id');

        return view(
            'app.employee_holidays_periods.edit',
            compact('employeeHolidaysPeriod', 'employees')
        );
    }

    /**
     * @param \App\Http\Requests\EmployeeHolidaysPeriodUpdateRequest $request
     * @param \App\Models\EmployeeHolidaysPeriod $employeeHolidaysPeriod
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeeHolidaysPeriodUpdateRequest $request,
        EmployeeHolidaysPeriod $employeeHolidaysPeriod
    ) {
        $this->authorize('update', $employeeHolidaysPeriod);

        $validated = $request->validated();

        $employeeHolidaysPeriod->update($validated);

        return redirect()
            ->route('employee-holidays-periods.edit', $employeeHolidaysPeriod)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeHolidaysPeriod $employeeHolidaysPeriod
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        Request $request,
        EmployeeHolidaysPeriod $employeeHolidaysPeriod
    ) {
        $this->authorize('delete', $employeeHolidaysPeriod);

        $employeeHolidaysPeriod->delete();

        return redirect()
            ->route('employee-holidays-periods.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
