<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\EmployeeHospital;
use App\Http\Requests\EmployeeHospitalStoreRequest;
use App\Http\Requests\EmployeeHospitalUpdateRequest;

class EmployeeHospitalController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeeHospital::class);

        $search = $request->get('search', '');

        $employeeHospitals = EmployeeHospital::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.employee_hospitals.index',
            compact('employeeHospitals', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', EmployeeHospital::class);

        $employees = Employee::pluck('iin', 'id');

        return view('app.employee_hospitals.create', compact('employees'));
    }

    /**
     * @param \App\Http\Requests\EmployeeHospitalStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeHospitalStoreRequest $request)
    {
        $this->authorize('create', EmployeeHospital::class);

        $validated = $request->validated();

        $employeeHospital = EmployeeHospital::create($validated);

        return redirect()
            ->route('employee-hospitals.edit', $employeeHospital)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeHospital $employeeHospital
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EmployeeHospital $employeeHospital)
    {
        $this->authorize('view', $employeeHospital);

        return view('app.employee_hospitals.show', compact('employeeHospital'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeHospital $employeeHospital
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, EmployeeHospital $employeeHospital)
    {
        $this->authorize('update', $employeeHospital);

        $employees = Employee::pluck('iin', 'id');

        return view(
            'app.employee_hospitals.edit',
            compact('employeeHospital', 'employees')
        );
    }

    /**
     * @param \App\Http\Requests\EmployeeHospitalUpdateRequest $request
     * @param \App\Models\EmployeeHospital $employeeHospital
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeeHospitalUpdateRequest $request,
        EmployeeHospital $employeeHospital
    ) {
        $this->authorize('update', $employeeHospital);

        $validated = $request->validated();

        $employeeHospital->update($validated);

        return redirect()
            ->route('employee-hospitals.edit', $employeeHospital)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeHospital $employeeHospital
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        Request $request,
        EmployeeHospital $employeeHospital
    ) {
        $this->authorize('delete', $employeeHospital);

        $employeeHospital->delete();

        return redirect()
            ->route('employee-hospitals.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
