<?php

namespace App\Http\Controllers;

use App\Models\RefAction;
use Illuminate\Http\Request;
use App\Http\Requests\RefActionStoreRequest;
use App\Http\Requests\RefActionUpdateRequest;

class RefActionController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefAction::class);

        $search = $request->get('search', '');

        $refActions = RefAction::search($search)->paginate()
            ->withQueryString();

        return view('app.ref_actions.index', compact('refActions', 'search'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', RefAction::class);

        return view('app.ref_actions.create');
    }

    /**
     * @param \App\Http\Requests\RefActionStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefActionStoreRequest $request)
    {
        $this->authorize('create', RefAction::class);

        $validated = $request->validated();

        $refAction = RefAction::create($validated);

        return redirect()
            ->route('ref-actions.edit', $refAction)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefAction $refAction
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefAction $refAction)
    {
        $this->authorize('view', $refAction);

        return view('app.ref_actions.show', compact('refAction'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefAction $refAction
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, RefAction $refAction)
    {
        $this->authorize('update', $refAction);

        return view('app.ref_actions.edit', compact('refAction'));
    }

    /**
     * @param \App\Http\Requests\RefActionUpdateRequest $request
     * @param \App\Models\RefAction $refAction
     * @return \Illuminate\Http\Response
     */
    public function update(
        RefActionUpdateRequest $request,
        RefAction $refAction
    ) {
        $this->authorize('update', $refAction);

        $validated = $request->validated();

        $refAction->update($validated);

        return redirect()
            ->route('ref-actions.edit', $refAction)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefAction $refAction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, RefAction $refAction)
    {
        $this->authorize('delete', $refAction);

        $refAction->delete();

        return redirect()
            ->route('ref-actions.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
