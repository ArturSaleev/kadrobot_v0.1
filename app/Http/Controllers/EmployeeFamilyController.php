<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\EmployeeFamily;
use App\Models\RefFamilyState;
use App\Http\Requests\EmployeeFamilyStoreRequest;
use App\Http\Requests\EmployeeFamilyUpdateRequest;

class EmployeeFamilyController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeeFamily::class);

        $search = $request->get('search', '');

        $employeeFamilies = EmployeeFamily::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.employee_families.index',
            compact('employeeFamilies', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', EmployeeFamily::class);

        $employees = Employee::pluck('iin', 'id');
        $refFamilyStates = RefFamilyState::pluck('id', 'id');

        return view(
            'app.employee_families.create',
            compact('employees', 'refFamilyStates')
        );
    }

    /**
     * @param \App\Http\Requests\EmployeeFamilyStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeFamilyStoreRequest $request)
    {
        $this->authorize('create', EmployeeFamily::class);

        $validated = $request->validated();

        $employeeFamily = EmployeeFamily::create($validated);

        return redirect()
            ->route('employee-families.edit', $employeeFamily)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeFamily $employeeFamily
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EmployeeFamily $employeeFamily)
    {
        $this->authorize('view', $employeeFamily);

        return view('app.employee_families.show', compact('employeeFamily'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeFamily $employeeFamily
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, EmployeeFamily $employeeFamily)
    {
        $this->authorize('update', $employeeFamily);

        $employees = Employee::pluck('iin', 'id');
        $refFamilyStates = RefFamilyState::pluck('id', 'id');

        return view(
            'app.employee_families.edit',
            compact('employeeFamily', 'employees', 'refFamilyStates')
        );
    }

    /**
     * @param \App\Http\Requests\EmployeeFamilyUpdateRequest $request
     * @param \App\Models\EmployeeFamily $employeeFamily
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeeFamilyUpdateRequest $request,
        EmployeeFamily $employeeFamily
    ) {
        $this->authorize('update', $employeeFamily);

        $validated = $request->validated();

        $employeeFamily->update($validated);

        return redirect()
            ->route('employee-families.edit', $employeeFamily)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeFamily $employeeFamily
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, EmployeeFamily $employeeFamily)
    {
        $this->authorize('delete', $employeeFamily);

        $employeeFamily->delete();

        return redirect()
            ->route('employee-families.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
