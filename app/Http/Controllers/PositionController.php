<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Position;
use App\Models\PositionDeclension;
use App\Traits\DeclensionTrait;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Http\Requests\PositionStoreRequest;
use App\Http\Requests\PositionUpdateRequest;
use Illuminate\Http\Response;

class PositionController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', Position::class);

        $search = $request->get('search', '');

        $positions = Position::search($search)
            ->paginate()
            ->withQueryString();

        return view('app.positions.index', compact('positions', 'search'));
    }

    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function create(Request $request)
    {
        $this->authorize('create', Position::class);

        $departments = Department::all();
        $positions = Position::all();

        return view('app.positions.create', compact('departments', 'positions'));
    }

    /**
     * @param PositionStoreRequest $request
     * @return Response
     * @throws AuthorizationException
     */
    public function store(PositionStoreRequest $request)
    {
        $this->authorize('create', Position::class);

        $validated = $request->validated();
        $position = new Position();
        $position = $this->save($validated, $position);

        return redirect()
            ->route('positions.edit', $position)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param Request $request
     * @param Position $position
     * @return Response
     * @throws AuthorizationException
     */
    public function show(Request $request, Position $position)
    {
        $this->authorize('view', $position);

        $id_pos = $position->pos_level;
        $main_position = '-';
        if($id_pos > 0){
            $ps = Position::find($id_pos);
            if($ps){
                $main_position = $ps->name;
            }
        }

        return view('app.positions.show', compact('position', 'main_position'));
    }

    /**
     * @param Request $request
     * @param Position $position
     * @return Response
     * @throws AuthorizationException
     */
    public function edit(Request $request, Position $position)
    {
        $this->authorize('update', $position);

        $departments = Department::all();
        $positions = Position::query()->whereNot('id', $position->id)->get();

        return view('app.positions.edit', compact('position', 'departments', 'positions'));
    }

    /**
     * @param PositionUpdateRequest $request
     * @param Position $position
     * @return Response
     * @throws AuthorizationException
     */
    public function update(PositionUpdateRequest $request, Position $position)
    {
        $this->authorize('update', $position);

        $validated = $request->validated();

        $this->save($validated, $position);

        return redirect()
            ->route('positions.edit', $position)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param Request $request
     * @param Position $position
     * @return Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request, Position $position)
    {
        $this->authorize('delete', $position);

        $position->delete();

        return redirect()
            ->route('positions.index')
            ->withSuccess(__('crud.common.removed'));
    }

    private function save($validated, $position)
    {
        $position->cnt = $validated['cnt'];
        $position->pos_level = (isset($validated['pos_level'])) ? $validated['pos_level'] : 0;
        $position->min_salary = (isset($validated['min_salary'])) ? $validated['min_salary'] : 0;
        $position->max_salary = (isset($validated['max_salary'])) ? $validated['max_salary'] : 0;
        $position->department_id = $validated['department_id'];
        foreach($validated['name'] as $lang=>$name){
            $position->translateOrNew($lang)->name = $name;
        }
        $position->save();

        if(isset($validated['declensions'])){
            $position->positionDeclensions()->delete();
            foreach($validated['declensions'] as $name=>$array){
                foreach($array as $lang=>$items) {
                    foreach($items as $i=>$item) {
                        foreach($item as $type=>$value){
                            $posDecl = new PositionDeclension();
                            $posDecl->position_id = $position->id;
                            $posDecl->locale = $lang;
                            $posDecl->case_type = $type;
                            $posDecl->value = $value;
                            if(isset($validated['declensions_descript'][$name][$lang][$i][$type])) {
                                $posDecl->type_description = $validated['declensions_descript'][$name][$lang][$i][$type];
                            }
                            $posDecl->save();
                        }
                    }
                }
            }
        }else{
            if(count($position->positionDeclensions) <= 0){
                $data = [];
                foreach(config('panel.available_languages', []) as $lang=>$title){
                    $data[$lang] = DeclensionTrait::declensionsOther( $lang, $position->translate($lang)->name);
                }

                foreach($data as $lang=>$items){
                    foreach($items as $item) {
                        $posDecl = new PositionDeclension();
                        $posDecl->position_id = $position->id;
                        $posDecl->locale = $lang;
                        $posDecl->case_type = $item['type'];
                        $posDecl->value = $item['value'];
                        $posDecl->type_description = $item['name'];
                        $posDecl->save();
                    }
                }
            }
        }

        return $position;
    }

    public function declensionPosition(Request $request)
    {
        if(!$request->has('declensions')){
            return \response()->json([
                'data' => [],
                'message' => __('declension.errors.no_name')
            ]);
        }

        $request->validate([
            'declensions' => ["required", 'array']
        ]);

        $result = [];
        foreach($request->declensions as $decl){
            $lang  = $decl['lang'];
            $value = $decl['value'];
            $name  = $decl['name'];

           $result[$decl['lang']] = [
               "name" => $name,
               "data" => DeclensionTrait::declensionsOther($lang, $value)
           ];
        }

        return view('components.declensions_text', ['result' => $result]);
    }
}
