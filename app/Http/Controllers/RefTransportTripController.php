<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RefTransportTrip;
use App\Http\Requests\RefTransportTripStoreRequest;
use App\Http\Requests\RefTransportTripUpdateRequest;

class RefTransportTripController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefTransportTrip::class);

        $search = $request->get('search', '');

        $refTransportTrips = RefTransportTrip::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.ref_transport_trips.index',
            compact('refTransportTrips', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', RefTransportTrip::class);

        return view('app.ref_transport_trips.create');
    }

    /**
     * @param \App\Http\Requests\RefTransportTripStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefTransportTripStoreRequest $request)
    {
        $this->authorize('create', RefTransportTrip::class);

        $validated = $request->validated();

        $refTransportTrip = RefTransportTrip::create($validated);

        return redirect()
            ->route('ref-transport-trips.edit', $refTransportTrip)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTransportTrip $refTransportTrip
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefTransportTrip $refTransportTrip)
    {
        $this->authorize('view', $refTransportTrip);

        return view(
            'app.ref_transport_trips.show',
            compact('refTransportTrip')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTransportTrip $refTransportTrip
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, RefTransportTrip $refTransportTrip)
    {
        $this->authorize('update', $refTransportTrip);

        return view(
            'app.ref_transport_trips.edit',
            compact('refTransportTrip')
        );
    }

    /**
     * @param \App\Http\Requests\RefTransportTripUpdateRequest $request
     * @param \App\Models\RefTransportTrip $refTransportTrip
     * @return \Illuminate\Http\Response
     */
    public function update(
        RefTransportTripUpdateRequest $request,
        RefTransportTrip $refTransportTrip
    ) {
        $this->authorize('update', $refTransportTrip);

        $validated = $request->validated();

        $refTransportTrip->update($validated);

        return redirect()
            ->route('ref-transport-trips.edit', $refTransportTrip)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTransportTrip $refTransportTrip
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        Request $request,
        RefTransportTrip $refTransportTrip
    ) {
        $this->authorize('delete', $refTransportTrip);

        $refTransportTrip->delete();

        return redirect()
            ->route('ref-transport-trips.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
