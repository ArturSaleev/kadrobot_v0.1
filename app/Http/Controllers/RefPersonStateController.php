<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RefPersonState;
use App\Http\Requests\RefPersonStateStoreRequest;
use App\Http\Requests\RefPersonStateUpdateRequest;

class RefPersonStateController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefPersonState::class);

        $search = $request->get('search', '');

        $refPersonStates = RefPersonState::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.ref_person_states.index',
            compact('refPersonStates', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', RefPersonState::class);

        return view('app.ref_person_states.create');
    }

    /**
     * @param \App\Http\Requests\RefPersonStateStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefPersonStateStoreRequest $request)
    {
        $this->authorize('create', RefPersonState::class);

        $validated = $request->validated();

        $refPersonState = RefPersonState::create($validated);

        return redirect()
            ->route('ref-person-states.edit', $refPersonState)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefPersonState $refPersonState
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefPersonState $refPersonState)
    {
        $this->authorize('view', $refPersonState);

        return view('app.ref_person_states.show', compact('refPersonState'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefPersonState $refPersonState
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, RefPersonState $refPersonState)
    {
        $this->authorize('update', $refPersonState);

        return view('app.ref_person_states.edit', compact('refPersonState'));
    }

    /**
     * @param \App\Http\Requests\RefPersonStateUpdateRequest $request
     * @param \App\Models\RefPersonState $refPersonState
     * @return \Illuminate\Http\Response
     */
    public function update(
        RefPersonStateUpdateRequest $request,
        RefPersonState $refPersonState
    ) {
        $this->authorize('update', $refPersonState);

        $validated = $request->validated();

        $refPersonState->update($validated);

        return redirect()
            ->route('ref-person-states.edit', $refPersonState)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefPersonState $refPersonState
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, RefPersonState $refPersonState)
    {
        $this->authorize('delete', $refPersonState);

        $refPersonState->delete();

        return redirect()
            ->route('ref-person-states.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
