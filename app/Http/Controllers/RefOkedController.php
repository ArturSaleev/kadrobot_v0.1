<?php

namespace App\Http\Controllers;

use App\Models\RefOked;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Http\Requests\RefOkedStoreRequest;
use App\Http\Requests\RefOkedUpdateRequest;
use Illuminate\Http\Response;

class RefOkedController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefOked::class);

        $search = $request->get('search', '');

        $refOkeds = RefOked::search($search)->paginate()
            ->withQueryString();

        return view('app.ref_okeds.index', compact('refOkeds', 'search'));
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', RefOked::class);

        return view('app.ref_okeds.create');
    }

    /**
     * @param RefOkedStoreRequest $request
     * @return Response
     * @throws AuthorizationException
     */
    public function store(RefOkedStoreRequest $request)
    {
        $this->authorize('create', RefOked::class);

        $validated = $request->validated();

        $refOked = new RefOked();
        $refOked = $this->save($validated, $refOked);

        return redirect()
            ->route('ref-okeds.edit', $refOked)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param Request $request
     * @param RefOked $refOked
     * @return Response
     * @throws AuthorizationException
     */
    public function show(Request $request, RefOked $refOked)
    {
        $this->authorize('view', $refOked);

        return view('app.ref_okeds.show', compact('refOked'));
    }

    /**
     * @param Request $request
     * @param RefOked $refOked
     * @return Response
     * @throws AuthorizationException
     */
    public function edit(Request $request, RefOked $refOked)
    {
        $this->authorize('update', $refOked);

        return view('app.ref_okeds.edit', compact('refOked'));
    }

    /**
     * @param RefOkedUpdateRequest $request
     * @param RefOked $refOked
     * @return Response
     * @throws AuthorizationException
     */
    public function update(RefOkedUpdateRequest $request, RefOked $refOked)
    {
        $this->authorize('update', $refOked);

        $validated = $request->validated();

        $this->save($validated, $refOked);

        return redirect()
            ->route('ref-okeds.edit', $refOked)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param Request $request
     * @param RefOked $refOked
     * @return Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request, RefOked $refOked)
    {
        $this->authorize('delete', $refOked);

        $refOked->delete();

        return redirect()
            ->route('ref-okeds.index')
            ->withSuccess(__('crud.common.removed'));
    }

    private function save($validated, $model)
    {
        $model->oked = $validated['oked'];
        foreach($validated['name'] as $lang => $value){
            $model->translateOrNew($lang)->name = $value;
        }
        foreach($validated['name_oked'] as $lang => $value){
            $model->translateOrNew($lang)->name_oked = $value;
        }
        $model->save();
        return $model;
    }
}
