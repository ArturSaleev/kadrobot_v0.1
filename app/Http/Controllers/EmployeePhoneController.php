<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\RefTypePhone;
use App\Models\EmployeePhone;
use App\Http\Requests\EmployeePhoneStoreRequest;
use App\Http\Requests\EmployeePhoneUpdateRequest;

class EmployeePhoneController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeePhone::class);

        $search = $request->get('search', '');

        $employeePhones = EmployeePhone::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.employee_phones.index',
            compact('employeePhones', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', EmployeePhone::class);

        $refTypePhones = RefTypePhone::pluck('id', 'id');
        $employees = Employee::pluck('iin', 'id');

        return view(
            'app.employee_phones.create',
            compact('refTypePhones', 'employees')
        );
    }

    /**
     * @param \App\Http\Requests\EmployeePhoneStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeePhoneStoreRequest $request)
    {
        $this->authorize('create', EmployeePhone::class);

        $validated = $request->validated();

        $employeePhone = EmployeePhone::create($validated);

        return redirect()
            ->route('employee-phones.edit', $employeePhone)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeePhone $employeePhone
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EmployeePhone $employeePhone)
    {
        $this->authorize('view', $employeePhone);

        return view('app.employee_phones.show', compact('employeePhone'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeePhone $employeePhone
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, EmployeePhone $employeePhone)
    {
        $this->authorize('update', $employeePhone);

        $refTypePhones = RefTypePhone::pluck('id', 'id');
        $employees = Employee::pluck('iin', 'id');

        return view(
            'app.employee_phones.edit',
            compact('employeePhone', 'refTypePhones', 'employees')
        );
    }

    /**
     * @param \App\Http\Requests\EmployeePhoneUpdateRequest $request
     * @param \App\Models\EmployeePhone $employeePhone
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeePhoneUpdateRequest $request,
        EmployeePhone $employeePhone
    ) {
        $this->authorize('update', $employeePhone);

        $validated = $request->validated();

        $employeePhone->update($validated);

        return redirect()
            ->route('employee-phones.edit', $employeePhone)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeePhone $employeePhone
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, EmployeePhone $employeePhone)
    {
        $this->authorize('delete', $employeePhone);

        $employeePhone->delete();

        return redirect()
            ->route('employee-phones.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
