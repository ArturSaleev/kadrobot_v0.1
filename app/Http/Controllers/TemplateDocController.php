<?php

namespace App\Http\Controllers;

use App\Models\TemplateDoc;
use App\Models\TemplateDocTranslation;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TemplateDocController extends Controller
{
    public $idTypes = [];

    public function __construct()
    {
        $this->idTypes = [
            [
                'id' => 0,
                'name' => __('crud.id_types.pattern')
            ],
            [
                'id' => 1,
                'name' => __('crud.id_types.document')
            ]
        ];
    }

    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', TemplateDoc::class);

        $search = $request->get('search', '');

        if($search !== ''){
            $res = TemplateDocTranslation::query()
                ->where('name')
                ->pluck('template_doc_id')
                ->toArray();

            $templateDocs = TemplateDoc::query()
                ->whereIn('id', $res)
                ->paginate()
                ->withQueryString();
        }else{
            $templateDocs = TemplateDoc::query()
                ->paginate()
                ->withQueryString();
        }


        return view('app.template_docs.index', compact('templateDocs', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->authorize('create', TemplateDoc::class);

        $types = $this->idTypes;
        return view('app.template_docs.create', compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', TemplateDoc::class);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $this->authorize('show', TemplateDoc::class);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $this->authorize('update', TemplateDoc::class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update', TemplateDoc::class);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->authorize('delete', TemplateDoc::class);
    }
}
