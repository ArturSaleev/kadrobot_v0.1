<?php

namespace App\Http\Controllers;

use App\Models\RefStatus;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Http\Requests\RefStatusStoreRequest;
use App\Http\Requests\RefStatusUpdateRequest;
use Illuminate\Http\Response;

class RefStatusController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefStatus::class);

        $search = $request->get('search', '');

        $refStatuses = RefStatus::search($search)->paginate()
            ->withQueryString();

        return view('app.ref_statuses.index', compact('refStatuses', 'search'));
    }

    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function create(Request $request)
    {
        $this->authorize('create', RefStatus::class);

        return view('app.ref_statuses.create');
    }

    /**
     * @param RefStatusStoreRequest $request
     * @return Response
     * @throws AuthorizationException
     */
    public function store(RefStatusStoreRequest $request)
    {
        $this->authorize('create', RefStatus::class);

        $validated = $request->validated();

        $refStatus = new RefStatus();
        $refStatus = $this->save($validated, $refStatus);

        return redirect()
            ->route('ref-statuses.edit', $refStatus)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param Request $request
     * @param RefStatus $refStatus
     * @return Response
     * @throws AuthorizationException
     */
    public function show(Request $request, RefStatus $refStatus)
    {
        $this->authorize('view', $refStatus);

        return view('app.ref_statuses.show', compact('refStatus'));
    }

    /**
     * @param Request $request
     * @param RefStatus $refStatus
     * @return Response
     * @throws AuthorizationException
     */
    public function edit(Request $request, RefStatus $refStatus)
    {
        $this->authorize('update', $refStatus);

        return view('app.ref_statuses.edit', compact('refStatus'));
    }

    /**
     * @param RefStatusUpdateRequest $request
     * @param RefStatus $refStatus
     * @return Response
     */
    public function update(
        RefStatusUpdateRequest $request,
        RefStatus $refStatus
    ) {
        $this->authorize('update', $refStatus);

        $validated = $request->validated();

        $this->save($validated, $refStatus);

        return redirect()
            ->route('ref-statuses.edit', $refStatus)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param Request $request
     * @param RefStatus $refStatus
     * @return Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request, RefStatus $refStatus)
    {
        $this->authorize('delete', $refStatus);

        $refStatus->delete();

        return redirect()
            ->route('ref-statuses.index')
            ->withSuccess(__('crud.common.removed'));
    }

    private function save($validated, $model)
    {
        foreach($validated['name'] as $lang => $value){
            $model->translateOrNew($lang)->name = $value;
        }
        $model->save();
        return $model;
    }
}
