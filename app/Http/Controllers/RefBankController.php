<?php

namespace App\Http\Controllers;

use App\Models\RefBank;
use App\Models\RefStatus;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Http\Requests\RefBankStoreRequest;
use App\Http\Requests\RefBankUpdateRequest;
use Illuminate\Http\Response;

class RefBankController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefBank::class);

        $search = $request->get('search', '');

        $refBanks = RefBank::search($search)->paginate()
            ->withQueryString();

        return view('app.ref_banks.index', compact('refBanks', 'search'));
    }

    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function create(Request $request)
    {
        $this->authorize('create', RefBank::class);

        $refStatuses = RefStatus::all();

        return view('app.ref_banks.create', compact('refStatuses'));
    }

    /**
     * @param RefBankStoreRequest $request
     * @return Response
     * @throws AuthorizationException
     */
    public function store(RefBankStoreRequest $request)
    {
        $this->authorize('create', RefBank::class);

        $validated = $request->validated();

        $refBank = new RefBank();
        $refBank = $this->save($validated, $refBank);

        return redirect()
            ->route('ref-banks.edit', $refBank)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param Request $request
     * @param RefBank $refBank
     * @return Response
     * @throws AuthorizationException
     */
    public function show(Request $request, RefBank $refBank)
    {
        $this->authorize('view', $refBank);

        return view('app.ref_banks.show', compact('refBank'));
    }

    /**
     * @param Request $request
     * @param RefBank $refBank
     * @return Response
     * @throws AuthorizationException
     */
    public function edit(Request $request, RefBank $refBank)
    {
        $this->authorize('update', $refBank);

        $refStatuses = RefStatus::all();

        return view('app.ref_banks.edit', compact('refBank', 'refStatuses'));
    }

    /**
     * @param RefBankUpdateRequest $request
     * @param RefBank $refBank
     * @return Response
     * @throws AuthorizationException
     */
    public function update(RefBankUpdateRequest $request, RefBank $refBank)
    {
        $this->authorize('update', $refBank);

        $validated = $request->validated();

        $this->save($validated, $refBank);

        return redirect()
            ->route('ref-banks.edit', $refBank)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param Request $request
     * @param RefBank $refBank
     * @return Response
     */
    public function destroy(Request $request, RefBank $refBank)
    {
        $this->authorize('delete', $refBank);

        $refBank->delete();

        return redirect()
            ->route('ref-banks.index')
            ->withSuccess(__('crud.common.removed'));
    }

    private function save($validated, $model)
    {
        foreach($validated['name'] as $lang => $value){
            $model->translateOrNew($lang)->name = $value;
        }
        $model->bin = $validated['bin'];
        $model->ref_status_id = $validated['ref_status_id'];
        $model->mfo = $validated['mfo'];
        $model->mfo_head = $validated['mfo_head'];
        $model->mfo_rkc = $validated['mfo_rkc'];
        $model->kor_account = $validated['kor_account'];
        $model->commis = $validated['commis'];
        $model->bik_old = $validated['bik_old'];
        $model->save();
        return $model;
    }
}
