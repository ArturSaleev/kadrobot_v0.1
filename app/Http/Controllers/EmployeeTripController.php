<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\EmployeeTrip;
use Illuminate\Http\Request;
use App\Models\RefTransportTrip;
use App\Http\Requests\EmployeeTripStoreRequest;
use App\Http\Requests\EmployeeTripUpdateRequest;

class EmployeeTripController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeeTrip::class);

        $search = $request->get('search', '');

        $employeeTrips = EmployeeTrip::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.employee_trips.index',
            compact('employeeTrips', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', EmployeeTrip::class);

        $employees = Employee::pluck('iin', 'id');
        $refTransportTrips = RefTransportTrip::pluck('id', 'id');

        return view(
            'app.employee_trips.create',
            compact('employees', 'refTransportTrips')
        );
    }

    /**
     * @param \App\Http\Requests\EmployeeTripStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeTripStoreRequest $request)
    {
        $this->authorize('create', EmployeeTrip::class);

        $validated = $request->validated();

        $employeeTrip = EmployeeTrip::create($validated);

        return redirect()
            ->route('employee-trips.edit', $employeeTrip)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeTrip $employeeTrip
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EmployeeTrip $employeeTrip)
    {
        $this->authorize('view', $employeeTrip);

        return view('app.employee_trips.show', compact('employeeTrip'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeTrip $employeeTrip
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, EmployeeTrip $employeeTrip)
    {
        $this->authorize('update', $employeeTrip);

        $employees = Employee::pluck('iin', 'id');
        $refTransportTrips = RefTransportTrip::pluck('id', 'id');

        return view(
            'app.employee_trips.edit',
            compact('employeeTrip', 'employees', 'refTransportTrips')
        );
    }

    /**
     * @param \App\Http\Requests\EmployeeTripUpdateRequest $request
     * @param \App\Models\EmployeeTrip $employeeTrip
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeeTripUpdateRequest $request,
        EmployeeTrip $employeeTrip
    ) {
        $this->authorize('update', $employeeTrip);

        $validated = $request->validated();

        $employeeTrip->update($validated);

        return redirect()
            ->route('employee-trips.edit', $employeeTrip)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeTrip $employeeTrip
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, EmployeeTrip $employeeTrip)
    {
        $this->authorize('delete', $employeeTrip);

        $employeeTrip->delete();

        return redirect()
            ->route('employee-trips.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
