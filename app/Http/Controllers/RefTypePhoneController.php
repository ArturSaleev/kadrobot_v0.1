<?php

namespace App\Http\Controllers;

use App\Models\RefTypePhone;
use Illuminate\Http\Request;
use App\Http\Requests\RefTypePhoneStoreRequest;
use App\Http\Requests\RefTypePhoneUpdateRequest;

class RefTypePhoneController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefTypePhone::class);

        $search = $request->get('search', '');

        $refTypePhones = RefTypePhone::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.ref_type_phones.index',
            compact('refTypePhones', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', RefTypePhone::class);

        return view('app.ref_type_phones.create');
    }

    /**
     * @param \App\Http\Requests\RefTypePhoneStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefTypePhoneStoreRequest $request)
    {
        $this->authorize('create', RefTypePhone::class);

        $validated = $request->validated();

        $refTypePhone = RefTypePhone::create($validated);

        return redirect()
            ->route('ref-type-phones.edit', $refTypePhone)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTypePhone $refTypePhone
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefTypePhone $refTypePhone)
    {
        $this->authorize('view', $refTypePhone);

        return view('app.ref_type_phones.show', compact('refTypePhone'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTypePhone $refTypePhone
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, RefTypePhone $refTypePhone)
    {
        $this->authorize('update', $refTypePhone);

        return view('app.ref_type_phones.edit', compact('refTypePhone'));
    }

    /**
     * @param \App\Http\Requests\RefTypePhoneUpdateRequest $request
     * @param \App\Models\RefTypePhone $refTypePhone
     * @return \Illuminate\Http\Response
     */
    public function update(
        RefTypePhoneUpdateRequest $request,
        RefTypePhone $refTypePhone
    ) {
        $this->authorize('update', $refTypePhone);

        $validated = $request->validated();

        $refTypePhone->update($validated);

        return redirect()
            ->route('ref-type-phones.edit', $refTypePhone)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTypePhone $refTypePhone
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, RefTypePhone $refTypePhone)
    {
        $this->authorize('delete', $refTypePhone);

        $refTypePhone->delete();

        return redirect()
            ->route('ref-type-phones.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
