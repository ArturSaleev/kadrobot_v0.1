<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Models\RefKindHoliday;
use App\Http\Requests\RefKindHolidayStoreRequest;
use App\Http\Requests\RefKindHolidayUpdateRequest;
use Illuminate\Http\Response;

class RefKindHolidayController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefKindHoliday::class);

        $search = $request->get('search', '');

        $refKindHolidays = RefKindHoliday::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.ref_kind_holidays.index',
            compact('refKindHolidays', 'search')
        );
    }

    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function create(Request $request)
    {
        $this->authorize('create', RefKindHoliday::class);

        return view('app.ref_kind_holidays.create');
    }

    /**
     * @param RefKindHolidayStoreRequest $request
     * @return Response
     * @throws AuthorizationException
     */
    public function store(RefKindHolidayStoreRequest $request)
    {
        $this->authorize('create', RefKindHoliday::class);

        $validated = $request->validated();

        $refKindHoliday = new RefKindHoliday();
        $refKindHoliday = $this->save($validated, $refKindHoliday);

        return redirect()
            ->route('ref-kind-holidays.edit', $refKindHoliday)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param Request $request
     * @param RefKindHoliday $refKindHoliday
     * @return Response
     * @throws AuthorizationException
     */
    public function show(Request $request, RefKindHoliday $refKindHoliday)
    {
        $this->authorize('view', $refKindHoliday);

        return view('app.ref_kind_holidays.show', compact('refKindHoliday'));
    }

    /**
     * @param Request $request
     * @param RefKindHoliday $refKindHoliday
     * @return Response
     * @throws AuthorizationException
     */
    public function edit(Request $request, RefKindHoliday $refKindHoliday)
    {
        $this->authorize('update', $refKindHoliday);

        return view('app.ref_kind_holidays.edit', compact('refKindHoliday'));
    }

    /**
     * @param RefKindHolidayUpdateRequest $request
     * @param RefKindHoliday $refKindHoliday
     * @return Response
     * @throws AuthorizationException
     */
    public function update(
        RefKindHolidayUpdateRequest $request,
        RefKindHoliday $refKindHoliday
    ) {
        $this->authorize('update', $refKindHoliday);

        $validated = $request->validated();

        $this->save($validated, $refKindHoliday);

        return redirect()
            ->route('ref-kind-holidays.edit', $refKindHoliday)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param Request $request
     * @param RefKindHoliday $refKindHoliday
     * @return Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request, RefKindHoliday $refKindHoliday)
    {
        $this->authorize('delete', $refKindHoliday);

        $refKindHoliday->delete();

        return redirect()
            ->route('ref-kind-holidays.index')
            ->withSuccess(__('crud.common.removed'));
    }

    private function save($validated, $model)
    {
        foreach($validated['name'] as $lang => $value){
            $model->translateOrNew($lang)->name = $value;
        }
        $model->save();
        return $model;
    }
}
