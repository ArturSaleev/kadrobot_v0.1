<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EmployeeTrip;
use App\Models\RefTransportTrip;
use App\Models\EmployeeTripFromTo;
use App\Http\Requests\EmployeeTripFromToStoreRequest;
use App\Http\Requests\EmployeeTripFromToUpdateRequest;

class EmployeeTripFromToController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeeTripFromTo::class);

        $search = $request->get('search', '');

        $employeeTripFromTos = EmployeeTripFromTo::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.employee_trip_from_tos.index',
            compact('employeeTripFromTos', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', EmployeeTripFromTo::class);

        $employeeTrips = EmployeeTrip::pluck('date_begin', 'id');
        $refTransportTrips = RefTransportTrip::pluck('id', 'id');

        return view(
            'app.employee_trip_from_tos.create',
            compact('employeeTrips', 'refTransportTrips')
        );
    }

    /**
     * @param \App\Http\Requests\EmployeeTripFromToStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeTripFromToStoreRequest $request)
    {
        $this->authorize('create', EmployeeTripFromTo::class);

        $validated = $request->validated();

        $employeeTripFromTo = EmployeeTripFromTo::create($validated);

        return redirect()
            ->route('employee-trip-from-tos.edit', $employeeTripFromTo)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeTripFromTo $employeeTripFromTo
     * @return \Illuminate\Http\Response
     */
    public function show(
        Request $request,
        EmployeeTripFromTo $employeeTripFromTo
    ) {
        $this->authorize('view', $employeeTripFromTo);

        return view(
            'app.employee_trip_from_tos.show',
            compact('employeeTripFromTo')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeTripFromTo $employeeTripFromTo
     * @return \Illuminate\Http\Response
     */
    public function edit(
        Request $request,
        EmployeeTripFromTo $employeeTripFromTo
    ) {
        $this->authorize('update', $employeeTripFromTo);

        $employeeTrips = EmployeeTrip::pluck('date_begin', 'id');
        $refTransportTrips = RefTransportTrip::pluck('id', 'id');

        return view(
            'app.employee_trip_from_tos.edit',
            compact('employeeTripFromTo', 'employeeTrips', 'refTransportTrips')
        );
    }

    /**
     * @param \App\Http\Requests\EmployeeTripFromToUpdateRequest $request
     * @param \App\Models\EmployeeTripFromTo $employeeTripFromTo
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeeTripFromToUpdateRequest $request,
        EmployeeTripFromTo $employeeTripFromTo
    ) {
        $this->authorize('update', $employeeTripFromTo);

        $validated = $request->validated();

        $employeeTripFromTo->update($validated);

        return redirect()
            ->route('employee-trip-from-tos.edit', $employeeTripFromTo)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeTripFromTo $employeeTripFromTo
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        Request $request,
        EmployeeTripFromTo $employeeTripFromTo
    ) {
        $this->authorize('delete', $employeeTripFromTo);

        $employeeTripFromTo->delete();

        return redirect()
            ->route('employee-trip-from-tos.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
