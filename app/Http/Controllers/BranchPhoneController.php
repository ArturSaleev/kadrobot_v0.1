<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\BranchPhone;
use Illuminate\Http\Request;
use App\Models\RefTypePhone;
use App\Http\Requests\BranchPhoneStoreRequest;
use App\Http\Requests\BranchPhoneUpdateRequest;

class BranchPhoneController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', BranchPhone::class);

        $search = $request->get('search', '');

        $branchPhones = BranchPhone::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.branch_phones.index',
            compact('branchPhones', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', BranchPhone::class);

        $branches = Branch::pluck('id', 'id');
        $refTypePhones = RefTypePhone::pluck('id', 'id');

        return view(
            'app.branch_phones.create',
            compact('branches', 'refTypePhones')
        );
    }

    /**
     * @param \App\Http\Requests\BranchPhoneStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(BranchPhoneStoreRequest $request)
    {
        $this->authorize('create', BranchPhone::class);

        $validated = $request->validated();

        $branchPhone = BranchPhone::create($validated);

        return redirect()
            ->route('branch-phones.edit', $branchPhone)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\BranchPhone $branchPhone
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, BranchPhone $branchPhone)
    {
        $this->authorize('view', $branchPhone);

        return view('app.branch_phones.show', compact('branchPhone'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\BranchPhone $branchPhone
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, BranchPhone $branchPhone)
    {
        $this->authorize('update', $branchPhone);

        $branches = Branch::pluck('id', 'id');
        $refTypePhones = RefTypePhone::pluck('id', 'id');

        return view(
            'app.branch_phones.edit',
            compact('branchPhone', 'branches', 'refTypePhones')
        );
    }

    /**
     * @param \App\Http\Requests\BranchPhoneUpdateRequest $request
     * @param \App\Models\BranchPhone $branchPhone
     * @return \Illuminate\Http\Response
     */
    public function update(
        BranchPhoneUpdateRequest $request,
        BranchPhone $branchPhone
    ) {
        $this->authorize('update', $branchPhone);

        $validated = $request->validated();

        $branchPhone->update($validated);

        return redirect()
            ->route('branch-phones.edit', $branchPhone)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\BranchPhone $branchPhone
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, BranchPhone $branchPhone)
    {
        $this->authorize('delete', $branchPhone);

        $branchPhone->delete();

        return redirect()
            ->route('branch-phones.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
