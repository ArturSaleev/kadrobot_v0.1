<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\EmployeeStazh;
use App\Http\Requests\EmployeeStazhStoreRequest;
use App\Http\Requests\EmployeeStazhUpdateRequest;

class EmployeeStazhController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeeStazh::class);

        $search = $request->get('search', '');

        $employeeStazhs = EmployeeStazh::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.employee_stazhs.index',
            compact('employeeStazhs', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', EmployeeStazh::class);

        $employees = Employee::pluck('iin', 'id');

        return view('app.employee_stazhs.create', compact('employees'));
    }

    /**
     * @param \App\Http\Requests\EmployeeStazhStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeStazhStoreRequest $request)
    {
        $this->authorize('create', EmployeeStazh::class);

        $validated = $request->validated();

        $employeeStazh = EmployeeStazh::create($validated);

        return redirect()
            ->route('employee-stazhs.edit', $employeeStazh)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeStazh $employeeStazh
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EmployeeStazh $employeeStazh)
    {
        $this->authorize('view', $employeeStazh);

        return view('app.employee_stazhs.show', compact('employeeStazh'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeStazh $employeeStazh
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, EmployeeStazh $employeeStazh)
    {
        $this->authorize('update', $employeeStazh);

        $employees = Employee::pluck('iin', 'id');

        return view(
            'app.employee_stazhs.edit',
            compact('employeeStazh', 'employees')
        );
    }

    /**
     * @param \App\Http\Requests\EmployeeStazhUpdateRequest $request
     * @param \App\Models\EmployeeStazh $employeeStazh
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeeStazhUpdateRequest $request,
        EmployeeStazh $employeeStazh
    ) {
        $this->authorize('update', $employeeStazh);

        $validated = $request->validated();

        $employeeStazh->update($validated);

        return redirect()
            ->route('employee-stazhs.edit', $employeeStazh)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeStazh $employeeStazh
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, EmployeeStazh $employeeStazh)
    {
        $this->authorize('delete', $employeeStazh);

        $employeeStazh->delete();

        return redirect()
            ->route('employee-stazhs.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
