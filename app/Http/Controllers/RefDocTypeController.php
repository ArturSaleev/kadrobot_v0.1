<?php

namespace App\Http\Controllers;

use App\Models\RefDocType;
use Illuminate\Auth\Access\AuthorizationException as AuthorizationExceptionAlias;
use Illuminate\Http\Request;
use App\Http\Requests\RefDocTypeStoreRequest;
use App\Http\Requests\RefDocTypeUpdateRequest;
use Illuminate\Http\Response;

class RefDocTypeController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationExceptionAlias
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefDocType::class);

        $search = $request->get('search', '');

        $refDocTypes = RefDocType::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.ref_doc_types.index',
            compact('refDocTypes', 'search')
        );
    }

    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationExceptionAlias
     */
    public function create(Request $request)
    {
        $this->authorize('create', RefDocType::class);

        return view('app.ref_doc_types.create');
    }

    /**
     * @param RefDocTypeStoreRequest $request
     * @return Response
     * @throws AuthorizationExceptionAlias
     */
    public function store(RefDocTypeStoreRequest $request)
    {
        $this->authorize('create', RefDocType::class);

        $validated = $request->validated();

        $refDocType = new RefDocType();
        $refDocType = $this->save($validated, $refDocType);

        return redirect()
            ->route('ref-doc-types.edit', $refDocType)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param Request $request
     * @param RefDocType $refDocType
     * @return Response
     * @throws AuthorizationExceptionAlias
     */
    public function show(Request $request, RefDocType $refDocType)
    {
        $this->authorize('view', $refDocType);

        return view('app.ref_doc_types.show', compact('refDocType'));
    }

    /**
     * @param Request $request
     * @param RefDocType $refDocType
     * @return Response
     * @throws AuthorizationExceptionAlias
     */
    public function edit(Request $request, RefDocType $refDocType)
    {
        $this->authorize('update', $refDocType);

        return view('app.ref_doc_types.edit', compact('refDocType'));
    }

    /**
     * @param RefDocTypeUpdateRequest $request
     * @param RefDocType $refDocType
     * @return Response
     * @throws AuthorizationExceptionAlias
     */
    public function update(
        RefDocTypeUpdateRequest $request,
        RefDocType $refDocType
    ) {
        $this->authorize('update', $refDocType);

        $validated = $request->validated();

        $this->save($validated, $refDocType);

        return redirect()
            ->route('ref-doc-types.edit', $refDocType)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param Request $request
     * @param RefDocType $refDocType
     * @return Response
     * @throws AuthorizationExceptionAlias
     */
    public function destroy(Request $request, RefDocType $refDocType)
    {
        $this->authorize('delete', $refDocType);

        $refDocType->delete();

        return redirect()
            ->route('ref-doc-types.index')
            ->withSuccess(__('crud.common.removed'));
    }

    private function save($validated, $model)
    {
        foreach($validated['name'] as $lang => $value){
            $model->translateOrNew($lang)->name = $value;
        }
        $model->save();
        return $model;
    }
}
