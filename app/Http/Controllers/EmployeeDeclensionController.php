<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\EmployeeDeclension;
use App\Http\Requests\EmployeeDeclensionStoreRequest;
use App\Http\Requests\EmployeeDeclensionUpdateRequest;

class EmployeeDeclensionController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeeDeclension::class);

        $search = $request->get('search', '');

        $employeeDeclensions = EmployeeDeclension::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.employee_declensions.index',
            compact('employeeDeclensions', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', EmployeeDeclension::class);

        $employees = Employee::pluck('iin', 'id');

        return view('app.employee_declensions.create', compact('employees'));
    }

    /**
     * @param \App\Http\Requests\EmployeeDeclensionStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeDeclensionStoreRequest $request)
    {
        $this->authorize('create', EmployeeDeclension::class);

        $validated = $request->validated();

        $employeeDeclension = EmployeeDeclension::create($validated);

        return redirect()
            ->route('employee-declensions.edit', $employeeDeclension)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeDeclension $employeeDeclension
     * @return \Illuminate\Http\Response
     */
    public function show(
        Request $request,
        EmployeeDeclension $employeeDeclension
    ) {
        $this->authorize('view', $employeeDeclension);

        return view(
            'app.employee_declensions.show',
            compact('employeeDeclension')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeDeclension $employeeDeclension
     * @return \Illuminate\Http\Response
     */
    public function edit(
        Request $request,
        EmployeeDeclension $employeeDeclension
    ) {
        $this->authorize('update', $employeeDeclension);

        $employees = Employee::pluck('iin', 'id');

        return view(
            'app.employee_declensions.edit',
            compact('employeeDeclension', 'employees')
        );
    }

    /**
     * @param \App\Http\Requests\EmployeeDeclensionUpdateRequest $request
     * @param \App\Models\EmployeeDeclension $employeeDeclension
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeeDeclensionUpdateRequest $request,
        EmployeeDeclension $employeeDeclension
    ) {
        $this->authorize('update', $employeeDeclension);

        $validated = $request->validated();

        $employeeDeclension->update($validated);

        return redirect()
            ->route('employee-declensions.edit', $employeeDeclension)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeDeclension $employeeDeclension
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        Request $request,
        EmployeeDeclension $employeeDeclension
    ) {
        $this->authorize('delete', $employeeDeclension);

        $employeeDeclension->delete();

        return redirect()
            ->route('employee-declensions.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
