<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\EmployeeInvalide;
use App\Http\Requests\EmployeeInvalideStoreRequest;
use App\Http\Requests\EmployeeInvalideUpdateRequest;

class EmployeeInvalideController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeeInvalide::class);

        $search = $request->get('search', '');

        $employeeInvalides = EmployeeInvalide::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.employee_invalides.index',
            compact('employeeInvalides', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', EmployeeInvalide::class);

        $employees = Employee::pluck('iin', 'id');

        return view('app.employee_invalides.create', compact('employees'));
    }

    /**
     * @param \App\Http\Requests\EmployeeInvalideStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeInvalideStoreRequest $request)
    {
        $this->authorize('create', EmployeeInvalide::class);

        $validated = $request->validated();

        $employeeInvalide = EmployeeInvalide::create($validated);

        return redirect()
            ->route('employee-invalides.edit', $employeeInvalide)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeInvalide $employeeInvalide
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EmployeeInvalide $employeeInvalide)
    {
        $this->authorize('view', $employeeInvalide);

        return view('app.employee_invalides.show', compact('employeeInvalide'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeInvalide $employeeInvalide
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, EmployeeInvalide $employeeInvalide)
    {
        $this->authorize('update', $employeeInvalide);

        $employees = Employee::pluck('iin', 'id');

        return view(
            'app.employee_invalides.edit',
            compact('employeeInvalide', 'employees')
        );
    }

    /**
     * @param \App\Http\Requests\EmployeeInvalideUpdateRequest $request
     * @param \App\Models\EmployeeInvalide $employeeInvalide
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeeInvalideUpdateRequest $request,
        EmployeeInvalide $employeeInvalide
    ) {
        $this->authorize('update', $employeeInvalide);

        $validated = $request->validated();

        $employeeInvalide->update($validated);

        return redirect()
            ->route('employee-invalides.edit', $employeeInvalide)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeInvalide $employeeInvalide
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        Request $request,
        EmployeeInvalide $employeeInvalide
    ) {
        $this->authorize('delete', $employeeInvalide);

        $employeeInvalide->delete();

        return redirect()
            ->route('employee-invalides.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
