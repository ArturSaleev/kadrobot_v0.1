<?php

namespace App\Http\Controllers;

use App\Models\RefSex;
use Illuminate\Auth\Access\AuthorizationException as AuthorizationExceptionAlias;
use Illuminate\Http\Request;
use App\Http\Requests\RefSexStoreRequest;
use App\Http\Requests\RefSexUpdateRequest;
use Illuminate\Http\Response;

class RefSexController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationExceptionAlias
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefSex::class);

        $search = $request->get('search', '');

        $refSexes = RefSex::search($search)->paginate()
            ->withQueryString();

        return view('app.ref_sexes.index', compact('refSexes', 'search'));
    }

    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationExceptionAlias
     */
    public function create(Request $request)
    {
        $this->authorize('create', RefSex::class);

        return view('app.ref_sexes.create');
    }

    /**
     * @param RefSexStoreRequest $request
     * @return Response
     * @throws AuthorizationExceptionAlias
     */
    public function store(RefSexStoreRequest $request)
    {
        $this->authorize('create', RefSex::class);

        $validated = $request->validated();

        $refSex = new RefSex();
        $refSex = $this->save($validated, $refSex);

        return redirect()
            ->route('ref-sexes.edit', $refSex)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param Request $request
     * @param RefSex $refSex
     * @return Response
     * @throws AuthorizationExceptionAlias
     */
    public function show(Request $request, RefSex $refSex)
    {
        $this->authorize('view', $refSex);

        return view('app.ref_sexes.show', compact('refSex'));
    }

    /**
     * @param Request $request
     * @param RefSex $refSex
     * @return Response
     * @throws AuthorizationExceptionAlias
     */
    public function edit(Request $request, RefSex $refSex)
    {
        $this->authorize('update', $refSex);

        return view('app.ref_sexes.edit', compact('refSex'));
    }

    /**
     * @param RefSexUpdateRequest $request
     * @param RefSex $refSex
     * @return Response
     * @throws AuthorizationExceptionAlias
     */
    public function update(RefSexUpdateRequest $request, RefSex $refSex)
    {
        $this->authorize('update', $refSex);

        $validated = $request->validated();

        $this->save($validated, $refSex);

        return redirect()
            ->route('ref-sexes.edit', $refSex)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param Request $request
     * @param RefSex $refSex
     * @return Response
     */
    public function destroy(Request $request, RefSex $refSex)
    {
        $this->authorize('delete', $refSex);

        $refSex->delete();

        return redirect()
            ->route('ref-sexes.index')
            ->withSuccess(__('crud.common.removed'));
    }

    private function save($validated, $model)
    {
        foreach($validated['name'] as $lang => $value){
            $model->translateOrNew($lang)->name = $value;
        }
        $model->save();
        return $model;
    }
}
