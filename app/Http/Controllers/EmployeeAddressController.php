<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\RefAddressType;
use App\Models\EmployeeAddress;
use App\Http\Requests\EmployeeAddressStoreRequest;
use App\Http\Requests\EmployeeAddressUpdateRequest;

class EmployeeAddressController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeeAddress::class);

        $search = $request->get('search', '');

        $employeeAddresses = EmployeeAddress::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.employee_addresses.index',
            compact('employeeAddresses', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', EmployeeAddress::class);

        $employees = Employee::pluck('iin', 'id');
        $refAddressTypes = RefAddressType::pluck('id', 'id');

        return view(
            'app.employee_addresses.create',
            compact('employees', 'refAddressTypes')
        );
    }

    /**
     * @param \App\Http\Requests\EmployeeAddressStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeAddressStoreRequest $request)
    {
        $this->authorize('create', EmployeeAddress::class);

        $validated = $request->validated();

        $employeeAddress = EmployeeAddress::create($validated);

        return redirect()
            ->route('employee-addresses.edit', $employeeAddress)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeAddress $employeeAddress
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EmployeeAddress $employeeAddress)
    {
        $this->authorize('view', $employeeAddress);

        return view('app.employee_addresses.show', compact('employeeAddress'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeAddress $employeeAddress
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, EmployeeAddress $employeeAddress)
    {
        $this->authorize('update', $employeeAddress);

        $employees = Employee::pluck('iin', 'id');
        $refAddressTypes = RefAddressType::pluck('id', 'id');

        return view(
            'app.employee_addresses.edit',
            compact('employeeAddress', 'employees', 'refAddressTypes')
        );
    }

    /**
     * @param \App\Http\Requests\EmployeeAddressUpdateRequest $request
     * @param \App\Models\EmployeeAddress $employeeAddress
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeeAddressUpdateRequest $request,
        EmployeeAddress $employeeAddress
    ) {
        $this->authorize('update', $employeeAddress);

        $validated = $request->validated();

        $employeeAddress->update($validated);

        return redirect()
            ->route('employee-addresses.edit', $employeeAddress)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeAddress $employeeAddress
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, EmployeeAddress $employeeAddress)
    {
        $this->authorize('delete', $employeeAddress);

        $employeeAddress->delete();

        return redirect()
            ->route('employee-addresses.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
