<?php

namespace App\Http\Controllers;

use App\Models\Curator;
use App\Models\Employee;
use App\Models\Department;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Http\Requests\CuratorStoreRequest;
use App\Http\Requests\CuratorUpdateRequest;
use Illuminate\Http\Response;

class CuratorController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', Curator::class);

        $search = $request->get('search', '');

        $curators = Curator::search($search)->paginate()
            ->withQueryString();

        return view('app.curators.index', compact('curators', 'search'));
    }

    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function create(Request $request)
    {
        $this->authorize('create', Curator::class);

        $employees = Employee::all();
        $departments = Department::all();

        return view('app.curators.create', compact('employees', 'departments'));
    }

    /**
     * @param CuratorStoreRequest $request
     * @return Response
     * @throws AuthorizationException
     */
    public function store(CuratorStoreRequest $request)
    {
        $this->authorize('create', Curator::class);

        $validated = $request->validated();

        $curator = Curator::create($validated);

        return redirect()
            ->route('curators.edit', $curator)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param Request $request
     * @param Curator $curator
     * @return Response
     * @throws AuthorizationException
     */
    public function show(Request $request, Curator $curator)
    {
        $this->authorize('view', $curator);

        return view('app.curators.show', compact('curator'));
    }

    /**
     * @param Request $request
     * @param Curator $curator
     * @return Response
     * @throws AuthorizationException
     */
    public function edit(Request $request, Curator $curator)
    {
        $this->authorize('update', $curator);

        $employees = Employee::all();
        $departments = Department::all();

        return view(
            'app.curators.edit',
            compact('curator', 'employees', 'departments')
        );
    }

    /**
     * @param CuratorUpdateRequest $request
     * @param Curator $curator
     * @return Response
     * @throws AuthorizationException
     */
    public function update(CuratorUpdateRequest $request, Curator $curator)
    {
        $this->authorize('update', $curator);

        $validated = $request->validated();

        $curator->update($validated);

        return redirect()
            ->route('curators.edit', $curator)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param Request $request
     * @param Curator $curator
     * @return Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request, Curator $curator)
    {
        $this->authorize('delete', $curator);

        $curator->delete();

        return redirect()
            ->route('curators.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
