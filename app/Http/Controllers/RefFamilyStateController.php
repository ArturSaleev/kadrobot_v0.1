<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Models\RefFamilyState;
use App\Http\Requests\RefFamilyStateStoreRequest;
use App\Http\Requests\RefFamilyStateUpdateRequest;
use Illuminate\Http\Response;

class RefFamilyStateController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefFamilyState::class);

        $search = $request->get('search', '');

        $refFamilyStates = RefFamilyState::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.ref_family_states.index',
            compact('refFamilyStates', 'search')
        );
    }

    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function create(Request $request)
    {
        $this->authorize('create', RefFamilyState::class);

        return view('app.ref_family_states.create');
    }

    /**
     * @param RefFamilyStateStoreRequest $request
     * @return Response
     * @throws AuthorizationException
     */
    public function store(RefFamilyStateStoreRequest $request)
    {
        $this->authorize('create', RefFamilyState::class);

        $validated = $request->validated();

        $refFamilyState = new RefFamilyState();
        $refFamilyState = $this->save($validated, $refFamilyState);

        return redirect()
            ->route('ref-family-states.edit', $refFamilyState)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param Request $request
     * @param RefFamilyState $refFamilyState
     * @return Response
     * @throws AuthorizationException
     */
    public function show(Request $request, RefFamilyState $refFamilyState)
    {
        $this->authorize('view', $refFamilyState);

        return view('app.ref_family_states.show', compact('refFamilyState'));
    }

    /**
     * @param Request $request
     * @param RefFamilyState $refFamilyState
     * @return Response
     * @throws AuthorizationException
     */
    public function edit(Request $request, RefFamilyState $refFamilyState)
    {
        $this->authorize('update', $refFamilyState);

        return view('app.ref_family_states.edit', compact('refFamilyState'));
    }

    /**
     * @param RefFamilyStateUpdateRequest $request
     * @param RefFamilyState $refFamilyState
     * @return Response
     * @throws AuthorizationException
     */
    public function update(
        RefFamilyStateUpdateRequest $request,
        RefFamilyState $refFamilyState
    ) {
        $this->authorize('update', $refFamilyState);

        $validated = $request->validated();
        $this->save($validated, $refFamilyState);

        return redirect()
            ->route('ref-family-states.edit', $refFamilyState)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param Request $request
     * @param RefFamilyState $refFamilyState
     * @return Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request, RefFamilyState $refFamilyState)
    {
        $this->authorize('delete', $refFamilyState);

        $refFamilyState->delete();

        return redirect()
            ->route('ref-family-states.index')
            ->withSuccess(__('crud.common.removed'));
    }

    private function save($validated, $model)
    {
        foreach($validated['name'] as $lang => $value){
            $model->translateOrNew($lang)->name = $value;
        }
        $model->save();
        return $model;
    }
}
