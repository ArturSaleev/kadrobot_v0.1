<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Models\RefTypeRodstv;
use App\Http\Requests\RefTypeRodstvStoreRequest;
use App\Http\Requests\RefTypeRodstvUpdateRequest;
use Illuminate\Http\Response;

class RefTypeRodstvController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefTypeRodstv::class);

        $search = $request->get('search', '');

        $refTypeRodstvs = RefTypeRodstv::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.ref_type_rodstvs.index',
            compact('refTypeRodstvs', 'search')
        );
    }

    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function create(Request $request)
    {
        $this->authorize('create', RefTypeRodstv::class);

        return view('app.ref_type_rodstvs.create');
    }

    /**
     * @param RefTypeRodstvStoreRequest $request
     * @return Response
     * @throws AuthorizationException
     */
    public function store(RefTypeRodstvStoreRequest $request)
    {
        $this->authorize('create', RefTypeRodstv::class);

        $validated = $request->validated();

        $refTypeRodstv = new RefTypeRodstv();
        $refTypeRodstv = $this->save($validated, $refTypeRodstv);

        return redirect()
            ->route('ref-type-rodstvs.edit', $refTypeRodstv)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param Request $request
     * @param RefTypeRodstv $refTypeRodstv
     * @return Response
     * @throws AuthorizationException
     */
    public function show(Request $request, RefTypeRodstv $refTypeRodstv)
    {
        $this->authorize('view', $refTypeRodstv);

        return view('app.ref_type_rodstvs.show', compact('refTypeRodstv'));
    }

    /**
     * @param Request $request
     * @param RefTypeRodstv $refTypeRodstv
     * @return Response
     * @throws AuthorizationException
     */
    public function edit(Request $request, RefTypeRodstv $refTypeRodstv)
    {
        $this->authorize('update', $refTypeRodstv);

        return view('app.ref_type_rodstvs.edit', compact('refTypeRodstv'));
    }

    /**
     * @param RefTypeRodstvUpdateRequest $request
     * @param RefTypeRodstv $refTypeRodstv
     * @return Response
     * @throws AuthorizationException
     */
    public function update(
        RefTypeRodstvUpdateRequest $request,
        RefTypeRodstv $refTypeRodstv
    ) {
        $this->authorize('update', $refTypeRodstv);

        $validated = $request->validated();

        $this->save($validated, $refTypeRodstv);

        return redirect()
            ->route('ref-type-rodstvs.edit', $refTypeRodstv)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param Request $request
     * @param RefTypeRodstv $refTypeRodstv
     * @return Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request, RefTypeRodstv $refTypeRodstv)
    {
        $this->authorize('delete', $refTypeRodstv);

        $refTypeRodstv->delete();

        return redirect()
            ->route('ref-type-rodstvs.index')
            ->withSuccess(__('crud.common.removed'));
    }

    private function save($validated, $model)
    {
        foreach($validated['name'] as $lang => $value){
            $model->translateOrNew($lang)->name = $value;
        }
        $model->save();
        return $model;
    }
}
