<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\RefVidHoliday;
use App\Models\EmployeeHoliday;
use App\Http\Requests\EmployeeHolidayStoreRequest;
use App\Http\Requests\EmployeeHolidayUpdateRequest;

class EmployeeHolidayController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeeHoliday::class);

        $search = $request->get('search', '');

        $employeeHolidays = EmployeeHoliday::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.employee_holidays.index',
            compact('employeeHolidays', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', EmployeeHoliday::class);

        $employees = Employee::pluck('iin', 'id');
        $refVidHolidays = RefVidHoliday::pluck('id', 'id');

        return view(
            'app.employee_holidays.create',
            compact('employees', 'refVidHolidays')
        );
    }

    /**
     * @param \App\Http\Requests\EmployeeHolidayStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeHolidayStoreRequest $request)
    {
        $this->authorize('create', EmployeeHoliday::class);

        $validated = $request->validated();

        $employeeHoliday = EmployeeHoliday::create($validated);

        return redirect()
            ->route('employee-holidays.edit', $employeeHoliday)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeHoliday $employeeHoliday
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EmployeeHoliday $employeeHoliday)
    {
        $this->authorize('view', $employeeHoliday);

        return view('app.employee_holidays.show', compact('employeeHoliday'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeHoliday $employeeHoliday
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, EmployeeHoliday $employeeHoliday)
    {
        $this->authorize('update', $employeeHoliday);

        $employees = Employee::pluck('iin', 'id');
        $refVidHolidays = RefVidHoliday::pluck('id', 'id');

        return view(
            'app.employee_holidays.edit',
            compact('employeeHoliday', 'employees', 'refVidHolidays')
        );
    }

    /**
     * @param \App\Http\Requests\EmployeeHolidayUpdateRequest $request
     * @param \App\Models\EmployeeHoliday $employeeHoliday
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeeHolidayUpdateRequest $request,
        EmployeeHoliday $employeeHoliday
    ) {
        $this->authorize('update', $employeeHoliday);

        $validated = $request->validated();

        $employeeHoliday->update($validated);

        return redirect()
            ->route('employee-holidays.edit', $employeeHoliday)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeHoliday $employeeHoliday
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, EmployeeHoliday $employeeHoliday)
    {
        $this->authorize('delete', $employeeHoliday);

        $employeeHoliday->delete();

        return redirect()
            ->route('employee-holidays.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
