<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\RefOked;
use App\Models\RefStatus;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Http\Requests\CompanyStoreRequest;
use App\Http\Requests\CompanyUpdateRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class CompanyController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('view', Company::class);
        $company = Company::first();

        return view('app.companies.show', compact('company'));
    }

    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function create(Request $request)
    {
        $this->authorize('create', Company::class);

        $refStatuses = RefStatus::all();

        return view('app.companies.create', compact('refStatuses'));
    }

    /**
     * @param CompanyStoreRequest $request
     * @return Response
     * @throws AuthorizationException
     */
    public function store(CompanyStoreRequest $request)
    {
        $this->authorize('create', Company::class);

        $validated = $request->validated();

        $company = Company::create($validated);

        return redirect()
            ->route('companies.edit', $company)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param Request $request
     * @param Company $company
     * @return Response
     * @throws AuthorizationException
     */
    public function show(Request $request, Company $company)
    {
        $this->authorize('view', $company);
        $oked = RefOked::find($company->oked);
        return view('app.companies.show', compact('company', 'oked'));
    }

    /**
     * @param Request $request
     * @param Company $company
     * @return Response
     * @throws AuthorizationException
     */
    public function edit(Request $request, Company $company)
    {
        $this->authorize('update', $company);

        $refOkeds = RefOked::all();

        return view('app.companies.edit', compact('company', 'refOkeds'));
    }

    /**
     * @param CompanyUpdateRequest $request
     * @param Company $company
     * @return Response
     * @throws AuthorizationException
     */
    public function update(CompanyUpdateRequest $request, Company $company)
    {
        $this->authorize('update', $company);

        $company->bin = $request['bin'];
        $company->rnn = $request['rnn'];
        $company->oked = $request['oked'];
        foreach($request['name'] as $lang=>$value){
            $company->translateOrNew($lang)->name = $value;
        }
        $company->save();
        return redirect()
            ->route('companies.show', $company)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param Request $request
     * @param Company $company
     * @return Response
     */
    public function destroy(Request $request, Company $company)
    {
        $this->authorize('delete', $company);

//        $company->delete();

        return redirect()
            ->route('companies.show', $company)
            ->withErrors(__('crud.common.removed_error'));
    }
}
