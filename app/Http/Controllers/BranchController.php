<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\BranchAddreses;
use App\Models\BranchPhone;
use App\Models\Company;
use App\Models\RefAddressType;
use App\Models\RefTypePhone;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Http\Requests\BranchStoreRequest;
use App\Http\Requests\BranchUpdateRequest;
use Illuminate\Http\Response;

class BranchController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', Branch::class);

        $search = $request->get('search', '');

        $branches = Branch::search($search)
            ->paginate()
            ->withQueryString();

        return view('app.branches.index', compact('branches', 'search'));
    }

    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function create(Request $request)
    {
        $this->authorize('create', Branch::class);
        $refTypePhone = RefTypePhone::all();
        $refTypeAddress = RefAddressType::all();
        return view('app.branches.create', compact('refTypePhone', 'refTypeAddress'));
    }

    /**
     * @param BranchStoreRequest $request
     * @return Response
     * @throws AuthorizationException
     */
    public function store(BranchStoreRequest $request)
    {
        $this->authorize('create', Branch::class);

        $validated = $request->validated();

        $branch = new Branch();
        $branchData = $this->save($validated, $branch);

//        $branch = Branch::create($validated);

        return redirect()
            ->route('branches.edit', $branchData)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param Request $request
     * @param Branch $branch
     * @return Response
     * @throws AuthorizationException
     */
    public function show(Request $request, Branch $branch)
    {
        $this->authorize('view', $branch);

        return view('app.branches.show', compact('branch'));
    }

    /**
     * @param Request $request
     * @param Branch $branch
     * @return Response
     * @throws AuthorizationException
     */
    public function edit(Request $request, Branch $branch)
    {
        $this->authorize('update', $branch);

        $refTypePhone = RefTypePhone::all();
        $refTypeAddress = RefAddressType::all();

        return view('app.branches.edit', compact('branch', 'refTypePhone', 'refTypeAddress'));
    }

    /**
     * @param BranchUpdateRequest $request
     * @param Branch $branch
     * @return Response
     * @throws AuthorizationException
     */
    public function update(BranchUpdateRequest $request, Branch $branch)
    {
        $this->authorize('update', $branch);

        $validated = $request->validated();

        $this->save($validated, $branch);

//        $res = $branch->update($validated);
        return redirect()
            ->route('branches.edit', $branch)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param Request $request
     * @param Branch $branch
     * @return Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request, Branch $branch)
    {
        $this->authorize('delete', $branch);

        $branch->delete();

        return redirect()
            ->route('branches.index')
            ->withSuccess(__('crud.common.removed'));
    }

    private function save($validated, $branch)
    {
        $branch->company_id = $validated['company_id'];
        $branch->branch_main = $validated['branch_main'];
        foreach($validated['name'] as $lang=>$value){
            $branch->translateOrNew($lang)->name = $value;
        }
        $branch->save();

        if(isset($validated['branchAddress'])){
            $branch->branchAddreses()->delete();
            foreach($validated['branchAddress'] as $key=>$value){
                $branchAddress = new BranchAddreses();
                $branchAddress->branch_id = $branch->id;
                $branchAddress->ref_address_type_id = $value['ref_type_address_id'];
                foreach($value['name'] as $lang=>$name){
                    $branchAddress->translateOrNew($lang)->name = $name;
                }
                $branchAddress->save();
            }
        }

        if(isset($validated['phones'])){
            $branch->branchPhones()->delete();
            foreach($validated['phones'] as $key=>$value){
                $branchPhone = new BranchPhone();
                $branchPhone->branch_id = $branch->id;
                $branchPhone->ref_type_phone_id = $value['ref_type_phone_id'];
                $branchPhone->name = $value['name'];
                $branchPhone->save();
            }
        }
        return $branch;
    }
}
