<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RefVidHoliday;
use App\Models\RefVidHolidayType;
use App\Http\Requests\RefVidHolidayStoreRequest;
use App\Http\Requests\RefVidHolidayUpdateRequest;

class RefVidHolidayController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefVidHoliday::class);

        $search = $request->get('search', '');

        $refVidHolidays = RefVidHoliday::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.ref_vid_holidays.index',
            compact('refVidHolidays', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', RefVidHoliday::class);

        $refVidHolidayTypes = RefVidHolidayType::pluck('id', 'id');

        return view(
            'app.ref_vid_holidays.create',
            compact('refVidHolidayTypes')
        );
    }

    /**
     * @param \App\Http\Requests\RefVidHolidayStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefVidHolidayStoreRequest $request)
    {
        $this->authorize('create', RefVidHoliday::class);

        $validated = $request->validated();

        $refVidHoliday = RefVidHoliday::create($validated);

        return redirect()
            ->route('ref-vid-holidays.edit', $refVidHoliday)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefVidHoliday $refVidHoliday
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefVidHoliday $refVidHoliday)
    {
        $this->authorize('view', $refVidHoliday);

        return view('app.ref_vid_holidays.show', compact('refVidHoliday'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefVidHoliday $refVidHoliday
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, RefVidHoliday $refVidHoliday)
    {
        $this->authorize('update', $refVidHoliday);

        $refVidHolidayTypes = RefVidHolidayType::pluck('id', 'id');

        return view(
            'app.ref_vid_holidays.edit',
            compact('refVidHoliday', 'refVidHolidayTypes')
        );
    }

    /**
     * @param \App\Http\Requests\RefVidHolidayUpdateRequest $request
     * @param \App\Models\RefVidHoliday $refVidHoliday
     * @return \Illuminate\Http\Response
     */
    public function update(
        RefVidHolidayUpdateRequest $request,
        RefVidHoliday $refVidHoliday
    ) {
        $this->authorize('update', $refVidHoliday);

        $validated = $request->validated();

        $refVidHoliday->update($validated);

        return redirect()
            ->route('ref-vid-holidays.edit', $refVidHoliday)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefVidHoliday $refVidHoliday
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, RefVidHoliday $refVidHoliday)
    {
        $this->authorize('delete', $refVidHoliday);

        $refVidHoliday->delete();

        return redirect()
            ->route('ref-vid-holidays.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
