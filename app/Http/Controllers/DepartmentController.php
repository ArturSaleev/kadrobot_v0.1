<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\Department;
use App\Models\DepartmentPhone;
use App\Models\RefTypePhone;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Http\Requests\DepartmentStoreRequest;
use App\Http\Requests\DepartmentUpdateRequest;
use Illuminate\Http\Response;

class DepartmentController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', Department::class);

        $search = $request->get('search', '');

        $departments = Department::search($search)
            ->paginate()
            ->withQueryString();

        return view('app.departments.index', compact('departments', 'search'));
    }

    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function create(Request $request)
    {
        $this->authorize('create', Department::class);

        $branches = Branch::all();
        $refTypePhone = RefTypePhone::all();

        return view('app.departments.create', compact('branches', 'refTypePhone'));
    }

    /**
     * @param DepartmentStoreRequest $request
     * @return Response
     * @throws AuthorizationException
     */
    public function store(DepartmentStoreRequest $request)
    {
        $this->authorize('create', Department::class);

        $validated = $request->validated();

        $department = new Department();
        $department = $this->save($validated, $department);

        return redirect()
            ->route('departments.edit', $department)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param Request $request
     * @param Department $department
     * @return Response
     * @throws AuthorizationException
     */
    public function show(Request $request, Department $department)
    {
        $this->authorize('view', $department);

        return view('app.departments.show', compact('department'));
    }

    /**
     * @param Request $request
     * @param Department $department
     * @return Response
     * @throws AuthorizationException
     */
    public function edit(Request $request, Department $department)
    {
        $this->authorize('update', $department);

        $branches = Branch::all();
        $refTypePhone = RefTypePhone::all();

        return view('app.departments.edit', compact('department', 'branches', 'refTypePhone'));
    }

    /**
     * @param DepartmentUpdateRequest $request
     * @param Department $department
     * @return Response
     * @throws AuthorizationException
     */
    public function update(DepartmentUpdateRequest $request, Department $department) {
        $this->authorize('update', $department);

        $validated = $request->validated();
        $this->save($validated, $department);

        return redirect()
            ->route('departments.edit', $department)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param Request $request
     * @param Department $department
     * @return Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request, Department $department)
    {
        $this->authorize('delete', $department);

        $department->delete();

        return redirect()
            ->route('departments.index')
            ->withSuccess(__('crud.common.removed'));
    }

    private function save($validated, $department)
    {
        $department->parent_id = isset($validated['parent_id']) ? $validated['parent_id'] : 0;
        $department->branch_id = $validated['branch_id'];
        $department->email = $validated['email'];
        foreach($validated['name'] as $lang=>$value){
            $department->translateOrNew($lang)->name = $value;
        }
        $department->save();

        if(isset($validated['phones'])){
            $department->departmentPhones()->delete();
            foreach($validated['phones'] as $key=>$value){
                $phone = new DepartmentPhone();
                $phone->department_id = $department->id;
                $phone->ref_type_phone_id = $value['ref_type_phone_id'];
                $phone->name = $value['name'];
                $phone->save();
            }
        }
        return $department;
    }
}
