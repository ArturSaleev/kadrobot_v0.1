<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RefVidHolidayType;
use App\Http\Requests\RefVidHolidayTypeStoreRequest;
use App\Http\Requests\RefVidHolidayTypeUpdateRequest;

class RefVidHolidayTypeController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefVidHolidayType::class);

        $search = $request->get('search', '');

        $refVidHolidayTypes = RefVidHolidayType::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.ref_vid_holiday_types.index',
            compact('refVidHolidayTypes', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', RefVidHolidayType::class);

        return view('app.ref_vid_holiday_types.create');
    }

    /**
     * @param \App\Http\Requests\RefVidHolidayTypeStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefVidHolidayTypeStoreRequest $request)
    {
        $this->authorize('create', RefVidHolidayType::class);

        $validated = $request->validated();

        $refVidHolidayType = RefVidHolidayType::create($validated);

        return redirect()
            ->route('ref-vid-holiday-types.edit', $refVidHolidayType)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefVidHolidayType $refVidHolidayType
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefVidHolidayType $refVidHolidayType)
    {
        $this->authorize('view', $refVidHolidayType);

        return view(
            'app.ref_vid_holiday_types.show',
            compact('refVidHolidayType')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefVidHolidayType $refVidHolidayType
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, RefVidHolidayType $refVidHolidayType)
    {
        $this->authorize('update', $refVidHolidayType);

        return view(
            'app.ref_vid_holiday_types.edit',
            compact('refVidHolidayType')
        );
    }

    /**
     * @param \App\Http\Requests\RefVidHolidayTypeUpdateRequest $request
     * @param \App\Models\RefVidHolidayType $refVidHolidayType
     * @return \Illuminate\Http\Response
     */
    public function update(
        RefVidHolidayTypeUpdateRequest $request,
        RefVidHolidayType $refVidHolidayType
    ) {
        $this->authorize('update', $refVidHolidayType);

        $validated = $request->validated();

        $refVidHolidayType->update($validated);

        return redirect()
            ->route('ref-vid-holiday-types.edit', $refVidHolidayType)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefVidHolidayType $refVidHolidayType
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        Request $request,
        RefVidHolidayType $refVidHolidayType
    ) {
        $this->authorize('delete', $refVidHolidayType);

        $refVidHolidayType->delete();

        return redirect()
            ->route('ref-vid-holiday-types.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
