<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Models\RefUserStatus;
use App\Http\Requests\RefUserStatusStoreRequest;
use App\Http\Requests\RefUserStatusUpdateRequest;
use Illuminate\Http\Response;

class RefUserStatusController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefUserStatus::class);

        $search = $request->get('search', '');

        $refUserStatuses = RefUserStatus::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.ref_user_statuses.index',
            compact('refUserStatuses', 'search')
        );
    }

    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function create(Request $request)
    {
        $this->authorize('create', RefUserStatus::class);

        return view('app.ref_user_statuses.create');
    }

    /**
     * @param RefUserStatusStoreRequest $request
     * @return Response
     * @throws AuthorizationException
     */
    public function store(RefUserStatusStoreRequest $request)
    {
        $this->authorize('create', RefUserStatus::class);

        $validated = $request->validated();

        $refUserStatus = new RefUserStatus();
        $refUserStatus = $this->save($validated, $refUserStatus);

        return redirect()
            ->route('ref-user-statuses.edit', $refUserStatus)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param Request $request
     * @param RefUserStatus $refUserStatus
     * @return Response
     * @throws AuthorizationException
     */
    public function show(Request $request, RefUserStatus $refUserStatus)
    {
        $this->authorize('view', $refUserStatus);

        return view('app.ref_user_statuses.show', compact('refUserStatus'));
    }

    /**
     * @param Request $request
     * @param RefUserStatus $refUserStatus
     * @return Response
     * @throws AuthorizationException
     */
    public function edit(Request $request, RefUserStatus $refUserStatus)
    {
        $this->authorize('update', $refUserStatus);

        return view('app.ref_user_statuses.edit', compact('refUserStatus'));
    }

    /**
     * @param RefUserStatusUpdateRequest $request
     * @param RefUserStatus $refUserStatus
     * @return Response
     * @throws AuthorizationException
     */
    public function update(
        RefUserStatusUpdateRequest $request,
        RefUserStatus $refUserStatus
    ) {
        $this->authorize('update', $refUserStatus);

        $validated = $request->validated();

        $this->save($validated, $refUserStatus);

        return redirect()
            ->route('ref-user-statuses.edit', $refUserStatus)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param Request $request
     * @param RefUserStatus $refUserStatus
     * @return Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request, RefUserStatus $refUserStatus)
    {
        $this->authorize('delete', $refUserStatus);

        $refUserStatus->delete();

        return redirect()
            ->route('ref-user-statuses.index')
            ->withSuccess(__('crud.common.removed'));
    }

    private function save($validated, $model)
    {
        foreach($validated['name'] as $lang => $value){
            $model->translateOrNew($lang)->name = $value;
        }
        $model->save();
        return $model;
    }
}
