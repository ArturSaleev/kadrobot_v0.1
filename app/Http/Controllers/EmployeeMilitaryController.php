<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\EmployeeMilitary;
use App\Http\Requests\EmployeeMilitaryStoreRequest;
use App\Http\Requests\EmployeeMilitaryUpdateRequest;

class EmployeeMilitaryController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeeMilitary::class);

        $search = $request->get('search', '');

        $employeeMilitaries = EmployeeMilitary::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.employee_militaries.index',
            compact('employeeMilitaries', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', EmployeeMilitary::class);

        $employees = Employee::pluck('iin', 'id');

        return view('app.employee_militaries.create', compact('employees'));
    }

    /**
     * @param \App\Http\Requests\EmployeeMilitaryStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeMilitaryStoreRequest $request)
    {
        $this->authorize('create', EmployeeMilitary::class);

        $validated = $request->validated();

        $employeeMilitary = EmployeeMilitary::create($validated);

        return redirect()
            ->route('employee-militaries.edit', $employeeMilitary)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeMilitary $employeeMilitary
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EmployeeMilitary $employeeMilitary)
    {
        $this->authorize('view', $employeeMilitary);

        return view(
            'app.employee_militaries.show',
            compact('employeeMilitary')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeMilitary $employeeMilitary
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, EmployeeMilitary $employeeMilitary)
    {
        $this->authorize('update', $employeeMilitary);

        $employees = Employee::pluck('iin', 'id');

        return view(
            'app.employee_militaries.edit',
            compact('employeeMilitary', 'employees')
        );
    }

    /**
     * @param \App\Http\Requests\EmployeeMilitaryUpdateRequest $request
     * @param \App\Models\EmployeeMilitary $employeeMilitary
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeeMilitaryUpdateRequest $request,
        EmployeeMilitary $employeeMilitary
    ) {
        $this->authorize('update', $employeeMilitary);

        $validated = $request->validated();

        $employeeMilitary->update($validated);

        return redirect()
            ->route('employee-militaries.edit', $employeeMilitary)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeMilitary $employeeMilitary
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        Request $request,
        EmployeeMilitary $employeeMilitary
    ) {
        $this->authorize('delete', $employeeMilitary);

        $employeeMilitary->delete();

        return redirect()
            ->route('employee-militaries.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
