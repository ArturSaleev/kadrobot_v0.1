<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\EmployeeDeclension;
use App\Models\Position;
use App\Models\RefSex;
use App\Models\Employee;
use App\Traits\DeclensionTrait;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Models\RefUserStatus;
use App\Models\RefNationality;
use App\Models\RefFamilyState;
use App\Models\RefAccountType;
use App\Http\Requests\EmployeeStoreRequest;
use App\Http\Requests\EmployeeUpdateRequest;
use Illuminate\Http\Response;

class EmployeeController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', Employee::class);

        $search = $request->get('search', '');

        $employees = Employee::search($search)
            ->paginate()
            ->withQueryString();

        return view('app.employees.index', compact('employees', 'search'));
    }

    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function create(Request $request)
    {
        $this->authorize('create', Employee::class);

        $refSexes = RefSex::all();
        $refNationalities = RefNationality::all();
        $refFamilyStates = RefFamilyState::all();
        $refAccountTypes = RefAccountType::all();
        $refUserStatuses = RefUserStatus::all();
        //$positions = Position::all();
        $positions = Branch::with(['refDepartments' => function($query){
            return $query->with(['positions']);
        }])->get();

        return view('app.employees.create', compact(
                'refSexes',
                'refNationalities',
                'refFamilyStates',
                'refAccountTypes',
                'refUserStatuses',
                'positions'
            )
        );
    }

    /**
     * @param EmployeeStoreRequest $request
     * @return Response
     * @throws AuthorizationException
     */
    public function store(EmployeeStoreRequest $request)
    {
        $this->authorize('create', Employee::class);

        $validated = $request->validated();

        $employee = new Employee();
        $employee = $this->save($validated, $employee);

        return redirect()
            ->route('employees.show', $employee)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param Request $request
     * @param Employee $employee
     * @return Response
     * @throws AuthorizationException
     */
    public function show(Request $request, Employee $employee)
    {
        $this->authorize('view', $employee);

        return view('app.employees.show', compact('employee'));
    }

    /**
     * @param Request $request
     * @param Employee $employee
     * @return Response
     * @throws AuthorizationException
     */
    public function edit(Request $request, Employee $employee)
    {
        $this->authorize('update', $employee);

        $refSexes = RefSex::all();
        $refNationalities = RefNationality::all();
        $refFamilyStates = RefFamilyState::all();
        $refAccountTypes = RefAccountType::all();
        $refUserStatuses = RefUserStatus::all();
        $positions = Branch::with(['refDepartments' => function($query){
            return $query->with(['positions']);
        }])->get();

        return view(
            'app.employees.edit',
            compact(
                'employee',
                'refSexes',
                'refNationalities',
                'refFamilyStates',
                'refAccountTypes',
                'refUserStatuses',
                'positions'
            )
        );
    }

    /**
     * @param EmployeeUpdateRequest $request
     * @param Employee $employee
     * @return Response
     * @throws AuthorizationException
     */
    public function update(EmployeeUpdateRequest $request, Employee $employee)
    {
        $this->authorize('update', $employee);

        $validated = $request->validated();

        $this->save($validated, $employee);

        return redirect()
            ->route('employees.edit', $employee)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param Request $request
     * @param Employee $employee
     * @return Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request, Employee $employee)
    {
        $this->authorize('delete', $employee);

        $employee->delete();

        return redirect()
            ->route('employees.index')
            ->withSuccess(__('crud.common.removed'));
    }

    private function save($validated, $employee)
    {
        $employee->iin = $validated['iin'];
        $employee->date_post = $validated['date_post'];
//        $employee->date_loyoff = $validated['date_loyoff'];
//        $employee->reason_loyoff = $validated[''];
        $employee->contract_num = $validated['contract_num'];
        $employee->contract_date = $validated['contract_date'];
        $employee->tab_num = $validated['tab_num'];
        $employee->birthday = $validated['birthday'];
        $employee->birth_place = $validated['birth_place'];
        $employee->ref_sex_id = $validated['ref_sex_id'];
        $employee->ref_nationality_id = $validated['ref_nationality_id'];
        $employee->ref_family_state_id = $validated['ref_family_state_id'];
        $employee->ref_account_type_id = $validated['ref_account_type_id'];
        $employee->ref_user_status_id = $validated['ref_user_status_id'];
        $employee->position_id = $validated['position_id'];
        $employee->email = $validated['email'];
        $employee->account = $validated['account'];
        $employee->date_zav = $validated['date_zav'];
        $employee->oklad = $validated['oklad'];
//        $employee->gos_nagr = $validated['gos_nagr'];
//        $employee->pens = $validated['pens'];
//        $employee->pens_date = $validated['pens_date'];
//        $employee->lgot = $validated['lgot'];
        $employee->person_email = $validated['person_email'];
        $employee->save();

        EmployeeDeclension::query()->where('employee_id', $employee->id)->delete();
        if(isset($validated['declensions'])){
            dd($validated['declensions']);

            foreach($validated['declensions'] as $item){
//                foreach($items as $item) {
                    $empDecl = new EmployeeDeclension();
                    $empDecl->employee_id = $employee->id;
                    $empDecl->locale = $item['lang'];
                    $empDecl->lastname = $item['lastname'];
                    $empDecl->firstname = $item['firstname'];
                    $empDecl->middlename = $item['middlename'];
                    $empDecl->case_type = $item['type'];
                    $empDecl->type_description = $item['name'];
                    $empDecl->save();
//                }
            }
        }else{
            $empDeclensions = DeclensionTrait::getDeclensions(true, $validated['lastname'], $validated['firstname'], $validated['middlename']);
            foreach($empDeclensions as $lang=>$items){
                foreach($items as $item) {
                    $empDecl = new EmployeeDeclension();
                    $empDecl->employee_id = $employee->id;
                    $empDecl->locale = $lang;
                    $empDecl->lastname = $item['lastname'];
                    $empDecl->firstname = $item['firstname'];
                    $empDecl->middlename = $item['middlename'];
                    $empDecl->case_type = $item['type'];
                    $empDecl->type_description = $item['name'];
                    $empDecl->save();
                }
            }
        }

        return $employee;
    }
}
