<?php

namespace App\Http\Controllers;

use App\Models\RefMeta;
use Illuminate\Http\Request;
use App\Http\Requests\RefMetaStoreRequest;
use App\Http\Requests\RefMetaUpdateRequest;

class RefMetaController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefMeta::class);

        $search = $request->get('search', '');

        $refMetas = RefMeta::search($search)->paginate()
            ->withQueryString();

        return view('app.ref_metas.index', compact('refMetas', 'search'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', RefMeta::class);

        return view('app.ref_metas.create');
    }

    /**
     * @param \App\Http\Requests\RefMetaStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefMetaStoreRequest $request)
    {
        $this->authorize('create', RefMeta::class);

        $validated = $request->validated();

        $refMeta = RefMeta::create($validated);

        return redirect()
            ->route('ref-metas.edit', $refMeta)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefMeta $refMeta
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefMeta $refMeta)
    {
        $this->authorize('view', $refMeta);

        return view('app.ref_metas.show', compact('refMeta'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefMeta $refMeta
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, RefMeta $refMeta)
    {
        $this->authorize('update', $refMeta);

        return view('app.ref_metas.edit', compact('refMeta'));
    }

    /**
     * @param \App\Http\Requests\RefMetaUpdateRequest $request
     * @param \App\Models\RefMeta $refMeta
     * @return \Illuminate\Http\Response
     */
    public function update(RefMetaUpdateRequest $request, RefMeta $refMeta)
    {
        $this->authorize('update', $refMeta);

        $validated = $request->validated();

        $refMeta->update($validated);

        return redirect()
            ->route('ref-metas.edit', $refMeta)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefMeta $refMeta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, RefMeta $refMeta)
    {
        $this->authorize('delete', $refMeta);

        $refMeta->delete();

        return redirect()
            ->route('ref-metas.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
