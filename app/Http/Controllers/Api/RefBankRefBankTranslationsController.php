<?php

namespace App\Http\Controllers\Api;

use App\Models\RefBank;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefBankTranslationResource;
use App\Http\Resources\RefBankTranslationCollection;

class RefBankRefBankTranslationsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefBank $refBank
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefBank $refBank)
    {
        $this->authorize('view', $refBank);

        $search = $request->get('search', '');

        $refBankTranslations = $refBank
            ->refBankTranslations()
            ->search($search)->paginate();

        return new RefBankTranslationCollection($refBankTranslations);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefBank $refBank
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefBank $refBank)
    {
        $this->authorize('create', RefBankTranslation::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'name' => ['required', 'max:255', 'string'],
        ]);

        $refBankTranslation = $refBank
            ->refBankTranslations()
            ->create($validated);

        return new RefBankTranslationResource($refBankTranslation);
    }
}
