<?php

namespace App\Http\Controllers\Api;

use App\Models\Branch;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\BranchPhoneResource;
use App\Http\Resources\BranchPhoneCollection;

class BranchBranchPhonesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Branch $branch
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Branch $branch)
    {
        $this->authorize('view', $branch);

        $search = $request->get('search', '');

        $branchPhones = $branch
            ->branchPhones()
            ->search($search)->paginate();

        return new BranchPhoneCollection($branchPhones);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Branch $branch
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Branch $branch)
    {
        $this->authorize('create', BranchPhone::class);

        $validated = $request->validate([
            'ref_type_phone_id' => ['required', 'exists:ref_type_phones,id'],
            'name' => ['required', 'max:255', 'string'],
        ]);

        $branchPhone = $branch->branchPhones()->create($validated);

        return new BranchPhoneResource($branchPhone);
    }
}
