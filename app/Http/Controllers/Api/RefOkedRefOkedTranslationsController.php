<?php

namespace App\Http\Controllers\Api;

use App\Models\RefOked;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefOkedTranslationResource;
use App\Http\Resources\RefOkedTranslationCollection;

class RefOkedRefOkedTranslationsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefOked $refOked
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefOked $refOked)
    {
        $this->authorize('view', $refOked);

        $search = $request->get('search', '');

        $refOkedTranslations = $refOked
            ->refOkedTransations()
            ->search($search)->paginate();

        return new RefOkedTranslationCollection($refOkedTranslations);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefOked $refOked
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefOked $refOked)
    {
        $this->authorize('create', RefOkedTranslation::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'name_oked' => ['nullable', 'max:255', 'string'],
            'name' => ['nullable', 'max:255', 'string'],
        ]);

        $refOkedTranslation = $refOked
            ->refOkedTransations()
            ->create($validated);

        return new RefOkedTranslationResource($refOkedTranslation);
    }
}
