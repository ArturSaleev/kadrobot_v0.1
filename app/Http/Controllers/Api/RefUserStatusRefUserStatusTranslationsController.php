<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefUserStatus;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefUserStatusTranslationResource;
use App\Http\Resources\RefUserStatusTranslationCollection;

class RefUserStatusRefUserStatusTranslationsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefUserStatus $refUserStatus
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefUserStatus $refUserStatus)
    {
        $this->authorize('view', $refUserStatus);

        $search = $request->get('search', '');

        $refUserStatusTranslations = $refUserStatus
            ->refUserStatusTranslations()
            ->search($search)->paginate();

        return new RefUserStatusTranslationCollection(
            $refUserStatusTranslations
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefUserStatus $refUserStatus
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefUserStatus $refUserStatus)
    {
        $this->authorize('create', RefUserStatusTranslation::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'name' => ['required', 'max:255', 'string'],
        ]);

        $refUserStatusTranslation = $refUserStatus
            ->refUserStatusTranslations()
            ->create($validated);

        return new RefUserStatusTranslationResource($refUserStatusTranslation);
    }
}
