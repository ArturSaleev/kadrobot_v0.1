<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefAddressType;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeAddressResource;
use App\Http\Resources\EmployeeAddressCollection;

class RefAddressTypeEmployeeAddressesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefAddressType $refAddressType
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefAddressType $refAddressType)
    {
        $this->authorize('view', $refAddressType);

        $search = $request->get('search', '');

        $employeeAddresses = $refAddressType
            ->employeeAddresses()
            ->search($search)->paginate();

        return new EmployeeAddressCollection($employeeAddresses);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefAddressType $refAddressType
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefAddressType $refAddressType)
    {
        $this->authorize('create', EmployeeAddress::class);

        $validated = $request->validate([
            'employee_id' => ['required', 'exists:employees,id'],
            'locale' => ['required', 'max:8', 'string'],
        ]);

        $employeeAddress = $refAddressType
            ->employeeAddresses()
            ->create($validated);

        return new EmployeeAddressResource($employeeAddress);
    }
}
