<?php

namespace App\Http\Controllers\Api;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeMilitaryResource;
use App\Http\Resources\EmployeeMilitaryCollection;

class EmployeeEmployeeMilitariesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Employee $employee)
    {
        $this->authorize('view', $employee);

        $search = $request->get('search', '');

        $employeeMilitaries = $employee
            ->employeeMilitaries()
            ->search($search)->paginate();

        return new EmployeeMilitaryCollection($employeeMilitaries);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Employee $employee)
    {
        $this->authorize('create', EmployeeMilitary::class);

        $validated = $request->validate([
            'group' => ['required', 'max:255', 'string'],
            'category' => ['required', 'max:255', 'string'],
            'rank' => ['required', 'max:255', 'string'],
            'speciality' => ['required', 'max:255', 'string'],
            'voenkom' => ['required', 'max:255', 'string'],
            'spec_uch' => ['required', 'max:255', 'string'],
            'spec_uch_num' => ['required', 'max:255', 'string'],
            'fit' => ['required', 'max:255', 'string'],
        ]);

        $employeeMilitary = $employee->employeeMilitaries()->create($validated);

        return new EmployeeMilitaryResource($employeeMilitary);
    }
}
