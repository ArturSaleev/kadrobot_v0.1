<?php

namespace App\Http\Controllers\Api;

use App\Models\RefSex;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeResource;
use App\Http\Resources\EmployeeCollection;

class RefSexEmployeesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefSex $refSex
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefSex $refSex)
    {
        $this->authorize('view', $refSex);

        $search = $request->get('search', '');

        $employees = $refSex
            ->employees()
            ->search($search)->paginate();

        return new EmployeeCollection($employees);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefSex $refSex
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefSex $refSex)
    {
        $this->authorize('create', Employee::class);

        $validated = $request->validate([
            'date_post' => ['nullable', 'date'],
            'date_loyoff' => ['nullable', 'date'],
            'tab_num' => ['nullable', 'numeric'],
            'birthday' => ['nullable', 'date'],
            'birth_place' => ['nullable', 'max:255', 'string'],
            'iin' => ['required', 'max:255', 'string'],
            'ref_nationality_id' => ['nullable', 'exists:ref_nationalities,id'],
            'ref_family_state_id' => [
                'nullable',
                'exists:ref_family_states,id',
            ],
            'contract_num' => ['nullable', 'max:255', 'string'],
            'contract_date' => ['nullable', 'date'],
            'ref_account_type_id' => [
                'required',
                'exists:ref_account_types,id',
            ],
            'email' => ['required', 'email'],
            'account' => ['nullable', 'max:255', 'string'],
            'date_zav' => ['required', 'date'],
            'oklad' => ['nullable', 'numeric'],
            'gos_nagr' => ['required', 'max:255', 'string'],
            'pens' => ['required', 'boolean'],
            'pens_date' => ['nullable', 'date'],
            'lgot' => ['nullable', 'max:255', 'string'],
            'person_email' => ['nullable', 'max:255', 'string'],
            'ref_user_status_id' => ['required', 'exists:ref_user_statuses,id'],
        ]);

        $employee = $refSex->employees()->create($validated);

        return new EmployeeResource($employee);
    }
}
