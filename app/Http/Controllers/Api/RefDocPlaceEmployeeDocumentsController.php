<?php

namespace App\Http\Controllers\Api;

use App\Models\RefDocPlace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeDocumentResource;
use App\Http\Resources\EmployeeDocumentCollection;

class RefDocPlaceEmployeeDocumentsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefDocPlace $refDocPlace
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefDocPlace $refDocPlace)
    {
        $this->authorize('view', $refDocPlace);

        $search = $request->get('search', '');

        $employeeDocuments = $refDocPlace
            ->employeeDocuments()
            ->search($search)->paginate();

        return new EmployeeDocumentCollection($employeeDocuments);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefDocPlace $refDocPlace
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefDocPlace $refDocPlace)
    {
        $this->authorize('create', EmployeeDocument::class);

        $validated = $request->validate([
            'employee_id' => ['required', 'exists:employees,id'],
            'ref_doc_type_id' => ['required', 'exists:ref_doc_types,id'],
            'doc_seria' => ['nullable', 'max:255', 'string'],
            'doc_num' => ['required', 'max:255', 'string'],
            'doc_date' => ['required', 'date'],
        ]);

        $employeeDocument = $refDocPlace
            ->employeeDocuments()
            ->create($validated);

        return new EmployeeDocumentResource($employeeDocument);
    }
}
