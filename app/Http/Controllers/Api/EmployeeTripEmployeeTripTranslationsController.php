<?php

namespace App\Http\Controllers\Api;

use App\Models\EmployeeTrip;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeTripTranslationResource;
use App\Http\Resources\EmployeeTripTranslationCollection;

class EmployeeTripEmployeeTripTranslationsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeTrip $employeeTrip
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, EmployeeTrip $employeeTrip)
    {
        $this->authorize('view', $employeeTrip);

        $search = $request->get('search', '');

        $employeeTripTranslations = $employeeTrip
            ->employeeTripTranslations()
            ->search($search)->paginate();

        return new EmployeeTripTranslationCollection($employeeTripTranslations);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeTrip $employeeTrip
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, EmployeeTrip $employeeTrip)
    {
        $this->authorize('create', EmployeeTripTranslation::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'aim' => ['required', 'max:255', 'string'],
            'final_distantion' => ['required', 'max:255', 'string'],
        ]);

        $employeeTripTranslation = $employeeTrip
            ->employeeTripTranslations()
            ->create($validated);

        return new EmployeeTripTranslationResource($employeeTripTranslation);
    }
}
