<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefTechnicType;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeTechicResource;
use App\Http\Resources\EmployeeTechicCollection;

class RefTechnicTypeEmployeeTechicsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTechnicType $refTechnicType
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefTechnicType $refTechnicType)
    {
        $this->authorize('view', $refTechnicType);

        $search = $request->get('search', '');

        $employeeTechics = $refTechnicType
            ->employeeTechics()
            ->search($search)->paginate();

        return new EmployeeTechicCollection($employeeTechics);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTechnicType $refTechnicType
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefTechnicType $refTechnicType)
    {
        $this->authorize('create', EmployeeTechic::class);

        $validated = $request->validate([
            'employee_id' => ['required', 'exists:employees,id'],
            'invent_num' => ['nullable', 'max:255', 'string'],
            'price' => ['nullable', 'numeric'],
        ]);

        $employeeTechic = $refTechnicType
            ->employeeTechics()
            ->create($validated);

        return new EmployeeTechicResource($employeeTechic);
    }
}
