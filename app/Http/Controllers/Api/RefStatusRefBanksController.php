<?php

namespace App\Http\Controllers\Api;

use App\Models\RefStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefBankResource;
use App\Http\Resources\RefBankCollection;

class RefStatusRefBanksController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefStatus $refStatus
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefStatus $refStatus)
    {
        $this->authorize('view', $refStatus);

        $search = $request->get('search', '');

        $refBanks = $refStatus
            ->refBanks()
            ->search($search)->paginate();

        return new RefBankCollection($refBanks);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefStatus $refStatus
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefStatus $refStatus)
    {
        $this->authorize('create', RefBank::class);

        $validated = $request->validate([
            'mfo' => ['nullable', 'max:255', 'string'],
            'mfo_head' => ['nullable', 'max:255', 'string'],
            'mfo_rkc' => ['nullable', 'max:255', 'string'],
            'kor_account' => ['nullable', 'max:255', 'string'],
            'commis' => ['required', 'numeric'],
            'bin' => ['nullable', 'max:255', 'string'],
            'bik_old' => ['nullable', 'max:255', 'string'],
        ]);

        $refBank = $refStatus->refBanks()->create($validated);

        return new RefBankResource($refBank);
    }
}
