<?php

namespace App\Http\Controllers\Api;

use App\Models\RefTypePhone;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefTypePhoneResource;
use App\Http\Resources\RefTypePhoneCollection;
use App\Http\Requests\RefTypePhoneStoreRequest;
use App\Http\Requests\RefTypePhoneUpdateRequest;

class RefTypePhoneController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefTypePhone::class);

        $search = $request->get('search', '');

        $refTypePhones = RefTypePhone::search($search)->paginate();

        return new RefTypePhoneCollection($refTypePhones);
    }

    /**
     * @param \App\Http\Requests\RefTypePhoneStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefTypePhoneStoreRequest $request)
    {
        $this->authorize('create', RefTypePhone::class);

        $validated = $request->validated();

        $refTypePhone = RefTypePhone::create($validated);

        return new RefTypePhoneResource($refTypePhone);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTypePhone $refTypePhone
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefTypePhone $refTypePhone)
    {
        $this->authorize('view', $refTypePhone);

        return new RefTypePhoneResource($refTypePhone);
    }

    /**
     * @param \App\Http\Requests\RefTypePhoneUpdateRequest $request
     * @param \App\Models\RefTypePhone $refTypePhone
     * @return \Illuminate\Http\Response
     */
    public function update(
        RefTypePhoneUpdateRequest $request,
        RefTypePhone $refTypePhone
    ) {
        $this->authorize('update', $refTypePhone);

        $validated = $request->validated();

        $refTypePhone->update($validated);

        return new RefTypePhoneResource($refTypePhone);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTypePhone $refTypePhone
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, RefTypePhone $refTypePhone)
    {
        $this->authorize('delete', $refTypePhone);

        $refTypePhone->delete();

        return response()->noContent();
    }
}
