<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefVidHolidayType;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefVidHolidayResource;
use App\Http\Resources\RefVidHolidayCollection;

class RefVidHolidayTypeRefVidHolidaysController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefVidHolidayType $refVidHolidayType
     * @return \Illuminate\Http\Response
     */
    public function index(
        Request $request,
        RefVidHolidayType $refVidHolidayType
    ) {
        $this->authorize('view', $refVidHolidayType);

        $search = $request->get('search', '');

        $refVidHolidays = $refVidHolidayType
            ->refVidHolidays()
            ->search($search)->paginate();

        return new RefVidHolidayCollection($refVidHolidays);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefVidHolidayType $refVidHolidayType
     * @return \Illuminate\Http\Response
     */
    public function store(
        Request $request,
        RefVidHolidayType $refVidHolidayType
    ) {
        $this->authorize('create', RefVidHoliday::class);

        $validated = $request->validate([]);

        $refVidHoliday = $refVidHolidayType
            ->refVidHolidays()
            ->create($validated);

        return new RefVidHolidayResource($refVidHoliday);
    }
}
