<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefTransportTrip;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeTripFromToResource;
use App\Http\Resources\EmployeeTripFromToCollection;

class RefTransportTripEmployeeTripFromTosController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTransportTrip $refTransportTrip
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefTransportTrip $refTransportTrip)
    {
        $this->authorize('view', $refTransportTrip);

        $search = $request->get('search', '');

        $employeeTripFromTos = $refTransportTrip
            ->employeeTripFromTos()
            ->search($search)->paginate();

        return new EmployeeTripFromToCollection($employeeTripFromTos);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTransportTrip $refTransportTrip
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefTransportTrip $refTransportTrip)
    {
        $this->authorize('create', EmployeeTripFromTo::class);

        $validated = $request->validate([
            'employee_trip_id' => ['required', 'exists:employee_trips,id'],
            'from_place' => ['required', 'max:255', 'string'],
            'to_place' => ['required', 'max:255', 'string'],
        ]);

        $employeeTripFromTo = $refTransportTrip
            ->employeeTripFromTos()
            ->create($validated);

        return new EmployeeTripFromToResource($employeeTripFromTo);
    }
}
