<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefFamilyState;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefFamilyStateTranslationResource;
use App\Http\Resources\RefFamilyStateTranslationCollection;

class RefFamilyStateRefFamilyStateTranslationsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefFamilyState $refFamilyState
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefFamilyState $refFamilyState)
    {
        $this->authorize('view', $refFamilyState);

        $search = $request->get('search', '');

        $refFamilyStateTranslations = $refFamilyState
            ->refFamilyStateTranslations()
            ->search($search)->paginate();

        return new RefFamilyStateTranslationCollection(
            $refFamilyStateTranslations
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefFamilyState $refFamilyState
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefFamilyState $refFamilyState)
    {
        $this->authorize('create', RefFamilyStateTranslation::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'name' => ['required', 'max:255', 'string'],
        ]);

        $refFamilyStateTranslation = $refFamilyState
            ->refFamilyStateTranslations()
            ->create($validated);

        return new RefFamilyStateTranslationResource(
            $refFamilyStateTranslation
        );
    }
}
