<?php

namespace App\Http\Controllers\Api;

use App\Models\Curator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CuratorResource;
use App\Http\Resources\CuratorCollection;
use App\Http\Requests\CuratorStoreRequest;
use App\Http\Requests\CuratorUpdateRequest;

class CuratorController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', Curator::class);

        $search = $request->get('search', '');

        $curators = Curator::search($search)->paginate();

        return new CuratorCollection($curators);
    }

    /**
     * @param \App\Http\Requests\CuratorStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CuratorStoreRequest $request)
    {
        $this->authorize('create', Curator::class);

        $validated = $request->validated();

        $curator = Curator::create($validated);

        return new CuratorResource($curator);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Curator $curator
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Curator $curator)
    {
        $this->authorize('view', $curator);

        return new CuratorResource($curator);
    }

    /**
     * @param \App\Http\Requests\CuratorUpdateRequest $request
     * @param \App\Models\Curator $curator
     * @return \Illuminate\Http\Response
     */
    public function update(CuratorUpdateRequest $request, Curator $curator)
    {
        $this->authorize('update', $curator);

        $validated = $request->validated();

        $curator->update($validated);

        return new CuratorResource($curator);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Curator $curator
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Curator $curator)
    {
        $this->authorize('delete', $curator);

        $curator->delete();

        return response()->noContent();
    }
}
