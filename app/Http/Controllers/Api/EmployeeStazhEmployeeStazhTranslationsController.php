<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\EmployeeStazh;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeStazhTranslationResource;
use App\Http\Resources\EmployeeStazhTranslationCollection;

class EmployeeStazhEmployeeStazhTranslationsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeStazh $employeeStazh
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, EmployeeStazh $employeeStazh)
    {
        $this->authorize('view', $employeeStazh);

        $search = $request->get('search', '');

        $employeeStazhTranslations = $employeeStazh
            ->employeeStazhTranslations()
            ->search($search)->paginate();

        return new EmployeeStazhTranslationCollection(
            $employeeStazhTranslations
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeStazh $employeeStazh
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, EmployeeStazh $employeeStazh)
    {
        $this->authorize('create', EmployeeStazhTranslation::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'organization' => ['required', 'max:255', 'string'],
            'dolgnost' => ['required', 'max:255', 'string'],
            'address' => ['required', 'max:255', 'string'],
        ]);

        $employeeStazhTranslation = $employeeStazh
            ->employeeStazhTranslations()
            ->create($validated);

        return new EmployeeStazhTranslationResource($employeeStazhTranslation);
    }
}
