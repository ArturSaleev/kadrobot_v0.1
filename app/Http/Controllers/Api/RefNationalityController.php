<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefNationality;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefNationalityResource;
use App\Http\Resources\RefNationalityCollection;
use App\Http\Requests\RefNationalityStoreRequest;
use App\Http\Requests\RefNationalityUpdateRequest;

class RefNationalityController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefNationality::class);

        $search = $request->get('search', '');

        $refNationalities = RefNationality::search($search)->paginate();

        return new RefNationalityCollection($refNationalities);
    }

    /**
     * @param \App\Http\Requests\RefNationalityStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefNationalityStoreRequest $request)
    {
        $this->authorize('create', RefNationality::class);

        $validated = $request->validated();

        $refNationality = RefNationality::create($validated);

        return new RefNationalityResource($refNationality);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefNationality $refNationality
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefNationality $refNationality)
    {
        $this->authorize('view', $refNationality);

        return new RefNationalityResource($refNationality);
    }

    /**
     * @param \App\Http\Requests\RefNationalityUpdateRequest $request
     * @param \App\Models\RefNationality $refNationality
     * @return \Illuminate\Http\Response
     */
    public function update(
        RefNationalityUpdateRequest $request,
        RefNationality $refNationality
    ) {
        $this->authorize('update', $refNationality);

        $validated = $request->validated();

        $refNationality->update($validated);

        return new RefNationalityResource($refNationality);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefNationality $refNationality
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, RefNationality $refNationality)
    {
        $this->authorize('delete', $refNationality);

        $refNationality->delete();

        return response()->noContent();
    }
}
