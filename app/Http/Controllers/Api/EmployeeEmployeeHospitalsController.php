<?php

namespace App\Http\Controllers\Api;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeHospitalResource;
use App\Http\Resources\EmployeeHospitalCollection;

class EmployeeEmployeeHospitalsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Employee $employee)
    {
        $this->authorize('view', $employee);

        $search = $request->get('search', '');

        $employeeHospitals = $employee
            ->employeeHospitals()
            ->search($search)->paginate();

        return new EmployeeHospitalCollection($employeeHospitals);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Employee $employee)
    {
        $this->authorize('create', EmployeeHospital::class);

        $validated = $request->validate([
            'date_begin' => ['required', 'date'],
            'date_end' => ['required', 'date'],
            'cnt_days' => ['required', 'numeric'],
        ]);

        $employeeHospital = $employee->employeeHospitals()->create($validated);

        return new EmployeeHospitalResource($employeeHospital);
    }
}
