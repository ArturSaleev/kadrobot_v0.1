<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefTypeRodstv;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefTypeRodstvResource;
use App\Http\Resources\RefTypeRodstvCollection;
use App\Http\Requests\RefTypeRodstvStoreRequest;
use App\Http\Requests\RefTypeRodstvUpdateRequest;

class RefTypeRodstvController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefTypeRodstv::class);

        $search = $request->get('search', '');

        $refTypeRodstvs = RefTypeRodstv::search($search)->paginate();

        return new RefTypeRodstvCollection($refTypeRodstvs);
    }

    /**
     * @param \App\Http\Requests\RefTypeRodstvStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefTypeRodstvStoreRequest $request)
    {
        $this->authorize('create', RefTypeRodstv::class);

        $validated = $request->validated();

        $refTypeRodstv = RefTypeRodstv::create($validated);

        return new RefTypeRodstvResource($refTypeRodstv);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTypeRodstv $refTypeRodstv
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefTypeRodstv $refTypeRodstv)
    {
        $this->authorize('view', $refTypeRodstv);

        return new RefTypeRodstvResource($refTypeRodstv);
    }

    /**
     * @param \App\Http\Requests\RefTypeRodstvUpdateRequest $request
     * @param \App\Models\RefTypeRodstv $refTypeRodstv
     * @return \Illuminate\Http\Response
     */
    public function update(
        RefTypeRodstvUpdateRequest $request,
        RefTypeRodstv $refTypeRodstv
    ) {
        $this->authorize('update', $refTypeRodstv);

        $validated = $request->validated();

        $refTypeRodstv->update($validated);

        return new RefTypeRodstvResource($refTypeRodstv);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTypeRodstv $refTypeRodstv
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, RefTypeRodstv $refTypeRodstv)
    {
        $this->authorize('delete', $refTypeRodstv);

        $refTypeRodstv->delete();

        return response()->noContent();
    }
}
