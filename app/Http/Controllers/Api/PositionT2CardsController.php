<?php

namespace App\Http\Controllers\Api;

use App\Models\Position;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\T2CardResource;
use App\Http\Resources\T2CardCollection;

class PositionT2CardsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Position $position
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Position $position)
    {
        $this->authorize('view', $position);

        $search = $request->get('search', '');

        $t2Cards = $position
            ->t2Cards()
            ->search($search)->paginate();

        return new T2CardCollection($t2Cards);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Position $position
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Position $position)
    {
        $this->authorize('create', T2Card::class);

        $validated = $request->validate([
            'company_id' => ['required', 'exists:companies,id'],
            'employee_id' => ['required', 'exists:employees,id'],
            'ref_branch_id' => ['required', 'exists:branches,id'],
            'ref_action_id' => ['required', 'exists:ref_actions,id'],
        ]);

        $t2Card = $position->t2Cards()->create($validated);

        return new T2CardResource($t2Card);
    }
}
