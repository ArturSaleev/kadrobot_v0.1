<?php

namespace App\Http\Controllers\Api;

use App\Models\RefStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyResource;
use App\Http\Resources\CompanyCollection;

class RefStatusCompaniesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefStatus $refStatus
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefStatus $refStatus)
    {
        $this->authorize('view', $refStatus);

        $search = $request->get('search', '');

        $companies = $refStatus
            ->companies()
            ->search($search)->paginate();

        return new CompanyCollection($companies);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefStatus $refStatus
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefStatus $refStatus)
    {
        $this->authorize('create', Company::class);

        $validated = $request->validate([
            'bin' => ['required', 'max:255', 'string'],
            'rnn' => ['required', 'max:255', 'string'],
            'name' => ['required'],
        ]);

        $company = $refStatus->companies()->create($validated);

        return new CompanyResource($company);
    }
}
