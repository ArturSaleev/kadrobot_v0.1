<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefAddressType;
use App\Http\Controllers\Controller;
use App\Http\Resources\BranchAddresesResource;
use App\Http\Resources\BranchAddresesCollection;

class RefAddressTypeAllBranchAddresesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefAddressType $refAddressType
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefAddressType $refAddressType)
    {
        $this->authorize('view', $refAddressType);

        $search = $request->get('search', '');

        $allBranchAddreses = $refAddressType
            ->allCompanyAddreses()
            ->search($search)->paginate();

        return new BranchAddresesCollection($allBranchAddreses);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefAddressType $refAddressType
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefAddressType $refAddressType)
    {
        $this->authorize('create', BranchAddreses::class);

        $validated = $request->validate([
            'branch_id' => ['required', 'exists:branches,id'],
        ]);

        $branchAddreses = $refAddressType
            ->allCompanyAddreses()
            ->create($validated);

        return new BranchAddresesResource($branchAddreses);
    }
}
