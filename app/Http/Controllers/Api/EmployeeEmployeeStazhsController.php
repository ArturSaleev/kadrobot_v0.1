<?php

namespace App\Http\Controllers\Api;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeStazhResource;
use App\Http\Resources\EmployeeStazhCollection;

class EmployeeEmployeeStazhsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Employee $employee)
    {
        $this->authorize('view', $employee);

        $search = $request->get('search', '');

        $employeeStazhs = $employee
            ->employeeStazhs()
            ->search($search)->paginate();

        return new EmployeeStazhCollection($employeeStazhs);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Employee $employee)
    {
        $this->authorize('create', EmployeeStazh::class);

        $validated = $request->validate([
            'date_begin' => ['required', 'date'],
            'date_end' => ['required', 'date'],
            'cnt_mes' => ['required', 'numeric'],
        ]);

        $employeeStazh = $employee->employeeStazhs()->create($validated);

        return new EmployeeStazhResource($employeeStazh);
    }
}
