<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\EmployeeDocument;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeDocumentResource;
use App\Http\Resources\EmployeeDocumentCollection;
use App\Http\Requests\EmployeeDocumentStoreRequest;
use App\Http\Requests\EmployeeDocumentUpdateRequest;

class EmployeeDocumentController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeeDocument::class);

        $search = $request->get('search', '');

        $employeeDocuments = EmployeeDocument::search($search)->paginate();

        return new EmployeeDocumentCollection($employeeDocuments);
    }

    /**
     * @param \App\Http\Requests\EmployeeDocumentStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeDocumentStoreRequest $request)
    {
        $this->authorize('create', EmployeeDocument::class);

        $validated = $request->validated();

        $employeeDocument = EmployeeDocument::create($validated);

        return new EmployeeDocumentResource($employeeDocument);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeDocument $employeeDocument
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EmployeeDocument $employeeDocument)
    {
        $this->authorize('view', $employeeDocument);

        return new EmployeeDocumentResource($employeeDocument);
    }

    /**
     * @param \App\Http\Requests\EmployeeDocumentUpdateRequest $request
     * @param \App\Models\EmployeeDocument $employeeDocument
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeeDocumentUpdateRequest $request,
        EmployeeDocument $employeeDocument
    ) {
        $this->authorize('update', $employeeDocument);

        $validated = $request->validated();

        $employeeDocument->update($validated);

        return new EmployeeDocumentResource($employeeDocument);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeDocument $employeeDocument
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        Request $request,
        EmployeeDocument $employeeDocument
    ) {
        $this->authorize('delete', $employeeDocument);

        $employeeDocument->delete();

        return response()->noContent();
    }
}
