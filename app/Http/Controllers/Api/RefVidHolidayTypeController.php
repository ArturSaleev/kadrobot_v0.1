<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefVidHolidayType;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefVidHolidayTypeResource;
use App\Http\Resources\RefVidHolidayTypeCollection;
use App\Http\Requests\RefVidHolidayTypeStoreRequest;
use App\Http\Requests\RefVidHolidayTypeUpdateRequest;

class RefVidHolidayTypeController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefVidHolidayType::class);

        $search = $request->get('search', '');

        $refVidHolidayTypes = RefVidHolidayType::search($search)->paginate();

        return new RefVidHolidayTypeCollection($refVidHolidayTypes);
    }

    /**
     * @param \App\Http\Requests\RefVidHolidayTypeStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefVidHolidayTypeStoreRequest $request)
    {
        $this->authorize('create', RefVidHolidayType::class);

        $validated = $request->validated();

        $refVidHolidayType = RefVidHolidayType::create($validated);

        return new RefVidHolidayTypeResource($refVidHolidayType);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefVidHolidayType $refVidHolidayType
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefVidHolidayType $refVidHolidayType)
    {
        $this->authorize('view', $refVidHolidayType);

        return new RefVidHolidayTypeResource($refVidHolidayType);
    }

    /**
     * @param \App\Http\Requests\RefVidHolidayTypeUpdateRequest $request
     * @param \App\Models\RefVidHolidayType $refVidHolidayType
     * @return \Illuminate\Http\Response
     */
    public function update(
        RefVidHolidayTypeUpdateRequest $request,
        RefVidHolidayType $refVidHolidayType
    ) {
        $this->authorize('update', $refVidHolidayType);

        $validated = $request->validated();

        $refVidHolidayType->update($validated);

        return new RefVidHolidayTypeResource($refVidHolidayType);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefVidHolidayType $refVidHolidayType
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        Request $request,
        RefVidHolidayType $refVidHolidayType
    ) {
        $this->authorize('delete', $refVidHolidayType);

        $refVidHolidayType->delete();

        return response()->noContent();
    }
}
