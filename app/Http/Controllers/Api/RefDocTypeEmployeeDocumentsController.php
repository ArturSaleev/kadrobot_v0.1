<?php

namespace App\Http\Controllers\Api;

use App\Models\RefDocType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeDocumentResource;
use App\Http\Resources\EmployeeDocumentCollection;

class RefDocTypeEmployeeDocumentsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefDocType $refDocType
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefDocType $refDocType)
    {
        $this->authorize('view', $refDocType);

        $search = $request->get('search', '');

        $employeeDocuments = $refDocType
            ->employeeDocuments()
            ->search($search)->paginate();

        return new EmployeeDocumentCollection($employeeDocuments);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefDocType $refDocType
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefDocType $refDocType)
    {
        $this->authorize('create', EmployeeDocument::class);

        $validated = $request->validate([
            'employee_id' => ['required', 'exists:employees,id'],
            'ref_doc_place_id' => ['required', 'exists:ref_doc_places,id'],
            'doc_seria' => ['nullable', 'max:255', 'string'],
            'doc_num' => ['required', 'max:255', 'string'],
            'doc_date' => ['required', 'date'],
        ]);

        $employeeDocument = $refDocType
            ->employeeDocuments()
            ->create($validated);

        return new EmployeeDocumentResource($employeeDocument);
    }
}
