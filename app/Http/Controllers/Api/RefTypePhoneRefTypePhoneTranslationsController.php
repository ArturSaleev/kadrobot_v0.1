<?php

namespace App\Http\Controllers\Api;

use App\Models\RefTypePhone;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefTypePhoneTranslationResource;
use App\Http\Resources\RefTypePhoneTranslationCollection;

class RefTypePhoneRefTypePhoneTranslationsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTypePhone $refTypePhone
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefTypePhone $refTypePhone)
    {
        $this->authorize('view', $refTypePhone);

        $search = $request->get('search', '');

        $refTypePhoneTranslations = $refTypePhone
            ->refTypePhoneTranslations()
            ->search($search)->paginate();

        return new RefTypePhoneTranslationCollection($refTypePhoneTranslations);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTypePhone $refTypePhone
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefTypePhone $refTypePhone)
    {
        $this->authorize('create', RefTypePhoneTranslation::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'name' => ['required', 'max:255', 'string'],
        ]);

        $refTypePhoneTranslation = $refTypePhone
            ->refTypePhoneTranslations()
            ->create($validated);

        return new RefTypePhoneTranslationResource($refTypePhoneTranslation);
    }
}
