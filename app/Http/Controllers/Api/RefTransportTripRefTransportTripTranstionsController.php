<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefTransportTrip;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefTransportTripTranstionResource;
use App\Http\Resources\RefTransportTripTranstionCollection;

class RefTransportTripRefTransportTripTranstionsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTransportTrip $refTransportTrip
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefTransportTrip $refTransportTrip)
    {
        $this->authorize('view', $refTransportTrip);

        $search = $request->get('search', '');

        $refTransportTripTranstions = $refTransportTrip
            ->refTransportTripTranstions()
            ->search($search)->paginate();

        return new RefTransportTripTranstionCollection(
            $refTransportTripTranstions
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTransportTrip $refTransportTrip
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefTransportTrip $refTransportTrip)
    {
        $this->authorize('create', RefTransportTripTranstion::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'name' => ['required', 'max:255', 'string'],
        ]);

        $refTransportTripTranstion = $refTransportTrip
            ->refTransportTripTranstions()
            ->create($validated);

        return new RefTransportTripTranstionResource(
            $refTransportTripTranstion
        );
    }
}
