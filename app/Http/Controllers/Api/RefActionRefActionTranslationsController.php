<?php

namespace App\Http\Controllers\Api;

use App\Models\RefAction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefActionTranslationResource;
use App\Http\Resources\RefActionTranslationCollection;

class RefActionRefActionTranslationsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefAction $refAction
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefAction $refAction)
    {
        $this->authorize('view', $refAction);

        $search = $request->get('search', '');

        $refActionTranslations = $refAction
            ->refActionTranslations()
            ->search($search)->paginate();

        return new RefActionTranslationCollection($refActionTranslations);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefAction $refAction
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefAction $refAction)
    {
        $this->authorize('create', RefActionTranslation::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'name' => ['required', 'max:255', 'string'],
        ]);

        $refActionTranslation = $refAction
            ->refActionTranslations()
            ->create($validated);

        return new RefActionTranslationResource($refActionTranslation);
    }
}
