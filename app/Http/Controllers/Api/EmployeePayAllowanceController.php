<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\EmployeePayAllowance;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeePayAllowanceResource;
use App\Http\Resources\EmployeePayAllowanceCollection;
use App\Http\Requests\EmployeePayAllowanceStoreRequest;
use App\Http\Requests\EmployeePayAllowanceUpdateRequest;

class EmployeePayAllowanceController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeePayAllowance::class);

        $search = $request->get('search', '');

        $employeePayAllowances = EmployeePayAllowance::search($search)->paginate();

        return new EmployeePayAllowanceCollection($employeePayAllowances);
    }

    /**
     * @param \App\Http\Requests\EmployeePayAllowanceStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeePayAllowanceStoreRequest $request)
    {
        $this->authorize('create', EmployeePayAllowance::class);

        $validated = $request->validated();

        $employeePayAllowance = EmployeePayAllowance::create($validated);

        return new EmployeePayAllowanceResource($employeePayAllowance);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeePayAllowance $employeePayAllowance
     * @return \Illuminate\Http\Response
     */
    public function show(
        Request $request,
        EmployeePayAllowance $employeePayAllowance
    ) {
        $this->authorize('view', $employeePayAllowance);

        return new EmployeePayAllowanceResource($employeePayAllowance);
    }

    /**
     * @param \App\Http\Requests\EmployeePayAllowanceUpdateRequest $request
     * @param \App\Models\EmployeePayAllowance $employeePayAllowance
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeePayAllowanceUpdateRequest $request,
        EmployeePayAllowance $employeePayAllowance
    ) {
        $this->authorize('update', $employeePayAllowance);

        $validated = $request->validated();

        $employeePayAllowance->update($validated);

        return new EmployeePayAllowanceResource($employeePayAllowance);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeePayAllowance $employeePayAllowance
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        Request $request,
        EmployeePayAllowance $employeePayAllowance
    ) {
        $this->authorize('delete', $employeePayAllowance);

        $employeePayAllowance->delete();

        return response()->noContent();
    }
}
