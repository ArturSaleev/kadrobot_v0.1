<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefAddressType;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefAddressTypeTranslationResource;
use App\Http\Resources\RefAddressTypeTranslationCollection;

class RefAddressTypeRefAddressTypeTranslationsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefAddressType $refAddressType
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefAddressType $refAddressType)
    {
        $this->authorize('view', $refAddressType);

        $search = $request->get('search', '');

        $refAddressTypeTranslations = $refAddressType
            ->refAddressTypeTranslations()
            ->search($search)->paginate();

        return new RefAddressTypeTranslationCollection(
            $refAddressTypeTranslations
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefAddressType $refAddressType
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefAddressType $refAddressType)
    {
        $this->authorize('create', RefAddressTypeTranslation::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'name' => ['required', 'max:255', 'string'],
        ]);

        $refAddressTypeTranslation = $refAddressType
            ->refAddressTypeTranslations()
            ->create($validated);

        return new RefAddressTypeTranslationResource(
            $refAddressTypeTranslation
        );
    }
}
