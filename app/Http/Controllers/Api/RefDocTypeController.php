<?php

namespace App\Http\Controllers\Api;

use App\Models\RefDocType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefDocTypeResource;
use App\Http\Resources\RefDocTypeCollection;
use App\Http\Requests\RefDocTypeStoreRequest;
use App\Http\Requests\RefDocTypeUpdateRequest;

class RefDocTypeController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefDocType::class);

        $search = $request->get('search', '');

        $refDocTypes = RefDocType::search($search)->paginate();

        return new RefDocTypeCollection($refDocTypes);
    }

    /**
     * @param \App\Http\Requests\RefDocTypeStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefDocTypeStoreRequest $request)
    {
        $this->authorize('create', RefDocType::class);

        $validated = $request->validated();

        $refDocType = RefDocType::create($validated);

        return new RefDocTypeResource($refDocType);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefDocType $refDocType
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefDocType $refDocType)
    {
        $this->authorize('view', $refDocType);

        return new RefDocTypeResource($refDocType);
    }

    /**
     * @param \App\Http\Requests\RefDocTypeUpdateRequest $request
     * @param \App\Models\RefDocType $refDocType
     * @return \Illuminate\Http\Response
     */
    public function update(
        RefDocTypeUpdateRequest $request,
        RefDocType $refDocType
    ) {
        $this->authorize('update', $refDocType);

        $validated = $request->validated();

        $refDocType->update($validated);

        return new RefDocTypeResource($refDocType);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefDocType $refDocType
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, RefDocType $refDocType)
    {
        $this->authorize('delete', $refDocType);

        $refDocType->delete();

        return response()->noContent();
    }
}
