<?php

namespace App\Http\Controllers\Api;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeAddressResource;
use App\Http\Resources\EmployeeAddressCollection;

class EmployeeEmployeeAddressesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Employee $employee)
    {
        $this->authorize('view', $employee);

        $search = $request->get('search', '');

        $employeeAddresses = $employee
            ->employeeAddresses()
            ->search($search)->paginate();

        return new EmployeeAddressCollection($employeeAddresses);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Employee $employee)
    {
        $this->authorize('create', EmployeeAddress::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'ref_address_type_id' => [
                'required',
                'exists:ref_address_types,id',
            ],
        ]);

        $employeeAddress = $employee->employeeAddresses()->create($validated);

        return new EmployeeAddressResource($employeeAddress);
    }
}
