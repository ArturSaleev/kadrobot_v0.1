<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefAllowanceType;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefAllowanceTypeTranslationResource;
use App\Http\Resources\RefAllowanceTypeTranslationCollection;

class RefAllowanceTypeRefAllowanceTypeTranslationsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefAllowanceType $refAllowanceType
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefAllowanceType $refAllowanceType)
    {
        $this->authorize('view', $refAllowanceType);

        $search = $request->get('search', '');

        $refAllowanceTypeTranslations = $refAllowanceType
            ->refAllowanceTypeTranslations()
            ->search($search)->paginate();

        return new RefAllowanceTypeTranslationCollection(
            $refAllowanceTypeTranslations
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefAllowanceType $refAllowanceType
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefAllowanceType $refAllowanceType)
    {
        $this->authorize('create', RefAllowanceTypeTranslation::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'name' => ['required', 'max:255', 'string'],
        ]);

        $refAllowanceTypeTranslation = $refAllowanceType
            ->refAllowanceTypeTranslations()
            ->create($validated);

        return new RefAllowanceTypeTranslationResource(
            $refAllowanceTypeTranslation
        );
    }
}
