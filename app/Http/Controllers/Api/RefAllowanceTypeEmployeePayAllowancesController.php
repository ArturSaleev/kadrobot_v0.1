<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefAllowanceType;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeePayAllowanceResource;
use App\Http\Resources\EmployeePayAllowanceCollection;

class RefAllowanceTypeEmployeePayAllowancesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefAllowanceType $refAllowanceType
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefAllowanceType $refAllowanceType)
    {
        $this->authorize('view', $refAllowanceType);

        $search = $request->get('search', '');

        $employeePayAllowances = $refAllowanceType
            ->employeePayAllowances()
            ->search($search)->paginate();

        return new EmployeePayAllowanceCollection($employeePayAllowances);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefAllowanceType $refAllowanceType
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefAllowanceType $refAllowanceType)
    {
        $this->authorize('create', EmployeePayAllowance::class);

        $validated = $request->validate([
            'employee_id' => ['required', 'exists:employees,id'],
            'date_add' => ['required', 'date'],
            'period_begin' => ['required', 'date'],
            'period_end' => ['required', 'date'],
            'paysum' => ['required', 'numeric'],
        ]);

        $employeePayAllowance = $refAllowanceType
            ->employeePayAllowances()
            ->create($validated);

        return new EmployeePayAllowanceResource($employeePayAllowance);
    }
}
