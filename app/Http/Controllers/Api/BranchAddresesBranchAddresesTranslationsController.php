<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\BranchAddreses;
use App\Http\Controllers\Controller;
use App\Http\Resources\BranchAddresesTranslationResource;
use App\Http\Resources\BranchAddresesTranslationCollection;

class BranchAddresesBranchAddresesTranslationsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\BranchAddreses $branchAddreses
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, BranchAddreses $branchAddreses)
    {
        $this->authorize('view', $branchAddreses);

        $search = $request->get('search', '');

        $branchAddresesTranslations = $branchAddreses
            ->branchAddresesTranslations()
            ->search($search)->paginate();

        return new BranchAddresesTranslationCollection(
            $branchAddresesTranslations
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\BranchAddreses $branchAddreses
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, BranchAddreses $branchAddreses)
    {
        $this->authorize('create', BranchAddresesTranslation::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'name' => ['required', 'max:255', 'string'],
        ]);

        $branchAddresesTranslation = $branchAddreses
            ->branchAddresesTranslations()
            ->create($validated);

        return new BranchAddresesTranslationResource(
            $branchAddresesTranslation
        );
    }
}
