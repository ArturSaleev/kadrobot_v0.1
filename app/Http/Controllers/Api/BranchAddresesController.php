<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\BranchAddreses;
use App\Http\Controllers\Controller;
use App\Http\Resources\BranchAddresesResource;
use App\Http\Resources\BranchAddresesCollection;
use App\Http\Requests\BranchAddresesStoreRequest;
use App\Http\Requests\BranchAddresesUpdateRequest;

class BranchAddresesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', BranchAddreses::class);

        $search = $request->get('search', '');

        $allBranchAddreses = BranchAddreses::search($search)->paginate();

        return new BranchAddresesCollection($allBranchAddreses);
    }

    /**
     * @param \App\Http\Requests\BranchAddresesStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(BranchAddresesStoreRequest $request)
    {
        $this->authorize('create', BranchAddreses::class);

        $validated = $request->validated();

        $branchAddreses = BranchAddreses::create($validated);

        return new BranchAddresesResource($branchAddreses);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\BranchAddreses $branchAddreses
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, BranchAddreses $branchAddreses)
    {
        $this->authorize('view', $branchAddreses);

        return new BranchAddresesResource($branchAddreses);
    }

    /**
     * @param \App\Http\Requests\BranchAddresesUpdateRequest $request
     * @param \App\Models\BranchAddreses $branchAddreses
     * @return \Illuminate\Http\Response
     */
    public function update(
        BranchAddresesUpdateRequest $request,
        BranchAddreses $branchAddreses
    ) {
        $this->authorize('update', $branchAddreses);

        $validated = $request->validated();

        $branchAddreses->update($validated);

        return new BranchAddresesResource($branchAddreses);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\BranchAddreses $branchAddreses
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, BranchAddreses $branchAddreses)
    {
        $this->authorize('delete', $branchAddreses);

        $branchAddreses->delete();

        return response()->noContent();
    }
}
