<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefAccountType;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefAccountTypeTranslationResource;
use App\Http\Resources\RefAccountTypeTranslationCollection;

class RefAccountTypeRefAccountTypeTranslationsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefAccountType $refAccountType
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefAccountType $refAccountType)
    {
        $this->authorize('view', $refAccountType);

        $search = $request->get('search', '');

        $refAccountTypeTranslations = $refAccountType
            ->refAccountTypeTranslations()
            ->search($search)->paginate();

        return new RefAccountTypeTranslationCollection(
            $refAccountTypeTranslations
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefAccountType $refAccountType
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefAccountType $refAccountType)
    {
        $this->authorize('create', RefAccountTypeTranslation::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'name' => ['required', 'max:255', 'string'],
        ]);

        $refAccountTypeTranslation = $refAccountType
            ->refAccountTypeTranslations()
            ->create($validated);

        return new RefAccountTypeTranslationResource(
            $refAccountTypeTranslation
        );
    }
}
