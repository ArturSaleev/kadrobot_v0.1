<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefKindHoliday;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefKindHolidayResource;
use App\Http\Resources\RefKindHolidayCollection;
use App\Http\Requests\RefKindHolidayStoreRequest;
use App\Http\Requests\RefKindHolidayUpdateRequest;

class RefKindHolidayController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefKindHoliday::class);

        $search = $request->get('search', '');

        $refKindHolidays = RefKindHoliday::search($search)->paginate();

        return new RefKindHolidayCollection($refKindHolidays);
    }

    /**
     * @param \App\Http\Requests\RefKindHolidayStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefKindHolidayStoreRequest $request)
    {
        $this->authorize('create', RefKindHoliday::class);

        $validated = $request->validated();

        $refKindHoliday = RefKindHoliday::create($validated);

        return new RefKindHolidayResource($refKindHoliday);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefKindHoliday $refKindHoliday
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefKindHoliday $refKindHoliday)
    {
        $this->authorize('view', $refKindHoliday);

        return new RefKindHolidayResource($refKindHoliday);
    }

    /**
     * @param \App\Http\Requests\RefKindHolidayUpdateRequest $request
     * @param \App\Models\RefKindHoliday $refKindHoliday
     * @return \Illuminate\Http\Response
     */
    public function update(
        RefKindHolidayUpdateRequest $request,
        RefKindHoliday $refKindHoliday
    ) {
        $this->authorize('update', $refKindHoliday);

        $validated = $request->validated();

        $refKindHoliday->update($validated);

        return new RefKindHolidayResource($refKindHoliday);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefKindHoliday $refKindHoliday
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, RefKindHoliday $refKindHoliday)
    {
        $this->authorize('delete', $refKindHoliday);

        $refKindHoliday->delete();

        return response()->noContent();
    }
}
