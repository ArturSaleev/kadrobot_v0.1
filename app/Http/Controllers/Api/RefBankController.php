<?php

namespace App\Http\Controllers\Api;

use App\Models\RefBank;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefBankResource;
use App\Http\Resources\RefBankCollection;
use App\Http\Requests\RefBankStoreRequest;
use App\Http\Requests\RefBankUpdateRequest;

class RefBankController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefBank::class);

        $search = $request->get('search', '');

        $refBanks = RefBank::search($search)->paginate();

        return new RefBankCollection($refBanks);
    }

    /**
     * @param \App\Http\Requests\RefBankStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefBankStoreRequest $request)
    {
        $this->authorize('create', RefBank::class);

        $validated = $request->validated();

        $refBank = RefBank::create($validated);

        return new RefBankResource($refBank);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefBank $refBank
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefBank $refBank)
    {
        $this->authorize('view', $refBank);

        return new RefBankResource($refBank);
    }

    /**
     * @param \App\Http\Requests\RefBankUpdateRequest $request
     * @param \App\Models\RefBank $refBank
     * @return \Illuminate\Http\Response
     */
    public function update(RefBankUpdateRequest $request, RefBank $refBank)
    {
        $this->authorize('update', $refBank);

        $validated = $request->validated();

        $refBank->update($validated);

        return new RefBankResource($refBank);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefBank $refBank
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, RefBank $refBank)
    {
        $this->authorize('delete', $refBank);

        $refBank->delete();

        return response()->noContent();
    }
}
