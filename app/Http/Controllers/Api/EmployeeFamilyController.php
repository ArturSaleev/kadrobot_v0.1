<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\EmployeeFamily;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeFamilyResource;
use App\Http\Resources\EmployeeFamilyCollection;
use App\Http\Requests\EmployeeFamilyStoreRequest;
use App\Http\Requests\EmployeeFamilyUpdateRequest;

class EmployeeFamilyController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeeFamily::class);

        $search = $request->get('search', '');

        $employeeFamilies = EmployeeFamily::search($search)->paginate();

        return new EmployeeFamilyCollection($employeeFamilies);
    }

    /**
     * @param \App\Http\Requests\EmployeeFamilyStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeFamilyStoreRequest $request)
    {
        $this->authorize('create', EmployeeFamily::class);

        $validated = $request->validated();

        $employeeFamily = EmployeeFamily::create($validated);

        return new EmployeeFamilyResource($employeeFamily);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeFamily $employeeFamily
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EmployeeFamily $employeeFamily)
    {
        $this->authorize('view', $employeeFamily);

        return new EmployeeFamilyResource($employeeFamily);
    }

    /**
     * @param \App\Http\Requests\EmployeeFamilyUpdateRequest $request
     * @param \App\Models\EmployeeFamily $employeeFamily
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeeFamilyUpdateRequest $request,
        EmployeeFamily $employeeFamily
    ) {
        $this->authorize('update', $employeeFamily);

        $validated = $request->validated();

        $employeeFamily->update($validated);

        return new EmployeeFamilyResource($employeeFamily);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeFamily $employeeFamily
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, EmployeeFamily $employeeFamily)
    {
        $this->authorize('delete', $employeeFamily);

        $employeeFamily->delete();

        return response()->noContent();
    }
}
