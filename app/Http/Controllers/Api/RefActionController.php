<?php

namespace App\Http\Controllers\Api;

use App\Models\RefAction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefActionResource;
use App\Http\Resources\RefActionCollection;
use App\Http\Requests\RefActionStoreRequest;
use App\Http\Requests\RefActionUpdateRequest;

class RefActionController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefAction::class);

        $search = $request->get('search', '');

        $refActions = RefAction::search($search)->paginate();

        return new RefActionCollection($refActions);
    }

    /**
     * @param \App\Http\Requests\RefActionStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefActionStoreRequest $request)
    {
        $this->authorize('create', RefAction::class);

        $validated = $request->validated();

        $refAction = RefAction::create($validated);

        return new RefActionResource($refAction);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefAction $refAction
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefAction $refAction)
    {
        $this->authorize('view', $refAction);

        return new RefActionResource($refAction);
    }

    /**
     * @param \App\Http\Requests\RefActionUpdateRequest $request
     * @param \App\Models\RefAction $refAction
     * @return \Illuminate\Http\Response
     */
    public function update(
        RefActionUpdateRequest $request,
        RefAction $refAction
    ) {
        $this->authorize('update', $refAction);

        $validated = $request->validated();

        $refAction->update($validated);

        return new RefActionResource($refAction);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefAction $refAction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, RefAction $refAction)
    {
        $this->authorize('delete', $refAction);

        $refAction->delete();

        return response()->noContent();
    }
}
