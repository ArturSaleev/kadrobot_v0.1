<?php

namespace App\Http\Controllers\Api;

use App\Models\RefTypePhone;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\BranchPhoneResource;
use App\Http\Resources\BranchPhoneCollection;

class RefTypePhoneBranchPhonesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTypePhone $refTypePhone
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefTypePhone $refTypePhone)
    {
        $this->authorize('view', $refTypePhone);

        $search = $request->get('search', '');

        $branchPhones = $refTypePhone
            ->branchPhones()
            ->search($search)->paginate();

        return new BranchPhoneCollection($branchPhones);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTypePhone $refTypePhone
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefTypePhone $refTypePhone)
    {
        $this->authorize('create', BranchPhone::class);

        $validated = $request->validate([
            'branch_id' => ['required', 'exists:branches,id'],
            'name' => ['required', 'max:255', 'string'],
        ]);

        $branchPhone = $refTypePhone->branchPhones()->create($validated);

        return new BranchPhoneResource($branchPhone);
    }
}
