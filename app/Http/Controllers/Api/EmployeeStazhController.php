<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\EmployeeStazh;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeStazhResource;
use App\Http\Resources\EmployeeStazhCollection;
use App\Http\Requests\EmployeeStazhStoreRequest;
use App\Http\Requests\EmployeeStazhUpdateRequest;

class EmployeeStazhController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeeStazh::class);

        $search = $request->get('search', '');

        $employeeStazhs = EmployeeStazh::search($search)->paginate();

        return new EmployeeStazhCollection($employeeStazhs);
    }

    /**
     * @param \App\Http\Requests\EmployeeStazhStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeStazhStoreRequest $request)
    {
        $this->authorize('create', EmployeeStazh::class);

        $validated = $request->validated();

        $employeeStazh = EmployeeStazh::create($validated);

        return new EmployeeStazhResource($employeeStazh);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeStazh $employeeStazh
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EmployeeStazh $employeeStazh)
    {
        $this->authorize('view', $employeeStazh);

        return new EmployeeStazhResource($employeeStazh);
    }

    /**
     * @param \App\Http\Requests\EmployeeStazhUpdateRequest $request
     * @param \App\Models\EmployeeStazh $employeeStazh
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeeStazhUpdateRequest $request,
        EmployeeStazh $employeeStazh
    ) {
        $this->authorize('update', $employeeStazh);

        $validated = $request->validated();

        $employeeStazh->update($validated);

        return new EmployeeStazhResource($employeeStazh);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeStazh $employeeStazh
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, EmployeeStazh $employeeStazh)
    {
        $this->authorize('delete', $employeeStazh);

        $employeeStazh->delete();

        return response()->noContent();
    }
}
