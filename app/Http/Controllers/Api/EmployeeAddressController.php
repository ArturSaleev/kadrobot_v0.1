<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\EmployeeAddress;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeAddressResource;
use App\Http\Resources\EmployeeAddressCollection;
use App\Http\Requests\EmployeeAddressStoreRequest;
use App\Http\Requests\EmployeeAddressUpdateRequest;

class EmployeeAddressController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeeAddress::class);

        $search = $request->get('search', '');

        $employeeAddresses = EmployeeAddress::search($search)->paginate();

        return new EmployeeAddressCollection($employeeAddresses);
    }

    /**
     * @param \App\Http\Requests\EmployeeAddressStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeAddressStoreRequest $request)
    {
        $this->authorize('create', EmployeeAddress::class);

        $validated = $request->validated();

        $employeeAddress = EmployeeAddress::create($validated);

        return new EmployeeAddressResource($employeeAddress);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeAddress $employeeAddress
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EmployeeAddress $employeeAddress)
    {
        $this->authorize('view', $employeeAddress);

        return new EmployeeAddressResource($employeeAddress);
    }

    /**
     * @param \App\Http\Requests\EmployeeAddressUpdateRequest $request
     * @param \App\Models\EmployeeAddress $employeeAddress
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeeAddressUpdateRequest $request,
        EmployeeAddress $employeeAddress
    ) {
        $this->authorize('update', $employeeAddress);

        $validated = $request->validated();

        $employeeAddress->update($validated);

        return new EmployeeAddressResource($employeeAddress);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeAddress $employeeAddress
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, EmployeeAddress $employeeAddress)
    {
        $this->authorize('delete', $employeeAddress);

        $employeeAddress->delete();

        return response()->noContent();
    }
}
