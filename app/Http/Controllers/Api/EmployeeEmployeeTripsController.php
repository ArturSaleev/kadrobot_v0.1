<?php

namespace App\Http\Controllers\Api;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeTripResource;
use App\Http\Resources\EmployeeTripCollection;

class EmployeeEmployeeTripsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Employee $employee)
    {
        $this->authorize('view', $employee);

        $search = $request->get('search', '');

        $employeeTrips = $employee
            ->employeeTrips()
            ->search($search)->paginate();

        return new EmployeeTripCollection($employeeTrips);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Employee $employee)
    {
        $this->authorize('create', EmployeeTrip::class);

        $validated = $request->validate([
            'ref_transport_trip_id' => [
                'required',
                'exists:ref_transport_trips,id',
            ],
            'date_begin' => ['required', 'date'],
            'date_end' => ['required', 'date'],
            'cnt_days' => ['required', 'numeric'],
            'order_num' => ['required', 'max:255', 'string'],
            'order_date' => ['required', 'date'],
        ]);

        $employeeTrip = $employee->employeeTrips()->create($validated);

        return new EmployeeTripResource($employeeTrip);
    }
}
