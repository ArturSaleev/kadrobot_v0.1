<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefTransportTrip;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefTransportTripResource;
use App\Http\Resources\RefTransportTripCollection;
use App\Http\Requests\RefTransportTripStoreRequest;
use App\Http\Requests\RefTransportTripUpdateRequest;

class RefTransportTripController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefTransportTrip::class);

        $search = $request->get('search', '');

        $refTransportTrips = RefTransportTrip::search($search)->paginate();

        return new RefTransportTripCollection($refTransportTrips);
    }

    /**
     * @param \App\Http\Requests\RefTransportTripStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefTransportTripStoreRequest $request)
    {
        $this->authorize('create', RefTransportTrip::class);

        $validated = $request->validated();

        $refTransportTrip = RefTransportTrip::create($validated);

        return new RefTransportTripResource($refTransportTrip);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTransportTrip $refTransportTrip
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefTransportTrip $refTransportTrip)
    {
        $this->authorize('view', $refTransportTrip);

        return new RefTransportTripResource($refTransportTrip);
    }

    /**
     * @param \App\Http\Requests\RefTransportTripUpdateRequest $request
     * @param \App\Models\RefTransportTrip $refTransportTrip
     * @return \Illuminate\Http\Response
     */
    public function update(
        RefTransportTripUpdateRequest $request,
        RefTransportTrip $refTransportTrip
    ) {
        $this->authorize('update', $refTransportTrip);

        $validated = $request->validated();

        $refTransportTrip->update($validated);

        return new RefTransportTripResource($refTransportTrip);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTransportTrip $refTransportTrip
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        Request $request,
        RefTransportTrip $refTransportTrip
    ) {
        $this->authorize('delete', $refTransportTrip);

        $refTransportTrip->delete();

        return response()->noContent();
    }
}
