<?php

namespace App\Http\Controllers\Api;

use App\Models\RefTypePhone;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\DepartmentPhoneResource;
use App\Http\Resources\DepartmentPhoneCollection;

class RefTypePhoneDepartmentPhonesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTypePhone $refTypePhone
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefTypePhone $refTypePhone)
    {
        $this->authorize('view', $refTypePhone);

        $search = $request->get('search', '');

        $departmentPhones = $refTypePhone
            ->departmentPhones()
            ->search($search)->paginate();

        return new DepartmentPhoneCollection($departmentPhones);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTypePhone $refTypePhone
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefTypePhone $refTypePhone)
    {
        $this->authorize('create', DepartmentPhone::class);

        $validated = $request->validate([
            'department_id' => ['required', 'exists:departments,id'],
            'name' => ['required', 'max:255', 'string'],
        ]);

        $departmentPhone = $refTypePhone
            ->departmentPhones()
            ->create($validated);

        return new DepartmentPhoneResource($departmentPhone);
    }
}
