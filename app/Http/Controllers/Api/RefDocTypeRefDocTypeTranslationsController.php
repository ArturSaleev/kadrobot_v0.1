<?php

namespace App\Http\Controllers\Api;

use App\Models\RefDocType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefDocTypeTranslationResource;
use App\Http\Resources\RefDocTypeTranslationCollection;

class RefDocTypeRefDocTypeTranslationsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefDocType $refDocType
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefDocType $refDocType)
    {
        $this->authorize('view', $refDocType);

        $search = $request->get('search', '');

        $refDocTypeTranslations = $refDocType
            ->refDocTypeTranslations()
            ->search($search)->paginate();

        return new RefDocTypeTranslationCollection($refDocTypeTranslations);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefDocType $refDocType
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefDocType $refDocType)
    {
        $this->authorize('create', RefDocTypeTranslation::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'name' => ['required', 'max:255', 'string'],
        ]);

        $refDocTypeTranslation = $refDocType
            ->refDocTypeTranslations()
            ->create($validated);

        return new RefDocTypeTranslationResource($refDocTypeTranslation);
    }
}
