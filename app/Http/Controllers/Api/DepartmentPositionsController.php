<?php

namespace App\Http\Controllers\Api;

use App\Models\Department;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PositionResource;
use App\Http\Resources\PositionCollection;

class DepartmentPositionsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Department $department
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Department $department)
    {
        $this->authorize('view', $department);

        $search = $request->get('search', '');

        $positions = $department
            ->positions()
            ->search($search)->paginate();

        return new PositionCollection($positions);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Department $department
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Department $department)
    {
        $this->authorize('create', Position::class);

        $validated = $request->validate([
            'cnt' => ['required', 'numeric'],
            'pos_level' => ['required', 'numeric'],
            'min_salary' => ['required', 'numeric'],
            'max_salary' => ['required', 'numeric'],
        ]);

        $position = $department->positions()->create($validated);

        return new PositionResource($position);
    }
}
