<?php

namespace App\Http\Controllers\Api;

use App\Models\RefStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefStatusResource;
use App\Http\Resources\RefStatusCollection;
use App\Http\Requests\RefStatusStoreRequest;
use App\Http\Requests\RefStatusUpdateRequest;

class RefStatusController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefStatus::class);

        $search = $request->get('search', '');

        $refStatuses = RefStatus::search($search)->paginate();

        return new RefStatusCollection($refStatuses);
    }

    /**
     * @param \App\Http\Requests\RefStatusStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefStatusStoreRequest $request)
    {
        $this->authorize('create', RefStatus::class);

        $validated = $request->validated();

        $refStatus = RefStatus::create($validated);

        return new RefStatusResource($refStatus);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefStatus $refStatus
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefStatus $refStatus)
    {
        $this->authorize('view', $refStatus);

        return new RefStatusResource($refStatus);
    }

    /**
     * @param \App\Http\Requests\RefStatusUpdateRequest $request
     * @param \App\Models\RefStatus $refStatus
     * @return \Illuminate\Http\Response
     */
    public function update(
        RefStatusUpdateRequest $request,
        RefStatus $refStatus
    ) {
        $this->authorize('update', $refStatus);

        $validated = $request->validated();

        $refStatus->update($validated);

        return new RefStatusResource($refStatus);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefStatus $refStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, RefStatus $refStatus)
    {
        $this->authorize('delete', $refStatus);

        $refStatus->delete();

        return response()->noContent();
    }
}
