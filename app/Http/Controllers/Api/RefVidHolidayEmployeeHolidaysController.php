<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefVidHoliday;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeHolidayResource;
use App\Http\Resources\EmployeeHolidayCollection;

class RefVidHolidayEmployeeHolidaysController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefVidHoliday $refVidHoliday
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefVidHoliday $refVidHoliday)
    {
        $this->authorize('view', $refVidHoliday);

        $search = $request->get('search', '');

        $employeeHolidays = $refVidHoliday
            ->employeeHolidays()
            ->search($search)->paginate();

        return new EmployeeHolidayCollection($employeeHolidays);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefVidHoliday $refVidHoliday
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefVidHoliday $refVidHoliday)
    {
        $this->authorize('create', EmployeeHoliday::class);

        $validated = $request->validate([
            'employee_id' => ['required', 'exists:employees,id'],
            'date_begin' => ['nullable', 'date'],
            'date_end' => ['nullable', 'date'],
            'cnt_days' => ['nullable', 'numeric'],
            'period_begin' => ['nullable', 'date'],
            'period_end' => ['nullable', 'date'],
            'order_num' => ['nullable', 'max:255', 'string'],
            'order_date' => ['nullable', 'date'],
            'deligate' => ['nullable', 'numeric'],
            'deligate_emp_id' => ['nullable', 'max:255'],
            'doc_content' => ['required', 'max:255', 'string'],
        ]);

        $employeeHoliday = $refVidHoliday
            ->employeeHolidays()
            ->create($validated);

        return new EmployeeHolidayResource($employeeHoliday);
    }
}
