<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefUserStatus;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefUserStatusResource;
use App\Http\Resources\RefUserStatusCollection;
use App\Http\Requests\RefUserStatusStoreRequest;
use App\Http\Requests\RefUserStatusUpdateRequest;

class RefUserStatusController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefUserStatus::class);

        $search = $request->get('search', '');

        $refUserStatuses = RefUserStatus::search($search)->paginate();

        return new RefUserStatusCollection($refUserStatuses);
    }

    /**
     * @param \App\Http\Requests\RefUserStatusStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefUserStatusStoreRequest $request)
    {
        $this->authorize('create', RefUserStatus::class);

        $validated = $request->validated();

        $refUserStatus = RefUserStatus::create($validated);

        return new RefUserStatusResource($refUserStatus);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefUserStatus $refUserStatus
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefUserStatus $refUserStatus)
    {
        $this->authorize('view', $refUserStatus);

        return new RefUserStatusResource($refUserStatus);
    }

    /**
     * @param \App\Http\Requests\RefUserStatusUpdateRequest $request
     * @param \App\Models\RefUserStatus $refUserStatus
     * @return \Illuminate\Http\Response
     */
    public function update(
        RefUserStatusUpdateRequest $request,
        RefUserStatus $refUserStatus
    ) {
        $this->authorize('update', $refUserStatus);

        $validated = $request->validated();

        $refUserStatus->update($validated);

        return new RefUserStatusResource($refUserStatus);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefUserStatus $refUserStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, RefUserStatus $refUserStatus)
    {
        $this->authorize('delete', $refUserStatus);

        $refUserStatus->delete();

        return response()->noContent();
    }
}
