<?php

namespace App\Http\Controllers\Api;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeHolidaysPeriodResource;
use App\Http\Resources\EmployeeHolidaysPeriodCollection;

class EmployeeEmployeeHolidaysPeriodsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Employee $employee)
    {
        $this->authorize('view', $employee);

        $search = $request->get('search', '');

        $employeeHolidaysPeriods = $employee
            ->employeeHolidaysPeriods()
            ->search($search)->paginate();

        return new EmployeeHolidaysPeriodCollection($employeeHolidaysPeriods);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Employee $employee)
    {
        $this->authorize('create', EmployeeHolidaysPeriod::class);

        $validated = $request->validate([
            'period_start' => ['required', 'date'],
            'period_end' => ['required', 'date'],
            'day_count_used_for_today' => ['required', 'numeric'],
            'didnt_add' => ['required', 'numeric'],
            'paying_for_health' => ['required', 'numeric'],
        ]);

        $employeeHolidaysPeriod = $employee
            ->employeeHolidaysPeriods()
            ->create($validated);

        return new EmployeeHolidaysPeriodResource($employeeHolidaysPeriod);
    }
}
