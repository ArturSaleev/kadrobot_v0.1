<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\EmployeeMilitary;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeMilitaryResource;
use App\Http\Resources\EmployeeMilitaryCollection;
use App\Http\Requests\EmployeeMilitaryStoreRequest;
use App\Http\Requests\EmployeeMilitaryUpdateRequest;

class EmployeeMilitaryController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeeMilitary::class);

        $search = $request->get('search', '');

        $employeeMilitaries = EmployeeMilitary::search($search)->paginate();

        return new EmployeeMilitaryCollection($employeeMilitaries);
    }

    /**
     * @param \App\Http\Requests\EmployeeMilitaryStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeMilitaryStoreRequest $request)
    {
        $this->authorize('create', EmployeeMilitary::class);

        $validated = $request->validated();

        $employeeMilitary = EmployeeMilitary::create($validated);

        return new EmployeeMilitaryResource($employeeMilitary);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeMilitary $employeeMilitary
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EmployeeMilitary $employeeMilitary)
    {
        $this->authorize('view', $employeeMilitary);

        return new EmployeeMilitaryResource($employeeMilitary);
    }

    /**
     * @param \App\Http\Requests\EmployeeMilitaryUpdateRequest $request
     * @param \App\Models\EmployeeMilitary $employeeMilitary
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeeMilitaryUpdateRequest $request,
        EmployeeMilitary $employeeMilitary
    ) {
        $this->authorize('update', $employeeMilitary);

        $validated = $request->validated();

        $employeeMilitary->update($validated);

        return new EmployeeMilitaryResource($employeeMilitary);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeMilitary $employeeMilitary
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        Request $request,
        EmployeeMilitary $employeeMilitary
    ) {
        $this->authorize('delete', $employeeMilitary);

        $employeeMilitary->delete();

        return response()->noContent();
    }
}
