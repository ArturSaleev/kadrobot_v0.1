<?php

namespace App\Http\Controllers\Api;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeEducationResource;
use App\Http\Resources\EmployeeEducationCollection;

class EmployeeEmployeeEducationsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Employee $employee)
    {
        $this->authorize('view', $employee);

        $search = $request->get('search', '');

        $employeeEducations = $employee
            ->employeeEducations()
            ->search($search)->paginate();

        return new EmployeeEducationCollection($employeeEducations);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Employee $employee)
    {
        $this->authorize('create', EmployeeEducation::class);

        $validated = $request->validate([
            'institution' => ['required', 'max:255', 'string'],
            'year_begin' => ['nullable', 'numeric'],
            'year_end' => ['nullable', 'numeric'],
            'date_begin' => ['nullable', 'date'],
            'date_end' => ['nullable', 'date'],
            'speciality' => ['nullable', 'max:255', 'string'],
            'qualification' => ['nullable', 'max:255', 'string'],
            'diplom_num' => ['nullable', 'max:255', 'string'],
            'diplom_date' => ['nullable', 'date'],
        ]);

        $employeeEducation = $employee
            ->employeeEducations()
            ->create($validated);

        return new EmployeeEducationResource($employeeEducation);
    }
}
