<?php

namespace App\Http\Controllers\Api;

use App\Models\Branch;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\BranchTranslationResource;
use App\Http\Resources\BranchTranslationCollection;

class BranchBranchTranslationsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Branch $branch
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Branch $branch)
    {
        $this->authorize('view', $branch);

        $search = $request->get('search', '');

        $branchTranslations = $branch
            ->branchTranslations()
            ->search($search)->paginate();

        return new BranchTranslationCollection($branchTranslations);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Branch $branch
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Branch $branch)
    {
        $this->authorize('create', BranchTranslation::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'name' => ['required', 'max:255', 'string'],
            'short_name' => ['nullable', 'max:255', 'string'],
        ]);

        $branchTranslation = $branch->branchTranslations()->create($validated);

        return new BranchTranslationResource($branchTranslation);
    }
}
