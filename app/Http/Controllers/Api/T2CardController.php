<?php

namespace App\Http\Controllers\Api;

use App\Models\T2Card;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\T2CardResource;
use App\Http\Resources\T2CardCollection;
use App\Http\Requests\T2CardStoreRequest;
use App\Http\Requests\T2CardUpdateRequest;

class T2CardController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', T2Card::class);

        $search = $request->get('search', '');

        $t2Cards = T2Card::search($search)->paginate();

        return new T2CardCollection($t2Cards);
    }

    /**
     * @param \App\Http\Requests\T2CardStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(T2CardStoreRequest $request)
    {
        $this->authorize('create', T2Card::class);

        $validated = $request->validated();

        $t2Card = T2Card::create($validated);

        return new T2CardResource($t2Card);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\T2Card $t2Card
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, T2Card $t2Card)
    {
        $this->authorize('view', $t2Card);

        return new T2CardResource($t2Card);
    }

    /**
     * @param \App\Http\Requests\T2CardUpdateRequest $request
     * @param \App\Models\T2Card $t2Card
     * @return \Illuminate\Http\Response
     */
    public function update(T2CardUpdateRequest $request, T2Card $t2Card)
    {
        $this->authorize('update', $t2Card);

        $validated = $request->validated();

        $t2Card->update($validated);

        return new T2CardResource($t2Card);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\T2Card $t2Card
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, T2Card $t2Card)
    {
        $this->authorize('delete', $t2Card);

        $t2Card->delete();

        return response()->noContent();
    }
}
