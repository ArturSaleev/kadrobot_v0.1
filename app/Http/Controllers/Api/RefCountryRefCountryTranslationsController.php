<?php

namespace App\Http\Controllers\Api;

use App\Models\RefCountry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefCountryTranslationResource;
use App\Http\Resources\RefCountryTranslationCollection;

class RefCountryRefCountryTranslationsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefCountry $refCountry
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefCountry $refCountry)
    {
        $this->authorize('view', $refCountry);

        $search = $request->get('search', '');

        $refCountryTranslations = $refCountry
            ->refCountryTranslations()
            ->search($search)->paginate();

        return new RefCountryTranslationCollection($refCountryTranslations);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefCountry $refCountry
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefCountry $refCountry)
    {
        $this->authorize('create', RefCountryTranslation::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'name' => ['required', 'max:255', 'string'],
        ]);

        $refCountryTranslation = $refCountry
            ->refCountryTranslations()
            ->create($validated);

        return new RefCountryTranslationResource($refCountryTranslation);
    }
}
