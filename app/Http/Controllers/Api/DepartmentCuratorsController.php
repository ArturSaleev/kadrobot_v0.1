<?php

namespace App\Http\Controllers\Api;

use App\Models\Department;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CuratorResource;
use App\Http\Resources\CuratorCollection;

class DepartmentCuratorsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Department $department
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Department $department)
    {
        $this->authorize('view', $department);

        $search = $request->get('search', '');

        $curators = $department
            ->curators()
            ->search($search)->paginate();

        return new CuratorCollection($curators);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Department $department
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Department $department)
    {
        $this->authorize('create', Curator::class);

        $validated = $request->validate([
            'employee_id' => ['required', 'exists:employees,id'],
        ]);

        $curator = $department->curators()->create($validated);

        return new CuratorResource($curator);
    }
}
