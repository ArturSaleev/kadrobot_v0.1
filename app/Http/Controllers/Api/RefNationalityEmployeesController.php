<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefNationality;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeResource;
use App\Http\Resources\EmployeeCollection;

class RefNationalityEmployeesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefNationality $refNationality
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefNationality $refNationality)
    {
        $this->authorize('view', $refNationality);

        $search = $request->get('search', '');

        $employees = $refNationality
            ->employees()
            ->search($search)->paginate();

        return new EmployeeCollection($employees);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefNationality $refNationality
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefNationality $refNationality)
    {
        $this->authorize('create', Employee::class);

        $validated = $request->validate([
            'date_post' => ['nullable', 'date'],
            'date_loyoff' => ['nullable', 'date'],
            'tab_num' => ['nullable', 'numeric'],
            'birthday' => ['nullable', 'date'],
            'birth_place' => ['nullable', 'max:255', 'string'],
            'ref_sex_id' => ['nullable', 'exists:ref_sexes,id'],
            'iin' => ['required', 'max:255', 'string'],
            'ref_family_state_id' => [
                'nullable',
                'exists:ref_family_states,id',
            ],
            'contract_num' => ['nullable', 'max:255', 'string'],
            'contract_date' => ['nullable', 'date'],
            'ref_account_type_id' => [
                'required',
                'exists:ref_account_types,id',
            ],
            'email' => ['required', 'email'],
            'account' => ['nullable', 'max:255', 'string'],
            'date_zav' => ['required', 'date'],
            'oklad' => ['nullable', 'numeric'],
            'gos_nagr' => ['required', 'max:255', 'string'],
            'pens' => ['required', 'boolean'],
            'pens_date' => ['nullable', 'date'],
            'lgot' => ['nullable', 'max:255', 'string'],
            'person_email' => ['nullable', 'max:255', 'string'],
            'ref_user_status_id' => ['required', 'exists:ref_user_statuses,id'],
        ]);

        $employee = $refNationality->employees()->create($validated);

        return new EmployeeResource($employee);
    }
}
