<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\EmployeeInvalide;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeInvalideResource;
use App\Http\Resources\EmployeeInvalideCollection;
use App\Http\Requests\EmployeeInvalideStoreRequest;
use App\Http\Requests\EmployeeInvalideUpdateRequest;

class EmployeeInvalideController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeeInvalide::class);

        $search = $request->get('search', '');

        $employeeInvalides = EmployeeInvalide::search($search)->paginate();

        return new EmployeeInvalideCollection($employeeInvalides);
    }

    /**
     * @param \App\Http\Requests\EmployeeInvalideStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeInvalideStoreRequest $request)
    {
        $this->authorize('create', EmployeeInvalide::class);

        $validated = $request->validated();

        $employeeInvalide = EmployeeInvalide::create($validated);

        return new EmployeeInvalideResource($employeeInvalide);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeInvalide $employeeInvalide
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EmployeeInvalide $employeeInvalide)
    {
        $this->authorize('view', $employeeInvalide);

        return new EmployeeInvalideResource($employeeInvalide);
    }

    /**
     * @param \App\Http\Requests\EmployeeInvalideUpdateRequest $request
     * @param \App\Models\EmployeeInvalide $employeeInvalide
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeeInvalideUpdateRequest $request,
        EmployeeInvalide $employeeInvalide
    ) {
        $this->authorize('update', $employeeInvalide);

        $validated = $request->validated();

        $employeeInvalide->update($validated);

        return new EmployeeInvalideResource($employeeInvalide);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeInvalide $employeeInvalide
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        Request $request,
        EmployeeInvalide $employeeInvalide
    ) {
        $this->authorize('delete', $employeeInvalide);

        $employeeInvalide->delete();

        return response()->noContent();
    }
}
