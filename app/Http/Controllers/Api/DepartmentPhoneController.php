<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\DepartmentPhone;
use App\Http\Controllers\Controller;
use App\Http\Resources\DepartmentPhoneResource;
use App\Http\Resources\DepartmentPhoneCollection;
use App\Http\Requests\DepartmentPhoneStoreRequest;
use App\Http\Requests\DepartmentPhoneUpdateRequest;

class DepartmentPhoneController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', DepartmentPhone::class);

        $search = $request->get('search', '');

        $departmentPhones = DepartmentPhone::search($search)->paginate();

        return new DepartmentPhoneCollection($departmentPhones);
    }

    /**
     * @param \App\Http\Requests\DepartmentPhoneStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepartmentPhoneStoreRequest $request)
    {
        $this->authorize('create', DepartmentPhone::class);

        $validated = $request->validated();

        $departmentPhone = DepartmentPhone::create($validated);

        return new DepartmentPhoneResource($departmentPhone);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\DepartmentPhone $departmentPhone
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, DepartmentPhone $departmentPhone)
    {
        $this->authorize('view', $departmentPhone);

        return new DepartmentPhoneResource($departmentPhone);
    }

    /**
     * @param \App\Http\Requests\DepartmentPhoneUpdateRequest $request
     * @param \App\Models\DepartmentPhone $departmentPhone
     * @return \Illuminate\Http\Response
     */
    public function update(
        DepartmentPhoneUpdateRequest $request,
        DepartmentPhone $departmentPhone
    ) {
        $this->authorize('update', $departmentPhone);

        $validated = $request->validated();

        $departmentPhone->update($validated);

        return new DepartmentPhoneResource($departmentPhone);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\DepartmentPhone $departmentPhone
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, DepartmentPhone $departmentPhone)
    {
        $this->authorize('delete', $departmentPhone);

        $departmentPhone->delete();

        return response()->noContent();
    }
}
