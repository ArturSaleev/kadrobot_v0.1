<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefKindHoliday;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefKindHolidayTranslationResource;
use App\Http\Resources\RefKindHolidayTranslationCollection;

class RefKindHolidayRefKindHolidayTranslationsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefKindHoliday $refKindHoliday
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefKindHoliday $refKindHoliday)
    {
        $this->authorize('view', $refKindHoliday);

        $search = $request->get('search', '');

        $refKindHolidayTranslations = $refKindHoliday
            ->refKindHolidayTranslations()
            ->search($search)->paginate();

        return new RefKindHolidayTranslationCollection(
            $refKindHolidayTranslations
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefKindHoliday $refKindHoliday
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefKindHoliday $refKindHoliday)
    {
        $this->authorize('create', RefKindHolidayTranslation::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'name' => ['required', 'max:255', 'string'],
        ]);

        $refKindHolidayTranslation = $refKindHoliday
            ->refKindHolidayTranslations()
            ->create($validated);

        return new RefKindHolidayTranslationResource(
            $refKindHolidayTranslation
        );
    }
}
