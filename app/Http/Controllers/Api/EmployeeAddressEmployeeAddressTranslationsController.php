<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\EmployeeAddress;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeAddressTranslationResource;
use App\Http\Resources\EmployeeAddressTranslationCollection;

class EmployeeAddressEmployeeAddressTranslationsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeAddress $employeeAddress
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, EmployeeAddress $employeeAddress)
    {
        $this->authorize('view', $employeeAddress);

        $search = $request->get('search', '');

        $employeeAddressTranslations = $employeeAddress
            ->employeeAddressTranslations()
            ->search($search)->paginate();

        return new EmployeeAddressTranslationCollection(
            $employeeAddressTranslations
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeAddress $employeeAddress
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, EmployeeAddress $employeeAddress)
    {
        $this->authorize('create', EmployeeAddressTranslation::class);

        $validated = $request->validate([]);

        $employeeAddressTranslation = $employeeAddress
            ->employeeAddressTranslations()
            ->create($validated);

        return new EmployeeAddressTranslationResource(
            $employeeAddressTranslation
        );
    }
}
