<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\EmployeePhone;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeePhoneResource;
use App\Http\Resources\EmployeePhoneCollection;
use App\Http\Requests\EmployeePhoneStoreRequest;
use App\Http\Requests\EmployeePhoneUpdateRequest;

class EmployeePhoneController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeePhone::class);

        $search = $request->get('search', '');

        $employeePhones = EmployeePhone::search($search)->paginate();

        return new EmployeePhoneCollection($employeePhones);
    }

    /**
     * @param \App\Http\Requests\EmployeePhoneStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeePhoneStoreRequest $request)
    {
        $this->authorize('create', EmployeePhone::class);

        $validated = $request->validated();

        $employeePhone = EmployeePhone::create($validated);

        return new EmployeePhoneResource($employeePhone);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeePhone $employeePhone
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EmployeePhone $employeePhone)
    {
        $this->authorize('view', $employeePhone);

        return new EmployeePhoneResource($employeePhone);
    }

    /**
     * @param \App\Http\Requests\EmployeePhoneUpdateRequest $request
     * @param \App\Models\EmployeePhone $employeePhone
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeePhoneUpdateRequest $request,
        EmployeePhone $employeePhone
    ) {
        $this->authorize('update', $employeePhone);

        $validated = $request->validated();

        $employeePhone->update($validated);

        return new EmployeePhoneResource($employeePhone);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeePhone $employeePhone
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, EmployeePhone $employeePhone)
    {
        $this->authorize('delete', $employeePhone);

        $employeePhone->delete();

        return response()->noContent();
    }
}
