<?php

namespace App\Http\Controllers\Api;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeePayAllowanceResource;
use App\Http\Resources\EmployeePayAllowanceCollection;

class EmployeeEmployeePayAllowancesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Employee $employee)
    {
        $this->authorize('view', $employee);

        $search = $request->get('search', '');

        $employeePayAllowances = $employee
            ->employeePayAllowances()
            ->search($search)->paginate();

        return new EmployeePayAllowanceCollection($employeePayAllowances);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Employee $employee)
    {
        $this->authorize('create', EmployeePayAllowance::class);

        $validated = $request->validate([
            'ref_allowance_type_id' => [
                'required',
                'exists:ref_allowance_types,id',
            ],
            'date_add' => ['required', 'date'],
            'period_begin' => ['required', 'date'],
            'period_end' => ['required', 'date'],
            'paysum' => ['required', 'numeric'],
        ]);

        $employeePayAllowance = $employee
            ->employeePayAllowances()
            ->create($validated);

        return new EmployeePayAllowanceResource($employeePayAllowance);
    }
}
