<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefVidHolidayType;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefVidHolidayTypeTranslationResource;
use App\Http\Resources\RefVidHolidayTypeTranslationCollection;

class RefVidHolidayTypeRefVidHolidayTypeTranslationsController extends
    Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefVidHolidayType $refVidHolidayType
     * @return \Illuminate\Http\Response
     */
    public function index(
        Request $request,
        RefVidHolidayType $refVidHolidayType
    ) {
        $this->authorize('view', $refVidHolidayType);

        $search = $request->get('search', '');

        $refVidHolidayTypeTranslations = $refVidHolidayType
            ->refVidHolidayTypeTranslations()
            ->search($search)->paginate();

        return new RefVidHolidayTypeTranslationCollection(
            $refVidHolidayTypeTranslations
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefVidHolidayType $refVidHolidayType
     * @return \Illuminate\Http\Response
     */
    public function store(
        Request $request,
        RefVidHolidayType $refVidHolidayType
    ) {
        $this->authorize('create', RefVidHolidayTypeTranslation::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'name' => ['required', 'max:255', 'string'],
        ]);

        $refVidHolidayTypeTranslation = $refVidHolidayType
            ->refVidHolidayTypeTranslations()
            ->create($validated);

        return new RefVidHolidayTypeTranslationResource(
            $refVidHolidayTypeTranslation
        );
    }
}
