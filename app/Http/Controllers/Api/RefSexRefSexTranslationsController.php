<?php

namespace App\Http\Controllers\Api;

use App\Models\RefSex;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefSexTranslationResource;
use App\Http\Resources\RefSexTranslationCollection;

class RefSexRefSexTranslationsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefSex $refSex
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefSex $refSex)
    {
        $this->authorize('view', $refSex);

        $search = $request->get('search', '');

        $refSexTranslations = $refSex
            ->refSexTranslations()
            ->search($search)->paginate();

        return new RefSexTranslationCollection($refSexTranslations);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefSex $refSex
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefSex $refSex)
    {
        $this->authorize('create', RefSexTranslation::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'name' => ['required', 'max:255', 'string'],
        ]);

        $refSexTranslation = $refSex->refSexTranslations()->create($validated);

        return new RefSexTranslationResource($refSexTranslation);
    }
}
