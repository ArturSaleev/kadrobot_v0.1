<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefFamilyState;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefFamilyStateResource;
use App\Http\Resources\RefFamilyStateCollection;
use App\Http\Requests\RefFamilyStateStoreRequest;
use App\Http\Requests\RefFamilyStateUpdateRequest;

class RefFamilyStateController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefFamilyState::class);

        $search = $request->get('search', '');

        $refFamilyStates = RefFamilyState::search($search)->paginate();

        return new RefFamilyStateCollection($refFamilyStates);
    }

    /**
     * @param \App\Http\Requests\RefFamilyStateStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefFamilyStateStoreRequest $request)
    {
        $this->authorize('create', RefFamilyState::class);

        $validated = $request->validated();

        $refFamilyState = RefFamilyState::create($validated);

        return new RefFamilyStateResource($refFamilyState);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefFamilyState $refFamilyState
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefFamilyState $refFamilyState)
    {
        $this->authorize('view', $refFamilyState);

        return new RefFamilyStateResource($refFamilyState);
    }

    /**
     * @param \App\Http\Requests\RefFamilyStateUpdateRequest $request
     * @param \App\Models\RefFamilyState $refFamilyState
     * @return \Illuminate\Http\Response
     */
    public function update(
        RefFamilyStateUpdateRequest $request,
        RefFamilyState $refFamilyState
    ) {
        $this->authorize('update', $refFamilyState);

        $validated = $request->validated();

        $refFamilyState->update($validated);

        return new RefFamilyStateResource($refFamilyState);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefFamilyState $refFamilyState
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, RefFamilyState $refFamilyState)
    {
        $this->authorize('delete', $refFamilyState);

        $refFamilyState->delete();

        return response()->noContent();
    }
}
