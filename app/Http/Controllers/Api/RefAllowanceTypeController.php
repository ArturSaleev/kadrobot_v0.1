<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefAllowanceType;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefAllowanceTypeResource;
use App\Http\Resources\RefAllowanceTypeCollection;
use App\Http\Requests\RefAllowanceTypeStoreRequest;
use App\Http\Requests\RefAllowanceTypeUpdateRequest;

class RefAllowanceTypeController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefAllowanceType::class);

        $search = $request->get('search', '');

        $refAllowanceTypes = RefAllowanceType::search($search)->paginate();

        return new RefAllowanceTypeCollection($refAllowanceTypes);
    }

    /**
     * @param \App\Http\Requests\RefAllowanceTypeStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefAllowanceTypeStoreRequest $request)
    {
        $this->authorize('create', RefAllowanceType::class);

        $validated = $request->validated();

        $refAllowanceType = RefAllowanceType::create($validated);

        return new RefAllowanceTypeResource($refAllowanceType);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefAllowanceType $refAllowanceType
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefAllowanceType $refAllowanceType)
    {
        $this->authorize('view', $refAllowanceType);

        return new RefAllowanceTypeResource($refAllowanceType);
    }

    /**
     * @param \App\Http\Requests\RefAllowanceTypeUpdateRequest $request
     * @param \App\Models\RefAllowanceType $refAllowanceType
     * @return \Illuminate\Http\Response
     */
    public function update(
        RefAllowanceTypeUpdateRequest $request,
        RefAllowanceType $refAllowanceType
    ) {
        $this->authorize('update', $refAllowanceType);

        $validated = $request->validated();

        $refAllowanceType->update($validated);

        return new RefAllowanceTypeResource($refAllowanceType);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefAllowanceType $refAllowanceType
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        Request $request,
        RefAllowanceType $refAllowanceType
    ) {
        $this->authorize('delete', $refAllowanceType);

        $refAllowanceType->delete();

        return response()->noContent();
    }
}
