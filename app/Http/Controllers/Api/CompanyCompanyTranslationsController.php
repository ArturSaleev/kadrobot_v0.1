<?php

namespace App\Http\Controllers\Api;

use App\Models\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyTranslationResource;
use App\Http\Resources\CompanyTranslationCollection;

class CompanyCompanyTranslationsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Company $company
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Company $company)
    {
        $this->authorize('view', $company);

        $search = $request->get('search', '');

        $companyTranslations = $company
            ->companyTranslations()
            ->search($search)->paginate();

        return new CompanyTranslationCollection($companyTranslations);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Company $company
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Company $company)
    {
        $this->authorize('create', CompanyTranslation::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'name' => ['required', 'max:255', 'string'],
        ]);

        $companyTranslation = $company
            ->companyTranslations()
            ->create($validated);

        return new CompanyTranslationResource($companyTranslation);
    }
}
