<?php

namespace App\Http\Controllers\Api;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeInvalideResource;
use App\Http\Resources\EmployeeInvalideCollection;

class EmployeeEmployeeInvalidesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Employee $employee)
    {
        $this->authorize('view', $employee);

        $search = $request->get('search', '');

        $employeeInvalides = $employee
            ->employeeInvalides()
            ->search($search)->paginate();

        return new EmployeeInvalideCollection($employeeInvalides);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Employee $employee)
    {
        $this->authorize('create', EmployeeInvalide::class);

        $validated = $request->validate([
            'num' => ['nullable', 'max:255', 'string'],
            'date_add' => ['nullable', 'date'],
            'period_begin' => ['nullable', 'date'],
            'period_end' => ['nullable', 'date'],
        ]);

        $employeeInvalide = $employee->employeeInvalides()->create($validated);

        return new EmployeeInvalideResource($employeeInvalide);
    }
}
