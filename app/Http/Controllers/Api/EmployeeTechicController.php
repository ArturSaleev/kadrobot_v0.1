<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\EmployeeTechic;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeTechicResource;
use App\Http\Resources\EmployeeTechicCollection;
use App\Http\Requests\EmployeeTechicStoreRequest;
use App\Http\Requests\EmployeeTechicUpdateRequest;

class EmployeeTechicController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeeTechic::class);

        $search = $request->get('search', '');

        $employeeTechics = EmployeeTechic::search($search)->paginate();

        return new EmployeeTechicCollection($employeeTechics);
    }

    /**
     * @param \App\Http\Requests\EmployeeTechicStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeTechicStoreRequest $request)
    {
        $this->authorize('create', EmployeeTechic::class);

        $validated = $request->validated();

        $employeeTechic = EmployeeTechic::create($validated);

        return new EmployeeTechicResource($employeeTechic);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeTechic $employeeTechic
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EmployeeTechic $employeeTechic)
    {
        $this->authorize('view', $employeeTechic);

        return new EmployeeTechicResource($employeeTechic);
    }

    /**
     * @param \App\Http\Requests\EmployeeTechicUpdateRequest $request
     * @param \App\Models\EmployeeTechic $employeeTechic
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeeTechicUpdateRequest $request,
        EmployeeTechic $employeeTechic
    ) {
        $this->authorize('update', $employeeTechic);

        $validated = $request->validated();

        $employeeTechic->update($validated);

        return new EmployeeTechicResource($employeeTechic);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeTechic $employeeTechic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, EmployeeTechic $employeeTechic)
    {
        $this->authorize('delete', $employeeTechic);

        $employeeTechic->delete();

        return response()->noContent();
    }
}
