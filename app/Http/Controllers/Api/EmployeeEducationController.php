<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\EmployeeEducation;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeEducationResource;
use App\Http\Resources\EmployeeEducationCollection;
use App\Http\Requests\EmployeeEducationStoreRequest;
use App\Http\Requests\EmployeeEducationUpdateRequest;

class EmployeeEducationController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeeEducation::class);

        $search = $request->get('search', '');

        $employeeEducations = EmployeeEducation::search($search)->paginate();

        return new EmployeeEducationCollection($employeeEducations);
    }

    /**
     * @param \App\Http\Requests\EmployeeEducationStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeEducationStoreRequest $request)
    {
        $this->authorize('create', EmployeeEducation::class);

        $validated = $request->validated();

        $employeeEducation = EmployeeEducation::create($validated);

        return new EmployeeEducationResource($employeeEducation);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeEducation $employeeEducation
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EmployeeEducation $employeeEducation)
    {
        $this->authorize('view', $employeeEducation);

        return new EmployeeEducationResource($employeeEducation);
    }

    /**
     * @param \App\Http\Requests\EmployeeEducationUpdateRequest $request
     * @param \App\Models\EmployeeEducation $employeeEducation
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeeEducationUpdateRequest $request,
        EmployeeEducation $employeeEducation
    ) {
        $this->authorize('update', $employeeEducation);

        $validated = $request->validated();

        $employeeEducation->update($validated);

        return new EmployeeEducationResource($employeeEducation);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeEducation $employeeEducation
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        Request $request,
        EmployeeEducation $employeeEducation
    ) {
        $this->authorize('delete', $employeeEducation);

        $employeeEducation->delete();

        return response()->noContent();
    }
}
