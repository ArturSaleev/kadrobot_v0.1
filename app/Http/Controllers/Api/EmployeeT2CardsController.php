<?php

namespace App\Http\Controllers\Api;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\T2CardResource;
use App\Http\Resources\T2CardCollection;

class EmployeeT2CardsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Employee $employee)
    {
        $this->authorize('view', $employee);

        $search = $request->get('search', '');

        $t2Cards = $employee
            ->t2Cards()
            ->search($search)->paginate();

        return new T2CardCollection($t2Cards);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Employee $employee)
    {
        $this->authorize('create', T2Card::class);

        $validated = $request->validate([
            'company_id' => ['required', 'exists:companies,id'],
            'ref_branch_id' => ['required', 'exists:branches,id'],
            'ref_action_id' => ['required', 'exists:ref_actions,id'],
            'ref_position_id' => ['required', 'exists:positions,id'],
        ]);

        $t2Card = $employee->t2Cards()->create($validated);

        return new T2CardResource($t2Card);
    }
}
