<?php

namespace App\Http\Controllers\Api;

use App\Models\RefTypePhone;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeePhoneResource;
use App\Http\Resources\EmployeePhoneCollection;

class RefTypePhoneEmployeePhonesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTypePhone $refTypePhone
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefTypePhone $refTypePhone)
    {
        $this->authorize('view', $refTypePhone);

        $search = $request->get('search', '');

        $employeePhones = $refTypePhone
            ->employeePhones()
            ->search($search)->paginate();

        return new EmployeePhoneCollection($employeePhones);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTypePhone $refTypePhone
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefTypePhone $refTypePhone)
    {
        $this->authorize('create', EmployeePhone::class);

        $validated = $request->validate([
            'employee_id' => ['required', 'exists:employees,id'],
            'phone' => ['required', 'max:255', 'string'],
        ]);

        $employeePhone = $refTypePhone->employeePhones()->create($validated);

        return new EmployeePhoneResource($employeePhone);
    }
}
