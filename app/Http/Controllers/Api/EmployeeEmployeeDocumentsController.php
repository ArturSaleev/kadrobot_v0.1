<?php

namespace App\Http\Controllers\Api;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeDocumentResource;
use App\Http\Resources\EmployeeDocumentCollection;

class EmployeeEmployeeDocumentsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Employee $employee)
    {
        $this->authorize('view', $employee);

        $search = $request->get('search', '');

        $employeeDocuments = $employee
            ->employeeDocuments()
            ->search($search)->paginate();

        return new EmployeeDocumentCollection($employeeDocuments);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Employee $employee)
    {
        $this->authorize('create', EmployeeDocument::class);

        $validated = $request->validate([
            'ref_doc_type_id' => ['required', 'exists:ref_doc_types,id'],
            'ref_doc_place_id' => ['required', 'exists:ref_doc_places,id'],
            'doc_seria' => ['nullable', 'max:255', 'string'],
            'doc_num' => ['required', 'max:255', 'string'],
            'doc_date' => ['required', 'date'],
        ]);

        $employeeDocument = $employee->employeeDocuments()->create($validated);

        return new EmployeeDocumentResource($employeeDocument);
    }
}
