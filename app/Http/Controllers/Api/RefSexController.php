<?php

namespace App\Http\Controllers\Api;

use App\Models\RefSex;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefSexResource;
use App\Http\Resources\RefSexCollection;
use App\Http\Requests\RefSexStoreRequest;
use App\Http\Requests\RefSexUpdateRequest;

class RefSexController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefSex::class);

        $search = $request->get('search', '');

        $refSexes = RefSex::search($search)->paginate();

        return new RefSexCollection($refSexes);
    }

    /**
     * @param \App\Http\Requests\RefSexStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefSexStoreRequest $request)
    {
        $this->authorize('create', RefSex::class);

        $validated = $request->validated();

        $refSex = RefSex::create($validated);

        return new RefSexResource($refSex);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefSex $refSex
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefSex $refSex)
    {
        $this->authorize('view', $refSex);

        return new RefSexResource($refSex);
    }

    /**
     * @param \App\Http\Requests\RefSexUpdateRequest $request
     * @param \App\Models\RefSex $refSex
     * @return \Illuminate\Http\Response
     */
    public function update(RefSexUpdateRequest $request, RefSex $refSex)
    {
        $this->authorize('update', $refSex);

        $validated = $request->validated();

        $refSex->update($validated);

        return new RefSexResource($refSex);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefSex $refSex
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, RefSex $refSex)
    {
        $this->authorize('delete', $refSex);

        $refSex->delete();

        return response()->noContent();
    }
}
