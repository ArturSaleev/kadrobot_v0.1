<?php

namespace App\Http\Controllers\Api;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CuratorResource;
use App\Http\Resources\CuratorCollection;

class EmployeeCuratorsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Employee $employee)
    {
        $this->authorize('view', $employee);

        $search = $request->get('search', '');

        $curators = $employee
            ->curators()
            ->search($search)->paginate();

        return new CuratorCollection($curators);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Employee $employee)
    {
        $this->authorize('create', Curator::class);

        $validated = $request->validate([
            'department_id' => ['required', 'exists:departments,id'],
        ]);

        $curator = $employee->curators()->create($validated);

        return new CuratorResource($curator);
    }
}
