<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefFamilyState;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeFamilyResource;
use App\Http\Resources\EmployeeFamilyCollection;

class RefFamilyStateEmployeeFamiliesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefFamilyState $refFamilyState
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefFamilyState $refFamilyState)
    {
        $this->authorize('view', $refFamilyState);

        $search = $request->get('search', '');

        $employeeFamilies = $refFamilyState
            ->employeeFamilies()
            ->search($search)->paginate();

        return new EmployeeFamilyCollection($employeeFamilies);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefFamilyState $refFamilyState
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefFamilyState $refFamilyState)
    {
        $this->authorize('create', EmployeeFamily::class);

        $validated = $request->validate([
            'employee_id' => ['required', 'exists:employees,id'],
            'lastname' => ['required', 'max:255', 'string'],
            'firstname' => ['required', 'max:255', 'string'],
            'middlename' => ['required', 'max:255', 'string'],
            'birthday' => ['required', 'date'],
        ]);

        $employeeFamily = $refFamilyState
            ->employeeFamilies()
            ->create($validated);

        return new EmployeeFamilyResource($employeeFamily);
    }
}
