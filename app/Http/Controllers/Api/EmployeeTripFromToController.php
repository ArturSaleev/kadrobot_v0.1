<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\EmployeeTripFromTo;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeTripFromToResource;
use App\Http\Resources\EmployeeTripFromToCollection;
use App\Http\Requests\EmployeeTripFromToStoreRequest;
use App\Http\Requests\EmployeeTripFromToUpdateRequest;

class EmployeeTripFromToController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeeTripFromTo::class);

        $search = $request->get('search', '');

        $employeeTripFromTos = EmployeeTripFromTo::search($search)->paginate();

        return new EmployeeTripFromToCollection($employeeTripFromTos);
    }

    /**
     * @param \App\Http\Requests\EmployeeTripFromToStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeTripFromToStoreRequest $request)
    {
        $this->authorize('create', EmployeeTripFromTo::class);

        $validated = $request->validated();

        $employeeTripFromTo = EmployeeTripFromTo::create($validated);

        return new EmployeeTripFromToResource($employeeTripFromTo);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeTripFromTo $employeeTripFromTo
     * @return \Illuminate\Http\Response
     */
    public function show(
        Request $request,
        EmployeeTripFromTo $employeeTripFromTo
    ) {
        $this->authorize('view', $employeeTripFromTo);

        return new EmployeeTripFromToResource($employeeTripFromTo);
    }

    /**
     * @param \App\Http\Requests\EmployeeTripFromToUpdateRequest $request
     * @param \App\Models\EmployeeTripFromTo $employeeTripFromTo
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeeTripFromToUpdateRequest $request,
        EmployeeTripFromTo $employeeTripFromTo
    ) {
        $this->authorize('update', $employeeTripFromTo);

        $validated = $request->validated();

        $employeeTripFromTo->update($validated);

        return new EmployeeTripFromToResource($employeeTripFromTo);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeTripFromTo $employeeTripFromTo
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        Request $request,
        EmployeeTripFromTo $employeeTripFromTo
    ) {
        $this->authorize('delete', $employeeTripFromTo);

        $employeeTripFromTo->delete();

        return response()->noContent();
    }
}
