<?php

namespace App\Http\Controllers\Api;

use App\Models\RefAction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\T2CardResource;
use App\Http\Resources\T2CardCollection;

class RefActionT2CardsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefAction $refAction
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefAction $refAction)
    {
        $this->authorize('view', $refAction);

        $search = $request->get('search', '');

        $t2Cards = $refAction
            ->t2Cards()
            ->search($search)->paginate();

        return new T2CardCollection($t2Cards);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefAction $refAction
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefAction $refAction)
    {
        $this->authorize('create', T2Card::class);

        $validated = $request->validate([
            'company_id' => ['required', 'exists:companies,id'],
            'employee_id' => ['required', 'exists:employees,id'],
            'ref_branch_id' => ['required', 'exists:branches,id'],
            'ref_position_id' => ['required', 'exists:positions,id'],
        ]);

        $t2Card = $refAction->t2Cards()->create($validated);

        return new T2CardResource($t2Card);
    }
}
