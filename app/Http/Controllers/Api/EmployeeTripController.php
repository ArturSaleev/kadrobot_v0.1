<?php

namespace App\Http\Controllers\Api;

use App\Models\EmployeeTrip;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeTripResource;
use App\Http\Resources\EmployeeTripCollection;
use App\Http\Requests\EmployeeTripStoreRequest;
use App\Http\Requests\EmployeeTripUpdateRequest;

class EmployeeTripController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeeTrip::class);

        $search = $request->get('search', '');

        $employeeTrips = EmployeeTrip::search($search)->paginate();

        return new EmployeeTripCollection($employeeTrips);
    }

    /**
     * @param \App\Http\Requests\EmployeeTripStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeTripStoreRequest $request)
    {
        $this->authorize('create', EmployeeTrip::class);

        $validated = $request->validated();

        $employeeTrip = EmployeeTrip::create($validated);

        return new EmployeeTripResource($employeeTrip);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeTrip $employeeTrip
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EmployeeTrip $employeeTrip)
    {
        $this->authorize('view', $employeeTrip);

        return new EmployeeTripResource($employeeTrip);
    }

    /**
     * @param \App\Http\Requests\EmployeeTripUpdateRequest $request
     * @param \App\Models\EmployeeTrip $employeeTrip
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeeTripUpdateRequest $request,
        EmployeeTrip $employeeTrip
    ) {
        $this->authorize('update', $employeeTrip);

        $validated = $request->validated();

        $employeeTrip->update($validated);

        return new EmployeeTripResource($employeeTrip);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeTrip $employeeTrip
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, EmployeeTrip $employeeTrip)
    {
        $this->authorize('delete', $employeeTrip);

        $employeeTrip->delete();

        return response()->noContent();
    }
}
