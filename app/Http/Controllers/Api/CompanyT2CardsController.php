<?php

namespace App\Http\Controllers\Api;

use App\Models\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\T2CardResource;
use App\Http\Resources\T2CardCollection;

class CompanyT2CardsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Company $company
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Company $company)
    {
        $this->authorize('view', $company);

        $search = $request->get('search', '');

        $t2Cards = $company
            ->t2Cards()
            ->search($search)->paginate();

        return new T2CardCollection($t2Cards);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Company $company
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Company $company)
    {
        $this->authorize('create', T2Card::class);

        $validated = $request->validate([
            'employee_id' => ['required', 'exists:employees,id'],
            'ref_branch_id' => ['required', 'exists:branches,id'],
            'ref_action_id' => ['required', 'exists:ref_actions,id'],
            'ref_position_id' => ['required', 'exists:positions,id'],
        ]);

        $t2Card = $company->t2Cards()->create($validated);

        return new T2CardResource($t2Card);
    }
}
