<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefNationality;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefNationalityTranslationResource;
use App\Http\Resources\RefNationalityTranslationCollection;

class RefNationalityRefNationalityTranslationsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefNationality $refNationality
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefNationality $refNationality)
    {
        $this->authorize('view', $refNationality);

        $search = $request->get('search', '');

        $refNationalityTranslations = $refNationality
            ->refNationalityTranslations()
            ->search($search)->paginate();

        return new RefNationalityTranslationCollection(
            $refNationalityTranslations
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefNationality $refNationality
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefNationality $refNationality)
    {
        $this->authorize('create', RefNationalityTranslation::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'name' => ['required', 'max:255', 'string'],
        ]);

        $refNationalityTranslation = $refNationality
            ->refNationalityTranslations()
            ->create($validated);

        return new RefNationalityTranslationResource(
            $refNationalityTranslation
        );
    }
}
