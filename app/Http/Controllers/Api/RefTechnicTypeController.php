<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefTechnicType;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefTechnicTypeResource;
use App\Http\Resources\RefTechnicTypeCollection;
use App\Http\Requests\RefTechnicTypeStoreRequest;
use App\Http\Requests\RefTechnicTypeUpdateRequest;

class RefTechnicTypeController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefTechnicType::class);

        $search = $request->get('search', '');

        $refTechnicTypes = RefTechnicType::search($search)->paginate();

        return new RefTechnicTypeCollection($refTechnicTypes);
    }

    /**
     * @param \App\Http\Requests\RefTechnicTypeStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefTechnicTypeStoreRequest $request)
    {
        $this->authorize('create', RefTechnicType::class);

        $validated = $request->validated();

        $refTechnicType = RefTechnicType::create($validated);

        return new RefTechnicTypeResource($refTechnicType);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTechnicType $refTechnicType
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefTechnicType $refTechnicType)
    {
        $this->authorize('view', $refTechnicType);

        return new RefTechnicTypeResource($refTechnicType);
    }

    /**
     * @param \App\Http\Requests\RefTechnicTypeUpdateRequest $request
     * @param \App\Models\RefTechnicType $refTechnicType
     * @return \Illuminate\Http\Response
     */
    public function update(
        RefTechnicTypeUpdateRequest $request,
        RefTechnicType $refTechnicType
    ) {
        $this->authorize('update', $refTechnicType);

        $validated = $request->validated();

        $refTechnicType->update($validated);

        return new RefTechnicTypeResource($refTechnicType);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTechnicType $refTechnicType
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, RefTechnicType $refTechnicType)
    {
        $this->authorize('delete', $refTechnicType);

        $refTechnicType->delete();

        return response()->noContent();
    }
}
