<?php

namespace App\Http\Controllers\Api;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeePhoneResource;
use App\Http\Resources\EmployeePhoneCollection;

class EmployeeEmployeePhonesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Employee $employee)
    {
        $this->authorize('view', $employee);

        $search = $request->get('search', '');

        $employeePhones = $employee
            ->employeePhones()
            ->search($search)->paginate();

        return new EmployeePhoneCollection($employeePhones);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Employee $employee)
    {
        $this->authorize('create', EmployeePhone::class);

        $validated = $request->validate([
            'ref_type_phone_id' => ['required', 'exists:ref_type_phones,id'],
            'phone' => ['required', 'max:255', 'string'],
        ]);

        $employeePhone = $employee->employeePhones()->create($validated);

        return new EmployeePhoneResource($employeePhone);
    }
}
