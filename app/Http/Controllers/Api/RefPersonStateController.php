<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefPersonState;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefPersonStateResource;
use App\Http\Resources\RefPersonStateCollection;
use App\Http\Requests\RefPersonStateStoreRequest;
use App\Http\Requests\RefPersonStateUpdateRequest;

class RefPersonStateController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefPersonState::class);

        $search = $request->get('search', '');

        $refPersonStates = RefPersonState::search($search)->paginate();

        return new RefPersonStateCollection($refPersonStates);
    }

    /**
     * @param \App\Http\Requests\RefPersonStateStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefPersonStateStoreRequest $request)
    {
        $this->authorize('create', RefPersonState::class);

        $validated = $request->validated();

        $refPersonState = RefPersonState::create($validated);

        return new RefPersonStateResource($refPersonState);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefPersonState $refPersonState
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefPersonState $refPersonState)
    {
        $this->authorize('view', $refPersonState);

        return new RefPersonStateResource($refPersonState);
    }

    /**
     * @param \App\Http\Requests\RefPersonStateUpdateRequest $request
     * @param \App\Models\RefPersonState $refPersonState
     * @return \Illuminate\Http\Response
     */
    public function update(
        RefPersonStateUpdateRequest $request,
        RefPersonState $refPersonState
    ) {
        $this->authorize('update', $refPersonState);

        $validated = $request->validated();

        $refPersonState->update($validated);

        return new RefPersonStateResource($refPersonState);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefPersonState $refPersonState
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, RefPersonState $refPersonState)
    {
        $this->authorize('delete', $refPersonState);

        $refPersonState->delete();

        return response()->noContent();
    }
}
