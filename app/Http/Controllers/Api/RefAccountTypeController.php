<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefAccountType;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefAccountTypeResource;
use App\Http\Resources\RefAccountTypeCollection;
use App\Http\Requests\RefAccountTypeStoreRequest;
use App\Http\Requests\RefAccountTypeUpdateRequest;

class RefAccountTypeController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefAccountType::class);

        $search = $request->get('search', '');

        $refAccountTypes = RefAccountType::search($search)->paginate();

        return new RefAccountTypeCollection($refAccountTypes);
    }

    /**
     * @param \App\Http\Requests\RefAccountTypeStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefAccountTypeStoreRequest $request)
    {
        $this->authorize('create', RefAccountType::class);

        $validated = $request->validated();

        $refAccountType = RefAccountType::create($validated);

        return new RefAccountTypeResource($refAccountType);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefAccountType $refAccountType
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefAccountType $refAccountType)
    {
        $this->authorize('view', $refAccountType);

        return new RefAccountTypeResource($refAccountType);
    }

    /**
     * @param \App\Http\Requests\RefAccountTypeUpdateRequest $request
     * @param \App\Models\RefAccountType $refAccountType
     * @return \Illuminate\Http\Response
     */
    public function update(
        RefAccountTypeUpdateRequest $request,
        RefAccountType $refAccountType
    ) {
        $this->authorize('update', $refAccountType);

        $validated = $request->validated();

        $refAccountType->update($validated);

        return new RefAccountTypeResource($refAccountType);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefAccountType $refAccountType
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, RefAccountType $refAccountType)
    {
        $this->authorize('delete', $refAccountType);

        $refAccountType->delete();

        return response()->noContent();
    }
}
