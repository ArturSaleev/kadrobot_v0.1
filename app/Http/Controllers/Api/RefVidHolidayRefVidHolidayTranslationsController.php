<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefVidHoliday;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefVidHolidayTranslationResource;
use App\Http\Resources\RefVidHolidayTranslationCollection;

class RefVidHolidayRefVidHolidayTranslationsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefVidHoliday $refVidHoliday
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefVidHoliday $refVidHoliday)
    {
        $this->authorize('view', $refVidHoliday);

        $search = $request->get('search', '');

        $refVidHolidayTranslations = $refVidHoliday
            ->refVidHolidayTranslates()
            ->search($search)->paginate();

        return new RefVidHolidayTranslationCollection(
            $refVidHolidayTranslations
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefVidHoliday $refVidHoliday
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefVidHoliday $refVidHoliday)
    {
        $this->authorize('create', RefVidHolidayTranslation::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'name' => ['required', 'max:255', 'string'],
        ]);

        $refVidHolidayTranslation = $refVidHoliday
            ->refVidHolidayTranslates()
            ->create($validated);

        return new RefVidHolidayTranslationResource($refVidHolidayTranslation);
    }
}
