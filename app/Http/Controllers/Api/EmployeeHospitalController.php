<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\EmployeeHospital;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeHospitalResource;
use App\Http\Resources\EmployeeHospitalCollection;
use App\Http\Requests\EmployeeHospitalStoreRequest;
use App\Http\Requests\EmployeeHospitalUpdateRequest;

class EmployeeHospitalController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeeHospital::class);

        $search = $request->get('search', '');

        $employeeHospitals = EmployeeHospital::search($search)->paginate();

        return new EmployeeHospitalCollection($employeeHospitals);
    }

    /**
     * @param \App\Http\Requests\EmployeeHospitalStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeHospitalStoreRequest $request)
    {
        $this->authorize('create', EmployeeHospital::class);

        $validated = $request->validated();

        $employeeHospital = EmployeeHospital::create($validated);

        return new EmployeeHospitalResource($employeeHospital);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeHospital $employeeHospital
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EmployeeHospital $employeeHospital)
    {
        $this->authorize('view', $employeeHospital);

        return new EmployeeHospitalResource($employeeHospital);
    }

    /**
     * @param \App\Http\Requests\EmployeeHospitalUpdateRequest $request
     * @param \App\Models\EmployeeHospital $employeeHospital
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeeHospitalUpdateRequest $request,
        EmployeeHospital $employeeHospital
    ) {
        $this->authorize('update', $employeeHospital);

        $validated = $request->validated();

        $employeeHospital->update($validated);

        return new EmployeeHospitalResource($employeeHospital);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeHospital $employeeHospital
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        Request $request,
        EmployeeHospital $employeeHospital
    ) {
        $this->authorize('delete', $employeeHospital);

        $employeeHospital->delete();

        return response()->noContent();
    }
}
