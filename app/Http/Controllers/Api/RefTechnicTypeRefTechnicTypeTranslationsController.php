<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefTechnicType;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefTechnicTypeTranslationResource;
use App\Http\Resources\RefTechnicTypeTranslationCollection;

class RefTechnicTypeRefTechnicTypeTranslationsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTechnicType $refTechnicType
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefTechnicType $refTechnicType)
    {
        $this->authorize('view', $refTechnicType);

        $search = $request->get('search', '');

        $refTechnicTypeTranslations = $refTechnicType
            ->refTechnicTypeTranslations()
            ->search($search)->paginate();

        return new RefTechnicTypeTranslationCollection(
            $refTechnicTypeTranslations
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTechnicType $refTechnicType
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefTechnicType $refTechnicType)
    {
        $this->authorize('create', RefTechnicTypeTranslation::class);

        $validated = $request->validate([
            'name' => ['required', 'max:255', 'string'],
            'locale' => ['required', 'max:8', 'string'],
        ]);

        $refTechnicTypeTranslation = $refTechnicType
            ->refTechnicTypeTranslations()
            ->create($validated);

        return new RefTechnicTypeTranslationResource(
            $refTechnicTypeTranslation
        );
    }
}
