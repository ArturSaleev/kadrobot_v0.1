<?php

namespace App\Http\Controllers\Api;

use App\Models\EmployeeTrip;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeTripFromToResource;
use App\Http\Resources\EmployeeTripFromToCollection;

class EmployeeTripEmployeeTripFromTosController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeTrip $employeeTrip
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, EmployeeTrip $employeeTrip)
    {
        $this->authorize('view', $employeeTrip);

        $search = $request->get('search', '');

        $employeeTripFromTos = $employeeTrip
            ->employeeTripFromTos()
            ->search($search)->paginate();

        return new EmployeeTripFromToCollection($employeeTripFromTos);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeTrip $employeeTrip
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, EmployeeTrip $employeeTrip)
    {
        $this->authorize('create', EmployeeTripFromTo::class);

        $validated = $request->validate([
            'from_place' => ['required', 'max:255', 'string'],
            'to_place' => ['required', 'max:255', 'string'],
            'ref_transport_trip_id' => [
                'required',
                'exists:ref_transport_trips,id',
            ],
        ]);

        $employeeTripFromTo = $employeeTrip
            ->employeeTripFromTos()
            ->create($validated);

        return new EmployeeTripFromToResource($employeeTripFromTo);
    }
}
