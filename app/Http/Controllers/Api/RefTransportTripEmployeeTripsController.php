<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefTransportTrip;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeTripResource;
use App\Http\Resources\EmployeeTripCollection;

class RefTransportTripEmployeeTripsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTransportTrip $refTransportTrip
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefTransportTrip $refTransportTrip)
    {
        $this->authorize('view', $refTransportTrip);

        $search = $request->get('search', '');

        $employeeTrips = $refTransportTrip
            ->employeeTrips()
            ->search($search)->paginate();

        return new EmployeeTripCollection($employeeTrips);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTransportTrip $refTransportTrip
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefTransportTrip $refTransportTrip)
    {
        $this->authorize('create', EmployeeTrip::class);

        $validated = $request->validate([
            'employee_id' => ['required', 'exists:employees,id'],
            'date_begin' => ['required', 'date'],
            'date_end' => ['required', 'date'],
            'cnt_days' => ['required', 'numeric'],
            'order_num' => ['required', 'max:255', 'string'],
            'order_date' => ['required', 'date'],
        ]);

        $employeeTrip = $refTransportTrip->employeeTrips()->create($validated);

        return new EmployeeTripResource($employeeTrip);
    }
}
