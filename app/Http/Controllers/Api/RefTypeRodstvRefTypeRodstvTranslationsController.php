<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefTypeRodstv;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefTypeRodstvTranslationResource;
use App\Http\Resources\RefTypeRodstvTranslationCollection;

class RefTypeRodstvRefTypeRodstvTranslationsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTypeRodstv $refTypeRodstv
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefTypeRodstv $refTypeRodstv)
    {
        $this->authorize('view', $refTypeRodstv);

        $search = $request->get('search', '');

        $refTypeRodstvTranslations = $refTypeRodstv
            ->refTypeRodstvTranslations()
            ->search($search)->paginate();

        return new RefTypeRodstvTranslationCollection(
            $refTypeRodstvTranslations
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefTypeRodstv $refTypeRodstv
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefTypeRodstv $refTypeRodstv)
    {
        $this->authorize('create', RefTypeRodstvTranslation::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'name' => ['required', 'max:255', 'string'],
        ]);

        $refTypeRodstvTranslation = $refTypeRodstv
            ->refTypeRodstvTranslations()
            ->create($validated);

        return new RefTypeRodstvTranslationResource($refTypeRodstvTranslation);
    }
}
