<?php

namespace App\Http\Controllers\Api;

use App\Models\Branch;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\BranchAddresesResource;
use App\Http\Resources\BranchAddresesCollection;

class BranchAllBranchAddresesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Branch $branch
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Branch $branch)
    {
        $this->authorize('view', $branch);

        $search = $request->get('search', '');

        $allBranchAddreses = $branch
            ->branchAddreses()
            ->search($search)->paginate();

        return new BranchAddresesCollection($allBranchAddreses);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Branch $branch
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Branch $branch)
    {
        $this->authorize('create', BranchAddreses::class);

        $validated = $request->validate([
            'ref_address_type_id' => [
                'required',
                'exists:ref_address_types,id',
            ],
        ]);

        $branchAddreses = $branch->branchAddreses()->create($validated);

        return new BranchAddresesResource($branchAddreses);
    }
}
