<?php

namespace App\Http\Controllers\Api;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeFamilyResource;
use App\Http\Resources\EmployeeFamilyCollection;

class EmployeeEmployeeFamiliesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Employee $employee)
    {
        $this->authorize('view', $employee);

        $search = $request->get('search', '');

        $employeeFamilies = $employee
            ->employeeFamilies()
            ->search($search)->paginate();

        return new EmployeeFamilyCollection($employeeFamilies);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Employee $employee)
    {
        $this->authorize('create', EmployeeFamily::class);

        $validated = $request->validate([
            'lastname' => ['required', 'max:255', 'string'],
            'firstname' => ['required', 'max:255', 'string'],
            'middlename' => ['required', 'max:255', 'string'],
            'birthday' => ['required', 'date'],
            'ref_family_state_id' => [
                'required',
                'exists:ref_family_states,id',
            ],
        ]);

        $employeeFamily = $employee->employeeFamilies()->create($validated);

        return new EmployeeFamilyResource($employeeFamily);
    }
}
