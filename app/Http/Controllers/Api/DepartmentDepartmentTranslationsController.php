<?php

namespace App\Http\Controllers\Api;

use App\Models\Department;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\DepartmentTranslationResource;
use App\Http\Resources\DepartmentTranslationCollection;

class DepartmentDepartmentTranslationsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Department $department
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Department $department)
    {
        $this->authorize('view', $department);

        $search = $request->get('search', '');

        $departmentTranslations = $department
            ->departmentTranstions()
            ->search($search)->paginate();

        return new DepartmentTranslationCollection($departmentTranslations);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Department $department
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Department $department)
    {
        $this->authorize('create', DepartmentTranslation::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'name' => ['required', 'max:255', 'string'],
        ]);

        $departmentTranslation = $department
            ->departmentTranstions()
            ->create($validated);

        return new DepartmentTranslationResource($departmentTranslation);
    }
}
