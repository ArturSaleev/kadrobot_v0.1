<?php

namespace App\Http\Controllers\Api;

use App\Models\Branch;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\T2CardResource;
use App\Http\Resources\T2CardCollection;

class BranchT2CardsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Branch $branch
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Branch $branch)
    {
        $this->authorize('view', $branch);

        $search = $request->get('search', '');

        $t2Cards = $branch
            ->t2Cards()
            ->search($search)->paginate();

        return new T2CardCollection($t2Cards);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Branch $branch
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Branch $branch)
    {
        $this->authorize('create', T2Card::class);

        $validated = $request->validate([
            'company_id' => ['required', 'exists:companies,id'],
            'employee_id' => ['required', 'exists:employees,id'],
            'ref_action_id' => ['required', 'exists:ref_actions,id'],
            'ref_position_id' => ['required', 'exists:positions,id'],
        ]);

        $t2Card = $branch->t2Cards()->create($validated);

        return new T2CardResource($t2Card);
    }
}
