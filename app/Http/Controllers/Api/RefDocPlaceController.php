<?php

namespace App\Http\Controllers\Api;

use App\Models\RefDocPlace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefDocPlaceResource;
use App\Http\Resources\RefDocPlaceCollection;
use App\Http\Requests\RefDocPlaceStoreRequest;
use App\Http\Requests\RefDocPlaceUpdateRequest;

class RefDocPlaceController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefDocPlace::class);

        $search = $request->get('search', '');

        $refDocPlaces = RefDocPlace::search($search)->paginate();

        return new RefDocPlaceCollection($refDocPlaces);
    }

    /**
     * @param \App\Http\Requests\RefDocPlaceStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefDocPlaceStoreRequest $request)
    {
        $this->authorize('create', RefDocPlace::class);

        $validated = $request->validated();

        $refDocPlace = RefDocPlace::create($validated);

        return new RefDocPlaceResource($refDocPlace);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefDocPlace $refDocPlace
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefDocPlace $refDocPlace)
    {
        $this->authorize('view', $refDocPlace);

        return new RefDocPlaceResource($refDocPlace);
    }

    /**
     * @param \App\Http\Requests\RefDocPlaceUpdateRequest $request
     * @param \App\Models\RefDocPlace $refDocPlace
     * @return \Illuminate\Http\Response
     */
    public function update(
        RefDocPlaceUpdateRequest $request,
        RefDocPlace $refDocPlace
    ) {
        $this->authorize('update', $refDocPlace);

        $validated = $request->validated();

        $refDocPlace->update($validated);

        return new RefDocPlaceResource($refDocPlace);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefDocPlace $refDocPlace
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, RefDocPlace $refDocPlace)
    {
        $this->authorize('delete', $refDocPlace);

        $refDocPlace->delete();

        return response()->noContent();
    }
}
