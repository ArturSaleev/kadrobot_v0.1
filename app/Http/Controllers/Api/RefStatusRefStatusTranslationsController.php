<?php

namespace App\Http\Controllers\Api;

use App\Models\RefStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefStatusTranslationResource;
use App\Http\Resources\RefStatusTranslationCollection;

class RefStatusRefStatusTranslationsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefStatus $refStatus
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefStatus $refStatus)
    {
        $this->authorize('view', $refStatus);

        $search = $request->get('search', '');

        $refStatusTranslations = $refStatus
            ->refStatusTranslations()
            ->search($search)->paginate();

        return new RefStatusTranslationCollection($refStatusTranslations);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefStatus $refStatus
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefStatus $refStatus)
    {
        $this->authorize('create', RefStatusTranslation::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'name' => ['required', 'max:255', 'string'],
        ]);

        $refStatusTranslation = $refStatus
            ->refStatusTranslations()
            ->create($validated);

        return new RefStatusTranslationResource($refStatusTranslation);
    }
}
