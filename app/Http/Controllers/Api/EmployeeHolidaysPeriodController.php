<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EmployeeHolidaysPeriod;
use App\Http\Resources\EmployeeHolidaysPeriodResource;
use App\Http\Resources\EmployeeHolidaysPeriodCollection;
use App\Http\Requests\EmployeeHolidaysPeriodStoreRequest;
use App\Http\Requests\EmployeeHolidaysPeriodUpdateRequest;

class EmployeeHolidaysPeriodController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeeHolidaysPeriod::class);

        $search = $request->get('search', '');

        $employeeHolidaysPeriods = EmployeeHolidaysPeriod::search($search)->paginate();

        return new EmployeeHolidaysPeriodCollection($employeeHolidaysPeriods);
    }

    /**
     * @param \App\Http\Requests\EmployeeHolidaysPeriodStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeHolidaysPeriodStoreRequest $request)
    {
        $this->authorize('create', EmployeeHolidaysPeriod::class);

        $validated = $request->validated();

        $employeeHolidaysPeriod = EmployeeHolidaysPeriod::create($validated);

        return new EmployeeHolidaysPeriodResource($employeeHolidaysPeriod);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeHolidaysPeriod $employeeHolidaysPeriod
     * @return \Illuminate\Http\Response
     */
    public function show(
        Request $request,
        EmployeeHolidaysPeriod $employeeHolidaysPeriod
    ) {
        $this->authorize('view', $employeeHolidaysPeriod);

        return new EmployeeHolidaysPeriodResource($employeeHolidaysPeriod);
    }

    /**
     * @param \App\Http\Requests\EmployeeHolidaysPeriodUpdateRequest $request
     * @param \App\Models\EmployeeHolidaysPeriod $employeeHolidaysPeriod
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeeHolidaysPeriodUpdateRequest $request,
        EmployeeHolidaysPeriod $employeeHolidaysPeriod
    ) {
        $this->authorize('update', $employeeHolidaysPeriod);

        $validated = $request->validated();

        $employeeHolidaysPeriod->update($validated);

        return new EmployeeHolidaysPeriodResource($employeeHolidaysPeriod);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeHolidaysPeriod $employeeHolidaysPeriod
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        Request $request,
        EmployeeHolidaysPeriod $employeeHolidaysPeriod
    ) {
        $this->authorize('delete', $employeeHolidaysPeriod);

        $employeeHolidaysPeriod->delete();

        return response()->noContent();
    }
}
