<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefPersonState;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefPersonStateTranslationResource;
use App\Http\Resources\RefPersonStateTranslationCollection;

class RefPersonStateRefPersonStateTranslationsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefPersonState $refPersonState
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefPersonState $refPersonState)
    {
        $this->authorize('view', $refPersonState);

        $search = $request->get('search', '');

        $refPersonStateTranslations = $refPersonState
            ->refPersonStateTranstions()
            ->search($search)->paginate();

        return new RefPersonStateTranslationCollection(
            $refPersonStateTranslations
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefPersonState $refPersonState
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefPersonState $refPersonState)
    {
        $this->authorize('create', RefPersonStateTranslation::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'name' => ['required', 'max:255', 'string'],
        ]);

        $refPersonStateTranslation = $refPersonState
            ->refPersonStateTranstions()
            ->create($validated);

        return new RefPersonStateTranslationResource(
            $refPersonStateTranslation
        );
    }
}
