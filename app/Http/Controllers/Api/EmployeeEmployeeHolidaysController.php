<?php

namespace App\Http\Controllers\Api;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeHolidayResource;
use App\Http\Resources\EmployeeHolidayCollection;

class EmployeeEmployeeHolidaysController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Employee $employee)
    {
        $this->authorize('view', $employee);

        $search = $request->get('search', '');

        $employeeHolidays = $employee
            ->employeeHolidays()
            ->search($search)->paginate();

        return new EmployeeHolidayCollection($employeeHolidays);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Employee $employee)
    {
        $this->authorize('create', EmployeeHoliday::class);

        $validated = $request->validate([
            'date_begin' => ['nullable', 'date'],
            'date_end' => ['nullable', 'date'],
            'cnt_days' => ['nullable', 'numeric'],
            'period_begin' => ['nullable', 'date'],
            'period_end' => ['nullable', 'date'],
            'order_num' => ['nullable', 'max:255', 'string'],
            'order_date' => ['nullable', 'date'],
            'ref_vid_holiday_id' => ['required', 'exists:ref_vid_holidays,id'],
            'deligate' => ['nullable', 'numeric'],
            'deligate_emp_id' => ['nullable', 'max:255'],
            'doc_content' => ['required', 'max:255', 'string'],
        ]);

        $employeeHoliday = $employee->employeeHolidays()->create($validated);

        return new EmployeeHolidayResource($employeeHoliday);
    }
}
