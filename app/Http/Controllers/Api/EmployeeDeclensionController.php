<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\EmployeeDeclension;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeDeclensionResource;
use App\Http\Resources\EmployeeDeclensionCollection;
use App\Http\Requests\EmployeeDeclensionStoreRequest;
use App\Http\Requests\EmployeeDeclensionUpdateRequest;

class EmployeeDeclensionController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeeDeclension::class);

        $search = $request->get('search', '');

        $employeeDeclensions = EmployeeDeclension::search($search)->paginate();

        return new EmployeeDeclensionCollection($employeeDeclensions);
    }

    /**
     * @param \App\Http\Requests\EmployeeDeclensionStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeDeclensionStoreRequest $request)
    {
        $this->authorize('create', EmployeeDeclension::class);

        $validated = $request->validated();

        $employeeDeclension = EmployeeDeclension::create($validated);

        return new EmployeeDeclensionResource($employeeDeclension);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeDeclension $employeeDeclension
     * @return \Illuminate\Http\Response
     */
    public function show(
        Request $request,
        EmployeeDeclension $employeeDeclension
    ) {
        $this->authorize('view', $employeeDeclension);

        return new EmployeeDeclensionResource($employeeDeclension);
    }

    /**
     * @param \App\Http\Requests\EmployeeDeclensionUpdateRequest $request
     * @param \App\Models\EmployeeDeclension $employeeDeclension
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeeDeclensionUpdateRequest $request,
        EmployeeDeclension $employeeDeclension
    ) {
        $this->authorize('update', $employeeDeclension);

        $validated = $request->validated();

        $employeeDeclension->update($validated);

        return new EmployeeDeclensionResource($employeeDeclension);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeDeclension $employeeDeclension
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        Request $request,
        EmployeeDeclension $employeeDeclension
    ) {
        $this->authorize('delete', $employeeDeclension);

        $employeeDeclension->delete();

        return response()->noContent();
    }
}
