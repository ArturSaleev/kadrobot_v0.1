<?php

namespace App\Http\Controllers\Api;

use App\Models\RefCountry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefCountryResource;
use App\Http\Resources\RefCountryCollection;
use App\Http\Requests\RefCountryStoreRequest;
use App\Http\Requests\RefCountryUpdateRequest;

class RefCountryController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefCountry::class);

        $search = $request->get('search', '');

        $refCountries = RefCountry::search($search)->paginate();

        return new RefCountryCollection($refCountries);
    }

    /**
     * @param \App\Http\Requests\RefCountryStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefCountryStoreRequest $request)
    {
        $this->authorize('create', RefCountry::class);

        $validated = $request->validated();

        $refCountry = RefCountry::create($validated);

        return new RefCountryResource($refCountry);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefCountry $refCountry
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefCountry $refCountry)
    {
        $this->authorize('view', $refCountry);

        return new RefCountryResource($refCountry);
    }

    /**
     * @param \App\Http\Requests\RefCountryUpdateRequest $request
     * @param \App\Models\RefCountry $refCountry
     * @return \Illuminate\Http\Response
     */
    public function update(
        RefCountryUpdateRequest $request,
        RefCountry $refCountry
    ) {
        $this->authorize('update', $refCountry);

        $validated = $request->validated();

        $refCountry->update($validated);

        return new RefCountryResource($refCountry);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefCountry $refCountry
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, RefCountry $refCountry)
    {
        $this->authorize('delete', $refCountry);

        $refCountry->delete();

        return response()->noContent();
    }
}
