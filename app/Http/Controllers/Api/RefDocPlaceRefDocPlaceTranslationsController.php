<?php

namespace App\Http\Controllers\Api;

use App\Models\RefDocPlace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefDocPlaceTranslationResource;
use App\Http\Resources\RefDocPlaceTranslationCollection;

class RefDocPlaceRefDocPlaceTranslationsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefDocPlace $refDocPlace
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RefDocPlace $refDocPlace)
    {
        $this->authorize('view', $refDocPlace);

        $search = $request->get('search', '');

        $refDocPlaceTranslations = $refDocPlace
            ->refDocPlaceTranlations()
            ->search($search)->paginate();

        return new RefDocPlaceTranslationCollection($refDocPlaceTranslations);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefDocPlace $refDocPlace
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RefDocPlace $refDocPlace)
    {
        $this->authorize('create', RefDocPlaceTranslation::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'name' => ['required', 'max:255', 'string'],
        ]);

        $refDocPlaceTranslation = $refDocPlace
            ->refDocPlaceTranlations()
            ->create($validated);

        return new RefDocPlaceTranslationResource($refDocPlaceTranslation);
    }
}
