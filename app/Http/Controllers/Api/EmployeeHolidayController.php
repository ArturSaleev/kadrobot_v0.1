<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\EmployeeHoliday;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeHolidayResource;
use App\Http\Resources\EmployeeHolidayCollection;
use App\Http\Requests\EmployeeHolidayStoreRequest;
use App\Http\Requests\EmployeeHolidayUpdateRequest;

class EmployeeHolidayController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeeHoliday::class);

        $search = $request->get('search', '');

        $employeeHolidays = EmployeeHoliday::search($search)->paginate();

        return new EmployeeHolidayCollection($employeeHolidays);
    }

    /**
     * @param \App\Http\Requests\EmployeeHolidayStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeHolidayStoreRequest $request)
    {
        $this->authorize('create', EmployeeHoliday::class);

        $validated = $request->validated();

        $employeeHoliday = EmployeeHoliday::create($validated);

        return new EmployeeHolidayResource($employeeHoliday);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeHoliday $employeeHoliday
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EmployeeHoliday $employeeHoliday)
    {
        $this->authorize('view', $employeeHoliday);

        return new EmployeeHolidayResource($employeeHoliday);
    }

    /**
     * @param \App\Http\Requests\EmployeeHolidayUpdateRequest $request
     * @param \App\Models\EmployeeHoliday $employeeHoliday
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeeHolidayUpdateRequest $request,
        EmployeeHoliday $employeeHoliday
    ) {
        $this->authorize('update', $employeeHoliday);

        $validated = $request->validated();

        $employeeHoliday->update($validated);

        return new EmployeeHolidayResource($employeeHoliday);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeHoliday $employeeHoliday
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, EmployeeHoliday $employeeHoliday)
    {
        $this->authorize('delete', $employeeHoliday);

        $employeeHoliday->delete();

        return response()->noContent();
    }
}
