<?php

namespace App\Http\Controllers\Api;

use App\Models\BranchPhone;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\BranchPhoneResource;
use App\Http\Resources\BranchPhoneCollection;
use App\Http\Requests\BranchPhoneStoreRequest;
use App\Http\Requests\BranchPhoneUpdateRequest;

class BranchPhoneController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', BranchPhone::class);

        $search = $request->get('search', '');

        $branchPhones = BranchPhone::search($search)->paginate();

        return new BranchPhoneCollection($branchPhones);
    }

    /**
     * @param \App\Http\Requests\BranchPhoneStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(BranchPhoneStoreRequest $request)
    {
        $this->authorize('create', BranchPhone::class);

        $validated = $request->validated();

        $branchPhone = BranchPhone::create($validated);

        return new BranchPhoneResource($branchPhone);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\BranchPhone $branchPhone
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, BranchPhone $branchPhone)
    {
        $this->authorize('view', $branchPhone);

        return new BranchPhoneResource($branchPhone);
    }

    /**
     * @param \App\Http\Requests\BranchPhoneUpdateRequest $request
     * @param \App\Models\BranchPhone $branchPhone
     * @return \Illuminate\Http\Response
     */
    public function update(
        BranchPhoneUpdateRequest $request,
        BranchPhone $branchPhone
    ) {
        $this->authorize('update', $branchPhone);

        $validated = $request->validated();

        $branchPhone->update($validated);

        return new BranchPhoneResource($branchPhone);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\BranchPhone $branchPhone
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, BranchPhone $branchPhone)
    {
        $this->authorize('delete', $branchPhone);

        $branchPhone->delete();

        return response()->noContent();
    }
}
