<?php

namespace App\Http\Controllers\Api;

use App\Models\Position;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PositionDeclensionResource;
use App\Http\Resources\PositionDeclensionCollection;

class PositionPositionDeclensionsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Position $position
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Position $position)
    {
        $this->authorize('view', $position);

        $search = $request->get('search', '');

        $positionDeclensions = $position
            ->positionDeclensions()
            ->search($search)->paginate();

        return new PositionDeclensionCollection($positionDeclensions);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Position $position
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Position $position)
    {
        $this->authorize('create', PositionDeclension::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'value' => ['required', 'max:255', 'string'],
            'case_type' => ['nullable', 'max:255', 'string'],
            'type_description' => ['nullable', 'max:255', 'string'],
        ]);

        $positionDeclension = $position
            ->positionDeclensions()
            ->create($validated);

        return new PositionDeclensionResource($positionDeclension);
    }
}
