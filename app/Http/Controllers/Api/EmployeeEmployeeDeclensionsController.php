<?php

namespace App\Http\Controllers\Api;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeDeclensionResource;
use App\Http\Resources\EmployeeDeclensionCollection;

class EmployeeEmployeeDeclensionsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Employee $employee)
    {
        $this->authorize('view', $employee);

        $search = $request->get('search', '');

        $employeeDeclensions = $employee
            ->employeeDeclensions()
            ->search($search)->paginate();

        return new EmployeeDeclensionCollection($employeeDeclensions);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Employee $employee)
    {
        $this->authorize('create', EmployeeDeclension::class);

        $validated = $request->validate([
            'locale' => ['required', 'max:8', 'string'],
            'lastname' => ['required', 'max:255', 'string'],
            'firstname' => ['required', 'max:255', 'string'],
            'middlename' => ['nullable', 'max:255', 'string'],
            'case_type' => ['nullable', 'max:255', 'string'],
        ]);

        $employeeDeclension = $employee
            ->employeeDeclensions()
            ->create($validated);

        return new EmployeeDeclensionResource($employeeDeclension);
    }
}
