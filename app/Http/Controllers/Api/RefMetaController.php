<?php

namespace App\Http\Controllers\Api;

use App\Models\RefMeta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefMetaResource;
use App\Http\Resources\RefMetaCollection;
use App\Http\Requests\RefMetaStoreRequest;
use App\Http\Requests\RefMetaUpdateRequest;

class RefMetaController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefMeta::class);

        $search = $request->get('search', '');

        $refMetas = RefMeta::search($search)->paginate();

        return new RefMetaCollection($refMetas);
    }

    /**
     * @param \App\Http\Requests\RefMetaStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefMetaStoreRequest $request)
    {
        $this->authorize('create', RefMeta::class);

        $validated = $request->validated();

        $refMeta = RefMeta::create($validated);

        return new RefMetaResource($refMeta);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefMeta $refMeta
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefMeta $refMeta)
    {
        $this->authorize('view', $refMeta);

        return new RefMetaResource($refMeta);
    }

    /**
     * @param \App\Http\Requests\RefMetaUpdateRequest $request
     * @param \App\Models\RefMeta $refMeta
     * @return \Illuminate\Http\Response
     */
    public function update(RefMetaUpdateRequest $request, RefMeta $refMeta)
    {
        $this->authorize('update', $refMeta);

        $validated = $request->validated();

        $refMeta->update($validated);

        return new RefMetaResource($refMeta);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefMeta $refMeta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, RefMeta $refMeta)
    {
        $this->authorize('delete', $refMeta);

        $refMeta->delete();

        return response()->noContent();
    }
}
