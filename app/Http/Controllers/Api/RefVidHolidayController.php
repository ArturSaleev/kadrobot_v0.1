<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefVidHoliday;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefVidHolidayResource;
use App\Http\Resources\RefVidHolidayCollection;
use App\Http\Requests\RefVidHolidayStoreRequest;
use App\Http\Requests\RefVidHolidayUpdateRequest;

class RefVidHolidayController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefVidHoliday::class);

        $search = $request->get('search', '');

        $refVidHolidays = RefVidHoliday::search($search)->paginate();

        return new RefVidHolidayCollection($refVidHolidays);
    }

    /**
     * @param \App\Http\Requests\RefVidHolidayStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefVidHolidayStoreRequest $request)
    {
        $this->authorize('create', RefVidHoliday::class);

        $validated = $request->validated();

        $refVidHoliday = RefVidHoliday::create($validated);

        return new RefVidHolidayResource($refVidHoliday);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefVidHoliday $refVidHoliday
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefVidHoliday $refVidHoliday)
    {
        $this->authorize('view', $refVidHoliday);

        return new RefVidHolidayResource($refVidHoliday);
    }

    /**
     * @param \App\Http\Requests\RefVidHolidayUpdateRequest $request
     * @param \App\Models\RefVidHoliday $refVidHoliday
     * @return \Illuminate\Http\Response
     */
    public function update(
        RefVidHolidayUpdateRequest $request,
        RefVidHoliday $refVidHoliday
    ) {
        $this->authorize('update', $refVidHoliday);

        $validated = $request->validated();

        $refVidHoliday->update($validated);

        return new RefVidHolidayResource($refVidHoliday);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefVidHoliday $refVidHoliday
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, RefVidHoliday $refVidHoliday)
    {
        $this->authorize('delete', $refVidHoliday);

        $refVidHoliday->delete();

        return response()->noContent();
    }
}
