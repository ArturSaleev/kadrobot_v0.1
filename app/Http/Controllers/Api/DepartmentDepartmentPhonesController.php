<?php

namespace App\Http\Controllers\Api;

use App\Models\Department;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\DepartmentPhoneResource;
use App\Http\Resources\DepartmentPhoneCollection;

class DepartmentDepartmentPhonesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Department $department
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Department $department)
    {
        $this->authorize('view', $department);

        $search = $request->get('search', '');

        $departmentPhones = $department
            ->departmentPhones()
            ->search($search)->paginate();

        return new DepartmentPhoneCollection($departmentPhones);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Department $department
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Department $department)
    {
        $this->authorize('create', DepartmentPhone::class);

        $validated = $request->validate([
            'ref_type_phone_id' => ['required', 'exists:ref_type_phones,id'],
            'name' => ['required', 'max:255', 'string'],
        ]);

        $departmentPhone = $department->departmentPhones()->create($validated);

        return new DepartmentPhoneResource($departmentPhone);
    }
}
