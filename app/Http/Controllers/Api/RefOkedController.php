<?php

namespace App\Http\Controllers\Api;

use App\Models\RefOked;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefOkedResource;
use App\Http\Resources\RefOkedCollection;
use App\Http\Requests\RefOkedStoreRequest;
use App\Http\Requests\RefOkedUpdateRequest;

class RefOkedController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefOked::class);

        $search = $request->get('search', '');

        $refOkeds = RefOked::search($search)->paginate();

        return new RefOkedCollection($refOkeds);
    }

    /**
     * @param \App\Http\Requests\RefOkedStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefOkedStoreRequest $request)
    {
        $this->authorize('create', RefOked::class);

        $validated = $request->validated();

        $refOked = RefOked::create($validated);

        return new RefOkedResource($refOked);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefOked $refOked
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefOked $refOked)
    {
        $this->authorize('view', $refOked);

        return new RefOkedResource($refOked);
    }

    /**
     * @param \App\Http\Requests\RefOkedUpdateRequest $request
     * @param \App\Models\RefOked $refOked
     * @return \Illuminate\Http\Response
     */
    public function update(RefOkedUpdateRequest $request, RefOked $refOked)
    {
        $this->authorize('update', $refOked);

        $validated = $request->validated();

        $refOked->update($validated);

        return new RefOkedResource($refOked);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefOked $refOked
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, RefOked $refOked)
    {
        $this->authorize('delete', $refOked);

        $refOked->delete();

        return response()->noContent();
    }
}
