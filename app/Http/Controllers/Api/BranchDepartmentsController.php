<?php

namespace App\Http\Controllers\Api;

use App\Models\Branch;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\DepartmentResource;
use App\Http\Resources\DepartmentCollection;

class BranchDepartmentsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Branch $branch
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Branch $branch)
    {
        $this->authorize('view', $branch);

        $search = $request->get('search', '');

        $departments = $branch
            ->refDepartments()
            ->search($search)->paginate();

        return new DepartmentCollection($departments);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Branch $branch
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Branch $branch)
    {
        $this->authorize('create', Department::class);

        $validated = $request->validate([
            'parent_id' => ['required', 'max:255'],
            'email' => ['required', 'email'],
        ]);

        $department = $branch->refDepartments()->create($validated);

        return new DepartmentResource($department);
    }
}
