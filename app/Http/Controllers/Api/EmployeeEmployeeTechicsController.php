<?php

namespace App\Http\Controllers\Api;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeeTechicResource;
use App\Http\Resources\EmployeeTechicCollection;

class EmployeeEmployeeTechicsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Employee $employee)
    {
        $this->authorize('view', $employee);

        $search = $request->get('search', '');

        $employeeTechics = $employee
            ->employeeTechics()
            ->search($search)->paginate();

        return new EmployeeTechicCollection($employeeTechics);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Employee $employee)
    {
        $this->authorize('create', EmployeeTechic::class);

        $validated = $request->validate([
            'ref_technic_type_id' => [
                'required',
                'exists:ref_technic_types,id',
            ],
            'invent_num' => ['nullable', 'max:255', 'string'],
            'price' => ['nullable', 'numeric'],
        ]);

        $employeeTechic = $employee->employeeTechics()->create($validated);

        return new EmployeeTechicResource($employeeTechic);
    }
}
