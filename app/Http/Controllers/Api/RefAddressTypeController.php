<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\RefAddressType;
use App\Http\Controllers\Controller;
use App\Http\Resources\RefAddressTypeResource;
use App\Http\Resources\RefAddressTypeCollection;
use App\Http\Requests\RefAddressTypeStoreRequest;
use App\Http\Requests\RefAddressTypeUpdateRequest;

class RefAddressTypeController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefAddressType::class);

        $search = $request->get('search', '');

        $refAddressTypes = RefAddressType::search($search)->paginate();

        return new RefAddressTypeCollection($refAddressTypes);
    }

    /**
     * @param \App\Http\Requests\RefAddressTypeStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefAddressTypeStoreRequest $request)
    {
        $this->authorize('create', RefAddressType::class);

        $validated = $request->validated();

        $refAddressType = RefAddressType::create($validated);

        return new RefAddressTypeResource($refAddressType);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefAddressType $refAddressType
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RefAddressType $refAddressType)
    {
        $this->authorize('view', $refAddressType);

        return new RefAddressTypeResource($refAddressType);
    }

    /**
     * @param \App\Http\Requests\RefAddressTypeUpdateRequest $request
     * @param \App\Models\RefAddressType $refAddressType
     * @return \Illuminate\Http\Response
     */
    public function update(
        RefAddressTypeUpdateRequest $request,
        RefAddressType $refAddressType
    ) {
        $this->authorize('update', $refAddressType);

        $validated = $request->validated();

        $refAddressType->update($validated);

        return new RefAddressTypeResource($refAddressType);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\RefAddressType $refAddressType
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, RefAddressType $refAddressType)
    {
        $this->authorize('delete', $refAddressType);

        $refAddressType->delete();

        return response()->noContent();
    }
}
