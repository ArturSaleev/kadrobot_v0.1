<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Models\RefNationality;
use App\Http\Requests\RefNationalityStoreRequest;
use App\Http\Requests\RefNationalityUpdateRequest;
use Illuminate\Http\Response;

class RefNationalityController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefNationality::class);

        $search = $request->get('search', '');

        $refNationalities = RefNationality::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.ref_nationalities.index',
            compact('refNationalities', 'search')
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', RefNationality::class);

        return view('app.ref_nationalities.create');
    }

    /**
     * @param RefNationalityStoreRequest $request
     * @return Response
     * @throws AuthorizationException
     */
    public function store(RefNationalityStoreRequest $request)
    {
        $this->authorize('create', RefNationality::class);

        $validated = $request->validated();

        $refNationality = new RefNationality();
        $refNationality = $this->save($validated, $refNationality);

        return redirect()
            ->route('ref-nationalities.edit', $refNationality)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param Request $request
     * @param RefNationality $refNationality
     * @return Response
     * @throws AuthorizationException
     */
    public function show(Request $request, RefNationality $refNationality)
    {
        $this->authorize('view', $refNationality);

        return view('app.ref_nationalities.show', compact('refNationality'));
    }

    /**
     * @param Request $request
     * @param RefNationality $refNationality
     * @return Response
     * @throws AuthorizationException
     */
    public function edit(Request $request, RefNationality $refNationality)
    {
        $this->authorize('update', $refNationality);

        return view('app.ref_nationalities.edit', compact('refNationality'));
    }

    /**
     * @param RefNationalityUpdateRequest $request
     * @param RefNationality $refNationality
     * @return Response
     * @throws AuthorizationException
     */
    public function update(
        RefNationalityUpdateRequest $request,
        RefNationality $refNationality
    ) {
        $this->authorize('update', $refNationality);

        $validated = $request->validated();

        $refNationality = $this->save($validated, $refNationality);

        return redirect()
            ->route('ref-nationalities.edit', $refNationality)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param Request $request
     * @param RefNationality $refNationality
     * @return Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request, RefNationality $refNationality)
    {
        $this->authorize('delete', $refNationality);

        $refNationality->delete();

        return redirect()
            ->route('ref-nationalities.index')
            ->withSuccess(__('crud.common.removed'));
    }

    private function save($validated, $model)
    {
        foreach($validated['name'] as $lang => $value){
            $model->translateOrNew($lang)->name = $value;
        }
        $model->save();
        return $model;
    }
}
