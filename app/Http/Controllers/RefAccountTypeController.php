<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Models\RefAccountType;
use App\Http\Requests\RefAccountTypeStoreRequest;
use App\Http\Requests\RefAccountTypeUpdateRequest;
use Illuminate\Http\Response;

class RefAccountTypeController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefAccountType::class);

        $search = $request->get('search', '');

        $refAccountTypes = RefAccountType::search($search)
            ->paginate()
            ->withQueryString();

        return view(
            'app.ref_account_types.index',
            compact('refAccountTypes', 'search')
        );
    }

    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function create(Request $request)
    {
        $this->authorize('create', RefAccountType::class);

        return view('app.ref_account_types.create');
    }

    /**
     * @param RefAccountTypeStoreRequest $request
     * @return Response
     * @throws AuthorizationException
     */
    public function store(RefAccountTypeStoreRequest $request)
    {
        $this->authorize('create', RefAccountType::class);

        $validated = $request->validated();

        $refAccountType = new RefAccountType();
        $refAccountType = $this->save($validated, $refAccountType);

        return redirect()
            ->route('ref-account-types.edit', $refAccountType)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param Request $request
     * @param RefAccountType $refAccountType
     * @return Response
     * @throws AuthorizationException
     */
    public function show(Request $request, RefAccountType $refAccountType)
    {
        $this->authorize('view', $refAccountType);

        return view('app.ref_account_types.show', compact('refAccountType'));
    }

    /**
     * @param Request $request
     * @param RefAccountType $refAccountType
     * @return Response
     * @throws AuthorizationException
     */
    public function edit(Request $request, RefAccountType $refAccountType)
    {
        $this->authorize('update', $refAccountType);

        return view('app.ref_account_types.edit', compact('refAccountType'));
    }

    /**
     * @param RefAccountTypeUpdateRequest $request
     * @param RefAccountType $refAccountType
     * @return Response
     * @throws AuthorizationException
     */
    public function update(
        RefAccountTypeUpdateRequest $request,
        RefAccountType $refAccountType
    ) {
        $this->authorize('update', $refAccountType);

        $validated = $request->validated();

        $this->save($validated, $refAccountType);

        return redirect()
            ->route('ref-account-types.edit', $refAccountType)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param Request $request
     * @param RefAccountType $refAccountType
     * @return Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request, RefAccountType $refAccountType)
    {
        $this->authorize('delete', $refAccountType);

        $refAccountType->delete();

        return redirect()
            ->route('ref-account-types.index')
            ->withSuccess(__('crud.common.removed'));
    }

    private function save($validated, $model)
    {
        foreach($validated['name'] as $lang => $value){
            $model->translateOrNew($lang)->name = $value;
        }
        $model->save();
        return $model;
    }
}
