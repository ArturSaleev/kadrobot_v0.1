<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\RefDocType;
use App\Models\RefDocPlace;
use Illuminate\Http\Request;
use App\Models\EmployeeDocument;
use App\Http\Requests\EmployeeDocumentStoreRequest;
use App\Http\Requests\EmployeeDocumentUpdateRequest;

class EmployeeDocumentController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeeDocument::class);

        $search = $request->get('search', '');

        $employeeDocuments = EmployeeDocument::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.employee_documents.index',
            compact('employeeDocuments', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', EmployeeDocument::class);

        $employees = Employee::pluck('iin', 'id');
        $refDocTypes = RefDocType::pluck('id', 'id');
        $refDocPlaces = RefDocPlace::pluck('id', 'id');

        return view(
            'app.employee_documents.create',
            compact('employees', 'refDocTypes', 'refDocPlaces')
        );
    }

    /**
     * @param \App\Http\Requests\EmployeeDocumentStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeDocumentStoreRequest $request)
    {
        $this->authorize('create', EmployeeDocument::class);

        $validated = $request->validated();

        $employeeDocument = EmployeeDocument::create($validated);

        return redirect()
            ->route('employee-documents.edit', $employeeDocument)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeDocument $employeeDocument
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EmployeeDocument $employeeDocument)
    {
        $this->authorize('view', $employeeDocument);

        return view('app.employee_documents.show', compact('employeeDocument'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeDocument $employeeDocument
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, EmployeeDocument $employeeDocument)
    {
        $this->authorize('update', $employeeDocument);

        $employees = Employee::pluck('iin', 'id');
        $refDocTypes = RefDocType::pluck('id', 'id');
        $refDocPlaces = RefDocPlace::pluck('id', 'id');

        return view(
            'app.employee_documents.edit',
            compact(
                'employeeDocument',
                'employees',
                'refDocTypes',
                'refDocPlaces'
            )
        );
    }

    /**
     * @param \App\Http\Requests\EmployeeDocumentUpdateRequest $request
     * @param \App\Models\EmployeeDocument $employeeDocument
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeeDocumentUpdateRequest $request,
        EmployeeDocument $employeeDocument
    ) {
        $this->authorize('update', $employeeDocument);

        $validated = $request->validated();

        $employeeDocument->update($validated);

        return redirect()
            ->route('employee-documents.edit', $employeeDocument)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeDocument $employeeDocument
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        Request $request,
        EmployeeDocument $employeeDocument
    ) {
        $this->authorize('delete', $employeeDocument);

        $employeeDocument->delete();

        return redirect()
            ->route('employee-documents.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
