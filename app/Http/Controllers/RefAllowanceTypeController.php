<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Models\RefAllowanceType;
use App\Http\Requests\RefAllowanceTypeStoreRequest;
use App\Http\Requests\RefAllowanceTypeUpdateRequest;
use Illuminate\Http\Response;

class RefAllowanceTypeController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefAllowanceType::class);

        $search = $request->get('search', '');

        $refAllowanceTypes = RefAllowanceType::search($search)
            ->paginate()
            ->withQueryString();

        return view(
            'app.ref_allowance_types.index',
            compact('refAllowanceTypes', 'search')
        );
    }

    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function create(Request $request)
    {
        $this->authorize('create', RefAllowanceType::class);

        return view('app.ref_allowance_types.create');
    }

    /**
     * @param RefAllowanceTypeStoreRequest $request
     * @return Response
     */
    public function store(RefAllowanceTypeStoreRequest $request)
    {
        $this->authorize('create', RefAllowanceType::class);

        $validated = $request->validated();

        $refAllowanceType = new RefAllowanceType();
        $refAllowanceType = $this->save($validated, $refAllowanceType);

        return redirect()
            ->route('ref-allowance-types.edit', $refAllowanceType)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param Request $request
     * @param RefAllowanceType $refAllowanceType
     * @return Response
     * @throws AuthorizationException
     */
    public function show(Request $request, RefAllowanceType $refAllowanceType)
    {
        $this->authorize('view', $refAllowanceType);

        return view(
            'app.ref_allowance_types.show',
            compact('refAllowanceType')
        );
    }

    /**
     * @param Request $request
     * @param RefAllowanceType $refAllowanceType
     * @return Response
     * @throws AuthorizationException
     */
    public function edit(Request $request, RefAllowanceType $refAllowanceType)
    {
        $this->authorize('update', $refAllowanceType);

        return view(
            'app.ref_allowance_types.edit',
            compact('refAllowanceType')
        );
    }

    /**
     * @param RefAllowanceTypeUpdateRequest $request
     * @param RefAllowanceType $refAllowanceType
     * @return Response
     * @throws AuthorizationException
     */
    public function update(
        RefAllowanceTypeUpdateRequest $request,
        RefAllowanceType $refAllowanceType
    ) {
        $this->authorize('update', $refAllowanceType);

        $validated = $request->validated();

        $this->save($validated, $refAllowanceType);

        return redirect()
            ->route('ref-allowance-types.edit', $refAllowanceType)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param Request $request
     * @param RefAllowanceType $refAllowanceType
     * @return Response
     * @throws AuthorizationException
     */
    public function destroy(
        Request $request,
        RefAllowanceType $refAllowanceType
    ) {
        $this->authorize('delete', $refAllowanceType);

        $refAllowanceType->delete();

        return redirect()
            ->route('ref-allowance-types.index')
            ->withSuccess(__('crud.common.removed'));
    }

    private function save($validated, $model)
    {
        foreach($validated['name'] as $lang => $value){
            $model->translateOrNew($lang)->name = $value;
        }
        $model->save();
        return $model;
    }
}
