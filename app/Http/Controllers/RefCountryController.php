<?php

namespace App\Http\Controllers;

use App\Models\RefCountry;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Http\Requests\RefCountryStoreRequest;
use App\Http\Requests\RefCountryUpdateRequest;
use Illuminate\Http\Response;

class RefCountryController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', RefCountry::class);

        $search = $request->get('search', '');

        $refCountries = RefCountry::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.ref_countries.index',
            compact('refCountries', 'search')
        );
    }

    /**
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function create(Request $request)
    {
        $this->authorize('create', RefCountry::class);

        return view('app.ref_countries.create');
    }

    /**
     * @param RefCountryStoreRequest $request
     * @return Response
     * @throws AuthorizationException
     */
    public function store(RefCountryStoreRequest $request)
    {
        $this->authorize('create', RefCountry::class);

        $validated = $request->validated();

        $refCountry = new RefCountry();
        $refCountry = $this->save($validated, $refCountry);

        return redirect()
            ->route('ref-countries.edit', $refCountry)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param Request $request
     * @param RefCountry $refCountry
     * @return Response
     * @throws AuthorizationException
     */
    public function show(Request $request, RefCountry $refCountry)
    {
        $this->authorize('view', $refCountry);

        return view('app.ref_countries.show', compact('refCountry'));
    }

    /**
     * @param Request $request
     * @param RefCountry $refCountry
     * @return Response
     * @throws AuthorizationException
     */
    public function edit(Request $request, RefCountry $refCountry)
    {
        $this->authorize('update', $refCountry);

        return view('app.ref_countries.edit', compact('refCountry'));
    }

    /**
     * @param RefCountryUpdateRequest $request
     * @param RefCountry $refCountry
     * @return Response
     * @throws AuthorizationException
     */
    public function update(
        RefCountryUpdateRequest $request,
        RefCountry $refCountry
    ) {
        $this->authorize('update', $refCountry);

        $validated = $request->validated();

        $this->save($validated, $refCountry);

        return redirect()
            ->route('ref-countries.edit', $refCountry)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param Request $request
     * @param RefCountry $refCountry
     * @return Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request, RefCountry $refCountry)
    {
        $this->authorize('delete', $refCountry);

        $refCountry->delete();

        return redirect()
            ->route('ref-countries.index')
            ->withSuccess(__('crud.common.removed'));
    }

    private function save($validated, $model)
    {
        foreach($validated['name'] as $lang => $value){
            $model->translateOrNew($lang)->name = $value;
        }
        $model->code = $validated['code'];
        $model->save();
        return $model;
    }
}
