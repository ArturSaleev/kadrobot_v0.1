<?php

namespace App\Http\Controllers;

use App\Models\Department;
use Illuminate\Http\Request;
use App\Models\RefTypePhone;
use App\Models\DepartmentPhone;
use App\Http\Requests\DepartmentPhoneStoreRequest;
use App\Http\Requests\DepartmentPhoneUpdateRequest;

class DepartmentPhoneController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', DepartmentPhone::class);

        $search = $request->get('search', '');

        $departmentPhones = DepartmentPhone::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.department_phones.index',
            compact('departmentPhones', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', DepartmentPhone::class);

        $departments = Department::pluck('email', 'id');
        $refTypePhones = RefTypePhone::pluck('id', 'id');

        return view(
            'app.department_phones.create',
            compact('departments', 'refTypePhones')
        );
    }

    /**
     * @param \App\Http\Requests\DepartmentPhoneStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepartmentPhoneStoreRequest $request)
    {
        $this->authorize('create', DepartmentPhone::class);

        $validated = $request->validated();

        $departmentPhone = DepartmentPhone::create($validated);

        return redirect()
            ->route('department-phones.edit', $departmentPhone)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\DepartmentPhone $departmentPhone
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, DepartmentPhone $departmentPhone)
    {
        $this->authorize('view', $departmentPhone);

        return view('app.department_phones.show', compact('departmentPhone'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\DepartmentPhone $departmentPhone
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, DepartmentPhone $departmentPhone)
    {
        $this->authorize('update', $departmentPhone);

        $departments = Department::pluck('email', 'id');
        $refTypePhones = RefTypePhone::pluck('id', 'id');

        return view(
            'app.department_phones.edit',
            compact('departmentPhone', 'departments', 'refTypePhones')
        );
    }

    /**
     * @param \App\Http\Requests\DepartmentPhoneUpdateRequest $request
     * @param \App\Models\DepartmentPhone $departmentPhone
     * @return \Illuminate\Http\Response
     */
    public function update(
        DepartmentPhoneUpdateRequest $request,
        DepartmentPhone $departmentPhone
    ) {
        $this->authorize('update', $departmentPhone);

        $validated = $request->validated();

        $departmentPhone->update($validated);

        return redirect()
            ->route('department-phones.edit', $departmentPhone)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\DepartmentPhone $departmentPhone
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, DepartmentPhone $departmentPhone)
    {
        $this->authorize('delete', $departmentPhone);

        $departmentPhone->delete();

        return redirect()
            ->route('department-phones.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
