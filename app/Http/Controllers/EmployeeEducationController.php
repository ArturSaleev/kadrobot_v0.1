<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\EmployeeEducation;
use App\Http\Requests\EmployeeEducationStoreRequest;
use App\Http\Requests\EmployeeEducationUpdateRequest;

class EmployeeEducationController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EmployeeEducation::class);

        $search = $request->get('search', '');

        $employeeEducations = EmployeeEducation::search($search)->paginate()
            ->withQueryString();

        return view(
            'app.employee_educations.index',
            compact('employeeEducations', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', EmployeeEducation::class);

        $employees = Employee::pluck('iin', 'id');

        return view('app.employee_educations.create', compact('employees'));
    }

    /**
     * @param \App\Http\Requests\EmployeeEducationStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeEducationStoreRequest $request)
    {
        $this->authorize('create', EmployeeEducation::class);

        $validated = $request->validated();

        $employeeEducation = EmployeeEducation::create($validated);

        return redirect()
            ->route('employee-educations.edit', $employeeEducation)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeEducation $employeeEducation
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EmployeeEducation $employeeEducation)
    {
        $this->authorize('view', $employeeEducation);

        return view(
            'app.employee_educations.show',
            compact('employeeEducation')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeEducation $employeeEducation
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, EmployeeEducation $employeeEducation)
    {
        $this->authorize('update', $employeeEducation);

        $employees = Employee::pluck('iin', 'id');

        return view(
            'app.employee_educations.edit',
            compact('employeeEducation', 'employees')
        );
    }

    /**
     * @param \App\Http\Requests\EmployeeEducationUpdateRequest $request
     * @param \App\Models\EmployeeEducation $employeeEducation
     * @return \Illuminate\Http\Response
     */
    public function update(
        EmployeeEducationUpdateRequest $request,
        EmployeeEducation $employeeEducation
    ) {
        $this->authorize('update', $employeeEducation);

        $validated = $request->validated();

        $employeeEducation->update($validated);

        return redirect()
            ->route('employee-educations.edit', $employeeEducation)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EmployeeEducation $employeeEducation
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        Request $request,
        EmployeeEducation $employeeEducation
    ) {
        $this->authorize('delete', $employeeEducation);

        $employeeEducation->delete();

        return redirect()
            ->route('employee-educations.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
