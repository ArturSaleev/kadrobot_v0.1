<?php

namespace App\Http\Controllers;

use App\Models\T2Card;
use App\Models\Branch;
use App\Models\Company;
use App\Models\Employee;
use App\Models\Position;
use App\Models\RefAction;
use Illuminate\Http\Request;
use App\Http\Requests\T2CardStoreRequest;
use App\Http\Requests\T2CardUpdateRequest;

class T2CardController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', T2Card::class);

        $search = $request->get('search', '');

        $t2Cards = T2Card::search($search)->paginate()
            ->withQueryString();

        return view('app.t2_cards.index', compact('t2Cards', 'search'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', T2Card::class);

        $companies = Company::pluck('bin', 'id');
        $employees = Employee::pluck('iin', 'id');
        $branches = Branch::pluck('id', 'id');
        $refActions = RefAction::pluck('id', 'id');
        $positions = Position::pluck('id', 'id');

        return view(
            'app.t2_cards.create',
            compact(
                'companies',
                'employees',
                'branches',
                'refActions',
                'positions'
            )
        );
    }

    /**
     * @param \App\Http\Requests\T2CardStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(T2CardStoreRequest $request)
    {
        $this->authorize('create', T2Card::class);

        $validated = $request->validated();

        $t2Card = T2Card::create($validated);

        return redirect()
            ->route('t2-cards.edit', $t2Card)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\T2Card $t2Card
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, T2Card $t2Card)
    {
        $this->authorize('view', $t2Card);

        return view('app.t2_cards.show', compact('t2Card'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\T2Card $t2Card
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, T2Card $t2Card)
    {
        $this->authorize('update', $t2Card);

        $companies = Company::pluck('bin', 'id');
        $employees = Employee::pluck('iin', 'id');
        $branches = Branch::pluck('id', 'id');
        $refActions = RefAction::pluck('id', 'id');
        $positions = Position::pluck('id', 'id');

        return view(
            'app.t2_cards.edit',
            compact(
                't2Card',
                'companies',
                'employees',
                'branches',
                'refActions',
                'positions'
            )
        );
    }

    /**
     * @param \App\Http\Requests\T2CardUpdateRequest $request
     * @param \App\Models\T2Card $t2Card
     * @return \Illuminate\Http\Response
     */
    public function update(T2CardUpdateRequest $request, T2Card $t2Card)
    {
        $this->authorize('update', $t2Card);

        $validated = $request->validated();

        $t2Card->update($validated);

        return redirect()
            ->route('t2-cards.edit', $t2Card)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\T2Card $t2Card
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, T2Card $t2Card)
    {
        $this->authorize('delete', $t2Card);

        $t2Card->delete();

        return redirect()
            ->route('t2-cards.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
