<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EmployeeHolidaysPeriod;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeeHolidaysPeriodPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the employeeHolidaysPeriod can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list employeeholidaysperiods');
    }

    /**
     * Determine whether the employeeHolidaysPeriod can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeHolidaysPeriod  $model
     * @return mixed
     */
    public function view(User $user, EmployeeHolidaysPeriod $model)
    {
        return $user->hasPermissionTo('view employeeholidaysperiods');
    }

    /**
     * Determine whether the employeeHolidaysPeriod can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create employeeholidaysperiods');
    }

    /**
     * Determine whether the employeeHolidaysPeriod can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeHolidaysPeriod  $model
     * @return mixed
     */
    public function update(User $user, EmployeeHolidaysPeriod $model)
    {
        return $user->hasPermissionTo('update employeeholidaysperiods');
    }

    /**
     * Determine whether the employeeHolidaysPeriod can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeHolidaysPeriod  $model
     * @return mixed
     */
    public function delete(User $user, EmployeeHolidaysPeriod $model)
    {
        return $user->hasPermissionTo('delete employeeholidaysperiods');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeHolidaysPeriod  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete employeeholidaysperiods');
    }

    /**
     * Determine whether the employeeHolidaysPeriod can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeHolidaysPeriod  $model
     * @return mixed
     */
    public function restore(User $user, EmployeeHolidaysPeriod $model)
    {
        return false;
    }

    /**
     * Determine whether the employeeHolidaysPeriod can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeHolidaysPeriod  $model
     * @return mixed
     */
    public function forceDelete(User $user, EmployeeHolidaysPeriod $model)
    {
        return false;
    }
}
