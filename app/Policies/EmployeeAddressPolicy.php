<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EmployeeAddress;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeeAddressPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the employeeAddress can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list employeeaddresses');
    }

    /**
     * Determine whether the employeeAddress can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeAddress  $model
     * @return mixed
     */
    public function view(User $user, EmployeeAddress $model)
    {
        return $user->hasPermissionTo('view employeeaddresses');
    }

    /**
     * Determine whether the employeeAddress can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create employeeaddresses');
    }

    /**
     * Determine whether the employeeAddress can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeAddress  $model
     * @return mixed
     */
    public function update(User $user, EmployeeAddress $model)
    {
        return $user->hasPermissionTo('update employeeaddresses');
    }

    /**
     * Determine whether the employeeAddress can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeAddress  $model
     * @return mixed
     */
    public function delete(User $user, EmployeeAddress $model)
    {
        return $user->hasPermissionTo('delete employeeaddresses');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeAddress  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete employeeaddresses');
    }

    /**
     * Determine whether the employeeAddress can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeAddress  $model
     * @return mixed
     */
    public function restore(User $user, EmployeeAddress $model)
    {
        return false;
    }

    /**
     * Determine whether the employeeAddress can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeAddress  $model
     * @return mixed
     */
    public function forceDelete(User $user, EmployeeAddress $model)
    {
        return false;
    }
}
