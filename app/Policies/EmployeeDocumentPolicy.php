<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EmployeeDocument;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeeDocumentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the employeeDocument can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list employeedocuments');
    }

    /**
     * Determine whether the employeeDocument can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeDocument  $model
     * @return mixed
     */
    public function view(User $user, EmployeeDocument $model)
    {
        return $user->hasPermissionTo('view employeedocuments');
    }

    /**
     * Determine whether the employeeDocument can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create employeedocuments');
    }

    /**
     * Determine whether the employeeDocument can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeDocument  $model
     * @return mixed
     */
    public function update(User $user, EmployeeDocument $model)
    {
        return $user->hasPermissionTo('update employeedocuments');
    }

    /**
     * Determine whether the employeeDocument can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeDocument  $model
     * @return mixed
     */
    public function delete(User $user, EmployeeDocument $model)
    {
        return $user->hasPermissionTo('delete employeedocuments');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeDocument  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete employeedocuments');
    }

    /**
     * Determine whether the employeeDocument can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeDocument  $model
     * @return mixed
     */
    public function restore(User $user, EmployeeDocument $model)
    {
        return false;
    }

    /**
     * Determine whether the employeeDocument can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeDocument  $model
     * @return mixed
     */
    public function forceDelete(User $user, EmployeeDocument $model)
    {
        return false;
    }
}
