<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefVidHoliday;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefVidHolidayPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refVidHoliday can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refvidholidays');
    }

    /**
     * Determine whether the refVidHoliday can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefVidHoliday  $model
     * @return mixed
     */
    public function view(User $user, RefVidHoliday $model)
    {
        return $user->hasPermissionTo('view refvidholidays');
    }

    /**
     * Determine whether the refVidHoliday can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refvidholidays');
    }

    /**
     * Determine whether the refVidHoliday can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefVidHoliday  $model
     * @return mixed
     */
    public function update(User $user, RefVidHoliday $model)
    {
        return $user->hasPermissionTo('update refvidholidays');
    }

    /**
     * Determine whether the refVidHoliday can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefVidHoliday  $model
     * @return mixed
     */
    public function delete(User $user, RefVidHoliday $model)
    {
        return $user->hasPermissionTo('delete refvidholidays');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefVidHoliday  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refvidholidays');
    }

    /**
     * Determine whether the refVidHoliday can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefVidHoliday  $model
     * @return mixed
     */
    public function restore(User $user, RefVidHoliday $model)
    {
        return false;
    }

    /**
     * Determine whether the refVidHoliday can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefVidHoliday  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefVidHoliday $model)
    {
        return false;
    }
}
