<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EmployeeEducation;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeeEducationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the employeeEducation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list employeeeducations');
    }

    /**
     * Determine whether the employeeEducation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeEducation  $model
     * @return mixed
     */
    public function view(User $user, EmployeeEducation $model)
    {
        return $user->hasPermissionTo('view employeeeducations');
    }

    /**
     * Determine whether the employeeEducation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create employeeeducations');
    }

    /**
     * Determine whether the employeeEducation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeEducation  $model
     * @return mixed
     */
    public function update(User $user, EmployeeEducation $model)
    {
        return $user->hasPermissionTo('update employeeeducations');
    }

    /**
     * Determine whether the employeeEducation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeEducation  $model
     * @return mixed
     */
    public function delete(User $user, EmployeeEducation $model)
    {
        return $user->hasPermissionTo('delete employeeeducations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeEducation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete employeeeducations');
    }

    /**
     * Determine whether the employeeEducation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeEducation  $model
     * @return mixed
     */
    public function restore(User $user, EmployeeEducation $model)
    {
        return false;
    }

    /**
     * Determine whether the employeeEducation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeEducation  $model
     * @return mixed
     */
    public function forceDelete(User $user, EmployeeEducation $model)
    {
        return false;
    }
}
