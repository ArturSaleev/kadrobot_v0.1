<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EmployeeAddressTranslation;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeeAddressTranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the employeeAddressTranslation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list employeeaddresstranslations');
    }

    /**
     * Determine whether the employeeAddressTranslation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeAddressTranslation  $model
     * @return mixed
     */
    public function view(User $user, EmployeeAddressTranslation $model)
    {
        return $user->hasPermissionTo('view employeeaddresstranslations');
    }

    /**
     * Determine whether the employeeAddressTranslation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create employeeaddresstranslations');
    }

    /**
     * Determine whether the employeeAddressTranslation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeAddressTranslation  $model
     * @return mixed
     */
    public function update(User $user, EmployeeAddressTranslation $model)
    {
        return $user->hasPermissionTo('update employeeaddresstranslations');
    }

    /**
     * Determine whether the employeeAddressTranslation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeAddressTranslation  $model
     * @return mixed
     */
    public function delete(User $user, EmployeeAddressTranslation $model)
    {
        return $user->hasPermissionTo('delete employeeaddresstranslations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeAddressTranslation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete employeeaddresstranslations');
    }

    /**
     * Determine whether the employeeAddressTranslation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeAddressTranslation  $model
     * @return mixed
     */
    public function restore(User $user, EmployeeAddressTranslation $model)
    {
        return false;
    }

    /**
     * Determine whether the employeeAddressTranslation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeAddressTranslation  $model
     * @return mixed
     */
    public function forceDelete(User $user, EmployeeAddressTranslation $model)
    {
        return false;
    }
}
