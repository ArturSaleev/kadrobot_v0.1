<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefDocPlaceTranslation;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefDocPlaceTranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refDocPlaceTranslation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refdocplacetranslations');
    }

    /**
     * Determine whether the refDocPlaceTranslation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefDocPlaceTranslation  $model
     * @return mixed
     */
    public function view(User $user, RefDocPlaceTranslation $model)
    {
        return $user->hasPermissionTo('view refdocplacetranslations');
    }

    /**
     * Determine whether the refDocPlaceTranslation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refdocplacetranslations');
    }

    /**
     * Determine whether the refDocPlaceTranslation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefDocPlaceTranslation  $model
     * @return mixed
     */
    public function update(User $user, RefDocPlaceTranslation $model)
    {
        return $user->hasPermissionTo('update refdocplacetranslations');
    }

    /**
     * Determine whether the refDocPlaceTranslation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefDocPlaceTranslation  $model
     * @return mixed
     */
    public function delete(User $user, RefDocPlaceTranslation $model)
    {
        return $user->hasPermissionTo('delete refdocplacetranslations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefDocPlaceTranslation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refdocplacetranslations');
    }

    /**
     * Determine whether the refDocPlaceTranslation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefDocPlaceTranslation  $model
     * @return mixed
     */
    public function restore(User $user, RefDocPlaceTranslation $model)
    {
        return false;
    }

    /**
     * Determine whether the refDocPlaceTranslation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefDocPlaceTranslation  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefDocPlaceTranslation $model)
    {
        return false;
    }
}
