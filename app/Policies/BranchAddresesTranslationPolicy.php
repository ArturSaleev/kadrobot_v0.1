<?php

namespace App\Policies;

use App\Models\User;
use App\Models\BranchAddresesTranslation;
use Illuminate\Auth\Access\HandlesAuthorization;

class BranchAddresesTranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the branchAddresesTranslation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list branchaddresestranslations');
    }

    /**
     * Determine whether the branchAddresesTranslation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BranchAddresesTranslation  $model
     * @return mixed
     */
    public function view(User $user, BranchAddresesTranslation $model)
    {
        return $user->hasPermissionTo('view branchaddresestranslations');
    }

    /**
     * Determine whether the branchAddresesTranslation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create branchaddresestranslations');
    }

    /**
     * Determine whether the branchAddresesTranslation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BranchAddresesTranslation  $model
     * @return mixed
     */
    public function update(User $user, BranchAddresesTranslation $model)
    {
        return $user->hasPermissionTo('update branchaddresestranslations');
    }

    /**
     * Determine whether the branchAddresesTranslation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BranchAddresesTranslation  $model
     * @return mixed
     */
    public function delete(User $user, BranchAddresesTranslation $model)
    {
        return $user->hasPermissionTo('delete branchaddresestranslations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BranchAddresesTranslation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete branchaddresestranslations');
    }

    /**
     * Determine whether the branchAddresesTranslation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BranchAddresesTranslation  $model
     * @return mixed
     */
    public function restore(User $user, BranchAddresesTranslation $model)
    {
        return false;
    }

    /**
     * Determine whether the branchAddresesTranslation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BranchAddresesTranslation  $model
     * @return mixed
     */
    public function forceDelete(User $user, BranchAddresesTranslation $model)
    {
        return false;
    }
}
