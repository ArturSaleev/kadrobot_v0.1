<?php

namespace App\Policies;

use App\Models\User;
use App\Models\PositionDeclension;
use Illuminate\Auth\Access\HandlesAuthorization;

class PositionDeclensionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the positionDeclension can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list positiondeclensions');
    }

    /**
     * Determine whether the positionDeclension can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\PositionDeclension  $model
     * @return mixed
     */
    public function view(User $user, PositionDeclension $model)
    {
        return $user->hasPermissionTo('view positiondeclensions');
    }

    /**
     * Determine whether the positionDeclension can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create positiondeclensions');
    }

    /**
     * Determine whether the positionDeclension can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\PositionDeclension  $model
     * @return mixed
     */
    public function update(User $user, PositionDeclension $model)
    {
        return $user->hasPermissionTo('update positiondeclensions');
    }

    /**
     * Determine whether the positionDeclension can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\PositionDeclension  $model
     * @return mixed
     */
    public function delete(User $user, PositionDeclension $model)
    {
        return $user->hasPermissionTo('delete positiondeclensions');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\PositionDeclension  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete positiondeclensions');
    }

    /**
     * Determine whether the positionDeclension can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\PositionDeclension  $model
     * @return mixed
     */
    public function restore(User $user, PositionDeclension $model)
    {
        return false;
    }

    /**
     * Determine whether the positionDeclension can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\PositionDeclension  $model
     * @return mixed
     */
    public function forceDelete(User $user, PositionDeclension $model)
    {
        return false;
    }
}
