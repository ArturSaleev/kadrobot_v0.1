<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EmployeeHospital;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeeHospitalPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the employeeHospital can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list employeehospitals');
    }

    /**
     * Determine whether the employeeHospital can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeHospital  $model
     * @return mixed
     */
    public function view(User $user, EmployeeHospital $model)
    {
        return $user->hasPermissionTo('view employeehospitals');
    }

    /**
     * Determine whether the employeeHospital can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create employeehospitals');
    }

    /**
     * Determine whether the employeeHospital can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeHospital  $model
     * @return mixed
     */
    public function update(User $user, EmployeeHospital $model)
    {
        return $user->hasPermissionTo('update employeehospitals');
    }

    /**
     * Determine whether the employeeHospital can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeHospital  $model
     * @return mixed
     */
    public function delete(User $user, EmployeeHospital $model)
    {
        return $user->hasPermissionTo('delete employeehospitals');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeHospital  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete employeehospitals');
    }

    /**
     * Determine whether the employeeHospital can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeHospital  $model
     * @return mixed
     */
    public function restore(User $user, EmployeeHospital $model)
    {
        return false;
    }

    /**
     * Determine whether the employeeHospital can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeHospital  $model
     * @return mixed
     */
    public function forceDelete(User $user, EmployeeHospital $model)
    {
        return false;
    }
}
