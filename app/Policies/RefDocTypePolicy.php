<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefDocType;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefDocTypePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refDocType can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refdoctypes');
    }

    /**
     * Determine whether the refDocType can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefDocType  $model
     * @return mixed
     */
    public function view(User $user, RefDocType $model)
    {
        return $user->hasPermissionTo('view refdoctypes');
    }

    /**
     * Determine whether the refDocType can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refdoctypes');
    }

    /**
     * Determine whether the refDocType can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefDocType  $model
     * @return mixed
     */
    public function update(User $user, RefDocType $model)
    {
        return $user->hasPermissionTo('update refdoctypes');
    }

    /**
     * Determine whether the refDocType can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefDocType  $model
     * @return mixed
     */
    public function delete(User $user, RefDocType $model)
    {
        return $user->hasPermissionTo('delete refdoctypes');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefDocType  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refdoctypes');
    }

    /**
     * Determine whether the refDocType can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefDocType  $model
     * @return mixed
     */
    public function restore(User $user, RefDocType $model)
    {
        return false;
    }

    /**
     * Determine whether the refDocType can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefDocType  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefDocType $model)
    {
        return false;
    }
}
