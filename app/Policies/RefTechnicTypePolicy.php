<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefTechnicType;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefTechnicTypePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refTechnicType can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list reftechnictypes');
    }

    /**
     * Determine whether the refTechnicType can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTechnicType  $model
     * @return mixed
     */
    public function view(User $user, RefTechnicType $model)
    {
        return $user->hasPermissionTo('view reftechnictypes');
    }

    /**
     * Determine whether the refTechnicType can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create reftechnictypes');
    }

    /**
     * Determine whether the refTechnicType can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTechnicType  $model
     * @return mixed
     */
    public function update(User $user, RefTechnicType $model)
    {
        return $user->hasPermissionTo('update reftechnictypes');
    }

    /**
     * Determine whether the refTechnicType can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTechnicType  $model
     * @return mixed
     */
    public function delete(User $user, RefTechnicType $model)
    {
        return $user->hasPermissionTo('delete reftechnictypes');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTechnicType  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete reftechnictypes');
    }

    /**
     * Determine whether the refTechnicType can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTechnicType  $model
     * @return mixed
     */
    public function restore(User $user, RefTechnicType $model)
    {
        return false;
    }

    /**
     * Determine whether the refTechnicType can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTechnicType  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefTechnicType $model)
    {
        return false;
    }
}
