<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefOkedTranslation;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefOkedTranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refOkedTranslation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refokedtranslations');
    }

    /**
     * Determine whether the refOkedTranslation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefOkedTranslation  $model
     * @return mixed
     */
    public function view(User $user, RefOkedTranslation $model)
    {
        return $user->hasPermissionTo('view refokedtranslations');
    }

    /**
     * Determine whether the refOkedTranslation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refokedtranslations');
    }

    /**
     * Determine whether the refOkedTranslation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefOkedTranslation  $model
     * @return mixed
     */
    public function update(User $user, RefOkedTranslation $model)
    {
        return $user->hasPermissionTo('update refokedtranslations');
    }

    /**
     * Determine whether the refOkedTranslation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefOkedTranslation  $model
     * @return mixed
     */
    public function delete(User $user, RefOkedTranslation $model)
    {
        return $user->hasPermissionTo('delete refokedtranslations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefOkedTranslation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refokedtranslations');
    }

    /**
     * Determine whether the refOkedTranslation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefOkedTranslation  $model
     * @return mixed
     */
    public function restore(User $user, RefOkedTranslation $model)
    {
        return false;
    }

    /**
     * Determine whether the refOkedTranslation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefOkedTranslation  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefOkedTranslation $model)
    {
        return false;
    }
}
