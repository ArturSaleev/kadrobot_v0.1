<?php

namespace App\Policies;

use App\Models\User;
use App\Models\DepartmentPhone;
use Illuminate\Auth\Access\HandlesAuthorization;

class DepartmentPhonePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the departmentPhone can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list departmentphones');
    }

    /**
     * Determine whether the departmentPhone can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\DepartmentPhone  $model
     * @return mixed
     */
    public function view(User $user, DepartmentPhone $model)
    {
        return $user->hasPermissionTo('view departmentphones');
    }

    /**
     * Determine whether the departmentPhone can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create departmentphones');
    }

    /**
     * Determine whether the departmentPhone can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\DepartmentPhone  $model
     * @return mixed
     */
    public function update(User $user, DepartmentPhone $model)
    {
        return $user->hasPermissionTo('update departmentphones');
    }

    /**
     * Determine whether the departmentPhone can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\DepartmentPhone  $model
     * @return mixed
     */
    public function delete(User $user, DepartmentPhone $model)
    {
        return $user->hasPermissionTo('delete departmentphones');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\DepartmentPhone  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete departmentphones');
    }

    /**
     * Determine whether the departmentPhone can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\DepartmentPhone  $model
     * @return mixed
     */
    public function restore(User $user, DepartmentPhone $model)
    {
        return false;
    }

    /**
     * Determine whether the departmentPhone can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\DepartmentPhone  $model
     * @return mixed
     */
    public function forceDelete(User $user, DepartmentPhone $model)
    {
        return false;
    }
}
