<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefTypeRodstv;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefTypeRodstvPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refTypeRodstv can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list reftyperodstvs');
    }

    /**
     * Determine whether the refTypeRodstv can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTypeRodstv  $model
     * @return mixed
     */
    public function view(User $user, RefTypeRodstv $model)
    {
        return $user->hasPermissionTo('view reftyperodstvs');
    }

    /**
     * Determine whether the refTypeRodstv can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create reftyperodstvs');
    }

    /**
     * Determine whether the refTypeRodstv can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTypeRodstv  $model
     * @return mixed
     */
    public function update(User $user, RefTypeRodstv $model)
    {
        return $user->hasPermissionTo('update reftyperodstvs');
    }

    /**
     * Determine whether the refTypeRodstv can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTypeRodstv  $model
     * @return mixed
     */
    public function delete(User $user, RefTypeRodstv $model)
    {
        return $user->hasPermissionTo('delete reftyperodstvs');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTypeRodstv  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete reftyperodstvs');
    }

    /**
     * Determine whether the refTypeRodstv can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTypeRodstv  $model
     * @return mixed
     */
    public function restore(User $user, RefTypeRodstv $model)
    {
        return false;
    }

    /**
     * Determine whether the refTypeRodstv can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTypeRodstv  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefTypeRodstv $model)
    {
        return false;
    }
}
