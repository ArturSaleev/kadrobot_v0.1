<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefOked;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefOkedPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refOked can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refokeds');
    }

    /**
     * Determine whether the refOked can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefOked  $model
     * @return mixed
     */
    public function view(User $user, RefOked $model)
    {
        return $user->hasPermissionTo('view refokeds');
    }

    /**
     * Determine whether the refOked can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refokeds');
    }

    /**
     * Determine whether the refOked can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefOked  $model
     * @return mixed
     */
    public function update(User $user, RefOked $model)
    {
        return $user->hasPermissionTo('update refokeds');
    }

    /**
     * Determine whether the refOked can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefOked  $model
     * @return mixed
     */
    public function delete(User $user, RefOked $model)
    {
        return $user->hasPermissionTo('delete refokeds');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefOked  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refokeds');
    }

    /**
     * Determine whether the refOked can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefOked  $model
     * @return mixed
     */
    public function restore(User $user, RefOked $model)
    {
        return false;
    }

    /**
     * Determine whether the refOked can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefOked  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefOked $model)
    {
        return false;
    }
}
