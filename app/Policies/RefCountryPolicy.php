<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefCountry;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefCountryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refCountry can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refcountries');
    }

    /**
     * Determine whether the refCountry can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefCountry  $model
     * @return mixed
     */
    public function view(User $user, RefCountry $model)
    {
        return $user->hasPermissionTo('view refcountries');
    }

    /**
     * Determine whether the refCountry can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refcountries');
    }

    /**
     * Determine whether the refCountry can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefCountry  $model
     * @return mixed
     */
    public function update(User $user, RefCountry $model)
    {
        return $user->hasPermissionTo('update refcountries');
    }

    /**
     * Determine whether the refCountry can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefCountry  $model
     * @return mixed
     */
    public function delete(User $user, RefCountry $model)
    {
        return $user->hasPermissionTo('delete refcountries');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefCountry  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refcountries');
    }

    /**
     * Determine whether the refCountry can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefCountry  $model
     * @return mixed
     */
    public function restore(User $user, RefCountry $model)
    {
        return false;
    }

    /**
     * Determine whether the refCountry can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefCountry  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefCountry $model)
    {
        return false;
    }
}
