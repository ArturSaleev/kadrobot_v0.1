<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EmployeeTrip;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeeTripPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the employeeTrip can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list employeetrips');
    }

    /**
     * Determine whether the employeeTrip can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeTrip  $model
     * @return mixed
     */
    public function view(User $user, EmployeeTrip $model)
    {
        return $user->hasPermissionTo('view employeetrips');
    }

    /**
     * Determine whether the employeeTrip can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create employeetrips');
    }

    /**
     * Determine whether the employeeTrip can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeTrip  $model
     * @return mixed
     */
    public function update(User $user, EmployeeTrip $model)
    {
        return $user->hasPermissionTo('update employeetrips');
    }

    /**
     * Determine whether the employeeTrip can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeTrip  $model
     * @return mixed
     */
    public function delete(User $user, EmployeeTrip $model)
    {
        return $user->hasPermissionTo('delete employeetrips');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeTrip  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete employeetrips');
    }

    /**
     * Determine whether the employeeTrip can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeTrip  $model
     * @return mixed
     */
    public function restore(User $user, EmployeeTrip $model)
    {
        return false;
    }

    /**
     * Determine whether the employeeTrip can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeTrip  $model
     * @return mixed
     */
    public function forceDelete(User $user, EmployeeTrip $model)
    {
        return false;
    }
}
