<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefAction;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefActionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refAction can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refactions');
    }

    /**
     * Determine whether the refAction can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAction  $model
     * @return mixed
     */
    public function view(User $user, RefAction $model)
    {
        return $user->hasPermissionTo('view refactions');
    }

    /**
     * Determine whether the refAction can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refactions');
    }

    /**
     * Determine whether the refAction can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAction  $model
     * @return mixed
     */
    public function update(User $user, RefAction $model)
    {
        return $user->hasPermissionTo('update refactions');
    }

    /**
     * Determine whether the refAction can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAction  $model
     * @return mixed
     */
    public function delete(User $user, RefAction $model)
    {
        return $user->hasPermissionTo('delete refactions');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAction  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refactions');
    }

    /**
     * Determine whether the refAction can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAction  $model
     * @return mixed
     */
    public function restore(User $user, RefAction $model)
    {
        return false;
    }

    /**
     * Determine whether the refAction can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAction  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefAction $model)
    {
        return false;
    }
}
