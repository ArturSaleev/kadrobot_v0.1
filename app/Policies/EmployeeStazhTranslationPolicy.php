<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EmployeeStazhTranslation;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeeStazhTranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the employeeStazhTranslation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list employeestazhtranslations');
    }

    /**
     * Determine whether the employeeStazhTranslation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeStazhTranslation  $model
     * @return mixed
     */
    public function view(User $user, EmployeeStazhTranslation $model)
    {
        return $user->hasPermissionTo('view employeestazhtranslations');
    }

    /**
     * Determine whether the employeeStazhTranslation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create employeestazhtranslations');
    }

    /**
     * Determine whether the employeeStazhTranslation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeStazhTranslation  $model
     * @return mixed
     */
    public function update(User $user, EmployeeStazhTranslation $model)
    {
        return $user->hasPermissionTo('update employeestazhtranslations');
    }

    /**
     * Determine whether the employeeStazhTranslation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeStazhTranslation  $model
     * @return mixed
     */
    public function delete(User $user, EmployeeStazhTranslation $model)
    {
        return $user->hasPermissionTo('delete employeestazhtranslations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeStazhTranslation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete employeestazhtranslations');
    }

    /**
     * Determine whether the employeeStazhTranslation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeStazhTranslation  $model
     * @return mixed
     */
    public function restore(User $user, EmployeeStazhTranslation $model)
    {
        return false;
    }

    /**
     * Determine whether the employeeStazhTranslation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeStazhTranslation  $model
     * @return mixed
     */
    public function forceDelete(User $user, EmployeeStazhTranslation $model)
    {
        return false;
    }
}
