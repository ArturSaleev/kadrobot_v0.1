<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefAllowanceType;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefAllowanceTypePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refAllowanceType can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refallowancetypes');
    }

    /**
     * Determine whether the refAllowanceType can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAllowanceType  $model
     * @return mixed
     */
    public function view(User $user, RefAllowanceType $model)
    {
        return $user->hasPermissionTo('view refallowancetypes');
    }

    /**
     * Determine whether the refAllowanceType can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refallowancetypes');
    }

    /**
     * Determine whether the refAllowanceType can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAllowanceType  $model
     * @return mixed
     */
    public function update(User $user, RefAllowanceType $model)
    {
        return $user->hasPermissionTo('update refallowancetypes');
    }

    /**
     * Determine whether the refAllowanceType can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAllowanceType  $model
     * @return mixed
     */
    public function delete(User $user, RefAllowanceType $model)
    {
        return $user->hasPermissionTo('delete refallowancetypes');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAllowanceType  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refallowancetypes');
    }

    /**
     * Determine whether the refAllowanceType can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAllowanceType  $model
     * @return mixed
     */
    public function restore(User $user, RefAllowanceType $model)
    {
        return false;
    }

    /**
     * Determine whether the refAllowanceType can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAllowanceType  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefAllowanceType $model)
    {
        return false;
    }
}
