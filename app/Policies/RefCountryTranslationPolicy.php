<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefCountryTranslation;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefCountryTranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refCountryTranslation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refcountrytranslations');
    }

    /**
     * Determine whether the refCountryTranslation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefCountryTranslation  $model
     * @return mixed
     */
    public function view(User $user, RefCountryTranslation $model)
    {
        return $user->hasPermissionTo('view refcountrytranslations');
    }

    /**
     * Determine whether the refCountryTranslation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refcountrytranslations');
    }

    /**
     * Determine whether the refCountryTranslation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefCountryTranslation  $model
     * @return mixed
     */
    public function update(User $user, RefCountryTranslation $model)
    {
        return $user->hasPermissionTo('update refcountrytranslations');
    }

    /**
     * Determine whether the refCountryTranslation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefCountryTranslation  $model
     * @return mixed
     */
    public function delete(User $user, RefCountryTranslation $model)
    {
        return $user->hasPermissionTo('delete refcountrytranslations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefCountryTranslation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refcountrytranslations');
    }

    /**
     * Determine whether the refCountryTranslation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefCountryTranslation  $model
     * @return mixed
     */
    public function restore(User $user, RefCountryTranslation $model)
    {
        return false;
    }

    /**
     * Determine whether the refCountryTranslation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefCountryTranslation  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefCountryTranslation $model)
    {
        return false;
    }
}
