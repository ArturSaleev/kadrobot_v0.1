<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefDocTypeTranslation;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefDocTypeTranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refDocTypeTranslation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refdoctypetranslations');
    }

    /**
     * Determine whether the refDocTypeTranslation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefDocTypeTranslation  $model
     * @return mixed
     */
    public function view(User $user, RefDocTypeTranslation $model)
    {
        return $user->hasPermissionTo('view refdoctypetranslations');
    }

    /**
     * Determine whether the refDocTypeTranslation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refdoctypetranslations');
    }

    /**
     * Determine whether the refDocTypeTranslation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefDocTypeTranslation  $model
     * @return mixed
     */
    public function update(User $user, RefDocTypeTranslation $model)
    {
        return $user->hasPermissionTo('update refdoctypetranslations');
    }

    /**
     * Determine whether the refDocTypeTranslation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefDocTypeTranslation  $model
     * @return mixed
     */
    public function delete(User $user, RefDocTypeTranslation $model)
    {
        return $user->hasPermissionTo('delete refdoctypetranslations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefDocTypeTranslation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refdoctypetranslations');
    }

    /**
     * Determine whether the refDocTypeTranslation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefDocTypeTranslation  $model
     * @return mixed
     */
    public function restore(User $user, RefDocTypeTranslation $model)
    {
        return false;
    }

    /**
     * Determine whether the refDocTypeTranslation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefDocTypeTranslation  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefDocTypeTranslation $model)
    {
        return false;
    }
}
