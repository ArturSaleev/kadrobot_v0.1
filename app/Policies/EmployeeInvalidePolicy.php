<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EmployeeInvalide;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeeInvalidePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the employeeInvalide can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list employeeinvalides');
    }

    /**
     * Determine whether the employeeInvalide can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeInvalide  $model
     * @return mixed
     */
    public function view(User $user, EmployeeInvalide $model)
    {
        return $user->hasPermissionTo('view employeeinvalides');
    }

    /**
     * Determine whether the employeeInvalide can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create employeeinvalides');
    }

    /**
     * Determine whether the employeeInvalide can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeInvalide  $model
     * @return mixed
     */
    public function update(User $user, EmployeeInvalide $model)
    {
        return $user->hasPermissionTo('update employeeinvalides');
    }

    /**
     * Determine whether the employeeInvalide can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeInvalide  $model
     * @return mixed
     */
    public function delete(User $user, EmployeeInvalide $model)
    {
        return $user->hasPermissionTo('delete employeeinvalides');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeInvalide  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete employeeinvalides');
    }

    /**
     * Determine whether the employeeInvalide can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeInvalide  $model
     * @return mixed
     */
    public function restore(User $user, EmployeeInvalide $model)
    {
        return false;
    }

    /**
     * Determine whether the employeeInvalide can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeInvalide  $model
     * @return mixed
     */
    public function forceDelete(User $user, EmployeeInvalide $model)
    {
        return false;
    }
}
