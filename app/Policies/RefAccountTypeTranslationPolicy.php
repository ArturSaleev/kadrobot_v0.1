<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefAccountTypeTranslation;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefAccountTypeTranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refAccountTypeTranslation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refaccounttypetranslations');
    }

    /**
     * Determine whether the refAccountTypeTranslation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAccountTypeTranslation  $model
     * @return mixed
     */
    public function view(User $user, RefAccountTypeTranslation $model)
    {
        return $user->hasPermissionTo('view refaccounttypetranslations');
    }

    /**
     * Determine whether the refAccountTypeTranslation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refaccounttypetranslations');
    }

    /**
     * Determine whether the refAccountTypeTranslation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAccountTypeTranslation  $model
     * @return mixed
     */
    public function update(User $user, RefAccountTypeTranslation $model)
    {
        return $user->hasPermissionTo('update refaccounttypetranslations');
    }

    /**
     * Determine whether the refAccountTypeTranslation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAccountTypeTranslation  $model
     * @return mixed
     */
    public function delete(User $user, RefAccountTypeTranslation $model)
    {
        return $user->hasPermissionTo('delete refaccounttypetranslations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAccountTypeTranslation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refaccounttypetranslations');
    }

    /**
     * Determine whether the refAccountTypeTranslation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAccountTypeTranslation  $model
     * @return mixed
     */
    public function restore(User $user, RefAccountTypeTranslation $model)
    {
        return false;
    }

    /**
     * Determine whether the refAccountTypeTranslation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAccountTypeTranslation  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefAccountTypeTranslation $model)
    {
        return false;
    }
}
