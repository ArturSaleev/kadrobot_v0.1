<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EmployeePhone;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeePhonePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the employeePhone can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list employeephones');
    }

    /**
     * Determine whether the employeePhone can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeePhone  $model
     * @return mixed
     */
    public function view(User $user, EmployeePhone $model)
    {
        return $user->hasPermissionTo('view employeephones');
    }

    /**
     * Determine whether the employeePhone can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create employeephones');
    }

    /**
     * Determine whether the employeePhone can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeePhone  $model
     * @return mixed
     */
    public function update(User $user, EmployeePhone $model)
    {
        return $user->hasPermissionTo('update employeephones');
    }

    /**
     * Determine whether the employeePhone can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeePhone  $model
     * @return mixed
     */
    public function delete(User $user, EmployeePhone $model)
    {
        return $user->hasPermissionTo('delete employeephones');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeePhone  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete employeephones');
    }

    /**
     * Determine whether the employeePhone can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeePhone  $model
     * @return mixed
     */
    public function restore(User $user, EmployeePhone $model)
    {
        return false;
    }

    /**
     * Determine whether the employeePhone can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeePhone  $model
     * @return mixed
     */
    public function forceDelete(User $user, EmployeePhone $model)
    {
        return false;
    }
}
