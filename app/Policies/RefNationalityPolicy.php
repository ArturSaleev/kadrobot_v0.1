<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefNationality;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefNationalityPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refNationality can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refnationalities');
    }

    /**
     * Determine whether the refNationality can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefNationality  $model
     * @return mixed
     */
    public function view(User $user, RefNationality $model)
    {
        return $user->hasPermissionTo('view refnationalities');
    }

    /**
     * Determine whether the refNationality can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refnationalities');
    }

    /**
     * Determine whether the refNationality can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefNationality  $model
     * @return mixed
     */
    public function update(User $user, RefNationality $model)
    {
        return $user->hasPermissionTo('update refnationalities');
    }

    /**
     * Determine whether the refNationality can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefNationality  $model
     * @return mixed
     */
    public function delete(User $user, RefNationality $model)
    {
        return $user->hasPermissionTo('delete refnationalities');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefNationality  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refnationalities');
    }

    /**
     * Determine whether the refNationality can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefNationality  $model
     * @return mixed
     */
    public function restore(User $user, RefNationality $model)
    {
        return false;
    }

    /**
     * Determine whether the refNationality can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefNationality  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefNationality $model)
    {
        return false;
    }
}
