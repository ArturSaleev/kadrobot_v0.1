<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefVidHolidayTypeTranslation;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefVidHolidayTypeTranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refVidHolidayTypeTranslation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refvidholidaytypetranslations');
    }

    /**
     * Determine whether the refVidHolidayTypeTranslation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefVidHolidayTypeTranslation  $model
     * @return mixed
     */
    public function view(User $user, RefVidHolidayTypeTranslation $model)
    {
        return $user->hasPermissionTo('view refvidholidaytypetranslations');
    }

    /**
     * Determine whether the refVidHolidayTypeTranslation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refvidholidaytypetranslations');
    }

    /**
     * Determine whether the refVidHolidayTypeTranslation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefVidHolidayTypeTranslation  $model
     * @return mixed
     */
    public function update(User $user, RefVidHolidayTypeTranslation $model)
    {
        return $user->hasPermissionTo('update refvidholidaytypetranslations');
    }

    /**
     * Determine whether the refVidHolidayTypeTranslation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefVidHolidayTypeTranslation  $model
     * @return mixed
     */
    public function delete(User $user, RefVidHolidayTypeTranslation $model)
    {
        return $user->hasPermissionTo('delete refvidholidaytypetranslations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefVidHolidayTypeTranslation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refvidholidaytypetranslations');
    }

    /**
     * Determine whether the refVidHolidayTypeTranslation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefVidHolidayTypeTranslation  $model
     * @return mixed
     */
    public function restore(User $user, RefVidHolidayTypeTranslation $model)
    {
        return false;
    }

    /**
     * Determine whether the refVidHolidayTypeTranslation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefVidHolidayTypeTranslation  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefVidHolidayTypeTranslation $model)
    {
        return false;
    }
}
