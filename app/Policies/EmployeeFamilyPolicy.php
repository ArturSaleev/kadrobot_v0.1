<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EmployeeFamily;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeeFamilyPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the employeeFamily can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list employeefamilies');
    }

    /**
     * Determine whether the employeeFamily can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeFamily  $model
     * @return mixed
     */
    public function view(User $user, EmployeeFamily $model)
    {
        return $user->hasPermissionTo('view employeefamilies');
    }

    /**
     * Determine whether the employeeFamily can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create employeefamilies');
    }

    /**
     * Determine whether the employeeFamily can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeFamily  $model
     * @return mixed
     */
    public function update(User $user, EmployeeFamily $model)
    {
        return $user->hasPermissionTo('update employeefamilies');
    }

    /**
     * Determine whether the employeeFamily can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeFamily  $model
     * @return mixed
     */
    public function delete(User $user, EmployeeFamily $model)
    {
        return $user->hasPermissionTo('delete employeefamilies');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeFamily  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete employeefamilies');
    }

    /**
     * Determine whether the employeeFamily can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeFamily  $model
     * @return mixed
     */
    public function restore(User $user, EmployeeFamily $model)
    {
        return false;
    }

    /**
     * Determine whether the employeeFamily can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeFamily  $model
     * @return mixed
     */
    public function forceDelete(User $user, EmployeeFamily $model)
    {
        return false;
    }
}
