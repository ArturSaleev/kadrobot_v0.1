<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefNationalityTranslation;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefNationalityTranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refNationalityTranslation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refnationalitytranslations');
    }

    /**
     * Determine whether the refNationalityTranslation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefNationalityTranslation  $model
     * @return mixed
     */
    public function view(User $user, RefNationalityTranslation $model)
    {
        return $user->hasPermissionTo('view refnationalitytranslations');
    }

    /**
     * Determine whether the refNationalityTranslation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refnationalitytranslations');
    }

    /**
     * Determine whether the refNationalityTranslation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefNationalityTranslation  $model
     * @return mixed
     */
    public function update(User $user, RefNationalityTranslation $model)
    {
        return $user->hasPermissionTo('update refnationalitytranslations');
    }

    /**
     * Determine whether the refNationalityTranslation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefNationalityTranslation  $model
     * @return mixed
     */
    public function delete(User $user, RefNationalityTranslation $model)
    {
        return $user->hasPermissionTo('delete refnationalitytranslations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefNationalityTranslation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refnationalitytranslations');
    }

    /**
     * Determine whether the refNationalityTranslation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefNationalityTranslation  $model
     * @return mixed
     */
    public function restore(User $user, RefNationalityTranslation $model)
    {
        return false;
    }

    /**
     * Determine whether the refNationalityTranslation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefNationalityTranslation  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefNationalityTranslation $model)
    {
        return false;
    }
}
