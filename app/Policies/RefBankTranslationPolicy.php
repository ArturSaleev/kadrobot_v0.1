<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefBankTranslation;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefBankTranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refBankTranslation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refbanktranslations');
    }

    /**
     * Determine whether the refBankTranslation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefBankTranslation  $model
     * @return mixed
     */
    public function view(User $user, RefBankTranslation $model)
    {
        return $user->hasPermissionTo('view refbanktranslations');
    }

    /**
     * Determine whether the refBankTranslation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refbanktranslations');
    }

    /**
     * Determine whether the refBankTranslation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefBankTranslation  $model
     * @return mixed
     */
    public function update(User $user, RefBankTranslation $model)
    {
        return $user->hasPermissionTo('update refbanktranslations');
    }

    /**
     * Determine whether the refBankTranslation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefBankTranslation  $model
     * @return mixed
     */
    public function delete(User $user, RefBankTranslation $model)
    {
        return $user->hasPermissionTo('delete refbanktranslations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefBankTranslation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refbanktranslations');
    }

    /**
     * Determine whether the refBankTranslation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefBankTranslation  $model
     * @return mixed
     */
    public function restore(User $user, RefBankTranslation $model)
    {
        return false;
    }

    /**
     * Determine whether the refBankTranslation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefBankTranslation  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefBankTranslation $model)
    {
        return false;
    }
}
