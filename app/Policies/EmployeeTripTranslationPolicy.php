<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EmployeeTripTranslation;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeeTripTranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the employeeTripTranslation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list employeetriptranslations');
    }

    /**
     * Determine whether the employeeTripTranslation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeTripTranslation  $model
     * @return mixed
     */
    public function view(User $user, EmployeeTripTranslation $model)
    {
        return $user->hasPermissionTo('view employeetriptranslations');
    }

    /**
     * Determine whether the employeeTripTranslation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create employeetriptranslations');
    }

    /**
     * Determine whether the employeeTripTranslation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeTripTranslation  $model
     * @return mixed
     */
    public function update(User $user, EmployeeTripTranslation $model)
    {
        return $user->hasPermissionTo('update employeetriptranslations');
    }

    /**
     * Determine whether the employeeTripTranslation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeTripTranslation  $model
     * @return mixed
     */
    public function delete(User $user, EmployeeTripTranslation $model)
    {
        return $user->hasPermissionTo('delete employeetriptranslations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeTripTranslation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete employeetriptranslations');
    }

    /**
     * Determine whether the employeeTripTranslation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeTripTranslation  $model
     * @return mixed
     */
    public function restore(User $user, EmployeeTripTranslation $model)
    {
        return false;
    }

    /**
     * Determine whether the employeeTripTranslation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeTripTranslation  $model
     * @return mixed
     */
    public function forceDelete(User $user, EmployeeTripTranslation $model)
    {
        return false;
    }
}
