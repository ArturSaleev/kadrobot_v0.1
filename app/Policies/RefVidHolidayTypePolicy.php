<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefVidHolidayType;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefVidHolidayTypePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refVidHolidayType can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refvidholidaytypes');
    }

    /**
     * Determine whether the refVidHolidayType can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefVidHolidayType  $model
     * @return mixed
     */
    public function view(User $user, RefVidHolidayType $model)
    {
        return $user->hasPermissionTo('view refvidholidaytypes');
    }

    /**
     * Determine whether the refVidHolidayType can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refvidholidaytypes');
    }

    /**
     * Determine whether the refVidHolidayType can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefVidHolidayType  $model
     * @return mixed
     */
    public function update(User $user, RefVidHolidayType $model)
    {
        return $user->hasPermissionTo('update refvidholidaytypes');
    }

    /**
     * Determine whether the refVidHolidayType can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefVidHolidayType  $model
     * @return mixed
     */
    public function delete(User $user, RefVidHolidayType $model)
    {
        return $user->hasPermissionTo('delete refvidholidaytypes');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefVidHolidayType  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refvidholidaytypes');
    }

    /**
     * Determine whether the refVidHolidayType can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefVidHolidayType  $model
     * @return mixed
     */
    public function restore(User $user, RefVidHolidayType $model)
    {
        return false;
    }

    /**
     * Determine whether the refVidHolidayType can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefVidHolidayType  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefVidHolidayType $model)
    {
        return false;
    }
}
