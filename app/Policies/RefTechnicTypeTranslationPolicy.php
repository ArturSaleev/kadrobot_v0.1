<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefTechnicTypeTranslation;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefTechnicTypeTranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refTechnicTypeTranslation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list reftechnictypetranslations');
    }

    /**
     * Determine whether the refTechnicTypeTranslation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTechnicTypeTranslation  $model
     * @return mixed
     */
    public function view(User $user, RefTechnicTypeTranslation $model)
    {
        return $user->hasPermissionTo('view reftechnictypetranslations');
    }

    /**
     * Determine whether the refTechnicTypeTranslation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create reftechnictypetranslations');
    }

    /**
     * Determine whether the refTechnicTypeTranslation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTechnicTypeTranslation  $model
     * @return mixed
     */
    public function update(User $user, RefTechnicTypeTranslation $model)
    {
        return $user->hasPermissionTo('update reftechnictypetranslations');
    }

    /**
     * Determine whether the refTechnicTypeTranslation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTechnicTypeTranslation  $model
     * @return mixed
     */
    public function delete(User $user, RefTechnicTypeTranslation $model)
    {
        return $user->hasPermissionTo('delete reftechnictypetranslations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTechnicTypeTranslation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete reftechnictypetranslations');
    }

    /**
     * Determine whether the refTechnicTypeTranslation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTechnicTypeTranslation  $model
     * @return mixed
     */
    public function restore(User $user, RefTechnicTypeTranslation $model)
    {
        return false;
    }

    /**
     * Determine whether the refTechnicTypeTranslation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTechnicTypeTranslation  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefTechnicTypeTranslation $model)
    {
        return false;
    }
}
