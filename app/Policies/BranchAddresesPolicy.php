<?php

namespace App\Policies;

use App\Models\User;
use App\Models\BranchAddreses;
use Illuminate\Auth\Access\HandlesAuthorization;

class BranchAddresesPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the branchAddreses can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list allbranchaddreses');
    }

    /**
     * Determine whether the branchAddreses can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BranchAddreses  $model
     * @return mixed
     */
    public function view(User $user, BranchAddreses $model)
    {
        return $user->hasPermissionTo('view allbranchaddreses');
    }

    /**
     * Determine whether the branchAddreses can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create allbranchaddreses');
    }

    /**
     * Determine whether the branchAddreses can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BranchAddreses  $model
     * @return mixed
     */
    public function update(User $user, BranchAddreses $model)
    {
        return $user->hasPermissionTo('update allbranchaddreses');
    }

    /**
     * Determine whether the branchAddreses can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BranchAddreses  $model
     * @return mixed
     */
    public function delete(User $user, BranchAddreses $model)
    {
        return $user->hasPermissionTo('delete allbranchaddreses');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BranchAddreses  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete allbranchaddreses');
    }

    /**
     * Determine whether the branchAddreses can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BranchAddreses  $model
     * @return mixed
     */
    public function restore(User $user, BranchAddreses $model)
    {
        return false;
    }

    /**
     * Determine whether the branchAddreses can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BranchAddreses  $model
     * @return mixed
     */
    public function forceDelete(User $user, BranchAddreses $model)
    {
        return false;
    }
}
