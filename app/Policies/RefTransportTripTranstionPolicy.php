<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefTransportTripTranstion;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefTransportTripTranstionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refTransportTripTranstion can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list reftransporttriptranstions');
    }

    /**
     * Determine whether the refTransportTripTranstion can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTransportTripTranstion  $model
     * @return mixed
     */
    public function view(User $user, RefTransportTripTranstion $model)
    {
        return $user->hasPermissionTo('view reftransporttriptranstions');
    }

    /**
     * Determine whether the refTransportTripTranstion can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create reftransporttriptranstions');
    }

    /**
     * Determine whether the refTransportTripTranstion can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTransportTripTranstion  $model
     * @return mixed
     */
    public function update(User $user, RefTransportTripTranstion $model)
    {
        return $user->hasPermissionTo('update reftransporttriptranstions');
    }

    /**
     * Determine whether the refTransportTripTranstion can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTransportTripTranstion  $model
     * @return mixed
     */
    public function delete(User $user, RefTransportTripTranstion $model)
    {
        return $user->hasPermissionTo('delete reftransporttriptranstions');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTransportTripTranstion  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete reftransporttriptranstions');
    }

    /**
     * Determine whether the refTransportTripTranstion can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTransportTripTranstion  $model
     * @return mixed
     */
    public function restore(User $user, RefTransportTripTranstion $model)
    {
        return false;
    }

    /**
     * Determine whether the refTransportTripTranstion can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTransportTripTranstion  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefTransportTripTranstion $model)
    {
        return false;
    }
}
