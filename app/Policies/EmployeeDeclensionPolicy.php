<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EmployeeDeclension;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeeDeclensionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the employeeDeclension can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list employeedeclensions');
    }

    /**
     * Determine whether the employeeDeclension can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeDeclension  $model
     * @return mixed
     */
    public function view(User $user, EmployeeDeclension $model)
    {
        return $user->hasPermissionTo('view employeedeclensions');
    }

    /**
     * Determine whether the employeeDeclension can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create employeedeclensions');
    }

    /**
     * Determine whether the employeeDeclension can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeDeclension  $model
     * @return mixed
     */
    public function update(User $user, EmployeeDeclension $model)
    {
        return $user->hasPermissionTo('update employeedeclensions');
    }

    /**
     * Determine whether the employeeDeclension can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeDeclension  $model
     * @return mixed
     */
    public function delete(User $user, EmployeeDeclension $model)
    {
        return $user->hasPermissionTo('delete employeedeclensions');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeDeclension  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete employeedeclensions');
    }

    /**
     * Determine whether the employeeDeclension can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeDeclension  $model
     * @return mixed
     */
    public function restore(User $user, EmployeeDeclension $model)
    {
        return false;
    }

    /**
     * Determine whether the employeeDeclension can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeDeclension  $model
     * @return mixed
     */
    public function forceDelete(User $user, EmployeeDeclension $model)
    {
        return false;
    }
}
