<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Curator;
use Illuminate\Auth\Access\HandlesAuthorization;

class CuratorPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the curator can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list curators');
    }

    /**
     * Determine whether the curator can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Curator  $model
     * @return mixed
     */
    public function view(User $user, Curator $model)
    {
        return $user->hasPermissionTo('view curators');
    }

    /**
     * Determine whether the curator can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create curators');
    }

    /**
     * Determine whether the curator can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Curator  $model
     * @return mixed
     */
    public function update(User $user, Curator $model)
    {
        return $user->hasPermissionTo('update curators');
    }

    /**
     * Determine whether the curator can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Curator  $model
     * @return mixed
     */
    public function delete(User $user, Curator $model)
    {
        return $user->hasPermissionTo('delete curators');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Curator  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete curators');
    }

    /**
     * Determine whether the curator can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Curator  $model
     * @return mixed
     */
    public function restore(User $user, Curator $model)
    {
        return false;
    }

    /**
     * Determine whether the curator can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Curator  $model
     * @return mixed
     */
    public function forceDelete(User $user, Curator $model)
    {
        return false;
    }
}
