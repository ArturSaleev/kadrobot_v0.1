<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EmployeeMilitary;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeeMilitaryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the employeeMilitary can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list employeemilitaries');
    }

    /**
     * Determine whether the employeeMilitary can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeMilitary  $model
     * @return mixed
     */
    public function view(User $user, EmployeeMilitary $model)
    {
        return $user->hasPermissionTo('view employeemilitaries');
    }

    /**
     * Determine whether the employeeMilitary can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create employeemilitaries');
    }

    /**
     * Determine whether the employeeMilitary can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeMilitary  $model
     * @return mixed
     */
    public function update(User $user, EmployeeMilitary $model)
    {
        return $user->hasPermissionTo('update employeemilitaries');
    }

    /**
     * Determine whether the employeeMilitary can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeMilitary  $model
     * @return mixed
     */
    public function delete(User $user, EmployeeMilitary $model)
    {
        return $user->hasPermissionTo('delete employeemilitaries');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeMilitary  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete employeemilitaries');
    }

    /**
     * Determine whether the employeeMilitary can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeMilitary  $model
     * @return mixed
     */
    public function restore(User $user, EmployeeMilitary $model)
    {
        return false;
    }

    /**
     * Determine whether the employeeMilitary can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeMilitary  $model
     * @return mixed
     */
    public function forceDelete(User $user, EmployeeMilitary $model)
    {
        return false;
    }
}
