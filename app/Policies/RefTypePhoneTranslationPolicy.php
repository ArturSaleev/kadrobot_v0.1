<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefTypePhoneTranslation;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefTypePhoneTranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refTypePhoneTranslation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list reftypephonetranslations');
    }

    /**
     * Determine whether the refTypePhoneTranslation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTypePhoneTranslation  $model
     * @return mixed
     */
    public function view(User $user, RefTypePhoneTranslation $model)
    {
        return $user->hasPermissionTo('view reftypephonetranslations');
    }

    /**
     * Determine whether the refTypePhoneTranslation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create reftypephonetranslations');
    }

    /**
     * Determine whether the refTypePhoneTranslation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTypePhoneTranslation  $model
     * @return mixed
     */
    public function update(User $user, RefTypePhoneTranslation $model)
    {
        return $user->hasPermissionTo('update reftypephonetranslations');
    }

    /**
     * Determine whether the refTypePhoneTranslation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTypePhoneTranslation  $model
     * @return mixed
     */
    public function delete(User $user, RefTypePhoneTranslation $model)
    {
        return $user->hasPermissionTo('delete reftypephonetranslations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTypePhoneTranslation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete reftypephonetranslations');
    }

    /**
     * Determine whether the refTypePhoneTranslation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTypePhoneTranslation  $model
     * @return mixed
     */
    public function restore(User $user, RefTypePhoneTranslation $model)
    {
        return false;
    }

    /**
     * Determine whether the refTypePhoneTranslation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTypePhoneTranslation  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefTypePhoneTranslation $model)
    {
        return false;
    }
}
