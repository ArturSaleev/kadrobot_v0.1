<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefVidHolidayTranslation;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefVidHolidayTranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refVidHolidayTranslation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refvidholidaytranslations');
    }

    /**
     * Determine whether the refVidHolidayTranslation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefVidHolidayTranslation  $model
     * @return mixed
     */
    public function view(User $user, RefVidHolidayTranslation $model)
    {
        return $user->hasPermissionTo('view refvidholidaytranslations');
    }

    /**
     * Determine whether the refVidHolidayTranslation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refvidholidaytranslations');
    }

    /**
     * Determine whether the refVidHolidayTranslation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefVidHolidayTranslation  $model
     * @return mixed
     */
    public function update(User $user, RefVidHolidayTranslation $model)
    {
        return $user->hasPermissionTo('update refvidholidaytranslations');
    }

    /**
     * Determine whether the refVidHolidayTranslation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefVidHolidayTranslation  $model
     * @return mixed
     */
    public function delete(User $user, RefVidHolidayTranslation $model)
    {
        return $user->hasPermissionTo('delete refvidholidaytranslations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefVidHolidayTranslation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refvidholidaytranslations');
    }

    /**
     * Determine whether the refVidHolidayTranslation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefVidHolidayTranslation  $model
     * @return mixed
     */
    public function restore(User $user, RefVidHolidayTranslation $model)
    {
        return false;
    }

    /**
     * Determine whether the refVidHolidayTranslation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefVidHolidayTranslation  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefVidHolidayTranslation $model)
    {
        return false;
    }
}
