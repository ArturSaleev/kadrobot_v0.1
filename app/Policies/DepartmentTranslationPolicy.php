<?php

namespace App\Policies;

use App\Models\User;
use App\Models\DepartmentTranslation;
use Illuminate\Auth\Access\HandlesAuthorization;

class DepartmentTranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the departmentTranslation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list departmenttranslations');
    }

    /**
     * Determine whether the departmentTranslation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\DepartmentTranslation  $model
     * @return mixed
     */
    public function view(User $user, DepartmentTranslation $model)
    {
        return $user->hasPermissionTo('view departmenttranslations');
    }

    /**
     * Determine whether the departmentTranslation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create departmenttranslations');
    }

    /**
     * Determine whether the departmentTranslation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\DepartmentTranslation  $model
     * @return mixed
     */
    public function update(User $user, DepartmentTranslation $model)
    {
        return $user->hasPermissionTo('update departmenttranslations');
    }

    /**
     * Determine whether the departmentTranslation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\DepartmentTranslation  $model
     * @return mixed
     */
    public function delete(User $user, DepartmentTranslation $model)
    {
        return $user->hasPermissionTo('delete departmenttranslations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\DepartmentTranslation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete departmenttranslations');
    }

    /**
     * Determine whether the departmentTranslation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\DepartmentTranslation  $model
     * @return mixed
     */
    public function restore(User $user, DepartmentTranslation $model)
    {
        return false;
    }

    /**
     * Determine whether the departmentTranslation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\DepartmentTranslation  $model
     * @return mixed
     */
    public function forceDelete(User $user, DepartmentTranslation $model)
    {
        return false;
    }
}
