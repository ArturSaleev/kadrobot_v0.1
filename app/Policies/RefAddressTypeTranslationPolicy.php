<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefAddressTypeTranslation;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefAddressTypeTranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refAddressTypeTranslation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refaddresstypetranslations');
    }

    /**
     * Determine whether the refAddressTypeTranslation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAddressTypeTranslation  $model
     * @return mixed
     */
    public function view(User $user, RefAddressTypeTranslation $model)
    {
        return $user->hasPermissionTo('view refaddresstypetranslations');
    }

    /**
     * Determine whether the refAddressTypeTranslation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refaddresstypetranslations');
    }

    /**
     * Determine whether the refAddressTypeTranslation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAddressTypeTranslation  $model
     * @return mixed
     */
    public function update(User $user, RefAddressTypeTranslation $model)
    {
        return $user->hasPermissionTo('update refaddresstypetranslations');
    }

    /**
     * Determine whether the refAddressTypeTranslation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAddressTypeTranslation  $model
     * @return mixed
     */
    public function delete(User $user, RefAddressTypeTranslation $model)
    {
        return $user->hasPermissionTo('delete refaddresstypetranslations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAddressTypeTranslation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refaddresstypetranslations');
    }

    /**
     * Determine whether the refAddressTypeTranslation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAddressTypeTranslation  $model
     * @return mixed
     */
    public function restore(User $user, RefAddressTypeTranslation $model)
    {
        return false;
    }

    /**
     * Determine whether the refAddressTypeTranslation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAddressTypeTranslation  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefAddressTypeTranslation $model)
    {
        return false;
    }
}
