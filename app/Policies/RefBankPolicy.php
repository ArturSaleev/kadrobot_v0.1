<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefBank;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefBankPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refBank can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refbanks');
    }

    /**
     * Determine whether the refBank can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefBank  $model
     * @return mixed
     */
    public function view(User $user, RefBank $model)
    {
        return $user->hasPermissionTo('view refbanks');
    }

    /**
     * Determine whether the refBank can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refbanks');
    }

    /**
     * Determine whether the refBank can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefBank  $model
     * @return mixed
     */
    public function update(User $user, RefBank $model)
    {
        return $user->hasPermissionTo('update refbanks');
    }

    /**
     * Determine whether the refBank can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefBank  $model
     * @return mixed
     */
    public function delete(User $user, RefBank $model)
    {
        return $user->hasPermissionTo('delete refbanks');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefBank  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refbanks');
    }

    /**
     * Determine whether the refBank can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefBank  $model
     * @return mixed
     */
    public function restore(User $user, RefBank $model)
    {
        return false;
    }

    /**
     * Determine whether the refBank can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefBank  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefBank $model)
    {
        return false;
    }
}
