<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefStatusTranslation;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefStatusTranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refStatusTranslation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refstatustranslations');
    }

    /**
     * Determine whether the refStatusTranslation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefStatusTranslation  $model
     * @return mixed
     */
    public function view(User $user, RefStatusTranslation $model)
    {
        return $user->hasPermissionTo('view refstatustranslations');
    }

    /**
     * Determine whether the refStatusTranslation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refstatustranslations');
    }

    /**
     * Determine whether the refStatusTranslation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefStatusTranslation  $model
     * @return mixed
     */
    public function update(User $user, RefStatusTranslation $model)
    {
        return $user->hasPermissionTo('update refstatustranslations');
    }

    /**
     * Determine whether the refStatusTranslation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefStatusTranslation  $model
     * @return mixed
     */
    public function delete(User $user, RefStatusTranslation $model)
    {
        return $user->hasPermissionTo('delete refstatustranslations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefStatusTranslation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refstatustranslations');
    }

    /**
     * Determine whether the refStatusTranslation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefStatusTranslation  $model
     * @return mixed
     */
    public function restore(User $user, RefStatusTranslation $model)
    {
        return false;
    }

    /**
     * Determine whether the refStatusTranslation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefStatusTranslation  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefStatusTranslation $model)
    {
        return false;
    }
}
