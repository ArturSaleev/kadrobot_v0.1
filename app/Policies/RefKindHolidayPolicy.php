<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefKindHoliday;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefKindHolidayPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refKindHoliday can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refkindholidays');
    }

    /**
     * Determine whether the refKindHoliday can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefKindHoliday  $model
     * @return mixed
     */
    public function view(User $user, RefKindHoliday $model)
    {
        return $user->hasPermissionTo('view refkindholidays');
    }

    /**
     * Determine whether the refKindHoliday can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refkindholidays');
    }

    /**
     * Determine whether the refKindHoliday can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefKindHoliday  $model
     * @return mixed
     */
    public function update(User $user, RefKindHoliday $model)
    {
        return $user->hasPermissionTo('update refkindholidays');
    }

    /**
     * Determine whether the refKindHoliday can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefKindHoliday  $model
     * @return mixed
     */
    public function delete(User $user, RefKindHoliday $model)
    {
        return $user->hasPermissionTo('delete refkindholidays');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefKindHoliday  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refkindholidays');
    }

    /**
     * Determine whether the refKindHoliday can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefKindHoliday  $model
     * @return mixed
     */
    public function restore(User $user, RefKindHoliday $model)
    {
        return false;
    }

    /**
     * Determine whether the refKindHoliday can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefKindHoliday  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefKindHoliday $model)
    {
        return false;
    }
}
