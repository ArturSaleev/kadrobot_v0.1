<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefStatus;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefStatusPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refStatus can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refstatuses');
    }

    /**
     * Determine whether the refStatus can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefStatus  $model
     * @return mixed
     */
    public function view(User $user, RefStatus $model)
    {
        return $user->hasPermissionTo('view refstatuses');
    }

    /**
     * Determine whether the refStatus can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refstatuses');
    }

    /**
     * Determine whether the refStatus can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefStatus  $model
     * @return mixed
     */
    public function update(User $user, RefStatus $model)
    {
        return $user->hasPermissionTo('update refstatuses');
    }

    /**
     * Determine whether the refStatus can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefStatus  $model
     * @return mixed
     */
    public function delete(User $user, RefStatus $model)
    {
        return $user->hasPermissionTo('delete refstatuses');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefStatus  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refstatuses');
    }

    /**
     * Determine whether the refStatus can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefStatus  $model
     * @return mixed
     */
    public function restore(User $user, RefStatus $model)
    {
        return false;
    }

    /**
     * Determine whether the refStatus can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefStatus  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefStatus $model)
    {
        return false;
    }
}
