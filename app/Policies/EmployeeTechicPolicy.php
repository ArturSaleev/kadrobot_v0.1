<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EmployeeTechic;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeeTechicPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the employeeTechic can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list employeetechics');
    }

    /**
     * Determine whether the employeeTechic can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeTechic  $model
     * @return mixed
     */
    public function view(User $user, EmployeeTechic $model)
    {
        return $user->hasPermissionTo('view employeetechics');
    }

    /**
     * Determine whether the employeeTechic can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create employeetechics');
    }

    /**
     * Determine whether the employeeTechic can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeTechic  $model
     * @return mixed
     */
    public function update(User $user, EmployeeTechic $model)
    {
        return $user->hasPermissionTo('update employeetechics');
    }

    /**
     * Determine whether the employeeTechic can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeTechic  $model
     * @return mixed
     */
    public function delete(User $user, EmployeeTechic $model)
    {
        return $user->hasPermissionTo('delete employeetechics');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeTechic  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete employeetechics');
    }

    /**
     * Determine whether the employeeTechic can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeTechic  $model
     * @return mixed
     */
    public function restore(User $user, EmployeeTechic $model)
    {
        return false;
    }

    /**
     * Determine whether the employeeTechic can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeTechic  $model
     * @return mixed
     */
    public function forceDelete(User $user, EmployeeTechic $model)
    {
        return false;
    }
}
