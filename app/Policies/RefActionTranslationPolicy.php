<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefActionTranslation;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefActionTranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refActionTranslation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refactiontranslations');
    }

    /**
     * Determine whether the refActionTranslation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefActionTranslation  $model
     * @return mixed
     */
    public function view(User $user, RefActionTranslation $model)
    {
        return $user->hasPermissionTo('view refactiontranslations');
    }

    /**
     * Determine whether the refActionTranslation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refactiontranslations');
    }

    /**
     * Determine whether the refActionTranslation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefActionTranslation  $model
     * @return mixed
     */
    public function update(User $user, RefActionTranslation $model)
    {
        return $user->hasPermissionTo('update refactiontranslations');
    }

    /**
     * Determine whether the refActionTranslation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefActionTranslation  $model
     * @return mixed
     */
    public function delete(User $user, RefActionTranslation $model)
    {
        return $user->hasPermissionTo('delete refactiontranslations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefActionTranslation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refactiontranslations');
    }

    /**
     * Determine whether the refActionTranslation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefActionTranslation  $model
     * @return mixed
     */
    public function restore(User $user, RefActionTranslation $model)
    {
        return false;
    }

    /**
     * Determine whether the refActionTranslation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefActionTranslation  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefActionTranslation $model)
    {
        return false;
    }
}
