<?php

namespace App\Policies;

use App\Models\User;
use App\Models\BranchTranslation;
use Illuminate\Auth\Access\HandlesAuthorization;

class BranchTranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the branchTranslation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list branchtranslations');
    }

    /**
     * Determine whether the branchTranslation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BranchTranslation  $model
     * @return mixed
     */
    public function view(User $user, BranchTranslation $model)
    {
        return $user->hasPermissionTo('view branchtranslations');
    }

    /**
     * Determine whether the branchTranslation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create branchtranslations');
    }

    /**
     * Determine whether the branchTranslation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BranchTranslation  $model
     * @return mixed
     */
    public function update(User $user, BranchTranslation $model)
    {
        return $user->hasPermissionTo('update branchtranslations');
    }

    /**
     * Determine whether the branchTranslation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BranchTranslation  $model
     * @return mixed
     */
    public function delete(User $user, BranchTranslation $model)
    {
        return $user->hasPermissionTo('delete branchtranslations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BranchTranslation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete branchtranslations');
    }

    /**
     * Determine whether the branchTranslation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BranchTranslation  $model
     * @return mixed
     */
    public function restore(User $user, BranchTranslation $model)
    {
        return false;
    }

    /**
     * Determine whether the branchTranslation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BranchTranslation  $model
     * @return mixed
     */
    public function forceDelete(User $user, BranchTranslation $model)
    {
        return false;
    }
}
