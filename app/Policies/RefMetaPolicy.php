<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefMeta;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefMetaPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refMeta can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refmetas');
    }

    /**
     * Determine whether the refMeta can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefMeta  $model
     * @return mixed
     */
    public function view(User $user, RefMeta $model)
    {
        return $user->hasPermissionTo('view refmetas');
    }

    /**
     * Determine whether the refMeta can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refmetas');
    }

    /**
     * Determine whether the refMeta can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefMeta  $model
     * @return mixed
     */
    public function update(User $user, RefMeta $model)
    {
        return $user->hasPermissionTo('update refmetas');
    }

    /**
     * Determine whether the refMeta can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefMeta  $model
     * @return mixed
     */
    public function delete(User $user, RefMeta $model)
    {
        return $user->hasPermissionTo('delete refmetas');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefMeta  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refmetas');
    }

    /**
     * Determine whether the refMeta can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefMeta  $model
     * @return mixed
     */
    public function restore(User $user, RefMeta $model)
    {
        return false;
    }

    /**
     * Determine whether the refMeta can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefMeta  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefMeta $model)
    {
        return false;
    }
}
