<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefTypePhone;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefTypePhonePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refTypePhone can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list reftypephones');
    }

    /**
     * Determine whether the refTypePhone can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTypePhone  $model
     * @return mixed
     */
    public function view(User $user, RefTypePhone $model)
    {
        return $user->hasPermissionTo('view reftypephones');
    }

    /**
     * Determine whether the refTypePhone can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create reftypephones');
    }

    /**
     * Determine whether the refTypePhone can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTypePhone  $model
     * @return mixed
     */
    public function update(User $user, RefTypePhone $model)
    {
        return $user->hasPermissionTo('update reftypephones');
    }

    /**
     * Determine whether the refTypePhone can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTypePhone  $model
     * @return mixed
     */
    public function delete(User $user, RefTypePhone $model)
    {
        return $user->hasPermissionTo('delete reftypephones');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTypePhone  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete reftypephones');
    }

    /**
     * Determine whether the refTypePhone can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTypePhone  $model
     * @return mixed
     */
    public function restore(User $user, RefTypePhone $model)
    {
        return false;
    }

    /**
     * Determine whether the refTypePhone can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTypePhone  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefTypePhone $model)
    {
        return false;
    }
}
