<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefSex;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefSexPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refSex can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refsexes');
    }

    /**
     * Determine whether the refSex can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefSex  $model
     * @return mixed
     */
    public function view(User $user, RefSex $model)
    {
        return $user->hasPermissionTo('view refsexes');
    }

    /**
     * Determine whether the refSex can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refsexes');
    }

    /**
     * Determine whether the refSex can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefSex  $model
     * @return mixed
     */
    public function update(User $user, RefSex $model)
    {
        return $user->hasPermissionTo('update refsexes');
    }

    /**
     * Determine whether the refSex can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefSex  $model
     * @return mixed
     */
    public function delete(User $user, RefSex $model)
    {
        return $user->hasPermissionTo('delete refsexes');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefSex  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refsexes');
    }

    /**
     * Determine whether the refSex can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefSex  $model
     * @return mixed
     */
    public function restore(User $user, RefSex $model)
    {
        return false;
    }

    /**
     * Determine whether the refSex can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefSex  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefSex $model)
    {
        return false;
    }
}
