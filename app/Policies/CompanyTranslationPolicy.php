<?php

namespace App\Policies;

use App\Models\User;
use App\Models\CompanyTranslation;
use Illuminate\Auth\Access\HandlesAuthorization;

class CompanyTranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the companyTranslation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list companytranslations');
    }

    /**
     * Determine whether the companyTranslation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\CompanyTranslation  $model
     * @return mixed
     */
    public function view(User $user, CompanyTranslation $model)
    {
        return $user->hasPermissionTo('view companytranslations');
    }

    /**
     * Determine whether the companyTranslation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create companytranslations');
    }

    /**
     * Determine whether the companyTranslation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\CompanyTranslation  $model
     * @return mixed
     */
    public function update(User $user, CompanyTranslation $model)
    {
        return $user->hasPermissionTo('update companytranslations');
    }

    /**
     * Determine whether the companyTranslation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\CompanyTranslation  $model
     * @return mixed
     */
    public function delete(User $user, CompanyTranslation $model)
    {
        return $user->hasPermissionTo('delete companytranslations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\CompanyTranslation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete companytranslations');
    }

    /**
     * Determine whether the companyTranslation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\CompanyTranslation  $model
     * @return mixed
     */
    public function restore(User $user, CompanyTranslation $model)
    {
        return false;
    }

    /**
     * Determine whether the companyTranslation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\CompanyTranslation  $model
     * @return mixed
     */
    public function forceDelete(User $user, CompanyTranslation $model)
    {
        return false;
    }
}
