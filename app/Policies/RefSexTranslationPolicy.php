<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefSexTranslation;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefSexTranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refSexTranslation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refsextranslations');
    }

    /**
     * Determine whether the refSexTranslation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefSexTranslation  $model
     * @return mixed
     */
    public function view(User $user, RefSexTranslation $model)
    {
        return $user->hasPermissionTo('view refsextranslations');
    }

    /**
     * Determine whether the refSexTranslation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refsextranslations');
    }

    /**
     * Determine whether the refSexTranslation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefSexTranslation  $model
     * @return mixed
     */
    public function update(User $user, RefSexTranslation $model)
    {
        return $user->hasPermissionTo('update refsextranslations');
    }

    /**
     * Determine whether the refSexTranslation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefSexTranslation  $model
     * @return mixed
     */
    public function delete(User $user, RefSexTranslation $model)
    {
        return $user->hasPermissionTo('delete refsextranslations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefSexTranslation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refsextranslations');
    }

    /**
     * Determine whether the refSexTranslation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefSexTranslation  $model
     * @return mixed
     */
    public function restore(User $user, RefSexTranslation $model)
    {
        return false;
    }

    /**
     * Determine whether the refSexTranslation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefSexTranslation  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefSexTranslation $model)
    {
        return false;
    }
}
