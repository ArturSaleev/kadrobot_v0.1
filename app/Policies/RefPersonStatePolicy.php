<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefPersonState;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefPersonStatePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refPersonState can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refpersonstates');
    }

    /**
     * Determine whether the refPersonState can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefPersonState  $model
     * @return mixed
     */
    public function view(User $user, RefPersonState $model)
    {
        return $user->hasPermissionTo('view refpersonstates');
    }

    /**
     * Determine whether the refPersonState can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refpersonstates');
    }

    /**
     * Determine whether the refPersonState can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefPersonState  $model
     * @return mixed
     */
    public function update(User $user, RefPersonState $model)
    {
        return $user->hasPermissionTo('update refpersonstates');
    }

    /**
     * Determine whether the refPersonState can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefPersonState  $model
     * @return mixed
     */
    public function delete(User $user, RefPersonState $model)
    {
        return $user->hasPermissionTo('delete refpersonstates');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefPersonState  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refpersonstates');
    }

    /**
     * Determine whether the refPersonState can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefPersonState  $model
     * @return mixed
     */
    public function restore(User $user, RefPersonState $model)
    {
        return false;
    }

    /**
     * Determine whether the refPersonState can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefPersonState  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefPersonState $model)
    {
        return false;
    }
}
