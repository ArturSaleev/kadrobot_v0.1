<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefFamilyState;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefFamilyStatePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refFamilyState can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list reffamilystates');
    }

    /**
     * Determine whether the refFamilyState can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefFamilyState  $model
     * @return mixed
     */
    public function view(User $user, RefFamilyState $model)
    {
        return $user->hasPermissionTo('view reffamilystates');
    }

    /**
     * Determine whether the refFamilyState can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create reffamilystates');
    }

    /**
     * Determine whether the refFamilyState can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefFamilyState  $model
     * @return mixed
     */
    public function update(User $user, RefFamilyState $model)
    {
        return $user->hasPermissionTo('update reffamilystates');
    }

    /**
     * Determine whether the refFamilyState can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefFamilyState  $model
     * @return mixed
     */
    public function delete(User $user, RefFamilyState $model)
    {
        return $user->hasPermissionTo('delete reffamilystates');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefFamilyState  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete reffamilystates');
    }

    /**
     * Determine whether the refFamilyState can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefFamilyState  $model
     * @return mixed
     */
    public function restore(User $user, RefFamilyState $model)
    {
        return false;
    }

    /**
     * Determine whether the refFamilyState can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefFamilyState  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefFamilyState $model)
    {
        return false;
    }
}
