<?php

namespace App\Policies;

use App\Models\User;
use App\Models\BranchPhone;
use Illuminate\Auth\Access\HandlesAuthorization;

class BranchPhonePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the branchPhone can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list branchphones');
    }

    /**
     * Determine whether the branchPhone can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BranchPhone  $model
     * @return mixed
     */
    public function view(User $user, BranchPhone $model)
    {
        return $user->hasPermissionTo('view branchphones');
    }

    /**
     * Determine whether the branchPhone can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create branchphones');
    }

    /**
     * Determine whether the branchPhone can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BranchPhone  $model
     * @return mixed
     */
    public function update(User $user, BranchPhone $model)
    {
        return $user->hasPermissionTo('update branchphones');
    }

    /**
     * Determine whether the branchPhone can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BranchPhone  $model
     * @return mixed
     */
    public function delete(User $user, BranchPhone $model)
    {
        return $user->hasPermissionTo('delete branchphones');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BranchPhone  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete branchphones');
    }

    /**
     * Determine whether the branchPhone can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BranchPhone  $model
     * @return mixed
     */
    public function restore(User $user, BranchPhone $model)
    {
        return false;
    }

    /**
     * Determine whether the branchPhone can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BranchPhone  $model
     * @return mixed
     */
    public function forceDelete(User $user, BranchPhone $model)
    {
        return false;
    }
}
