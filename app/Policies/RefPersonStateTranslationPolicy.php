<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefPersonStateTranslation;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefPersonStateTranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refPersonStateTranslation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refpersonstatetranslations');
    }

    /**
     * Determine whether the refPersonStateTranslation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefPersonStateTranslation  $model
     * @return mixed
     */
    public function view(User $user, RefPersonStateTranslation $model)
    {
        return $user->hasPermissionTo('view refpersonstatetranslations');
    }

    /**
     * Determine whether the refPersonStateTranslation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refpersonstatetranslations');
    }

    /**
     * Determine whether the refPersonStateTranslation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefPersonStateTranslation  $model
     * @return mixed
     */
    public function update(User $user, RefPersonStateTranslation $model)
    {
        return $user->hasPermissionTo('update refpersonstatetranslations');
    }

    /**
     * Determine whether the refPersonStateTranslation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefPersonStateTranslation  $model
     * @return mixed
     */
    public function delete(User $user, RefPersonStateTranslation $model)
    {
        return $user->hasPermissionTo('delete refpersonstatetranslations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefPersonStateTranslation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refpersonstatetranslations');
    }

    /**
     * Determine whether the refPersonStateTranslation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefPersonStateTranslation  $model
     * @return mixed
     */
    public function restore(User $user, RefPersonStateTranslation $model)
    {
        return false;
    }

    /**
     * Determine whether the refPersonStateTranslation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefPersonStateTranslation  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefPersonStateTranslation $model)
    {
        return false;
    }
}
