<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EmployeeTripFromTo;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeeTripFromToPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the employeeTripFromTo can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list employeetripfromtos');
    }

    /**
     * Determine whether the employeeTripFromTo can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeTripFromTo  $model
     * @return mixed
     */
    public function view(User $user, EmployeeTripFromTo $model)
    {
        return $user->hasPermissionTo('view employeetripfromtos');
    }

    /**
     * Determine whether the employeeTripFromTo can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create employeetripfromtos');
    }

    /**
     * Determine whether the employeeTripFromTo can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeTripFromTo  $model
     * @return mixed
     */
    public function update(User $user, EmployeeTripFromTo $model)
    {
        return $user->hasPermissionTo('update employeetripfromtos');
    }

    /**
     * Determine whether the employeeTripFromTo can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeTripFromTo  $model
     * @return mixed
     */
    public function delete(User $user, EmployeeTripFromTo $model)
    {
        return $user->hasPermissionTo('delete employeetripfromtos');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeTripFromTo  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete employeetripfromtos');
    }

    /**
     * Determine whether the employeeTripFromTo can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeTripFromTo  $model
     * @return mixed
     */
    public function restore(User $user, EmployeeTripFromTo $model)
    {
        return false;
    }

    /**
     * Determine whether the employeeTripFromTo can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeTripFromTo  $model
     * @return mixed
     */
    public function forceDelete(User $user, EmployeeTripFromTo $model)
    {
        return false;
    }
}
