<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefUserStatusTranslation;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefUserStatusTranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refUserStatusTranslation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refuserstatustranslations');
    }

    /**
     * Determine whether the refUserStatusTranslation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefUserStatusTranslation  $model
     * @return mixed
     */
    public function view(User $user, RefUserStatusTranslation $model)
    {
        return $user->hasPermissionTo('view refuserstatustranslations');
    }

    /**
     * Determine whether the refUserStatusTranslation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refuserstatustranslations');
    }

    /**
     * Determine whether the refUserStatusTranslation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefUserStatusTranslation  $model
     * @return mixed
     */
    public function update(User $user, RefUserStatusTranslation $model)
    {
        return $user->hasPermissionTo('update refuserstatustranslations');
    }

    /**
     * Determine whether the refUserStatusTranslation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefUserStatusTranslation  $model
     * @return mixed
     */
    public function delete(User $user, RefUserStatusTranslation $model)
    {
        return $user->hasPermissionTo('delete refuserstatustranslations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefUserStatusTranslation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refuserstatustranslations');
    }

    /**
     * Determine whether the refUserStatusTranslation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefUserStatusTranslation  $model
     * @return mixed
     */
    public function restore(User $user, RefUserStatusTranslation $model)
    {
        return false;
    }

    /**
     * Determine whether the refUserStatusTranslation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefUserStatusTranslation  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefUserStatusTranslation $model)
    {
        return false;
    }
}
