<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefAllowanceTypeTranslation;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefAllowanceTypeTranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refAllowanceTypeTranslation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refallowancetypetranslations');
    }

    /**
     * Determine whether the refAllowanceTypeTranslation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAllowanceTypeTranslation  $model
     * @return mixed
     */
    public function view(User $user, RefAllowanceTypeTranslation $model)
    {
        return $user->hasPermissionTo('view refallowancetypetranslations');
    }

    /**
     * Determine whether the refAllowanceTypeTranslation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refallowancetypetranslations');
    }

    /**
     * Determine whether the refAllowanceTypeTranslation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAllowanceTypeTranslation  $model
     * @return mixed
     */
    public function update(User $user, RefAllowanceTypeTranslation $model)
    {
        return $user->hasPermissionTo('update refallowancetypetranslations');
    }

    /**
     * Determine whether the refAllowanceTypeTranslation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAllowanceTypeTranslation  $model
     * @return mixed
     */
    public function delete(User $user, RefAllowanceTypeTranslation $model)
    {
        return $user->hasPermissionTo('delete refallowancetypetranslations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAllowanceTypeTranslation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refallowancetypetranslations');
    }

    /**
     * Determine whether the refAllowanceTypeTranslation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAllowanceTypeTranslation  $model
     * @return mixed
     */
    public function restore(User $user, RefAllowanceTypeTranslation $model)
    {
        return false;
    }

    /**
     * Determine whether the refAllowanceTypeTranslation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAllowanceTypeTranslation  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefAllowanceTypeTranslation $model)
    {
        return false;
    }
}
