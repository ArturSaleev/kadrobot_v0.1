<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefKindHolidayTranslation;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefKindHolidayTranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refKindHolidayTranslation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refkindholidaytranslations');
    }

    /**
     * Determine whether the refKindHolidayTranslation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefKindHolidayTranslation  $model
     * @return mixed
     */
    public function view(User $user, RefKindHolidayTranslation $model)
    {
        return $user->hasPermissionTo('view refkindholidaytranslations');
    }

    /**
     * Determine whether the refKindHolidayTranslation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refkindholidaytranslations');
    }

    /**
     * Determine whether the refKindHolidayTranslation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefKindHolidayTranslation  $model
     * @return mixed
     */
    public function update(User $user, RefKindHolidayTranslation $model)
    {
        return $user->hasPermissionTo('update refkindholidaytranslations');
    }

    /**
     * Determine whether the refKindHolidayTranslation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefKindHolidayTranslation  $model
     * @return mixed
     */
    public function delete(User $user, RefKindHolidayTranslation $model)
    {
        return $user->hasPermissionTo('delete refkindholidaytranslations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefKindHolidayTranslation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refkindholidaytranslations');
    }

    /**
     * Determine whether the refKindHolidayTranslation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefKindHolidayTranslation  $model
     * @return mixed
     */
    public function restore(User $user, RefKindHolidayTranslation $model)
    {
        return false;
    }

    /**
     * Determine whether the refKindHolidayTranslation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefKindHolidayTranslation  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefKindHolidayTranslation $model)
    {
        return false;
    }
}
