<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EmployeeStazh;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeeStazhPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the employeeStazh can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list employeestazhs');
    }

    /**
     * Determine whether the employeeStazh can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeStazh  $model
     * @return mixed
     */
    public function view(User $user, EmployeeStazh $model)
    {
        return $user->hasPermissionTo('view employeestazhs');
    }

    /**
     * Determine whether the employeeStazh can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create employeestazhs');
    }

    /**
     * Determine whether the employeeStazh can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeStazh  $model
     * @return mixed
     */
    public function update(User $user, EmployeeStazh $model)
    {
        return $user->hasPermissionTo('update employeestazhs');
    }

    /**
     * Determine whether the employeeStazh can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeStazh  $model
     * @return mixed
     */
    public function delete(User $user, EmployeeStazh $model)
    {
        return $user->hasPermissionTo('delete employeestazhs');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeStazh  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete employeestazhs');
    }

    /**
     * Determine whether the employeeStazh can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeStazh  $model
     * @return mixed
     */
    public function restore(User $user, EmployeeStazh $model)
    {
        return false;
    }

    /**
     * Determine whether the employeeStazh can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeStazh  $model
     * @return mixed
     */
    public function forceDelete(User $user, EmployeeStazh $model)
    {
        return false;
    }
}
