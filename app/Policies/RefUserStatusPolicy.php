<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefUserStatus;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefUserStatusPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refUserStatus can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refuserstatuses');
    }

    /**
     * Determine whether the refUserStatus can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefUserStatus  $model
     * @return mixed
     */
    public function view(User $user, RefUserStatus $model)
    {
        return $user->hasPermissionTo('view refuserstatuses');
    }

    /**
     * Determine whether the refUserStatus can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refuserstatuses');
    }

    /**
     * Determine whether the refUserStatus can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefUserStatus  $model
     * @return mixed
     */
    public function update(User $user, RefUserStatus $model)
    {
        return $user->hasPermissionTo('update refuserstatuses');
    }

    /**
     * Determine whether the refUserStatus can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefUserStatus  $model
     * @return mixed
     */
    public function delete(User $user, RefUserStatus $model)
    {
        return $user->hasPermissionTo('delete refuserstatuses');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefUserStatus  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refuserstatuses');
    }

    /**
     * Determine whether the refUserStatus can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefUserStatus  $model
     * @return mixed
     */
    public function restore(User $user, RefUserStatus $model)
    {
        return false;
    }

    /**
     * Determine whether the refUserStatus can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefUserStatus  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefUserStatus $model)
    {
        return false;
    }
}
