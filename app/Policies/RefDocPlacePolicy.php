<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefDocPlace;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefDocPlacePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refDocPlace can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refdocplaces');
    }

    /**
     * Determine whether the refDocPlace can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefDocPlace  $model
     * @return mixed
     */
    public function view(User $user, RefDocPlace $model)
    {
        return $user->hasPermissionTo('view refdocplaces');
    }

    /**
     * Determine whether the refDocPlace can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refdocplaces');
    }

    /**
     * Determine whether the refDocPlace can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefDocPlace  $model
     * @return mixed
     */
    public function update(User $user, RefDocPlace $model)
    {
        return $user->hasPermissionTo('update refdocplaces');
    }

    /**
     * Determine whether the refDocPlace can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefDocPlace  $model
     * @return mixed
     */
    public function delete(User $user, RefDocPlace $model)
    {
        return $user->hasPermissionTo('delete refdocplaces');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefDocPlace  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refdocplaces');
    }

    /**
     * Determine whether the refDocPlace can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefDocPlace  $model
     * @return mixed
     */
    public function restore(User $user, RefDocPlace $model)
    {
        return false;
    }

    /**
     * Determine whether the refDocPlace can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefDocPlace  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefDocPlace $model)
    {
        return false;
    }
}
