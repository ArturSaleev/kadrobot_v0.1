<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EmployeePayAllowance;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeePayAllowancePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the employeePayAllowance can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list employeepayallowances');
    }

    /**
     * Determine whether the employeePayAllowance can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeePayAllowance  $model
     * @return mixed
     */
    public function view(User $user, EmployeePayAllowance $model)
    {
        return $user->hasPermissionTo('view employeepayallowances');
    }

    /**
     * Determine whether the employeePayAllowance can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create employeepayallowances');
    }

    /**
     * Determine whether the employeePayAllowance can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeePayAllowance  $model
     * @return mixed
     */
    public function update(User $user, EmployeePayAllowance $model)
    {
        return $user->hasPermissionTo('update employeepayallowances');
    }

    /**
     * Determine whether the employeePayAllowance can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeePayAllowance  $model
     * @return mixed
     */
    public function delete(User $user, EmployeePayAllowance $model)
    {
        return $user->hasPermissionTo('delete employeepayallowances');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeePayAllowance  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete employeepayallowances');
    }

    /**
     * Determine whether the employeePayAllowance can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeePayAllowance  $model
     * @return mixed
     */
    public function restore(User $user, EmployeePayAllowance $model)
    {
        return false;
    }

    /**
     * Determine whether the employeePayAllowance can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeePayAllowance  $model
     * @return mixed
     */
    public function forceDelete(User $user, EmployeePayAllowance $model)
    {
        return false;
    }
}
