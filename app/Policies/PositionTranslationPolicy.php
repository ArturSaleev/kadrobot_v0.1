<?php

namespace App\Policies;

use App\Models\User;
use App\Models\PositionTranslation;
use Illuminate\Auth\Access\HandlesAuthorization;

class PositionTranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the positionTranslation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list positiontranslations');
    }

    /**
     * Determine whether the positionTranslation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\PositionTranslation  $model
     * @return mixed
     */
    public function view(User $user, PositionTranslation $model)
    {
        return $user->hasPermissionTo('view positiontranslations');
    }

    /**
     * Determine whether the positionTranslation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create positiontranslations');
    }

    /**
     * Determine whether the positionTranslation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\PositionTranslation  $model
     * @return mixed
     */
    public function update(User $user, PositionTranslation $model)
    {
        return $user->hasPermissionTo('update positiontranslations');
    }

    /**
     * Determine whether the positionTranslation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\PositionTranslation  $model
     * @return mixed
     */
    public function delete(User $user, PositionTranslation $model)
    {
        return $user->hasPermissionTo('delete positiontranslations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\PositionTranslation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete positiontranslations');
    }

    /**
     * Determine whether the positionTranslation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\PositionTranslation  $model
     * @return mixed
     */
    public function restore(User $user, PositionTranslation $model)
    {
        return false;
    }

    /**
     * Determine whether the positionTranslation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\PositionTranslation  $model
     * @return mixed
     */
    public function forceDelete(User $user, PositionTranslation $model)
    {
        return false;
    }
}
