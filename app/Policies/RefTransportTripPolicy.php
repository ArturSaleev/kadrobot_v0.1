<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefTransportTrip;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefTransportTripPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refTransportTrip can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list reftransporttrips');
    }

    /**
     * Determine whether the refTransportTrip can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTransportTrip  $model
     * @return mixed
     */
    public function view(User $user, RefTransportTrip $model)
    {
        return $user->hasPermissionTo('view reftransporttrips');
    }

    /**
     * Determine whether the refTransportTrip can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create reftransporttrips');
    }

    /**
     * Determine whether the refTransportTrip can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTransportTrip  $model
     * @return mixed
     */
    public function update(User $user, RefTransportTrip $model)
    {
        return $user->hasPermissionTo('update reftransporttrips');
    }

    /**
     * Determine whether the refTransportTrip can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTransportTrip  $model
     * @return mixed
     */
    public function delete(User $user, RefTransportTrip $model)
    {
        return $user->hasPermissionTo('delete reftransporttrips');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTransportTrip  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete reftransporttrips');
    }

    /**
     * Determine whether the refTransportTrip can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTransportTrip  $model
     * @return mixed
     */
    public function restore(User $user, RefTransportTrip $model)
    {
        return false;
    }

    /**
     * Determine whether the refTransportTrip can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefTransportTrip  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefTransportTrip $model)
    {
        return false;
    }
}
