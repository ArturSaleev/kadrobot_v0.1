<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefAddressType;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefAddressTypePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refAddressType can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refaddresstypes');
    }

    /**
     * Determine whether the refAddressType can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAddressType  $model
     * @return mixed
     */
    public function view(User $user, RefAddressType $model)
    {
        return $user->hasPermissionTo('view refaddresstypes');
    }

    /**
     * Determine whether the refAddressType can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refaddresstypes');
    }

    /**
     * Determine whether the refAddressType can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAddressType  $model
     * @return mixed
     */
    public function update(User $user, RefAddressType $model)
    {
        return $user->hasPermissionTo('update refaddresstypes');
    }

    /**
     * Determine whether the refAddressType can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAddressType  $model
     * @return mixed
     */
    public function delete(User $user, RefAddressType $model)
    {
        return $user->hasPermissionTo('delete refaddresstypes');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAddressType  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refaddresstypes');
    }

    /**
     * Determine whether the refAddressType can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAddressType  $model
     * @return mixed
     */
    public function restore(User $user, RefAddressType $model)
    {
        return false;
    }

    /**
     * Determine whether the refAddressType can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAddressType  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefAddressType $model)
    {
        return false;
    }
}
