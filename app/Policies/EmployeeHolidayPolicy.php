<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EmployeeHoliday;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeeHolidayPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the employeeHoliday can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list employeeholidays');
    }

    /**
     * Determine whether the employeeHoliday can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeHoliday  $model
     * @return mixed
     */
    public function view(User $user, EmployeeHoliday $model)
    {
        return $user->hasPermissionTo('view employeeholidays');
    }

    /**
     * Determine whether the employeeHoliday can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create employeeholidays');
    }

    /**
     * Determine whether the employeeHoliday can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeHoliday  $model
     * @return mixed
     */
    public function update(User $user, EmployeeHoliday $model)
    {
        return $user->hasPermissionTo('update employeeholidays');
    }

    /**
     * Determine whether the employeeHoliday can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeHoliday  $model
     * @return mixed
     */
    public function delete(User $user, EmployeeHoliday $model)
    {
        return $user->hasPermissionTo('delete employeeholidays');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeHoliday  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete employeeholidays');
    }

    /**
     * Determine whether the employeeHoliday can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeHoliday  $model
     * @return mixed
     */
    public function restore(User $user, EmployeeHoliday $model)
    {
        return false;
    }

    /**
     * Determine whether the employeeHoliday can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EmployeeHoliday  $model
     * @return mixed
     */
    public function forceDelete(User $user, EmployeeHoliday $model)
    {
        return false;
    }
}
