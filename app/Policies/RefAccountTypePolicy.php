<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefAccountType;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefAccountTypePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refAccountType can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list refaccounttypes');
    }

    /**
     * Determine whether the refAccountType can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAccountType  $model
     * @return mixed
     */
    public function view(User $user, RefAccountType $model)
    {
        return $user->hasPermissionTo('view refaccounttypes');
    }

    /**
     * Determine whether the refAccountType can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create refaccounttypes');
    }

    /**
     * Determine whether the refAccountType can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAccountType  $model
     * @return mixed
     */
    public function update(User $user, RefAccountType $model)
    {
        return $user->hasPermissionTo('update refaccounttypes');
    }

    /**
     * Determine whether the refAccountType can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAccountType  $model
     * @return mixed
     */
    public function delete(User $user, RefAccountType $model)
    {
        return $user->hasPermissionTo('delete refaccounttypes');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAccountType  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete refaccounttypes');
    }

    /**
     * Determine whether the refAccountType can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAccountType  $model
     * @return mixed
     */
    public function restore(User $user, RefAccountType $model)
    {
        return false;
    }

    /**
     * Determine whether the refAccountType can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefAccountType  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefAccountType $model)
    {
        return false;
    }
}
