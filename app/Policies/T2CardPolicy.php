<?php

namespace App\Policies;

use App\Models\User;
use App\Models\T2Card;
use Illuminate\Auth\Access\HandlesAuthorization;

class T2CardPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the t2Card can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list t2cards');
    }

    /**
     * Determine whether the t2Card can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\T2Card  $model
     * @return mixed
     */
    public function view(User $user, T2Card $model)
    {
        return $user->hasPermissionTo('view t2cards');
    }

    /**
     * Determine whether the t2Card can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create t2cards');
    }

    /**
     * Determine whether the t2Card can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\T2Card  $model
     * @return mixed
     */
    public function update(User $user, T2Card $model)
    {
        return $user->hasPermissionTo('update t2cards');
    }

    /**
     * Determine whether the t2Card can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\T2Card  $model
     * @return mixed
     */
    public function delete(User $user, T2Card $model)
    {
        return $user->hasPermissionTo('delete t2cards');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\T2Card  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete t2cards');
    }

    /**
     * Determine whether the t2Card can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\T2Card  $model
     * @return mixed
     */
    public function restore(User $user, T2Card $model)
    {
        return false;
    }

    /**
     * Determine whether the t2Card can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\T2Card  $model
     * @return mixed
     */
    public function forceDelete(User $user, T2Card $model)
    {
        return false;
    }
}
