<?php

namespace App\Policies;

use App\Models\User;
use App\Models\RefFamilyStateTranslation;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefFamilyStateTranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the refFamilyStateTranslation can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list reffamilystatetranslations');
    }

    /**
     * Determine whether the refFamilyStateTranslation can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefFamilyStateTranslation  $model
     * @return mixed
     */
    public function view(User $user, RefFamilyStateTranslation $model)
    {
        return $user->hasPermissionTo('view reffamilystatetranslations');
    }

    /**
     * Determine whether the refFamilyStateTranslation can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create reffamilystatetranslations');
    }

    /**
     * Determine whether the refFamilyStateTranslation can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefFamilyStateTranslation  $model
     * @return mixed
     */
    public function update(User $user, RefFamilyStateTranslation $model)
    {
        return $user->hasPermissionTo('update reffamilystatetranslations');
    }

    /**
     * Determine whether the refFamilyStateTranslation can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefFamilyStateTranslation  $model
     * @return mixed
     */
    public function delete(User $user, RefFamilyStateTranslation $model)
    {
        return $user->hasPermissionTo('delete reffamilystatetranslations');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefFamilyStateTranslation  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete reffamilystatetranslations');
    }

    /**
     * Determine whether the refFamilyStateTranslation can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefFamilyStateTranslation  $model
     * @return mixed
     */
    public function restore(User $user, RefFamilyStateTranslation $model)
    {
        return false;
    }

    /**
     * Determine whether the refFamilyStateTranslation can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\RefFamilyStateTranslation  $model
     * @return mixed
     */
    public function forceDelete(User $user, RefFamilyStateTranslation $model)
    {
        return false;
    }
}
