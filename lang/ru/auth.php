<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Эти учетные данные не соответствуют нашим записям.',
    'password' => 'Предоставленный пароль неверен.',
    'throttle' => 'Слишком много попыток входа. Повторите попытку через :seconds секунд.',

    'email' => 'Email адрес',
    'password_text' => 'Пароль',
    'remember_me' => 'Запомнить меня',
    'login' => 'Войти',
    'login_text' => 'Авторизация',
    'forgot_password' => 'Забыли пароль?'

];
