<?php

return [
    'common' => [
        'actions' => 'Действия',
        'add' => 'Добавить',
        'create' => 'Создать',
        'edit' => 'Редактировать',
        'update' => 'Обновить',
        'new' => 'Новый',
        'cancel' => 'Отмена',
        'attach' => 'Прикрепить',
        'detach' => 'Открепить',
        'save' => 'Сохранить',
        'delete' => 'Удалить',
        'delete_selected' => 'Удалить выбранные',
        'search' => 'Поиск...',
        'back' => 'Назад на главную',
        'are_you_sure' => 'Вы уверены?',
        'no_items_found' => 'Ни чего не найдено',
        'created' => 'Создано успешно',
        'saved' => 'Успешно сохранено',
        'removed' => 'Успешно удалено',
        'removed_error' => 'Удаление запрещено',
    ],

    'lang_input' => [
        'ru' => 'на русском языке',
        'kz' => 'на казахском языке',
        'en' => 'на английском языке',
    ],

    "structure" => 'Структура',
    'Logout' => 'Выйти',
    'references' => 'Справочники',
    'main' => 'Главная',
    'reports' => 'Отчеты',
    'templates_docs' => 'Шаблоны документов',

    'name_ru' => 'Наименование на русском',
    'name_kz' => 'Наименование на казахском',
    'name_en' => 'Наименование на английском',
    'names'   => 'Наименования',
    'report_name' => 'Название документа',
    'report_type' => 'Тип документа',
    'report_markers' => 'Маркеры',

    'please_select' => 'Выбрать...',
    'other_data' => 'Другие данные',

    'id_types' => [
        'pattern' => 'Шаблон',
        'document' => 'Рабочий документ'
    ],

    'default_title' => [
        'index' => 'Список',
        'new' => 'Новый',
        'create' => 'Создать',
        'edit' => 'Редактировать',
        'show' => 'Данные',
    ],

    'branches' => [
        'name' => 'Филиалы',
        'index_title' => 'Список филиалов',
        'new_title' => 'Новый филиал',
        'create_title' => 'Создание филиала',
        'edit_title' => 'Редактирование филиала',
        'show_title' => 'Данные по филиалу',
        'inputs' => [
            'company_id' => 'Компания',
            'branch_main' => 'Основной филиал',
            'name' => 'Наименование',
            'phone' => 'Связь',
            'address' => 'Адреса'
        ],
    ],

    'companies' => [
        'name' => 'Компания',
        'index_title' => 'Список',
        'new_title' => 'Новый',
        'create_title' => 'Создать',
        'edit_title' => 'Редактировать',
        'show_title' => 'Данные компании',
        'inputs' => [
            'bin' => 'БИН',
            'ref_status_id' => 'Статус',
            'rnn' => 'РНН',
            'oked' => 'ОКЕД',
            'name' => "Наименование",
            'name_ru' => 'Наименование на русском',
            'name_kz' => 'Наименование на казахском',
            'name_en' => 'Наименование на английском',
        ],
    ],

    'all_branch_addreses' => [
        'name' => 'Адрес филиала',
        'index_title' => 'AllBranchAddreses List',
        'new_title' => 'New Branch addreses',
        'create_title' => 'Create BranchAddreses',
        'edit_title' => 'Edit BranchAddreses',
        'show_title' => 'Show BranchAddreses',
        'inputs' => [
            'branch_id' => 'Branch',
            'ref_address_type_id' => 'Ref Address Type',
        ],
    ],

    'branch_phones' => [
        'name' => 'Телефоны',
        'index_title' => 'BranchPhones List',
        'new_title' => 'New Branch phone',
        'create_title' => 'Create BranchPhone',
        'edit_title' => 'Edit BranchPhone',
        'show_title' => 'Show BranchPhone',
        'inputs' => [
            'branch_id' => 'Branch',
            'ref_type_phone_id' => 'Тип телефона',
            'name' => 'Номер',
        ],
    ],

    'curators' => [
        'name' => 'Кураторы',
        'index_title' => 'Список',
        'new_title' => 'Новый',
        'create_title' => 'Создать',
        'edit_title' => 'Редактирование',
        'show_title' => 'Куратор',
        'inputs' => [
            'employee_id' => 'Сотрудник',
            'department_id' => 'Департмент',
        ],
    ],

    'departments' => [
        'name' => 'Департаменты',
        'index_title' => 'Департаменты/Отделы список',
        'new_title' => 'Новый Департамент/Отдел',
        'create_title' => 'Создать Департамент/Отдел',
        'edit_title' => 'Редактирование данных Департамента/Отдела',
        'show_title' => 'Данные Департамента/Отдела',
        'inputs' => [
            'parent_id' => 'Parent Id',
            'branch_id' => 'Филиал',
            'email' => 'Email',
            'name' => 'Наименование',
            'phone' => 'Связь (телефоны)'
        ],
    ],

    'department_phones' => [
        'name' => 'Department Phones',
        'index_title' => 'DepartmentPhones List',
        'new_title' => 'New Department phone',
        'create_title' => 'Create DepartmentPhone',
        'edit_title' => 'Edit DepartmentPhone',
        'show_title' => 'Show DepartmentPhone',
        'inputs' => [
            'department_id' => 'Department',
            'ref_type_phone_id' => 'Ref Type Phone',
            'name' => 'Name',
        ],
    ],

    'employees' => [
        'name' => 'Сотрудники',
        'index_title' => 'Список сотрудников',
        'new_title' => 'Новый сотрудник',
        'create_title' => 'Создание нового сотрудника',
        'edit_title' => 'Редактирование данных сотурдника',
        'show_title' => 'Данные сотрудника',
        'inputs' => [
            'date_post' => 'Дата принятия',
            'date_loyoff' => 'Дата увольнения',
            'reason_loyoff' => "Причина увольнения",
            'tab_num' => 'Табельный номер',
            'birthday' => 'Дата рождения',
            'birth_place' => 'Место рождения',
            'ref_sex_id' => 'Пол',
            'iin' => 'ИИН',
            'ref_nationality_id' => 'Национальность',
            'ref_family_state_id' => 'Семейное положение',
            'contract_num' => 'Номер договора',
            'contract_date' => 'Дата договора',
            'ref_account_type_id' => 'Тип счета',
            'email' => 'Email',
            'account' => 'Счет',
            'date_zav' => 'Дата заявления',
            'oklad' => 'Оклад',
            'gos_nagr' => 'Гос. награды',
            'pens' => 'На пенсии',
            'pens_date' => 'Дата выхода на пенсию',
            'lgot' => 'Льготы',
            'person_email' => 'Личный Email',
            'ref_user_status_id' => 'Статус',
            'lastname' => 'Фамилия',
            'firstname' => 'Имя',
            'middlename' => 'Отчество',
            'name' => 'ФИО',
            'position_id' => 'Должность/Позиция'
        ],
    ],

    'employee_addresses' => [
        'name' => 'Адреса сотрудника',
        'index_title' => 'Список адресов сотрудника',
        'new_title' => 'New Employee address',
        'create_title' => 'Create EmployeeAddress',
        'edit_title' => 'Edit EmployeeAddress',
        'show_title' => 'Show EmployeeAddress',
        'inputs' => [
            'employee_id' => 'Employee',
            'locale' => 'Locale',
            'ref_address_type_id' => 'Тип',
            'name' => 'Наименование'
        ],
    ],

    'employee_declensions' => [
        'name' => 'Employee Declensions',
        'index_title' => 'EmployeeDeclensions List',
        'new_title' => 'New Employee declension',
        'create_title' => 'Create EmployeeDeclension',
        'edit_title' => 'Edit EmployeeDeclension',
        'show_title' => 'Show EmployeeDeclension',
        'inputs' => [
            'employee_id' => 'Employee',
            'locale' => 'Locale',
            'lastname' => 'Фамилия',
            'firstname' => 'Имя',
            'middlename' => 'Отчество',
            'case_type' => 'Падеж',
        ],
    ],

    'employee_documents' => [
        'name' => 'Документы сотрудника',
        'index_title' => 'EmployeeDocuments List',
        'new_title' => 'New Employee document',
        'create_title' => 'Create EmployeeDocument',
        'edit_title' => 'Edit EmployeeDocument',
        'show_title' => 'Show EmployeeDocument',
        'inputs' => [
            'employee_id' => 'Employee',
            'ref_doc_type_id' => 'Тип документа',
            'ref_doc_place_id' => 'Кем выдан',
            'doc_seria' => 'Серия',
            'doc_num' => 'Номер',
            'doc_date' => 'Дата выдачи',
        ],
    ],

    'employee_educations' => [
        'name' => 'Образование сотрудника',
        'index_title' => 'EmployeeEducations List',
        'new_title' => 'New Employee education',
        'create_title' => 'Create EmployeeEducation',
        'edit_title' => 'Edit EmployeeEducation',
        'show_title' => 'Show EmployeeEducation',
        'inputs' => [
            'employee_id' => 'Employee',
            'institution' => 'Учебное заведение',
            'year_begin' => 'Год начала обучения',
            'year_end' => 'Год окончания обучения',
            'date_begin' => 'Дата начала обучения',
            'date_end' => 'Дата окончания обучения',
            'speciality' => 'Специализация',
            'qualification' => 'Квалификация',
            'diplom_num' => 'Номер диплома',
            'diplom_date' => 'Дата диплома',
        ],
    ],

    'employee_families' => [
        'name' => 'Родственники сотрудника',
        'index_title' => 'EmployeeFamilies List',
        'new_title' => 'New Employee family',
        'create_title' => 'Create EmployeeFamily',
        'edit_title' => 'Edit EmployeeFamily',
        'show_title' => 'Show EmployeeFamily',
        'inputs' => [
            'employee_id' => 'Employee',
            'lastname' => 'Фамилия',
            'firstname' => 'Имя',
            'middlename' => 'Отчество',
            'birthday' => 'Дата рождения',
            'ref_family_state_id' => 'Вид родства',
        ],
    ],

    'employee_holidays' => [
        'name' => 'Employee Holidays',
        'index_title' => 'EmployeeHolidays List',
        'new_title' => 'New Employee holiday',
        'create_title' => 'Create EmployeeHoliday',
        'edit_title' => 'Edit EmployeeHoliday',
        'show_title' => 'Show EmployeeHoliday',
        'inputs' => [
            'employee_id' => 'Employee',
            'date_begin' => 'Date Begin',
            'date_end' => 'Date End',
            'cnt_days' => 'Cnt Days',
            'period_begin' => 'Period Begin',
            'period_end' => 'Period End',
            'order_num' => 'Order Num',
            'order_date' => 'Order Date',
            'ref_vid_holiday_id' => 'Ref Vid Holiday',
            'deligate' => 'Deligate',
            'deligate_emp_id' => 'Deligate Emp Id',
            'doc_content' => 'Doc Content',
        ],
    ],

    'employee_holidays_periods' => [
        'name' => 'Employee Holidays Periods',
        'index_title' => 'EmployeeHolidaysPeriods List',
        'new_title' => 'New Employee holidays period',
        'create_title' => 'Create EmployeeHolidaysPeriod',
        'edit_title' => 'Edit EmployeeHolidaysPeriod',
        'show_title' => 'Show EmployeeHolidaysPeriod',
        'inputs' => [
            'employee_id' => 'Employee',
            'period_start' => 'Period Start',
            'period_end' => 'Period End',
            'day_count_used_for_today' => 'Day Count Used For Today',
            'didnt_add' => 'Didnt Add',
            'paying_for_health' => 'Paying For Health',
        ],
    ],

    'employee_hospitals' => [
        'name' => 'Employee Hospitals',
        'index_title' => 'EmployeeHospitals List',
        'new_title' => 'New Employee hospital',
        'create_title' => 'Create EmployeeHospital',
        'edit_title' => 'Edit EmployeeHospital',
        'show_title' => 'Show EmployeeHospital',
        'inputs' => [
            'date_begin' => 'Date Begin',
            'date_end' => 'Date End',
            'cnt_days' => 'Cnt Days',
            'employee_id' => 'Employee',
        ],
    ],

    'employee_invalides' => [
        'name' => 'Employee Invalides',
        'index_title' => 'EmployeeInvalides List',
        'new_title' => 'New Employee invalide',
        'create_title' => 'Create EmployeeInvalide',
        'edit_title' => 'Edit EmployeeInvalide',
        'show_title' => 'Show EmployeeInvalide',
        'inputs' => [
            'employee_id' => 'Employee',
            'num' => 'Num',
            'date_add' => 'Date Add',
            'period_begin' => 'Period Begin',
            'period_end' => 'Period End',
        ],
    ],

    'employee_militaries' => [
        'name' => 'Employee Militaries',
        'index_title' => 'EmployeeMilitaries List',
        'new_title' => 'New Employee military',
        'create_title' => 'Create EmployeeMilitary',
        'edit_title' => 'Edit EmployeeMilitary',
        'show_title' => 'Show EmployeeMilitary',
        'inputs' => [
            'employee_id' => 'Employee',
            'group' => 'Group',
            'category' => 'Category',
            'rank' => 'Rank',
            'speciality' => 'Speciality',
            'voenkom' => 'Voenkom',
            'spec_uch' => 'Spec Uch',
            'spec_uch_num' => 'Spec Uch Num',
            'fit' => 'Fit',
        ],
    ],

    'employee_pay_allowances' => [
        'name' => 'Employee Pay Allowances',
        'index_title' => 'EmployeePayAllowances List',
        'new_title' => 'New Employee pay allowance',
        'create_title' => 'Create EmployeePayAllowance',
        'edit_title' => 'Edit EmployeePayAllowance',
        'show_title' => 'Show EmployeePayAllowance',
        'inputs' => [
            'employee_id' => 'Employee',
            'ref_allowance_type_id' => 'Ref Allowance Type',
            'date_add' => 'Date Add',
            'period_begin' => 'Period Begin',
            'period_end' => 'Period End',
            'paysum' => 'Paysum',
        ],
    ],

    'employee_phones' => [
        'name' => 'Employee Phones',
        'index_title' => 'EmployeePhones List',
        'new_title' => 'New Employee phone',
        'create_title' => 'Create EmployeePhone',
        'edit_title' => 'Edit EmployeePhone',
        'show_title' => 'Show EmployeePhone',
        'inputs' => [
            'ref_type_phone_id' => 'Ref Type Phone',
            'employee_id' => 'Employee',
            'phone' => 'Phone',
        ],
    ],

    'employee_stazhs' => [
        'name' => 'Стаж сотрудника',
        'index_title' => 'EmployeeStazhs List',
        'new_title' => 'New Employee stazh',
        'create_title' => 'Create EmployeeStazh',
        'edit_title' => 'Edit EmployeeStazh',
        'show_title' => 'Show EmployeeStazh',
        'inputs' => [
            'employee_id' => 'Employee',
            'date_begin' => 'Дата начала',
            'date_end' => 'дата окончания',
            'cnt_mes' => 'Кол-во месяцев',
            'name' => "Наименование",
            'organization' => 'Организация',
            'dolgnost' => 'Должность',
            'address' => 'Адрес'
        ],
    ],

    'employee_techics' => [
        'name' => 'Employee Techics',
        'index_title' => 'EmployeeTechics List',
        'new_title' => 'New Employee techic',
        'create_title' => 'Create EmployeeTechic',
        'edit_title' => 'Edit EmployeeTechic',
        'show_title' => 'Show EmployeeTechic',
        'inputs' => [
            'employee_id' => 'Employee',
            'ref_technic_type_id' => 'Ref Technic Type',
            'invent_num' => 'Invent Num',
            'price' => 'Price',
        ],
    ],

    'employee_trips' => [
        'name' => 'Employee Trips',
        'index_title' => 'EmployeeTrips List',
        'new_title' => 'New Employee trip',
        'create_title' => 'Create EmployeeTrip',
        'edit_title' => 'Edit EmployeeTrip',
        'show_title' => 'Show EmployeeTrip',
        'inputs' => [
            'employee_id' => 'Employee',
            'ref_transport_trip_id' => 'Ref Transport Trip',
            'date_begin' => 'Date Begin',
            'date_end' => 'Date End',
            'cnt_days' => 'Cnt Days',
            'order_num' => 'Order Num',
            'order_date' => 'Order Date',
            'name' => 'Наименование',
        ],
    ],

    'employee_trip_from_tos' => [
        'name' => 'Employee Trip From Tos',
        'index_title' => 'EmployeeTripFromTos List',
        'new_title' => 'New Employee trip from to',
        'create_title' => 'Create EmployeeTripFromTo',
        'edit_title' => 'Edit EmployeeTripFromTo',
        'show_title' => 'Show EmployeeTripFromTo',
        'inputs' => [
            'employee_trip_id' => 'Employee Trip',
            'from_place' => 'From Place',
            'to_place' => 'To Place',
            'ref_transport_trip_id' => 'Ref Transport Trip',
        ],
    ],

    'holidays' => [
        'name' => 'Holidays',
        'index_title' => 'Holidays List',
        'new_title' => 'New Holiday',
        'create_title' => 'Create Holiday',
        'edit_title' => 'Edit Holiday',
        'show_title' => 'Show Holiday',
        'inputs' => [
            'date' => 'Date',
        ],
    ],

    'positions' => [
        'name' => 'Позиции',
        'index_title' => 'Позиции/Должности Общий список',
        'new_title' => 'Новая Позиция/Должность',
        'create_title' => 'Создать новую Позицию/Должность',
        'edit_title' => 'Редактировать Позицию/Должность',
        'show_title' => 'Данные выбранной Позиции/Должности',
        'inputs' => [
            'cnt' => 'Кол-во',
            'pos_level' => 'В подчинении у ....',
            'min_salary' => 'Оклад (минимальный)',
            'max_salary' => 'Оклад (Максимальный)',
            'name' => 'Наименование',
            'department_id' => 'Депаратмент/Отдел'
        ],
    ],

    'ref_account_types' => [
        'name' => 'Тип счета',
        'index_title' => 'Справочник типов счетов',
        'new_title' => 'Новый тип счета',
        'create_title' => 'Создать новый тип счета',
        'edit_title' => 'Редактировать данные типа счета',
        'show_title' => 'Данные выбранного типа счета',
        'inputs' => [
            'name' => 'Наименование'
        ],
    ],

    'ref_actions' => [
        'name' => 'Ref Actions',
        'index_title' => 'RefActions List',
        'new_title' => 'New Ref action',
        'create_title' => 'Create RefAction',
        'edit_title' => 'Edit RefAction',
        'show_title' => 'Show RefAction',
        'inputs' => [
            'act_type' => 'Act Type',
        ],
    ],

    'ref_address_types' => [
        'name' => 'Тип объекта',
        'index_title' => 'Список типов объектов',
        'new_title' => 'Новый тип объекта',
        'create_title' => 'Создание типа объекта',
        'edit_title' => 'Редактирование типа объекта',
        'show_title' => 'Данные типа объекта',
        'inputs' => [
            'name_ru' => 'Адрес на русском',
            'name_kz' => 'Адрес на казахском',
            'name_en' => 'Адрес на английском'
        ],
    ],

    'ref_allowance_types' => [
        'name' => 'Типы надбавок',
        'index_title' => 'Список типов надбавок',
        'new_title' => 'Новый тип надбавки',
        'create_title' => 'Создание типа надбавки',
        'edit_title' => 'Редактирование типа надбавки',
        'show_title' => 'Данные типа надбавки',
        'inputs' => [
            'name' => "Наименование"
        ],
    ],

    'ref_banks' => [
        'name' => 'Банки',
        'index_title' => 'Список банков',
        'new_title' => 'Новый банк',
        'create_title' => 'Добавление банка',
        'edit_title' => 'Редактирование данных банка',
        'show_title' => 'Данные банка',
        'inputs' => [
            'ref_status_id' => 'Статус',
            'mfo' => 'МФО',
            'mfo_head' => 'Mfo Head',
            'mfo_rkc' => 'Mfo Rkc',
            'kor_account' => 'Счет',
            'commis' => 'Коммиссия',
            'bin' => 'БИН',
            'bik_old' => 'БИК',
        ],
    ],

    'ref_countries' => [
        'name' => 'Страны',
        'index_title' => 'Список стран',
        'new_title' => 'Новое название страны',
        'create_title' => 'Добавление страны',
        'edit_title' => 'Редактирование страны',
        'show_title' => 'Данные страны',
        'inputs' => [
            'code' => 'Код',
        ],
    ],

    'ref_doc_places' => [
        'name' => 'Документ Кем выдан',
        'index_title' => 'Список',
        'new_title' => 'Новый',
        'create_title' => 'Создать',
        'edit_title' => 'Редактировать',
        'show_title' => 'Данные',
        'inputs' => [
            'name' => "Наименование"
        ],
    ],

    'ref_doc_types' => [
        'name' => 'Тип документа',
        'index_title' => 'Список',
        'new_title' => 'Новый',
        'create_title' => 'Создать',
        'edit_title' => 'Редактировать',
        'show_title' => 'Данные',
        'inputs' => [
            'name' => "Наименование"
        ],
    ],

    'ref_family_states' => [
        'name' => 'Семейное положение',
        'index_title' => 'Список',
        'new_title' => 'Новый',
        'create_title' => 'Создать',
        'edit_title' => 'Редактировать',
        'show_title' => 'Семейное положение',
        'inputs' => [
            'name' => "Наименование"
        ],
    ],

    'ref_kind_holidays' => [
        'name' => 'Виды выходных',
        'index_title' => 'Список',
        'new_title' => 'Новый',
        'create_title' => 'Создать',
        'edit_title' => 'Редактировать',
        'show_title' => 'Данные',
        'inputs' => [
            'name' => "Наименование"
        ],
    ],

    'ref_metas' => [
        'name' => 'Ref Metas',
        'index_title' => 'RefMetas List',
        'new_title' => 'New Ref meta',
        'create_title' => 'Create RefMeta',
        'edit_title' => 'Edit RefMeta',
        'show_title' => 'Show RefMeta',
        'inputs' => [
            'title' => 'Title',
            'name' => 'Name',
            'tablename' => 'Tablename',
            'column_name' => 'Column Name',
        ],
    ],

    'ref_nationalities' => [
        'name' => 'Национальности',
        'index_title' => 'Список',
        'new_title' => 'Новый',
        'create_title' => 'Создать',
        'edit_title' => 'Редактировать',
        'show_title' => 'Данные по национальности',
        'inputs' => [
            'name' => "Наименование"
        ],
    ],

    'ref_okeds' => [
        'name' => 'ОКЕД',
        'index_title' => 'Общий квалификатор видов экономической деятельности',
        'new_title' => 'Новый',
        'create_title' => 'Создать',
        'edit_title' => 'Редактировать',
        'show_title' => 'Данные',
        'inputs' => [
            'oked' => 'Код',
            'name' => 'Группа',
            'name_oked' => 'Подгруппа'
        ],
    ],

    'ref_person_states' => [
        'name' => 'Ref Person States',
        'index_title' => 'RefPersonStates List',
        'new_title' => 'New Ref person state',
        'create_title' => 'Create RefPersonState',
        'edit_title' => 'Edit RefPersonState',
        'show_title' => 'Show RefPersonState',
        'inputs' => [
            'name' => "Наименование"
        ],
    ],

    'ref_sexes' => [
        'name' => 'Гендер',
        'index_title' => 'Список',
        'new_title' => 'Новый',
        'create_title' => 'Создать',
        'edit_title' => 'Редактировать',
        'show_title' => 'Гендерные данные',
        'inputs' => [
            'name' => "Пол"
        ],
    ],

    'ref_statuses' => [
        'name' => 'Статус активности',
        'index_title' => 'Список',
        'new_title' => 'Новый',
        'create_title' => 'Создать',
        'edit_title' => 'Редактировать',
        'show_title' => 'Данные',
        'inputs' => [
            'name' => "Наименование"
        ],
    ],

    'ref_technic_types' => [
        'name' => 'Инвентарь',
        'index_title' => 'Список',
        'new_title' => 'Новый',
        'create_title' => 'Создать',
        'edit_title' => 'Редактировать',
        'show_title' => 'Данные',
        'inputs' => [
            'name' => 'Name',
        ],
    ],

    'ref_transport_trips' => [
        'name' => 'Ref Transport Trips',
        'index_title' => 'RefTransportTrips List',
        'new_title' => 'New Ref transport trip',
        'create_title' => 'Create RefTransportTrip',
        'edit_title' => 'Edit RefTransportTrip',
        'show_title' => 'Show RefTransportTrip',
        'inputs' => [
            'name' => "Наименование"
        ],
    ],

    'ref_type_phones' => [
        'name' => 'Тип связи',
        'index_title' => 'RefTypePhones List',
        'new_title' => 'New Ref type phone',
        'create_title' => 'Create RefTypePhone',
        'edit_title' => 'Edit RefTypePhone',
        'show_title' => 'Show RefTypePhone',
        'placeholder' => '+ (000) 000-00-00',
        'inputs' => [
            'name' => 'Номер'
        ],
    ],

    'ref_type_rodstvs' => [
        'name' => 'Тип родства',
        'index_title' => 'Список',
        'new_title' => 'Новый',
        'create_title' => 'Создать',
        'edit_title' => 'Редактировать',
        'show_title' => 'Данные',
        'inputs' => [
            'name' => "Наименование"
        ],
    ],

    'ref_user_statuses' => [
        'name' => 'Статус пользователя',
        'index_title' => 'Список',
        'new_title' => 'Новый',
        'create_title' => 'Создать',
        'edit_title' => 'Редактировать',
        'show_title' => 'Данные',
        'inputs' => [
            'name' => 'Name',
        ],
    ],

    'ref_vid_holidays' => [
        'name' => 'Ref Vid Holidays',
        'index_title' => 'RefVidHolidays List',
        'new_title' => 'New Ref vid holiday',
        'create_title' => 'Create RefVidHoliday',
        'edit_title' => 'Edit RefVidHoliday',
        'show_title' => 'Show RefVidHoliday',
        'inputs' => [
            'ref_vid_holiday_type_id' => 'Ref Vid Holiday Type',
        ],
    ],

    'ref_vid_holiday_types' => [
        'name' => 'Ref Vid Holiday Types',
        'index_title' => 'RefVidHolidayTypes List',
        'new_title' => 'New Ref vid holiday type',
        'create_title' => 'Create RefVidHolidayType',
        'edit_title' => 'Edit RefVidHolidayType',
        'show_title' => 'Show RefVidHolidayType',
        'inputs' => [
            'name' => "Наименование"
        ],
    ],

    'report_htmls' => [
        'name' => 'Report Htmls',
        'index_title' => 'ReportHtmls List',
        'new_title' => 'New Report html',
        'create_title' => 'Create ReportHtml',
        'edit_title' => 'Edit ReportHtml',
        'show_title' => 'Show ReportHtml',
        'inputs' => [
            'html_text' => 'Html Text',
            'sql_text' => 'Sql Text',
            'title_text' => 'Title Text',
            'date_edd' => 'Date Edd',
        ],
    ],

    'report_html_others' => [
        'name' => 'Report Html Others',
        'index_title' => 'ReportHtmlOthers List',
        'new_title' => 'New Report html other',
        'create_title' => 'Create ReportHtmlOther',
        'edit_title' => 'Edit ReportHtmlOther',
        'show_title' => 'Show ReportHtmlOther',
        'inputs' => [
            'report_html_id' => 'Report Html',
            'title' => 'Title',
            'html_text' => 'Html Text',
            'sql_text' => 'Sql Text',
            'position' => 'Position',
            'num_pp' => 'Num Pp',
        ],
    ],

    't2_cards' => [
        'name' => 'T2 Cards',
        'index_title' => 'T2Cards List',
        'new_title' => 'New T2 card',
        'create_title' => 'Create T2Card',
        'edit_title' => 'Edit T2Card',
        'show_title' => 'Show T2Card',
        'inputs' => [
            'company_id' => 'Company',
            'employee_id' => 'Employee',
            'ref_branch_id' => 'Ref Branch',
            'ref_action_id' => 'Ref Action',
            'ref_position_id' => 'Ref Position',
        ],
    ],

    'users' => [
        'name' => 'Users',
        'index_title' => 'Users List',
        'new_title' => 'New User',
        'create_title' => 'Create User',
        'edit_title' => 'Edit User',
        'show_title' => 'Show User',
        'inputs' => [
            'name' => 'Name',
            'email' => 'Email',
            'password' => 'Password',
        ],
    ],

    'roles' => [
        'name' => 'Roles',
        'index_title' => 'Roles List',
        'create_title' => 'Create Role',
        'edit_title' => 'Edit Role',
        'show_title' => 'Show Role',
        'inputs' => [
            'name' => 'Name',
        ],
    ],

    'permissions' => [
        'name' => 'Permissions',
        'index_title' => 'Permissions List',
        'create_title' => 'Create Permission',
        'edit_title' => 'Edit Permission',
        'show_title' => 'Show Permission',
        'inputs' => [
            'name' => 'Name',
        ],
    ],
];
