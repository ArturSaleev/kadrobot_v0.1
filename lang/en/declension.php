<?php

return [
    "im" => "Nominative case",
    "rod" => "Genitive case",
    "dat" => "Dative case",
    "vin" => "Accusative case",
    "tvor" => "Creative case",
    "predl" => "Prepositional case",
    "gde" => "local case",
    "predl-o" => "Prepositional case with the preposition oobobo",
    "plural" => "plural",


    'errors' => [
        "no_name" => "Error! Invalid data transfer!",
        "trim_name" => 'All fields "Name" must be filled in',
    ],
    'labels' => [
        'show' => 'Decline names by cases',
        'fio' => 'Decline full name by cases'
    ]
];
