<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Бұл тіркелгі деректері біздің жазбаларымызға сәйкес келмейді.',
    'password' => 'Берілген құпия сөз дұрыс емес.',
    'throttle' => 'Жүйеге кіру әрекеттері тым көп. :секунд секундтан кейін әрекетті қайталаңыз.',

    'email' => 'Электрондық поштаның адресі',
    'password_text' => 'Құпия сөз',
    'remember_me' => 'Мені есте сақтау',
    'login' => 'Кіру',
    'login_text' => 'Авторизация',
    'forgot_password' => 'Құпия сөзіңізді ұмыттыңыз ба?'

];
