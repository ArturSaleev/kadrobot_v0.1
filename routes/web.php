<?php

use App\Http\Controllers\LanguageController;
use App\Http\Controllers\TemplateDocController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\BranchController;
use App\Http\Controllers\RefSexController;
use App\Http\Controllers\T2CardController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\CuratorController;
use App\Http\Controllers\HolidayController;
use App\Http\Controllers\RefBankController;
use App\Http\Controllers\RefMetaController;
use App\Http\Controllers\RefOkedController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\PositionController;
use App\Http\Controllers\RefActionController;
use App\Http\Controllers\RefStatusController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\RefCountryController;
use App\Http\Controllers\RefDocTypeController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\BranchPhoneController;
use App\Http\Controllers\RefDocPlaceController;
use App\Http\Controllers\EmployeeTripController;
use App\Http\Controllers\RefTypePhoneController;
use App\Http\Controllers\EmployeePhoneController;
use App\Http\Controllers\EmployeeStazhController;
use App\Http\Controllers\RefTypeRodstvController;
use App\Http\Controllers\RefUserStatusController;
use App\Http\Controllers\RefVidHolidayController;
use App\Http\Controllers\BranchAddresesController;
use App\Http\Controllers\EmployeeFamilyController;
use App\Http\Controllers\EmployeeTechicController;
use App\Http\Controllers\RefAccountTypeController;
use App\Http\Controllers\RefAddressTypeController;
use App\Http\Controllers\RefFamilyStateController;
use App\Http\Controllers\RefKindHolidayController;
use App\Http\Controllers\RefNationalityController;
use App\Http\Controllers\RefPersonStateController;
use App\Http\Controllers\RefTechnicTypeController;
use App\Http\Controllers\DepartmentPhoneController;
use App\Http\Controllers\EmployeeAddressController;
use App\Http\Controllers\EmployeeHolidayController;
use App\Http\Controllers\EmployeeDocumentController;
use App\Http\Controllers\EmployeeHospitalController;
use App\Http\Controllers\EmployeeInvalideController;
use App\Http\Controllers\EmployeeMilitaryController;
use App\Http\Controllers\RefAllowanceTypeController;
use App\Http\Controllers\RefTransportTripController;
use App\Http\Controllers\EmployeeEducationController;
use App\Http\Controllers\RefVidHolidayTypeController;
use App\Http\Controllers\EmployeeDeclensionController;
use App\Http\Controllers\EmployeeTripFromToController;
use App\Http\Controllers\EmployeePayAllowanceController;
use App\Http\Controllers\EmployeeHolidaysPeriodController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::prefix('/')
    ->middleware('auth')
    ->group(function () {
        Route::resource('roles', RoleController::class);
        Route::resource('permissions', PermissionController::class);

        Route::resource('branches', BranchController::class);
        Route::resource('companies', CompanyController::class);
        Route::get('all-branch-addreses', [
            BranchAddresesController::class,
            'index',
        ])->name('all-branch-addreses.index');
        Route::post('all-branch-addreses', [
            BranchAddresesController::class,
            'store',
        ])->name('all-branch-addreses.store');
        Route::get('all-branch-addreses/create', [
            BranchAddresesController::class,
            'create',
        ])->name('all-branch-addreses.create');
        Route::get('all-branch-addreses/{branchAddreses}', [
            BranchAddresesController::class,
            'show',
        ])->name('all-branch-addreses.show');
        Route::get('all-branch-addreses/{branchAddreses}/edit', [
            BranchAddresesController::class,
            'edit',
        ])->name('all-branch-addreses.edit');
        Route::put('all-branch-addreses/{branchAddreses}', [
            BranchAddresesController::class,
            'update',
        ])->name('all-branch-addreses.update');
        Route::delete('all-branch-addreses/{branchAddreses}', [
            BranchAddresesController::class,
            'destroy',
        ])->name('all-branch-addreses.destroy');

        Route::resource('branch-phones', BranchPhoneController::class);
        Route::resource('curators', CuratorController::class);
        Route::resource('departments', DepartmentController::class);
        Route::resource('department-phones', DepartmentPhoneController::class);
        Route::resource('employees', EmployeeController::class);
        Route::resource('employee-addresses', EmployeeAddressController::class);
        Route::resource(
            'employee-declensions',
            EmployeeDeclensionController::class
        );
        Route::resource(
            'employee-documents',
            EmployeeDocumentController::class
        );
        Route::resource(
            'employee-educations',
            EmployeeEducationController::class
        );
        Route::resource('employee-families', EmployeeFamilyController::class);
        Route::resource('employee-holidays', EmployeeHolidayController::class);
        Route::resource(
            'employee-holidays-periods',
            EmployeeHolidaysPeriodController::class
        );
        Route::resource(
            'employee-hospitals',
            EmployeeHospitalController::class
        );
        Route::resource(
            'employee-invalides',
            EmployeeInvalideController::class
        );
        Route::resource(
            'employee-militaries',
            EmployeeMilitaryController::class
        );
        Route::resource(
            'employee-pay-allowances',
            EmployeePayAllowanceController::class
        );
        Route::resource('employee-phones', EmployeePhoneController::class);
        Route::resource('employee-stazhs', EmployeeStazhController::class);
        Route::resource('employee-techics', EmployeeTechicController::class);
        Route::resource('employee-trips', EmployeeTripController::class);
        Route::resource(
            'employee-trip-from-tos',
            EmployeeTripFromToController::class
        );
        Route::resource('holidays', HolidayController::class);
        Route::post('position_declensions', [PositionController::class, 'declensionPosition']);
        Route::resource('positions', PositionController::class);

        Route::resource('ref-account-types', RefAccountTypeController::class);
        Route::resource('ref-actions', RefActionController::class);
        Route::resource('ref-address-types', RefAddressTypeController::class);
        Route::resource(
            'ref-allowance-types',
            RefAllowanceTypeController::class
        );
        Route::resource('ref-banks', RefBankController::class);
        Route::resource('ref-countries', RefCountryController::class);
        Route::resource('ref-doc-places', RefDocPlaceController::class);
        Route::resource('ref-doc-types', RefDocTypeController::class);
        Route::resource('ref-family-states', RefFamilyStateController::class);
        Route::resource('ref-kind-holidays', RefKindHolidayController::class);
        Route::resource('ref-metas', RefMetaController::class);
        Route::resource('ref-nationalities', RefNationalityController::class);
        Route::resource('ref-okeds', RefOkedController::class);
        Route::resource('ref-person-states', RefPersonStateController::class);
        Route::resource('ref-sexes', RefSexController::class);
        Route::resource('ref-statuses', RefStatusController::class);
        Route::resource('ref-technic-types', RefTechnicTypeController::class);
        Route::resource(
            'ref-transport-trips',
            RefTransportTripController::class
        );
        Route::resource('ref-type-phones', RefTypePhoneController::class);
        Route::resource('ref-type-rodstvs', RefTypeRodstvController::class);
        Route::resource('ref-user-statuses', RefUserStatusController::class);
        Route::resource('ref-vid-holidays', RefVidHolidayController::class);
        Route::resource(
            'ref-vid-holiday-types',
            RefVidHolidayTypeController::class
        );
        Route::resource('t2-cards', T2CardController::class);
        Route::resource('users', UserController::class);

        Route::resource('template_docs', TemplateDocController::class);
    });
