<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\RoleController;
use App\Http\Controllers\Api\BranchController;
use App\Http\Controllers\Api\RefSexController;
use App\Http\Controllers\Api\T2CardController;
use App\Http\Controllers\Api\CompanyController;
use App\Http\Controllers\Api\CuratorController;
use App\Http\Controllers\Api\HolidayController;
use App\Http\Controllers\Api\RefBankController;
use App\Http\Controllers\Api\RefMetaController;
use App\Http\Controllers\Api\RefOkedController;
use App\Http\Controllers\Api\EmployeeController;
use App\Http\Controllers\Api\PositionController;
use App\Http\Controllers\Api\RefActionController;
use App\Http\Controllers\Api\RefStatusController;
use App\Http\Controllers\Api\DepartmentController;
use App\Http\Controllers\Api\RefCountryController;
use App\Http\Controllers\Api\RefDocTypeController;
use App\Http\Controllers\Api\ReportHtmlController;
use App\Http\Controllers\Api\PermissionController;
use App\Http\Controllers\Api\BranchPhoneController;
use App\Http\Controllers\Api\RefDocPlaceController;
use App\Http\Controllers\Api\EmployeeTripController;
use App\Http\Controllers\Api\RefTypePhoneController;
use App\Http\Controllers\Api\BranchT2CardsController;
use App\Http\Controllers\Api\EmployeePhoneController;
use App\Http\Controllers\Api\EmployeeStazhController;
use App\Http\Controllers\Api\RefTypeRodstvController;
use App\Http\Controllers\Api\RefUserStatusController;
use App\Http\Controllers\Api\RefVidHolidayController;
use App\Http\Controllers\Api\CompanyT2CardsController;
use App\Http\Controllers\Api\BranchAddresesController;
use App\Http\Controllers\Api\EmployeeFamilyController;
use App\Http\Controllers\Api\EmployeeTechicController;
use App\Http\Controllers\Api\RefAccountTypeController;
use App\Http\Controllers\Api\RefAddressTypeController;
use App\Http\Controllers\Api\RefFamilyStateController;
use App\Http\Controllers\Api\RefKindHolidayController;
use App\Http\Controllers\Api\RefNationalityController;
use App\Http\Controllers\Api\RefPersonStateController;
use App\Http\Controllers\Api\RefTechnicTypeController;
use App\Http\Controllers\Api\CompanyBranchesController;
use App\Http\Controllers\Api\DepartmentPhoneController;
use App\Http\Controllers\Api\EmployeeT2CardsController;
use App\Http\Controllers\Api\EmployeeAddressController;
use App\Http\Controllers\Api\EmployeeHolidayController;
use App\Http\Controllers\Api\PositionT2CardsController;
use App\Http\Controllers\Api\RefSexEmployeesController;
use App\Http\Controllers\Api\ReportHtmlOtherController;
use App\Http\Controllers\Api\EmployeeCuratorsController;
use App\Http\Controllers\Api\EmployeeDocumentController;
use App\Http\Controllers\Api\EmployeeHospitalController;
use App\Http\Controllers\Api\EmployeeInvalideController;
use App\Http\Controllers\Api\EmployeeMilitaryController;
use App\Http\Controllers\Api\RefActionT2CardsController;
use App\Http\Controllers\Api\RefAllowanceTypeController;
use App\Http\Controllers\Api\RefTransportTripController;
use App\Http\Controllers\Api\BranchDepartmentsController;
use App\Http\Controllers\Api\EmployeeEducationController;
use App\Http\Controllers\Api\PositionEmployeesController;
use App\Http\Controllers\Api\RefStatusRefBanksController;
use App\Http\Controllers\Api\RefVidHolidayTypeController;
use App\Http\Controllers\Api\BranchBranchPhonesController;
use App\Http\Controllers\Api\DepartmentCuratorsController;
use App\Http\Controllers\Api\EmployeeDeclensionController;
use App\Http\Controllers\Api\EmployeeTripFromToController;
use App\Http\Controllers\Api\RefStatusCompaniesController;
use App\Http\Controllers\Api\DepartmentPositionsController;
use App\Http\Controllers\Api\EmployeePayAllowanceController;
use App\Http\Controllers\Api\EmployeeEmployeeTripsController;
use App\Http\Controllers\Api\EmployeeEmployeePhonesController;
use App\Http\Controllers\Api\EmployeeEmployeeStazhsController;
use App\Http\Controllers\Api\EmployeeHolidaysPeriodController;
use App\Http\Controllers\Api\RefUserStatusEmployeesController;
use App\Http\Controllers\Api\BranchAllBranchAddresesController;
use App\Http\Controllers\Api\EmployeeEmployeeTechicsController;
use App\Http\Controllers\Api\RefAccountTypeEmployeesController;
use App\Http\Controllers\Api\RefFamilyStateEmployeesController;
use App\Http\Controllers\Api\RefNationalityEmployeesController;
use App\Http\Controllers\Api\BranchBranchTranslationsController;
use App\Http\Controllers\Api\EmployeeEmployeeFamiliesController;
use App\Http\Controllers\Api\EmployeeEmployeeHolidaysController;
use App\Http\Controllers\Api\RefSexRefSexTranslationsController;
use App\Http\Controllers\Api\RefTypePhoneBranchPhonesController;
use App\Http\Controllers\Api\EmployeeEmployeeAddressesController;
use App\Http\Controllers\Api\EmployeeEmployeeDocumentsController;
use App\Http\Controllers\Api\EmployeeEmployeeInvalidesController;
use App\Http\Controllers\Api\EmployeeEmployeeHospitalsController;
use App\Http\Controllers\Api\CompanyCompanyTranslationsController;
use App\Http\Controllers\Api\DepartmentDepartmentPhonesController;
use App\Http\Controllers\Api\EmployeeEmployeeMilitariesController;
use App\Http\Controllers\Api\EmployeeEmployeeEducationsController;
use App\Http\Controllers\Api\RefBankRefBankTranslationsController;
use App\Http\Controllers\Api\RefOkedRefOkedTranslationsController;
use App\Http\Controllers\Api\RefTypePhoneEmployeePhonesController;
use App\Http\Controllers\Api\ReportHtmlReportHtmlOthersController;
use App\Http\Controllers\Api\EmployeeEmployeeDeclensionsController;
use App\Http\Controllers\Api\PositionPositionDeclensionsController;
use App\Http\Controllers\Api\RefDocTypeEmployeeDocumentsController;
use App\Http\Controllers\Api\PositionPositionTranslationsController;
use App\Http\Controllers\Api\RefDocPlaceEmployeeDocumentsController;
use App\Http\Controllers\Api\RefTypePhoneDepartmentPhonesController;
use App\Http\Controllers\Api\EmployeeEmployeePayAllowancesController;
use App\Http\Controllers\Api\RefTechnicTypeEmployeeTechicsController;
use App\Http\Controllers\Api\RefTransportTripEmployeeTripsController;
use App\Http\Controllers\Api\RefVidHolidayEmployeeHolidaysController;
use App\Http\Controllers\Api\RefActionRefActionTranslationsController;
use App\Http\Controllers\Api\RefFamilyStateEmployeeFamiliesController;
use App\Http\Controllers\Api\RefStatusRefStatusTranslationsController;
use App\Http\Controllers\Api\EmployeeEmployeeHolidaysPeriodsController;
use App\Http\Controllers\Api\EmployeeTripEmployeeTripFromTosController;
use App\Http\Controllers\Api\RefAddressTypeEmployeeAddressesController;
use App\Http\Controllers\Api\RefAddressTypeAllBranchAddresesController;
use App\Http\Controllers\Api\RefVidHolidayTypeRefVidHolidaysController;
use App\Http\Controllers\Api\DepartmentDepartmentTranslationsController;
use App\Http\Controllers\Api\RefCountryRefCountryTranslationsController;
use App\Http\Controllers\Api\RefDocTypeRefDocTypeTranslationsController;
use App\Http\Controllers\Api\RefDocPlaceRefDocPlaceTranslationsController;
use App\Http\Controllers\Api\RefTransportTripEmployeeTripFromTosController;
use App\Http\Controllers\Api\EmployeeTripEmployeeTripTranslationsController;
use App\Http\Controllers\Api\RefTypePhoneRefTypePhoneTranslationsController;
use App\Http\Controllers\Api\RefAllowanceTypeEmployeePayAllowancesController;
use App\Http\Controllers\Api\EmployeeStazhEmployeeStazhTranslationsController;
use App\Http\Controllers\Api\RefTypeRodstvRefTypeRodstvTranslationsController;
use App\Http\Controllers\Api\RefUserStatusRefUserStatusTranslationsController;
use App\Http\Controllers\Api\RefVidHolidayRefVidHolidayTranslationsController;
use App\Http\Controllers\Api\BranchAddresesBranchAddresesTranslationsController;
use App\Http\Controllers\Api\RefAccountTypeRefAccountTypeTranslationsController;
use App\Http\Controllers\Api\RefAddressTypeRefAddressTypeTranslationsController;
use App\Http\Controllers\Api\RefFamilyStateRefFamilyStateTranslationsController;
use App\Http\Controllers\Api\RefKindHolidayRefKindHolidayTranslationsController;
use App\Http\Controllers\Api\RefNationalityRefNationalityTranslationsController;
use App\Http\Controllers\Api\RefPersonStateRefPersonStateTranslationsController;
use App\Http\Controllers\Api\RefTechnicTypeRefTechnicTypeTranslationsController;
use App\Http\Controllers\Api\EmployeeAddressEmployeeAddressTranslationsController;
use App\Http\Controllers\Api\RefTransportTripRefTransportTripTranstionsController;
use App\Http\Controllers\Api\RefAllowanceTypeRefAllowanceTypeTranslationsController;
use App\Http\Controllers\Api\RefVidHolidayTypeRefVidHolidayTypeTranslationsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [AuthController::class, 'login'])->name('api.login');

Route::middleware('auth:sanctum')
    ->get('/user', function (Request $request) {
        return $request->user();
    })
    ->name('api.user');

Route::name('api.')
    ->middleware('auth:sanctum')
    ->group(function () {
        Route::apiResource('roles', RoleController::class);
        Route::apiResource('permissions', PermissionController::class);

        Route::apiResource('branches', BranchController::class);

        // Branch T2 Cards
        Route::get('/branches/{branch}/t2-cards', [
            BranchT2CardsController::class,
            'index',
        ])->name('branches.t2-cards.index');
        Route::post('/branches/{branch}/t2-cards', [
            BranchT2CardsController::class,
            'store',
        ])->name('branches.t2-cards.store');

        // Branch Branch Phones
        Route::get('/branches/{branch}/branch-phones', [
            BranchBranchPhonesController::class,
            'index',
        ])->name('branches.branch-phones.index');
        Route::post('/branches/{branch}/branch-phones', [
            BranchBranchPhonesController::class,
            'store',
        ])->name('branches.branch-phones.store');

        // Branch Ref Departments
        Route::get('/branches/{branch}/departments', [
            BranchDepartmentsController::class,
            'index',
        ])->name('branches.departments.index');
        Route::post('/branches/{branch}/departments', [
            BranchDepartmentsController::class,
            'store',
        ])->name('branches.departments.store');

        // Branch Branch Translations
        Route::get('/branches/{branch}/branch-translations', [
            BranchBranchTranslationsController::class,
            'index',
        ])->name('branches.branch-translations.index');
        Route::post('/branches/{branch}/branch-translations', [
            BranchBranchTranslationsController::class,
            'store',
        ])->name('branches.branch-translations.store');

        // Branch Branch Addreses
        Route::get('/branches/{branch}/all-branch-addreses', [
            BranchAllBranchAddresesController::class,
            'index',
        ])->name('branches.all-branch-addreses.index');
        Route::post('/branches/{branch}/all-branch-addreses', [
            BranchAllBranchAddresesController::class,
            'store',
        ])->name('branches.all-branch-addreses.store');

        Route::apiResource('companies', CompanyController::class);

        // Company Ref Branches
        Route::get('/companies/{company}/branches', [
            CompanyBranchesController::class,
            'index',
        ])->name('companies.branches.index');
        Route::post('/companies/{company}/branches', [
            CompanyBranchesController::class,
            'store',
        ])->name('companies.branches.store');

        // Company T2 Cards
        Route::get('/companies/{company}/t2-cards', [
            CompanyT2CardsController::class,
            'index',
        ])->name('companies.t2-cards.index');
        Route::post('/companies/{company}/t2-cards', [
            CompanyT2CardsController::class,
            'store',
        ])->name('companies.t2-cards.store');

        // Company Company Translations
        Route::get('/companies/{company}/company-translations', [
            CompanyCompanyTranslationsController::class,
            'index',
        ])->name('companies.company-translations.index');
        Route::post('/companies/{company}/company-translations', [
            CompanyCompanyTranslationsController::class,
            'store',
        ])->name('companies.company-translations.store');

        Route::apiResource(
            'all-branch-addreses',
            BranchAddresesController::class
        );

        // BranchAddreses Branch Addreses Translations
        Route::get(
            '/all-branch-addreses/{branchAddreses}/branch-addreses-translations',
            [BranchAddresesBranchAddresesTranslationsController::class, 'index']
        )->name('all-branch-addreses.branch-addreses-translations.index');
        Route::post(
            '/all-branch-addreses/{branchAddreses}/branch-addreses-translations',
            [BranchAddresesBranchAddresesTranslationsController::class, 'store']
        )->name('all-branch-addreses.branch-addreses-translations.store');

        Route::apiResource('branch-phones', BranchPhoneController::class);

        Route::apiResource('curators', CuratorController::class);

        Route::apiResource('departments', DepartmentController::class);

        // Department Department Phones
        Route::get('/departments/{department}/department-phones', [
            DepartmentDepartmentPhonesController::class,
            'index',
        ])->name('departments.department-phones.index');
        Route::post('/departments/{department}/department-phones', [
            DepartmentDepartmentPhonesController::class,
            'store',
        ])->name('departments.department-phones.store');

        // Department Curators
        Route::get('/departments/{department}/curators', [
            DepartmentCuratorsController::class,
            'index',
        ])->name('departments.curators.index');
        Route::post('/departments/{department}/curators', [
            DepartmentCuratorsController::class,
            'store',
        ])->name('departments.curators.store');

        // Department Department Transtions
        Route::get('/departments/{department}/department-translations', [
            DepartmentDepartmentTranslationsController::class,
            'index',
        ])->name('departments.department-translations.index');
        Route::post('/departments/{department}/department-translations', [
            DepartmentDepartmentTranslationsController::class,
            'store',
        ])->name('departments.department-translations.store');

        // Department Positions
        Route::get('/departments/{department}/positions', [
            DepartmentPositionsController::class,
            'index',
        ])->name('departments.positions.index');
        Route::post('/departments/{department}/positions', [
            DepartmentPositionsController::class,
            'store',
        ])->name('departments.positions.store');

        Route::apiResource(
            'department-phones',
            DepartmentPhoneController::class
        );

        Route::apiResource('employees', EmployeeController::class);

        // Employee Curators
        Route::get('/employees/{employee}/curators', [
            EmployeeCuratorsController::class,
            'index',
        ])->name('employees.curators.index');
        Route::post('/employees/{employee}/curators', [
            EmployeeCuratorsController::class,
            'store',
        ])->name('employees.curators.store');

        // Employee Employee Phones
        Route::get('/employees/{employee}/employee-phones', [
            EmployeeEmployeePhonesController::class,
            'index',
        ])->name('employees.employee-phones.index');
        Route::post('/employees/{employee}/employee-phones', [
            EmployeeEmployeePhonesController::class,
            'store',
        ])->name('employees.employee-phones.store');

        // Employee Employee Addresses
        Route::get('/employees/{employee}/employee-addresses', [
            EmployeeEmployeeAddressesController::class,
            'index',
        ])->name('employees.employee-addresses.index');
        Route::post('/employees/{employee}/employee-addresses', [
            EmployeeEmployeeAddressesController::class,
            'store',
        ])->name('employees.employee-addresses.store');

        // Employee Employee Militaries
        Route::get('/employees/{employee}/employee-militaries', [
            EmployeeEmployeeMilitariesController::class,
            'index',
        ])->name('employees.employee-militaries.index');
        Route::post('/employees/{employee}/employee-militaries', [
            EmployeeEmployeeMilitariesController::class,
            'store',
        ])->name('employees.employee-militaries.store');

        // Employee Employee Documents
        Route::get('/employees/{employee}/employee-documents', [
            EmployeeEmployeeDocumentsController::class,
            'index',
        ])->name('employees.employee-documents.index');
        Route::post('/employees/{employee}/employee-documents', [
            EmployeeEmployeeDocumentsController::class,
            'store',
        ])->name('employees.employee-documents.store');

        // Employee Employee Invalides
        Route::get('/employees/{employee}/employee-invalides', [
            EmployeeEmployeeInvalidesController::class,
            'index',
        ])->name('employees.employee-invalides.index');
        Route::post('/employees/{employee}/employee-invalides', [
            EmployeeEmployeeInvalidesController::class,
            'store',
        ])->name('employees.employee-invalides.store');

        // Employee Employee Pay Allowances
        Route::get('/employees/{employee}/employee-pay-allowances', [
            EmployeeEmployeePayAllowancesController::class,
            'index',
        ])->name('employees.employee-pay-allowances.index');
        Route::post('/employees/{employee}/employee-pay-allowances', [
            EmployeeEmployeePayAllowancesController::class,
            'store',
        ])->name('employees.employee-pay-allowances.store');

        // Employee Employee Families
        Route::get('/employees/{employee}/employee-families', [
            EmployeeEmployeeFamiliesController::class,
            'index',
        ])->name('employees.employee-families.index');
        Route::post('/employees/{employee}/employee-families', [
            EmployeeEmployeeFamiliesController::class,
            'store',
        ])->name('employees.employee-families.store');

        // Employee Employee Educations
        Route::get('/employees/{employee}/employee-educations', [
            EmployeeEmployeeEducationsController::class,
            'index',
        ])->name('employees.employee-educations.index');
        Route::post('/employees/{employee}/employee-educations', [
            EmployeeEmployeeEducationsController::class,
            'store',
        ])->name('employees.employee-educations.store');

        // Employee Employee Stazhs
        Route::get('/employees/{employee}/employee-stazhs', [
            EmployeeEmployeeStazhsController::class,
            'index',
        ])->name('employees.employee-stazhs.index');
        Route::post('/employees/{employee}/employee-stazhs', [
            EmployeeEmployeeStazhsController::class,
            'store',
        ])->name('employees.employee-stazhs.store');

        // Employee Employee Trips
        Route::get('/employees/{employee}/employee-trips', [
            EmployeeEmployeeTripsController::class,
            'index',
        ])->name('employees.employee-trips.index');
        Route::post('/employees/{employee}/employee-trips', [
            EmployeeEmployeeTripsController::class,
            'store',
        ])->name('employees.employee-trips.store');

        // Employee Employee Techics
        Route::get('/employees/{employee}/employee-techics', [
            EmployeeEmployeeTechicsController::class,
            'index',
        ])->name('employees.employee-techics.index');
        Route::post('/employees/{employee}/employee-techics', [
            EmployeeEmployeeTechicsController::class,
            'store',
        ])->name('employees.employee-techics.store');

        // Employee Employee Holidays
        Route::get('/employees/{employee}/employee-holidays', [
            EmployeeEmployeeHolidaysController::class,
            'index',
        ])->name('employees.employee-holidays.index');
        Route::post('/employees/{employee}/employee-holidays', [
            EmployeeEmployeeHolidaysController::class,
            'store',
        ])->name('employees.employee-holidays.store');

        // Employee Employee Holidays Periods
        Route::get('/employees/{employee}/employee-holidays-periods', [
            EmployeeEmployeeHolidaysPeriodsController::class,
            'index',
        ])->name('employees.employee-holidays-periods.index');
        Route::post('/employees/{employee}/employee-holidays-periods', [
            EmployeeEmployeeHolidaysPeriodsController::class,
            'store',
        ])->name('employees.employee-holidays-periods.store');

        // Employee T2 Cards
        Route::get('/employees/{employee}/t2-cards', [
            EmployeeT2CardsController::class,
            'index',
        ])->name('employees.t2-cards.index');
        Route::post('/employees/{employee}/t2-cards', [
            EmployeeT2CardsController::class,
            'store',
        ])->name('employees.t2-cards.store');

        // Employee Employee Declensions
        Route::get('/employees/{employee}/employee-declensions', [
            EmployeeEmployeeDeclensionsController::class,
            'index',
        ])->name('employees.employee-declensions.index');
        Route::post('/employees/{employee}/employee-declensions', [
            EmployeeEmployeeDeclensionsController::class,
            'store',
        ])->name('employees.employee-declensions.store');

        // Employee Employee Hospitals
        Route::get('/employees/{employee}/employee-hospitals', [
            EmployeeEmployeeHospitalsController::class,
            'index',
        ])->name('employees.employee-hospitals.index');
        Route::post('/employees/{employee}/employee-hospitals', [
            EmployeeEmployeeHospitalsController::class,
            'store',
        ])->name('employees.employee-hospitals.store');

        Route::apiResource(
            'employee-addresses',
            EmployeeAddressController::class
        );

        // EmployeeAddress Employee Address Translations
        Route::get(
            '/employee-addresses/{employeeAddress}/employee-address-translations',
            [
                EmployeeAddressEmployeeAddressTranslationsController::class,
                'index',
            ]
        )->name('employee-addresses.employee-address-translations.index');
        Route::post(
            '/employee-addresses/{employeeAddress}/employee-address-translations',
            [
                EmployeeAddressEmployeeAddressTranslationsController::class,
                'store',
            ]
        )->name('employee-addresses.employee-address-translations.store');

        Route::apiResource(
            'employee-declensions',
            EmployeeDeclensionController::class
        );

        Route::apiResource(
            'employee-documents',
            EmployeeDocumentController::class
        );

        Route::apiResource(
            'employee-educations',
            EmployeeEducationController::class
        );

        Route::apiResource(
            'employee-families',
            EmployeeFamilyController::class
        );

        Route::apiResource(
            'employee-holidays',
            EmployeeHolidayController::class
        );

        Route::apiResource(
            'employee-holidays-periods',
            EmployeeHolidaysPeriodController::class
        );

        Route::apiResource(
            'employee-hospitals',
            EmployeeHospitalController::class
        );

        Route::apiResource(
            'employee-invalides',
            EmployeeInvalideController::class
        );

        Route::apiResource(
            'employee-militaries',
            EmployeeMilitaryController::class
        );

        Route::apiResource(
            'employee-pay-allowances',
            EmployeePayAllowanceController::class
        );

        Route::apiResource('employee-phones', EmployeePhoneController::class);

        Route::apiResource('employee-stazhs', EmployeeStazhController::class);

        // EmployeeStazh Employee Stazh Translations
        Route::get(
            '/employee-stazhs/{employeeStazh}/employee-stazh-translations',
            [EmployeeStazhEmployeeStazhTranslationsController::class, 'index']
        )->name('employee-stazhs.employee-stazh-translations.index');
        Route::post(
            '/employee-stazhs/{employeeStazh}/employee-stazh-translations',
            [EmployeeStazhEmployeeStazhTranslationsController::class, 'store']
        )->name('employee-stazhs.employee-stazh-translations.store');

        Route::apiResource('employee-techics', EmployeeTechicController::class);

        Route::apiResource('employee-trips', EmployeeTripController::class);

        // EmployeeTrip Employee Trip From Tos
        Route::get('/employee-trips/{employeeTrip}/employee-trip-from-tos', [
            EmployeeTripEmployeeTripFromTosController::class,
            'index',
        ])->name('employee-trips.employee-trip-from-tos.index');
        Route::post('/employee-trips/{employeeTrip}/employee-trip-from-tos', [
            EmployeeTripEmployeeTripFromTosController::class,
            'store',
        ])->name('employee-trips.employee-trip-from-tos.store');

        // EmployeeTrip Employee Trip Translations
        Route::get(
            '/employee-trips/{employeeTrip}/employee-trip-translations',
            [EmployeeTripEmployeeTripTranslationsController::class, 'index']
        )->name('employee-trips.employee-trip-translations.index');
        Route::post(
            '/employee-trips/{employeeTrip}/employee-trip-translations',
            [EmployeeTripEmployeeTripTranslationsController::class, 'store']
        )->name('employee-trips.employee-trip-translations.store');

        Route::apiResource(
            'employee-trip-from-tos',
            EmployeeTripFromToController::class
        );

        Route::apiResource('holidays', HolidayController::class);

        Route::apiResource('positions', PositionController::class);

        // Position T2 Cards
        Route::get('/positions/{position}/t2-cards', [
            PositionT2CardsController::class,
            'index',
        ])->name('positions.t2-cards.index');
        Route::post('/positions/{position}/t2-cards', [
            PositionT2CardsController::class,
            'store',
        ])->name('positions.t2-cards.store');

        // Position Position Translations
        Route::get('/positions/{position}/position-translations', [
            PositionPositionTranslationsController::class,
            'index',
        ])->name('positions.position-translations.index');
        Route::post('/positions/{position}/position-translations', [
            PositionPositionTranslationsController::class,
            'store',
        ])->name('positions.position-translations.store');

        // Position Employees
        Route::get('/positions/{position}/employees', [
            PositionEmployeesController::class,
            'index',
        ])->name('positions.employees.index');
        Route::post('/positions/{position}/employees', [
            PositionEmployeesController::class,
            'store',
        ])->name('positions.employees.store');

        // Position Position Declensions
        Route::get('/positions/{position}/position-declensions', [
            PositionPositionDeclensionsController::class,
            'index',
        ])->name('positions.position-declensions.index');
        Route::post('/positions/{position}/position-declensions', [
            PositionPositionDeclensionsController::class,
            'store',
        ])->name('positions.position-declensions.store');

        Route::apiResource(
            'ref-account-types',
            RefAccountTypeController::class
        );

        // RefAccountType Employees
        Route::get('/ref-account-types/{refAccountType}/employees', [
            RefAccountTypeEmployeesController::class,
            'index',
        ])->name('ref-account-types.employees.index');
        Route::post('/ref-account-types/{refAccountType}/employees', [
            RefAccountTypeEmployeesController::class,
            'store',
        ])->name('ref-account-types.employees.store');

        // RefAccountType Ref Account Type Translations
        Route::get(
            '/ref-account-types/{refAccountType}/ref-account-type-translations',
            [RefAccountTypeRefAccountTypeTranslationsController::class, 'index']
        )->name('ref-account-types.ref-account-type-translations.index');
        Route::post(
            '/ref-account-types/{refAccountType}/ref-account-type-translations',
            [RefAccountTypeRefAccountTypeTranslationsController::class, 'store']
        )->name('ref-account-types.ref-account-type-translations.store');

        Route::apiResource('ref-actions', RefActionController::class);

        // RefAction T2 Cards
        Route::get('/ref-actions/{refAction}/t2-cards', [
            RefActionT2CardsController::class,
            'index',
        ])->name('ref-actions.t2-cards.index');
        Route::post('/ref-actions/{refAction}/t2-cards', [
            RefActionT2CardsController::class,
            'store',
        ])->name('ref-actions.t2-cards.store');

        // RefAction Ref Action Translations
        Route::get('/ref-actions/{refAction}/ref-action-translations', [
            RefActionRefActionTranslationsController::class,
            'index',
        ])->name('ref-actions.ref-action-translations.index');
        Route::post('/ref-actions/{refAction}/ref-action-translations', [
            RefActionRefActionTranslationsController::class,
            'store',
        ])->name('ref-actions.ref-action-translations.store');

        Route::apiResource(
            'ref-address-types',
            RefAddressTypeController::class
        );

        // RefAddressType Employee Addresses
        Route::get('/ref-address-types/{refAddressType}/employee-addresses', [
            RefAddressTypeEmployeeAddressesController::class,
            'index',
        ])->name('ref-address-types.employee-addresses.index');
        Route::post('/ref-address-types/{refAddressType}/employee-addresses', [
            RefAddressTypeEmployeeAddressesController::class,
            'store',
        ])->name('ref-address-types.employee-addresses.store');

        // RefAddressType All Company Addreses
        Route::get('/ref-address-types/{refAddressType}/all-branch-addreses', [
            RefAddressTypeAllBranchAddresesController::class,
            'index',
        ])->name('ref-address-types.all-branch-addreses.index');
        Route::post('/ref-address-types/{refAddressType}/all-branch-addreses', [
            RefAddressTypeAllBranchAddresesController::class,
            'store',
        ])->name('ref-address-types.all-branch-addreses.store');

        // RefAddressType Ref Address Type Translations
        Route::get(
            '/ref-address-types/{refAddressType}/ref-address-type-translations',
            [RefAddressTypeRefAddressTypeTranslationsController::class, 'index']
        )->name('ref-address-types.ref-address-type-translations.index');
        Route::post(
            '/ref-address-types/{refAddressType}/ref-address-type-translations',
            [RefAddressTypeRefAddressTypeTranslationsController::class, 'store']
        )->name('ref-address-types.ref-address-type-translations.store');

        Route::apiResource(
            'ref-allowance-types',
            RefAllowanceTypeController::class
        );

        // RefAllowanceType Employee Pay Allowances
        Route::get(
            '/ref-allowance-types/{refAllowanceType}/employee-pay-allowances',
            [RefAllowanceTypeEmployeePayAllowancesController::class, 'index']
        )->name('ref-allowance-types.employee-pay-allowances.index');
        Route::post(
            '/ref-allowance-types/{refAllowanceType}/employee-pay-allowances',
            [RefAllowanceTypeEmployeePayAllowancesController::class, 'store']
        )->name('ref-allowance-types.employee-pay-allowances.store');

        // RefAllowanceType Ref Allowance Type Translations
        Route::get(
            '/ref-allowance-types/{refAllowanceType}/ref-allowance-type-translations',
            [
                RefAllowanceTypeRefAllowanceTypeTranslationsController::class,
                'index',
            ]
        )->name('ref-allowance-types.ref-allowance-type-translations.index');
        Route::post(
            '/ref-allowance-types/{refAllowanceType}/ref-allowance-type-translations',
            [
                RefAllowanceTypeRefAllowanceTypeTranslationsController::class,
                'store',
            ]
        )->name('ref-allowance-types.ref-allowance-type-translations.store');

        Route::apiResource('ref-banks', RefBankController::class);

        // RefBank Ref Bank Translations
        Route::get('/ref-banks/{refBank}/ref-bank-translations', [
            RefBankRefBankTranslationsController::class,
            'index',
        ])->name('ref-banks.ref-bank-translations.index');
        Route::post('/ref-banks/{refBank}/ref-bank-translations', [
            RefBankRefBankTranslationsController::class,
            'store',
        ])->name('ref-banks.ref-bank-translations.store');

        Route::apiResource('ref-countries', RefCountryController::class);

        // RefCountry Ref Country Translations
        Route::get('/ref-countries/{refCountry}/ref-country-translations', [
            RefCountryRefCountryTranslationsController::class,
            'index',
        ])->name('ref-countries.ref-country-translations.index');
        Route::post('/ref-countries/{refCountry}/ref-country-translations', [
            RefCountryRefCountryTranslationsController::class,
            'store',
        ])->name('ref-countries.ref-country-translations.store');

        Route::apiResource('ref-doc-places', RefDocPlaceController::class);

        // RefDocPlace Employee Documents
        Route::get('/ref-doc-places/{refDocPlace}/employee-documents', [
            RefDocPlaceEmployeeDocumentsController::class,
            'index',
        ])->name('ref-doc-places.employee-documents.index');
        Route::post('/ref-doc-places/{refDocPlace}/employee-documents', [
            RefDocPlaceEmployeeDocumentsController::class,
            'store',
        ])->name('ref-doc-places.employee-documents.store');

        // RefDocPlace Ref Doc Place Tranlations
        Route::get('/ref-doc-places/{refDocPlace}/ref-doc-place-translations', [
            RefDocPlaceRefDocPlaceTranslationsController::class,
            'index',
        ])->name('ref-doc-places.ref-doc-place-translations.index');
        Route::post(
            '/ref-doc-places/{refDocPlace}/ref-doc-place-translations',
            [RefDocPlaceRefDocPlaceTranslationsController::class, 'store']
        )->name('ref-doc-places.ref-doc-place-translations.store');

        Route::apiResource('ref-doc-types', RefDocTypeController::class);

        // RefDocType Employee Documents
        Route::get('/ref-doc-types/{refDocType}/employee-documents', [
            RefDocTypeEmployeeDocumentsController::class,
            'index',
        ])->name('ref-doc-types.employee-documents.index');
        Route::post('/ref-doc-types/{refDocType}/employee-documents', [
            RefDocTypeEmployeeDocumentsController::class,
            'store',
        ])->name('ref-doc-types.employee-documents.store');

        // RefDocType Ref Doc Type Translations
        Route::get('/ref-doc-types/{refDocType}/ref-doc-type-translations', [
            RefDocTypeRefDocTypeTranslationsController::class,
            'index',
        ])->name('ref-doc-types.ref-doc-type-translations.index');
        Route::post('/ref-doc-types/{refDocType}/ref-doc-type-translations', [
            RefDocTypeRefDocTypeTranslationsController::class,
            'store',
        ])->name('ref-doc-types.ref-doc-type-translations.store');

        Route::apiResource(
            'ref-family-states',
            RefFamilyStateController::class
        );

        // RefFamilyState Employees
        Route::get('/ref-family-states/{refFamilyState}/employees', [
            RefFamilyStateEmployeesController::class,
            'index',
        ])->name('ref-family-states.employees.index');
        Route::post('/ref-family-states/{refFamilyState}/employees', [
            RefFamilyStateEmployeesController::class,
            'store',
        ])->name('ref-family-states.employees.store');

        // RefFamilyState Employee Families
        Route::get('/ref-family-states/{refFamilyState}/employee-families', [
            RefFamilyStateEmployeeFamiliesController::class,
            'index',
        ])->name('ref-family-states.employee-families.index');
        Route::post('/ref-family-states/{refFamilyState}/employee-families', [
            RefFamilyStateEmployeeFamiliesController::class,
            'store',
        ])->name('ref-family-states.employee-families.store');

        // RefFamilyState Ref Family State Translations
        Route::get(
            '/ref-family-states/{refFamilyState}/ref-family-state-translations',
            [RefFamilyStateRefFamilyStateTranslationsController::class, 'index']
        )->name('ref-family-states.ref-family-state-translations.index');
        Route::post(
            '/ref-family-states/{refFamilyState}/ref-family-state-translations',
            [RefFamilyStateRefFamilyStateTranslationsController::class, 'store']
        )->name('ref-family-states.ref-family-state-translations.store');

        Route::apiResource(
            'ref-kind-holidays',
            RefKindHolidayController::class
        );

        // RefKindHoliday Ref Kind Holiday Translations
        Route::get(
            '/ref-kind-holidays/{refKindHoliday}/ref-kind-holiday-translations',
            [RefKindHolidayRefKindHolidayTranslationsController::class, 'index']
        )->name('ref-kind-holidays.ref-kind-holiday-translations.index');
        Route::post(
            '/ref-kind-holidays/{refKindHoliday}/ref-kind-holiday-translations',
            [RefKindHolidayRefKindHolidayTranslationsController::class, 'store']
        )->name('ref-kind-holidays.ref-kind-holiday-translations.store');

        Route::apiResource('ref-metas', RefMetaController::class);

        Route::apiResource(
            'ref-nationalities',
            RefNationalityController::class
        );

        // RefNationality Employees
        Route::get('/ref-nationalities/{refNationality}/employees', [
            RefNationalityEmployeesController::class,
            'index',
        ])->name('ref-nationalities.employees.index');
        Route::post('/ref-nationalities/{refNationality}/employees', [
            RefNationalityEmployeesController::class,
            'store',
        ])->name('ref-nationalities.employees.store');

        // RefNationality Ref Nationality Translations
        Route::get(
            '/ref-nationalities/{refNationality}/ref-nationality-translations',
            [RefNationalityRefNationalityTranslationsController::class, 'index']
        )->name('ref-nationalities.ref-nationality-translations.index');
        Route::post(
            '/ref-nationalities/{refNationality}/ref-nationality-translations',
            [RefNationalityRefNationalityTranslationsController::class, 'store']
        )->name('ref-nationalities.ref-nationality-translations.store');

        Route::apiResource('ref-okeds', RefOkedController::class);

        // RefOked Ref Oked Transations
        Route::get('/ref-okeds/{refOked}/ref-oked-translations', [
            RefOkedRefOkedTranslationsController::class,
            'index',
        ])->name('ref-okeds.ref-oked-translations.index');
        Route::post('/ref-okeds/{refOked}/ref-oked-translations', [
            RefOkedRefOkedTranslationsController::class,
            'store',
        ])->name('ref-okeds.ref-oked-translations.store');

        Route::apiResource(
            'ref-person-states',
            RefPersonStateController::class
        );

        // RefPersonState Ref Person State Transtions
        Route::get(
            '/ref-person-states/{refPersonState}/ref-person-state-translations',
            [RefPersonStateRefPersonStateTranslationsController::class, 'index']
        )->name('ref-person-states.ref-person-state-translations.index');
        Route::post(
            '/ref-person-states/{refPersonState}/ref-person-state-translations',
            [RefPersonStateRefPersonStateTranslationsController::class, 'store']
        )->name('ref-person-states.ref-person-state-translations.store');

        Route::apiResource('ref-sexes', RefSexController::class);

        // RefSex Employees
        Route::get('/ref-sexes/{refSex}/employees', [
            RefSexEmployeesController::class,
            'index',
        ])->name('ref-sexes.employees.index');
        Route::post('/ref-sexes/{refSex}/employees', [
            RefSexEmployeesController::class,
            'store',
        ])->name('ref-sexes.employees.store');

        // RefSex Ref Sex Translations
        Route::get('/ref-sexes/{refSex}/ref-sex-translations', [
            RefSexRefSexTranslationsController::class,
            'index',
        ])->name('ref-sexes.ref-sex-translations.index');
        Route::post('/ref-sexes/{refSex}/ref-sex-translations', [
            RefSexRefSexTranslationsController::class,
            'store',
        ])->name('ref-sexes.ref-sex-translations.store');

        Route::apiResource('ref-statuses', RefStatusController::class);

        // RefStatus Ref Banks
        Route::get('/ref-statuses/{refStatus}/ref-banks', [
            RefStatusRefBanksController::class,
            'index',
        ])->name('ref-statuses.ref-banks.index');
        Route::post('/ref-statuses/{refStatus}/ref-banks', [
            RefStatusRefBanksController::class,
            'store',
        ])->name('ref-statuses.ref-banks.store');

        // RefStatus Companies
        Route::get('/ref-statuses/{refStatus}/companies', [
            RefStatusCompaniesController::class,
            'index',
        ])->name('ref-statuses.companies.index');
        Route::post('/ref-statuses/{refStatus}/companies', [
            RefStatusCompaniesController::class,
            'store',
        ])->name('ref-statuses.companies.store');

        // RefStatus Ref Status Translations
        Route::get('/ref-statuses/{refStatus}/ref-status-translations', [
            RefStatusRefStatusTranslationsController::class,
            'index',
        ])->name('ref-statuses.ref-status-translations.index');
        Route::post('/ref-statuses/{refStatus}/ref-status-translations', [
            RefStatusRefStatusTranslationsController::class,
            'store',
        ])->name('ref-statuses.ref-status-translations.store');

        Route::apiResource(
            'ref-technic-types',
            RefTechnicTypeController::class
        );

        // RefTechnicType Employee Techics
        Route::get('/ref-technic-types/{refTechnicType}/employee-techics', [
            RefTechnicTypeEmployeeTechicsController::class,
            'index',
        ])->name('ref-technic-types.employee-techics.index');
        Route::post('/ref-technic-types/{refTechnicType}/employee-techics', [
            RefTechnicTypeEmployeeTechicsController::class,
            'store',
        ])->name('ref-technic-types.employee-techics.store');

        // RefTechnicType Ref Technic Type Translations
        Route::get(
            '/ref-technic-types/{refTechnicType}/ref-technic-type-translations',
            [RefTechnicTypeRefTechnicTypeTranslationsController::class, 'index']
        )->name('ref-technic-types.ref-technic-type-translations.index');
        Route::post(
            '/ref-technic-types/{refTechnicType}/ref-technic-type-translations',
            [RefTechnicTypeRefTechnicTypeTranslationsController::class, 'store']
        )->name('ref-technic-types.ref-technic-type-translations.store');

        Route::apiResource(
            'ref-transport-trips',
            RefTransportTripController::class
        );

        // RefTransportTrip Employee Trips
        Route::get('/ref-transport-trips/{refTransportTrip}/employee-trips', [
            RefTransportTripEmployeeTripsController::class,
            'index',
        ])->name('ref-transport-trips.employee-trips.index');
        Route::post('/ref-transport-trips/{refTransportTrip}/employee-trips', [
            RefTransportTripEmployeeTripsController::class,
            'store',
        ])->name('ref-transport-trips.employee-trips.store');

        // RefTransportTrip Employee Trip From Tos
        Route::get(
            '/ref-transport-trips/{refTransportTrip}/employee-trip-from-tos',
            [RefTransportTripEmployeeTripFromTosController::class, 'index']
        )->name('ref-transport-trips.employee-trip-from-tos.index');
        Route::post(
            '/ref-transport-trips/{refTransportTrip}/employee-trip-from-tos',
            [RefTransportTripEmployeeTripFromTosController::class, 'store']
        )->name('ref-transport-trips.employee-trip-from-tos.store');

        // RefTransportTrip Ref Transport Trip Transtions
        Route::get(
            '/ref-transport-trips/{refTransportTrip}/ref-transport-trip-transtions',
            [
                RefTransportTripRefTransportTripTranstionsController::class,
                'index',
            ]
        )->name('ref-transport-trips.ref-transport-trip-transtions.index');
        Route::post(
            '/ref-transport-trips/{refTransportTrip}/ref-transport-trip-transtions',
            [
                RefTransportTripRefTransportTripTranstionsController::class,
                'store',
            ]
        )->name('ref-transport-trips.ref-transport-trip-transtions.store');

        Route::apiResource('ref-type-phones', RefTypePhoneController::class);

        // RefTypePhone Employee Phones
        Route::get('/ref-type-phones/{refTypePhone}/employee-phones', [
            RefTypePhoneEmployeePhonesController::class,
            'index',
        ])->name('ref-type-phones.employee-phones.index');
        Route::post('/ref-type-phones/{refTypePhone}/employee-phones', [
            RefTypePhoneEmployeePhonesController::class,
            'store',
        ])->name('ref-type-phones.employee-phones.store');

        // RefTypePhone Department Phones
        Route::get('/ref-type-phones/{refTypePhone}/department-phones', [
            RefTypePhoneDepartmentPhonesController::class,
            'index',
        ])->name('ref-type-phones.department-phones.index');
        Route::post('/ref-type-phones/{refTypePhone}/department-phones', [
            RefTypePhoneDepartmentPhonesController::class,
            'store',
        ])->name('ref-type-phones.department-phones.store');

        // RefTypePhone Ref Type Phone Translations
        Route::get(
            '/ref-type-phones/{refTypePhone}/ref-type-phone-translations',
            [RefTypePhoneRefTypePhoneTranslationsController::class, 'index']
        )->name('ref-type-phones.ref-type-phone-translations.index');
        Route::post(
            '/ref-type-phones/{refTypePhone}/ref-type-phone-translations',
            [RefTypePhoneRefTypePhoneTranslationsController::class, 'store']
        )->name('ref-type-phones.ref-type-phone-translations.store');

        // RefTypePhone Branch Phones
        Route::get('/ref-type-phones/{refTypePhone}/branch-phones', [
            RefTypePhoneBranchPhonesController::class,
            'index',
        ])->name('ref-type-phones.branch-phones.index');
        Route::post('/ref-type-phones/{refTypePhone}/branch-phones', [
            RefTypePhoneBranchPhonesController::class,
            'store',
        ])->name('ref-type-phones.branch-phones.store');

        Route::apiResource('ref-type-rodstvs', RefTypeRodstvController::class);

        // RefTypeRodstv Ref Type Rodstv Translations
        Route::get(
            '/ref-type-rodstvs/{refTypeRodstv}/ref-type-rodstv-translations',
            [RefTypeRodstvRefTypeRodstvTranslationsController::class, 'index']
        )->name('ref-type-rodstvs.ref-type-rodstv-translations.index');
        Route::post(
            '/ref-type-rodstvs/{refTypeRodstv}/ref-type-rodstv-translations',
            [RefTypeRodstvRefTypeRodstvTranslationsController::class, 'store']
        )->name('ref-type-rodstvs.ref-type-rodstv-translations.store');

        Route::apiResource('ref-user-statuses', RefUserStatusController::class);

        // RefUserStatus Employees
        Route::get('/ref-user-statuses/{refUserStatus}/employees', [
            RefUserStatusEmployeesController::class,
            'index',
        ])->name('ref-user-statuses.employees.index');
        Route::post('/ref-user-statuses/{refUserStatus}/employees', [
            RefUserStatusEmployeesController::class,
            'store',
        ])->name('ref-user-statuses.employees.store');

        // RefUserStatus Ref User Status Translations
        Route::get(
            '/ref-user-statuses/{refUserStatus}/ref-user-status-translations',
            [RefUserStatusRefUserStatusTranslationsController::class, 'index']
        )->name('ref-user-statuses.ref-user-status-translations.index');
        Route::post(
            '/ref-user-statuses/{refUserStatus}/ref-user-status-translations',
            [RefUserStatusRefUserStatusTranslationsController::class, 'store']
        )->name('ref-user-statuses.ref-user-status-translations.store');

        Route::apiResource('ref-vid-holidays', RefVidHolidayController::class);

        // RefVidHoliday Employee Holidays
        Route::get('/ref-vid-holidays/{refVidHoliday}/employee-holidays', [
            RefVidHolidayEmployeeHolidaysController::class,
            'index',
        ])->name('ref-vid-holidays.employee-holidays.index');
        Route::post('/ref-vid-holidays/{refVidHoliday}/employee-holidays', [
            RefVidHolidayEmployeeHolidaysController::class,
            'store',
        ])->name('ref-vid-holidays.employee-holidays.store');

        // RefVidHoliday Ref Vid Holiday Translates
        Route::get(
            '/ref-vid-holidays/{refVidHoliday}/ref-vid-holiday-translations',
            [RefVidHolidayRefVidHolidayTranslationsController::class, 'index']
        )->name('ref-vid-holidays.ref-vid-holiday-translations.index');
        Route::post(
            '/ref-vid-holidays/{refVidHoliday}/ref-vid-holiday-translations',
            [RefVidHolidayRefVidHolidayTranslationsController::class, 'store']
        )->name('ref-vid-holidays.ref-vid-holiday-translations.store');

        Route::apiResource(
            'ref-vid-holiday-types',
            RefVidHolidayTypeController::class
        );

        // RefVidHolidayType Ref Vid Holidays
        Route::get(
            '/ref-vid-holiday-types/{refVidHolidayType}/ref-vid-holidays',
            [RefVidHolidayTypeRefVidHolidaysController::class, 'index']
        )->name('ref-vid-holiday-types.ref-vid-holidays.index');
        Route::post(
            '/ref-vid-holiday-types/{refVidHolidayType}/ref-vid-holidays',
            [RefVidHolidayTypeRefVidHolidaysController::class, 'store']
        )->name('ref-vid-holiday-types.ref-vid-holidays.store');

        // RefVidHolidayType Ref Vid Holiday Type Translations
        Route::get(
            '/ref-vid-holiday-types/{refVidHolidayType}/ref-vid-holiday-type-translations',
            [
                RefVidHolidayTypeRefVidHolidayTypeTranslationsController::class,
                'index',
            ]
        )->name(
            'ref-vid-holiday-types.ref-vid-holiday-type-translations.index'
        );
        Route::post(
            '/ref-vid-holiday-types/{refVidHolidayType}/ref-vid-holiday-type-translations',
            [
                RefVidHolidayTypeRefVidHolidayTypeTranslationsController::class,
                'store',
            ]
        )->name(
            'ref-vid-holiday-types.ref-vid-holiday-type-translations.store'
        );

        Route::apiResource('report-htmls', ReportHtmlController::class);

        // ReportHtml Report Html Others
        Route::get('/report-htmls/{reportHtml}/report-html-others', [
            ReportHtmlReportHtmlOthersController::class,
            'index',
        ])->name('report-htmls.report-html-others.index');
        Route::post('/report-htmls/{reportHtml}/report-html-others', [
            ReportHtmlReportHtmlOthersController::class,
            'store',
        ])->name('report-htmls.report-html-others.store');

        Route::apiResource(
            'report-html-others',
            ReportHtmlOtherController::class
        );

        Route::apiResource('t2-cards', T2CardController::class);

        Route::apiResource('users', UserController::class);
    });
