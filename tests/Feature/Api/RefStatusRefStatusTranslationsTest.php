<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefStatus;
use App\Models\RefStatusTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefStatusRefStatusTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_status_ref_status_translations()
    {
        $refStatus = RefStatus::factory()->create();
        $refStatusTranslations = RefStatusTranslation::factory()
            ->count(2)
            ->create([
                'ref_status_id' => $refStatus->id,
            ]);

        $response = $this->getJson(
            route('api.ref-statuses.ref-status-translations.index', $refStatus)
        );

        $response->assertOk()->assertSee($refStatusTranslations[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_status_ref_status_translations()
    {
        $refStatus = RefStatus::factory()->create();
        $data = RefStatusTranslation::factory()
            ->make([
                'ref_status_id' => $refStatus->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.ref-statuses.ref-status-translations.store', $refStatus),
            $data
        );

        unset($data['ref_status_id']);
        unset($data['locale']);
        unset($data['name']);

        $this->assertDatabaseHas('ref_status_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $refStatusTranslation = RefStatusTranslation::latest('id')->first();

        $this->assertEquals(
            $refStatus->id,
            $refStatusTranslation->ref_status_id
        );
    }
}
