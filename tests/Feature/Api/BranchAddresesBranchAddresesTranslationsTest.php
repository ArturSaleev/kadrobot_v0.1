<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\BranchAddreses;
use App\Models\BranchAddresesTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BranchAddresesBranchAddresesTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_branch_addreses_branch_addreses_translations()
    {
        $branchAddreses = BranchAddreses::factory()->create();
        $branchAddresesTranslations = BranchAddresesTranslation::factory()
            ->count(2)
            ->create([
                'branch_addreses_id' => $branchAddreses->id,
            ]);

        $response = $this->getJson(
            route(
                'api.all-branch-addreses.branch-addreses-translations.index',
                $branchAddreses
            )
        );

        $response->assertOk()->assertSee($branchAddresesTranslations[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_branch_addreses_branch_addreses_translations()
    {
        $branchAddreses = BranchAddreses::factory()->create();
        $data = BranchAddresesTranslation::factory()
            ->make([
                'branch_addreses_id' => $branchAddreses->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.all-branch-addreses.branch-addreses-translations.store',
                $branchAddreses
            ),
            $data
        );

        unset($data['locale']);
        unset($data['name']);
        unset($data['branch_addreses_id']);

        $this->assertDatabaseHas('branch_addreses_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $branchAddresesTranslation = BranchAddresesTranslation::latest(
            'id'
        )->first();

        $this->assertEquals(
            $branchAddreses->id,
            $branchAddresesTranslation->branch_addreses_id
        );
    }
}
