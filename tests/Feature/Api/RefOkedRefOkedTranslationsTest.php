<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefOked;
use App\Models\RefOkedTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefOkedRefOkedTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_oked_ref_oked_translations()
    {
        $refOked = RefOked::factory()->create();
        $refOkedTranslations = RefOkedTranslation::factory()
            ->count(2)
            ->create([
                'ref_oked_id' => $refOked->id,
            ]);

        $response = $this->getJson(
            route('api.ref-okeds.ref-oked-translations.index', $refOked)
        );

        $response->assertOk()->assertSee($refOkedTranslations[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_oked_ref_oked_translations()
    {
        $refOked = RefOked::factory()->create();
        $data = RefOkedTranslation::factory()
            ->make([
                'ref_oked_id' => $refOked->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.ref-okeds.ref-oked-translations.store', $refOked),
            $data
        );

        unset($data['ref_oked_id']);
        unset($data['locale']);
        unset($data['name_oked']);
        unset($data['name']);

        $this->assertDatabaseHas('ref_oked_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $refOkedTranslation = RefOkedTranslation::latest('id')->first();

        $this->assertEquals($refOked->id, $refOkedTranslation->ref_oked_id);
    }
}
