<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\EmployeeInvalide;

use App\Models\Employee;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeInvalideTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_invalides_list()
    {
        $employeeInvalides = EmployeeInvalide::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.employee-invalides.index'));

        $response->assertOk()->assertSee($employeeInvalides[0]->num);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_invalide()
    {
        $data = EmployeeInvalide::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(
            route('api.employee-invalides.store'),
            $data
        );

        $this->assertDatabaseHas('employee_invalides', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_employee_invalide()
    {
        $employeeInvalide = EmployeeInvalide::factory()->create();

        $employee = Employee::factory()->create();

        $data = [
            'num' => $this->faker->text(255),
            'date_add' => $this->faker->date,
            'period_begin' => $this->faker->date,
            'period_end' => $this->faker->date,
            'employee_id' => $employee->id,
        ];

        $response = $this->putJson(
            route('api.employee-invalides.update', $employeeInvalide),
            $data
        );

        $data['id'] = $employeeInvalide->id;

        $this->assertDatabaseHas('employee_invalides', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_invalide()
    {
        $employeeInvalide = EmployeeInvalide::factory()->create();

        $response = $this->deleteJson(
            route('api.employee-invalides.destroy', $employeeInvalide)
        );

        $this->assertSoftDeleted($employeeInvalide);

        $response->assertNoContent();
    }
}
