<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Employee;
use App\Models\RefFamilyState;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefFamilyStateEmployeesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_family_state_employees()
    {
        $refFamilyState = RefFamilyState::factory()->create();
        $employees = Employee::factory()
            ->count(2)
            ->create([
                'ref_family_state_id' => $refFamilyState->id,
            ]);

        $response = $this->getJson(
            route('api.ref-family-states.employees.index', $refFamilyState)
        );

        $response->assertOk()->assertSee($employees[0]->iin);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_family_state_employees()
    {
        $refFamilyState = RefFamilyState::factory()->create();
        $data = Employee::factory()
            ->make([
                'ref_family_state_id' => $refFamilyState->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.ref-family-states.employees.store', $refFamilyState),
            $data
        );

        unset($data['reason_loyoff']);
        unset($data['position_id']);

        $this->assertDatabaseHas('employees', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employee = Employee::latest('id')->first();

        $this->assertEquals(
            $refFamilyState->id,
            $employee->ref_family_state_id
        );
    }
}
