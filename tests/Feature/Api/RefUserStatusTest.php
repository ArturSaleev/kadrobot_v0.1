<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefUserStatus;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefUserStatusTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_user_statuses_list()
    {
        $refUserStatuses = RefUserStatus::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.ref-user-statuses.index'));

        $response->assertOk()->assertSee($refUserStatuses[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_user_status()
    {
        $data = RefUserStatus::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(
            route('api.ref-user-statuses.store'),
            $data
        );

        $this->assertDatabaseHas('ref_user_statuses', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_ref_user_status()
    {
        $refUserStatus = RefUserStatus::factory()->create();

        $data = [];

        $response = $this->putJson(
            route('api.ref-user-statuses.update', $refUserStatus),
            $data
        );

        $data['id'] = $refUserStatus->id;

        $this->assertDatabaseHas('ref_user_statuses', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_user_status()
    {
        $refUserStatus = RefUserStatus::factory()->create();

        $response = $this->deleteJson(
            route('api.ref-user-statuses.destroy', $refUserStatus)
        );

        $this->assertSoftDeleted($refUserStatus);

        $response->assertNoContent();
    }
}
