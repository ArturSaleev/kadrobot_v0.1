<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefUserStatus;
use App\Models\RefUserStatusTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefUserStatusRefUserStatusTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_user_status_ref_user_status_translations()
    {
        $refUserStatus = RefUserStatus::factory()->create();
        $refUserStatusTranslations = RefUserStatusTranslation::factory()
            ->count(2)
            ->create([
                'ref_user_status_id' => $refUserStatus->id,
            ]);

        $response = $this->getJson(
            route(
                'api.ref-user-statuses.ref-user-status-translations.index',
                $refUserStatus
            )
        );

        $response->assertOk()->assertSee($refUserStatusTranslations[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_user_status_ref_user_status_translations()
    {
        $refUserStatus = RefUserStatus::factory()->create();
        $data = RefUserStatusTranslation::factory()
            ->make([
                'ref_user_status_id' => $refUserStatus->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.ref-user-statuses.ref-user-status-translations.store',
                $refUserStatus
            ),
            $data
        );

        unset($data['ref_user_status_id']);
        unset($data['locale']);
        unset($data['name']);

        $this->assertDatabaseHas('ref_user_status_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $refUserStatusTranslation = RefUserStatusTranslation::latest(
            'id'
        )->first();

        $this->assertEquals(
            $refUserStatus->id,
            $refUserStatusTranslation->ref_user_status_id
        );
    }
}
