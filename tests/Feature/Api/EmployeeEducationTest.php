<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\EmployeeEducation;

use App\Models\Employee;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeEducationTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_educations_list()
    {
        $employeeEducations = EmployeeEducation::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.employee-educations.index'));

        $response->assertOk()->assertSee($employeeEducations[0]->institution);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_education()
    {
        $data = EmployeeEducation::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(
            route('api.employee-educations.store'),
            $data
        );

        $this->assertDatabaseHas('employee_educations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_employee_education()
    {
        $employeeEducation = EmployeeEducation::factory()->create();

        $employee = Employee::factory()->create();

        $data = [
            'institution' => $this->faker->text(255),
            'year_begin' => $this->faker->randomNumber(0),
            'year_end' => $this->faker->randomNumber(0),
            'date_begin' => $this->faker->date,
            'date_end' => $this->faker->date,
            'speciality' => $this->faker->text(255),
            'qualification' => $this->faker->text(255),
            'diplom_num' => $this->faker->text(255),
            'diplom_date' => $this->faker->date,
            'employee_id' => $employee->id,
        ];

        $response = $this->putJson(
            route('api.employee-educations.update', $employeeEducation),
            $data
        );

        $data['id'] = $employeeEducation->id;

        $this->assertDatabaseHas('employee_educations', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_education()
    {
        $employeeEducation = EmployeeEducation::factory()->create();

        $response = $this->deleteJson(
            route('api.employee-educations.destroy', $employeeEducation)
        );

        $this->assertSoftDeleted($employeeEducation);

        $response->assertNoContent();
    }
}
