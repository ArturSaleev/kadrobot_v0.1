<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefAction;
use App\Models\RefActionTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefActionRefActionTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_action_ref_action_translations()
    {
        $refAction = RefAction::factory()->create();
        $refActionTranslations = RefActionTranslation::factory()
            ->count(2)
            ->create([
                'ref_action_id' => $refAction->id,
            ]);

        $response = $this->getJson(
            route('api.ref-actions.ref-action-translations.index', $refAction)
        );

        $response->assertOk()->assertSee($refActionTranslations[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_action_ref_action_translations()
    {
        $refAction = RefAction::factory()->create();
        $data = RefActionTranslation::factory()
            ->make([
                'ref_action_id' => $refAction->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.ref-actions.ref-action-translations.store', $refAction),
            $data
        );

        unset($data['ref_action_id']);
        unset($data['locale']);
        unset($data['name']);

        $this->assertDatabaseHas('ref_action_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $refActionTranslation = RefActionTranslation::latest('id')->first();

        $this->assertEquals(
            $refAction->id,
            $refActionTranslation->ref_action_id
        );
    }
}
