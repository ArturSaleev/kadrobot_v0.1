<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefAccountType;
use App\Models\RefAccountTypeTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefAccountTypeRefAccountTypeTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_account_type_ref_account_type_translations()
    {
        $refAccountType = RefAccountType::factory()->create();
        $refAccountTypeTranslations = RefAccountTypeTranslation::factory()
            ->count(2)
            ->create([
                'ref_account_type_id' => $refAccountType->id,
            ]);

        $response = $this->getJson(
            route(
                'api.ref-account-types.ref-account-type-translations.index',
                $refAccountType
            )
        );

        $response->assertOk()->assertSee($refAccountTypeTranslations[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_account_type_ref_account_type_translations()
    {
        $refAccountType = RefAccountType::factory()->create();
        $data = RefAccountTypeTranslation::factory()
            ->make([
                'ref_account_type_id' => $refAccountType->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.ref-account-types.ref-account-type-translations.store',
                $refAccountType
            ),
            $data
        );

        unset($data['ref_account_type_id']);
        unset($data['locale']);
        unset($data['name']);

        $this->assertDatabaseHas('ref_account_type_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $refAccountTypeTranslation = RefAccountTypeTranslation::latest(
            'id'
        )->first();

        $this->assertEquals(
            $refAccountType->id,
            $refAccountTypeTranslation->ref_account_type_id
        );
    }
}
