<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefAddressType;
use App\Models\RefAddressTypeTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefAddressTypeRefAddressTypeTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_address_type_ref_address_type_translations()
    {
        $refAddressType = RefAddressType::factory()->create();
        $refAddressTypeTranslations = RefAddressTypeTranslation::factory()
            ->count(2)
            ->create([
                'ref_address_type_id' => $refAddressType->id,
            ]);

        $response = $this->getJson(
            route(
                'api.ref-address-types.ref-address-type-translations.index',
                $refAddressType
            )
        );

        $response->assertOk()->assertSee($refAddressTypeTranslations[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_address_type_ref_address_type_translations()
    {
        $refAddressType = RefAddressType::factory()->create();
        $data = RefAddressTypeTranslation::factory()
            ->make([
                'ref_address_type_id' => $refAddressType->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.ref-address-types.ref-address-type-translations.store',
                $refAddressType
            ),
            $data
        );

        unset($data['ref_address_type_id']);
        unset($data['locale']);
        unset($data['name']);

        $this->assertDatabaseHas('ref_address_type_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $refAddressTypeTranslation = RefAddressTypeTranslation::latest(
            'id'
        )->first();

        $this->assertEquals(
            $refAddressType->id,
            $refAddressTypeTranslation->ref_address_type_id
        );
    }
}
