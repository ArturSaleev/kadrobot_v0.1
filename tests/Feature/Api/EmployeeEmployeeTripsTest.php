<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Employee;
use App\Models\EmployeeTrip;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeEmployeeTripsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_employee_trips()
    {
        $employee = Employee::factory()->create();
        $employeeTrips = EmployeeTrip::factory()
            ->count(2)
            ->create([
                'employee_id' => $employee->id,
            ]);

        $response = $this->getJson(
            route('api.employees.employee-trips.index', $employee)
        );

        $response->assertOk()->assertSee($employeeTrips[0]->date_begin);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_employee_trips()
    {
        $employee = Employee::factory()->create();
        $data = EmployeeTrip::factory()
            ->make([
                'employee_id' => $employee->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.employees.employee-trips.store', $employee),
            $data
        );

        $this->assertDatabaseHas('employee_trips', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employeeTrip = EmployeeTrip::latest('id')->first();

        $this->assertEquals($employee->id, $employeeTrip->employee_id);
    }
}
