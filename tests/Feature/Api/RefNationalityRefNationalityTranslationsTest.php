<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefNationality;
use App\Models\RefNationalityTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefNationalityRefNationalityTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_nationality_ref_nationality_translations()
    {
        $refNationality = RefNationality::factory()->create();
        $refNationalityTranslations = RefNationalityTranslation::factory()
            ->count(2)
            ->create([
                'ref_nationality_id' => $refNationality->id,
            ]);

        $response = $this->getJson(
            route(
                'api.ref-nationalities.ref-nationality-translations.index',
                $refNationality
            )
        );

        $response->assertOk()->assertSee($refNationalityTranslations[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_nationality_ref_nationality_translations()
    {
        $refNationality = RefNationality::factory()->create();
        $data = RefNationalityTranslation::factory()
            ->make([
                'ref_nationality_id' => $refNationality->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.ref-nationalities.ref-nationality-translations.store',
                $refNationality
            ),
            $data
        );

        unset($data['ref_nationality_id']);
        unset($data['locale']);
        unset($data['name']);

        $this->assertDatabaseHas('ref_nationality_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $refNationalityTranslation = RefNationalityTranslation::latest(
            'id'
        )->first();

        $this->assertEquals(
            $refNationality->id,
            $refNationalityTranslation->ref_nationality_id
        );
    }
}
