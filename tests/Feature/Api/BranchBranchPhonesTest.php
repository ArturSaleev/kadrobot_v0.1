<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Branch;
use App\Models\BranchPhone;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BranchBranchPhonesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_branch_branch_phones()
    {
        $branch = Branch::factory()->create();
        $branchPhones = BranchPhone::factory()
            ->count(2)
            ->create([
                'branch_id' => $branch->id,
            ]);

        $response = $this->getJson(
            route('api.branches.branch-phones.index', $branch)
        );

        $response->assertOk()->assertSee($branchPhones[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_branch_branch_phones()
    {
        $branch = Branch::factory()->create();
        $data = BranchPhone::factory()
            ->make([
                'branch_id' => $branch->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.branches.branch-phones.store', $branch),
            $data
        );

        $this->assertDatabaseHas('branch_phones', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $branchPhone = BranchPhone::latest('id')->first();

        $this->assertEquals($branch->id, $branchPhone->branch_id);
    }
}
