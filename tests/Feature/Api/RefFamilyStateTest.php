<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefFamilyState;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefFamilyStateTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_family_states_list()
    {
        $refFamilyStates = RefFamilyState::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.ref-family-states.index'));

        $response->assertOk()->assertSee($refFamilyStates[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_family_state()
    {
        $data = RefFamilyState::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(
            route('api.ref-family-states.store'),
            $data
        );

        $this->assertDatabaseHas('ref_family_states', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_ref_family_state()
    {
        $refFamilyState = RefFamilyState::factory()->create();

        $data = [];

        $response = $this->putJson(
            route('api.ref-family-states.update', $refFamilyState),
            $data
        );

        $data['id'] = $refFamilyState->id;

        $this->assertDatabaseHas('ref_family_states', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_family_state()
    {
        $refFamilyState = RefFamilyState::factory()->create();

        $response = $this->deleteJson(
            route('api.ref-family-states.destroy', $refFamilyState)
        );

        $this->assertSoftDeleted($refFamilyState);

        $response->assertNoContent();
    }
}
