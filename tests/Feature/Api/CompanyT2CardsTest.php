<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\T2Card;
use App\Models\Company;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CompanyT2CardsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_company_t2_cards()
    {
        $company = Company::factory()->create();
        $t2Cards = T2Card::factory()
            ->count(2)
            ->create([
                'company_id' => $company->id,
            ]);

        $response = $this->getJson(
            route('api.companies.t2-cards.index', $company)
        );

        $response->assertOk()->assertSee($t2Cards[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_company_t2_cards()
    {
        $company = Company::factory()->create();
        $data = T2Card::factory()
            ->make([
                'company_id' => $company->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.companies.t2-cards.store', $company),
            $data
        );

        $this->assertDatabaseHas('t2_cards', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $t2Card = T2Card::latest('id')->first();

        $this->assertEquals($company->id, $t2Card->company_id);
    }
}
