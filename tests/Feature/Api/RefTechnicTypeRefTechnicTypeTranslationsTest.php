<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefTechnicType;
use App\Models\RefTechnicTypeTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefTechnicTypeRefTechnicTypeTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_technic_type_ref_technic_type_translations()
    {
        $refTechnicType = RefTechnicType::factory()->create();
        $refTechnicTypeTranslations = RefTechnicTypeTranslation::factory()
            ->count(2)
            ->create([
                'ref_technic_type_id' => $refTechnicType->id,
            ]);

        $response = $this->getJson(
            route(
                'api.ref-technic-types.ref-technic-type-translations.index',
                $refTechnicType
            )
        );

        $response->assertOk()->assertSee($refTechnicTypeTranslations[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_technic_type_ref_technic_type_translations()
    {
        $refTechnicType = RefTechnicType::factory()->create();
        $data = RefTechnicTypeTranslation::factory()
            ->make([
                'ref_technic_type_id' => $refTechnicType->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.ref-technic-types.ref-technic-type-translations.store',
                $refTechnicType
            ),
            $data
        );

        unset($data['name']);
        unset($data['locale']);
        unset($data['ref_technic_type_id']);

        $this->assertDatabaseHas('ref_technic_type_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $refTechnicTypeTranslation = RefTechnicTypeTranslation::latest(
            'id'
        )->first();

        $this->assertEquals(
            $refTechnicType->id,
            $refTechnicTypeTranslation->ref_technic_type_id
        );
    }
}
