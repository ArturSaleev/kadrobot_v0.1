<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefPersonState;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefPersonStateTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_person_states_list()
    {
        $refPersonStates = RefPersonState::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.ref-person-states.index'));

        $response->assertOk()->assertSee($refPersonStates[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_person_state()
    {
        $data = RefPersonState::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(
            route('api.ref-person-states.store'),
            $data
        );

        $this->assertDatabaseHas('ref_person_states', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_ref_person_state()
    {
        $refPersonState = RefPersonState::factory()->create();

        $data = [];

        $response = $this->putJson(
            route('api.ref-person-states.update', $refPersonState),
            $data
        );

        $data['id'] = $refPersonState->id;

        $this->assertDatabaseHas('ref_person_states', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_person_state()
    {
        $refPersonState = RefPersonState::factory()->create();

        $response = $this->deleteJson(
            route('api.ref-person-states.destroy', $refPersonState)
        );

        $this->assertSoftDeleted($refPersonState);

        $response->assertNoContent();
    }
}
