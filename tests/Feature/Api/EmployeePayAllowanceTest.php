<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\EmployeePayAllowance;

use App\Models\Employee;
use App\Models\RefAllowanceType;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeePayAllowanceTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_pay_allowances_list()
    {
        $employeePayAllowances = EmployeePayAllowance::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.employee-pay-allowances.index'));

        $response->assertOk()->assertSee($employeePayAllowances[0]->date_add);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_pay_allowance()
    {
        $data = EmployeePayAllowance::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(
            route('api.employee-pay-allowances.store'),
            $data
        );

        $this->assertDatabaseHas('employee_pay_allowances', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_employee_pay_allowance()
    {
        $employeePayAllowance = EmployeePayAllowance::factory()->create();

        $employee = Employee::factory()->create();
        $refAllowanceType = RefAllowanceType::factory()->create();

        $data = [
            'date_add' => $this->faker->date,
            'period_begin' => $this->faker->date,
            'period_end' => $this->faker->date,
            'paysum' => $this->faker->randomNumber(2),
            'employee_id' => $employee->id,
            'ref_allowance_type_id' => $refAllowanceType->id,
        ];

        $response = $this->putJson(
            route('api.employee-pay-allowances.update', $employeePayAllowance),
            $data
        );

        $data['id'] = $employeePayAllowance->id;

        $this->assertDatabaseHas('employee_pay_allowances', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_pay_allowance()
    {
        $employeePayAllowance = EmployeePayAllowance::factory()->create();

        $response = $this->deleteJson(
            route('api.employee-pay-allowances.destroy', $employeePayAllowance)
        );

        $this->assertSoftDeleted($employeePayAllowance);

        $response->assertNoContent();
    }
}
