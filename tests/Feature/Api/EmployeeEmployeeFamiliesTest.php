<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Employee;
use App\Models\EmployeeFamily;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeEmployeeFamiliesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_employee_families()
    {
        $employee = Employee::factory()->create();
        $employeeFamilies = EmployeeFamily::factory()
            ->count(2)
            ->create([
                'employee_id' => $employee->id,
            ]);

        $response = $this->getJson(
            route('api.employees.employee-families.index', $employee)
        );

        $response->assertOk()->assertSee($employeeFamilies[0]->lastname);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_employee_families()
    {
        $employee = Employee::factory()->create();
        $data = EmployeeFamily::factory()
            ->make([
                'employee_id' => $employee->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.employees.employee-families.store', $employee),
            $data
        );

        $this->assertDatabaseHas('employee_families', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employeeFamily = EmployeeFamily::latest('id')->first();

        $this->assertEquals($employee->id, $employeeFamily->employee_id);
    }
}
