<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefVidHoliday;
use App\Models\EmployeeHoliday;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefVidHolidayEmployeeHolidaysTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_vid_holiday_employee_holidays()
    {
        $refVidHoliday = RefVidHoliday::factory()->create();
        $employeeHolidays = EmployeeHoliday::factory()
            ->count(2)
            ->create([
                'ref_vid_holiday_id' => $refVidHoliday->id,
            ]);

        $response = $this->getJson(
            route(
                'api.ref-vid-holidays.employee-holidays.index',
                $refVidHoliday
            )
        );

        $response->assertOk()->assertSee($employeeHolidays[0]->date_begin);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_vid_holiday_employee_holidays()
    {
        $refVidHoliday = RefVidHoliday::factory()->create();
        $data = EmployeeHoliday::factory()
            ->make([
                'ref_vid_holiday_id' => $refVidHoliday->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.ref-vid-holidays.employee-holidays.store',
                $refVidHoliday
            ),
            $data
        );

        $this->assertDatabaseHas('employee_holidays', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employeeHoliday = EmployeeHoliday::latest('id')->first();

        $this->assertEquals(
            $refVidHoliday->id,
            $employeeHoliday->ref_vid_holiday_id
        );
    }
}
