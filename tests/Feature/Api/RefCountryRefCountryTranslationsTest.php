<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefCountry;
use App\Models\RefCountryTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefCountryRefCountryTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_country_ref_country_translations()
    {
        $refCountry = RefCountry::factory()->create();
        $refCountryTranslations = RefCountryTranslation::factory()
            ->count(2)
            ->create([
                'ref_country_id' => $refCountry->id,
            ]);

        $response = $this->getJson(
            route(
                'api.ref-countries.ref-country-translations.index',
                $refCountry
            )
        );

        $response->assertOk()->assertSee($refCountryTranslations[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_country_ref_country_translations()
    {
        $refCountry = RefCountry::factory()->create();
        $data = RefCountryTranslation::factory()
            ->make([
                'ref_country_id' => $refCountry->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.ref-countries.ref-country-translations.store',
                $refCountry
            ),
            $data
        );

        unset($data['ref_country_id']);
        unset($data['locale']);
        unset($data['name']);

        $this->assertDatabaseHas('ref_country_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $refCountryTranslation = RefCountryTranslation::latest('id')->first();

        $this->assertEquals(
            $refCountry->id,
            $refCountryTranslation->ref_country_id
        );
    }
}
