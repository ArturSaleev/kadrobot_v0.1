<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Employee;
use App\Models\EmployeeDeclension;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeEmployeeDeclensionsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_employee_declensions()
    {
        $employee = Employee::factory()->create();
        $employeeDeclensions = EmployeeDeclension::factory()
            ->count(2)
            ->create([
                'employee_id' => $employee->id,
            ]);

        $response = $this->getJson(
            route('api.employees.employee-declensions.index', $employee)
        );

        $response->assertOk()->assertSee($employeeDeclensions[0]->locale);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_employee_declensions()
    {
        $employee = Employee::factory()->create();
        $data = EmployeeDeclension::factory()
            ->make([
                'employee_id' => $employee->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.employees.employee-declensions.store', $employee),
            $data
        );

        unset($data['type_description']);

        $this->assertDatabaseHas('employee_declensions', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employeeDeclension = EmployeeDeclension::latest('id')->first();

        $this->assertEquals($employee->id, $employeeDeclension->employee_id);
    }
}
