<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\EmployeeStazh;
use App\Models\EmployeeStazhTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeStazhEmployeeStazhTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_stazh_employee_stazh_translations()
    {
        $employeeStazh = EmployeeStazh::factory()->create();
        $employeeStazhTranslations = EmployeeStazhTranslation::factory()
            ->count(2)
            ->create([
                'employee_stazh_id' => $employeeStazh->id,
            ]);

        $response = $this->getJson(
            route(
                'api.employee-stazhs.employee-stazh-translations.index',
                $employeeStazh
            )
        );

        $response->assertOk()->assertSee($employeeStazhTranslations[0]->locale);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_stazh_employee_stazh_translations()
    {
        $employeeStazh = EmployeeStazh::factory()->create();
        $data = EmployeeStazhTranslation::factory()
            ->make([
                'employee_stazh_id' => $employeeStazh->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.employee-stazhs.employee-stazh-translations.store',
                $employeeStazh
            ),
            $data
        );

        unset($data['employee_stazh_id']);
        unset($data['locale']);
        unset($data['organization']);
        unset($data['dolgnost']);
        unset($data['address']);

        $this->assertDatabaseHas('employee_stazh_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employeeStazhTranslation = EmployeeStazhTranslation::latest(
            'id'
        )->first();

        $this->assertEquals(
            $employeeStazh->id,
            $employeeStazhTranslation->employee_stazh_id
        );
    }
}
