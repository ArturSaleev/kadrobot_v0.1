<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\ReportHtml;
use App\Models\ReportHtmlOther;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReportHtmlReportHtmlOthersTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_report_html_report_html_others()
    {
        $reportHtml = ReportHtml::factory()->create();
        $reportHtmlOthers = ReportHtmlOther::factory()
            ->count(2)
            ->create([
                'report_html_id' => $reportHtml->id,
            ]);

        $response = $this->getJson(
            route('api.report-htmls.report-html-others.index', $reportHtml)
        );

        $response->assertOk()->assertSee($reportHtmlOthers[0]->title);
    }

    /**
     * @test
     */
    public function it_stores_the_report_html_report_html_others()
    {
        $reportHtml = ReportHtml::factory()->create();
        $data = ReportHtmlOther::factory()
            ->make([
                'report_html_id' => $reportHtml->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.report-htmls.report-html-others.store', $reportHtml),
            $data
        );

        $this->assertDatabaseHas('report_html_others', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $reportHtmlOther = ReportHtmlOther::latest('id')->first();

        $this->assertEquals($reportHtml->id, $reportHtmlOther->report_html_id);
    }
}
