<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\ReportHtml;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReportHtmlTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_report_htmls_list()
    {
        $reportHtmls = ReportHtml::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.report-htmls.index'));

        $response->assertOk()->assertSee($reportHtmls[0]->html_text);
    }

    /**
     * @test
     */
    public function it_stores_the_report_html()
    {
        $data = ReportHtml::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.report-htmls.store'), $data);

        $this->assertDatabaseHas('report_htmls', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_report_html()
    {
        $reportHtml = ReportHtml::factory()->create();

        $data = [
            'html_text' => $this->faker->text,
            'sql_text' => $this->faker->text,
            'title_text' => $this->faker->text,
            'date_edd' => $this->faker->dateTime,
        ];

        $response = $this->putJson(
            route('api.report-htmls.update', $reportHtml),
            $data
        );

        $data['id'] = $reportHtml->id;

        $this->assertDatabaseHas('report_htmls', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_report_html()
    {
        $reportHtml = ReportHtml::factory()->create();

        $response = $this->deleteJson(
            route('api.report-htmls.destroy', $reportHtml)
        );

        $this->assertSoftDeleted($reportHtml);

        $response->assertNoContent();
    }
}
