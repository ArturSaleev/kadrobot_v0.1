<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Position;
use App\Models\PositionTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PositionPositionTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_position_position_translations()
    {
        $position = Position::factory()->create();
        $positionTranslations = PositionTranslation::factory()
            ->count(2)
            ->create([
                'position_id' => $position->id,
            ]);

        $response = $this->getJson(
            route('api.positions.position-translations.index', $position)
        );

        $response->assertOk()->assertSee($positionTranslations[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_position_position_translations()
    {
        $position = Position::factory()->create();
        $data = PositionTranslation::factory()
            ->make([
                'position_id' => $position->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.positions.position-translations.store', $position),
            $data
        );

        unset($data['position_id']);
        unset($data['locale']);
        unset($data['name']);

        $this->assertDatabaseHas('position_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $positionTranslation = PositionTranslation::latest('id')->first();

        $this->assertEquals($position->id, $positionTranslation->position_id);
    }
}
