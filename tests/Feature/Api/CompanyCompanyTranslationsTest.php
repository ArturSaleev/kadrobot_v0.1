<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Company;
use App\Models\CompanyTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CompanyCompanyTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_company_company_translations()
    {
        $company = Company::factory()->create();
        $companyTranslations = CompanyTranslation::factory()
            ->count(2)
            ->create([
                'company_id' => $company->id,
            ]);

        $response = $this->getJson(
            route('api.companies.company-translations.index', $company)
        );

        $response->assertOk()->assertSee($companyTranslations[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_company_company_translations()
    {
        $company = Company::factory()->create();
        $data = CompanyTranslation::factory()
            ->make([
                'company_id' => $company->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.companies.company-translations.store', $company),
            $data
        );

        unset($data['company_id']);
        unset($data['locale']);
        unset($data['name']);

        $this->assertDatabaseHas('company_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $companyTranslation = CompanyTranslation::latest('id')->first();

        $this->assertEquals($company->id, $companyTranslation->company_id);
    }
}
