<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefSex;
use App\Models\Employee;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefSexEmployeesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_sex_employees()
    {
        $refSex = RefSex::factory()->create();
        $employees = Employee::factory()
            ->count(2)
            ->create([
                'ref_sex_id' => $refSex->id,
            ]);

        $response = $this->getJson(
            route('api.ref-sexes.employees.index', $refSex)
        );

        $response->assertOk()->assertSee($employees[0]->iin);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_sex_employees()
    {
        $refSex = RefSex::factory()->create();
        $data = Employee::factory()
            ->make([
                'ref_sex_id' => $refSex->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.ref-sexes.employees.store', $refSex),
            $data
        );

        unset($data['reason_loyoff']);
        unset($data['position_id']);

        $this->assertDatabaseHas('employees', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employee = Employee::latest('id')->first();

        $this->assertEquals($refSex->id, $employee->ref_sex_id);
    }
}
