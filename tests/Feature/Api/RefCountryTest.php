<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefCountry;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefCountryTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_countries_list()
    {
        $refCountries = RefCountry::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.ref-countries.index'));

        $response->assertOk()->assertSee($refCountries[0]->code);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_country()
    {
        $data = RefCountry::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.ref-countries.store'), $data);

        $this->assertDatabaseHas('ref_countries', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_ref_country()
    {
        $refCountry = RefCountry::factory()->create();

        $data = [
            'code' => $this->faker->unique->text(255),
        ];

        $response = $this->putJson(
            route('api.ref-countries.update', $refCountry),
            $data
        );

        $data['id'] = $refCountry->id;

        $this->assertDatabaseHas('ref_countries', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_country()
    {
        $refCountry = RefCountry::factory()->create();

        $response = $this->deleteJson(
            route('api.ref-countries.destroy', $refCountry)
        );

        $this->assertSoftDeleted($refCountry);

        $response->assertNoContent();
    }
}
