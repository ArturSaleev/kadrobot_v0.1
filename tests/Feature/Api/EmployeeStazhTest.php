<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\EmployeeStazh;

use App\Models\Employee;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeStazhTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_stazhs_list()
    {
        $employeeStazhs = EmployeeStazh::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.employee-stazhs.index'));

        $response->assertOk()->assertSee($employeeStazhs[0]->date_begin);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_stazh()
    {
        $data = EmployeeStazh::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.employee-stazhs.store'), $data);

        $this->assertDatabaseHas('employee_stazhs', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_employee_stazh()
    {
        $employeeStazh = EmployeeStazh::factory()->create();

        $employee = Employee::factory()->create();

        $data = [
            'date_begin' => $this->faker->date,
            'date_end' => $this->faker->date,
            'cnt_mes' => $this->faker->randomNumber(0),
            'employee_id' => $employee->id,
        ];

        $response = $this->putJson(
            route('api.employee-stazhs.update', $employeeStazh),
            $data
        );

        $data['id'] = $employeeStazh->id;

        $this->assertDatabaseHas('employee_stazhs', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_stazh()
    {
        $employeeStazh = EmployeeStazh::factory()->create();

        $response = $this->deleteJson(
            route('api.employee-stazhs.destroy', $employeeStazh)
        );

        $this->assertSoftDeleted($employeeStazh);

        $response->assertNoContent();
    }
}
