<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefSex;
use App\Models\RefSexTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefSexRefSexTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_sex_ref_sex_translations()
    {
        $refSex = RefSex::factory()->create();
        $refSexTranslations = RefSexTranslation::factory()
            ->count(2)
            ->create([
                'ref_sex_id' => $refSex->id,
            ]);

        $response = $this->getJson(
            route('api.ref-sexes.ref-sex-translations.index', $refSex)
        );

        $response->assertOk()->assertSee($refSexTranslations[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_sex_ref_sex_translations()
    {
        $refSex = RefSex::factory()->create();
        $data = RefSexTranslation::factory()
            ->make([
                'ref_sex_id' => $refSex->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.ref-sexes.ref-sex-translations.store', $refSex),
            $data
        );

        unset($data['ref_sex_id']);
        unset($data['locale']);
        unset($data['name']);

        $this->assertDatabaseHas('ref_sex_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $refSexTranslation = RefSexTranslation::latest('id')->first();

        $this->assertEquals($refSex->id, $refSexTranslation->ref_sex_id);
    }
}
