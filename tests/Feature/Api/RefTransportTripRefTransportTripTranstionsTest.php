<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefTransportTrip;
use App\Models\RefTransportTripTranstion;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefTransportTripRefTransportTripTranstionsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_transport_trip_ref_transport_trip_transtions()
    {
        $refTransportTrip = RefTransportTrip::factory()->create();
        $refTransportTripTranstions = RefTransportTripTranstion::factory()
            ->count(2)
            ->create([
                'ref_transport_trip_id' => $refTransportTrip->id,
            ]);

        $response = $this->getJson(
            route(
                'api.ref-transport-trips.ref-transport-trip-transtions.index',
                $refTransportTrip
            )
        );

        $response->assertOk()->assertSee($refTransportTripTranstions[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_transport_trip_ref_transport_trip_transtions()
    {
        $refTransportTrip = RefTransportTrip::factory()->create();
        $data = RefTransportTripTranstion::factory()
            ->make([
                'ref_transport_trip_id' => $refTransportTrip->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.ref-transport-trips.ref-transport-trip-transtions.store',
                $refTransportTrip
            ),
            $data
        );

        unset($data['ref_transport_trip_id']);
        unset($data['locale']);
        unset($data['name']);

        $this->assertDatabaseHas('ref_transport_trip_transtions', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $refTransportTripTranstion = RefTransportTripTranstion::latest(
            'id'
        )->first();

        $this->assertEquals(
            $refTransportTrip->id,
            $refTransportTripTranstion->ref_transport_trip_id
        );
    }
}
