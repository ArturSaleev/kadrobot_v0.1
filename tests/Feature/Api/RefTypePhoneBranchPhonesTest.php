<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\BranchPhone;
use App\Models\RefTypePhone;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefTypePhoneBranchPhonesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_type_phone_branch_phones()
    {
        $refTypePhone = RefTypePhone::factory()->create();
        $branchPhones = BranchPhone::factory()
            ->count(2)
            ->create([
                'ref_type_phone_id' => $refTypePhone->id,
            ]);

        $response = $this->getJson(
            route('api.ref-type-phones.branch-phones.index', $refTypePhone)
        );

        $response->assertOk()->assertSee($branchPhones[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_type_phone_branch_phones()
    {
        $refTypePhone = RefTypePhone::factory()->create();
        $data = BranchPhone::factory()
            ->make([
                'ref_type_phone_id' => $refTypePhone->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.ref-type-phones.branch-phones.store', $refTypePhone),
            $data
        );

        $this->assertDatabaseHas('branch_phones', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $branchPhone = BranchPhone::latest('id')->first();

        $this->assertEquals($refTypePhone->id, $branchPhone->ref_type_phone_id);
    }
}
