<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Employee;
use App\Models\RefNationality;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefNationalityEmployeesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_nationality_employees()
    {
        $refNationality = RefNationality::factory()->create();
        $employees = Employee::factory()
            ->count(2)
            ->create([
                'ref_nationality_id' => $refNationality->id,
            ]);

        $response = $this->getJson(
            route('api.ref-nationalities.employees.index', $refNationality)
        );

        $response->assertOk()->assertSee($employees[0]->iin);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_nationality_employees()
    {
        $refNationality = RefNationality::factory()->create();
        $data = Employee::factory()
            ->make([
                'ref_nationality_id' => $refNationality->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.ref-nationalities.employees.store', $refNationality),
            $data
        );

        unset($data['reason_loyoff']);
        unset($data['position_id']);

        $this->assertDatabaseHas('employees', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employee = Employee::latest('id')->first();

        $this->assertEquals($refNationality->id, $employee->ref_nationality_id);
    }
}
