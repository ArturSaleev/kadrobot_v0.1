<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Employee;
use App\Models\RefAccountType;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefAccountTypeEmployeesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_account_type_employees()
    {
        $refAccountType = RefAccountType::factory()->create();
        $employees = Employee::factory()
            ->count(2)
            ->create([
                'ref_account_type_id' => $refAccountType->id,
            ]);

        $response = $this->getJson(
            route('api.ref-account-types.employees.index', $refAccountType)
        );

        $response->assertOk()->assertSee($employees[0]->iin);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_account_type_employees()
    {
        $refAccountType = RefAccountType::factory()->create();
        $data = Employee::factory()
            ->make([
                'ref_account_type_id' => $refAccountType->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.ref-account-types.employees.store', $refAccountType),
            $data
        );

        unset($data['reason_loyoff']);
        unset($data['position_id']);

        $this->assertDatabaseHas('employees', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employee = Employee::latest('id')->first();

        $this->assertEquals(
            $refAccountType->id,
            $employee->ref_account_type_id
        );
    }
}
