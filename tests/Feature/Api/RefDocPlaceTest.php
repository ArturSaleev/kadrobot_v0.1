<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefDocPlace;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefDocPlaceTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_doc_places_list()
    {
        $refDocPlaces = RefDocPlace::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.ref-doc-places.index'));

        $response->assertOk()->assertSee($refDocPlaces[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_doc_place()
    {
        $data = RefDocPlace::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.ref-doc-places.store'), $data);

        $this->assertDatabaseHas('ref_doc_places', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_ref_doc_place()
    {
        $refDocPlace = RefDocPlace::factory()->create();

        $data = [];

        $response = $this->putJson(
            route('api.ref-doc-places.update', $refDocPlace),
            $data
        );

        $data['id'] = $refDocPlace->id;

        $this->assertDatabaseHas('ref_doc_places', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_doc_place()
    {
        $refDocPlace = RefDocPlace::factory()->create();

        $response = $this->deleteJson(
            route('api.ref-doc-places.destroy', $refDocPlace)
        );

        $this->assertSoftDeleted($refDocPlace);

        $response->assertNoContent();
    }
}
