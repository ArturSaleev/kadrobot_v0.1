<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\T2Card;
use App\Models\Position;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PositionT2CardsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_position_t2_cards()
    {
        $position = Position::factory()->create();
        $t2Cards = T2Card::factory()
            ->count(2)
            ->create([
                'ref_position_id' => $position->id,
            ]);

        $response = $this->getJson(
            route('api.positions.t2-cards.index', $position)
        );

        $response->assertOk()->assertSee($t2Cards[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_position_t2_cards()
    {
        $position = Position::factory()->create();
        $data = T2Card::factory()
            ->make([
                'ref_position_id' => $position->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.positions.t2-cards.store', $position),
            $data
        );

        $this->assertDatabaseHas('t2_cards', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $t2Card = T2Card::latest('id')->first();

        $this->assertEquals($position->id, $t2Card->ref_position_id);
    }
}
