<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefAccountType;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefAccountTypeTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_account_types_list()
    {
        $refAccountTypes = RefAccountType::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.ref-account-types.index'));

        $response->assertOk()->assertSee($refAccountTypes[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_account_type()
    {
        $data = RefAccountType::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(
            route('api.ref-account-types.store'),
            $data
        );

        $this->assertDatabaseHas('ref_account_types', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_ref_account_type()
    {
        $refAccountType = RefAccountType::factory()->create();

        $data = [];

        $response = $this->putJson(
            route('api.ref-account-types.update', $refAccountType),
            $data
        );

        $data['id'] = $refAccountType->id;

        $this->assertDatabaseHas('ref_account_types', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_account_type()
    {
        $refAccountType = RefAccountType::factory()->create();

        $response = $this->deleteJson(
            route('api.ref-account-types.destroy', $refAccountType)
        );

        $this->assertSoftDeleted($refAccountType);

        $response->assertNoContent();
    }
}
