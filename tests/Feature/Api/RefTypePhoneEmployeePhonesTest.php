<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefTypePhone;
use App\Models\EmployeePhone;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefTypePhoneEmployeePhonesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_type_phone_employee_phones()
    {
        $refTypePhone = RefTypePhone::factory()->create();
        $employeePhones = EmployeePhone::factory()
            ->count(2)
            ->create([
                'ref_type_phone_id' => $refTypePhone->id,
            ]);

        $response = $this->getJson(
            route('api.ref-type-phones.employee-phones.index', $refTypePhone)
        );

        $response->assertOk()->assertSee($employeePhones[0]->phone);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_type_phone_employee_phones()
    {
        $refTypePhone = RefTypePhone::factory()->create();
        $data = EmployeePhone::factory()
            ->make([
                'ref_type_phone_id' => $refTypePhone->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.ref-type-phones.employee-phones.store', $refTypePhone),
            $data
        );

        $this->assertDatabaseHas('employee_phones', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employeePhone = EmployeePhone::latest('id')->first();

        $this->assertEquals(
            $refTypePhone->id,
            $employeePhone->ref_type_phone_id
        );
    }
}
