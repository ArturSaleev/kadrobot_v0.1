<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefFamilyState;
use App\Models\RefFamilyStateTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefFamilyStateRefFamilyStateTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_family_state_ref_family_state_translations()
    {
        $refFamilyState = RefFamilyState::factory()->create();
        $refFamilyStateTranslations = RefFamilyStateTranslation::factory()
            ->count(2)
            ->create([
                'ref_family_state_id' => $refFamilyState->id,
            ]);

        $response = $this->getJson(
            route(
                'api.ref-family-states.ref-family-state-translations.index',
                $refFamilyState
            )
        );

        $response->assertOk()->assertSee($refFamilyStateTranslations[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_family_state_ref_family_state_translations()
    {
        $refFamilyState = RefFamilyState::factory()->create();
        $data = RefFamilyStateTranslation::factory()
            ->make([
                'ref_family_state_id' => $refFamilyState->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.ref-family-states.ref-family-state-translations.store',
                $refFamilyState
            ),
            $data
        );

        unset($data['ref_family_state_id']);
        unset($data['locale']);
        unset($data['name']);

        $this->assertDatabaseHas('ref_family_state_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $refFamilyStateTranslation = RefFamilyStateTranslation::latest(
            'id'
        )->first();

        $this->assertEquals(
            $refFamilyState->id,
            $refFamilyStateTranslation->ref_family_state_id
        );
    }
}
