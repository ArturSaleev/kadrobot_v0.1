<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefSex;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefSexTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_sexes_list()
    {
        $refSexes = RefSex::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.ref-sexes.index'));

        $response->assertOk()->assertSee($refSexes[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_sex()
    {
        $data = RefSex::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.ref-sexes.store'), $data);

        $this->assertDatabaseHas('ref_sexes', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_ref_sex()
    {
        $refSex = RefSex::factory()->create();

        $data = [];

        $response = $this->putJson(
            route('api.ref-sexes.update', $refSex),
            $data
        );

        $data['id'] = $refSex->id;

        $this->assertDatabaseHas('ref_sexes', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_sex()
    {
        $refSex = RefSex::factory()->create();

        $response = $this->deleteJson(route('api.ref-sexes.destroy', $refSex));

        $this->assertSoftDeleted($refSex);

        $response->assertNoContent();
    }
}
