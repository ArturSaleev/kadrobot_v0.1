<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefTypeRodstv;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefTypeRodstvTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_type_rodstvs_list()
    {
        $refTypeRodstvs = RefTypeRodstv::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.ref-type-rodstvs.index'));

        $response->assertOk()->assertSee($refTypeRodstvs[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_type_rodstv()
    {
        $data = RefTypeRodstv::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.ref-type-rodstvs.store'), $data);

        $this->assertDatabaseHas('ref_type_rodstvs', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_ref_type_rodstv()
    {
        $refTypeRodstv = RefTypeRodstv::factory()->create();

        $data = [];

        $response = $this->putJson(
            route('api.ref-type-rodstvs.update', $refTypeRodstv),
            $data
        );

        $data['id'] = $refTypeRodstv->id;

        $this->assertDatabaseHas('ref_type_rodstvs', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_type_rodstv()
    {
        $refTypeRodstv = RefTypeRodstv::factory()->create();

        $response = $this->deleteJson(
            route('api.ref-type-rodstvs.destroy', $refTypeRodstv)
        );

        $this->assertSoftDeleted($refTypeRodstv);

        $response->assertNoContent();
    }
}
