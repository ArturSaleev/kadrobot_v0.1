<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefAddressType;
use App\Models\EmployeeAddress;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefAddressTypeEmployeeAddressesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_address_type_employee_addresses()
    {
        $refAddressType = RefAddressType::factory()->create();
        $employeeAddresses = EmployeeAddress::factory()
            ->count(2)
            ->create([
                'ref_address_type_id' => $refAddressType->id,
            ]);

        $response = $this->getJson(
            route(
                'api.ref-address-types.employee-addresses.index',
                $refAddressType
            )
        );

        $response->assertOk()->assertSee($employeeAddresses[0]->locale);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_address_type_employee_addresses()
    {
        $refAddressType = RefAddressType::factory()->create();
        $data = EmployeeAddress::factory()
            ->make([
                'ref_address_type_id' => $refAddressType->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.ref-address-types.employee-addresses.store',
                $refAddressType
            ),
            $data
        );

        $this->assertDatabaseHas('employee_addresses', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employeeAddress = EmployeeAddress::latest('id')->first();

        $this->assertEquals(
            $refAddressType->id,
            $employeeAddress->ref_address_type_id
        );
    }
}
