<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Department;
use App\Models\DepartmentPhone;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DepartmentDepartmentPhonesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_department_department_phones()
    {
        $department = Department::factory()->create();
        $departmentPhones = DepartmentPhone::factory()
            ->count(2)
            ->create([
                'department_id' => $department->id,
            ]);

        $response = $this->getJson(
            route('api.departments.department-phones.index', $department)
        );

        $response->assertOk()->assertSee($departmentPhones[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_department_department_phones()
    {
        $department = Department::factory()->create();
        $data = DepartmentPhone::factory()
            ->make([
                'department_id' => $department->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.departments.department-phones.store', $department),
            $data
        );

        $this->assertDatabaseHas('department_phones', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $departmentPhone = DepartmentPhone::latest('id')->first();

        $this->assertEquals($department->id, $departmentPhone->department_id);
    }
}
