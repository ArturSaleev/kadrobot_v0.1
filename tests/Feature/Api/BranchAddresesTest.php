<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\BranchAddreses;

use App\Models\Branch;
use App\Models\RefAddressType;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BranchAddresesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_all_branch_addreses_list()
    {
        $allBranchAddreses = BranchAddreses::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.all-branch-addreses.index'));

        $response->assertOk()->assertSee($allBranchAddreses[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_branch_addreses()
    {
        $data = BranchAddreses::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(
            route('api.all-branch-addreses.store'),
            $data
        );

        $this->assertDatabaseHas('branch_addreses', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_branch_addreses()
    {
        $branchAddreses = BranchAddreses::factory()->create();

        $refAddressType = RefAddressType::factory()->create();
        $branch = Branch::factory()->create();

        $data = [
            'ref_address_type_id' => $refAddressType->id,
            'branch_id' => $branch->id,
        ];

        $response = $this->putJson(
            route('api.all-branch-addreses.update', $branchAddreses),
            $data
        );

        $data['id'] = $branchAddreses->id;

        $this->assertDatabaseHas('branch_addreses', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_branch_addreses()
    {
        $branchAddreses = BranchAddreses::factory()->create();

        $response = $this->deleteJson(
            route('api.all-branch-addreses.destroy', $branchAddreses)
        );

        $this->assertModelMissing($branchAddreses);

        $response->assertNoContent();
    }
}
