<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefTypePhone;
use App\Models\RefTypePhoneTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefTypePhoneRefTypePhoneTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_type_phone_ref_type_phone_translations()
    {
        $refTypePhone = RefTypePhone::factory()->create();
        $refTypePhoneTranslations = RefTypePhoneTranslation::factory()
            ->count(2)
            ->create([
                'ref_type_phone_id' => $refTypePhone->id,
            ]);

        $response = $this->getJson(
            route(
                'api.ref-type-phones.ref-type-phone-translations.index',
                $refTypePhone
            )
        );

        $response->assertOk()->assertSee($refTypePhoneTranslations[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_type_phone_ref_type_phone_translations()
    {
        $refTypePhone = RefTypePhone::factory()->create();
        $data = RefTypePhoneTranslation::factory()
            ->make([
                'ref_type_phone_id' => $refTypePhone->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.ref-type-phones.ref-type-phone-translations.store',
                $refTypePhone
            ),
            $data
        );

        unset($data['ref_type_phone_id']);
        unset($data['locale']);
        unset($data['name']);

        $this->assertDatabaseHas('ref_type_phone_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $refTypePhoneTranslation = RefTypePhoneTranslation::latest(
            'id'
        )->first();

        $this->assertEquals(
            $refTypePhone->id,
            $refTypePhoneTranslation->ref_type_phone_id
        );
    }
}
