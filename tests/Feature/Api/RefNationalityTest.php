<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefNationality;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefNationalityTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_nationalities_list()
    {
        $refNationalities = RefNationality::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.ref-nationalities.index'));

        $response->assertOk()->assertSee($refNationalities[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_nationality()
    {
        $data = RefNationality::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(
            route('api.ref-nationalities.store'),
            $data
        );

        $this->assertDatabaseHas('ref_nationalities', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_ref_nationality()
    {
        $refNationality = RefNationality::factory()->create();

        $data = [];

        $response = $this->putJson(
            route('api.ref-nationalities.update', $refNationality),
            $data
        );

        $data['id'] = $refNationality->id;

        $this->assertDatabaseHas('ref_nationalities', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_nationality()
    {
        $refNationality = RefNationality::factory()->create();

        $response = $this->deleteJson(
            route('api.ref-nationalities.destroy', $refNationality)
        );

        $this->assertSoftDeleted($refNationality);

        $response->assertNoContent();
    }
}
