<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Curator;

use App\Models\Employee;
use App\Models\Department;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CuratorTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_curators_list()
    {
        $curators = Curator::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.curators.index'));

        $response->assertOk()->assertSee($curators[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_curator()
    {
        $data = Curator::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.curators.store'), $data);

        $this->assertDatabaseHas('curators', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_curator()
    {
        $curator = Curator::factory()->create();

        $employee = Employee::factory()->create();
        $department = Department::factory()->create();

        $data = [
            'employee_id' => $employee->id,
            'department_id' => $department->id,
        ];

        $response = $this->putJson(
            route('api.curators.update', $curator),
            $data
        );

        $data['id'] = $curator->id;

        $this->assertDatabaseHas('curators', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_curator()
    {
        $curator = Curator::factory()->create();

        $response = $this->deleteJson(route('api.curators.destroy', $curator));

        $this->assertSoftDeleted($curator);

        $response->assertNoContent();
    }
}
