<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefDocPlace;
use App\Models\EmployeeDocument;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefDocPlaceEmployeeDocumentsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_doc_place_employee_documents()
    {
        $refDocPlace = RefDocPlace::factory()->create();
        $employeeDocuments = EmployeeDocument::factory()
            ->count(2)
            ->create([
                'ref_doc_place_id' => $refDocPlace->id,
            ]);

        $response = $this->getJson(
            route('api.ref-doc-places.employee-documents.index', $refDocPlace)
        );

        $response->assertOk()->assertSee($employeeDocuments[0]->doc_seria);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_doc_place_employee_documents()
    {
        $refDocPlace = RefDocPlace::factory()->create();
        $data = EmployeeDocument::factory()
            ->make([
                'ref_doc_place_id' => $refDocPlace->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.ref-doc-places.employee-documents.store', $refDocPlace),
            $data
        );

        $this->assertDatabaseHas('employee_documents', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employeeDocument = EmployeeDocument::latest('id')->first();

        $this->assertEquals(
            $refDocPlace->id,
            $employeeDocument->ref_doc_place_id
        );
    }
}
