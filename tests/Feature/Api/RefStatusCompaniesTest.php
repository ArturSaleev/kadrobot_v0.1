<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Company;
use App\Models\RefStatus;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefStatusCompaniesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_status_companies()
    {
        $refStatus = RefStatus::factory()->create();
        $companies = Company::factory()
            ->count(2)
            ->create([
                'ref_status_id' => $refStatus->id,
            ]);

        $response = $this->getJson(
            route('api.ref-statuses.companies.index', $refStatus)
        );

        $response->assertOk()->assertSee($companies[0]->bin);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_status_companies()
    {
        $refStatus = RefStatus::factory()->create();
        $data = Company::factory()
            ->make([
                'ref_status_id' => $refStatus->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.ref-statuses.companies.store', $refStatus),
            $data
        );

        unset($data['oked']);

        $this->assertDatabaseHas('companies', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $company = Company::latest('id')->first();

        $this->assertEquals($refStatus->id, $company->ref_status_id);
    }
}
