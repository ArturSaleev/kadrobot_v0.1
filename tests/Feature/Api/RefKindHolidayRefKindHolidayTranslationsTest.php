<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefKindHoliday;
use App\Models\RefKindHolidayTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefKindHolidayRefKindHolidayTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_kind_holiday_ref_kind_holiday_translations()
    {
        $refKindHoliday = RefKindHoliday::factory()->create();
        $refKindHolidayTranslations = RefKindHolidayTranslation::factory()
            ->count(2)
            ->create([
                'ref_kind_holiday_id' => $refKindHoliday->id,
            ]);

        $response = $this->getJson(
            route(
                'api.ref-kind-holidays.ref-kind-holiday-translations.index',
                $refKindHoliday
            )
        );

        $response->assertOk()->assertSee($refKindHolidayTranslations[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_kind_holiday_ref_kind_holiday_translations()
    {
        $refKindHoliday = RefKindHoliday::factory()->create();
        $data = RefKindHolidayTranslation::factory()
            ->make([
                'ref_kind_holiday_id' => $refKindHoliday->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.ref-kind-holidays.ref-kind-holiday-translations.store',
                $refKindHoliday
            ),
            $data
        );

        unset($data['ref_kind_holiday_id']);
        unset($data['locale']);
        unset($data['name']);

        $this->assertDatabaseHas('ref_kind_holiday_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $refKindHolidayTranslation = RefKindHolidayTranslation::latest(
            'id'
        )->first();

        $this->assertEquals(
            $refKindHoliday->id,
            $refKindHolidayTranslation->ref_kind_holiday_id
        );
    }
}
