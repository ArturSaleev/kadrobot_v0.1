<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Employee;
use App\Models\EmployeeStazh;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeEmployeeStazhsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_employee_stazhs()
    {
        $employee = Employee::factory()->create();
        $employeeStazhs = EmployeeStazh::factory()
            ->count(2)
            ->create([
                'employee_id' => $employee->id,
            ]);

        $response = $this->getJson(
            route('api.employees.employee-stazhs.index', $employee)
        );

        $response->assertOk()->assertSee($employeeStazhs[0]->date_begin);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_employee_stazhs()
    {
        $employee = Employee::factory()->create();
        $data = EmployeeStazh::factory()
            ->make([
                'employee_id' => $employee->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.employees.employee-stazhs.store', $employee),
            $data
        );

        $this->assertDatabaseHas('employee_stazhs', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employeeStazh = EmployeeStazh::latest('id')->first();

        $this->assertEquals($employee->id, $employeeStazh->employee_id);
    }
}
