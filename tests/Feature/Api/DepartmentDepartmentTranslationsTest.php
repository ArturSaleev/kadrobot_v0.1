<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Department;
use App\Models\DepartmentTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DepartmentDepartmentTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_department_department_translations()
    {
        $department = Department::factory()->create();
        $departmentTranslations = DepartmentTranslation::factory()
            ->count(2)
            ->create([
                'department_id' => $department->id,
            ]);

        $response = $this->getJson(
            route('api.departments.department-translations.index', $department)
        );

        $response->assertOk()->assertSee($departmentTranslations[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_department_department_translations()
    {
        $department = Department::factory()->create();
        $data = DepartmentTranslation::factory()
            ->make([
                'department_id' => $department->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.departments.department-translations.store', $department),
            $data
        );

        unset($data['department_id']);
        unset($data['locale']);
        unset($data['name']);

        $this->assertDatabaseHas('department_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $departmentTranslation = DepartmentTranslation::latest('id')->first();

        $this->assertEquals(
            $department->id,
            $departmentTranslation->department_id
        );
    }
}
