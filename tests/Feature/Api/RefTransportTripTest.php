<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefTransportTrip;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefTransportTripTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_transport_trips_list()
    {
        $refTransportTrips = RefTransportTrip::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.ref-transport-trips.index'));

        $response->assertOk()->assertSee($refTransportTrips[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_transport_trip()
    {
        $data = RefTransportTrip::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(
            route('api.ref-transport-trips.store'),
            $data
        );

        $this->assertDatabaseHas('ref_transport_trips', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_ref_transport_trip()
    {
        $refTransportTrip = RefTransportTrip::factory()->create();

        $data = [];

        $response = $this->putJson(
            route('api.ref-transport-trips.update', $refTransportTrip),
            $data
        );

        $data['id'] = $refTransportTrip->id;

        $this->assertDatabaseHas('ref_transport_trips', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_transport_trip()
    {
        $refTransportTrip = RefTransportTrip::factory()->create();

        $response = $this->deleteJson(
            route('api.ref-transport-trips.destroy', $refTransportTrip)
        );

        $this->assertSoftDeleted($refTransportTrip);

        $response->assertNoContent();
    }
}
