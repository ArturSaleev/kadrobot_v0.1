<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Branch;
use App\Models\T2Card;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BranchT2CardsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_branch_t2_cards()
    {
        $branch = Branch::factory()->create();
        $t2Cards = T2Card::factory()
            ->count(2)
            ->create([
                'ref_branch_id' => $branch->id,
            ]);

        $response = $this->getJson(
            route('api.branches.t2-cards.index', $branch)
        );

        $response->assertOk()->assertSee($t2Cards[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_branch_t2_cards()
    {
        $branch = Branch::factory()->create();
        $data = T2Card::factory()
            ->make([
                'ref_branch_id' => $branch->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.branches.t2-cards.store', $branch),
            $data
        );

        $this->assertDatabaseHas('t2_cards', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $t2Card = T2Card::latest('id')->first();

        $this->assertEquals($branch->id, $t2Card->ref_branch_id);
    }
}
