<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\EmployeeDocument;

use App\Models\Employee;
use App\Models\RefDocType;
use App\Models\RefDocPlace;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeDocumentTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_documents_list()
    {
        $employeeDocuments = EmployeeDocument::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.employee-documents.index'));

        $response->assertOk()->assertSee($employeeDocuments[0]->doc_seria);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_document()
    {
        $data = EmployeeDocument::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(
            route('api.employee-documents.store'),
            $data
        );

        $this->assertDatabaseHas('employee_documents', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_employee_document()
    {
        $employeeDocument = EmployeeDocument::factory()->create();

        $employee = Employee::factory()->create();
        $refDocType = RefDocType::factory()->create();
        $refDocPlace = RefDocPlace::factory()->create();

        $data = [
            'doc_seria' => $this->faker->text(255),
            'doc_num' => $this->faker->text(255),
            'doc_date' => $this->faker->date,
            'employee_id' => $employee->id,
            'ref_doc_type_id' => $refDocType->id,
            'ref_doc_place_id' => $refDocPlace->id,
        ];

        $response = $this->putJson(
            route('api.employee-documents.update', $employeeDocument),
            $data
        );

        $data['id'] = $employeeDocument->id;

        $this->assertDatabaseHas('employee_documents', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_document()
    {
        $employeeDocument = EmployeeDocument::factory()->create();

        $response = $this->deleteJson(
            route('api.employee-documents.destroy', $employeeDocument)
        );

        $this->assertSoftDeleted($employeeDocument);

        $response->assertNoContent();
    }
}
