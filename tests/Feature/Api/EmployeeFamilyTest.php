<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\EmployeeFamily;

use App\Models\Employee;
use App\Models\RefFamilyState;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeFamilyTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_families_list()
    {
        $employeeFamilies = EmployeeFamily::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.employee-families.index'));

        $response->assertOk()->assertSee($employeeFamilies[0]->lastname);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_family()
    {
        $data = EmployeeFamily::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(
            route('api.employee-families.store'),
            $data
        );

        $this->assertDatabaseHas('employee_families', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_employee_family()
    {
        $employeeFamily = EmployeeFamily::factory()->create();

        $employee = Employee::factory()->create();
        $refFamilyState = RefFamilyState::factory()->create();

        $data = [
            'lastname' => $this->faker->lastName,
            'firstname' => $this->faker->text(255),
            'middlename' => $this->faker->text(255),
            'birthday' => $this->faker->date,
            'employee_id' => $employee->id,
            'ref_family_state_id' => $refFamilyState->id,
        ];

        $response = $this->putJson(
            route('api.employee-families.update', $employeeFamily),
            $data
        );

        $data['id'] = $employeeFamily->id;

        $this->assertDatabaseHas('employee_families', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_family()
    {
        $employeeFamily = EmployeeFamily::factory()->create();

        $response = $this->deleteJson(
            route('api.employee-families.destroy', $employeeFamily)
        );

        $this->assertSoftDeleted($employeeFamily);

        $response->assertNoContent();
    }
}
