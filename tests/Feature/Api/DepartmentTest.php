<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Department;

use App\Models\Branch;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DepartmentTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_departments_list()
    {
        $departments = Department::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.departments.index'));

        $response->assertOk()->assertSee($departments[0]->email);
    }

    /**
     * @test
     */
    public function it_stores_the_department()
    {
        $data = Department::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.departments.store'), $data);

        $this->assertDatabaseHas('departments', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_department()
    {
        $department = Department::factory()->create();

        $branch = Branch::factory()->create();

        $data = [
            'parent_id' => $this->faker->randomNumber,
            'email' => $this->faker->email,
            'branch_id' => $branch->id,
        ];

        $response = $this->putJson(
            route('api.departments.update', $department),
            $data
        );

        $data['id'] = $department->id;

        $this->assertDatabaseHas('departments', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_department()
    {
        $department = Department::factory()->create();

        $response = $this->deleteJson(
            route('api.departments.destroy', $department)
        );

        $this->assertSoftDeleted($department);

        $response->assertNoContent();
    }
}
