<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefVidHoliday;

use App\Models\RefVidHolidayType;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefVidHolidayTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_vid_holidays_list()
    {
        $refVidHolidays = RefVidHoliday::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.ref-vid-holidays.index'));

        $response->assertOk()->assertSee($refVidHolidays[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_vid_holiday()
    {
        $data = RefVidHoliday::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.ref-vid-holidays.store'), $data);

        $this->assertDatabaseHas('ref_vid_holidays', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_ref_vid_holiday()
    {
        $refVidHoliday = RefVidHoliday::factory()->create();

        $refVidHolidayType = RefVidHolidayType::factory()->create();

        $data = [
            'ref_vid_holiday_type_id' => $refVidHolidayType->id,
        ];

        $response = $this->putJson(
            route('api.ref-vid-holidays.update', $refVidHoliday),
            $data
        );

        $data['id'] = $refVidHoliday->id;

        $this->assertDatabaseHas('ref_vid_holidays', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_vid_holiday()
    {
        $refVidHoliday = RefVidHoliday::factory()->create();

        $response = $this->deleteJson(
            route('api.ref-vid-holidays.destroy', $refVidHoliday)
        );

        $this->assertSoftDeleted($refVidHoliday);

        $response->assertNoContent();
    }
}
