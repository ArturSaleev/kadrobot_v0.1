<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefTechnicType;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefTechnicTypeTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_technic_types_list()
    {
        $refTechnicTypes = RefTechnicType::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.ref-technic-types.index'));

        $response->assertOk()->assertSee($refTechnicTypes[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_technic_type()
    {
        $data = RefTechnicType::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(
            route('api.ref-technic-types.store'),
            $data
        );

        $this->assertDatabaseHas('ref_technic_types', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_ref_technic_type()
    {
        $refTechnicType = RefTechnicType::factory()->create();

        $data = [
            'name' => $this->faker->name,
        ];

        $response = $this->putJson(
            route('api.ref-technic-types.update', $refTechnicType),
            $data
        );

        $data['id'] = $refTechnicType->id;

        $this->assertDatabaseHas('ref_technic_types', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_technic_type()
    {
        $refTechnicType = RefTechnicType::factory()->create();

        $response = $this->deleteJson(
            route('api.ref-technic-types.destroy', $refTechnicType)
        );

        $this->assertSoftDeleted($refTechnicType);

        $response->assertNoContent();
    }
}
