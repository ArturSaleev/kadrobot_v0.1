<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\EmployeeMilitary;

use App\Models\Employee;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeMilitaryTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_militaries_list()
    {
        $employeeMilitaries = EmployeeMilitary::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.employee-militaries.index'));

        $response->assertOk()->assertSee($employeeMilitaries[0]->group);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_military()
    {
        $data = EmployeeMilitary::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(
            route('api.employee-militaries.store'),
            $data
        );

        $this->assertDatabaseHas('employee_militaries', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_employee_military()
    {
        $employeeMilitary = EmployeeMilitary::factory()->create();

        $employee = Employee::factory()->create();

        $data = [
            'group' => $this->faker->text(255),
            'category' => $this->faker->text(255),
            'rank' => $this->faker->text(255),
            'speciality' => $this->faker->text(255),
            'voenkom' => $this->faker->text(255),
            'spec_uch' => $this->faker->text(255),
            'spec_uch_num' => $this->faker->text(255),
            'fit' => $this->faker->text(255),
            'employee_id' => $employee->id,
        ];

        $response = $this->putJson(
            route('api.employee-militaries.update', $employeeMilitary),
            $data
        );

        $data['id'] = $employeeMilitary->id;

        $this->assertDatabaseHas('employee_militaries', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_military()
    {
        $employeeMilitary = EmployeeMilitary::factory()->create();

        $response = $this->deleteJson(
            route('api.employee-militaries.destroy', $employeeMilitary)
        );

        $this->assertSoftDeleted($employeeMilitary);

        $response->assertNoContent();
    }
}
