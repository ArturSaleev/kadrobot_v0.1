<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\T2Card;
use App\Models\RefAction;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefActionT2CardsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_action_t2_cards()
    {
        $refAction = RefAction::factory()->create();
        $t2Cards = T2Card::factory()
            ->count(2)
            ->create([
                'ref_action_id' => $refAction->id,
            ]);

        $response = $this->getJson(
            route('api.ref-actions.t2-cards.index', $refAction)
        );

        $response->assertOk()->assertSee($t2Cards[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_action_t2_cards()
    {
        $refAction = RefAction::factory()->create();
        $data = T2Card::factory()
            ->make([
                'ref_action_id' => $refAction->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.ref-actions.t2-cards.store', $refAction),
            $data
        );

        $this->assertDatabaseHas('t2_cards', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $t2Card = T2Card::latest('id')->first();

        $this->assertEquals($refAction->id, $t2Card->ref_action_id);
    }
}
