<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefBank;
use App\Models\RefBankTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefBankRefBankTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_bank_ref_bank_translations()
    {
        $refBank = RefBank::factory()->create();
        $refBankTranslations = RefBankTranslation::factory()
            ->count(2)
            ->create([
                'ref_bank_id' => $refBank->id,
            ]);

        $response = $this->getJson(
            route('api.ref-banks.ref-bank-translations.index', $refBank)
        );

        $response->assertOk()->assertSee($refBankTranslations[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_bank_ref_bank_translations()
    {
        $refBank = RefBank::factory()->create();
        $data = RefBankTranslation::factory()
            ->make([
                'ref_bank_id' => $refBank->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.ref-banks.ref-bank-translations.store', $refBank),
            $data
        );

        unset($data['ref_bank_id']);
        unset($data['locale']);
        unset($data['name']);

        $this->assertDatabaseHas('ref_bank_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $refBankTranslation = RefBankTranslation::latest('id')->first();

        $this->assertEquals($refBank->id, $refBankTranslation->ref_bank_id);
    }
}
