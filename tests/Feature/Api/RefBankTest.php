<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefBank;

use App\Models\RefStatus;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefBankTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_banks_list()
    {
        $refBanks = RefBank::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.ref-banks.index'));

        $response->assertOk()->assertSee($refBanks[0]->mfo);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_bank()
    {
        $data = RefBank::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.ref-banks.store'), $data);

        $this->assertDatabaseHas('ref_banks', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_ref_bank()
    {
        $refBank = RefBank::factory()->create();

        $refStatus = RefStatus::factory()->create();

        $data = [
            'mfo' => $this->faker->text(255),
            'mfo_head' => $this->faker->text(255),
            'mfo_rkc' => $this->faker->text(255),
            'kor_account' => $this->faker->text(255),
            'commis' => $this->faker->randomNumber(2),
            'bin' => $this->faker->text(255),
            'bik_old' => $this->faker->text(255),
            'ref_status_id' => $refStatus->id,
        ];

        $response = $this->putJson(
            route('api.ref-banks.update', $refBank),
            $data
        );

        $data['id'] = $refBank->id;

        $this->assertDatabaseHas('ref_banks', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_bank()
    {
        $refBank = RefBank::factory()->create();

        $response = $this->deleteJson(route('api.ref-banks.destroy', $refBank));

        $this->assertSoftDeleted($refBank);

        $response->assertNoContent();
    }
}
