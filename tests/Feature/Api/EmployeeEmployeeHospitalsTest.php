<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Employee;
use App\Models\EmployeeHospital;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeEmployeeHospitalsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_employee_hospitals()
    {
        $employee = Employee::factory()->create();
        $employeeHospitals = EmployeeHospital::factory()
            ->count(2)
            ->create([
                'employee_id' => $employee->id,
            ]);

        $response = $this->getJson(
            route('api.employees.employee-hospitals.index', $employee)
        );

        $response->assertOk()->assertSee($employeeHospitals[0]->date_begin);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_employee_hospitals()
    {
        $employee = Employee::factory()->create();
        $data = EmployeeHospital::factory()
            ->make([
                'employee_id' => $employee->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.employees.employee-hospitals.store', $employee),
            $data
        );

        $this->assertDatabaseHas('employee_hospitals', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employeeHospital = EmployeeHospital::latest('id')->first();

        $this->assertEquals($employee->id, $employeeHospital->employee_id);
    }
}
