<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\ReportHtmlOther;

use App\Models\ReportHtml;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReportHtmlOtherTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_report_html_others_list()
    {
        $reportHtmlOthers = ReportHtmlOther::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.report-html-others.index'));

        $response->assertOk()->assertSee($reportHtmlOthers[0]->title);
    }

    /**
     * @test
     */
    public function it_stores_the_report_html_other()
    {
        $data = ReportHtmlOther::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(
            route('api.report-html-others.store'),
            $data
        );

        $this->assertDatabaseHas('report_html_others', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_report_html_other()
    {
        $reportHtmlOther = ReportHtmlOther::factory()->create();

        $reportHtml = ReportHtml::factory()->create();

        $data = [
            'title' => $this->faker->sentence(10),
            'html_text' => $this->faker->text,
            'sql_text' => $this->faker->text,
            'position' => $this->faker->randomNumber(0),
            'num_pp' => $this->faker->randomNumber(0),
            'report_html_id' => $reportHtml->id,
        ];

        $response = $this->putJson(
            route('api.report-html-others.update', $reportHtmlOther),
            $data
        );

        $data['id'] = $reportHtmlOther->id;

        $this->assertDatabaseHas('report_html_others', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_report_html_other()
    {
        $reportHtmlOther = ReportHtmlOther::factory()->create();

        $response = $this->deleteJson(
            route('api.report-html-others.destroy', $reportHtmlOther)
        );

        $this->assertSoftDeleted($reportHtmlOther);

        $response->assertNoContent();
    }
}
