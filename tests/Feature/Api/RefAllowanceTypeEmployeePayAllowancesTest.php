<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefAllowanceType;
use App\Models\EmployeePayAllowance;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefAllowanceTypeEmployeePayAllowancesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_allowance_type_employee_pay_allowances()
    {
        $refAllowanceType = RefAllowanceType::factory()->create();
        $employeePayAllowances = EmployeePayAllowance::factory()
            ->count(2)
            ->create([
                'ref_allowance_type_id' => $refAllowanceType->id,
            ]);

        $response = $this->getJson(
            route(
                'api.ref-allowance-types.employee-pay-allowances.index',
                $refAllowanceType
            )
        );

        $response->assertOk()->assertSee($employeePayAllowances[0]->date_add);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_allowance_type_employee_pay_allowances()
    {
        $refAllowanceType = RefAllowanceType::factory()->create();
        $data = EmployeePayAllowance::factory()
            ->make([
                'ref_allowance_type_id' => $refAllowanceType->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.ref-allowance-types.employee-pay-allowances.store',
                $refAllowanceType
            ),
            $data
        );

        $this->assertDatabaseHas('employee_pay_allowances', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employeePayAllowance = EmployeePayAllowance::latest('id')->first();

        $this->assertEquals(
            $refAllowanceType->id,
            $employeePayAllowance->ref_allowance_type_id
        );
    }
}
