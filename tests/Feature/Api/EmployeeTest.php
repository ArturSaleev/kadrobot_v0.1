<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Employee;

use App\Models\RefSex;
use App\Models\Position;
use App\Models\RefUserStatus;
use App\Models\RefNationality;
use App\Models\RefFamilyState;
use App\Models\RefAccountType;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employees_list()
    {
        $employees = Employee::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.employees.index'));

        $response->assertOk()->assertSee($employees[0]->iin);
    }

    /**
     * @test
     */
    public function it_stores_the_employee()
    {
        $data = Employee::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.employees.store'), $data);

        unset($data['reason_loyoff']);
        unset($data['position_id']);

        $this->assertDatabaseHas('employees', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_employee()
    {
        $employee = Employee::factory()->create();

        $refSex = RefSex::factory()->create();
        $refNationality = RefNationality::factory()->create();
        $refFamilyState = RefFamilyState::factory()->create();
        $refAccountType = RefAccountType::factory()->create();
        $refUserStatus = RefUserStatus::factory()->create();
        $position = Position::factory()->create();

        $data = [
            'iin' => $this->faker->text(255),
            'date_post' => $this->faker->date,
            'date_loyoff' => $this->faker->date,
            'reason_loyoff' => $this->faker->text,
            'contract_num' => $this->faker->text(255),
            'contract_date' => $this->faker->date,
            'tab_num' => $this->faker->randomNumber(0),
            'birthday' => $this->faker->date,
            'birth_place' => $this->faker->text,
            'email' => $this->faker->email,
            'account' => $this->faker->text(255),
            'date_zav' => $this->faker->date,
            'oklad' => $this->faker->randomNumber(2),
            'gos_nagr' => $this->faker->text(255),
            'pens' => $this->faker->boolean,
            'pens_date' => $this->faker->date,
            'lgot' => $this->faker->text(255),
            'person_email' => $this->faker->text(255),
            'ref_sex_id' => $refSex->id,
            'ref_nationality_id' => $refNationality->id,
            'ref_family_state_id' => $refFamilyState->id,
            'ref_account_type_id' => $refAccountType->id,
            'ref_user_status_id' => $refUserStatus->id,
            'position_id' => $position->id,
        ];

        $response = $this->putJson(
            route('api.employees.update', $employee),
            $data
        );

        unset($data['reason_loyoff']);
        unset($data['position_id']);

        $data['id'] = $employee->id;

        $this->assertDatabaseHas('employees', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_employee()
    {
        $employee = Employee::factory()->create();

        $response = $this->deleteJson(
            route('api.employees.destroy', $employee)
        );

        $this->assertSoftDeleted($employee);

        $response->assertNoContent();
    }
}
