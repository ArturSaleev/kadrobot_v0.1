<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Curator;
use App\Models\Employee;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeCuratorsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_curators()
    {
        $employee = Employee::factory()->create();
        $curators = Curator::factory()
            ->count(2)
            ->create([
                'employee_id' => $employee->id,
            ]);

        $response = $this->getJson(
            route('api.employees.curators.index', $employee)
        );

        $response->assertOk()->assertSee($curators[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_curators()
    {
        $employee = Employee::factory()->create();
        $data = Curator::factory()
            ->make([
                'employee_id' => $employee->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.employees.curators.store', $employee),
            $data
        );

        $this->assertDatabaseHas('curators', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $curator = Curator::latest('id')->first();

        $this->assertEquals($employee->id, $curator->employee_id);
    }
}
