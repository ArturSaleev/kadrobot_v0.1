<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefAllowanceType;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefAllowanceTypeTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_allowance_types_list()
    {
        $refAllowanceTypes = RefAllowanceType::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.ref-allowance-types.index'));

        $response->assertOk()->assertSee($refAllowanceTypes[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_allowance_type()
    {
        $data = RefAllowanceType::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(
            route('api.ref-allowance-types.store'),
            $data
        );

        $this->assertDatabaseHas('ref_allowance_types', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_ref_allowance_type()
    {
        $refAllowanceType = RefAllowanceType::factory()->create();

        $data = [];

        $response = $this->putJson(
            route('api.ref-allowance-types.update', $refAllowanceType),
            $data
        );

        $data['id'] = $refAllowanceType->id;

        $this->assertDatabaseHas('ref_allowance_types', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_allowance_type()
    {
        $refAllowanceType = RefAllowanceType::factory()->create();

        $response = $this->deleteJson(
            route('api.ref-allowance-types.destroy', $refAllowanceType)
        );

        $this->assertSoftDeleted($refAllowanceType);

        $response->assertNoContent();
    }
}
