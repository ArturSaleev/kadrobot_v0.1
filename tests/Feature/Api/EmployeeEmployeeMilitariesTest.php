<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Employee;
use App\Models\EmployeeMilitary;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeEmployeeMilitariesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_employee_militaries()
    {
        $employee = Employee::factory()->create();
        $employeeMilitaries = EmployeeMilitary::factory()
            ->count(2)
            ->create([
                'employee_id' => $employee->id,
            ]);

        $response = $this->getJson(
            route('api.employees.employee-militaries.index', $employee)
        );

        $response->assertOk()->assertSee($employeeMilitaries[0]->group);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_employee_militaries()
    {
        $employee = Employee::factory()->create();
        $data = EmployeeMilitary::factory()
            ->make([
                'employee_id' => $employee->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.employees.employee-militaries.store', $employee),
            $data
        );

        $this->assertDatabaseHas('employee_militaries', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employeeMilitary = EmployeeMilitary::latest('id')->first();

        $this->assertEquals($employee->id, $employeeMilitary->employee_id);
    }
}
