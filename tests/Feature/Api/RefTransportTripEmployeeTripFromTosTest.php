<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefTransportTrip;
use App\Models\EmployeeTripFromTo;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefTransportTripEmployeeTripFromTosTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_transport_trip_employee_trip_from_tos()
    {
        $refTransportTrip = RefTransportTrip::factory()->create();
        $employeeTripFromTos = EmployeeTripFromTo::factory()
            ->count(2)
            ->create([
                'ref_transport_trip_id' => $refTransportTrip->id,
            ]);

        $response = $this->getJson(
            route(
                'api.ref-transport-trips.employee-trip-from-tos.index',
                $refTransportTrip
            )
        );

        $response->assertOk()->assertSee($employeeTripFromTos[0]->from_place);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_transport_trip_employee_trip_from_tos()
    {
        $refTransportTrip = RefTransportTrip::factory()->create();
        $data = EmployeeTripFromTo::factory()
            ->make([
                'ref_transport_trip_id' => $refTransportTrip->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.ref-transport-trips.employee-trip-from-tos.store',
                $refTransportTrip
            ),
            $data
        );

        $this->assertDatabaseHas('employee_trip_from_tos', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employeeTripFromTo = EmployeeTripFromTo::latest('id')->first();

        $this->assertEquals(
            $refTransportTrip->id,
            $employeeTripFromTo->ref_transport_trip_id
        );
    }
}
