<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefStatus;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefStatusTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_statuses_list()
    {
        $refStatuses = RefStatus::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.ref-statuses.index'));

        $response->assertOk()->assertSee($refStatuses[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_status()
    {
        $data = RefStatus::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.ref-statuses.store'), $data);

        $this->assertDatabaseHas('ref_statuses', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_ref_status()
    {
        $refStatus = RefStatus::factory()->create();

        $data = [];

        $response = $this->putJson(
            route('api.ref-statuses.update', $refStatus),
            $data
        );

        $data['id'] = $refStatus->id;

        $this->assertDatabaseHas('ref_statuses', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_status()
    {
        $refStatus = RefStatus::factory()->create();

        $response = $this->deleteJson(
            route('api.ref-statuses.destroy', $refStatus)
        );

        $this->assertSoftDeleted($refStatus);

        $response->assertNoContent();
    }
}
