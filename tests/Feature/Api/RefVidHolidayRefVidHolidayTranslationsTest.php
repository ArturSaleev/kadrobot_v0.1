<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefVidHoliday;
use App\Models\RefVidHolidayTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefVidHolidayRefVidHolidayTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_vid_holiday_ref_vid_holiday_translations()
    {
        $refVidHoliday = RefVidHoliday::factory()->create();
        $refVidHolidayTranslations = RefVidHolidayTranslation::factory()
            ->count(2)
            ->create([
                'ref_vid_holiday_id' => $refVidHoliday->id,
            ]);

        $response = $this->getJson(
            route(
                'api.ref-vid-holidays.ref-vid-holiday-translations.index',
                $refVidHoliday
            )
        );

        $response->assertOk()->assertSee($refVidHolidayTranslations[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_vid_holiday_ref_vid_holiday_translations()
    {
        $refVidHoliday = RefVidHoliday::factory()->create();
        $data = RefVidHolidayTranslation::factory()
            ->make([
                'ref_vid_holiday_id' => $refVidHoliday->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.ref-vid-holidays.ref-vid-holiday-translations.store',
                $refVidHoliday
            ),
            $data
        );

        unset($data['ref_vid_holiday_id']);
        unset($data['locale']);
        unset($data['name']);

        $this->assertDatabaseHas('ref_vid_holiday_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $refVidHolidayTranslation = RefVidHolidayTranslation::latest(
            'id'
        )->first();

        $this->assertEquals(
            $refVidHoliday->id,
            $refVidHolidayTranslation->ref_vid_holiday_id
        );
    }
}
