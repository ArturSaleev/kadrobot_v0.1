<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\EmployeeTrip;
use App\Models\EmployeeTripFromTo;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeTripEmployeeTripFromTosTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_trip_employee_trip_from_tos()
    {
        $employeeTrip = EmployeeTrip::factory()->create();
        $employeeTripFromTos = EmployeeTripFromTo::factory()
            ->count(2)
            ->create([
                'employee_trip_id' => $employeeTrip->id,
            ]);

        $response = $this->getJson(
            route(
                'api.employee-trips.employee-trip-from-tos.index',
                $employeeTrip
            )
        );

        $response->assertOk()->assertSee($employeeTripFromTos[0]->from_place);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_trip_employee_trip_from_tos()
    {
        $employeeTrip = EmployeeTrip::factory()->create();
        $data = EmployeeTripFromTo::factory()
            ->make([
                'employee_trip_id' => $employeeTrip->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.employee-trips.employee-trip-from-tos.store',
                $employeeTrip
            ),
            $data
        );

        $this->assertDatabaseHas('employee_trip_from_tos', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employeeTripFromTo = EmployeeTripFromTo::latest('id')->first();

        $this->assertEquals(
            $employeeTrip->id,
            $employeeTripFromTo->employee_trip_id
        );
    }
}
