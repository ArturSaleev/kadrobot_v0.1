<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefTypePhone;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefTypePhoneTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_type_phones_list()
    {
        $refTypePhones = RefTypePhone::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.ref-type-phones.index'));

        $response->assertOk()->assertSee($refTypePhones[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_type_phone()
    {
        $data = RefTypePhone::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.ref-type-phones.store'), $data);

        $this->assertDatabaseHas('ref_type_phones', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_ref_type_phone()
    {
        $refTypePhone = RefTypePhone::factory()->create();

        $data = [];

        $response = $this->putJson(
            route('api.ref-type-phones.update', $refTypePhone),
            $data
        );

        $data['id'] = $refTypePhone->id;

        $this->assertDatabaseHas('ref_type_phones', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_type_phone()
    {
        $refTypePhone = RefTypePhone::factory()->create();

        $response = $this->deleteJson(
            route('api.ref-type-phones.destroy', $refTypePhone)
        );

        $this->assertSoftDeleted($refTypePhone);

        $response->assertNoContent();
    }
}
