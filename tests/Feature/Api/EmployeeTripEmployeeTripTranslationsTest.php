<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\EmployeeTrip;
use App\Models\EmployeeTripTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeTripEmployeeTripTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_trip_employee_trip_translations()
    {
        $employeeTrip = EmployeeTrip::factory()->create();
        $employeeTripTranslations = EmployeeTripTranslation::factory()
            ->count(2)
            ->create([
                'employee_trip_id' => $employeeTrip->id,
            ]);

        $response = $this->getJson(
            route(
                'api.employee-trips.employee-trip-translations.index',
                $employeeTrip
            )
        );

        $response->assertOk()->assertSee($employeeTripTranslations[0]->locale);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_trip_employee_trip_translations()
    {
        $employeeTrip = EmployeeTrip::factory()->create();
        $data = EmployeeTripTranslation::factory()
            ->make([
                'employee_trip_id' => $employeeTrip->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.employee-trips.employee-trip-translations.store',
                $employeeTrip
            ),
            $data
        );

        unset($data['employee_trip_id']);
        unset($data['locale']);
        unset($data['aim']);
        unset($data['final_distantion']);

        $this->assertDatabaseHas('employee_trip_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employeeTripTranslation = EmployeeTripTranslation::latest(
            'id'
        )->first();

        $this->assertEquals(
            $employeeTrip->id,
            $employeeTripTranslation->employee_trip_id
        );
    }
}
