<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Position;
use App\Models\PositionDeclension;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PositionPositionDeclensionsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_position_position_declensions()
    {
        $position = Position::factory()->create();
        $positionDeclensions = PositionDeclension::factory()
            ->count(2)
            ->create([
                'position_id' => $position->id,
            ]);

        $response = $this->getJson(
            route('api.positions.position-declensions.index', $position)
        );

        $response->assertOk()->assertSee($positionDeclensions[0]->locale);
    }

    /**
     * @test
     */
    public function it_stores_the_position_position_declensions()
    {
        $position = Position::factory()->create();
        $data = PositionDeclension::factory()
            ->make([
                'position_id' => $position->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.positions.position-declensions.store', $position),
            $data
        );

        unset($data['position_id']);
        unset($data['locale']);
        unset($data['value']);
        unset($data['case_type']);
        unset($data['type_description']);

        $this->assertDatabaseHas('position_declensions', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $positionDeclension = PositionDeclension::latest('id')->first();

        $this->assertEquals($position->id, $positionDeclension->position_id);
    }
}
