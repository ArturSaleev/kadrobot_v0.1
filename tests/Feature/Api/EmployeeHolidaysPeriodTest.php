<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\EmployeeHolidaysPeriod;

use App\Models\Employee;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeHolidaysPeriodTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_holidays_periods_list()
    {
        $employeeHolidaysPeriods = EmployeeHolidaysPeriod::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(
            route('api.employee-holidays-periods.index')
        );

        $response
            ->assertOk()
            ->assertSee($employeeHolidaysPeriods[0]->period_start);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_holidays_period()
    {
        $data = EmployeeHolidaysPeriod::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(
            route('api.employee-holidays-periods.store'),
            $data
        );

        $this->assertDatabaseHas('employee_holidays_periods', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_employee_holidays_period()
    {
        $employeeHolidaysPeriod = EmployeeHolidaysPeriod::factory()->create();

        $employee = Employee::factory()->create();

        $data = [
            'period_start' => $this->faker->date,
            'period_end' => $this->faker->date,
            'day_count_used_for_today' => $this->faker->randomNumber(0),
            'didnt_add' => $this->faker->randomNumber(0),
            'paying_for_health' => $this->faker->randomNumber(0),
            'employee_id' => $employee->id,
        ];

        $response = $this->putJson(
            route(
                'api.employee-holidays-periods.update',
                $employeeHolidaysPeriod
            ),
            $data
        );

        $data['id'] = $employeeHolidaysPeriod->id;

        $this->assertDatabaseHas('employee_holidays_periods', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_holidays_period()
    {
        $employeeHolidaysPeriod = EmployeeHolidaysPeriod::factory()->create();

        $response = $this->deleteJson(
            route(
                'api.employee-holidays-periods.destroy',
                $employeeHolidaysPeriod
            )
        );

        $this->assertSoftDeleted($employeeHolidaysPeriod);

        $response->assertNoContent();
    }
}
