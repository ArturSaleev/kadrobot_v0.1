<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Branch;
use App\Models\BranchTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BranchBranchTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_branch_branch_translations()
    {
        $branch = Branch::factory()->create();
        $branchTranslations = BranchTranslation::factory()
            ->count(2)
            ->create([
                'branch_id' => $branch->id,
            ]);

        $response = $this->getJson(
            route('api.branches.branch-translations.index', $branch)
        );

        $response->assertOk()->assertSee($branchTranslations[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_branch_branch_translations()
    {
        $branch = Branch::factory()->create();
        $data = BranchTranslation::factory()
            ->make([
                'branch_id' => $branch->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.branches.branch-translations.store', $branch),
            $data
        );

        unset($data['branch_id']);
        unset($data['locale']);
        unset($data['name']);
        unset($data['short_name']);

        $this->assertDatabaseHas('branch_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $branchTranslation = BranchTranslation::latest('id')->first();

        $this->assertEquals($branch->id, $branchTranslation->branch_id);
    }
}
