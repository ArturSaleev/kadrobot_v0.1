<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\EmployeeAddress;

use App\Models\Employee;
use App\Models\RefAddressType;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeAddressTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_addresses_list()
    {
        $employeeAddresses = EmployeeAddress::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.employee-addresses.index'));

        $response->assertOk()->assertSee($employeeAddresses[0]->locale);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_address()
    {
        $data = EmployeeAddress::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(
            route('api.employee-addresses.store'),
            $data
        );

        $this->assertDatabaseHas('employee_addresses', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_employee_address()
    {
        $employeeAddress = EmployeeAddress::factory()->create();

        $employee = Employee::factory()->create();
        $refAddressType = RefAddressType::factory()->create();

        $data = [
            'locale' => $this->faker->locale,
            'employee_id' => $employee->id,
            'ref_address_type_id' => $refAddressType->id,
        ];

        $response = $this->putJson(
            route('api.employee-addresses.update', $employeeAddress),
            $data
        );

        $data['id'] = $employeeAddress->id;

        $this->assertDatabaseHas('employee_addresses', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_address()
    {
        $employeeAddress = EmployeeAddress::factory()->create();

        $response = $this->deleteJson(
            route('api.employee-addresses.destroy', $employeeAddress)
        );

        $this->assertSoftDeleted($employeeAddress);

        $response->assertNoContent();
    }
}
