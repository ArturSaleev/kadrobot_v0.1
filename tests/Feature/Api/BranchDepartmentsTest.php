<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Branch;
use App\Models\Department;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BranchDepartmentsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_branch_departments()
    {
        $branch = Branch::factory()->create();
        $departments = Department::factory()
            ->count(2)
            ->create([
                'branch_id' => $branch->id,
            ]);

        $response = $this->getJson(
            route('api.branches.departments.index', $branch)
        );

        $response->assertOk()->assertSee($departments[0]->email);
    }

    /**
     * @test
     */
    public function it_stores_the_branch_departments()
    {
        $branch = Branch::factory()->create();
        $data = Department::factory()
            ->make([
                'branch_id' => $branch->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.branches.departments.store', $branch),
            $data
        );

        $this->assertDatabaseHas('departments', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $department = Department::latest('id')->first();

        $this->assertEquals($branch->id, $department->branch_id);
    }
}
