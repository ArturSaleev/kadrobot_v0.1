<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Employee;
use App\Models\EmployeeHolidaysPeriod;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeEmployeeHolidaysPeriodsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_employee_holidays_periods()
    {
        $employee = Employee::factory()->create();
        $employeeHolidaysPeriods = EmployeeHolidaysPeriod::factory()
            ->count(2)
            ->create([
                'employee_id' => $employee->id,
            ]);

        $response = $this->getJson(
            route('api.employees.employee-holidays-periods.index', $employee)
        );

        $response
            ->assertOk()
            ->assertSee($employeeHolidaysPeriods[0]->period_start);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_employee_holidays_periods()
    {
        $employee = Employee::factory()->create();
        $data = EmployeeHolidaysPeriod::factory()
            ->make([
                'employee_id' => $employee->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.employees.employee-holidays-periods.store', $employee),
            $data
        );

        $this->assertDatabaseHas('employee_holidays_periods', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employeeHolidaysPeriod = EmployeeHolidaysPeriod::latest('id')->first();

        $this->assertEquals(
            $employee->id,
            $employeeHolidaysPeriod->employee_id
        );
    }
}
