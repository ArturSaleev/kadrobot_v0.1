<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefAllowanceType;
use App\Models\RefAllowanceTypeTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefAllowanceTypeRefAllowanceTypeTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_allowance_type_ref_allowance_type_translations()
    {
        $refAllowanceType = RefAllowanceType::factory()->create();
        $refAllowanceTypeTranslations = RefAllowanceTypeTranslation::factory()
            ->count(2)
            ->create([
                'ref_allowance_type_id' => $refAllowanceType->id,
            ]);

        $response = $this->getJson(
            route(
                'api.ref-allowance-types.ref-allowance-type-translations.index',
                $refAllowanceType
            )
        );

        $response
            ->assertOk()
            ->assertSee($refAllowanceTypeTranslations[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_allowance_type_ref_allowance_type_translations()
    {
        $refAllowanceType = RefAllowanceType::factory()->create();
        $data = RefAllowanceTypeTranslation::factory()
            ->make([
                'ref_allowance_type_id' => $refAllowanceType->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.ref-allowance-types.ref-allowance-type-translations.store',
                $refAllowanceType
            ),
            $data
        );

        unset($data['ref_allowance_type_id']);
        unset($data['locale']);
        unset($data['name']);

        $this->assertDatabaseHas('ref_allowance_type_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $refAllowanceTypeTranslation = RefAllowanceTypeTranslation::latest(
            'id'
        )->first();

        $this->assertEquals(
            $refAllowanceType->id,
            $refAllowanceTypeTranslation->ref_allowance_type_id
        );
    }
}
