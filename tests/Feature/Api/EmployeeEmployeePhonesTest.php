<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Employee;
use App\Models\EmployeePhone;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeEmployeePhonesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_employee_phones()
    {
        $employee = Employee::factory()->create();
        $employeePhones = EmployeePhone::factory()
            ->count(2)
            ->create([
                'employee_id' => $employee->id,
            ]);

        $response = $this->getJson(
            route('api.employees.employee-phones.index', $employee)
        );

        $response->assertOk()->assertSee($employeePhones[0]->phone);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_employee_phones()
    {
        $employee = Employee::factory()->create();
        $data = EmployeePhone::factory()
            ->make([
                'employee_id' => $employee->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.employees.employee-phones.store', $employee),
            $data
        );

        $this->assertDatabaseHas('employee_phones', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employeePhone = EmployeePhone::latest('id')->first();

        $this->assertEquals($employee->id, $employeePhone->employee_id);
    }
}
