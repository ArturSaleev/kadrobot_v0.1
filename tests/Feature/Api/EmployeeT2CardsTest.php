<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\T2Card;
use App\Models\Employee;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeT2CardsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_t2_cards()
    {
        $employee = Employee::factory()->create();
        $t2Cards = T2Card::factory()
            ->count(2)
            ->create([
                'employee_id' => $employee->id,
            ]);

        $response = $this->getJson(
            route('api.employees.t2-cards.index', $employee)
        );

        $response->assertOk()->assertSee($t2Cards[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_t2_cards()
    {
        $employee = Employee::factory()->create();
        $data = T2Card::factory()
            ->make([
                'employee_id' => $employee->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.employees.t2-cards.store', $employee),
            $data
        );

        $this->assertDatabaseHas('t2_cards', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $t2Card = T2Card::latest('id')->first();

        $this->assertEquals($employee->id, $t2Card->employee_id);
    }
}
