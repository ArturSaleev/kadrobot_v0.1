<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\EmployeeTechic;

use App\Models\Employee;
use App\Models\RefTechnicType;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeTechicTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_techics_list()
    {
        $employeeTechics = EmployeeTechic::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.employee-techics.index'));

        $response->assertOk()->assertSee($employeeTechics[0]->invent_num);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_techic()
    {
        $data = EmployeeTechic::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.employee-techics.store'), $data);

        $this->assertDatabaseHas('employee_techics', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_employee_techic()
    {
        $employeeTechic = EmployeeTechic::factory()->create();

        $employee = Employee::factory()->create();
        $refTechnicType = RefTechnicType::factory()->create();

        $data = [
            'invent_num' => $this->faker->text(255),
            'price' => $this->faker->randomFloat(2, 0, 9999),
            'employee_id' => $employee->id,
            'ref_technic_type_id' => $refTechnicType->id,
        ];

        $response = $this->putJson(
            route('api.employee-techics.update', $employeeTechic),
            $data
        );

        $data['id'] = $employeeTechic->id;

        $this->assertDatabaseHas('employee_techics', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_techic()
    {
        $employeeTechic = EmployeeTechic::factory()->create();

        $response = $this->deleteJson(
            route('api.employee-techics.destroy', $employeeTechic)
        );

        $this->assertModelMissing($employeeTechic);

        $response->assertNoContent();
    }
}
