<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Employee;
use App\Models\EmployeePayAllowance;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeEmployeePayAllowancesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_employee_pay_allowances()
    {
        $employee = Employee::factory()->create();
        $employeePayAllowances = EmployeePayAllowance::factory()
            ->count(2)
            ->create([
                'employee_id' => $employee->id,
            ]);

        $response = $this->getJson(
            route('api.employees.employee-pay-allowances.index', $employee)
        );

        $response->assertOk()->assertSee($employeePayAllowances[0]->date_add);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_employee_pay_allowances()
    {
        $employee = Employee::factory()->create();
        $data = EmployeePayAllowance::factory()
            ->make([
                'employee_id' => $employee->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.employees.employee-pay-allowances.store', $employee),
            $data
        );

        $this->assertDatabaseHas('employee_pay_allowances', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employeePayAllowance = EmployeePayAllowance::latest('id')->first();

        $this->assertEquals($employee->id, $employeePayAllowance->employee_id);
    }
}
