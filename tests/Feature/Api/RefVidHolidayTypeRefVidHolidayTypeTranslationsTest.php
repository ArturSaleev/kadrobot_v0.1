<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefVidHolidayType;
use App\Models\RefVidHolidayTypeTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefVidHolidayTypeRefVidHolidayTypeTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_vid_holiday_type_ref_vid_holiday_type_translations()
    {
        $refVidHolidayType = RefVidHolidayType::factory()->create();
        $refVidHolidayTypeTranslations = RefVidHolidayTypeTranslation::factory()
            ->count(2)
            ->create([
                'ref_vid_holiday_type_id' => $refVidHolidayType->id,
            ]);

        $response = $this->getJson(
            route(
                'api.ref-vid-holiday-types.ref-vid-holiday-type-translations.index',
                $refVidHolidayType
            )
        );

        $response
            ->assertOk()
            ->assertSee($refVidHolidayTypeTranslations[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_vid_holiday_type_ref_vid_holiday_type_translations()
    {
        $refVidHolidayType = RefVidHolidayType::factory()->create();
        $data = RefVidHolidayTypeTranslation::factory()
            ->make([
                'ref_vid_holiday_type_id' => $refVidHolidayType->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.ref-vid-holiday-types.ref-vid-holiday-type-translations.store',
                $refVidHolidayType
            ),
            $data
        );

        unset($data['ref_vid_holiday_type_id']);
        unset($data['locale']);
        unset($data['name']);

        $this->assertDatabaseHas('ref_vid_holiday_type_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $refVidHolidayTypeTranslation = RefVidHolidayTypeTranslation::latest(
            'id'
        )->first();

        $this->assertEquals(
            $refVidHolidayType->id,
            $refVidHolidayTypeTranslation->ref_vid_holiday_type_id
        );
    }
}
