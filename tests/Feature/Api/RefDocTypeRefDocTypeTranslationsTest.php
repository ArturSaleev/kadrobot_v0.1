<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefDocType;
use App\Models\RefDocTypeTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefDocTypeRefDocTypeTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_doc_type_ref_doc_type_translations()
    {
        $refDocType = RefDocType::factory()->create();
        $refDocTypeTranslations = RefDocTypeTranslation::factory()
            ->count(2)
            ->create([
                'ref_doc_type_id' => $refDocType->id,
            ]);

        $response = $this->getJson(
            route(
                'api.ref-doc-types.ref-doc-type-translations.index',
                $refDocType
            )
        );

        $response->assertOk()->assertSee($refDocTypeTranslations[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_doc_type_ref_doc_type_translations()
    {
        $refDocType = RefDocType::factory()->create();
        $data = RefDocTypeTranslation::factory()
            ->make([
                'ref_doc_type_id' => $refDocType->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.ref-doc-types.ref-doc-type-translations.store',
                $refDocType
            ),
            $data
        );

        unset($data['ref_doc_type_id']);
        unset($data['locale']);
        unset($data['name']);

        $this->assertDatabaseHas('ref_doc_type_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $refDocTypeTranslation = RefDocTypeTranslation::latest('id')->first();

        $this->assertEquals(
            $refDocType->id,
            $refDocTypeTranslation->ref_doc_type_id
        );
    }
}
