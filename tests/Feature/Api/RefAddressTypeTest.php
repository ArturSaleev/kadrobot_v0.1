<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefAddressType;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefAddressTypeTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_address_types_list()
    {
        $refAddressTypes = RefAddressType::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.ref-address-types.index'));

        $response->assertOk()->assertSee($refAddressTypes[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_address_type()
    {
        $data = RefAddressType::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(
            route('api.ref-address-types.store'),
            $data
        );

        $this->assertDatabaseHas('ref_address_types', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_ref_address_type()
    {
        $refAddressType = RefAddressType::factory()->create();

        $data = [];

        $response = $this->putJson(
            route('api.ref-address-types.update', $refAddressType),
            $data
        );

        $data['id'] = $refAddressType->id;

        $this->assertDatabaseHas('ref_address_types', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_address_type()
    {
        $refAddressType = RefAddressType::factory()->create();

        $response = $this->deleteJson(
            route('api.ref-address-types.destroy', $refAddressType)
        );

        $this->assertSoftDeleted($refAddressType);

        $response->assertNoContent();
    }
}
