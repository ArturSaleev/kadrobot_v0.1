<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefKindHoliday;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefKindHolidayTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_kind_holidays_list()
    {
        $refKindHolidays = RefKindHoliday::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.ref-kind-holidays.index'));

        $response->assertOk()->assertSee($refKindHolidays[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_kind_holiday()
    {
        $data = RefKindHoliday::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(
            route('api.ref-kind-holidays.store'),
            $data
        );

        $this->assertDatabaseHas('ref_kind_holidays', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_ref_kind_holiday()
    {
        $refKindHoliday = RefKindHoliday::factory()->create();

        $data = [];

        $response = $this->putJson(
            route('api.ref-kind-holidays.update', $refKindHoliday),
            $data
        );

        $data['id'] = $refKindHoliday->id;

        $this->assertDatabaseHas('ref_kind_holidays', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_kind_holiday()
    {
        $refKindHoliday = RefKindHoliday::factory()->create();

        $response = $this->deleteJson(
            route('api.ref-kind-holidays.destroy', $refKindHoliday)
        );

        $this->assertSoftDeleted($refKindHoliday);

        $response->assertNoContent();
    }
}
