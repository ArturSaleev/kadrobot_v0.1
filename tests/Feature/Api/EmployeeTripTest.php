<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\EmployeeTrip;

use App\Models\Employee;
use App\Models\RefTransportTrip;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeTripTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_trips_list()
    {
        $employeeTrips = EmployeeTrip::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.employee-trips.index'));

        $response->assertOk()->assertSee($employeeTrips[0]->date_begin);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_trip()
    {
        $data = EmployeeTrip::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.employee-trips.store'), $data);

        $this->assertDatabaseHas('employee_trips', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_employee_trip()
    {
        $employeeTrip = EmployeeTrip::factory()->create();

        $employee = Employee::factory()->create();
        $refTransportTrip = RefTransportTrip::factory()->create();

        $data = [
            'date_begin' => $this->faker->date,
            'date_end' => $this->faker->date,
            'cnt_days' => $this->faker->randomNumber(0),
            'order_num' => $this->faker->text(255),
            'order_date' => $this->faker->date,
            'employee_id' => $employee->id,
            'ref_transport_trip_id' => $refTransportTrip->id,
        ];

        $response = $this->putJson(
            route('api.employee-trips.update', $employeeTrip),
            $data
        );

        $data['id'] = $employeeTrip->id;

        $this->assertDatabaseHas('employee_trips', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_trip()
    {
        $employeeTrip = EmployeeTrip::factory()->create();

        $response = $this->deleteJson(
            route('api.employee-trips.destroy', $employeeTrip)
        );

        $this->assertSoftDeleted($employeeTrip);

        $response->assertNoContent();
    }
}
