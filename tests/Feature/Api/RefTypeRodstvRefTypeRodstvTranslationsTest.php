<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefTypeRodstv;
use App\Models\RefTypeRodstvTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefTypeRodstvRefTypeRodstvTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_type_rodstv_ref_type_rodstv_translations()
    {
        $refTypeRodstv = RefTypeRodstv::factory()->create();
        $refTypeRodstvTranslations = RefTypeRodstvTranslation::factory()
            ->count(2)
            ->create([
                'ref_type_rodstv_id' => $refTypeRodstv->id,
            ]);

        $response = $this->getJson(
            route(
                'api.ref-type-rodstvs.ref-type-rodstv-translations.index',
                $refTypeRodstv
            )
        );

        $response->assertOk()->assertSee($refTypeRodstvTranslations[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_type_rodstv_ref_type_rodstv_translations()
    {
        $refTypeRodstv = RefTypeRodstv::factory()->create();
        $data = RefTypeRodstvTranslation::factory()
            ->make([
                'ref_type_rodstv_id' => $refTypeRodstv->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.ref-type-rodstvs.ref-type-rodstv-translations.store',
                $refTypeRodstv
            ),
            $data
        );

        unset($data['ref_type_rodstv_id']);
        unset($data['locale']);
        unset($data['name']);

        $this->assertDatabaseHas('ref_type_rodstv_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $refTypeRodstvTranslation = RefTypeRodstvTranslation::latest(
            'id'
        )->first();

        $this->assertEquals(
            $refTypeRodstv->id,
            $refTypeRodstvTranslation->ref_type_rodstv_id
        );
    }
}
