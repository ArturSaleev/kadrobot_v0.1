<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefVidHolidayType;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefVidHolidayTypeTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_vid_holiday_types_list()
    {
        $refVidHolidayTypes = RefVidHolidayType::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.ref-vid-holiday-types.index'));

        $response->assertOk()->assertSee($refVidHolidayTypes[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_vid_holiday_type()
    {
        $data = RefVidHolidayType::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(
            route('api.ref-vid-holiday-types.store'),
            $data
        );

        $this->assertDatabaseHas('ref_vid_holiday_types', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_ref_vid_holiday_type()
    {
        $refVidHolidayType = RefVidHolidayType::factory()->create();

        $data = [];

        $response = $this->putJson(
            route('api.ref-vid-holiday-types.update', $refVidHolidayType),
            $data
        );

        $data['id'] = $refVidHolidayType->id;

        $this->assertDatabaseHas('ref_vid_holiday_types', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_vid_holiday_type()
    {
        $refVidHolidayType = RefVidHolidayType::factory()->create();

        $response = $this->deleteJson(
            route('api.ref-vid-holiday-types.destroy', $refVidHolidayType)
        );

        $this->assertSoftDeleted($refVidHolidayType);

        $response->assertNoContent();
    }
}
