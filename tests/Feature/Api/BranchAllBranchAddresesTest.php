<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Branch;
use App\Models\BranchAddreses;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BranchAllBranchAddresesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_branch_all_branch_addreses()
    {
        $branch = Branch::factory()->create();
        $allBranchAddreses = BranchAddreses::factory()
            ->count(2)
            ->create([
                'branch_id' => $branch->id,
            ]);

        $response = $this->getJson(
            route('api.branches.all-branch-addreses.index', $branch)
        );

        $response->assertOk()->assertSee($allBranchAddreses[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_branch_all_branch_addreses()
    {
        $branch = Branch::factory()->create();
        $data = BranchAddreses::factory()
            ->make([
                'branch_id' => $branch->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.branches.all-branch-addreses.store', $branch),
            $data
        );

        $this->assertDatabaseHas('branch_addreses', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $branchAddreses = BranchAddreses::latest('id')->first();

        $this->assertEquals($branch->id, $branchAddreses->branch_id);
    }
}
