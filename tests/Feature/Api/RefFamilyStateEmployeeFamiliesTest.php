<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefFamilyState;
use App\Models\EmployeeFamily;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefFamilyStateEmployeeFamiliesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_family_state_employee_families()
    {
        $refFamilyState = RefFamilyState::factory()->create();
        $employeeFamilies = EmployeeFamily::factory()
            ->count(2)
            ->create([
                'ref_family_state_id' => $refFamilyState->id,
            ]);

        $response = $this->getJson(
            route(
                'api.ref-family-states.employee-families.index',
                $refFamilyState
            )
        );

        $response->assertOk()->assertSee($employeeFamilies[0]->lastname);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_family_state_employee_families()
    {
        $refFamilyState = RefFamilyState::factory()->create();
        $data = EmployeeFamily::factory()
            ->make([
                'ref_family_state_id' => $refFamilyState->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.ref-family-states.employee-families.store',
                $refFamilyState
            ),
            $data
        );

        $this->assertDatabaseHas('employee_families', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employeeFamily = EmployeeFamily::latest('id')->first();

        $this->assertEquals(
            $refFamilyState->id,
            $employeeFamily->ref_family_state_id
        );
    }
}
