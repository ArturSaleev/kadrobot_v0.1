<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Employee;
use App\Models\EmployeeInvalide;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeEmployeeInvalidesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_employee_invalides()
    {
        $employee = Employee::factory()->create();
        $employeeInvalides = EmployeeInvalide::factory()
            ->count(2)
            ->create([
                'employee_id' => $employee->id,
            ]);

        $response = $this->getJson(
            route('api.employees.employee-invalides.index', $employee)
        );

        $response->assertOk()->assertSee($employeeInvalides[0]->num);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_employee_invalides()
    {
        $employee = Employee::factory()->create();
        $data = EmployeeInvalide::factory()
            ->make([
                'employee_id' => $employee->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.employees.employee-invalides.store', $employee),
            $data
        );

        $this->assertDatabaseHas('employee_invalides', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employeeInvalide = EmployeeInvalide::latest('id')->first();

        $this->assertEquals($employee->id, $employeeInvalide->employee_id);
    }
}
