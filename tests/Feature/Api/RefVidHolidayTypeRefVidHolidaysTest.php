<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefVidHoliday;
use App\Models\RefVidHolidayType;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefVidHolidayTypeRefVidHolidaysTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_vid_holiday_type_ref_vid_holidays()
    {
        $refVidHolidayType = RefVidHolidayType::factory()->create();
        $refVidHolidays = RefVidHoliday::factory()
            ->count(2)
            ->create([
                'ref_vid_holiday_type_id' => $refVidHolidayType->id,
            ]);

        $response = $this->getJson(
            route(
                'api.ref-vid-holiday-types.ref-vid-holidays.index',
                $refVidHolidayType
            )
        );

        $response->assertOk()->assertSee($refVidHolidays[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_vid_holiday_type_ref_vid_holidays()
    {
        $refVidHolidayType = RefVidHolidayType::factory()->create();
        $data = RefVidHoliday::factory()
            ->make([
                'ref_vid_holiday_type_id' => $refVidHolidayType->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.ref-vid-holiday-types.ref-vid-holidays.store',
                $refVidHolidayType
            ),
            $data
        );

        $this->assertDatabaseHas('ref_vid_holidays', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $refVidHoliday = RefVidHoliday::latest('id')->first();

        $this->assertEquals(
            $refVidHolidayType->id,
            $refVidHoliday->ref_vid_holiday_type_id
        );
    }
}
