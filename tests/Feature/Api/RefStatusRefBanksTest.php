<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefBank;
use App\Models\RefStatus;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefStatusRefBanksTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_status_ref_banks()
    {
        $refStatus = RefStatus::factory()->create();
        $refBanks = RefBank::factory()
            ->count(2)
            ->create([
                'ref_status_id' => $refStatus->id,
            ]);

        $response = $this->getJson(
            route('api.ref-statuses.ref-banks.index', $refStatus)
        );

        $response->assertOk()->assertSee($refBanks[0]->mfo);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_status_ref_banks()
    {
        $refStatus = RefStatus::factory()->create();
        $data = RefBank::factory()
            ->make([
                'ref_status_id' => $refStatus->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.ref-statuses.ref-banks.store', $refStatus),
            $data
        );

        $this->assertDatabaseHas('ref_banks', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $refBank = RefBank::latest('id')->first();

        $this->assertEquals($refStatus->id, $refBank->ref_status_id);
    }
}
