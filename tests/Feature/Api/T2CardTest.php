<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\T2Card;

use App\Models\Branch;
use App\Models\Company;
use App\Models\Employee;
use App\Models\Position;
use App\Models\RefAction;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class T2CardTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_t2_cards_list()
    {
        $t2Cards = T2Card::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.t2-cards.index'));

        $response->assertOk()->assertSee($t2Cards[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_t2_card()
    {
        $data = T2Card::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.t2-cards.store'), $data);

        $this->assertDatabaseHas('t2_cards', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_t2_card()
    {
        $t2Card = T2Card::factory()->create();

        $company = Company::factory()->create();
        $employee = Employee::factory()->create();
        $branch = Branch::factory()->create();
        $refAction = RefAction::factory()->create();
        $position = Position::factory()->create();

        $data = [
            'company_id' => $company->id,
            'employee_id' => $employee->id,
            'ref_branch_id' => $branch->id,
            'ref_action_id' => $refAction->id,
            'ref_position_id' => $position->id,
        ];

        $response = $this->putJson(
            route('api.t2-cards.update', $t2Card),
            $data
        );

        $data['id'] = $t2Card->id;

        $this->assertDatabaseHas('t2_cards', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_t2_card()
    {
        $t2Card = T2Card::factory()->create();

        $response = $this->deleteJson(route('api.t2-cards.destroy', $t2Card));

        $this->assertSoftDeleted($t2Card);

        $response->assertNoContent();
    }
}
