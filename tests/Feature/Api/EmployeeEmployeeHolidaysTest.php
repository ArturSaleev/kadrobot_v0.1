<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Employee;
use App\Models\EmployeeHoliday;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeEmployeeHolidaysTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_employee_holidays()
    {
        $employee = Employee::factory()->create();
        $employeeHolidays = EmployeeHoliday::factory()
            ->count(2)
            ->create([
                'employee_id' => $employee->id,
            ]);

        $response = $this->getJson(
            route('api.employees.employee-holidays.index', $employee)
        );

        $response->assertOk()->assertSee($employeeHolidays[0]->date_begin);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_employee_holidays()
    {
        $employee = Employee::factory()->create();
        $data = EmployeeHoliday::factory()
            ->make([
                'employee_id' => $employee->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.employees.employee-holidays.store', $employee),
            $data
        );

        $this->assertDatabaseHas('employee_holidays', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employeeHoliday = EmployeeHoliday::latest('id')->first();

        $this->assertEquals($employee->id, $employeeHoliday->employee_id);
    }
}
