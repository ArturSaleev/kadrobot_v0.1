<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefDocType;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefDocTypeTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_doc_types_list()
    {
        $refDocTypes = RefDocType::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.ref-doc-types.index'));

        $response->assertOk()->assertSee($refDocTypes[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_doc_type()
    {
        $data = RefDocType::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.ref-doc-types.store'), $data);

        $this->assertDatabaseHas('ref_doc_types', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_ref_doc_type()
    {
        $refDocType = RefDocType::factory()->create();

        $data = [];

        $response = $this->putJson(
            route('api.ref-doc-types.update', $refDocType),
            $data
        );

        $data['id'] = $refDocType->id;

        $this->assertDatabaseHas('ref_doc_types', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_doc_type()
    {
        $refDocType = RefDocType::factory()->create();

        $response = $this->deleteJson(
            route('api.ref-doc-types.destroy', $refDocType)
        );

        $this->assertSoftDeleted($refDocType);

        $response->assertNoContent();
    }
}
