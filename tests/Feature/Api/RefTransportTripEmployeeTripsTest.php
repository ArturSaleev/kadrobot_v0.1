<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\EmployeeTrip;
use App\Models\RefTransportTrip;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefTransportTripEmployeeTripsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_transport_trip_employee_trips()
    {
        $refTransportTrip = RefTransportTrip::factory()->create();
        $employeeTrips = EmployeeTrip::factory()
            ->count(2)
            ->create([
                'ref_transport_trip_id' => $refTransportTrip->id,
            ]);

        $response = $this->getJson(
            route(
                'api.ref-transport-trips.employee-trips.index',
                $refTransportTrip
            )
        );

        $response->assertOk()->assertSee($employeeTrips[0]->date_begin);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_transport_trip_employee_trips()
    {
        $refTransportTrip = RefTransportTrip::factory()->create();
        $data = EmployeeTrip::factory()
            ->make([
                'ref_transport_trip_id' => $refTransportTrip->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.ref-transport-trips.employee-trips.store',
                $refTransportTrip
            ),
            $data
        );

        $this->assertDatabaseHas('employee_trips', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employeeTrip = EmployeeTrip::latest('id')->first();

        $this->assertEquals(
            $refTransportTrip->id,
            $employeeTrip->ref_transport_trip_id
        );
    }
}
