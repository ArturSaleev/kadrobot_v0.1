<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\EmployeeHoliday;

use App\Models\Employee;
use App\Models\RefVidHoliday;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeHolidayTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_holidays_list()
    {
        $employeeHolidays = EmployeeHoliday::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.employee-holidays.index'));

        $response->assertOk()->assertSee($employeeHolidays[0]->date_begin);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_holiday()
    {
        $data = EmployeeHoliday::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(
            route('api.employee-holidays.store'),
            $data
        );

        $this->assertDatabaseHas('employee_holidays', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_employee_holiday()
    {
        $employeeHoliday = EmployeeHoliday::factory()->create();

        $employee = Employee::factory()->create();
        $refVidHoliday = RefVidHoliday::factory()->create();

        $data = [
            'date_begin' => $this->faker->date,
            'date_end' => $this->faker->date,
            'cnt_days' => $this->faker->randomNumber(0),
            'period_begin' => $this->faker->date,
            'period_end' => $this->faker->date,
            'order_num' => $this->faker->text(255),
            'order_date' => $this->faker->date,
            'deligate' => $this->faker->randomNumber(0),
            'deligate_emp_id' => $this->faker->randomNumber,
            'doc_content' => $this->faker->text,
            'employee_id' => $employee->id,
            'ref_vid_holiday_id' => $refVidHoliday->id,
        ];

        $response = $this->putJson(
            route('api.employee-holidays.update', $employeeHoliday),
            $data
        );

        $data['id'] = $employeeHoliday->id;

        $this->assertDatabaseHas('employee_holidays', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_holiday()
    {
        $employeeHoliday = EmployeeHoliday::factory()->create();

        $response = $this->deleteJson(
            route('api.employee-holidays.destroy', $employeeHoliday)
        );

        $this->assertSoftDeleted($employeeHoliday);

        $response->assertNoContent();
    }
}
