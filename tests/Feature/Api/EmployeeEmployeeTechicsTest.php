<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Employee;
use App\Models\EmployeeTechic;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeEmployeeTechicsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_employee_techics()
    {
        $employee = Employee::factory()->create();
        $employeeTechics = EmployeeTechic::factory()
            ->count(2)
            ->create([
                'employee_id' => $employee->id,
            ]);

        $response = $this->getJson(
            route('api.employees.employee-techics.index', $employee)
        );

        $response->assertOk()->assertSee($employeeTechics[0]->invent_num);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_employee_techics()
    {
        $employee = Employee::factory()->create();
        $data = EmployeeTechic::factory()
            ->make([
                'employee_id' => $employee->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.employees.employee-techics.store', $employee),
            $data
        );

        $this->assertDatabaseHas('employee_techics', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employeeTechic = EmployeeTechic::latest('id')->first();

        $this->assertEquals($employee->id, $employeeTechic->employee_id);
    }
}
