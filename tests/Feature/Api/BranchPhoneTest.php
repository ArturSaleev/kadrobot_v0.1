<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\BranchPhone;

use App\Models\Branch;
use App\Models\RefTypePhone;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BranchPhoneTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_branch_phones_list()
    {
        $branchPhones = BranchPhone::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.branch-phones.index'));

        $response->assertOk()->assertSee($branchPhones[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_branch_phone()
    {
        $data = BranchPhone::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.branch-phones.store'), $data);

        $this->assertDatabaseHas('branch_phones', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_branch_phone()
    {
        $branchPhone = BranchPhone::factory()->create();

        $branch = Branch::factory()->create();
        $refTypePhone = RefTypePhone::factory()->create();

        $data = [
            'name' => $this->faker->name,
            'branch_id' => $branch->id,
            'ref_type_phone_id' => $refTypePhone->id,
        ];

        $response = $this->putJson(
            route('api.branch-phones.update', $branchPhone),
            $data
        );

        $data['id'] = $branchPhone->id;

        $this->assertDatabaseHas('branch_phones', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_branch_phone()
    {
        $branchPhone = BranchPhone::factory()->create();

        $response = $this->deleteJson(
            route('api.branch-phones.destroy', $branchPhone)
        );

        $this->assertModelMissing($branchPhone);

        $response->assertNoContent();
    }
}
