<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefMeta;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefMetaTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_metas_list()
    {
        $refMetas = RefMeta::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.ref-metas.index'));

        $response->assertOk()->assertSee($refMetas[0]->title);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_meta()
    {
        $data = RefMeta::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.ref-metas.store'), $data);

        $this->assertDatabaseHas('ref_metas', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_ref_meta()
    {
        $refMeta = RefMeta::factory()->create();

        $data = [
            'title' => $this->faker->text(255),
            'name' => $this->faker->name,
            'tablename' => $this->faker->text(255),
            'column_name' => $this->faker->text(255),
        ];

        $response = $this->putJson(
            route('api.ref-metas.update', $refMeta),
            $data
        );

        $data['id'] = $refMeta->id;

        $this->assertDatabaseHas('ref_metas', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_meta()
    {
        $refMeta = RefMeta::factory()->create();

        $response = $this->deleteJson(route('api.ref-metas.destroy', $refMeta));

        $this->assertSoftDeleted($refMeta);

        $response->assertNoContent();
    }
}
