<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Curator;
use App\Models\Department;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DepartmentCuratorsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_department_curators()
    {
        $department = Department::factory()->create();
        $curators = Curator::factory()
            ->count(2)
            ->create([
                'department_id' => $department->id,
            ]);

        $response = $this->getJson(
            route('api.departments.curators.index', $department)
        );

        $response->assertOk()->assertSee($curators[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_department_curators()
    {
        $department = Department::factory()->create();
        $data = Curator::factory()
            ->make([
                'department_id' => $department->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.departments.curators.store', $department),
            $data
        );

        $this->assertDatabaseHas('curators', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $curator = Curator::latest('id')->first();

        $this->assertEquals($department->id, $curator->department_id);
    }
}
