<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefAction;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefActionTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_actions_list()
    {
        $refActions = RefAction::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.ref-actions.index'));

        $response->assertOk()->assertSee($refActions[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_action()
    {
        $data = RefAction::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.ref-actions.store'), $data);

        $this->assertDatabaseHas('ref_actions', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_ref_action()
    {
        $refAction = RefAction::factory()->create();

        $data = [
            'act_type' => $this->faker->randomNumber(0),
        ];

        $response = $this->putJson(
            route('api.ref-actions.update', $refAction),
            $data
        );

        $data['id'] = $refAction->id;

        $this->assertDatabaseHas('ref_actions', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_action()
    {
        $refAction = RefAction::factory()->create();

        $response = $this->deleteJson(
            route('api.ref-actions.destroy', $refAction)
        );

        $this->assertSoftDeleted($refAction);

        $response->assertNoContent();
    }
}
