<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Employee;
use App\Models\EmployeeEducation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeEmployeeEducationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_employee_educations()
    {
        $employee = Employee::factory()->create();
        $employeeEducations = EmployeeEducation::factory()
            ->count(2)
            ->create([
                'employee_id' => $employee->id,
            ]);

        $response = $this->getJson(
            route('api.employees.employee-educations.index', $employee)
        );

        $response->assertOk()->assertSee($employeeEducations[0]->institution);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_employee_educations()
    {
        $employee = Employee::factory()->create();
        $data = EmployeeEducation::factory()
            ->make([
                'employee_id' => $employee->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.employees.employee-educations.store', $employee),
            $data
        );

        $this->assertDatabaseHas('employee_educations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employeeEducation = EmployeeEducation::latest('id')->first();

        $this->assertEquals($employee->id, $employeeEducation->employee_id);
    }
}
