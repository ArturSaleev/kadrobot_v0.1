<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefOked;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefOkedTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_okeds_list()
    {
        $refOkeds = RefOked::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.ref-okeds.index'));

        $response->assertOk()->assertSee($refOkeds[0]->oked);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_oked()
    {
        $data = RefOked::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.ref-okeds.store'), $data);

        $this->assertDatabaseHas('ref_okeds', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_ref_oked()
    {
        $refOked = RefOked::factory()->create();

        $data = [
            'oked' => $this->faker->unique->text(255),
        ];

        $response = $this->putJson(
            route('api.ref-okeds.update', $refOked),
            $data
        );

        $data['id'] = $refOked->id;

        $this->assertDatabaseHas('ref_okeds', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_oked()
    {
        $refOked = RefOked::factory()->create();

        $response = $this->deleteJson(route('api.ref-okeds.destroy', $refOked));

        $this->assertSoftDeleted($refOked);

        $response->assertNoContent();
    }
}
