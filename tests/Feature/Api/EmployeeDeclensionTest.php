<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\EmployeeDeclension;

use App\Models\Employee;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeDeclensionTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_declensions_list()
    {
        $employeeDeclensions = EmployeeDeclension::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.employee-declensions.index'));

        $response->assertOk()->assertSee($employeeDeclensions[0]->locale);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_declension()
    {
        $data = EmployeeDeclension::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(
            route('api.employee-declensions.store'),
            $data
        );

        unset($data['type_description']);

        $this->assertDatabaseHas('employee_declensions', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_employee_declension()
    {
        $employeeDeclension = EmployeeDeclension::factory()->create();

        $employee = Employee::factory()->create();

        $data = [
            'locale' => $this->faker->locale,
            'lastname' => $this->faker->lastName,
            'firstname' => $this->faker->text(255),
            'middlename' => $this->faker->text(255),
            'case_type' => $this->faker->text(255),
            'type_description' => $this->faker->text(255),
            'employee_id' => $employee->id,
        ];

        $response = $this->putJson(
            route('api.employee-declensions.update', $employeeDeclension),
            $data
        );

        unset($data['type_description']);

        $data['id'] = $employeeDeclension->id;

        $this->assertDatabaseHas('employee_declensions', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_declension()
    {
        $employeeDeclension = EmployeeDeclension::factory()->create();

        $response = $this->deleteJson(
            route('api.employee-declensions.destroy', $employeeDeclension)
        );

        $this->assertModelMissing($employeeDeclension);

        $response->assertNoContent();
    }
}
