<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\DepartmentPhone;

use App\Models\Department;
use App\Models\RefTypePhone;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DepartmentPhoneTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_department_phones_list()
    {
        $departmentPhones = DepartmentPhone::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.department-phones.index'));

        $response->assertOk()->assertSee($departmentPhones[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_department_phone()
    {
        $data = DepartmentPhone::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(
            route('api.department-phones.store'),
            $data
        );

        $this->assertDatabaseHas('department_phones', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_department_phone()
    {
        $departmentPhone = DepartmentPhone::factory()->create();

        $department = Department::factory()->create();
        $refTypePhone = RefTypePhone::factory()->create();

        $data = [
            'name' => $this->faker->name,
            'department_id' => $department->id,
            'ref_type_phone_id' => $refTypePhone->id,
        ];

        $response = $this->putJson(
            route('api.department-phones.update', $departmentPhone),
            $data
        );

        $data['id'] = $departmentPhone->id;

        $this->assertDatabaseHas('department_phones', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_department_phone()
    {
        $departmentPhone = DepartmentPhone::factory()->create();

        $response = $this->deleteJson(
            route('api.department-phones.destroy', $departmentPhone)
        );

        $this->assertModelMissing($departmentPhone);

        $response->assertNoContent();
    }
}
