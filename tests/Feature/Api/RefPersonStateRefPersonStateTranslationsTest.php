<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefPersonState;
use App\Models\RefPersonStateTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefPersonStateRefPersonStateTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_person_state_ref_person_state_translations()
    {
        $refPersonState = RefPersonState::factory()->create();
        $refPersonStateTranslations = RefPersonStateTranslation::factory()
            ->count(2)
            ->create([
                'ref_person_state_id' => $refPersonState->id,
            ]);

        $response = $this->getJson(
            route(
                'api.ref-person-states.ref-person-state-translations.index',
                $refPersonState
            )
        );

        $response->assertOk()->assertSee($refPersonStateTranslations[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_person_state_ref_person_state_translations()
    {
        $refPersonState = RefPersonState::factory()->create();
        $data = RefPersonStateTranslation::factory()
            ->make([
                'ref_person_state_id' => $refPersonState->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.ref-person-states.ref-person-state-translations.store',
                $refPersonState
            ),
            $data
        );

        unset($data['ref_person_state_id']);
        unset($data['locale']);
        unset($data['name']);

        $this->assertDatabaseHas('ref_person_state_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $refPersonStateTranslation = RefPersonStateTranslation::latest(
            'id'
        )->first();

        $this->assertEquals(
            $refPersonState->id,
            $refPersonStateTranslation->ref_person_state_id
        );
    }
}
