<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\EmployeeTripFromTo;

use App\Models\EmployeeTrip;
use App\Models\RefTransportTrip;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeTripFromToTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_trip_from_tos_list()
    {
        $employeeTripFromTos = EmployeeTripFromTo::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.employee-trip-from-tos.index'));

        $response->assertOk()->assertSee($employeeTripFromTos[0]->from_place);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_trip_from_to()
    {
        $data = EmployeeTripFromTo::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(
            route('api.employee-trip-from-tos.store'),
            $data
        );

        $this->assertDatabaseHas('employee_trip_from_tos', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_employee_trip_from_to()
    {
        $employeeTripFromTo = EmployeeTripFromTo::factory()->create();

        $employeeTrip = EmployeeTrip::factory()->create();
        $refTransportTrip = RefTransportTrip::factory()->create();

        $data = [
            'from_place' => $this->faker->text,
            'to_place' => $this->faker->text,
            'employee_trip_id' => $employeeTrip->id,
            'ref_transport_trip_id' => $refTransportTrip->id,
        ];

        $response = $this->putJson(
            route('api.employee-trip-from-tos.update', $employeeTripFromTo),
            $data
        );

        $data['id'] = $employeeTripFromTo->id;

        $this->assertDatabaseHas('employee_trip_from_tos', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_trip_from_to()
    {
        $employeeTripFromTo = EmployeeTripFromTo::factory()->create();

        $response = $this->deleteJson(
            route('api.employee-trip-from-tos.destroy', $employeeTripFromTo)
        );

        $this->assertSoftDeleted($employeeTripFromTo);

        $response->assertNoContent();
    }
}
