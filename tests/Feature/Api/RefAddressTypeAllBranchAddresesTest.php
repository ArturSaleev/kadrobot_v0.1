<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefAddressType;
use App\Models\BranchAddreses;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefAddressTypeAllBranchAddresesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_address_type_all_branch_addreses()
    {
        $refAddressType = RefAddressType::factory()->create();
        $allBranchAddreses = BranchAddreses::factory()
            ->count(2)
            ->create([
                'ref_address_type_id' => $refAddressType->id,
            ]);

        $response = $this->getJson(
            route(
                'api.ref-address-types.all-branch-addreses.index',
                $refAddressType
            )
        );

        $response->assertOk()->assertSee($allBranchAddreses[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_address_type_all_branch_addreses()
    {
        $refAddressType = RefAddressType::factory()->create();
        $data = BranchAddreses::factory()
            ->make([
                'ref_address_type_id' => $refAddressType->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.ref-address-types.all-branch-addreses.store',
                $refAddressType
            ),
            $data
        );

        $this->assertDatabaseHas('branch_addreses', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $branchAddreses = BranchAddreses::latest('id')->first();

        $this->assertEquals(
            $refAddressType->id,
            $branchAddreses->ref_address_type_id
        );
    }
}
