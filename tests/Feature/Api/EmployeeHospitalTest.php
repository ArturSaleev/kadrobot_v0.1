<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\EmployeeHospital;

use App\Models\Employee;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeHospitalTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_hospitals_list()
    {
        $employeeHospitals = EmployeeHospital::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.employee-hospitals.index'));

        $response->assertOk()->assertSee($employeeHospitals[0]->date_begin);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_hospital()
    {
        $data = EmployeeHospital::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(
            route('api.employee-hospitals.store'),
            $data
        );

        $this->assertDatabaseHas('employee_hospitals', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_employee_hospital()
    {
        $employeeHospital = EmployeeHospital::factory()->create();

        $employee = Employee::factory()->create();

        $data = [
            'date_begin' => $this->faker->date,
            'date_end' => $this->faker->date,
            'cnt_days' => $this->faker->randomNumber(0),
            'employee_id' => $employee->id,
        ];

        $response = $this->putJson(
            route('api.employee-hospitals.update', $employeeHospital),
            $data
        );

        $data['id'] = $employeeHospital->id;

        $this->assertDatabaseHas('employee_hospitals', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_hospital()
    {
        $employeeHospital = EmployeeHospital::factory()->create();

        $response = $this->deleteJson(
            route('api.employee-hospitals.destroy', $employeeHospital)
        );

        $this->assertSoftDeleted($employeeHospital);

        $response->assertNoContent();
    }
}
