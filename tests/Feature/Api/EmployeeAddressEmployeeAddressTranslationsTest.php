<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\EmployeeAddress;
use App\Models\EmployeeAddressTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeAddressEmployeeAddressTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_address_employee_address_translations()
    {
        $employeeAddress = EmployeeAddress::factory()->create();
        $employeeAddressTranslations = EmployeeAddressTranslation::factory()
            ->count(2)
            ->create([
                'employee_address_id' => $employeeAddress->id,
            ]);

        $response = $this->getJson(
            route(
                'api.employee-addresses.employee-address-translations.index',
                $employeeAddress
            )
        );

        $response->assertOk()->assertSee($employeeAddressTranslations[0]->id);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_address_employee_address_translations()
    {
        $employeeAddress = EmployeeAddress::factory()->create();
        $data = EmployeeAddressTranslation::factory()
            ->make([
                'employee_address_id' => $employeeAddress->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.employee-addresses.employee-address-translations.store',
                $employeeAddress
            ),
            $data
        );

        unset($data['employee_address_id']);

        $this->assertDatabaseHas('employee_address_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employeeAddressTranslation = EmployeeAddressTranslation::latest(
            'id'
        )->first();

        $this->assertEquals(
            $employeeAddress->id,
            $employeeAddressTranslation->employee_address_id
        );
    }
}
