<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Employee;
use App\Models\EmployeeDocument;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeEmployeeDocumentsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_employee_documents()
    {
        $employee = Employee::factory()->create();
        $employeeDocuments = EmployeeDocument::factory()
            ->count(2)
            ->create([
                'employee_id' => $employee->id,
            ]);

        $response = $this->getJson(
            route('api.employees.employee-documents.index', $employee)
        );

        $response->assertOk()->assertSee($employeeDocuments[0]->doc_seria);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_employee_documents()
    {
        $employee = Employee::factory()->create();
        $data = EmployeeDocument::factory()
            ->make([
                'employee_id' => $employee->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.employees.employee-documents.store', $employee),
            $data
        );

        $this->assertDatabaseHas('employee_documents', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employeeDocument = EmployeeDocument::latest('id')->first();

        $this->assertEquals($employee->id, $employeeDocument->employee_id);
    }
}
