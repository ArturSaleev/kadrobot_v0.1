<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Employee;
use App\Models\EmployeeAddress;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeEmployeeAddressesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_employee_addresses()
    {
        $employee = Employee::factory()->create();
        $employeeAddresses = EmployeeAddress::factory()
            ->count(2)
            ->create([
                'employee_id' => $employee->id,
            ]);

        $response = $this->getJson(
            route('api.employees.employee-addresses.index', $employee)
        );

        $response->assertOk()->assertSee($employeeAddresses[0]->locale);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_employee_addresses()
    {
        $employee = Employee::factory()->create();
        $data = EmployeeAddress::factory()
            ->make([
                'employee_id' => $employee->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.employees.employee-addresses.store', $employee),
            $data
        );

        $this->assertDatabaseHas('employee_addresses', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employeeAddress = EmployeeAddress::latest('id')->first();

        $this->assertEquals($employee->id, $employeeAddress->employee_id);
    }
}
