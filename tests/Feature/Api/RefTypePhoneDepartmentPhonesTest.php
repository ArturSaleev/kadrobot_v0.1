<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefTypePhone;
use App\Models\DepartmentPhone;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefTypePhoneDepartmentPhonesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_type_phone_department_phones()
    {
        $refTypePhone = RefTypePhone::factory()->create();
        $departmentPhones = DepartmentPhone::factory()
            ->count(2)
            ->create([
                'ref_type_phone_id' => $refTypePhone->id,
            ]);

        $response = $this->getJson(
            route('api.ref-type-phones.department-phones.index', $refTypePhone)
        );

        $response->assertOk()->assertSee($departmentPhones[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_type_phone_department_phones()
    {
        $refTypePhone = RefTypePhone::factory()->create();
        $data = DepartmentPhone::factory()
            ->make([
                'ref_type_phone_id' => $refTypePhone->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.ref-type-phones.department-phones.store', $refTypePhone),
            $data
        );

        $this->assertDatabaseHas('department_phones', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $departmentPhone = DepartmentPhone::latest('id')->first();

        $this->assertEquals(
            $refTypePhone->id,
            $departmentPhone->ref_type_phone_id
        );
    }
}
