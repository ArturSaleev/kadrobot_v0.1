<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefTechnicType;
use App\Models\EmployeeTechic;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefTechnicTypeEmployeeTechicsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_technic_type_employee_techics()
    {
        $refTechnicType = RefTechnicType::factory()->create();
        $employeeTechics = EmployeeTechic::factory()
            ->count(2)
            ->create([
                'ref_technic_type_id' => $refTechnicType->id,
            ]);

        $response = $this->getJson(
            route(
                'api.ref-technic-types.employee-techics.index',
                $refTechnicType
            )
        );

        $response->assertOk()->assertSee($employeeTechics[0]->invent_num);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_technic_type_employee_techics()
    {
        $refTechnicType = RefTechnicType::factory()->create();
        $data = EmployeeTechic::factory()
            ->make([
                'ref_technic_type_id' => $refTechnicType->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.ref-technic-types.employee-techics.store',
                $refTechnicType
            ),
            $data
        );

        $this->assertDatabaseHas('employee_techics', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $employeeTechic = EmployeeTechic::latest('id')->first();

        $this->assertEquals(
            $refTechnicType->id,
            $employeeTechic->ref_technic_type_id
        );
    }
}
