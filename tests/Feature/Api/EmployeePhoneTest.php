<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\EmployeePhone;

use App\Models\Employee;
use App\Models\RefTypePhone;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeePhoneTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_employee_phones_list()
    {
        $employeePhones = EmployeePhone::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.employee-phones.index'));

        $response->assertOk()->assertSee($employeePhones[0]->phone);
    }

    /**
     * @test
     */
    public function it_stores_the_employee_phone()
    {
        $data = EmployeePhone::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.employee-phones.store'), $data);

        $this->assertDatabaseHas('employee_phones', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_employee_phone()
    {
        $employeePhone = EmployeePhone::factory()->create();

        $refTypePhone = RefTypePhone::factory()->create();
        $employee = Employee::factory()->create();

        $data = [
            'phone' => $this->faker->phoneNumber,
            'ref_type_phone_id' => $refTypePhone->id,
            'employee_id' => $employee->id,
        ];

        $response = $this->putJson(
            route('api.employee-phones.update', $employeePhone),
            $data
        );

        $data['id'] = $employeePhone->id;

        $this->assertDatabaseHas('employee_phones', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_phone()
    {
        $employeePhone = EmployeePhone::factory()->create();

        $response = $this->deleteJson(
            route('api.employee-phones.destroy', $employeePhone)
        );

        $this->assertModelMissing($employeePhone);

        $response->assertNoContent();
    }
}
