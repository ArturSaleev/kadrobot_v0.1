<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\RefDocPlace;
use App\Models\RefDocPlaceTranslation;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefDocPlaceRefDocPlaceTranslationsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_ref_doc_place_ref_doc_place_translations()
    {
        $refDocPlace = RefDocPlace::factory()->create();
        $refDocPlaceTranslations = RefDocPlaceTranslation::factory()
            ->count(2)
            ->create([
                'ref_doc_place_id' => $refDocPlace->id,
            ]);

        $response = $this->getJson(
            route(
                'api.ref-doc-places.ref-doc-place-translations.index',
                $refDocPlace
            )
        );

        $response->assertOk()->assertSee($refDocPlaceTranslations[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_ref_doc_place_ref_doc_place_translations()
    {
        $refDocPlace = RefDocPlace::factory()->create();
        $data = RefDocPlaceTranslation::factory()
            ->make([
                'ref_doc_place_id' => $refDocPlace->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route(
                'api.ref-doc-places.ref-doc-place-translations.store',
                $refDocPlace
            ),
            $data
        );

        unset($data['ref_doc_place_id']);
        unset($data['locale']);
        unset($data['name']);

        $this->assertDatabaseHas('ref_doc_place_translations', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $refDocPlaceTranslation = RefDocPlaceTranslation::latest('id')->first();

        $this->assertEquals(
            $refDocPlace->id,
            $refDocPlaceTranslation->ref_doc_place_id
        );
    }
}
