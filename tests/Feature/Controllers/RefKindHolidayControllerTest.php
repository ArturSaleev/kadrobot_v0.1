<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\RefKindHoliday;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefKindHolidayControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_ref_kind_holidays()
    {
        $refKindHolidays = RefKindHoliday::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('ref-kind-holidays.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_kind_holidays.index')
            ->assertViewHas('refKindHolidays');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_ref_kind_holiday()
    {
        $response = $this->get(route('ref-kind-holidays.create'));

        $response->assertOk()->assertViewIs('app.ref_kind_holidays.create');
    }

    /**
     * @test
     */
    public function it_stores_the_ref_kind_holiday()
    {
        $data = RefKindHoliday::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('ref-kind-holidays.store'), $data);

        $this->assertDatabaseHas('ref_kind_holidays', $data);

        $refKindHoliday = RefKindHoliday::latest('id')->first();

        $response->assertRedirect(
            route('ref-kind-holidays.edit', $refKindHoliday)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_ref_kind_holiday()
    {
        $refKindHoliday = RefKindHoliday::factory()->create();

        $response = $this->get(
            route('ref-kind-holidays.show', $refKindHoliday)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.ref_kind_holidays.show')
            ->assertViewHas('refKindHoliday');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_ref_kind_holiday()
    {
        $refKindHoliday = RefKindHoliday::factory()->create();

        $response = $this->get(
            route('ref-kind-holidays.edit', $refKindHoliday)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.ref_kind_holidays.edit')
            ->assertViewHas('refKindHoliday');
    }

    /**
     * @test
     */
    public function it_updates_the_ref_kind_holiday()
    {
        $refKindHoliday = RefKindHoliday::factory()->create();

        $data = [];

        $response = $this->put(
            route('ref-kind-holidays.update', $refKindHoliday),
            $data
        );

        $data['id'] = $refKindHoliday->id;

        $this->assertDatabaseHas('ref_kind_holidays', $data);

        $response->assertRedirect(
            route('ref-kind-holidays.edit', $refKindHoliday)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_kind_holiday()
    {
        $refKindHoliday = RefKindHoliday::factory()->create();

        $response = $this->delete(
            route('ref-kind-holidays.destroy', $refKindHoliday)
        );

        $response->assertRedirect(route('ref-kind-holidays.index'));

        $this->assertSoftDeleted($refKindHoliday);
    }
}
