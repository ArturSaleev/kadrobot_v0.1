<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\Curator;

use App\Models\Employee;
use App\Models\Department;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CuratorControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_curators()
    {
        $curators = Curator::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('curators.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.curators.index')
            ->assertViewHas('curators');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_curator()
    {
        $response = $this->get(route('curators.create'));

        $response->assertOk()->assertViewIs('app.curators.create');
    }

    /**
     * @test
     */
    public function it_stores_the_curator()
    {
        $data = Curator::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('curators.store'), $data);

        $this->assertDatabaseHas('curators', $data);

        $curator = Curator::latest('id')->first();

        $response->assertRedirect(route('curators.edit', $curator));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_curator()
    {
        $curator = Curator::factory()->create();

        $response = $this->get(route('curators.show', $curator));

        $response
            ->assertOk()
            ->assertViewIs('app.curators.show')
            ->assertViewHas('curator');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_curator()
    {
        $curator = Curator::factory()->create();

        $response = $this->get(route('curators.edit', $curator));

        $response
            ->assertOk()
            ->assertViewIs('app.curators.edit')
            ->assertViewHas('curator');
    }

    /**
     * @test
     */
    public function it_updates_the_curator()
    {
        $curator = Curator::factory()->create();

        $employee = Employee::factory()->create();
        $department = Department::factory()->create();

        $data = [
            'employee_id' => $employee->id,
            'department_id' => $department->id,
        ];

        $response = $this->put(route('curators.update', $curator), $data);

        $data['id'] = $curator->id;

        $this->assertDatabaseHas('curators', $data);

        $response->assertRedirect(route('curators.edit', $curator));
    }

    /**
     * @test
     */
    public function it_deletes_the_curator()
    {
        $curator = Curator::factory()->create();

        $response = $this->delete(route('curators.destroy', $curator));

        $response->assertRedirect(route('curators.index'));

        $this->assertSoftDeleted($curator);
    }
}
