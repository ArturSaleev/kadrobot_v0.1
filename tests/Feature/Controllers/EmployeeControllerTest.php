<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\Employee;

use App\Models\RefSex;
use App\Models\Position;
use App\Models\RefUserStatus;
use App\Models\RefNationality;
use App\Models\RefFamilyState;
use App\Models\RefAccountType;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_employees()
    {
        $employees = Employee::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('employees.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.employees.index')
            ->assertViewHas('employees');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_employee()
    {
        $response = $this->get(route('employees.create'));

        $response->assertOk()->assertViewIs('app.employees.create');
    }

    /**
     * @test
     */
    public function it_stores_the_employee()
    {
        $data = Employee::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('employees.store'), $data);

        unset($data['reason_loyoff']);
        unset($data['position_id']);

        $this->assertDatabaseHas('employees', $data);

        $employee = Employee::latest('id')->first();

        $response->assertRedirect(route('employees.edit', $employee));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_employee()
    {
        $employee = Employee::factory()->create();

        $response = $this->get(route('employees.show', $employee));

        $response
            ->assertOk()
            ->assertViewIs('app.employees.show')
            ->assertViewHas('employee');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_employee()
    {
        $employee = Employee::factory()->create();

        $response = $this->get(route('employees.edit', $employee));

        $response
            ->assertOk()
            ->assertViewIs('app.employees.edit')
            ->assertViewHas('employee');
    }

    /**
     * @test
     */
    public function it_updates_the_employee()
    {
        $employee = Employee::factory()->create();

        $refSex = RefSex::factory()->create();
        $refNationality = RefNationality::factory()->create();
        $refFamilyState = RefFamilyState::factory()->create();
        $refAccountType = RefAccountType::factory()->create();
        $refUserStatus = RefUserStatus::factory()->create();
        $position = Position::factory()->create();

        $data = [
            'iin' => $this->faker->text(255),
            'date_post' => $this->faker->date,
            'date_loyoff' => $this->faker->date,
            'reason_loyoff' => $this->faker->text,
            'contract_num' => $this->faker->text(255),
            'contract_date' => $this->faker->date,
            'tab_num' => $this->faker->randomNumber(0),
            'birthday' => $this->faker->date,
            'birth_place' => $this->faker->text,
            'email' => $this->faker->email,
            'account' => $this->faker->text(255),
            'date_zav' => $this->faker->date,
            'oklad' => $this->faker->randomNumber(2),
            'gos_nagr' => $this->faker->text(255),
            'pens' => $this->faker->boolean,
            'pens_date' => $this->faker->date,
            'lgot' => $this->faker->text(255),
            'person_email' => $this->faker->text(255),
            'ref_sex_id' => $refSex->id,
            'ref_nationality_id' => $refNationality->id,
            'ref_family_state_id' => $refFamilyState->id,
            'ref_account_type_id' => $refAccountType->id,
            'ref_user_status_id' => $refUserStatus->id,
            'position_id' => $position->id,
        ];

        $response = $this->put(route('employees.update', $employee), $data);

        unset($data['reason_loyoff']);
        unset($data['position_id']);

        $data['id'] = $employee->id;

        $this->assertDatabaseHas('employees', $data);

        $response->assertRedirect(route('employees.edit', $employee));
    }

    /**
     * @test
     */
    public function it_deletes_the_employee()
    {
        $employee = Employee::factory()->create();

        $response = $this->delete(route('employees.destroy', $employee));

        $response->assertRedirect(route('employees.index'));

        $this->assertSoftDeleted($employee);
    }
}
