<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\RefFamilyState;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefFamilyStateControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_ref_family_states()
    {
        $refFamilyStates = RefFamilyState::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('ref-family-states.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_family_states.index')
            ->assertViewHas('refFamilyStates');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_ref_family_state()
    {
        $response = $this->get(route('ref-family-states.create'));

        $response->assertOk()->assertViewIs('app.ref_family_states.create');
    }

    /**
     * @test
     */
    public function it_stores_the_ref_family_state()
    {
        $data = RefFamilyState::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('ref-family-states.store'), $data);

        $this->assertDatabaseHas('ref_family_states', $data);

        $refFamilyState = RefFamilyState::latest('id')->first();

        $response->assertRedirect(
            route('ref-family-states.edit', $refFamilyState)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_ref_family_state()
    {
        $refFamilyState = RefFamilyState::factory()->create();

        $response = $this->get(
            route('ref-family-states.show', $refFamilyState)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.ref_family_states.show')
            ->assertViewHas('refFamilyState');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_ref_family_state()
    {
        $refFamilyState = RefFamilyState::factory()->create();

        $response = $this->get(
            route('ref-family-states.edit', $refFamilyState)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.ref_family_states.edit')
            ->assertViewHas('refFamilyState');
    }

    /**
     * @test
     */
    public function it_updates_the_ref_family_state()
    {
        $refFamilyState = RefFamilyState::factory()->create();

        $data = [];

        $response = $this->put(
            route('ref-family-states.update', $refFamilyState),
            $data
        );

        $data['id'] = $refFamilyState->id;

        $this->assertDatabaseHas('ref_family_states', $data);

        $response->assertRedirect(
            route('ref-family-states.edit', $refFamilyState)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_family_state()
    {
        $refFamilyState = RefFamilyState::factory()->create();

        $response = $this->delete(
            route('ref-family-states.destroy', $refFamilyState)
        );

        $response->assertRedirect(route('ref-family-states.index'));

        $this->assertSoftDeleted($refFamilyState);
    }
}
