<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\EmployeePhone;

use App\Models\Employee;
use App\Models\RefTypePhone;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeePhoneControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_employee_phones()
    {
        $employeePhones = EmployeePhone::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('employee-phones.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.employee_phones.index')
            ->assertViewHas('employeePhones');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_employee_phone()
    {
        $response = $this->get(route('employee-phones.create'));

        $response->assertOk()->assertViewIs('app.employee_phones.create');
    }

    /**
     * @test
     */
    public function it_stores_the_employee_phone()
    {
        $data = EmployeePhone::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('employee-phones.store'), $data);

        $this->assertDatabaseHas('employee_phones', $data);

        $employeePhone = EmployeePhone::latest('id')->first();

        $response->assertRedirect(
            route('employee-phones.edit', $employeePhone)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_employee_phone()
    {
        $employeePhone = EmployeePhone::factory()->create();

        $response = $this->get(route('employee-phones.show', $employeePhone));

        $response
            ->assertOk()
            ->assertViewIs('app.employee_phones.show')
            ->assertViewHas('employeePhone');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_employee_phone()
    {
        $employeePhone = EmployeePhone::factory()->create();

        $response = $this->get(route('employee-phones.edit', $employeePhone));

        $response
            ->assertOk()
            ->assertViewIs('app.employee_phones.edit')
            ->assertViewHas('employeePhone');
    }

    /**
     * @test
     */
    public function it_updates_the_employee_phone()
    {
        $employeePhone = EmployeePhone::factory()->create();

        $refTypePhone = RefTypePhone::factory()->create();
        $employee = Employee::factory()->create();

        $data = [
            'phone' => $this->faker->phoneNumber,
            'ref_type_phone_id' => $refTypePhone->id,
            'employee_id' => $employee->id,
        ];

        $response = $this->put(
            route('employee-phones.update', $employeePhone),
            $data
        );

        $data['id'] = $employeePhone->id;

        $this->assertDatabaseHas('employee_phones', $data);

        $response->assertRedirect(
            route('employee-phones.edit', $employeePhone)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_phone()
    {
        $employeePhone = EmployeePhone::factory()->create();

        $response = $this->delete(
            route('employee-phones.destroy', $employeePhone)
        );

        $response->assertRedirect(route('employee-phones.index'));

        $this->assertModelMissing($employeePhone);
    }
}
