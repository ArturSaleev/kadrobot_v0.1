<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\EmployeeHolidaysPeriod;

use App\Models\Employee;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeHolidaysPeriodControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_employee_holidays_periods()
    {
        $employeeHolidaysPeriods = EmployeeHolidaysPeriod::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('employee-holidays-periods.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.employee_holidays_periods.index')
            ->assertViewHas('employeeHolidaysPeriods');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_employee_holidays_period()
    {
        $response = $this->get(route('employee-holidays-periods.create'));

        $response
            ->assertOk()
            ->assertViewIs('app.employee_holidays_periods.create');
    }

    /**
     * @test
     */
    public function it_stores_the_employee_holidays_period()
    {
        $data = EmployeeHolidaysPeriod::factory()
            ->make()
            ->toArray();

        $response = $this->post(
            route('employee-holidays-periods.store'),
            $data
        );

        $this->assertDatabaseHas('employee_holidays_periods', $data);

        $employeeHolidaysPeriod = EmployeeHolidaysPeriod::latest('id')->first();

        $response->assertRedirect(
            route('employee-holidays-periods.edit', $employeeHolidaysPeriod)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_employee_holidays_period()
    {
        $employeeHolidaysPeriod = EmployeeHolidaysPeriod::factory()->create();

        $response = $this->get(
            route('employee-holidays-periods.show', $employeeHolidaysPeriod)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.employee_holidays_periods.show')
            ->assertViewHas('employeeHolidaysPeriod');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_employee_holidays_period()
    {
        $employeeHolidaysPeriod = EmployeeHolidaysPeriod::factory()->create();

        $response = $this->get(
            route('employee-holidays-periods.edit', $employeeHolidaysPeriod)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.employee_holidays_periods.edit')
            ->assertViewHas('employeeHolidaysPeriod');
    }

    /**
     * @test
     */
    public function it_updates_the_employee_holidays_period()
    {
        $employeeHolidaysPeriod = EmployeeHolidaysPeriod::factory()->create();

        $employee = Employee::factory()->create();

        $data = [
            'period_start' => $this->faker->date,
            'period_end' => $this->faker->date,
            'day_count_used_for_today' => $this->faker->randomNumber(0),
            'didnt_add' => $this->faker->randomNumber(0),
            'paying_for_health' => $this->faker->randomNumber(0),
            'employee_id' => $employee->id,
        ];

        $response = $this->put(
            route('employee-holidays-periods.update', $employeeHolidaysPeriod),
            $data
        );

        $data['id'] = $employeeHolidaysPeriod->id;

        $this->assertDatabaseHas('employee_holidays_periods', $data);

        $response->assertRedirect(
            route('employee-holidays-periods.edit', $employeeHolidaysPeriod)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_holidays_period()
    {
        $employeeHolidaysPeriod = EmployeeHolidaysPeriod::factory()->create();

        $response = $this->delete(
            route('employee-holidays-periods.destroy', $employeeHolidaysPeriod)
        );

        $response->assertRedirect(route('employee-holidays-periods.index'));

        $this->assertSoftDeleted($employeeHolidaysPeriod);
    }
}
