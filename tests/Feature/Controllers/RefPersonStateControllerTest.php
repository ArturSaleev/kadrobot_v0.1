<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\RefPersonState;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefPersonStateControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_ref_person_states()
    {
        $refPersonStates = RefPersonState::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('ref-person-states.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_person_states.index')
            ->assertViewHas('refPersonStates');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_ref_person_state()
    {
        $response = $this->get(route('ref-person-states.create'));

        $response->assertOk()->assertViewIs('app.ref_person_states.create');
    }

    /**
     * @test
     */
    public function it_stores_the_ref_person_state()
    {
        $data = RefPersonState::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('ref-person-states.store'), $data);

        $this->assertDatabaseHas('ref_person_states', $data);

        $refPersonState = RefPersonState::latest('id')->first();

        $response->assertRedirect(
            route('ref-person-states.edit', $refPersonState)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_ref_person_state()
    {
        $refPersonState = RefPersonState::factory()->create();

        $response = $this->get(
            route('ref-person-states.show', $refPersonState)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.ref_person_states.show')
            ->assertViewHas('refPersonState');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_ref_person_state()
    {
        $refPersonState = RefPersonState::factory()->create();

        $response = $this->get(
            route('ref-person-states.edit', $refPersonState)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.ref_person_states.edit')
            ->assertViewHas('refPersonState');
    }

    /**
     * @test
     */
    public function it_updates_the_ref_person_state()
    {
        $refPersonState = RefPersonState::factory()->create();

        $data = [];

        $response = $this->put(
            route('ref-person-states.update', $refPersonState),
            $data
        );

        $data['id'] = $refPersonState->id;

        $this->assertDatabaseHas('ref_person_states', $data);

        $response->assertRedirect(
            route('ref-person-states.edit', $refPersonState)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_person_state()
    {
        $refPersonState = RefPersonState::factory()->create();

        $response = $this->delete(
            route('ref-person-states.destroy', $refPersonState)
        );

        $response->assertRedirect(route('ref-person-states.index'));

        $this->assertSoftDeleted($refPersonState);
    }
}
