<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\RefTechnicType;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefTechnicTypeControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_ref_technic_types()
    {
        $refTechnicTypes = RefTechnicType::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('ref-technic-types.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_technic_types.index')
            ->assertViewHas('refTechnicTypes');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_ref_technic_type()
    {
        $response = $this->get(route('ref-technic-types.create'));

        $response->assertOk()->assertViewIs('app.ref_technic_types.create');
    }

    /**
     * @test
     */
    public function it_stores_the_ref_technic_type()
    {
        $data = RefTechnicType::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('ref-technic-types.store'), $data);

        $this->assertDatabaseHas('ref_technic_types', $data);

        $refTechnicType = RefTechnicType::latest('id')->first();

        $response->assertRedirect(
            route('ref-technic-types.edit', $refTechnicType)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_ref_technic_type()
    {
        $refTechnicType = RefTechnicType::factory()->create();

        $response = $this->get(
            route('ref-technic-types.show', $refTechnicType)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.ref_technic_types.show')
            ->assertViewHas('refTechnicType');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_ref_technic_type()
    {
        $refTechnicType = RefTechnicType::factory()->create();

        $response = $this->get(
            route('ref-technic-types.edit', $refTechnicType)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.ref_technic_types.edit')
            ->assertViewHas('refTechnicType');
    }

    /**
     * @test
     */
    public function it_updates_the_ref_technic_type()
    {
        $refTechnicType = RefTechnicType::factory()->create();

        $data = [
            'name' => $this->faker->name,
        ];

        $response = $this->put(
            route('ref-technic-types.update', $refTechnicType),
            $data
        );

        $data['id'] = $refTechnicType->id;

        $this->assertDatabaseHas('ref_technic_types', $data);

        $response->assertRedirect(
            route('ref-technic-types.edit', $refTechnicType)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_technic_type()
    {
        $refTechnicType = RefTechnicType::factory()->create();

        $response = $this->delete(
            route('ref-technic-types.destroy', $refTechnicType)
        );

        $response->assertRedirect(route('ref-technic-types.index'));

        $this->assertSoftDeleted($refTechnicType);
    }
}
