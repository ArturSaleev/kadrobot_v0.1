<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\EmployeeFamily;

use App\Models\Employee;
use App\Models\RefFamilyState;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeFamilyControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_employee_families()
    {
        $employeeFamilies = EmployeeFamily::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('employee-families.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.employee_families.index')
            ->assertViewHas('employeeFamilies');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_employee_family()
    {
        $response = $this->get(route('employee-families.create'));

        $response->assertOk()->assertViewIs('app.employee_families.create');
    }

    /**
     * @test
     */
    public function it_stores_the_employee_family()
    {
        $data = EmployeeFamily::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('employee-families.store'), $data);

        $this->assertDatabaseHas('employee_families', $data);

        $employeeFamily = EmployeeFamily::latest('id')->first();

        $response->assertRedirect(
            route('employee-families.edit', $employeeFamily)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_employee_family()
    {
        $employeeFamily = EmployeeFamily::factory()->create();

        $response = $this->get(
            route('employee-families.show', $employeeFamily)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.employee_families.show')
            ->assertViewHas('employeeFamily');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_employee_family()
    {
        $employeeFamily = EmployeeFamily::factory()->create();

        $response = $this->get(
            route('employee-families.edit', $employeeFamily)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.employee_families.edit')
            ->assertViewHas('employeeFamily');
    }

    /**
     * @test
     */
    public function it_updates_the_employee_family()
    {
        $employeeFamily = EmployeeFamily::factory()->create();

        $employee = Employee::factory()->create();
        $refFamilyState = RefFamilyState::factory()->create();

        $data = [
            'lastname' => $this->faker->lastName,
            'firstname' => $this->faker->text(255),
            'middlename' => $this->faker->text(255),
            'birthday' => $this->faker->date,
            'employee_id' => $employee->id,
            'ref_family_state_id' => $refFamilyState->id,
        ];

        $response = $this->put(
            route('employee-families.update', $employeeFamily),
            $data
        );

        $data['id'] = $employeeFamily->id;

        $this->assertDatabaseHas('employee_families', $data);

        $response->assertRedirect(
            route('employee-families.edit', $employeeFamily)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_family()
    {
        $employeeFamily = EmployeeFamily::factory()->create();

        $response = $this->delete(
            route('employee-families.destroy', $employeeFamily)
        );

        $response->assertRedirect(route('employee-families.index'));

        $this->assertSoftDeleted($employeeFamily);
    }
}
