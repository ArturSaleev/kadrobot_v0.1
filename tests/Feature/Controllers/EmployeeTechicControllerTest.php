<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\EmployeeTechic;

use App\Models\Employee;
use App\Models\RefTechnicType;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeTechicControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_employee_techics()
    {
        $employeeTechics = EmployeeTechic::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('employee-techics.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.employee_techics.index')
            ->assertViewHas('employeeTechics');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_employee_techic()
    {
        $response = $this->get(route('employee-techics.create'));

        $response->assertOk()->assertViewIs('app.employee_techics.create');
    }

    /**
     * @test
     */
    public function it_stores_the_employee_techic()
    {
        $data = EmployeeTechic::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('employee-techics.store'), $data);

        $this->assertDatabaseHas('employee_techics', $data);

        $employeeTechic = EmployeeTechic::latest('id')->first();

        $response->assertRedirect(
            route('employee-techics.edit', $employeeTechic)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_employee_techic()
    {
        $employeeTechic = EmployeeTechic::factory()->create();

        $response = $this->get(route('employee-techics.show', $employeeTechic));

        $response
            ->assertOk()
            ->assertViewIs('app.employee_techics.show')
            ->assertViewHas('employeeTechic');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_employee_techic()
    {
        $employeeTechic = EmployeeTechic::factory()->create();

        $response = $this->get(route('employee-techics.edit', $employeeTechic));

        $response
            ->assertOk()
            ->assertViewIs('app.employee_techics.edit')
            ->assertViewHas('employeeTechic');
    }

    /**
     * @test
     */
    public function it_updates_the_employee_techic()
    {
        $employeeTechic = EmployeeTechic::factory()->create();

        $employee = Employee::factory()->create();
        $refTechnicType = RefTechnicType::factory()->create();

        $data = [
            'invent_num' => $this->faker->text(255),
            'price' => $this->faker->randomFloat(2, 0, 9999),
            'employee_id' => $employee->id,
            'ref_technic_type_id' => $refTechnicType->id,
        ];

        $response = $this->put(
            route('employee-techics.update', $employeeTechic),
            $data
        );

        $data['id'] = $employeeTechic->id;

        $this->assertDatabaseHas('employee_techics', $data);

        $response->assertRedirect(
            route('employee-techics.edit', $employeeTechic)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_techic()
    {
        $employeeTechic = EmployeeTechic::factory()->create();

        $response = $this->delete(
            route('employee-techics.destroy', $employeeTechic)
        );

        $response->assertRedirect(route('employee-techics.index'));

        $this->assertModelMissing($employeeTechic);
    }
}
