<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\EmployeeDocument;

use App\Models\Employee;
use App\Models\RefDocType;
use App\Models\RefDocPlace;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeDocumentControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_employee_documents()
    {
        $employeeDocuments = EmployeeDocument::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('employee-documents.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.employee_documents.index')
            ->assertViewHas('employeeDocuments');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_employee_document()
    {
        $response = $this->get(route('employee-documents.create'));

        $response->assertOk()->assertViewIs('app.employee_documents.create');
    }

    /**
     * @test
     */
    public function it_stores_the_employee_document()
    {
        $data = EmployeeDocument::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('employee-documents.store'), $data);

        $this->assertDatabaseHas('employee_documents', $data);

        $employeeDocument = EmployeeDocument::latest('id')->first();

        $response->assertRedirect(
            route('employee-documents.edit', $employeeDocument)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_employee_document()
    {
        $employeeDocument = EmployeeDocument::factory()->create();

        $response = $this->get(
            route('employee-documents.show', $employeeDocument)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.employee_documents.show')
            ->assertViewHas('employeeDocument');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_employee_document()
    {
        $employeeDocument = EmployeeDocument::factory()->create();

        $response = $this->get(
            route('employee-documents.edit', $employeeDocument)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.employee_documents.edit')
            ->assertViewHas('employeeDocument');
    }

    /**
     * @test
     */
    public function it_updates_the_employee_document()
    {
        $employeeDocument = EmployeeDocument::factory()->create();

        $employee = Employee::factory()->create();
        $refDocType = RefDocType::factory()->create();
        $refDocPlace = RefDocPlace::factory()->create();

        $data = [
            'doc_seria' => $this->faker->text(255),
            'doc_num' => $this->faker->text(255),
            'doc_date' => $this->faker->date,
            'employee_id' => $employee->id,
            'ref_doc_type_id' => $refDocType->id,
            'ref_doc_place_id' => $refDocPlace->id,
        ];

        $response = $this->put(
            route('employee-documents.update', $employeeDocument),
            $data
        );

        $data['id'] = $employeeDocument->id;

        $this->assertDatabaseHas('employee_documents', $data);

        $response->assertRedirect(
            route('employee-documents.edit', $employeeDocument)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_document()
    {
        $employeeDocument = EmployeeDocument::factory()->create();

        $response = $this->delete(
            route('employee-documents.destroy', $employeeDocument)
        );

        $response->assertRedirect(route('employee-documents.index'));

        $this->assertSoftDeleted($employeeDocument);
    }
}
