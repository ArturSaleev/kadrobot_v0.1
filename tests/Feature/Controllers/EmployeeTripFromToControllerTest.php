<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\EmployeeTripFromTo;

use App\Models\EmployeeTrip;
use App\Models\RefTransportTrip;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeTripFromToControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_employee_trip_from_tos()
    {
        $employeeTripFromTos = EmployeeTripFromTo::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('employee-trip-from-tos.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.employee_trip_from_tos.index')
            ->assertViewHas('employeeTripFromTos');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_employee_trip_from_to()
    {
        $response = $this->get(route('employee-trip-from-tos.create'));

        $response
            ->assertOk()
            ->assertViewIs('app.employee_trip_from_tos.create');
    }

    /**
     * @test
     */
    public function it_stores_the_employee_trip_from_to()
    {
        $data = EmployeeTripFromTo::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('employee-trip-from-tos.store'), $data);

        $this->assertDatabaseHas('employee_trip_from_tos', $data);

        $employeeTripFromTo = EmployeeTripFromTo::latest('id')->first();

        $response->assertRedirect(
            route('employee-trip-from-tos.edit', $employeeTripFromTo)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_employee_trip_from_to()
    {
        $employeeTripFromTo = EmployeeTripFromTo::factory()->create();

        $response = $this->get(
            route('employee-trip-from-tos.show', $employeeTripFromTo)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.employee_trip_from_tos.show')
            ->assertViewHas('employeeTripFromTo');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_employee_trip_from_to()
    {
        $employeeTripFromTo = EmployeeTripFromTo::factory()->create();

        $response = $this->get(
            route('employee-trip-from-tos.edit', $employeeTripFromTo)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.employee_trip_from_tos.edit')
            ->assertViewHas('employeeTripFromTo');
    }

    /**
     * @test
     */
    public function it_updates_the_employee_trip_from_to()
    {
        $employeeTripFromTo = EmployeeTripFromTo::factory()->create();

        $employeeTrip = EmployeeTrip::factory()->create();
        $refTransportTrip = RefTransportTrip::factory()->create();

        $data = [
            'from_place' => $this->faker->text,
            'to_place' => $this->faker->text,
            'employee_trip_id' => $employeeTrip->id,
            'ref_transport_trip_id' => $refTransportTrip->id,
        ];

        $response = $this->put(
            route('employee-trip-from-tos.update', $employeeTripFromTo),
            $data
        );

        $data['id'] = $employeeTripFromTo->id;

        $this->assertDatabaseHas('employee_trip_from_tos', $data);

        $response->assertRedirect(
            route('employee-trip-from-tos.edit', $employeeTripFromTo)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_trip_from_to()
    {
        $employeeTripFromTo = EmployeeTripFromTo::factory()->create();

        $response = $this->delete(
            route('employee-trip-from-tos.destroy', $employeeTripFromTo)
        );

        $response->assertRedirect(route('employee-trip-from-tos.index'));

        $this->assertSoftDeleted($employeeTripFromTo);
    }
}
