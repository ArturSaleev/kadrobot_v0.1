<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\RefStatus;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefStatusControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_ref_statuses()
    {
        $refStatuses = RefStatus::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('ref-statuses.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_statuses.index')
            ->assertViewHas('refStatuses');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_ref_status()
    {
        $response = $this->get(route('ref-statuses.create'));

        $response->assertOk()->assertViewIs('app.ref_statuses.create');
    }

    /**
     * @test
     */
    public function it_stores_the_ref_status()
    {
        $data = RefStatus::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('ref-statuses.store'), $data);

        $this->assertDatabaseHas('ref_statuses', $data);

        $refStatus = RefStatus::latest('id')->first();

        $response->assertRedirect(route('ref-statuses.edit', $refStatus));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_ref_status()
    {
        $refStatus = RefStatus::factory()->create();

        $response = $this->get(route('ref-statuses.show', $refStatus));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_statuses.show')
            ->assertViewHas('refStatus');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_ref_status()
    {
        $refStatus = RefStatus::factory()->create();

        $response = $this->get(route('ref-statuses.edit', $refStatus));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_statuses.edit')
            ->assertViewHas('refStatus');
    }

    /**
     * @test
     */
    public function it_updates_the_ref_status()
    {
        $refStatus = RefStatus::factory()->create();

        $data = [];

        $response = $this->put(route('ref-statuses.update', $refStatus), $data);

        $data['id'] = $refStatus->id;

        $this->assertDatabaseHas('ref_statuses', $data);

        $response->assertRedirect(route('ref-statuses.edit', $refStatus));
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_status()
    {
        $refStatus = RefStatus::factory()->create();

        $response = $this->delete(route('ref-statuses.destroy', $refStatus));

        $response->assertRedirect(route('ref-statuses.index'));

        $this->assertSoftDeleted($refStatus);
    }
}
