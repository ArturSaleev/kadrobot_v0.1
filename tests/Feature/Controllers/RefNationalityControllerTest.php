<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\RefNationality;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefNationalityControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_ref_nationalities()
    {
        $refNationalities = RefNationality::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('ref-nationalities.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_nationalities.index')
            ->assertViewHas('refNationalities');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_ref_nationality()
    {
        $response = $this->get(route('ref-nationalities.create'));

        $response->assertOk()->assertViewIs('app.ref_nationalities.create');
    }

    /**
     * @test
     */
    public function it_stores_the_ref_nationality()
    {
        $data = RefNationality::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('ref-nationalities.store'), $data);

        $this->assertDatabaseHas('ref_nationalities', $data);

        $refNationality = RefNationality::latest('id')->first();

        $response->assertRedirect(
            route('ref-nationalities.edit', $refNationality)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_ref_nationality()
    {
        $refNationality = RefNationality::factory()->create();

        $response = $this->get(
            route('ref-nationalities.show', $refNationality)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.ref_nationalities.show')
            ->assertViewHas('refNationality');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_ref_nationality()
    {
        $refNationality = RefNationality::factory()->create();

        $response = $this->get(
            route('ref-nationalities.edit', $refNationality)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.ref_nationalities.edit')
            ->assertViewHas('refNationality');
    }

    /**
     * @test
     */
    public function it_updates_the_ref_nationality()
    {
        $refNationality = RefNationality::factory()->create();

        $data = [];

        $response = $this->put(
            route('ref-nationalities.update', $refNationality),
            $data
        );

        $data['id'] = $refNationality->id;

        $this->assertDatabaseHas('ref_nationalities', $data);

        $response->assertRedirect(
            route('ref-nationalities.edit', $refNationality)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_nationality()
    {
        $refNationality = RefNationality::factory()->create();

        $response = $this->delete(
            route('ref-nationalities.destroy', $refNationality)
        );

        $response->assertRedirect(route('ref-nationalities.index'));

        $this->assertSoftDeleted($refNationality);
    }
}
