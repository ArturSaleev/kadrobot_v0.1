<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\RefTransportTrip;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefTransportTripControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_ref_transport_trips()
    {
        $refTransportTrips = RefTransportTrip::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('ref-transport-trips.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_transport_trips.index')
            ->assertViewHas('refTransportTrips');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_ref_transport_trip()
    {
        $response = $this->get(route('ref-transport-trips.create'));

        $response->assertOk()->assertViewIs('app.ref_transport_trips.create');
    }

    /**
     * @test
     */
    public function it_stores_the_ref_transport_trip()
    {
        $data = RefTransportTrip::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('ref-transport-trips.store'), $data);

        $this->assertDatabaseHas('ref_transport_trips', $data);

        $refTransportTrip = RefTransportTrip::latest('id')->first();

        $response->assertRedirect(
            route('ref-transport-trips.edit', $refTransportTrip)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_ref_transport_trip()
    {
        $refTransportTrip = RefTransportTrip::factory()->create();

        $response = $this->get(
            route('ref-transport-trips.show', $refTransportTrip)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.ref_transport_trips.show')
            ->assertViewHas('refTransportTrip');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_ref_transport_trip()
    {
        $refTransportTrip = RefTransportTrip::factory()->create();

        $response = $this->get(
            route('ref-transport-trips.edit', $refTransportTrip)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.ref_transport_trips.edit')
            ->assertViewHas('refTransportTrip');
    }

    /**
     * @test
     */
    public function it_updates_the_ref_transport_trip()
    {
        $refTransportTrip = RefTransportTrip::factory()->create();

        $data = [];

        $response = $this->put(
            route('ref-transport-trips.update', $refTransportTrip),
            $data
        );

        $data['id'] = $refTransportTrip->id;

        $this->assertDatabaseHas('ref_transport_trips', $data);

        $response->assertRedirect(
            route('ref-transport-trips.edit', $refTransportTrip)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_transport_trip()
    {
        $refTransportTrip = RefTransportTrip::factory()->create();

        $response = $this->delete(
            route('ref-transport-trips.destroy', $refTransportTrip)
        );

        $response->assertRedirect(route('ref-transport-trips.index'));

        $this->assertSoftDeleted($refTransportTrip);
    }
}
