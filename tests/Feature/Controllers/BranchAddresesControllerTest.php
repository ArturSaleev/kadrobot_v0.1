<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\BranchAddreses;

use App\Models\Branch;
use App\Models\RefAddressType;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BranchAddresesControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_all_branch_addreses()
    {
        $allBranchAddreses = BranchAddreses::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('all-branch-addreses.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.all_branch_addreses.index')
            ->assertViewHas('allBranchAddreses');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_branch_addreses()
    {
        $response = $this->get(route('all-branch-addreses.create'));

        $response->assertOk()->assertViewIs('app.all_branch_addreses.create');
    }

    /**
     * @test
     */
    public function it_stores_the_branch_addreses()
    {
        $data = BranchAddreses::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('all-branch-addreses.store'), $data);

        $this->assertDatabaseHas('branch_addreses', $data);

        $branchAddreses = BranchAddreses::latest('id')->first();

        $response->assertRedirect(
            route('all-branch-addreses.edit', $branchAddreses)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_branch_addreses()
    {
        $branchAddreses = BranchAddreses::factory()->create();

        $response = $this->get(
            route('all-branch-addreses.show', $branchAddreses)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.all_branch_addreses.show')
            ->assertViewHas('branchAddreses');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_branch_addreses()
    {
        $branchAddreses = BranchAddreses::factory()->create();

        $response = $this->get(
            route('all-branch-addreses.edit', $branchAddreses)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.all_branch_addreses.edit')
            ->assertViewHas('branchAddreses');
    }

    /**
     * @test
     */
    public function it_updates_the_branch_addreses()
    {
        $branchAddreses = BranchAddreses::factory()->create();

        $refAddressType = RefAddressType::factory()->create();
        $branch = Branch::factory()->create();

        $data = [
            'ref_address_type_id' => $refAddressType->id,
            'branch_id' => $branch->id,
        ];

        $response = $this->put(
            route('all-branch-addreses.update', $branchAddreses),
            $data
        );

        $data['id'] = $branchAddreses->id;

        $this->assertDatabaseHas('branch_addreses', $data);

        $response->assertRedirect(
            route('all-branch-addreses.edit', $branchAddreses)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_branch_addreses()
    {
        $branchAddreses = BranchAddreses::factory()->create();

        $response = $this->delete(
            route('all-branch-addreses.destroy', $branchAddreses)
        );

        $response->assertRedirect(route('all-branch-addreses.index'));

        $this->assertModelMissing($branchAddreses);
    }
}
