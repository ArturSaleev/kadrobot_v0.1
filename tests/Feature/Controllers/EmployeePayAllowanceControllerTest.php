<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\EmployeePayAllowance;

use App\Models\Employee;
use App\Models\RefAllowanceType;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeePayAllowanceControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_employee_pay_allowances()
    {
        $employeePayAllowances = EmployeePayAllowance::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('employee-pay-allowances.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.employee_pay_allowances.index')
            ->assertViewHas('employeePayAllowances');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_employee_pay_allowance()
    {
        $response = $this->get(route('employee-pay-allowances.create'));

        $response
            ->assertOk()
            ->assertViewIs('app.employee_pay_allowances.create');
    }

    /**
     * @test
     */
    public function it_stores_the_employee_pay_allowance()
    {
        $data = EmployeePayAllowance::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('employee-pay-allowances.store'), $data);

        $this->assertDatabaseHas('employee_pay_allowances', $data);

        $employeePayAllowance = EmployeePayAllowance::latest('id')->first();

        $response->assertRedirect(
            route('employee-pay-allowances.edit', $employeePayAllowance)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_employee_pay_allowance()
    {
        $employeePayAllowance = EmployeePayAllowance::factory()->create();

        $response = $this->get(
            route('employee-pay-allowances.show', $employeePayAllowance)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.employee_pay_allowances.show')
            ->assertViewHas('employeePayAllowance');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_employee_pay_allowance()
    {
        $employeePayAllowance = EmployeePayAllowance::factory()->create();

        $response = $this->get(
            route('employee-pay-allowances.edit', $employeePayAllowance)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.employee_pay_allowances.edit')
            ->assertViewHas('employeePayAllowance');
    }

    /**
     * @test
     */
    public function it_updates_the_employee_pay_allowance()
    {
        $employeePayAllowance = EmployeePayAllowance::factory()->create();

        $employee = Employee::factory()->create();
        $refAllowanceType = RefAllowanceType::factory()->create();

        $data = [
            'date_add' => $this->faker->date,
            'period_begin' => $this->faker->date,
            'period_end' => $this->faker->date,
            'paysum' => $this->faker->randomNumber(2),
            'employee_id' => $employee->id,
            'ref_allowance_type_id' => $refAllowanceType->id,
        ];

        $response = $this->put(
            route('employee-pay-allowances.update', $employeePayAllowance),
            $data
        );

        $data['id'] = $employeePayAllowance->id;

        $this->assertDatabaseHas('employee_pay_allowances', $data);

        $response->assertRedirect(
            route('employee-pay-allowances.edit', $employeePayAllowance)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_pay_allowance()
    {
        $employeePayAllowance = EmployeePayAllowance::factory()->create();

        $response = $this->delete(
            route('employee-pay-allowances.destroy', $employeePayAllowance)
        );

        $response->assertRedirect(route('employee-pay-allowances.index'));

        $this->assertSoftDeleted($employeePayAllowance);
    }
}
