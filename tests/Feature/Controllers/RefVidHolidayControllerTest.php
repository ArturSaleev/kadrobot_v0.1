<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\RefVidHoliday;

use App\Models\RefVidHolidayType;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefVidHolidayControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_ref_vid_holidays()
    {
        $refVidHolidays = RefVidHoliday::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('ref-vid-holidays.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_vid_holidays.index')
            ->assertViewHas('refVidHolidays');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_ref_vid_holiday()
    {
        $response = $this->get(route('ref-vid-holidays.create'));

        $response->assertOk()->assertViewIs('app.ref_vid_holidays.create');
    }

    /**
     * @test
     */
    public function it_stores_the_ref_vid_holiday()
    {
        $data = RefVidHoliday::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('ref-vid-holidays.store'), $data);

        $this->assertDatabaseHas('ref_vid_holidays', $data);

        $refVidHoliday = RefVidHoliday::latest('id')->first();

        $response->assertRedirect(
            route('ref-vid-holidays.edit', $refVidHoliday)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_ref_vid_holiday()
    {
        $refVidHoliday = RefVidHoliday::factory()->create();

        $response = $this->get(route('ref-vid-holidays.show', $refVidHoliday));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_vid_holidays.show')
            ->assertViewHas('refVidHoliday');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_ref_vid_holiday()
    {
        $refVidHoliday = RefVidHoliday::factory()->create();

        $response = $this->get(route('ref-vid-holidays.edit', $refVidHoliday));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_vid_holidays.edit')
            ->assertViewHas('refVidHoliday');
    }

    /**
     * @test
     */
    public function it_updates_the_ref_vid_holiday()
    {
        $refVidHoliday = RefVidHoliday::factory()->create();

        $refVidHolidayType = RefVidHolidayType::factory()->create();

        $data = [
            'ref_vid_holiday_type_id' => $refVidHolidayType->id,
        ];

        $response = $this->put(
            route('ref-vid-holidays.update', $refVidHoliday),
            $data
        );

        $data['id'] = $refVidHoliday->id;

        $this->assertDatabaseHas('ref_vid_holidays', $data);

        $response->assertRedirect(
            route('ref-vid-holidays.edit', $refVidHoliday)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_vid_holiday()
    {
        $refVidHoliday = RefVidHoliday::factory()->create();

        $response = $this->delete(
            route('ref-vid-holidays.destroy', $refVidHoliday)
        );

        $response->assertRedirect(route('ref-vid-holidays.index'));

        $this->assertSoftDeleted($refVidHoliday);
    }
}
