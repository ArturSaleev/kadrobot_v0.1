<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\EmployeeStazh;

use App\Models\Employee;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeStazhControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_employee_stazhs()
    {
        $employeeStazhs = EmployeeStazh::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('employee-stazhs.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.employee_stazhs.index')
            ->assertViewHas('employeeStazhs');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_employee_stazh()
    {
        $response = $this->get(route('employee-stazhs.create'));

        $response->assertOk()->assertViewIs('app.employee_stazhs.create');
    }

    /**
     * @test
     */
    public function it_stores_the_employee_stazh()
    {
        $data = EmployeeStazh::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('employee-stazhs.store'), $data);

        $this->assertDatabaseHas('employee_stazhs', $data);

        $employeeStazh = EmployeeStazh::latest('id')->first();

        $response->assertRedirect(
            route('employee-stazhs.edit', $employeeStazh)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_employee_stazh()
    {
        $employeeStazh = EmployeeStazh::factory()->create();

        $response = $this->get(route('employee-stazhs.show', $employeeStazh));

        $response
            ->assertOk()
            ->assertViewIs('app.employee_stazhs.show')
            ->assertViewHas('employeeStazh');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_employee_stazh()
    {
        $employeeStazh = EmployeeStazh::factory()->create();

        $response = $this->get(route('employee-stazhs.edit', $employeeStazh));

        $response
            ->assertOk()
            ->assertViewIs('app.employee_stazhs.edit')
            ->assertViewHas('employeeStazh');
    }

    /**
     * @test
     */
    public function it_updates_the_employee_stazh()
    {
        $employeeStazh = EmployeeStazh::factory()->create();

        $employee = Employee::factory()->create();

        $data = [
            'date_begin' => $this->faker->date,
            'date_end' => $this->faker->date,
            'cnt_mes' => $this->faker->randomNumber(0),
            'employee_id' => $employee->id,
        ];

        $response = $this->put(
            route('employee-stazhs.update', $employeeStazh),
            $data
        );

        $data['id'] = $employeeStazh->id;

        $this->assertDatabaseHas('employee_stazhs', $data);

        $response->assertRedirect(
            route('employee-stazhs.edit', $employeeStazh)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_stazh()
    {
        $employeeStazh = EmployeeStazh::factory()->create();

        $response = $this->delete(
            route('employee-stazhs.destroy', $employeeStazh)
        );

        $response->assertRedirect(route('employee-stazhs.index'));

        $this->assertSoftDeleted($employeeStazh);
    }
}
