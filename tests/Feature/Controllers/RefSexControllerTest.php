<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\RefSex;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefSexControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_ref_sexes()
    {
        $refSexes = RefSex::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('ref-sexes.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_sexes.index')
            ->assertViewHas('refSexes');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_ref_sex()
    {
        $response = $this->get(route('ref-sexes.create'));

        $response->assertOk()->assertViewIs('app.ref_sexes.create');
    }

    /**
     * @test
     */
    public function it_stores_the_ref_sex()
    {
        $data = RefSex::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('ref-sexes.store'), $data);

        $this->assertDatabaseHas('ref_sexes', $data);

        $refSex = RefSex::latest('id')->first();

        $response->assertRedirect(route('ref-sexes.edit', $refSex));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_ref_sex()
    {
        $refSex = RefSex::factory()->create();

        $response = $this->get(route('ref-sexes.show', $refSex));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_sexes.show')
            ->assertViewHas('refSex');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_ref_sex()
    {
        $refSex = RefSex::factory()->create();

        $response = $this->get(route('ref-sexes.edit', $refSex));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_sexes.edit')
            ->assertViewHas('refSex');
    }

    /**
     * @test
     */
    public function it_updates_the_ref_sex()
    {
        $refSex = RefSex::factory()->create();

        $data = [];

        $response = $this->put(route('ref-sexes.update', $refSex), $data);

        $data['id'] = $refSex->id;

        $this->assertDatabaseHas('ref_sexes', $data);

        $response->assertRedirect(route('ref-sexes.edit', $refSex));
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_sex()
    {
        $refSex = RefSex::factory()->create();

        $response = $this->delete(route('ref-sexes.destroy', $refSex));

        $response->assertRedirect(route('ref-sexes.index'));

        $this->assertSoftDeleted($refSex);
    }
}
