<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\RefDocPlace;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefDocPlaceControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_ref_doc_places()
    {
        $refDocPlaces = RefDocPlace::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('ref-doc-places.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_doc_places.index')
            ->assertViewHas('refDocPlaces');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_ref_doc_place()
    {
        $response = $this->get(route('ref-doc-places.create'));

        $response->assertOk()->assertViewIs('app.ref_doc_places.create');
    }

    /**
     * @test
     */
    public function it_stores_the_ref_doc_place()
    {
        $data = RefDocPlace::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('ref-doc-places.store'), $data);

        $this->assertDatabaseHas('ref_doc_places', $data);

        $refDocPlace = RefDocPlace::latest('id')->first();

        $response->assertRedirect(route('ref-doc-places.edit', $refDocPlace));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_ref_doc_place()
    {
        $refDocPlace = RefDocPlace::factory()->create();

        $response = $this->get(route('ref-doc-places.show', $refDocPlace));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_doc_places.show')
            ->assertViewHas('refDocPlace');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_ref_doc_place()
    {
        $refDocPlace = RefDocPlace::factory()->create();

        $response = $this->get(route('ref-doc-places.edit', $refDocPlace));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_doc_places.edit')
            ->assertViewHas('refDocPlace');
    }

    /**
     * @test
     */
    public function it_updates_the_ref_doc_place()
    {
        $refDocPlace = RefDocPlace::factory()->create();

        $data = [];

        $response = $this->put(
            route('ref-doc-places.update', $refDocPlace),
            $data
        );

        $data['id'] = $refDocPlace->id;

        $this->assertDatabaseHas('ref_doc_places', $data);

        $response->assertRedirect(route('ref-doc-places.edit', $refDocPlace));
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_doc_place()
    {
        $refDocPlace = RefDocPlace::factory()->create();

        $response = $this->delete(
            route('ref-doc-places.destroy', $refDocPlace)
        );

        $response->assertRedirect(route('ref-doc-places.index'));

        $this->assertSoftDeleted($refDocPlace);
    }
}
