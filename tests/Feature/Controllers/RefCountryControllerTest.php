<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\RefCountry;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefCountryControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_ref_countries()
    {
        $refCountries = RefCountry::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('ref-countries.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_countries.index')
            ->assertViewHas('refCountries');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_ref_country()
    {
        $response = $this->get(route('ref-countries.create'));

        $response->assertOk()->assertViewIs('app.ref_countries.create');
    }

    /**
     * @test
     */
    public function it_stores_the_ref_country()
    {
        $data = RefCountry::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('ref-countries.store'), $data);

        $this->assertDatabaseHas('ref_countries', $data);

        $refCountry = RefCountry::latest('id')->first();

        $response->assertRedirect(route('ref-countries.edit', $refCountry));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_ref_country()
    {
        $refCountry = RefCountry::factory()->create();

        $response = $this->get(route('ref-countries.show', $refCountry));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_countries.show')
            ->assertViewHas('refCountry');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_ref_country()
    {
        $refCountry = RefCountry::factory()->create();

        $response = $this->get(route('ref-countries.edit', $refCountry));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_countries.edit')
            ->assertViewHas('refCountry');
    }

    /**
     * @test
     */
    public function it_updates_the_ref_country()
    {
        $refCountry = RefCountry::factory()->create();

        $data = [
            'code' => $this->faker->unique->text(255),
        ];

        $response = $this->put(
            route('ref-countries.update', $refCountry),
            $data
        );

        $data['id'] = $refCountry->id;

        $this->assertDatabaseHas('ref_countries', $data);

        $response->assertRedirect(route('ref-countries.edit', $refCountry));
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_country()
    {
        $refCountry = RefCountry::factory()->create();

        $response = $this->delete(route('ref-countries.destroy', $refCountry));

        $response->assertRedirect(route('ref-countries.index'));

        $this->assertSoftDeleted($refCountry);
    }
}
