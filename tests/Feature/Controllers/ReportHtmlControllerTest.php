<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\ReportHtml;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReportHtmlControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_report_htmls()
    {
        $reportHtmls = ReportHtml::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('report-htmls.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.report_htmls.index')
            ->assertViewHas('reportHtmls');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_report_html()
    {
        $response = $this->get(route('report-htmls.create'));

        $response->assertOk()->assertViewIs('app.report_htmls.create');
    }

    /**
     * @test
     */
    public function it_stores_the_report_html()
    {
        $data = ReportHtml::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('report-htmls.store'), $data);

        $this->assertDatabaseHas('report_htmls', $data);

        $reportHtml = ReportHtml::latest('id')->first();

        $response->assertRedirect(route('report-htmls.edit', $reportHtml));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_report_html()
    {
        $reportHtml = ReportHtml::factory()->create();

        $response = $this->get(route('report-htmls.show', $reportHtml));

        $response
            ->assertOk()
            ->assertViewIs('app.report_htmls.show')
            ->assertViewHas('reportHtml');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_report_html()
    {
        $reportHtml = ReportHtml::factory()->create();

        $response = $this->get(route('report-htmls.edit', $reportHtml));

        $response
            ->assertOk()
            ->assertViewIs('app.report_htmls.edit')
            ->assertViewHas('reportHtml');
    }

    /**
     * @test
     */
    public function it_updates_the_report_html()
    {
        $reportHtml = ReportHtml::factory()->create();

        $data = [
            'html_text' => $this->faker->text,
            'sql_text' => $this->faker->text,
            'title_text' => $this->faker->text,
            'date_edd' => $this->faker->dateTime,
        ];

        $response = $this->put(
            route('report-htmls.update', $reportHtml),
            $data
        );

        $data['id'] = $reportHtml->id;

        $this->assertDatabaseHas('report_htmls', $data);

        $response->assertRedirect(route('report-htmls.edit', $reportHtml));
    }

    /**
     * @test
     */
    public function it_deletes_the_report_html()
    {
        $reportHtml = ReportHtml::factory()->create();

        $response = $this->delete(route('report-htmls.destroy', $reportHtml));

        $response->assertRedirect(route('report-htmls.index'));

        $this->assertSoftDeleted($reportHtml);
    }
}
