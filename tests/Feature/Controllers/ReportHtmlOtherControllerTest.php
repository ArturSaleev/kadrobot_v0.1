<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\ReportHtmlOther;

use App\Models\ReportHtml;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReportHtmlOtherControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_report_html_others()
    {
        $reportHtmlOthers = ReportHtmlOther::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('report-html-others.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.report_html_others.index')
            ->assertViewHas('reportHtmlOthers');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_report_html_other()
    {
        $response = $this->get(route('report-html-others.create'));

        $response->assertOk()->assertViewIs('app.report_html_others.create');
    }

    /**
     * @test
     */
    public function it_stores_the_report_html_other()
    {
        $data = ReportHtmlOther::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('report-html-others.store'), $data);

        $this->assertDatabaseHas('report_html_others', $data);

        $reportHtmlOther = ReportHtmlOther::latest('id')->first();

        $response->assertRedirect(
            route('report-html-others.edit', $reportHtmlOther)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_report_html_other()
    {
        $reportHtmlOther = ReportHtmlOther::factory()->create();

        $response = $this->get(
            route('report-html-others.show', $reportHtmlOther)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.report_html_others.show')
            ->assertViewHas('reportHtmlOther');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_report_html_other()
    {
        $reportHtmlOther = ReportHtmlOther::factory()->create();

        $response = $this->get(
            route('report-html-others.edit', $reportHtmlOther)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.report_html_others.edit')
            ->assertViewHas('reportHtmlOther');
    }

    /**
     * @test
     */
    public function it_updates_the_report_html_other()
    {
        $reportHtmlOther = ReportHtmlOther::factory()->create();

        $reportHtml = ReportHtml::factory()->create();

        $data = [
            'title' => $this->faker->sentence(10),
            'html_text' => $this->faker->text,
            'sql_text' => $this->faker->text,
            'position' => $this->faker->randomNumber(0),
            'num_pp' => $this->faker->randomNumber(0),
            'report_html_id' => $reportHtml->id,
        ];

        $response = $this->put(
            route('report-html-others.update', $reportHtmlOther),
            $data
        );

        $data['id'] = $reportHtmlOther->id;

        $this->assertDatabaseHas('report_html_others', $data);

        $response->assertRedirect(
            route('report-html-others.edit', $reportHtmlOther)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_report_html_other()
    {
        $reportHtmlOther = ReportHtmlOther::factory()->create();

        $response = $this->delete(
            route('report-html-others.destroy', $reportHtmlOther)
        );

        $response->assertRedirect(route('report-html-others.index'));

        $this->assertSoftDeleted($reportHtmlOther);
    }
}
