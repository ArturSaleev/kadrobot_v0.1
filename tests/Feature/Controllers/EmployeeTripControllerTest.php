<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\EmployeeTrip;

use App\Models\Employee;
use App\Models\RefTransportTrip;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeTripControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_employee_trips()
    {
        $employeeTrips = EmployeeTrip::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('employee-trips.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.employee_trips.index')
            ->assertViewHas('employeeTrips');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_employee_trip()
    {
        $response = $this->get(route('employee-trips.create'));

        $response->assertOk()->assertViewIs('app.employee_trips.create');
    }

    /**
     * @test
     */
    public function it_stores_the_employee_trip()
    {
        $data = EmployeeTrip::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('employee-trips.store'), $data);

        $this->assertDatabaseHas('employee_trips', $data);

        $employeeTrip = EmployeeTrip::latest('id')->first();

        $response->assertRedirect(route('employee-trips.edit', $employeeTrip));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_employee_trip()
    {
        $employeeTrip = EmployeeTrip::factory()->create();

        $response = $this->get(route('employee-trips.show', $employeeTrip));

        $response
            ->assertOk()
            ->assertViewIs('app.employee_trips.show')
            ->assertViewHas('employeeTrip');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_employee_trip()
    {
        $employeeTrip = EmployeeTrip::factory()->create();

        $response = $this->get(route('employee-trips.edit', $employeeTrip));

        $response
            ->assertOk()
            ->assertViewIs('app.employee_trips.edit')
            ->assertViewHas('employeeTrip');
    }

    /**
     * @test
     */
    public function it_updates_the_employee_trip()
    {
        $employeeTrip = EmployeeTrip::factory()->create();

        $employee = Employee::factory()->create();
        $refTransportTrip = RefTransportTrip::factory()->create();

        $data = [
            'date_begin' => $this->faker->date,
            'date_end' => $this->faker->date,
            'cnt_days' => $this->faker->randomNumber(0),
            'order_num' => $this->faker->text(255),
            'order_date' => $this->faker->date,
            'employee_id' => $employee->id,
            'ref_transport_trip_id' => $refTransportTrip->id,
        ];

        $response = $this->put(
            route('employee-trips.update', $employeeTrip),
            $data
        );

        $data['id'] = $employeeTrip->id;

        $this->assertDatabaseHas('employee_trips', $data);

        $response->assertRedirect(route('employee-trips.edit', $employeeTrip));
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_trip()
    {
        $employeeTrip = EmployeeTrip::factory()->create();

        $response = $this->delete(
            route('employee-trips.destroy', $employeeTrip)
        );

        $response->assertRedirect(route('employee-trips.index'));

        $this->assertSoftDeleted($employeeTrip);
    }
}
