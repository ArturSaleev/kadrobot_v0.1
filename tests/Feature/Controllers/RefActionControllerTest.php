<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\RefAction;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefActionControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_ref_actions()
    {
        $refActions = RefAction::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('ref-actions.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_actions.index')
            ->assertViewHas('refActions');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_ref_action()
    {
        $response = $this->get(route('ref-actions.create'));

        $response->assertOk()->assertViewIs('app.ref_actions.create');
    }

    /**
     * @test
     */
    public function it_stores_the_ref_action()
    {
        $data = RefAction::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('ref-actions.store'), $data);

        $this->assertDatabaseHas('ref_actions', $data);

        $refAction = RefAction::latest('id')->first();

        $response->assertRedirect(route('ref-actions.edit', $refAction));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_ref_action()
    {
        $refAction = RefAction::factory()->create();

        $response = $this->get(route('ref-actions.show', $refAction));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_actions.show')
            ->assertViewHas('refAction');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_ref_action()
    {
        $refAction = RefAction::factory()->create();

        $response = $this->get(route('ref-actions.edit', $refAction));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_actions.edit')
            ->assertViewHas('refAction');
    }

    /**
     * @test
     */
    public function it_updates_the_ref_action()
    {
        $refAction = RefAction::factory()->create();

        $data = [
            'act_type' => $this->faker->randomNumber(0),
        ];

        $response = $this->put(route('ref-actions.update', $refAction), $data);

        $data['id'] = $refAction->id;

        $this->assertDatabaseHas('ref_actions', $data);

        $response->assertRedirect(route('ref-actions.edit', $refAction));
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_action()
    {
        $refAction = RefAction::factory()->create();

        $response = $this->delete(route('ref-actions.destroy', $refAction));

        $response->assertRedirect(route('ref-actions.index'));

        $this->assertSoftDeleted($refAction);
    }
}
