<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\RefAccountType;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefAccountTypeControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_ref_account_types()
    {
        $refAccountTypes = RefAccountType::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('ref-account-types.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_account_types.index')
            ->assertViewHas('refAccountTypes');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_ref_account_type()
    {
        $response = $this->get(route('ref-account-types.create'));

        $response->assertOk()->assertViewIs('app.ref_account_types.create');
    }

    /**
     * @test
     */
    public function it_stores_the_ref_account_type()
    {
        $data = RefAccountType::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('ref-account-types.store'), $data);

        $this->assertDatabaseHas('ref_account_types', $data);

        $refAccountType = RefAccountType::latest('id')->first();

        $response->assertRedirect(
            route('ref-account-types.edit', $refAccountType)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_ref_account_type()
    {
        $refAccountType = RefAccountType::factory()->create();

        $response = $this->get(
            route('ref-account-types.show', $refAccountType)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.ref_account_types.show')
            ->assertViewHas('refAccountType');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_ref_account_type()
    {
        $refAccountType = RefAccountType::factory()->create();

        $response = $this->get(
            route('ref-account-types.edit', $refAccountType)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.ref_account_types.edit')
            ->assertViewHas('refAccountType');
    }

    /**
     * @test
     */
    public function it_updates_the_ref_account_type()
    {
        $refAccountType = RefAccountType::factory()->create();

        $data = [];

        $response = $this->put(
            route('ref-account-types.update', $refAccountType),
            $data
        );

        $data['id'] = $refAccountType->id;

        $this->assertDatabaseHas('ref_account_types', $data);

        $response->assertRedirect(
            route('ref-account-types.edit', $refAccountType)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_account_type()
    {
        $refAccountType = RefAccountType::factory()->create();

        $response = $this->delete(
            route('ref-account-types.destroy', $refAccountType)
        );

        $response->assertRedirect(route('ref-account-types.index'));

        $this->assertSoftDeleted($refAccountType);
    }
}
