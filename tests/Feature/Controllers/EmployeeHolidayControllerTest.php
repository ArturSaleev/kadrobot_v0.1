<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\EmployeeHoliday;

use App\Models\Employee;
use App\Models\RefVidHoliday;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeHolidayControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_employee_holidays()
    {
        $employeeHolidays = EmployeeHoliday::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('employee-holidays.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.employee_holidays.index')
            ->assertViewHas('employeeHolidays');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_employee_holiday()
    {
        $response = $this->get(route('employee-holidays.create'));

        $response->assertOk()->assertViewIs('app.employee_holidays.create');
    }

    /**
     * @test
     */
    public function it_stores_the_employee_holiday()
    {
        $data = EmployeeHoliday::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('employee-holidays.store'), $data);

        $this->assertDatabaseHas('employee_holidays', $data);

        $employeeHoliday = EmployeeHoliday::latest('id')->first();

        $response->assertRedirect(
            route('employee-holidays.edit', $employeeHoliday)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_employee_holiday()
    {
        $employeeHoliday = EmployeeHoliday::factory()->create();

        $response = $this->get(
            route('employee-holidays.show', $employeeHoliday)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.employee_holidays.show')
            ->assertViewHas('employeeHoliday');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_employee_holiday()
    {
        $employeeHoliday = EmployeeHoliday::factory()->create();

        $response = $this->get(
            route('employee-holidays.edit', $employeeHoliday)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.employee_holidays.edit')
            ->assertViewHas('employeeHoliday');
    }

    /**
     * @test
     */
    public function it_updates_the_employee_holiday()
    {
        $employeeHoliday = EmployeeHoliday::factory()->create();

        $employee = Employee::factory()->create();
        $refVidHoliday = RefVidHoliday::factory()->create();

        $data = [
            'date_begin' => $this->faker->date,
            'date_end' => $this->faker->date,
            'cnt_days' => $this->faker->randomNumber(0),
            'period_begin' => $this->faker->date,
            'period_end' => $this->faker->date,
            'order_num' => $this->faker->text(255),
            'order_date' => $this->faker->date,
            'deligate' => $this->faker->randomNumber(0),
            'deligate_emp_id' => $this->faker->randomNumber,
            'doc_content' => $this->faker->text,
            'employee_id' => $employee->id,
            'ref_vid_holiday_id' => $refVidHoliday->id,
        ];

        $response = $this->put(
            route('employee-holidays.update', $employeeHoliday),
            $data
        );

        $data['id'] = $employeeHoliday->id;

        $this->assertDatabaseHas('employee_holidays', $data);

        $response->assertRedirect(
            route('employee-holidays.edit', $employeeHoliday)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_holiday()
    {
        $employeeHoliday = EmployeeHoliday::factory()->create();

        $response = $this->delete(
            route('employee-holidays.destroy', $employeeHoliday)
        );

        $response->assertRedirect(route('employee-holidays.index'));

        $this->assertSoftDeleted($employeeHoliday);
    }
}
