<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\EmployeeInvalide;

use App\Models\Employee;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeInvalideControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_employee_invalides()
    {
        $employeeInvalides = EmployeeInvalide::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('employee-invalides.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.employee_invalides.index')
            ->assertViewHas('employeeInvalides');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_employee_invalide()
    {
        $response = $this->get(route('employee-invalides.create'));

        $response->assertOk()->assertViewIs('app.employee_invalides.create');
    }

    /**
     * @test
     */
    public function it_stores_the_employee_invalide()
    {
        $data = EmployeeInvalide::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('employee-invalides.store'), $data);

        $this->assertDatabaseHas('employee_invalides', $data);

        $employeeInvalide = EmployeeInvalide::latest('id')->first();

        $response->assertRedirect(
            route('employee-invalides.edit', $employeeInvalide)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_employee_invalide()
    {
        $employeeInvalide = EmployeeInvalide::factory()->create();

        $response = $this->get(
            route('employee-invalides.show', $employeeInvalide)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.employee_invalides.show')
            ->assertViewHas('employeeInvalide');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_employee_invalide()
    {
        $employeeInvalide = EmployeeInvalide::factory()->create();

        $response = $this->get(
            route('employee-invalides.edit', $employeeInvalide)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.employee_invalides.edit')
            ->assertViewHas('employeeInvalide');
    }

    /**
     * @test
     */
    public function it_updates_the_employee_invalide()
    {
        $employeeInvalide = EmployeeInvalide::factory()->create();

        $employee = Employee::factory()->create();

        $data = [
            'num' => $this->faker->text(255),
            'date_add' => $this->faker->date,
            'period_begin' => $this->faker->date,
            'period_end' => $this->faker->date,
            'employee_id' => $employee->id,
        ];

        $response = $this->put(
            route('employee-invalides.update', $employeeInvalide),
            $data
        );

        $data['id'] = $employeeInvalide->id;

        $this->assertDatabaseHas('employee_invalides', $data);

        $response->assertRedirect(
            route('employee-invalides.edit', $employeeInvalide)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_invalide()
    {
        $employeeInvalide = EmployeeInvalide::factory()->create();

        $response = $this->delete(
            route('employee-invalides.destroy', $employeeInvalide)
        );

        $response->assertRedirect(route('employee-invalides.index'));

        $this->assertSoftDeleted($employeeInvalide);
    }
}
