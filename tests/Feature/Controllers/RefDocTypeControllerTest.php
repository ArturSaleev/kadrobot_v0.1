<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\RefDocType;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefDocTypeControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_ref_doc_types()
    {
        $refDocTypes = RefDocType::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('ref-doc-types.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_doc_types.index')
            ->assertViewHas('refDocTypes');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_ref_doc_type()
    {
        $response = $this->get(route('ref-doc-types.create'));

        $response->assertOk()->assertViewIs('app.ref_doc_types.create');
    }

    /**
     * @test
     */
    public function it_stores_the_ref_doc_type()
    {
        $data = RefDocType::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('ref-doc-types.store'), $data);

        $this->assertDatabaseHas('ref_doc_types', $data);

        $refDocType = RefDocType::latest('id')->first();

        $response->assertRedirect(route('ref-doc-types.edit', $refDocType));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_ref_doc_type()
    {
        $refDocType = RefDocType::factory()->create();

        $response = $this->get(route('ref-doc-types.show', $refDocType));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_doc_types.show')
            ->assertViewHas('refDocType');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_ref_doc_type()
    {
        $refDocType = RefDocType::factory()->create();

        $response = $this->get(route('ref-doc-types.edit', $refDocType));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_doc_types.edit')
            ->assertViewHas('refDocType');
    }

    /**
     * @test
     */
    public function it_updates_the_ref_doc_type()
    {
        $refDocType = RefDocType::factory()->create();

        $data = [];

        $response = $this->put(
            route('ref-doc-types.update', $refDocType),
            $data
        );

        $data['id'] = $refDocType->id;

        $this->assertDatabaseHas('ref_doc_types', $data);

        $response->assertRedirect(route('ref-doc-types.edit', $refDocType));
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_doc_type()
    {
        $refDocType = RefDocType::factory()->create();

        $response = $this->delete(route('ref-doc-types.destroy', $refDocType));

        $response->assertRedirect(route('ref-doc-types.index'));

        $this->assertSoftDeleted($refDocType);
    }
}
