<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\EmployeeMilitary;

use App\Models\Employee;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeMilitaryControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_employee_militaries()
    {
        $employeeMilitaries = EmployeeMilitary::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('employee-militaries.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.employee_militaries.index')
            ->assertViewHas('employeeMilitaries');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_employee_military()
    {
        $response = $this->get(route('employee-militaries.create'));

        $response->assertOk()->assertViewIs('app.employee_militaries.create');
    }

    /**
     * @test
     */
    public function it_stores_the_employee_military()
    {
        $data = EmployeeMilitary::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('employee-militaries.store'), $data);

        $this->assertDatabaseHas('employee_militaries', $data);

        $employeeMilitary = EmployeeMilitary::latest('id')->first();

        $response->assertRedirect(
            route('employee-militaries.edit', $employeeMilitary)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_employee_military()
    {
        $employeeMilitary = EmployeeMilitary::factory()->create();

        $response = $this->get(
            route('employee-militaries.show', $employeeMilitary)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.employee_militaries.show')
            ->assertViewHas('employeeMilitary');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_employee_military()
    {
        $employeeMilitary = EmployeeMilitary::factory()->create();

        $response = $this->get(
            route('employee-militaries.edit', $employeeMilitary)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.employee_militaries.edit')
            ->assertViewHas('employeeMilitary');
    }

    /**
     * @test
     */
    public function it_updates_the_employee_military()
    {
        $employeeMilitary = EmployeeMilitary::factory()->create();

        $employee = Employee::factory()->create();

        $data = [
            'group' => $this->faker->text(255),
            'category' => $this->faker->text(255),
            'rank' => $this->faker->text(255),
            'speciality' => $this->faker->text(255),
            'voenkom' => $this->faker->text(255),
            'spec_uch' => $this->faker->text(255),
            'spec_uch_num' => $this->faker->text(255),
            'fit' => $this->faker->text(255),
            'employee_id' => $employee->id,
        ];

        $response = $this->put(
            route('employee-militaries.update', $employeeMilitary),
            $data
        );

        $data['id'] = $employeeMilitary->id;

        $this->assertDatabaseHas('employee_militaries', $data);

        $response->assertRedirect(
            route('employee-militaries.edit', $employeeMilitary)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_military()
    {
        $employeeMilitary = EmployeeMilitary::factory()->create();

        $response = $this->delete(
            route('employee-militaries.destroy', $employeeMilitary)
        );

        $response->assertRedirect(route('employee-militaries.index'));

        $this->assertSoftDeleted($employeeMilitary);
    }
}
