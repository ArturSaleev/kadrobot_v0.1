<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\RefAllowanceType;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefAllowanceTypeControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_ref_allowance_types()
    {
        $refAllowanceTypes = RefAllowanceType::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('ref-allowance-types.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_allowance_types.index')
            ->assertViewHas('refAllowanceTypes');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_ref_allowance_type()
    {
        $response = $this->get(route('ref-allowance-types.create'));

        $response->assertOk()->assertViewIs('app.ref_allowance_types.create');
    }

    /**
     * @test
     */
    public function it_stores_the_ref_allowance_type()
    {
        $data = RefAllowanceType::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('ref-allowance-types.store'), $data);

        $this->assertDatabaseHas('ref_allowance_types', $data);

        $refAllowanceType = RefAllowanceType::latest('id')->first();

        $response->assertRedirect(
            route('ref-allowance-types.edit', $refAllowanceType)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_ref_allowance_type()
    {
        $refAllowanceType = RefAllowanceType::factory()->create();

        $response = $this->get(
            route('ref-allowance-types.show', $refAllowanceType)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.ref_allowance_types.show')
            ->assertViewHas('refAllowanceType');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_ref_allowance_type()
    {
        $refAllowanceType = RefAllowanceType::factory()->create();

        $response = $this->get(
            route('ref-allowance-types.edit', $refAllowanceType)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.ref_allowance_types.edit')
            ->assertViewHas('refAllowanceType');
    }

    /**
     * @test
     */
    public function it_updates_the_ref_allowance_type()
    {
        $refAllowanceType = RefAllowanceType::factory()->create();

        $data = [];

        $response = $this->put(
            route('ref-allowance-types.update', $refAllowanceType),
            $data
        );

        $data['id'] = $refAllowanceType->id;

        $this->assertDatabaseHas('ref_allowance_types', $data);

        $response->assertRedirect(
            route('ref-allowance-types.edit', $refAllowanceType)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_allowance_type()
    {
        $refAllowanceType = RefAllowanceType::factory()->create();

        $response = $this->delete(
            route('ref-allowance-types.destroy', $refAllowanceType)
        );

        $response->assertRedirect(route('ref-allowance-types.index'));

        $this->assertSoftDeleted($refAllowanceType);
    }
}
