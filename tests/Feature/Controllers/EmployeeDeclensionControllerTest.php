<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\EmployeeDeclension;

use App\Models\Employee;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeDeclensionControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_employee_declensions()
    {
        $employeeDeclensions = EmployeeDeclension::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('employee-declensions.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.employee_declensions.index')
            ->assertViewHas('employeeDeclensions');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_employee_declension()
    {
        $response = $this->get(route('employee-declensions.create'));

        $response->assertOk()->assertViewIs('app.employee_declensions.create');
    }

    /**
     * @test
     */
    public function it_stores_the_employee_declension()
    {
        $data = EmployeeDeclension::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('employee-declensions.store'), $data);

        unset($data['type_description']);

        $this->assertDatabaseHas('employee_declensions', $data);

        $employeeDeclension = EmployeeDeclension::latest('id')->first();

        $response->assertRedirect(
            route('employee-declensions.edit', $employeeDeclension)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_employee_declension()
    {
        $employeeDeclension = EmployeeDeclension::factory()->create();

        $response = $this->get(
            route('employee-declensions.show', $employeeDeclension)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.employee_declensions.show')
            ->assertViewHas('employeeDeclension');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_employee_declension()
    {
        $employeeDeclension = EmployeeDeclension::factory()->create();

        $response = $this->get(
            route('employee-declensions.edit', $employeeDeclension)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.employee_declensions.edit')
            ->assertViewHas('employeeDeclension');
    }

    /**
     * @test
     */
    public function it_updates_the_employee_declension()
    {
        $employeeDeclension = EmployeeDeclension::factory()->create();

        $employee = Employee::factory()->create();

        $data = [
            'locale' => $this->faker->locale,
            'lastname' => $this->faker->lastName,
            'firstname' => $this->faker->text(255),
            'middlename' => $this->faker->text(255),
            'case_type' => $this->faker->text(255),
            'type_description' => $this->faker->text(255),
            'employee_id' => $employee->id,
        ];

        $response = $this->put(
            route('employee-declensions.update', $employeeDeclension),
            $data
        );

        unset($data['type_description']);

        $data['id'] = $employeeDeclension->id;

        $this->assertDatabaseHas('employee_declensions', $data);

        $response->assertRedirect(
            route('employee-declensions.edit', $employeeDeclension)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_declension()
    {
        $employeeDeclension = EmployeeDeclension::factory()->create();

        $response = $this->delete(
            route('employee-declensions.destroy', $employeeDeclension)
        );

        $response->assertRedirect(route('employee-declensions.index'));

        $this->assertModelMissing($employeeDeclension);
    }
}
