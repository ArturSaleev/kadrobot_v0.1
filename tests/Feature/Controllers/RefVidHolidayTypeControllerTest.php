<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\RefVidHolidayType;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefVidHolidayTypeControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_ref_vid_holiday_types()
    {
        $refVidHolidayTypes = RefVidHolidayType::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('ref-vid-holiday-types.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_vid_holiday_types.index')
            ->assertViewHas('refVidHolidayTypes');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_ref_vid_holiday_type()
    {
        $response = $this->get(route('ref-vid-holiday-types.create'));

        $response->assertOk()->assertViewIs('app.ref_vid_holiday_types.create');
    }

    /**
     * @test
     */
    public function it_stores_the_ref_vid_holiday_type()
    {
        $data = RefVidHolidayType::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('ref-vid-holiday-types.store'), $data);

        $this->assertDatabaseHas('ref_vid_holiday_types', $data);

        $refVidHolidayType = RefVidHolidayType::latest('id')->first();

        $response->assertRedirect(
            route('ref-vid-holiday-types.edit', $refVidHolidayType)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_ref_vid_holiday_type()
    {
        $refVidHolidayType = RefVidHolidayType::factory()->create();

        $response = $this->get(
            route('ref-vid-holiday-types.show', $refVidHolidayType)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.ref_vid_holiday_types.show')
            ->assertViewHas('refVidHolidayType');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_ref_vid_holiday_type()
    {
        $refVidHolidayType = RefVidHolidayType::factory()->create();

        $response = $this->get(
            route('ref-vid-holiday-types.edit', $refVidHolidayType)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.ref_vid_holiday_types.edit')
            ->assertViewHas('refVidHolidayType');
    }

    /**
     * @test
     */
    public function it_updates_the_ref_vid_holiday_type()
    {
        $refVidHolidayType = RefVidHolidayType::factory()->create();

        $data = [];

        $response = $this->put(
            route('ref-vid-holiday-types.update', $refVidHolidayType),
            $data
        );

        $data['id'] = $refVidHolidayType->id;

        $this->assertDatabaseHas('ref_vid_holiday_types', $data);

        $response->assertRedirect(
            route('ref-vid-holiday-types.edit', $refVidHolidayType)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_vid_holiday_type()
    {
        $refVidHolidayType = RefVidHolidayType::factory()->create();

        $response = $this->delete(
            route('ref-vid-holiday-types.destroy', $refVidHolidayType)
        );

        $response->assertRedirect(route('ref-vid-holiday-types.index'));

        $this->assertSoftDeleted($refVidHolidayType);
    }
}
