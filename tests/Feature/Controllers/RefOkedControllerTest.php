<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\RefOked;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefOkedControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_ref_okeds()
    {
        $refOkeds = RefOked::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('ref-okeds.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_okeds.index')
            ->assertViewHas('refOkeds');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_ref_oked()
    {
        $response = $this->get(route('ref-okeds.create'));

        $response->assertOk()->assertViewIs('app.ref_okeds.create');
    }

    /**
     * @test
     */
    public function it_stores_the_ref_oked()
    {
        $data = RefOked::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('ref-okeds.store'), $data);

        $this->assertDatabaseHas('ref_okeds', $data);

        $refOked = RefOked::latest('id')->first();

        $response->assertRedirect(route('ref-okeds.edit', $refOked));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_ref_oked()
    {
        $refOked = RefOked::factory()->create();

        $response = $this->get(route('ref-okeds.show', $refOked));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_okeds.show')
            ->assertViewHas('refOked');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_ref_oked()
    {
        $refOked = RefOked::factory()->create();

        $response = $this->get(route('ref-okeds.edit', $refOked));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_okeds.edit')
            ->assertViewHas('refOked');
    }

    /**
     * @test
     */
    public function it_updates_the_ref_oked()
    {
        $refOked = RefOked::factory()->create();

        $data = [
            'oked' => $this->faker->unique->text(255),
        ];

        $response = $this->put(route('ref-okeds.update', $refOked), $data);

        $data['id'] = $refOked->id;

        $this->assertDatabaseHas('ref_okeds', $data);

        $response->assertRedirect(route('ref-okeds.edit', $refOked));
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_oked()
    {
        $refOked = RefOked::factory()->create();

        $response = $this->delete(route('ref-okeds.destroy', $refOked));

        $response->assertRedirect(route('ref-okeds.index'));

        $this->assertSoftDeleted($refOked);
    }
}
