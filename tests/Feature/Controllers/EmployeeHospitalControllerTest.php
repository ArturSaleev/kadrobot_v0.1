<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\EmployeeHospital;

use App\Models\Employee;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeHospitalControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_employee_hospitals()
    {
        $employeeHospitals = EmployeeHospital::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('employee-hospitals.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.employee_hospitals.index')
            ->assertViewHas('employeeHospitals');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_employee_hospital()
    {
        $response = $this->get(route('employee-hospitals.create'));

        $response->assertOk()->assertViewIs('app.employee_hospitals.create');
    }

    /**
     * @test
     */
    public function it_stores_the_employee_hospital()
    {
        $data = EmployeeHospital::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('employee-hospitals.store'), $data);

        $this->assertDatabaseHas('employee_hospitals', $data);

        $employeeHospital = EmployeeHospital::latest('id')->first();

        $response->assertRedirect(
            route('employee-hospitals.edit', $employeeHospital)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_employee_hospital()
    {
        $employeeHospital = EmployeeHospital::factory()->create();

        $response = $this->get(
            route('employee-hospitals.show', $employeeHospital)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.employee_hospitals.show')
            ->assertViewHas('employeeHospital');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_employee_hospital()
    {
        $employeeHospital = EmployeeHospital::factory()->create();

        $response = $this->get(
            route('employee-hospitals.edit', $employeeHospital)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.employee_hospitals.edit')
            ->assertViewHas('employeeHospital');
    }

    /**
     * @test
     */
    public function it_updates_the_employee_hospital()
    {
        $employeeHospital = EmployeeHospital::factory()->create();

        $employee = Employee::factory()->create();

        $data = [
            'date_begin' => $this->faker->date,
            'date_end' => $this->faker->date,
            'cnt_days' => $this->faker->randomNumber(0),
            'employee_id' => $employee->id,
        ];

        $response = $this->put(
            route('employee-hospitals.update', $employeeHospital),
            $data
        );

        $data['id'] = $employeeHospital->id;

        $this->assertDatabaseHas('employee_hospitals', $data);

        $response->assertRedirect(
            route('employee-hospitals.edit', $employeeHospital)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_hospital()
    {
        $employeeHospital = EmployeeHospital::factory()->create();

        $response = $this->delete(
            route('employee-hospitals.destroy', $employeeHospital)
        );

        $response->assertRedirect(route('employee-hospitals.index'));

        $this->assertSoftDeleted($employeeHospital);
    }
}
