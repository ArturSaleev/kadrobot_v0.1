<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\RefAddressType;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefAddressTypeControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_ref_address_types()
    {
        $refAddressTypes = RefAddressType::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('ref-address-types.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_address_types.index')
            ->assertViewHas('refAddressTypes');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_ref_address_type()
    {
        $response = $this->get(route('ref-address-types.create'));

        $response->assertOk()->assertViewIs('app.ref_address_types.create');
    }

    /**
     * @test
     */
    public function it_stores_the_ref_address_type()
    {
        $data = RefAddressType::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('ref-address-types.store'), $data);

        $this->assertDatabaseHas('ref_address_types', $data);

        $refAddressType = RefAddressType::latest('id')->first();

        $response->assertRedirect(
            route('ref-address-types.edit', $refAddressType)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_ref_address_type()
    {
        $refAddressType = RefAddressType::factory()->create();

        $response = $this->get(
            route('ref-address-types.show', $refAddressType)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.ref_address_types.show')
            ->assertViewHas('refAddressType');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_ref_address_type()
    {
        $refAddressType = RefAddressType::factory()->create();

        $response = $this->get(
            route('ref-address-types.edit', $refAddressType)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.ref_address_types.edit')
            ->assertViewHas('refAddressType');
    }

    /**
     * @test
     */
    public function it_updates_the_ref_address_type()
    {
        $refAddressType = RefAddressType::factory()->create();

        $data = [];

        $response = $this->put(
            route('ref-address-types.update', $refAddressType),
            $data
        );

        $data['id'] = $refAddressType->id;

        $this->assertDatabaseHas('ref_address_types', $data);

        $response->assertRedirect(
            route('ref-address-types.edit', $refAddressType)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_address_type()
    {
        $refAddressType = RefAddressType::factory()->create();

        $response = $this->delete(
            route('ref-address-types.destroy', $refAddressType)
        );

        $response->assertRedirect(route('ref-address-types.index'));

        $this->assertSoftDeleted($refAddressType);
    }
}
