<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\RefTypePhone;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefTypePhoneControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_ref_type_phones()
    {
        $refTypePhones = RefTypePhone::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('ref-type-phones.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_type_phones.index')
            ->assertViewHas('refTypePhones');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_ref_type_phone()
    {
        $response = $this->get(route('ref-type-phones.create'));

        $response->assertOk()->assertViewIs('app.ref_type_phones.create');
    }

    /**
     * @test
     */
    public function it_stores_the_ref_type_phone()
    {
        $data = RefTypePhone::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('ref-type-phones.store'), $data);

        $this->assertDatabaseHas('ref_type_phones', $data);

        $refTypePhone = RefTypePhone::latest('id')->first();

        $response->assertRedirect(route('ref-type-phones.edit', $refTypePhone));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_ref_type_phone()
    {
        $refTypePhone = RefTypePhone::factory()->create();

        $response = $this->get(route('ref-type-phones.show', $refTypePhone));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_type_phones.show')
            ->assertViewHas('refTypePhone');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_ref_type_phone()
    {
        $refTypePhone = RefTypePhone::factory()->create();

        $response = $this->get(route('ref-type-phones.edit', $refTypePhone));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_type_phones.edit')
            ->assertViewHas('refTypePhone');
    }

    /**
     * @test
     */
    public function it_updates_the_ref_type_phone()
    {
        $refTypePhone = RefTypePhone::factory()->create();

        $data = [];

        $response = $this->put(
            route('ref-type-phones.update', $refTypePhone),
            $data
        );

        $data['id'] = $refTypePhone->id;

        $this->assertDatabaseHas('ref_type_phones', $data);

        $response->assertRedirect(route('ref-type-phones.edit', $refTypePhone));
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_type_phone()
    {
        $refTypePhone = RefTypePhone::factory()->create();

        $response = $this->delete(
            route('ref-type-phones.destroy', $refTypePhone)
        );

        $response->assertRedirect(route('ref-type-phones.index'));

        $this->assertSoftDeleted($refTypePhone);
    }
}
