<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\RefTypeRodstv;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefTypeRodstvControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_ref_type_rodstvs()
    {
        $refTypeRodstvs = RefTypeRodstv::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('ref-type-rodstvs.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_type_rodstvs.index')
            ->assertViewHas('refTypeRodstvs');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_ref_type_rodstv()
    {
        $response = $this->get(route('ref-type-rodstvs.create'));

        $response->assertOk()->assertViewIs('app.ref_type_rodstvs.create');
    }

    /**
     * @test
     */
    public function it_stores_the_ref_type_rodstv()
    {
        $data = RefTypeRodstv::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('ref-type-rodstvs.store'), $data);

        $this->assertDatabaseHas('ref_type_rodstvs', $data);

        $refTypeRodstv = RefTypeRodstv::latest('id')->first();

        $response->assertRedirect(
            route('ref-type-rodstvs.edit', $refTypeRodstv)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_ref_type_rodstv()
    {
        $refTypeRodstv = RefTypeRodstv::factory()->create();

        $response = $this->get(route('ref-type-rodstvs.show', $refTypeRodstv));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_type_rodstvs.show')
            ->assertViewHas('refTypeRodstv');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_ref_type_rodstv()
    {
        $refTypeRodstv = RefTypeRodstv::factory()->create();

        $response = $this->get(route('ref-type-rodstvs.edit', $refTypeRodstv));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_type_rodstvs.edit')
            ->assertViewHas('refTypeRodstv');
    }

    /**
     * @test
     */
    public function it_updates_the_ref_type_rodstv()
    {
        $refTypeRodstv = RefTypeRodstv::factory()->create();

        $data = [];

        $response = $this->put(
            route('ref-type-rodstvs.update', $refTypeRodstv),
            $data
        );

        $data['id'] = $refTypeRodstv->id;

        $this->assertDatabaseHas('ref_type_rodstvs', $data);

        $response->assertRedirect(
            route('ref-type-rodstvs.edit', $refTypeRodstv)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_type_rodstv()
    {
        $refTypeRodstv = RefTypeRodstv::factory()->create();

        $response = $this->delete(
            route('ref-type-rodstvs.destroy', $refTypeRodstv)
        );

        $response->assertRedirect(route('ref-type-rodstvs.index'));

        $this->assertSoftDeleted($refTypeRodstv);
    }
}
