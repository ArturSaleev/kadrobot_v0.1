<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\EmployeeEducation;

use App\Models\Employee;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeEducationControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_employee_educations()
    {
        $employeeEducations = EmployeeEducation::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('employee-educations.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.employee_educations.index')
            ->assertViewHas('employeeEducations');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_employee_education()
    {
        $response = $this->get(route('employee-educations.create'));

        $response->assertOk()->assertViewIs('app.employee_educations.create');
    }

    /**
     * @test
     */
    public function it_stores_the_employee_education()
    {
        $data = EmployeeEducation::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('employee-educations.store'), $data);

        $this->assertDatabaseHas('employee_educations', $data);

        $employeeEducation = EmployeeEducation::latest('id')->first();

        $response->assertRedirect(
            route('employee-educations.edit', $employeeEducation)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_employee_education()
    {
        $employeeEducation = EmployeeEducation::factory()->create();

        $response = $this->get(
            route('employee-educations.show', $employeeEducation)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.employee_educations.show')
            ->assertViewHas('employeeEducation');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_employee_education()
    {
        $employeeEducation = EmployeeEducation::factory()->create();

        $response = $this->get(
            route('employee-educations.edit', $employeeEducation)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.employee_educations.edit')
            ->assertViewHas('employeeEducation');
    }

    /**
     * @test
     */
    public function it_updates_the_employee_education()
    {
        $employeeEducation = EmployeeEducation::factory()->create();

        $employee = Employee::factory()->create();

        $data = [
            'institution' => $this->faker->text(255),
            'year_begin' => $this->faker->randomNumber(0),
            'year_end' => $this->faker->randomNumber(0),
            'date_begin' => $this->faker->date,
            'date_end' => $this->faker->date,
            'speciality' => $this->faker->text(255),
            'qualification' => $this->faker->text(255),
            'diplom_num' => $this->faker->text(255),
            'diplom_date' => $this->faker->date,
            'employee_id' => $employee->id,
        ];

        $response = $this->put(
            route('employee-educations.update', $employeeEducation),
            $data
        );

        $data['id'] = $employeeEducation->id;

        $this->assertDatabaseHas('employee_educations', $data);

        $response->assertRedirect(
            route('employee-educations.edit', $employeeEducation)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_education()
    {
        $employeeEducation = EmployeeEducation::factory()->create();

        $response = $this->delete(
            route('employee-educations.destroy', $employeeEducation)
        );

        $response->assertRedirect(route('employee-educations.index'));

        $this->assertSoftDeleted($employeeEducation);
    }
}
