<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\EmployeeAddress;

use App\Models\Employee;
use App\Models\RefAddressType;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeAddressControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_employee_addresses()
    {
        $employeeAddresses = EmployeeAddress::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('employee-addresses.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.employee_addresses.index')
            ->assertViewHas('employeeAddresses');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_employee_address()
    {
        $response = $this->get(route('employee-addresses.create'));

        $response->assertOk()->assertViewIs('app.employee_addresses.create');
    }

    /**
     * @test
     */
    public function it_stores_the_employee_address()
    {
        $data = EmployeeAddress::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('employee-addresses.store'), $data);

        $this->assertDatabaseHas('employee_addresses', $data);

        $employeeAddress = EmployeeAddress::latest('id')->first();

        $response->assertRedirect(
            route('employee-addresses.edit', $employeeAddress)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_employee_address()
    {
        $employeeAddress = EmployeeAddress::factory()->create();

        $response = $this->get(
            route('employee-addresses.show', $employeeAddress)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.employee_addresses.show')
            ->assertViewHas('employeeAddress');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_employee_address()
    {
        $employeeAddress = EmployeeAddress::factory()->create();

        $response = $this->get(
            route('employee-addresses.edit', $employeeAddress)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.employee_addresses.edit')
            ->assertViewHas('employeeAddress');
    }

    /**
     * @test
     */
    public function it_updates_the_employee_address()
    {
        $employeeAddress = EmployeeAddress::factory()->create();

        $employee = Employee::factory()->create();
        $refAddressType = RefAddressType::factory()->create();

        $data = [
            'locale' => $this->faker->locale,
            'employee_id' => $employee->id,
            'ref_address_type_id' => $refAddressType->id,
        ];

        $response = $this->put(
            route('employee-addresses.update', $employeeAddress),
            $data
        );

        $data['id'] = $employeeAddress->id;

        $this->assertDatabaseHas('employee_addresses', $data);

        $response->assertRedirect(
            route('employee-addresses.edit', $employeeAddress)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_employee_address()
    {
        $employeeAddress = EmployeeAddress::factory()->create();

        $response = $this->delete(
            route('employee-addresses.destroy', $employeeAddress)
        );

        $response->assertRedirect(route('employee-addresses.index'));

        $this->assertSoftDeleted($employeeAddress);
    }
}
