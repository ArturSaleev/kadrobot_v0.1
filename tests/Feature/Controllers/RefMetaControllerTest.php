<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\RefMeta;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefMetaControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_ref_metas()
    {
        $refMetas = RefMeta::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('ref-metas.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_metas.index')
            ->assertViewHas('refMetas');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_ref_meta()
    {
        $response = $this->get(route('ref-metas.create'));

        $response->assertOk()->assertViewIs('app.ref_metas.create');
    }

    /**
     * @test
     */
    public function it_stores_the_ref_meta()
    {
        $data = RefMeta::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('ref-metas.store'), $data);

        $this->assertDatabaseHas('ref_metas', $data);

        $refMeta = RefMeta::latest('id')->first();

        $response->assertRedirect(route('ref-metas.edit', $refMeta));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_ref_meta()
    {
        $refMeta = RefMeta::factory()->create();

        $response = $this->get(route('ref-metas.show', $refMeta));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_metas.show')
            ->assertViewHas('refMeta');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_ref_meta()
    {
        $refMeta = RefMeta::factory()->create();

        $response = $this->get(route('ref-metas.edit', $refMeta));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_metas.edit')
            ->assertViewHas('refMeta');
    }

    /**
     * @test
     */
    public function it_updates_the_ref_meta()
    {
        $refMeta = RefMeta::factory()->create();

        $data = [
            'title' => $this->faker->text(255),
            'name' => $this->faker->name,
            'tablename' => $this->faker->text(255),
            'column_name' => $this->faker->text(255),
        ];

        $response = $this->put(route('ref-metas.update', $refMeta), $data);

        $data['id'] = $refMeta->id;

        $this->assertDatabaseHas('ref_metas', $data);

        $response->assertRedirect(route('ref-metas.edit', $refMeta));
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_meta()
    {
        $refMeta = RefMeta::factory()->create();

        $response = $this->delete(route('ref-metas.destroy', $refMeta));

        $response->assertRedirect(route('ref-metas.index'));

        $this->assertSoftDeleted($refMeta);
    }
}
