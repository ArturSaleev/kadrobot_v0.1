<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\BranchPhone;

use App\Models\Branch;
use App\Models\RefTypePhone;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BranchPhoneControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_branch_phones()
    {
        $branchPhones = BranchPhone::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('branch-phones.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.branch_phones.index')
            ->assertViewHas('branchPhones');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_branch_phone()
    {
        $response = $this->get(route('branch-phones.create'));

        $response->assertOk()->assertViewIs('app.branch_phones.create');
    }

    /**
     * @test
     */
    public function it_stores_the_branch_phone()
    {
        $data = BranchPhone::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('branch-phones.store'), $data);

        $this->assertDatabaseHas('branch_phones', $data);

        $branchPhone = BranchPhone::latest('id')->first();

        $response->assertRedirect(route('branch-phones.edit', $branchPhone));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_branch_phone()
    {
        $branchPhone = BranchPhone::factory()->create();

        $response = $this->get(route('branch-phones.show', $branchPhone));

        $response
            ->assertOk()
            ->assertViewIs('app.branch_phones.show')
            ->assertViewHas('branchPhone');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_branch_phone()
    {
        $branchPhone = BranchPhone::factory()->create();

        $response = $this->get(route('branch-phones.edit', $branchPhone));

        $response
            ->assertOk()
            ->assertViewIs('app.branch_phones.edit')
            ->assertViewHas('branchPhone');
    }

    /**
     * @test
     */
    public function it_updates_the_branch_phone()
    {
        $branchPhone = BranchPhone::factory()->create();

        $branch = Branch::factory()->create();
        $refTypePhone = RefTypePhone::factory()->create();

        $data = [
            'name' => $this->faker->name,
            'branch_id' => $branch->id,
            'ref_type_phone_id' => $refTypePhone->id,
        ];

        $response = $this->put(
            route('branch-phones.update', $branchPhone),
            $data
        );

        $data['id'] = $branchPhone->id;

        $this->assertDatabaseHas('branch_phones', $data);

        $response->assertRedirect(route('branch-phones.edit', $branchPhone));
    }

    /**
     * @test
     */
    public function it_deletes_the_branch_phone()
    {
        $branchPhone = BranchPhone::factory()->create();

        $response = $this->delete(route('branch-phones.destroy', $branchPhone));

        $response->assertRedirect(route('branch-phones.index'));

        $this->assertModelMissing($branchPhone);
    }
}
