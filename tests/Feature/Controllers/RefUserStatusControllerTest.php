<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\RefUserStatus;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefUserStatusControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_ref_user_statuses()
    {
        $refUserStatuses = RefUserStatus::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('ref-user-statuses.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_user_statuses.index')
            ->assertViewHas('refUserStatuses');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_ref_user_status()
    {
        $response = $this->get(route('ref-user-statuses.create'));

        $response->assertOk()->assertViewIs('app.ref_user_statuses.create');
    }

    /**
     * @test
     */
    public function it_stores_the_ref_user_status()
    {
        $data = RefUserStatus::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('ref-user-statuses.store'), $data);

        $this->assertDatabaseHas('ref_user_statuses', $data);

        $refUserStatus = RefUserStatus::latest('id')->first();

        $response->assertRedirect(
            route('ref-user-statuses.edit', $refUserStatus)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_ref_user_status()
    {
        $refUserStatus = RefUserStatus::factory()->create();

        $response = $this->get(route('ref-user-statuses.show', $refUserStatus));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_user_statuses.show')
            ->assertViewHas('refUserStatus');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_ref_user_status()
    {
        $refUserStatus = RefUserStatus::factory()->create();

        $response = $this->get(route('ref-user-statuses.edit', $refUserStatus));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_user_statuses.edit')
            ->assertViewHas('refUserStatus');
    }

    /**
     * @test
     */
    public function it_updates_the_ref_user_status()
    {
        $refUserStatus = RefUserStatus::factory()->create();

        $data = [];

        $response = $this->put(
            route('ref-user-statuses.update', $refUserStatus),
            $data
        );

        $data['id'] = $refUserStatus->id;

        $this->assertDatabaseHas('ref_user_statuses', $data);

        $response->assertRedirect(
            route('ref-user-statuses.edit', $refUserStatus)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_user_status()
    {
        $refUserStatus = RefUserStatus::factory()->create();

        $response = $this->delete(
            route('ref-user-statuses.destroy', $refUserStatus)
        );

        $response->assertRedirect(route('ref-user-statuses.index'));

        $this->assertSoftDeleted($refUserStatus);
    }
}
