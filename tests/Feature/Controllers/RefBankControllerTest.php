<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\RefBank;

use App\Models\RefStatus;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefBankControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_ref_banks()
    {
        $refBanks = RefBank::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('ref-banks.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_banks.index')
            ->assertViewHas('refBanks');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_ref_bank()
    {
        $response = $this->get(route('ref-banks.create'));

        $response->assertOk()->assertViewIs('app.ref_banks.create');
    }

    /**
     * @test
     */
    public function it_stores_the_ref_bank()
    {
        $data = RefBank::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('ref-banks.store'), $data);

        $this->assertDatabaseHas('ref_banks', $data);

        $refBank = RefBank::latest('id')->first();

        $response->assertRedirect(route('ref-banks.edit', $refBank));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_ref_bank()
    {
        $refBank = RefBank::factory()->create();

        $response = $this->get(route('ref-banks.show', $refBank));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_banks.show')
            ->assertViewHas('refBank');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_ref_bank()
    {
        $refBank = RefBank::factory()->create();

        $response = $this->get(route('ref-banks.edit', $refBank));

        $response
            ->assertOk()
            ->assertViewIs('app.ref_banks.edit')
            ->assertViewHas('refBank');
    }

    /**
     * @test
     */
    public function it_updates_the_ref_bank()
    {
        $refBank = RefBank::factory()->create();

        $refStatus = RefStatus::factory()->create();

        $data = [
            'mfo' => $this->faker->text(255),
            'mfo_head' => $this->faker->text(255),
            'mfo_rkc' => $this->faker->text(255),
            'kor_account' => $this->faker->text(255),
            'commis' => $this->faker->randomNumber(2),
            'bin' => $this->faker->text(255),
            'bik_old' => $this->faker->text(255),
            'ref_status_id' => $refStatus->id,
        ];

        $response = $this->put(route('ref-banks.update', $refBank), $data);

        $data['id'] = $refBank->id;

        $this->assertDatabaseHas('ref_banks', $data);

        $response->assertRedirect(route('ref-banks.edit', $refBank));
    }

    /**
     * @test
     */
    public function it_deletes_the_ref_bank()
    {
        $refBank = RefBank::factory()->create();

        $response = $this->delete(route('ref-banks.destroy', $refBank));

        $response->assertRedirect(route('ref-banks.index'));

        $this->assertSoftDeleted($refBank);
    }
}
