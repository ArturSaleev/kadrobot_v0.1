<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\DepartmentPhone;

use App\Models\Department;
use App\Models\RefTypePhone;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DepartmentPhoneControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_department_phones()
    {
        $departmentPhones = DepartmentPhone::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('department-phones.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.department_phones.index')
            ->assertViewHas('departmentPhones');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_department_phone()
    {
        $response = $this->get(route('department-phones.create'));

        $response->assertOk()->assertViewIs('app.department_phones.create');
    }

    /**
     * @test
     */
    public function it_stores_the_department_phone()
    {
        $data = DepartmentPhone::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('department-phones.store'), $data);

        $this->assertDatabaseHas('department_phones', $data);

        $departmentPhone = DepartmentPhone::latest('id')->first();

        $response->assertRedirect(
            route('department-phones.edit', $departmentPhone)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_department_phone()
    {
        $departmentPhone = DepartmentPhone::factory()->create();

        $response = $this->get(
            route('department-phones.show', $departmentPhone)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.department_phones.show')
            ->assertViewHas('departmentPhone');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_department_phone()
    {
        $departmentPhone = DepartmentPhone::factory()->create();

        $response = $this->get(
            route('department-phones.edit', $departmentPhone)
        );

        $response
            ->assertOk()
            ->assertViewIs('app.department_phones.edit')
            ->assertViewHas('departmentPhone');
    }

    /**
     * @test
     */
    public function it_updates_the_department_phone()
    {
        $departmentPhone = DepartmentPhone::factory()->create();

        $department = Department::factory()->create();
        $refTypePhone = RefTypePhone::factory()->create();

        $data = [
            'name' => $this->faker->name,
            'department_id' => $department->id,
            'ref_type_phone_id' => $refTypePhone->id,
        ];

        $response = $this->put(
            route('department-phones.update', $departmentPhone),
            $data
        );

        $data['id'] = $departmentPhone->id;

        $this->assertDatabaseHas('department_phones', $data);

        $response->assertRedirect(
            route('department-phones.edit', $departmentPhone)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_department_phone()
    {
        $departmentPhone = DepartmentPhone::factory()->create();

        $response = $this->delete(
            route('department-phones.destroy', $departmentPhone)
        );

        $response->assertRedirect(route('department-phones.index'));

        $this->assertModelMissing($departmentPhone);
    }
}
