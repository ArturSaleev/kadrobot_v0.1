<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\T2Card;

use App\Models\Branch;
use App\Models\Company;
use App\Models\Employee;
use App\Models\Position;
use App\Models\RefAction;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class T2CardControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_t2_cards()
    {
        $t2Cards = T2Card::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('t2-cards.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.t2_cards.index')
            ->assertViewHas('t2Cards');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_t2_card()
    {
        $response = $this->get(route('t2-cards.create'));

        $response->assertOk()->assertViewIs('app.t2_cards.create');
    }

    /**
     * @test
     */
    public function it_stores_the_t2_card()
    {
        $data = T2Card::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('t2-cards.store'), $data);

        $this->assertDatabaseHas('t2_cards', $data);

        $t2Card = T2Card::latest('id')->first();

        $response->assertRedirect(route('t2-cards.edit', $t2Card));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_t2_card()
    {
        $t2Card = T2Card::factory()->create();

        $response = $this->get(route('t2-cards.show', $t2Card));

        $response
            ->assertOk()
            ->assertViewIs('app.t2_cards.show')
            ->assertViewHas('t2Card');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_t2_card()
    {
        $t2Card = T2Card::factory()->create();

        $response = $this->get(route('t2-cards.edit', $t2Card));

        $response
            ->assertOk()
            ->assertViewIs('app.t2_cards.edit')
            ->assertViewHas('t2Card');
    }

    /**
     * @test
     */
    public function it_updates_the_t2_card()
    {
        $t2Card = T2Card::factory()->create();

        $company = Company::factory()->create();
        $employee = Employee::factory()->create();
        $branch = Branch::factory()->create();
        $refAction = RefAction::factory()->create();
        $position = Position::factory()->create();

        $data = [
            'company_id' => $company->id,
            'employee_id' => $employee->id,
            'ref_branch_id' => $branch->id,
            'ref_action_id' => $refAction->id,
            'ref_position_id' => $position->id,
        ];

        $response = $this->put(route('t2-cards.update', $t2Card), $data);

        $data['id'] = $t2Card->id;

        $this->assertDatabaseHas('t2_cards', $data);

        $response->assertRedirect(route('t2-cards.edit', $t2Card));
    }

    /**
     * @test
     */
    public function it_deletes_the_t2_card()
    {
        $t2Card = T2Card::factory()->create();

        $response = $this->delete(route('t2-cards.destroy', $t2Card));

        $response->assertRedirect(route('t2-cards.index'));

        $this->assertSoftDeleted($t2Card);
    }
}
